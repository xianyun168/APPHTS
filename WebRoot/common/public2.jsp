<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%> 
<% 
request.getSession().setMaxInactiveInterval(1800);
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<c:set var="ctx" value="${pageContext.request.scheme}${\'://\'}${pageContext.request.serverName}${':'}${pageContext.request.serverPort}${pageContext.request.contextPath}"></c:set>

<script type="text/javascript">
	var ctx = '${ctx}';
	//datagrid不能设置宽度长度百分比，手动转化
	function fixWidth(percent) {
		return document.body.clientWidth * percent; //这里你可以自己做调整   
	}

	//设置高度
	function fixHeight(percent) {
		return document.body.clientHeight * percent; //这里你可以自己做调整   
	}
	
	//日期格式转换
	function formattimed(val) {
		/*
		 *modiy by houwei 2014 11 30   不对空值进行格式化
		*/
		if (val == null || val == '') 
			return val;
		/***************************/
		var year=parseInt(val.year)+1900;
		var month=(parseInt(val.month)+1);
		month=month>9?month:('0'+month);
		var date=parseInt(val.date);
		date=date>9?date:('0'+date);
		var hours=parseInt(val.hours);
		hours=hours>9?hours:('0'+hours);
		var minutes=parseInt(val.minutes);
		minutes=minutes>9?minutes:('0'+minutes);
		var seconds=parseInt(val.seconds);
		seconds=seconds>9?seconds:('0'+seconds);
		var time=year+'-'+month+'-'+date;
		  return time;
	}	
	
	/**
	 *JQuery EasyUi时间格式化yyyy-MM-dd HH:mm:ss
	 *Author: 张瑞
	 *Dat：2014-11-20
	 */
	function formattimes(val) {
			/*
			 *modiy by houwei 2014 11 30   不对空值进行格式化
			*/
		if (val == null || val == '') 
			return val;
		/***************************/
		var year = parseInt(val.year) + 1900;
		var month = (parseInt(val.month) + 1);
		month = month > 9 ? month : ('0' + month);
		var date = parseInt(val.date);
		date = date > 9 ? date : ('0' + date);
		var hours = parseInt(val.hours);
		hours = hours > 9 ? hours : ('0' + hours);
		var minutes = parseInt(val.minutes);
		minutes = minutes > 9 ? minutes : ('0' + minutes);
		var seconds = parseInt(val.seconds);
		seconds = seconds > 9 ? seconds : ('0' + seconds);
		var time = year + '-' + month + '-' + date + ' ' + hours + ':'
				+ minutes + ':' + seconds;
		return time;
	}
	
//处理键盘事件 禁止后退键（Backspace）密码或单行、多行文本框除外  
function banBackSpace(e){     
    var ev = e || window.event;//获取event对象     
    var obj = ev.target || ev.srcElement;//获取事件源     
      
    var t = obj.type || obj.getAttribute('type');//获取事件源类型    
      
    //获取作为判断条件的事件类型  
    var vReadOnly = obj.getAttribute('readonly');  
    var vEnabled = obj.getAttribute('enabled');  
    //处理null值情况  
    vReadOnly = (vReadOnly == null) ? false : vReadOnly;  
    vEnabled = (vEnabled == null) ? true : vEnabled;  
      
    //当敲Backspace键时，事件源类型为密码或单行、多行文本的，  
    //并且readonly属性为true或enabled属性为false的，则退格键失效  
    var flag1=(ev.keyCode == 8 && (t=="password" || t=="text" || t=="textarea")   
                && (vReadOnly==true || vEnabled!=true))?true:false;  
     
    //当敲Backspace键时，事件源类型非密码或单行、多行文本的，则退格键失效  
    var flag2=(ev.keyCode == 8 && t != "password" && t != "text" && t != "textarea")  
                ?true:false; 
    //文本框输入空格时，空格键失效
    var flag3=(ev.keyCode ==32 && (t=="password" || t=="text"))
    			?true:false;
      
    //判断  
    if(flag2){  
        return false;  
    }  
    if(flag1){     
        return false;     
    }
    if(flag3){     
        return false;     
    }
}  
  
//禁止后退键 作用于Firefox、Opera  
document.onkeypress=banBackSpace;  
//禁止后退键  作用于IE、Chrome  
document.onkeydown=banBackSpace;
	
</script>
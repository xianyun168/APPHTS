/* 使用flexpaper加载一个swf文件
 * 必选参数：
 * 		cfg.containerId  一个div元素的id，用于装载swf
 * 		cfg.SWFFile      swf文件
 * 可选参数：
 * 		在【默认配置】不满足使用要求时，可在入参中重新指定。
 *
 * 一个最基本的用法：
 * 		loadSwfFile({containerId: 'divId', SWFFile: 'data/my.swf'});
 */
function loadSwfFile(cfg){
	
	// 【默认配置】如果用到了下面没有的配置项，需要先在下面补充其默认值，方可使用
	var defaultConfig = {
		jsDirectory : ctx + '/common/js/flexpaper/',
		//要展示的swf文件
		SWFFile : '',
		
		//缩放相关
		Scale : 0.6,
		ZoomTransition : 'easeOut',
		ZoomTime : 0.5,
		ZoomInterval : 0.2,
		MinZoomSize : 0.2,
		MaxZoomSize : 5,
		
		//初始化时自适应大小相关
		FitPageOnLoad : true,
		FitWidthOnLoad : false,
		
		//当设置为true的时候，单击全屏按钮会打开一个flexpaper最大化的新窗口而不是全屏
		FullScreenAsMaxWindow : false,
		//当设置为true的时候，展示文档时不会加载完整个文档，而是逐步加载
		ProgressiveLoading : false,
		//设置为true的时候，单击搜索所有符合条件的地方高亮显示
		SearchMatchAll : false,
		//设置启动模式如"Portrait" or "TwoPage"
		InitViewMode : 'Portrait',
		RenderingOrder : 'flash',
		//加载完成后显示第几页，为空时是第一页
		StartAtPage : '',

		//控制工具栏按钮显示
		//样式选择框
		ViewModeToolsVisible : true,
		//缩放工具
		ZoomToolsVisible : true,
		//导航工具
		NavToolsVisible : true,
		//光标工具
		CursorToolsVisible : true,
		//搜索
		SearchToolsVisible : true,
		WMode : 'window',
		//设置地区
		localeChain: 'zh_CN'
	};
	//优先使用入参中制定了的配置
	for(var i in defaultConfig){
		if(typeof(cfg[i]) != "undefined"){
			defaultConfig[i] = cfg[i];
		}
	}
	//加载
	$('#' + cfg.containerId).FlexPaperViewer(
        { config : defaultConfig }
	);
}
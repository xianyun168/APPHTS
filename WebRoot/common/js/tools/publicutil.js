/*************************************************************
 *@author:zhangrui
 *@date:2014-09-10 
 * 页面操作的通用方法
 *************************************************************/

/**
 * 打开一个增加页面
 */
function add_win(title, addurl, gname, width, height) {
	gridname = gname;
	width = width ? width : 700;
	height = height ? height : 400;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		content : 'url:' + addurl,
		lock : true,
		width : width,
		height : height,
		title : title,
		opacity : 0.3,
		cache : false
	});
}

/**
 * 打开一个详细信息页面
 */
function info(title, addurl, width, height) {
	width = width ? width : 1000;
	height = height ? height : 500;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		content : 'url:' + addurl,
		lock : true,
		width : width,
		height : height,
		title : title,
		opacity : 0.3,
		cache : false
	});
}

function update(title, url, id, width, height) {
	gridname = id;

	var rowsData = $('#' + id).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择编辑项目');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条记录再编辑');
		return;
	}

	url += '&pkId=' + rowsData[0].pkId;
	width = width ? width : 700;
	height = height ? height : 400;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		content : 'url:' + url,
		lock : true,
		width : width,
		height : height,
		title : title,
		opacity : 0.3,
		cache : false
	});
}

/**
 * 提示信息
 */
function tip(msg) {
	$.dialog.setting.zIndex = 1980;
	window.top.$.messager.show({
		title : '提示信息',
		msg : msg,
		timeout : 2000 * 1.5
	});
}

/**
 * 执行AJAX提交FORM
 * 
 * @param url
 * @param gridname
 */
function ajaxSubForm(url) {
	$('#myform').form('submit', {
		url : url,
		onSubmit : function() {
			iframe.editor.sync();
		},
		success : function(r) {
			tip('操作成功');
			reloadTable();
		}
	});
}

function saveData(url) {
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		// $.dialog.tips('数据保存中...',500,'loading.gif');
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
			'submit',
			{
				url : url,
				onSubmit : function() {
					var form = $("#myform").Validform();
					return form.check();
				},
				success : function(data) {
					var windowapi = frameElement.api, W = windowapi.opener;
					var d;
					try {
						d = $.parseJSON(data);
						if (d.success == true) {
							window.top.$.messager.progress('close');
							windowapi.close();
							W.tip(d.msg);
							W.query();
						}
						/*
						 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
						 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
						 */
						else {
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('提示', d.msg);
							return;
						}
					} catch (ex) {
						var emsg = data.substring(data.indexOf('错误描述'), data
								.indexOf('错误信息'));
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('错误', emsg);
						return;
					}
				}
			});
}
/**
 * 按id删除
 * 
 * @param url
 * @param id
 * @return
 */
function removeId(url, id) {
	var rowsData = $('#' + id).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择删除项目');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条记录再删除');
		return;
	}
	url += '&pkId=' + rowsData[0].pkId;
	window.top.$.messager.confirm('Confirm', '你确定要删除吗', function(r) {
		if (r) {
			$.post(url, function(result) {
				if (result.success) {
					$('#' + id).datagrid('reload'); // reload the user data
					$('#' + id).treegrid('reload'); // reload the user data
				} else {
					window.top.$.messager.show({// show error message
						title : 'Error',
						msg : result.errorMsg
					});
				}
			}, 'json');
		}
	});
}


/**
 * 按ID删除
 * 
 * @param url
 * @param id
 * @return
 */
function deleteId(url, id) {
	var rowsData = $('#' + id).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择删除项目');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条记录再删除');
		return;
	}
	var sPkId = sPkId = rowsData[0].nodePkId;
	if(sPkId == undefined || sPkId == ''){
		sPkId = rowsData[0].brokerPkId;
	}else{
		sPkId = rowsData[0].pkId;
	}
	url += '&pkId=' + sPkId;
	window.top.$.messager.confirm('Confirm', '你确定要删除吗', function(r) {
		if (r) {
			$.post(url, function(result) {
				if (result.success) {
					$('#' + id).datagrid('reload'); // reload the user data
					$('#' + id).treegrid('reload'); // reload the user data
				} else {
					window.top.$.messager.show({// show error message
						title : 'Error',
						msg : result.errorMsg
					});
				}
			}, 'json');
		}
	});
}



/**
 * 根据ID更新
 * 
 * @param url
 * @param id
 * @return
 */
function updateStatus(url, id) {
	var rowsData = $('#' + id).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择项目');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条记录');
		return;
	}
	url += '&pkId=' + rowsData[0].pkId;
	window.top.$.messager.confirm('Confirm', '你确定要更改状态吗', function(r) {
		if (r) {
			$.post(url, function(result) {
				if (result.success) {
					$('#' + id).datagrid('reload'); // reload the user data
					$('#' + id).treegrid('reload'); // reload the user data
				} else {
					window.top.$.messager.show({ // show error message
						title : 'Error',
						msg : result.errorMsg
					});
				}
			}, 'json');
		}
	});
}

/**
 * 根据路由权限IDauthorityId更新
 * 
 * @param url
 * @param id
 * @return
 */
function updateAuthority(url, id) {
	var rowsData = $('#' + id).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择一条记录');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条记录');
		return;
	}
	url += '&pkId=' + rowsData[0].pkId;
	window.top.$.messager.confirm('Confirm', '你确定要启用/禁用路由规则吗', function(r) {
		if (r) {
			$.post(url, function(result) {
				if (result.success) {
					$('#' + id).datagrid('reload'); // reload the user data
					$('#' + id).treegrid('reload'); // reload the user data
					
				} else {
					window.top.$.messager.show({ // show error message
						title : 'Error',
						msg : result.errorMsg
					});
				}
			}, 'json');
		}
		tip('路由规则启用/禁用成功');
	});
	
}

function init() {
	//回车时不提交表单
	 $(function () {
		  $("*").each(function () {
		   $(this).keypress(function (e) {
		    var key = window.event ? e.keyCode : e.which;
		    if (key.toString() == "13") {
		     return false;
		    }
		   });
		  });
		 });
	$("#myform").Validform(
			{
				tiptype : 4,
				btnSubmit : "#btn_sub",
				btnReset : "#btn_reset",
				ajaxPost : true,
				callback : function(data) {
					if (data.success == true) {
						var windowapi = frameElement.api, W = windowapi.opener;
						windowapi.close();
						W.tip("成功");
					} else {
						if (data.responseText == ''
								|| data.responseText == undefined) {
							window.top.$.messager.alert('错误', data.msg);
						} else {
							try {
								var emsg = data.responseText.substring(
										data.responseText.indexOf('错误描述'),
										data.responseText.indexOf('错误信息'));
								window.top.$.messager.alert('错误', emsg);
							} catch (ex) {
								window.top.$.messager.alert('错误', data.responseText + "");
							}
						}
						return false;
					}
				}
			});

}

function closeWindow() {
	var windowapi = frameElement.api;
	windowapi.close();
}

/**
 * 附件上传窗口 add by 张瑞 2014-11-19
 */
function openUpLoadWin(title, width, height) {
	$.dialog({
		content : 'url:fileController.do?fileUpload=key',
		zIndex : 1997,
		title : '附件上传',
		lock : true,
		width : 700,
		height : 400,
		cancel : false,
		left : '85%',
		top : '65%',
		opacity : 0.4
	});

}

/**
 *置空条件 add by  2014-12-30
 */
function setNull(obj){
	var inputs= $('#'+obj).find("input");
	var selects=$('#'+obj).find("select");
	for(var i=0;i<inputs.length;i++){
		if(inputs[i].type == 'text'){
			inputs[i].value='';
		}
	}
	for(var i=0;i<selects.length;i++){
		selects[i].value='';
	}
}

/*****************************附件上传的方法从public.jsp迁移过来*******************************************************/
/***
*附件的鼠标移动事件
*/
function divOver(div) {
	div.style.backgroundImage = "url(${ctx}/images/meun_bg_hover.jpg)";
}
function divOut(div) {
	div.style.backgroundImage = "url(${ctx}/images/meun_bg.jpg)";
}

/***
 *删除附件
 */
function delInfo(info, idno) {
	var uploadinfo = $('#uploadinfo').val();
	var showinfo = document.getElementById("showinfo" + idno);
	uploadinfo = uploadinfo.replace(info + ",", "");
	showinfo.style.display = "none";
	$('#uploadinfo').val(uploadinfo);
}


/**
*
*附件下载(下载公共方法)
* add by 张瑞
*/
function down(dgid) {
	var selection = $(dgid).datagrid('getSelections');
	if (!selection || selection.length == 0) {
		//$.messager.alert('提示', '请选择要下载的文件!', 'info');
		alert("请选择要下载的文件!");
		return;
	}
	var ids = '';
	for (i = 0; i < selection.length; i++) {
		ids = ids + selection[i].pkId + ',';
	}
	ids = ids.substr(0, ids.length - 1);
	//下载打包等待处理…………
	location.href = "fileController.do?downFiles=key&ids="+ ids;
}

//删除
function del(dgid){
	//获得选中的行
	var selection = $(dgid).datagrid('getSelections');
	if (!selection || selection.length == 0) {
		window.top.$.messager.alert('提示', '请选择要删除的文件!', 'info');
		return;
	}
	var deluploadinfo = $('#deluploadinfo').val();
	for (i = 0; i < selection.length; i++) {
		//获取选中行的新索引（index）,因为删除变更的原因，索引会根据删除个数个变化 
		var index = $(dgid).datagrid('getRowIndex',selection[i]);
		//获得附件的id  modiy by houwei  id  改成了  pkId   
		var id = selection[i]['pkId'];
		if(deluploadinfo == "" || deluploadinfo == null){
			deluploadinfo = id;
		}else{
			deluploadinfo = deluploadinfo +","+id;
		}
		$(dgid).datagrid('deleteRow',index);
	}
	//添加要删除的附件信息的id
	$('#deluploadinfo').val(deluploadinfo);
}
//撤销删除
function reDel(dgid){
	$('#deluploadinfo').val('');
	$(dgid).datagrid('reload');
}
/**
*
* 替换空格的公用方法
* add by 赵华
*/
function notNull(e){
	var ev = e || window.event;
	if(ev.keyCode == 32)ev.returnValue = false;
}
/**
*
* 校验开始结束时间的公用方法
* add by 赵华
*/

//解析String to date
function parseDate(str){   
  if(typeof str == 'string'){   
    var results = str.match(/^ *(\d{4})-(\d{1,2})-(\d{1,2}) *$/);   
    if(results && results.length>3)   
      return new Date(parseInt(results[1]),parseInt(results[2]) -1,parseInt(results[3]));    
    results = str.match(/^ *(\d{4})-(\d{1,2})-(\d{1,2}) +(\d{1,2}):(\d{1,2}):(\d{1,2}) *$/);   
    if(results && results.length>6)   
      return new Date(parseInt(results[1]),parseInt(results[2]) -1,parseInt(results[3]),parseInt(results[4]),parseInt(results[5]),parseInt(results[6]));    
    results = str.match(/^ *(\d{4})-(\d{1,2})-(\d{1,2}) +(\d{1,2}):(\d{1,2}):(\d{1,2})\.(\d{1,9}) *$/);   
    if(results && results.length>7)   
      return new Date(parseInt(results[1]),parseInt(results[2]) -1,parseInt(results[3]),parseInt(results[4]),parseInt(results[5]),parseInt(results[6]),parseInt(results[7]));    
  }   
  return null;   
}  
//校验开始时间和结束时间
function seach(startDay,endDay){
//	var startDay = $('#startTime').val();
//	var endDay = $('#endTime').val();
	var sd = new Date();
	if(startDay!=""&&endDay!=""){
//		var sDate = parseDate(startDay);
//		var eDate = parseDate(endDay);
		var sDate = new Date(startDay.replace(/-/g,"/"));
		var eDate = new Date(endDay.replace(/-/g,"/"));
		//alert("kaishi:"+sDate+" jieshu:"+eDate);
		if ((sd.getTime() - sDate.getTime()) < 0) {
			alert('开始日期不能晚于当前日期');
			return false;
		}
		if ((sd.getTime() - eDate.getTime()) < 0) {
			alert('结束日期不能晚于当前日期');
			return false;
		}
		if ((eDate.getTime() < sDate.getTime())) {
			alert('开始日期不能晚于结束日期');
			return false;
		}
	}
	if(startDay!=""&& endDay==""){
		alert('结束日期不能为空');
			return false;
//		var sDate = parseDate(startDay);
//		if ((sd.getTime() - sDate.getTime()) < 0) {
//			alert('开始日期不能晚于当前日期');
//			return false;
//		}
	}
	if(startDay==""&& endDay!=""){
		alert('开始日期不能为空');
			return false;
//		var eDate = parseDate(endDay);
//		if ((sd.getTime() - eDate.getTime()) < 0) {
//			alert('结束日期不能晚于当前日期');
//			return false;
//		}
	}
	return true;
}











/**
 * 显示进度条
 */
function showProgressBar(){
		var html = '<div>' +
						'<iframe name="dialog" style="display:none"></iframe>' +
						'<div style="width:400px;height:120px;border:1px solid #000">' +
							'<div class="progress progress-striped" style="width:250px;margin:15px 5px">' +
								'<div class="progress-bar progress-bar-success" id="bar"' +
									'role="progressbar" aria-valuenow="100" aria-valuemin="0"' +
									'aria-valuemax="100"' +
									'style="width:0%">' +
									'<span style="color:black" id="progress_percent">60%</span>'+
								'</div>' +
							'</div>' +
							'<div style="width:400px;margin:5px 5px">'+
								'已上传：<span id="has_upload">0 </span>MB'+
								'剩余：<span id="rem_upload">0 </span>MB'+  
								'速度：<span id="upload_speed">0 </span>KB/S'+
							'<div><span id="success"></span></div>'+
							'<div><span id="preview" style="display:none"><a id="previewUrl" href="#">预览</a></span></div>'+
							'</div>'+
							'<div style="height:2px;width:200px;margin:15px 5px;background-color:#000000"></div>'+
						'</div>'+
				   '</div>';

		dialog = $.dialog({
			id:'dlg',
			content: html,
			title:false,//'正在上传...'
			cancel:true,
			cancelVal:'上传终止',
			button:[{
		        name: '退出',
		        disabled: true,
		        focus: true
		    }],
			width: 500,
			height: 150,
			lock: true,
			cache:false
		});
}

var dialog;//弹出框命名
/**
 * 上传进度条的显示与监控
 * @param url
 * @return
 */
function addProgressBar(url){
	showProgressBar();
	getProgress(url);
}
/**
 * 上传成功后的处理方法
 * @param data
 * @param status
 * @return
 */
function success(data,status,id){
	var url = "/kmsFileController.do?view&fileId=" + id;
	$("#success").text(data);
	$("#previewUrl").attr("href",ctx + url);
	$("#preview").css("display","inline");
	dialog.button(
	    {	name: '退出',
	        disabled: false
	    }
	);	
}
/**
 * 监控进度(配合后台的com.ins.sinosoft.framework.base.filelode.ProgressEntity使用)
 * @param url 监控请求
 * @return
 */
function getProgress(url) {
	var now = new Date();
	var percent;
	var fn = function(data) {
    	percent = data.percent;
		$("#progress_percent").text(data.percent);
    	$("#rem_upload").text(data.mbRemain);
        $("#bar").width(data.percent);
        $("#has_upload").text(data.mbRead);
        $("#upload_speed").text(data.speed);
        if(percent != "100%"){
	    	setTimeout("getProgress('" + url + "')", 100);
	    }else{
	    	$("#upload_speed").text(0);
	    }
    }
    $.ajax({
        type: "post",
        dataType: "json",
        url: url,
        data: now.getTime(),
        success: function(data){fn(data);},
        error: function(err) {      	
        	$("#progress_percent").text("Error");
        }
    });
}
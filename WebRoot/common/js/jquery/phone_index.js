$(function(){
	// 延迟加载
	$("img.lazy").lazyload({
		effect : "fadeIn"
		//threshold : 400 //默认的时候是图片出现在可是区域就加载 故此 将此参数注释
	});
});


//获去首页轮播图个数num
var num='${num}';
//搜索引擎js配置
$().ready( function() {
	$productSearchForm = $("#productSearchForm");//搜索表单
	$productSearchFormButton = $("#productSearchFormButton");//搜索表单提交按钮
	$productSearchKeyword = $("#productSearchKeyword");//搜索关键词标签
	$keyword = $.trim($productSearchKeyword.val());//获取搜索关键词，并且去首尾空格
	$productSearchFormButton.bind("click",function(){
		var v =$.trim($productSearchKeyword.val());
		if(v=="" || v =="搜索"){
			sj_alert("请输入关键字!");
			return false;
		}else{
		//转码
			var k = escape(v);
			$("#keyWord").val(k);
			var ot=$("#orderType").val();
			if(ot==''){
				$("#orderType").val("putSaleDate");
			}
			$productSearchForm.submit();
		}
	});
	$productSearchKeyword.blur(function(){
		var v = $.trim($(this).val());
		if(v==""){
			$(this).val("搜索");
		}
	});
	$productSearchKeyword.focus(function(){
		var v = $.trim($(this).val());
		if(v=="搜索"){
			$(this).val("");
		}
	});
	if($keyword == ""){
		$productSearchKeyword.val("搜索");
	}
});
//提交表单拦截（按回车时）
function submitForm(event){
	var e= event ? event : (window.event ? window.event : null);
	if (e.keyCode == 13){
		e.returnValue=false;
		e.cancel = true;
		$productSearchForm = $("#productSearchForm");//搜索表单
		$productSearchFormButton = $("#productSearchFormButton");//搜索表单提交按钮
		$productSearchKeyword = $("#productSearchKeyword");//搜索关键词标签
		$keyword = $.trim($productSearchKeyword.val());//获取搜索关键词，并且去首尾空格
		var v =$.trim($productSearchKeyword.val());
		if(v==""||v =="搜索"){
			sj_alert("请输入关键字!");
			e.cancel = false;
			return false;
		}else{
			//转码
			var k = escape(v);
			$("#keyWord").val(k);
			$productSearchForm.submit();
		}
	}
}
$(document).ready(function() {
	$(".main_visual").hover(function() {
		$("#btn_prev,#btn_next").fadeIn()
	}, function() {
		$("#btn_prev,#btn_next").fadeOut()
	});
	$dragBln = false;
	$(".main_image").touchSlider({
		flexible : true,
		speed : 200,
		btn_prev : $("#btn_prev"),
		btn_next : $("#btn_next"),
		paging : $(".flicking_con a"),
		counter : function(e) {
			$(".flicking_con a").removeClass("on").eq(num-e.current).addClass("on");
		}
	});
	$(".main_image").bind("mousedown", function() {
		$dragBln = false;
	});
	$(".main_image").bind("dragstart", function() {
		$dragBln = true;
	});
	$(".main_image a").click(function() {
		if ($dragBln) {
			return false;
		}
	});
	timer = setInterval(function() {
		$("#btn_next").click();
	}, 5000);
	$(".main_visual").hover(function() {
		clearInterval(timer);
	}, function() {
		timer = setInterval(function() {
			$("#btn_next").click();
		}, 5000);
	});
	$(".main_image").bind("touchstart", function() {
		clearInterval(timer);
	}).bind("touchend", function() {
		timer = setInterval(function() {
			$("#btn_next").click();
		}, 5000);
	});
});
//首页顶部跳转分类
function searchCategory(){
	window.location = "/phone/gotoProductType.action";
}
//切换搜索模块
function gotoShowSearchPage(){
	$(".new_main").attr("style","display:none;");
	$("body").attr("style","background-color:#fff;");
	$("#searchPage").removeAttr("style");
	//追加搜索历史
	appendSearchHistory();
}
//返回首页
function goback(){
	$("#searchPage").attr("style","display:none;");
	$(".new_main").removeAttr("style");
}
//点击topSearch
function topSearch(keyWord){
	var newKey = escape(keyWord);
	if(newKey.length<300){
		selectProduct(keyWord);//搜索关键字写入cookie
		$("#keyWord").val(newKey);
		$("#productSearchForm").submit();
	}
}
//点击搜索
function submitSearch(){
	$productSearchKeyword = $("#productSearchKeyword");//搜索关键词标签
	$keyword = $.trim($productSearchKeyword.val());//获取搜索关键词，并且去首尾空格
	var v =$.trim($productSearchKeyword.val());
	if(v==""||v =="搜索"){ //搜索你想要的商品
		sj_alert("请输入关键字!");
	}else{
		selectProduct(v);//搜索关键字写入cookie
		//转码
		var k = escape(v);
		$("#keyWord").val(k);
		$productSearchForm.submit();
	}
}
//搜索关键字写入cookie
function selectProduct(selectKeyWord){
	var cookieValue = $.cookie('searchHistory');
	var value="[{'selectKeyWord':'"+selectKeyWord+"'}";
	if(cookieValue!=null){
		//从cookie中取出之前的值
		var oldCookieValue = eval(cookieValue);//格式化之前cookie中的数据，以便与当前加入的数据做对比
		var oldCookieLength = oldCookieValue.length;//不同关键字的个数
		for(var i=0;i<oldCookieLength;i++){//迭代之前cookie
			if(selectKeyWord!=oldCookieValue[i].selectKeyWord){//判断当前操作的关键字是否在之前的cookie中
				//重新组装之前cookie数据
				value +=",{'selectKeyWord':'"+oldCookieValue[i].selectKeyWord+"'}";//以json格式存放，方便维护和取
			}
		}
	}
	value =value+"]";//以json格式存放，方便维护和取
	//保存到cookie中
$.cookie('searchHistory', value , {expires: 7, path:"/"}); //设置带时间的cookie 7天，加path属性时，只能其他页面使用，且其他页面取值条件为当前页面的上级路径包含path一致
}
//清空cookie
function deleteCookie(){
	$.cookie('searchHistory', '' , {expires: -1, path:"/"});
	//追加搜索历史
	appendSearchHistory();
}
//遍历搜索历史cookie
function appendSearchHistory(){
	var cookieValue = $.cookie('searchHistory');
	if(cookieValue!=null){
		//从cookie中取出之前的值
		var oldCookieValue = eval(cookieValue);//格式化之前cookie中的数据，以便与当前加入的数据做对比
		var oldCookieLength = oldCookieValue.length;//不同关键字的个数
        var html_handle = "";//组成的显示搜索历史html字符串
        var html_handle_temp = "";//临时获取对象
        var html_handle_clone = $("#index_search_div").clone();//复制搜索标签模板，需要替换其中内容
        for(var i=0;i<oldCookieLength;i++){//迭代之前cookie
            //每次循环模板重复给临时对象复制
            html_handle_clone_temp = html_handle_clone;
            //改变a标签对象属性值
            html_handle_clone_temp.find("a").attr("onclick","javascript:topSearch('"+oldCookieValue[i].selectKeyWord+"');");
            //改变i标签对象文本结果值
            html_handle_clone_temp.find("i").html(oldCookieValue[i].selectKeyWord);
            //把临时对象值给删除对象
            html_handle += html_handle_clone_temp.html();
		}
	}
	$(".searchHistory").html(html_handle);
}
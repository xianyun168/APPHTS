/**
 * datatype扩展
 */
$.Datatype.need1 = function(gets, obj, curform, regxp) {
	var need = 1, numselected = curform.find("input[name='" + obj.attr("name") + "']:checked").length;
	return numselected >= need ? true : "请至少选择" + need + "项！";
};
$.Datatype.need2 = function(gets, obj, curform, regxp) {
	var need = 2, numselected = curform.find("input[name='" + obj.attr("name") + "']:checked").length;
	return numselected >= need ? true : "请至少选择" + need + "项！";
};
$.Datatype.d=/^(\d*\.)?\d+$/;


/**
 *不能输入汉字并少于100个字符 
 */
$.Datatype.NotChina= function(gets,obj,curform,regxp){
	if (/^[\/\.\:\w\\\u4e00-\u9fa5]{1,100}$/.test(gets)){ 
		return true;
	}
	return "不能输入特殊字符或标点符号且须少于100个字符";
};
/**
 *不能输入汉字并少于36个字符 
 */
$.Datatype.NotChina1_36= function(gets,obj,curform,regxp){
	if (!/[\u4e00-\u9fa5]/.test(gets)&&/^[\w\W]{1,36}$/.test(gets)){ 
		return true;
	}
	return "不能输入汉字且须少于36个字符";
};
/**
 *不能输入汉字并少于128个字符 
 */
$.Datatype.NotChina1_128= function(gets,obj,curform,regxp){
	if (!/[\u4e00-\u9fa5]/.test(gets)&&/^[\w\W]{1,128}$/.test(gets)){ 
		return true;
	}
	return "不能输入汉字且须少于128个字符";
};
/**
 *不能输入汉字并少于32个字符 
 */
$.Datatype.NotChina1_32= function(gets,obj,curform,regxp){
	if (!/[\u4e00-\u9fa5]/.test(gets)&&/^[\w\W]{1,32}$/.test(gets)){ 
		return true;
	}
	return "不能输入汉字且须少于32个字符";
};

$.Datatype.NotIp=function(gets,obj,curform,regxp){
	if (/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(gets)){ 
		return true;
	}
	return "请输入正确的ip地址";
};

$.Datatype.NotUrl=function(gets,obj,curform,regxp){
	if(/^(http|ftp|https):\/\/((2[0-4]\d|25[0-5]|[01]?\d\d?)\.){3}(2[0-4]\d|25[0-5]|[01]?\d\d?):\d{2,5}(\/\w*)*$/.test(gets)&&/^[\w\W]{1,100}$/.test(gets)){
		return true;
	}
	return "请输入正确地址";
};
$.Datatype.NotUr=function(gets,obj,curform,regxp){
	if(/^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\':+!]*([^<>\"\"])*$/.test(gets)){
		return true;
	}
	return "请输入正确地址";
};
/**
 * 不能输入错误的菜单地址
 */
$.Datatype.RequestUrl=function(gets,obj,curform,regxp){
	if(/^((\w+\.[d][o]\?\w+\=\w+)*)$/.test(gets)){
		return true;
	}
	return "请输入正确的菜单地址";
};
$.Datatype.en=function(gets,obj,curform,regxp){
	if(/^[a-zA-Z]{1,100}$/.test(gets)){
		return true;
	}
	return "请输入1-100位英文";
};
$.Datatype.english=function(gets,obj,curform,regxp){
	if(/^[a-zA-Z]{4,50}$/.test(gets)){
		return true;
	}
	return "请输入4-50位英文";
};
$.Datatype.EnData=function(gets,obj,curform,regxp){
	if(/^[a-zA-Z0-9]{1,40}$/.test(gets)){
		return true;
	}
	return "请输入1-40位英文或数字";
};
$.Datatype.lishenglish=function(gets,obj,curform,regxp){
	if(/^[a-zA-Z]{4,36}$/.test(gets)){
		return true;
	}
	return "请输入4-36位英文";
};
$.Datatype.zh=function(gets,obj,curform,regxp){
	if(/^[\u4e00-\u9fa5]{1,16}$/.test(gets)){
		return true;
	}
	return "请输入1-16个汉字";
};
$.Datatype.st=function(gets,obj,curform,regxp){
	if(/^[\u4e00-\u9fa5]|[a-z]|[A-Z]{1,128}$/.test(gets)){
		return true;
	}
	return "请输入1-128字符";
};
$.Datatype.zh2=function(gets,obj,curform,regxp){
	if(/[\u4e00-\u9fa5]{2,16}$/.test(gets)){
		return true;
	}
	return "请输入2-16个汉字";
};
$.Datatype.zhEn=function(gets,obj,curform,regxp){
	if(/^[\u4e00-\u9fa5]{1,12}$|^[\dA-Za-z_]{1,36}$/.test(gets)){
		return true;
	}
	return "请输入1-12中文或1-36英文数字";
};

$.Datatype.model=function(gets,obj,curform,regxp){
	if(/^[\u4e00-\u9fa5]{1,33}$|^[\dA-Za-z_]{1,100}$/.test(gets)){
		return true;
	}
	return "请输入1-100字符";
};

$.Datatype.re=function(gets,obj,curform,regxp){
	if(/[\u4e00-\u9fa5]|[a-z]|[A-Z]{1,50}$/.test(gets)){
		return true;
	}
	return "请输入1-50字符";
};

$.Datatype.chinese= function(gets,obj,curform,regxp){
	if (/[\u4e00-\u9fa5\\w]/.test(gets) &&  /^[\w\W]{1,25}$/.test(gets) /*/^\d{250}$/.test(gets) && /^[\w\W]{0,250}$/.test(gets) && /^[\u4e00-\u9fa5]{0,125}$/.test(gets)*/){ 
		return true;
	}
	return "请输入正确的备注";
};
//验证操作原因
$.Datatype.REASON= function(gets,obj,curform,regxp){
	if (/[\u4e00-\u9fa5\\w]/.test(gets) &&  /^[\w\W]{1,200}$/.test(gets) /*/^\d{250}$/.test(gets) && /^[\w\W]{0,250}$/.test(gets) && /^[\u4e00-\u9fa5]{0,125}$/.test(gets)*/){ 
		return true;
	}
	return "请输入正确的原因";
};
//验证 输入数字、字母、中文
$.Datatype.par= function(gets,obj,curform,regxp){
	if (/^.{1,200}$/.test(gets)){ 
		return true;
	}
	return "请输入1-200位合法的字符";
};
//验证操作原因
$.Datatype.Rea= function(gets,obj,curform,regxp){
	if (/^.{1,20}$/.test(gets)){ 
		return true;
	}
	return "请输入1-20位合法的字符";
};

//验证 输入数字、字母、中文
$.Datatype.rec= function(gets,obj,curform,regxp){
	if (/^.{0,20}$/.test(gets)){ 
		return true;
	}
	return "请输入正确的名称";
};

$.Datatype.address= function(gets,obj,curform,regxp){
	if (/[1-9a-zA-Z`~!@#$%^&\*()-_=+\{\}\[\]\\\;\'\"\,\.\<\>\?\/]{1,150}/.test(gets)){ 
		return true;
	}
	return "请输入正确的地址";
};
//[\s\S]*
$.Datatype.company= function(gets,obj,curform,regxp){
	if (/[\u4e00-\u9fa51-9a-zA-Z`~!@#$%^&\*()-_=+\{\}\[\]\\\;\'\"\,\.\<\>\?\/\u3002\uff1b\uff0c\uff1a\u201c\u201d\uff08\uff09\u3001\uff1f\u300a\u300b]{3,40}/.test(gets)){ 
		return true;
	}
	return "请输入正确的名称";
};
//校验金额
$.Datatype.money= function(gets,obj,curform,regxp){
	if (/^(([0-9]{1,10})|(([0]\.\d{1,2}|[0-9]{1,10}\.\d{1,2})))$/.test(gets)){ 
		return true;
	}
	return "请输入正确的金额";
};

//输入10为数字
$.Datatype.number= function(gets,obj,curform,regxp){
	if (/^([0-9]{1,10})$/.test(gets)){ 
		return true;
	}
	return "请输入正确的积分";
};

//快递单号校验  
$.Datatype.courierNum=function(gets,obj,curform,regxp){
	if(/^[a-zA-Z0-9]{12,18}$/.test(gets)){
		return true;
	}
	return "请输入12-18位英文或数字";
};


<html>
<head>
<title>ESB</title>
		<meta http-equiv="content-type" content="text/html;charset=utf-8">
		<link rel="stylesheet" href="${ctx}/common/js/easyui/1.4/themes/default/easyui.css" />
		<link rel="stylesheet" href="${ctx}/common/js/easyui/1.4/themes/default/datagrid.css" />
		<link rel="stylesheet" href="${ctx}/common/js/easyui/1.4/themes/icon.css" />
	     <link rel="stylesheet" href="${ctx}/common/js/accordion/css/accordion.css" />
	     <link rel="stylesheet" href="${ctx}/common/js/accordion/css/icons.css" type="text/css"/>
		
		<%-- <link rel="stylesheet" href="${ctx}/common/js/tools/css/common.css" /> --%>
		<link rel="stylesheet" href="${ctx}/common/js/Validform/css/style.css" />
		<link rel="stylesheet" href="${ctx}/common/js/Validform/css/tablefrom.css" />

    	<script src="${ctx}/common/js/jquery/1.11.1/jquery.min.js"></script>
    	<script src="${ctx}/common/js/jquery/jquery-ui.min.js"></script>
		<script src="${ctx}/common/js/jquery/jquery.format.js"></script>
		<script src="${ctx}/common/js/easyui/1.4/jquery.easyui.min.js"></script>
		<script src="${ctx}/common/js/tools/dataformat.js"></script>
		<script src="${ctx}/common/js/tools/syUtil.js"></script>
		<!-- jsp页面上传 -->
		<script src="${ctx}/common/js/jquery/jquery.form.js"></script>
		
		
		<!-- zTree控件 -->
		<link rel="stylesheet" href="${ctx}/common/js/zTree/zTreeStyle.css"/>
		<script src="${ctx}/common/js/zTree/jquery.ztree.core-3.5.js"></script>
		<script src="${ctx}/common/js/zTree/jquery.ztree.excheck-3.5.js"></script>
		<script src="${ctx}/common/js/zTree/jquery.ztree.exedit-3.5.js"></script>

		<script src="${ctx}/common/js/lhgDialog/lhgdialog.min.js"></script>
		<script src="${ctx}/common/js/tools/publicutil.js"></script>
		<script src="${ctx}/common/js/Validform/js/Validform_v5.3.2_min.js"></script>
		<script src="${ctx}/common/js/Validform/js/datatype.js"></script>
		<!-- 日期控件 -->
		<script t src="${ctx}/common/js/My97DatePicker/WdatePicker.js"></script>
		
		<!-- 上传控件 -->
		<link rel="stylesheet" href="${ctx}/common/js/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css"></link>
        <script type="text/javascript" src="${ctx}/common/js/plupload/js/plupload.full.js"></script>
        <script type="text/javascript" src="${ctx}/common/js/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
        <script type="text/javascript" src="${ctx}/common/js/plupload/js/i18n/cn.js"></script>
</head>
<body>

</body>
</html>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE HTML>
<script>
function closeUf(){
	var windowapi = frameElement.api, W = windowapi.opener;
		W.$("#templateName").val($('#FileName').val());
	windowapi.close();
	//query();
}
</script>

<body>
  	<form id="uf"  enctype="multipart/form-data"  method="post" >
	    <div id="uploader">
			<p>您的浏览器未安装 Flash, Silverlight, Gears, BrowserPlus 或者支持 HTML5 .</p>
		</div>
		<div style="" align="right">
		<input type="button"  value ="关闭"  onclick="closeUf()"/>
		<input id="FileName" value="" type="hidden"/>
		</div>
	</form>
	<script type="text/javascript">
		$(function(){
			$("#uploader").pluploadQueue({
				// General settings
				runtimes : 'gears,flash,silverlight,browserplus,html5,html4',
				url : '${ctx}/amsTemplateManageController.do?loadWenFile=key&pk=${pkid}',
				max_file_size : '5mb',
				unique_names : false,
				chunk_size: '5mb',
				filters : [
					{title : "所有文件", extensions : "*"}
				],
				flash_swf_url : '${ctx}/common/js/plupload/js/plupload.flash.swf',
				silverlight_xap_url : '${ctx}/common/js/plupload/js/plupload.silverlight.xap'
				
			});
			var uploader = $('#uploader').pluploadQueue();
			uploader.bind('QueueChanged', function(up) {
			var uploader1 = uploader.files[0].size;
			    if (uploader.files.length > 1) {
			        uploader.splice(1, 1);
			        tip('只能上传一个文件');
			    }
			    //alert(uploader.files[0].size);
			   if (uploader1==0) {
			        window.location.reload();
			        tip("不能上传空文档！");
			    } 
			});
			uploader.bind('FilesAdded', function(up, files) {
		        var fileName = files[0].name;
		        $('#FileName').val(fileName);
				status = 0;//不存在
				
			});
		var uploader = $('#uploader').pluploadQueue();
			uploader.bind('FileUploaded', function(up,files,responseObject) {
				var windowapi = frameElement.api, W = windowapi.opener;
				//responseObject.setCharacterEncoding("UTF-8");
				//alert(responseObject.response);
				W.$('#path').val(responseObject.response);
			});
			
			 uploader.bind("UploadComplete", function(up, files) {
					//alert(files[0].target_name);
					var windowapi = frameElement.api, W = windowapi.opener;
					W.$("#templateName").val(files[0].target_name);
					tip('上传完成');	
			    });
			
		});
	</script>
  </body>

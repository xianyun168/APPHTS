<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<script>
/***
 * 关闭上传后回显文件的上传信息
 */
function closeUf(){
	var windowapi = frameElement.api, W = windowapi.opener;
	var uploader=$("#uploader").pluploadQueue();
	$('#uf').form('submit',{
				url : '${ctx}/fileController.do?upinfo=key&uploaderName='+uploader,
				success : function(data) {
					var d =  $.parseJSON(data);
					d1 = d.msg;
					var value = d1.split(",");
					var showinfo = W.$('#showinfo').html();
					var uploadinfo = W.$('#uploadinfo').val();
					var uploads = uploadinfo.split(",");
					for (i = 0; i < value.length; i++) {
						if (value[i] != null && value[i] != "") {
							var info = value[i].split("-&gt;");  
							showinfo = showinfo
									+ "<div id='showinfo"
									+ (uploads.length + i + 1)
									+ "' style='height:15;display:inline;position: relative;background-image: url(${ctx}/common/images/meun_bg.jpg);background-repeat:repeat' onmouseover='divOver(this);' onmouseout='divOut(this)'>"
									+ info[info.length - 1]
									+ "<a href='#' style='outline:none;right:0px' onclick='delInfo(\""
									+ value[i]
									+ "\",\""
									+ (uploads.length + i + 1)
									+ "\")'><img src='${ctx}/common/images/close2.gif' border='0'/></a><img src='${ctx}/common/images/btbg_green.gif' border='0'/></div>";
						}
					}
					W.$('#showinfo').html(showinfo);
					W.$('#uploadinfo').html(W.$('#uploadinfo').html() + d1);
					
					 windowapi.close();
				}
			});
}

</script>

<body>
  	<form id="uf" method="post">
	    <div id="uploader">
			<p>您的浏览器未安装 Flash, Silverlight, Gears, BrowserPlus 或者支持 HTML5 .</p>
		</div>
		<div style="" align="right">
		<input type="button"  value ="关闭上传"  onclick="closeUf()"/>
		</div>
	</form>
	<script type="text/javascript">
		<%-- 上传控件  --%>
		$(function(){
			$("#uploader").pluploadQueue({
				// General settings
				runtimes : 'gears,flash,silverlight,browserplus,html5,html4',
				url : '${ctx}/fileController.do?loadFile=key',
				max_file_size : '10mb',
				unique_names : true,
				chunk_size: '10mb',
				filters : [
					{title : "所有文件", extensions : "*"}
				],
				flash_swf_url : '${ctx}/common/js/plupload/js/plupload.flash.swf',
				silverlight_xap_url : '${ctx}/common/js/plupload/js/plupload.silverlight.xap'
			});
		});
	</script>
  </body>

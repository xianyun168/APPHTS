<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>${content.newsType}新闻</title>
	<style type="text/css">
	body {
    font-size: 12px;
    letter-spacing: 0;
    }
	.main-title {
    font-weight: 700;
    }
    .main-title {
    font-size: 38px;
    color: #4d4f53;
    letter-spacing: 1px;
    line-height: 54px;
    padding: 40px 0;
    }
    .date-source {
    float: left;
    line-height: 16px;
    padding: 24px 0;
    }
    .date-source .date {
    font-size: 16px;
    color: #888;
    }
    .top-bar {
    border: 1px solid #e5e5e5;
    border-width: 1px 0;
    width: 100%;
    background: #fff;
}
    .top-bar, .top-bar-wrap {
    height: 64px;
}
#content{
	    width: 1000px;
        height: 129px;
        margin: 0 auto;
}
.news_content{
	    width: 800px;
        height: auto;
        margin: 0 auto;
}
.news_content p {
    margin-bottom: 18px;
    font-size: 18px;
    letter-spacing: 1px;
    line-height: 32px;
}
.news_content img {
    border: 0 none;
    vertical-align: top;
    margin-top: 13px;
    margin-left: 150px;
}
}
	</style>
</head>
<body>
	<div id="content">
		<h1 class="main-title">${content.title}</h1>
	<div id="top_bar"class="top-bar ani">
		
    <div class="date-source">
    	<span class="date">发布日期:<fmt:formatDate value="${content.publishDate}" pattern="yyyy-MM-dd HH:mm:ss"/> </span>
    	<span class="source">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;作者:&nbsp;${content.author}</span>
    </div>
    </div>

    <div class="news_content">
   ${content.newsContent}
    </div>
	</div>



</body>
</html>
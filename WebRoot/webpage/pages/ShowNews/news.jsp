<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Document</title>
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
</head>
<style>
html,body,img,ul {
	padding: 0px;
	margin: 0px;
}

#box {
	width: 641px;
	margin: 0px auto;
	height: 253px;
	overflow: hidden;
	position: relative;
	background-color: orange;
}

#box #mylist {
	position: absolute;
	width: 2450px;
	left: 0px;
	top: 0px;
	list-style-type: none;
}

#box ul li {
	float: left;
}

ul#underNum {
	position: absolute;
	right: 10px;
	bottom: 10px;
	background-color: yellow;
	width: 150px;
	height: 30px;
	list-style-type: none;
}

ul#underNum li {
	text-align: center;
	width: 30px;
	height: 30px;
	line-height: 30px;
}

ul#underNum li a {
	text-decoration: none;
}

.banner {
	height: 50px;
	width: 100%;
	background-color: skyblue;
	margin-top: 0;
	padding-top: 0;
}

a {
	text-decoration: none;
}

.w {
	width: 1210px;
	margin: 0 auto;
}

.banner ul {
	height: 50px;
	width: 640px;
	padding: 0;
	margin: 0px auto;
}

.banner ul li {
	float: left;
	padding-left: 44px;
	list-style: none;
	line-height: 50px;
	margin-right: 50px;
}

.newlist {
	margin-top: 20px;
}

.newlist img {
	width: 180px;
	height: 100px;
	float: left;
}

.ula li {
	list-style: none;
}

.ulb li {
	list-style: none;
}

.media-title {
	color: #444;
	font-size: 18px;
	font-weight: 700;
	line-height: 22px;
	position: absolute;
	top: 30px;
	left: 196px;
}

.media {
	display: table;
	position: relative;
	float: left;;
	table-layout: fixed;
	width: 100%;
	z-index: 1;
	border-bottom: 1px solid #BDB8B8;
	padding: 14px 0;
}

.media-body {
	position: relative;
}

img {
	vertical-align: middle;
	border: 0;
}

.media-feed .media-title {
	color: #444;
	font-size: 18px;
	font-weight: 700;
	line-height: 22px;
}

.media-feed .media-obj+.media-body {
	padding-left: 15px;
}

.news {
	margin: 0px auto;
	width: 640px;
	height: 500px;
}

.news_all {
	width: 640px;
	height: 129px;
	margin: 0 auto;
}

.news_all ul {
	padding: 6px 0px;
	font-size: 14px;
}

ul li {
	list-style: none;
}

li img {
	float: left;
}

ul .news_name {
	float: left;
	text-align: center;
	line-height: 100px;
}
</style>

<body>

	<div id="all">
		<div class="banner">
			<div class="w">
				<ul>
					<li><a href="">头条</a></li>
					<li><a href="">军事</a></li>
					<li><a href="">娱乐</a></li>
					<li><a href="">社会</a></li>
				</ul>
			</div>
		</div>
		<div id="box">
			<ul id="mylist">
				<li><a><img src="./img/01.jpeg"></a></li>
				<li><a><img src="./img/02.jpeg"></a></li>
				<li><a><img src="./img/03.jpeg"></a></li>
				<li><a><img src="./img/04.jpeg"></a></li>
			</ul>
			<ul id="underNum">
				<li><a href="">1</a></li>
				<li><a href="">2</a></li>
				<li><a href="">3</a></li>
				<li><a href="">4</a></li>
				<li><a href="">5</a></li>
			</ul>
		</div>
		<div id="newdd">
			<!-- <div class="news_all" style=" border-bottom:1px solid red">

        		<tr  align="center">
        			<td><img src="img/01.jpg"width="180" height="100"></td>
        			<td class="news_name"><a href="#">&nbsp&nbsp&nbsp&nbsp台湾一炼油厂发生爆炸 烈焰冲天</a></td>
        		</tr>
        		
        	</div> -->
		</div>

	</div>
	<script type="text/javascript">  
    /*  $(document).ready(function(){
    	 alert(obj)
    	 
    	 
    	for(var i=0;i<5;i++){
    		 $("#newdd").prepend("<div class=\"news_all\" style=\" border-bottom:1px solid red\"><tr  align=\"center\"><td><img src=\"img/01.jpg\"width=\"180\" height=\"100\"></td><td class=\"news_name\"><a href=\"#\">&nbsp&nbsp&nbsp&nbsp台湾一炼油厂发生爆炸 烈焰冲天</a></td></tr></div>");
    	}
    	 //$("div").remove("#father div");
    	}); */
    	
     	$(function() {
  
     		query();

 		}); 
     	
     	function query() {
     		 $.ajax({
                 type: "post",
                 url: "publishNewsController.do?selectnNews=key",
                 datatype : 'json',
                 success : function(data) {
                	 
         var ss  = data.obj;
                	var aa = $.parseJSON(data);
                	var bb = aa.obj;
              
                    	for(var i=0;i<bb.length;i++){
                    		 $("#newdd").prepend("<div class=\"news_all\" style=\" border-bottom:1px solid red\"><tr  align=\"center\"><td><img src=\"img/01.jpg\"width=\"180\" height=\"100\"></td><td class=\"news_name\"><a href=\"#\" onclick=show('publishNewsController.do?show=key','"+bb[i].pkId+"') >"+ bb[i].title +"</a></td></tr></div>");
                    		 
                    	}
                 }
             });
     	
     	};
     	
     	
            var timer=setInterval("hello()",2000);  
            var mylist=document.getElementById("mylist");  
            var num=document.getElementById("mylist").getElementsByTagName("li");  
            var now=0;  
            var left=0;  
            function hello(){  
                // body...  
                // document.write("ddd"+now);  
                if (left<=-(num.length-1)*490) {  
                    now=0;  
                    left=-now*490;  
                    mylist.style.left=left+"px";  
                    now=now+1;  
                }else{  
                    left=-now*490;  
                    mylist.style.left=left+"px";  
                    now=now+1;  
                };  
                  
            }  
            //移动的位置  
            function toCover(now1){  
                now=now1;  
                left=-now*490;  
                mylist.style.left=left+"px";  
                clearInterval(timer);  
                  
            }  
            var numli=document.getElementById("underNum").getElementsByTagName("li");  
            for (var i = 0; i <=numli.length - 1; i++) {  
                numli[i].index=i;  
                numli[i].onmouseover=function(){  
                    //alert(this.index);  
                    toCover(this.index);      
                };  
                numli[i].onmouseout=function(){  
                    //alert(this.index);  
                    timer=setInterval("hello()",1000);  
                };  
            };  
            
     
          //修改页面
    		function show(url, pkId) {    			
    			url += '&pkId=' + pkId;
    		window.location.href = '${ctx}/publishNewsController.do?show=key'
				+ '&pkId=' + pkId;
    		}
        </script>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="ProfitList" toolbar="#demoListtb"></table>
<div id="demoListtb" style="padding:3px; height: auto">
<input id="cpId" name="cpId" type="hidden" value="${cpId}">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
</div>
<script type="text/javascript">
	function query(){
		$('#ProfitList').datagrid({
			idField : 'remark1',
			title : '确认及付息 : ${name}',
		    url : ' ${ctx}/profitController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				
			},
			columns:[[
						{field : 'ck',checkbox:true},
						{
							field : 'remark1',
							title : '付息日期',
							formatter : function(value, row,
									index) {
								return "<a href='#' style='color:#3f99b1' onclick=info('"+row.remark1+"');>"+ value + "</a>";
							},
							width : fixWidth(0.11),
							align : 'center',
							sortable : true
						}
					    ]],
			onLoadSuccess : function(data) {
				$("#ProfitList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清楚所选的行
				$("#ProfitList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'companyInfoList';
			}
		});
		$("#ProfitList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#ProfitList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
	};

	$(function(){
		//查询
		query();	
		noauto();	
	});
	
	function reset(){
		$('#insRemark').val("");
	}
	
	
	function reloadTable() {
		try {
			$('#' + ProfitList).datagrid('reload');
			$('#' + ProfitList).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + ProfitList).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#ProfitList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	};
	
		//页面初始化时，设置按钮的权限
	function noauto(){
		//取得对应的权限的值(后台返回的是没有的权限的值)
	    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
		var oper = operCode.split(', ');
		var i = oper.length;
		$(oper).each(function(i){
			$('#' + oper[i]).hide();
		})
	}
	//查询付息详情
	function info(remark1) {
		
		var cpId = $("#cpId").val();
			add_win('确认收益及付息','${ctx}/profitController.do?detailList=key&remark1='+remark1+"&cpId="+cpId,'ProfitList',1000,500);
			
		}


</script>
</body>
</html>


		
			

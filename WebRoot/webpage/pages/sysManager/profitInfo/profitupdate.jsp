<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  
  <script type="text/javascript">
$(function(){

	 init();
});
function saveData(url) {
	
	var form = $("#myform").Validform();
	var CF_PK = $("#CF_PK").val();
    url += '&CF_PK=' + CF_PK;
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		// $.dialog.tips('数据保存中...',500,'loading.gif');
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
			'submit',
			{
				url : url,
				onSubmit : function() {
					var form = $("#myform").Validform();
					return form.check();
				},
				success : function(data) {
					var windowapi = frameElement.api, W = windowapi.opener;
					var d;
					try {
						d = $.parseJSON(data);
						if (d.success == true) {
							window.top.$.messager.progress('close');
							windowapi.close();
							W.tip(d.msg);
							W.query();
							//dateList.closeUf();
						}
						/*
						 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
						 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
						 */
						else {
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('提示', d.msg);
							return;
						}
					} catch (ex) {
						var emsg = data.substring(data.indexOf('错误描述'), data
								.indexOf('错误信息'));
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('错误', emsg);
						return;
					}
				}
			});
}

</script>
 </head>
 <body style="overflow-y: auto" scroll="no">
 <input id="CF_PK" name="CF_PK" value="${cPcfResult.CF_PK}" type="hidden"/>

 	<form id="myform" name="myform" method="post" >
                                   
  	<table style=" width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	<%-- 	<tr>
		  <td align="right" width="10%" nowrap>
    		  <label class="Validform_label"><span>折标日期:</span></label>
    	  </td>
    
    	  
    	  <td class="value" width="15%" >
		     <input type="text" id="date" name="date" class="inputxt" style="width: 260px; height: 35px;" value="${date}" readonly="readonly">
    	  </td>
    	  </tr> --%>
    	  <tr>
		  <td align="right" width="10%" nowrap>
    		  <label class="Validform_label"><span>本期返息金额:</span></label>
    	  </td>
    	  <td class="value" width="15%" >
    		  <input class="inputxt"  id="amount" onkeypress="notNull(this);" name="amount" value="${cPcfResult.amount}" datatype="/^\d{1,7}(\.\d{1,2})?$/" 
					nullmsg="请输入返息金额！" errormsg="请输入正确的金额"/><span style="color: red;">*</span>
    	  </td>
    	</tr>
   
    
    	
    	<tr>
     		<td align="center" colspan="8">
        		<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/profitController.do?saveUpdate=key')">保存</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
 
</body> 
<script type="text/javascript">

 </script>
  
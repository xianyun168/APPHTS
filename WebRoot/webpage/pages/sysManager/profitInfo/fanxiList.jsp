<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>产品信息</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
		<style>
			.datagrid .datagrid-pager {
				display: none;
			}
		</style>
	</head>
<body>
<table width="100%" id="fanxiList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
<input id="Data_date" name="Data_date" type="hidden" value="${fanxiDate}">
<input id="productId1" name="productId1" type="hidden" value="${productId1}">	
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<span class="orangebtnhover"><a onclick="export1()" id="export" plain="true" class="formbtn orangebtn">导出</a></span>
			<span class="bluebtnhover"><a onclick="querenList()" id="querenList" plain="true" class="formbtn bluebtn">批量收益确认</a></span>
			<span class="orangebtnhover"><a onclick="fuxiList()" id="fuxiList" plain="true" class="formbtn orangebtn">批量确认付息</a></span>    
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#fanxiList').datagrid({
			idField : 'CF_PK',
			/*  title : '活动信息管理', */
		     url : '${ctx}/profitController.do?datagrids=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10000,
			pagination : true,
			pageList : [ 30, 50, 100,10000 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : false,
			fitColumns : true,
			remoteSort:false,
			remoteSort:true,
			/* <!--查询参数 --> */
			queryParams:{
				'productName': $('#productName').val(),
				'pruductType': $('#pruductType').val(),
				'term': $('#term').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'CF_PK',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
						{field:'c_custnm',title:'客户名称',  width:fixWidth(0.1),align:'center',sortable:false},
						{field:'c_fundnm',title:'产品名称',  width:fixWidth(0.11),align:'center',sortable:false},
						{field:'c_mobile',title:'客户手机号',  width:fixWidth(0.11),align:'center',sortable:false},
						{field:'contract_Id',title:'合同编号',  width:fixWidth(0.1),align:'center',sortable:false},
						{field:'amount',title:'本期收益',  width:fixWidth(0.1),align:'center',sortable:false},
						{field:'Fin_element_ID',title:'现金流类型',
							formatter:function(value){
								if(value=='A101'){
									return "本金";
								}else{
									return "利息";
								}
							},
							width:fixWidth(0.1),align:'center',sortable:false},
						{field:'confirm_Flag',title:'收益确认状态',
							formatter:function(value){
								if(value=='Y'){
									return "已确认";
								}else if(value=='N'){
									return "未确认";
								}
								else if(value=='A'){
									return "已确认";
								}
							},
							width:fixWidth(0.11),align:'center',sortable:false},
						{field:'flag',title:'付息确认状态', 
								formatter:function(value){
									 if(value=='A'){
										return "未确认";
									}else if(value=='N'){
										return "未确认";
									}
									else if(value=='Y'){
										return "已确认";
									} 
								}, 
								width:fixWidth(0.11),align:'center',sortable:false},
						{field : 'opt',title : '操作',width:fixWidth(0.25),align:'center',sortable:false,
							formatter : function(value, rec,index) {
								if (!rec.CF_PK) {
									return '';
								}
								var href = '';
								href += "[<a href='#' onclick=update('"+rec.CF_PK+"','"+rec.confirm_Flag+"') style='color:#3f99b1;'>";
								href += "编辑</a>]";
								href += "[<a href='#' onclick=queren('profitController.do?queren=key','fanxiList','"+rec.CF_PK+"','"+rec.confirm_Flag+"') style='color:#3f99b1;'>";
								href += "收益确认</a>]";
								href += "[<a href='#' onclick=fuxi('profitController.do?fuxi=key','fanxiList','" + rec.CF_PK + "','"+rec.confirm_Flag+"') style='color:#3f99b1;'>";
								href += "确认付息</a>]";
						
								return href;
						}}
				    ]],
			onLoadSuccess : function(data) {
			    /* $("#fanxiList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				} */
				//清除所选的行
				//$("#fanxiList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'fanxiList';
			}
		});
		$("#fanxiList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#fanxiList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});

function reset(){
	$('#productName').val("");
	$('#pruductType').val("");
	$('#term').val("");
}


//批量收益确认
function querenList(field) {
	var ids = [];
	var rows = $('#fanxiList').datagrid('getSelections');
	if (!rows || rows.length == 0) {
		tip('请选择需要确认的信息');
		return;
	}
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i].CF_PK);
		if(rows[i].flag=="Y"){
			tip("收益已确认,不能再次确认，请重新选择");
			return;
		}
		/*if(rows[i].confirm_Flag=="A"){
			tip("收益已确认");
			return;
		}*/
	}
	ids.join(',');
	//window.location.href='${ctx}/profitController.do?querenList=key'+"&ids="+ids;
	$.ajax({
     	type: "post",
     	url: "profitController.do?querenList=key&ids="+ids,
     	dataType: "json",
     	success: function(data){
             if(data.obj=="ok"){
            	 $('#fanxiList').datagrid('reload');    // reload the user data
                 tip(data.msg);
             }else{
                tip(data.msg);
                return;
             }
         }
 	});
};



//收益确认
function queren(url,id,CF_PK,confirm_Flag) {
	if(confirm_Flag=="Y"){
		tip("收益已确认");
		return;
	}
	if(confirm_Flag=="A"){
		tip("收益已确认");
		return;
	}
url += '&CF_PK='+CF_PK;
window.top.$.messager.confirm('Confirm','是否进行收益确认',function(r){
    if (r){
        $.post(url,function(result){
            if (result.success){
            	if(result.obj =='alert'){
            		tip(result.msg);
            	}else{
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
            	}
            } else {
                window.top.$.messager.show({// show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
        },'json');
    }
});
}

//批量确认付息
function fuxiList(field) {
	var ids = [];
	var rows = $('#fanxiList').datagrid('getSelections');
	if (!rows || rows.length == 0) {
		tip('请选择需要确认的信息');
		return;
	}
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i].CF_PK);
		if(rows[i].confirm_Flag=="N"){
			tip("请先进行收益确认");
			return;
		}
		if(rows[i].confirm_Flag=="Y"){
			tip("已确认付息");
			return;
		}
	}
	ids.join(',');
	//window.location.href='${ctx}/profitController.do?fuxiList=key'+"&ids="+ids;
	$.ajax({
     	type: "post",
     	url: "profitController.do?fuxiList=key&ids="+ids,
     	dataType: "json",
     	success: function(data){
             if(data.obj=="ok"){
            	 $('#fanxiList').datagrid('reload');    // reload the user data
                 tip(data.msg);
             }else{
                tip(data.msg);
                return;
             }
         }
 	});
};


//付息确认
function fuxi(url,id,CF_PK,confirm_Flag) {
	if(confirm_Flag=="N"){
		tip("请先进行收益确认");
		return;
	}
	if(confirm_Flag=="Y"){
		tip("已确认付息");
		return;
	}
url += '&CF_PK='+CF_PK;
window.top.$.messager.confirm('Confirm','是否进行付息确认',function(r){
    if (r){
        $.post(url,function(result){
            if (result.success){
            	if(result.obj =='alert'){
            		tip(result.msg);
            	}else{
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
            	}
            } else {
                window.top.$.messager.show({// show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
        },'json');
    }
});
}
function reloadTable() {
	try {
		$('#' + fanxiList).datagrid('reload');
		$('#' + fanxiList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + fanxiList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

/* function getUserListSelections(field) {
	var ids = [];
	var rows = $('#fanxiList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids;
}; */

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}
//修改
function update(CF_PK,confirm_Flag) {
if(confirm_Flag=='A'){
	tip("收益已确认，不能修改");
	return;
}
if(confirm_Flag=='Y'){
	tip("收益已确认，不能修改");
	return;
}
		up('收益保存','${ctx}/profitController.do?add=key&CF_PK='+CF_PK,'ProfitList',500,200);
		
	}
function up(title, addurl, gname, width, height) {
	gridname = gname;
	width = width ? width : 700;
	height = height ? height : 400;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		zIndex: 1997,
		content : 'url:' + addurl,
		lock : true,
		width : width,
		height : height,
		title : title,
		opacity : 0.3,
		cache : false
	});
}

//导出
function export1(){
	var Data_date = $("#Data_date").val();
	window.location.href = '${ctx}/profitController.do?export=key&Data_date='+Data_date;
}
</script>
	</body>
</html>


		
			

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>ESBMM</title>
</head>
<body style="overflow-y: hidden" scroll="no">

	<div class="easyui-layout" fit="true">
		<div region="center" style="padding:1px;">
			<div class="easyui-panel" style="padding:1px;" fit="true"
				border="false" id="functionListPanel">
				<input type="hidden" name="roleId" value="${roleId}" id="rid">
				<a id="selecrAllBtn" onclick="selecrAll();">全选</a> <a id="resetBtn"
					onclick="reset();">重置</a>
				<ul id="departId"></ul>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$('#departId')
					.tree(
							{
								checkbox : true,
								url : 'roleController.do?setDepartAuthority=key&roleId=${roleId}',
								onLoadSuccess : function(node) {
									expandAll();
								}
							});
			$("#functionListPanel").panel({
				title : "组织机构列表",
				tools : [ {
					iconCls : 'icon-save',
					handler : function() {
						mysubmit();
					}
				} ]
			});
		});
		function mysubmit() {
			var roleId = $("#rid").val();
			var s = GetNode();
			doSubmit("roleController.do?updateDepartAuthority=key&rolefunctions=" + s
					+ "&roleId=" + roleId);
		}
		function GetNode() {
			var node = $('#departId').tree('getChecked');
			var cnodes = '';
			var pnodes = '';
			var pnode = null; //保存上一步所选父节点
			for ( var i = 0; i < node.length; i++) {
				if ($('#departId').tree('isLeaf', node[i].target)) {
					cnodes += node[i].id + ',';
					pnode = $('#departId').tree('getParent', node[i].target); //获取当前节点的父节点
					while (pnode != null) {//添加全部父节点
						pnodes += pnode.id + ',';
						pnode = $('#departId')
								.tree('getParent', pnode.target);
					}
				}
			}
			cnodes = cnodes.substring(0, cnodes.length - 1);
			pnodes = pnodes.substring(0, pnodes.length - 1);
			return cnodes + "," + pnodes;
		};

		function expandAll() {
			var node = $('#departId').tree('getSelected');
			if (node) {
				$('#departId').tree('expandAll', node.target);
			} else {
				$('#departId').tree('expandAll');
			}
		}
		function selecrAll() {
			var node = $('#departId').tree('getRoots');
			for ( var i = 0; i < node.length; i++) {
				var childrenNode = $('#departId').tree('getChildren',
						node[i].target);
				for ( var j = 0; j < childrenNode.length; j++) {
					$('#departId').tree("check", childrenNode[j].target);
				}
			}
		}
		function reset() {
			$('#departId').tree('reload');
		}
		
		/**
		 * 执行操作
		 * @param url
		 * @param index
		 */
		function doSubmit(url,name) {
			gridname=name;
			$.ajax({
				async : false,
				cache : false,
				type : 'POST',
				url : url,// 请求的action路径
				error : function() {// 请求失败处理函数
				},
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						var msg = d.msg;
						tip(msg);
						reloadTable();
					}
				}
			});
		}

		$('#selecrAllBtn').linkbutton({});
		$('#resetBtn').linkbutton({});
	</script>
</body>
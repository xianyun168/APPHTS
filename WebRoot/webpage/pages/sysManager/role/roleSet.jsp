<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>ESBMM</title>

</head>
<body style="overflow-y: hidden" scroll="no">

	<div class="easyui-layout" fit="true">
		<div region="center" style="padding:1px;">
			<div class="easyui-panel" style="padding:1px;" fit="true"
				border="false" id="functionListPanel">
				<input type="hidden" name="roleId" value="${roleId}" id="rid">
				<a id="selecrAllBtn" onclick="selecrAll();">全选</a> <a id="resetBtn"
					onclick="reset();">重置</a>
				<ul id="menuId"  class="ztree"></ul>
			</div>
		</div>
		<div region="east" style="width:150px; overflow: hidden;" split="true">
			<div class="easyui-panel" style="padding:1px;" fit="true"
				border="false" id="operationListpanel"></div>
		</div>	
		
	</div>
	<script type="text/javascript">
	var setting = {
					check: {
						enable: true,
						chkboxType:{ "Y": "ps", "N": "s" }
					},
					data: {
						simpleData: {
							enable: true
						}
					},
					callback: {
					onClick:  zTreeOnClick
					}
				};
	var zTree;
		$(function() {
			$.ajax({
				async : false,
				cache:false,
				type: 'POST',
				dataType : "json",
				url: "roleController.do?setAuthority=key&roleId=${roleId}",//请求的action路径
				error: function () {//请求失败处理函数
				alert('请求失败');
				},
				success:function(data){ //请求成功后处理函数。
					zTree=$.fn.zTree.init($("#menuId"), setting, data);
				}
				});
			$("#functionListPanel").panel({
				title : "菜单列表",
				tools : [ {
					iconCls : 'icon-save',
					handler : function() {
						mysubmit();
					}
				} ]
			});
			$("#operationListpanel").panel({
				title : "操作按钮列表",
				tools : [ {
					iconCls : 'icon-save',
					handler : function() {
						submitOperation();
					}
				} ]
			});
		});
		
		function zTreeOnClick(event, treeId, treeNode) {
			var roleId = $("#rid").val();
			$("#operationListpanel").panel(
										"refresh",
										"operationController.do?operationListForFunction=key&menuId="
										+ treeNode.id + "&roleId="
										+ roleId);
		};
		
		function mysubmit() {
			var roleId = $("#rid").val();
			var s = onCheck();
			doSubmit("roleController.do?updateAuthority=key&rolefunctions=" + s
					+ "&roleId=" + roleId);
		}
		function onCheck(){
			var disabled=zTree.getNodesByParam("chkDisabled",true);
            var node=zTree.getCheckedNodes(true);
			var cnodes = '';
			var pnodes = '';
			var pnode = null;
            for(var i=0;i<node.length;i++){
	            if (node[i].getParentNode()!=null) {
	            	cnodes += node[i].id + ',';
	            	pnode =node[i].getParentNode();
	            	pnodes += pnode.id + ',';
					pnode = node[i].getParentNode();
	            }
            }
             for(var i=0;i<disabled.length;i++){
	            if (disabled[i].getParentNode()!=null) {
	            	cnodes += disabled[i].id + ',';
	            	pnode =disabled[i].getParentNode();
	            	pnodes += pnode.id + ',';
					pnode = disabled[i].getParentNode();
	            }
            }
            cnodes = cnodes.substring(0, cnodes.length - 1);
			pnodes = pnodes.substring(0, pnodes.length - 1);
            return cnodes + "," + pnodes;
        }
		function GetNode() {
			alert("node");
			var node = $('#menuId').tree('getChecked');
			var cnodes = '';
			var pnodes = '';
			var pnode = null; //保存上一步所选父节点
			for ( var i = 0; i < node.length; i++) {
				if ($('#menuId').tree('isLeaf', node[i].target)) {
					cnodes += node[i].id + ',';
					pnode = $('#menuId').tree('getParent', node[i].target); //获取当前节点的父节点
					while (pnode != null) {//添加全部父节点
						pnodes += pnode.id + ',';
						pnode = $('#menuId')
								.tree('getParent', pnode.target);
					}
				}
			}
			cnodes = cnodes.substring(0, cnodes.length - 1);
			pnodes = pnodes.substring(0, pnodes.length - 1);
			return cnodes + "," + pnodes;
		};

		function expandAll() {
			alert("expandAll");
			var node = $('#menuId').tree('getSelected');
			if (node) {
				$('#menuId').tree('expandAll', node.target);
			} else {
				$('#menuId').tree('expandAll');
			}
		}
		function selecrAll() {
			//alert("selecrAll");
			var zTree = $.fn.zTree.getZTreeObj("menuId");
			zTree.checkAllNodes(true);
			/* var node = $('#menuId').tree('getRoots');
			for ( var i = 0; i < node.length; i++) {
				var childrenNode = $('#menuId').tree('getChildren',
						node[i].target);
				for ( var j = 0; j < childrenNode.length; j++) {
					$('#menuId').tree("check", childrenNode[j].target);
				}
			} */
		}
		function reset() {
			//$('#menuId').tree('reload');
			var zTree = $.fn.zTree.getZTreeObj("menuId");
			zTree.checkAllNodes(false);
		}
		
		/**
		 * 执行操作
		 * @param url
		 * @param index
		 */
		function doSubmit(url,name) {
			gridname=name;
			$.ajax({
				async : false,
				cache : false,
				type : 'POST',
				url : url,// 请求的action路径
				error : function() {// 请求失败处理函数
				},
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						var msg = d.msg;
						tip(msg);
						reloadTable();
					}
				}
			});
		}

		$('#selecrAllBtn').linkbutton({});
		$('#resetBtn').linkbutton({});
	</script>
</body>
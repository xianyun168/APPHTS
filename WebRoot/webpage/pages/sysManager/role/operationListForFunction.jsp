<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>ESBMM</title>
</head>
<body style="overflow-y: hidden" scroll="no">
<c:forEach items="${operationList}" var="operation" >
	 
	 <c:if test="${fn:contains(operationcodes, operation.operationCode)}"> 
	 <input style="width:20px;" type="checkbox" name="operationCheckbox" value="${operation.operationCode}" checked="checked"/>${operation.operationName}
	 </c:if>
	 <c:if test="${!fn:contains(operationcodes, operation.operationCode)}"> 
	 <input style="width:20px;" type="checkbox" name="operationCheckbox" value="${operation.operationCode}" />${operation.operationName}
	 </c:if>
	<br>
</c:forEach>
<script type="text/javascript">
function submitOperation() {
	var menuId = "${menuId}";
	var roleId = $("#rid").val();
	var operationcodes = "";
	$("input[name='operationCheckbox']").each(function(i){
		   if(this.checked){
			   operationcodes+=this.value+",";
		   }
	 });
	operationcodes=escape(operationcodes); 
	doSubmit("roleController.do?updateOperation=key&menuId=" + menuId + "&roleId=" + roleId+"&operationcodes="+operationcodes);
}


		/**
		 * 执行操作
		 * @param url
		 * @param index
		 */
		function doSubmit(url,name) {
			gridname=name;
			$.ajax({
				async : false,
				cache : false,
				type : 'POST',
				url : url,// 请求的action路径
				error : function() {// 请求失败处理函数
				},
				success : function(data) {
					var d = $.parseJSON(data);
					if (d.success) {
						var msg = d.msg;
						tip(msg);
						reloadTable();
					}
				}
			});
		}
</script>
</body>
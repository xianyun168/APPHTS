<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
<title>ESBMM</title>
<style>
.panel-header{
	background:#d2e1fe;
}
.panel-body{
	background:#f3f3f3;
}
</style>
</head>
<body>
	<div class="easyui-layout" fit="true" id="system_role_roleList">
	 <div region="center" style="padding: 1px;">
		<table width="100%" id="roleList" toolbar="#roleListtb"></table>
		<div id="roleListtb">
			<!-- 用于权限判断 start -->
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
		<div style="padding:20px 20px 0px 20px;">
			<div class="leftbtntool" style="width:400px;margin:auto;">			
				<span class="bluebtnhover"><a onclick="add_win('角色录入','${ctx}/roleController.do?add=key','roleList',430,200)" id="add" plain="true" class="formbtn bluebtn">角色录入</a></span>
				<span class="bluebtnhover"><a onclick="update('角色编辑','${ctx}/roleController.do?modify=key','roleList',430,200)" id="modify" plain="true" class="formbtn bluebtn">角色编辑</a></span>
				<span class="orangebtnhover"><a onclick="setfunbyrole();" plain="true" class="formbtn orangebtn">菜单权限设置</a></span>
				<%-- <span class="bluebtnhover"><a onclick="setdepartbyrole();" plain="true" class="formbtn bluebtn">组织机构权限设置</a></span>--%>
			</div>
		</div>
		
			<%-- <div name="searchColums"></div>
			<div style="height:30px;" class="datagrid-toolbar">
				<span style="float:left;"> 
				<a href="#"	class="easyui-linkbutton" plain="true" icon="icon-add"
					onclick="add_win('角色录入','${ctx}/roleController.do?add=key','roleList',700,100)"
					id="add">角色录入</a>
				<a href="#" class="easyui-linkbutton" plain="true" icon="icon-edit"
					onclick="update('角色编辑','${ctx}/roleController.do?modify=key','roleList',700,100)"
					id="update">角色编辑</a>
				<a href="#" class="easyui-linkbutton" plain="true" icon="icon-edit"
					onclick="setfunbyrole();">菜单权限设置</a>
				<a href="#" class="easyui-linkbutton" plain="true" icon="icon-edit"
					onclick="setdepartbyrole();">组织机构权限设置</a>
				</span> 
			</div> --%>
		</div>
		</div>
		<div data-options="region:'east',
		title:'菜单权限',
		collapsed:true,
		split:true,
		border:false,
		onExpand : function(){
			li_east = 1;
		},
		onCollapse : function() {
	    	li_east = 0;
		}" 
		 style="width: 400px; overflow: hidden;">
		<div tools="#tt" class="easyui-panel" title="权限设置" style="padding: 10px;" fit="true" border="false" id="function-panel">
       </div>
		</div>
		<div id="tt"></div>
	</div>
	<script type="text/javascript">
		function query() {
			$('#roleList').datagrid(
							{
								idField : 'pkId',
								url : 'roleController.do?datagrid=key',
								fit : true,
								title:'角色管理',
								loadMsg : '数据加载中...',
								pageSize : 10,
								pagination : true,
								pageList : [ 10, 20, 30 ],
								//sortOrder : 'asc',
								rownumbers : true,
								singleSelect : true,
								fitColumns : true,
								showFooter : true,
								frozenColumns : [ [] ],
								columns : [ [
									{field : 'ck',checkbox:true},
										{
											field : 'pkId',
											title : '编号',
											hidden : true,
											sortable : false,
										},
										{
											field : 'roleName',
											title : '角色名称',
											width : 50,
											align:'center',
											sortable : false,
										},
										{
											field : 'opt',
											title : '操作',
											align:'center',
											width : 30,
											formatter : function(value, rec,
													index) {
												if (!rec.pkId) {
													return '';
												}
												var href = '';
												href += "[<a href='#' onclick=delr('roleController.do?delete=key','roleList','"+rec.pkId+"') style='color:#3f99b1;'>";
												href += "删除</a>]";
												
												var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
												var oper = operCode.split(', ');
												var i = oper.length;
												if(oper !=""){
												  for(var j=0;j<oper.length;j++){
													 if(oper[j] == "delete"){
												 href=href.replace("[<a href='#' onclick=delr('roleController.do?delete=key','roleList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
															} 
												
													}
												}
												return href;
											}
										} ] ],
								onLoadSuccess : function(data) {
									$("#roleList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
									$("#roleList").datagrid("clearSelections");
								},
								onClickRow : function(rowIndex, rowData) {
									rowid = rowData.pkId;
									gridname = 'roleList';
								}
							});
			$('#roleList').datagrid('getPager').pagination({
				beforePageText : '',
				afterPageText : '/{pages}',
				displayMsg : '{from}-{to}共{total}条',
				showPageList : true,
				showRefresh : true
			});
			$('#roleList').datagrid('getPager').pagination({
				onBeforeRefresh : function(pageNumber, pageSize) {
					$(this).pagination('loading');
					$(this).pagination('loaded');
				}
			});
		}

		$(function() {
			var li_east = 0;
			query();
		});
		function reloadTable() {
			try {
				$('#' + gridname).datagrid('reload');
				$('#' + gridname).treegrid('reload');
			} catch (ex) {
			}
		}
		function reloadroleList() {
			$('#roleList').datagrid('reload');
		}
		function getroleListSelected(field) {
			return getSelected(field);
		}
		function getSelected(field) {
			var row = $('#' + gridname).datagrid('getSelected');
			if (row != null) {
				value = row[field];
			} else {
				value = '';
			}
			return value;
		}
		function getroleListSelections(field) {
			var ids = [];
			var rows = $('#roleList').datagrid('getSelections');
			for ( var i = 0; i < rows.length; i++) {
				ids.push(rows[i][field]);
			}
			ids.join(',');
			return ids
		};
		function roleListsearch() {
			var queryParams = $('#roleList').datagrid('options').queryParams;
			$('#roleListtb').find('*').each(function() {
				queryParams[$(this).attr('name')] = $(this).val();
			});
			$('#roleList').datagrid({
				url : 'roleController.do?roleGrid=key&field=id,roleCode,roleName,',
				pageNumber : 1
			});
		}
		function dosearch(params) {
			var jsonparams = $.parseJSON(params);
			$('#roleList').datagrid({
				url : 'roleController.do?dataGrid=key',
				queryParams : jsonparams
			});
		}
		function roleListsearchbox(value, name) {
			var queryParams = $('#roleList').datagrid('options').queryParams;
			queryParams[name] = value;
			queryParams.searchfield = name;
			$('#roleList').datagrid('reload');
		}
		$('#roleListsearchbox').searchbox({
			searcher : function(value, name) {
				roleListsearchbox(value, name);
			},
			menu : '#roleListmm',
			prompt : '请输入查询关键字'
		});
		function searchReset(name) {
			$("#" + name + "tb").find(":input").val("");
			roleListsearch();
		}
			//菜单权限
		function setfunbyrole() {
			var row = $('#' + 'roleList').treegrid('getSelected');
			if (row != null) {
				var id = row["pkId"];
				var roleName = row["roleName"];
			} else {
				tip("请选择一条记录");
				return;
			}
			if(li_east == 0){
	   			$('#system_role_roleList').layout('expand','east'); 
			}
			$("#function-panel").panel({
				title :  "当前权限:" + roleName,
				href : "roleController.do?fun=key&roleId=" + id
			});
			
			//$('#function-panel').panel("refresh");
		}
		//组织机构权限
		function setdepartbyrole(){
			var row = $('#' + 'roleList').treegrid('getSelected');
			if (row != null) {
				var id = row["pkId"];
				var roleName = row["roleName"];
			} else {
				tip("请选择一条记录");
				return;
			}
			$("#function-panel").panel({
				title :  "当前权限:" + roleName,
				href : "roleController.do?funDepart=key&roleId=" + id
			});
		}
		function delr(url,id,pkId) {
			/* var rowsData = $('#'+id).datagrid('getSelections');
			if (!rowsData || rowsData.length==0) {
				tip('请选择删除项目');
				return;
			}
			if (rowsData.length>1) {
				tip('请选择一条记录再删除');
				return;
			}
			url += '&pkId='+rowsData[0].pkId; */
			url += '&pkId='+pkId;

			window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
		        if (r){
		            $.post(url,function(result){
		                if (result.success){
		                	if(result.obj =='alert'){
		                		tip(result.msg);
		                	}else{
		                        $('#'+id).datagrid('reload');    // reload the user data
		                       // "$('#'+id).treegrid('reload');	 // reload the user data
		                	}
		                } else {
		                    window.top.$.messager.show({// show error message
		                        title: 'Error',
		                        msg: result.errorMsg
		                    });
		                }
		            },'json');
		        }
		    });
		}
		//删除角色
		function delRole(id) {
			//alert("ss");
			var tabName = 'roleList';
			var url = 'roleController.do?delRole=key&id=' + id;
			window.top.$.messager.confirm('确定删除该记录吗', function() {
				doSubmit(url, tabName);
				rowid = '';
				$("#function-panel").html("");//删除角色后，清空对应的权限
			}, function() {
			});

		}
		$(function() {
			//查询
			query();

			noauto();


		});
		//页面初始化时，设置按钮的权限
		function noauto() {
			//取得对应的权限的值(后台返回的是没有的权限的值)
			var operCode = $("#noauto_operationCodes").val().replace("[", "")
					.replace("]", "");
			var oper = operCode.split(', ');
			var i = oper.length;
			$(oper).each(function(i) {
				$('#' + oper[i]).hide();
			})
		}
	</script>
	<script type="text/javascript">
		$(function(){
			$(".pagination .pagination-num").attr("readonly","readonly");
		})
	</script>
<script type="text/javascript" src="${ctx}/webpage/pages/sysManager/js/sysmanager.js"></script>
</body>
</html>
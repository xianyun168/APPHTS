<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});

 </script>
 </head>
 <body style="overflow-y: hidden" scroll="no" >
  	<form id="myform" name="myform" method="post" >
  	  <c:if test="${roleInfo.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${roleInfo.pkId}">
  	  </c:if>
   	  <table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
    	  <td align="right" width="30%" nowrap>
    	    <label class="Validform_label"><span>角色名称:</span></label>
    	  </td>
    	  <td class="value" width="70%" >
    	      <input id="roleName" onkeypress="notNull(this);" class="inputxt"
						name="roleName" value="${roleInfo.roleName}"
						datatype="s1-16" errormsg="请输入1-16位有效字符" nullmsg="请输入1-16位角色名称！"><span style="color:red">*</span> 
    	  </td>
    	</tr>
		<tr>
    	  <td align="right" width="30%" nowrap>
    	  <label class="Validform_label"><span>描&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;述:</span></label>
    	  </td>
    	  <td class="value" width="70%" >    
    	      <input onkeypress="notNull(this);" id="description"
						name="description" value="${roleInfo.description}" class="inputxt"
						datatype="s1-40" errormsg="请输入40位以下有效字符" nullmsg="请输入40位以下描述！"><span style="color:red">*</span> 
    	  </td>
    	</tr>
    	<tr>
		 <td align="center" colspan="2">
		 	<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData(' ${ctx}/roleController.do?saveUpdate=key')" >保存</a></span>
			<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
		 </td>
		</tr>
      </table>
    </form>
</body> 
  
  
  
  

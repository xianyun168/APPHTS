<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<script type="text/javascript" src="${ctx}/webpage/pages/sysManager/js/sysmanager.js"></script>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
		<style>
			.datagrid-btable tr,
			.datagrid-btable td,.datagrid-cell, .datagrid-cell-group, .datagrid-header-rownumber, .datagrid-cell-rownumber{
				height:41px !important;
				line-height:41px !important;
			}
			.datagrid-btable tr,
			.datagrid-btable td,.treegrid-tr-tree{
				height:0px !important;
			}
		</style>
	</head>
<body>
<script type="text/javascript">
	$(function(){
		$(".pagination .pagination-num").attr("readonly","readonly");
	  query();
	});
	function query(){
		$('#menuList').treegrid(
						{
							idField : 'pkId',
							treeField : 'menuName',
							title : '菜单管理',
							url : 'menuController.do?menuTree=key&pkId =${pkId} ',
							fit : true,
							loadMsg : '数据加载中...',
							pageSize : 10,
							pagination : false,
							pageList : [ 10, 20, 30 ],
							//sortOrder : 'asc',
							rownumbers : true,
							singleSelect : true,
							fitColumns : true,
							showFooter : true,
							striped:true,
							frozenColumns : [ [] ],
							columns : [ [
								{field : 'ck',checkbox:true},
									{
										field : 'pkId',
										title : '编号',
										sortable : false,
										hidden : true,
										width : fixWidth(0.1),
										//align : 'center',
										align : 'left',
										
									},
									{
										field : 'menuName',
										title : '菜单名称',
										sortable : false,
										width : fixWidth(0.18),
										align : 'left',
									},
									{
										field : 'menuPath',
										title : '菜单地址',
										sortable : false,
										width : fixWidth(0.28),
										align : 'left',
									},
									{
										field : 'null',
										title : '操作',
										sortable : false,
										width : fixWidth(0.18),
										align : 'left',
										formatter : function(value, rec, index) {
											if (!rec.pkId) {
												return '';
											}
											var href = '';
											href += "[<a href='#' onclick=dele('menuController.do?delete=key','menuList','"+rec.pkId+"')>";
											href += "删除</a>]";
											
											if(rec.state == 'open'){
											
												href += "[<a href='#' onclick=operationDetail('"
														+ rec.pkId
														+ "','"
														+ index
														+ "')>";
												href += "按钮设置</a>]";
											}
											var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
											var oper = operCode.split(', ');
											var i = oper.length;
											if(oper !=""){
											  for(var j=0;j<oper.length;j++){
												 if(oper[j] == "delete"){
														 href=href.replace("[<a href='#' onclick=dele('menuController.do?delete=key','menuList','"+rec.pkId+"')>删除</a>]","");
														} 
											
												}
											}
											
											return href;
										}
									} ] ],
							onLoadSuccess : function(data) {
								$("#menuList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
								$("#menuList").treegrid("clearSelections");
							},
							onClickRow : function(rowData) {
								rowid = rowData.pkId;
								gridname = 'menuList';
							}
						});
		$('#menuList').treegrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		$('#menuList').treegrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
	}
	function reloadTable() {
		try {
			$('#' + gridname).datagrid('reload');
			$('#' + gridname).treegrid('reload');
		} catch (ex) {
		}
	}
	function reloadmenuList() {
		$('#menuList').treegrid('reload');
	}
	function getmenuListSelected(field) {
		return getSelected(field);
	}
	function getSelected(field) {
		var row = $('#' + gridname).treegrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	function getmenuListSelections(field) {
		var ids = [];
		var rows = $('#menuList').treegrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids;
	}

$(function() {
	var li_east = 0;
});
function operationDetail(menuId)
{
	if(li_east == 0){
	   $('#system_menu_menuList').layout('expand','east'); 
	}
	$("#operationDetailpanel").panel({
				//title :  "当前权限:" + roleName,
				href : "operationController.do?operationList=key&menuId=" +menuId
			});
//	$('#operationDetailpanel').panel("refresh", "operationController.do?operationList=key&menuId=" +menuId);
}
function addFun(title,url, id) {
	var rowData = $('#'+id).datagrid('getSelected');
	if (rowData) {
		url += '&pkId='+rowData.pkId;
	}
	add_win(title,url,'menuList',450,360);
}
function dele(url,id,pkId) {
	var rowsData = $('#'+id).datagrid('getSelections');
	
	url += '&pkId='+pkId;
	window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
        if (r){
            $.post(url,function(result){
                if (result.success){
                	if(result.obj =='alert'){
                		tip(result.msg);
                	}else{
                        $('#'+id).datagrid('reload');    // reload the user data
                        $('#'+id).treegrid('reload');	 // reload the user data
                	}
                } else {
                    window.top.$.messager.show({// show error message
                        title: 'Error',
                        msg: result.errorMsg
                    });
                }
            },'json');
        }
    });
}
$(function() {
	noauto();
});
//页面初始化时，设置按钮的权限
function noauto() {
	//取得对应的权限的值(后台返回的是没有的权限的值)
	var operCode = $("#noauto_operationCodes").val().replace("[", "")
			.replace("]", "");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i) {
		$('#' + oper[i]).hide();
	})
}
/*$(function(){
	//加载的添加框
				$("#add").click(function(){
			
				 $("#type1").text("");
			    $("#name1").text("");
			    $("#values").text("");
					$("#paramtype2").val("");$("#paramname2").val("");
			        $("#paramvalue").val("");$("#remark").val("");
					$('#myDialog').dialog({ 
				    title: '新增',
				    width: 500, 
				    height: 400, 
				    left:(window.parent.document.body.offsetWidth/2-515),
				    top:(window.parent.document.body.offsetHeight/2-200),
				    bgiframe: true,
				    draggable:false,    
				    closed: false,
				    cache: false,
				     modal: true 
				       
					});
				});		
	})*/
</script>

<div id="system_menu_menuList" class="easyui-layout" fit="true">
	<div region="center" style="padding:1px;">
		<table width="100%" id="menuList" toolbar="#menuListtb"></table>
		<div id="menuListtb">
			<!-- 用于权限判断 start -->
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
			<div class="leftbtntool" style="padding-top:20px; width:270px;margin:auto;">			
				<span class="bluebtnhover"><a onclick="addFun('菜单增加',' ${ctx}/menuController.do?add=key','menuList')" id="add" plain="true" class="formbtn bluebtn">菜单增加</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" class="formbtn bluebtn" plain="true" onclick="update('菜单编辑',' ${ctx}/menuController.do?modify=key','menuList',450,360)" id="modify">菜单编辑</a></span>
			</div>
		</div>
	</div>

	<div data-options="region:'east',
		title:'操作按钮',
		collapsed:true,
		split:true,
		border:false,
		onExpand : function(){
			li_east = 1;
		},
		onCollapse : function() {
		    li_east = 0;
		}" 
	style="width:400px; overflow: hidden;">
		<div class="easyui-panel" style="padding:1px;" fit="true" border="false" id="operationDetailpanel">
	
	 	</div>
	</div>
</div>

</body>
</html>
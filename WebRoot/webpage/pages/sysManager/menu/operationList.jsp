<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
	</head>
<body>
<script type="text/javascript">
	$(function() {
		query();
	});
	function query() {
		$('#operationList')  
				.datagrid(
						{
							idField : 'pkId',
							url : 'operationController.do?datagrid=key&menuPkId=${menuId}',
							fit : true,
							loadMsg : '数据加载中...',
							pageSize : 10,
							pagination : true,
							pageList : [ 10, 20, 30 ],
							sortOrder : 'asc',
							rownumbers : true,
							singleSelect : true,
							fitColumns : true,
							showFooter : true,
							frozenColumns : [ [] ],
							columns : [ [
                                    {field : 'ck',checkbox:true},
									{
										field : 'pkId',
										title : '编号',
										hidden : true,
										sortable : true
									},
									{
										field : 'operationName',
										title : '操作名称',
										width : 100,
										sortable : true
									},
									{
										field : 'operationCode',
										title : '代码',
										sortable : true
									},
									{
										field : 'menuName',
										title : '权限名称',
										hidden : true
									},
									
									{
										field : 'null',
										title : '操作',
										width : 100,
										formatter : function(value, rec, index) {
											if (!rec.pkId) {
												return '';
											}
											var href = '';
											href += "[<a href='#' onclick=removeId('operationController.do?delete=key','operationList')>";
											href += "删除</a>]";
											return href;
										}
									} ] ],
							onLoadSuccess : function(data) {
								$("#operationList").datagrid("clearSelections");
							},
							onClickRow : function(rowIndex, rowData) {
								rowid = rowData.id;
								gridname = 'operationList';
							}
						});
		$('#operationList').datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		$('#operationList').datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
	}
	function reloadTable() {
		try {
			$('#' + gridname).datagrid('reload');
			$('#' + gridname).treegrid('reload');
		} catch (ex) {
		}
	}
	function reloadoperationList() {
		$('#operationList').datagrid('reload');
	}
	function getoperationListSelected(field) {
		return getSelected(field);
	}
	function getSelected(field) {
		var row = $('#' + gridname).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	function getoperationListSelections(field) {
		var ids = [];
		var rows = $('#operationList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids;
	};

</script>

<table width="100%" id="operationList" toolbar="#operationListtb"></table>
<div id="operationListtb" style="padding:3px; height: auto">
	<div style="height:30px;" class="datagrid-toolbar">
		<span style="float:left;">
			<a href="#" class="easyui-linkbutton" plain="true" icon="icon-add" onclick="add_win('操作录入',' ${ctx}/operationController.do?add=key&menuPkId=${menuId}','operationList',700,120)">操作录入</a>
			<a href="#" class="easyui-linkbutton" plain="true" icon="icon-edit" onclick="update('操作编辑',' ${ctx}/operationController.do?modify=key','operationList',700,120)" id="update" >操作编辑</a>
		</span>
	</div>
	</body>
</html>
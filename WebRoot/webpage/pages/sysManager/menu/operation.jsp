<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <script type="text/javascript">
$(function(){
	  init();
});

 </script>
 </head>
  <body style="overflow-y: hidden" scroll="no">
  
  <form id="myform" name="myform" method="post" >
  <input id="menuPkId" name="menuPkId" type="hidden" value=${operation.menuPkId}>
  <c:if test="${operation.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value=${operation.pkId}>
   </c:if>
  <table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
    		 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 操作代码:
    		 </label>
    		</td>
    		 <td class="value" width="35%" >
    			<input class="inputxt" id="operationCode" name="operationCode" datatype="s1-20" value='${operation.operationCode}' onkeypress="notNull(this);" errormsg="请输入1-20位合法字符" nullmsg="请输入1-10操作代码！"/>
    		 <span style="color:red">*</span>
    		 </td>
    		 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 操作名称:
    		 </label>
    		</td>
    		 <td class="value" width="35%" >
    			<input class="inputxt" id="operationName" name="operationName" datatype="s1-10" value='${operation.operationName}' onkeypress="notNull(this);" errormsg="请输入1-10位合法字符" nullmsg="请输入1-10操作名称！"/>
    		 <span style="color:red">*</span>
    		 </td>
    	</tr>
        
     <tr>
     <td align="center" colspan="8" class="buttons">
      	<input id="saveBtn" type="button" value="保存" onclick="saveData('${ctx}/operationController.do?saveUpdate=key')" />
         <input id="restBtn" type="button" value="关闭" onclick="closeWindow()" />
   </td>
    </tr>
          </table>
 </form>
  </body> 
  
  
  
  

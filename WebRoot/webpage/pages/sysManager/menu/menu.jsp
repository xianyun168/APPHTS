<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>ESBMM</title>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
<script type="text/javascript">

	$(function() {
		
		init();
		
		if($('#menuGrade').val()!='0'){
			$('#pfun').show();
		}else{
			$('#pfun').hide();
		}
		
		$('#menuGrade').change(function(){
			if($(this).val()!='0'){
				$('#pfun').show();
				var t = $('#cc').combotree('tree');
				var nodes = t.tree('getRoots');
				if(nodes.length>0){
					$('#cc').combotree('setValue', nodes[0].id);
					$("#parentMenuId").val(nodes[0].id);
					t.tree('collapseAll');
				}
			}else{
				var t = $('#cc').combotree('tree');
				var node = t.tree('getSelected');
				if(node){
					$('#cc').combotree('setValue', null);
				}
				$('#pfun').hide();
			}
		});
	});		

	function save(url){
		$('#myform').form('submit', {
			url : url,
			onSubmit : function() {
				var form=$("#myform").Validform(); 
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try{
					d = $.parseJSON(data);
					if(d.success==true){
					 windowapi.close();
					 if(d.msg!=""){
						 W.tip(d.msg);
					 }else{
						  W.tip('操作成功');	 
					 }
					  W.reloadmenuList();;
				}
				}catch(ex){
					var emsg = data.substring(data.indexOf('错误描述'),
										data.indexOf('错误信息')); 
			        window.top.$.messager.alert('错误',emsg);
					return;
				}
			}
			
		});
		
	}
</script>
</head>
<body style="overflow-y: hidden;" class="adddialogbg" scroll="no" >
	<form id="myform" name="myform" method="post">
		<input name="pkId" type="hidden" value="${menuInfo.pkId}">
		<input name="operateFlag" type="hidden" value="${operateFlag}">
		
		<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
			<tr>
				 <td align="right" width="30%" nowrap>
	    		  <label class="Validform_label">
                         <span>菜单名称:</span></label></td>
                          <td class="value" width="70%" >
                          <input id="menuName" onkeypress="notNull(this);"
						name="menuName" value="${menuInfo.menuName}"
						datatype="s1-42"  style="width:260px;height:35px;"  errormsg="请输入1-42位合法字符！"><span style="color:red">*</span> 
				</td>
	    	</tr>
	    	<tr>
				<td align="right" width="30%" nowrap>
	    		  	<label class="Validform_label">
                         <span >菜单等级:</span></label>
                         </td>
	    		 <td class="value" width="70%" >
                         <select name="menuGrade" id="menuGrade" datatype="*" style="width:260px;height:35px;">
							<option value="0"
								<c:if test="${menuLevel eq '0'}">selected="selected"</c:if>>
								一级菜单</option>
							<option value="1"
								<c:if test="${menuLevel ne '0'}">selected="selected"</c:if>>
								下级菜单</option>
						</select>
				</td>
	    	</tr>
			<tr id="pfun">
				  <td align="right" width="30%" nowrap>
	    		  	<label class="Validform_label">
                         <span class="dialogformnameall">上级菜单: </span></label>
                         </td>
	    		 <td class="value" width="70%" >
                         <input class="easyui-combotree" name="parentMenuId" id="cc" value="${combotree_pkId}" style="width:260px;height:35px;" data-options="url:'menuController.do?getCombotree=key',method:'get'">
				</td>
	    	</tr>
			
	    	<tr>
				<td align="right" width="30%" nowrap>
	    		  	<label class="Validform_label">
                         <span class="dialogformnameall">菜单地址: </span></label>
                                </td>
	    		 <td class="value" width="70%" >
                         <input id="menuPath" name="menuPath" class="inputxt"  onkeypress="notNull(this);" style="width:260px;height:35px;" value="${menuInfo.menuPath}" datatype="RequestUrl" />
				</td>
	    	</tr>
	    	<tr>
				<td align="right" width="30%" nowrap>
	    		  	<label class="Validform_label">
                         <span class="dialogformnameall">菜单顺序:</span></label>
             </td><td class="value" width="70%" >
                         <input id="menuOrder" onkeypress="notNull(this);"
					name="menuOrder" value="${menuInfo.menuOrder}"
					datatype="n1-3" nullmsg="请输入1-3位数字！" style="width:260px;height:35px;"><span style="color:red">*</span>
				</td>
	    	</tr>
	    	<tr>
		     	<td align="center" colspan="2">
			      	<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn"
				onclick="save('${ctx}/menuController.do?saveUpdate=key')" />保存</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" 
				onclick="closeWindow()">关闭</a></span>
			   </td>
		    </tr>
    	</table>
	</form>
</body>
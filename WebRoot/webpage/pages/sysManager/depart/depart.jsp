<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>ESBMM</title>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
		
	$(function() {
		init();
		
		if($('#departGrade').val()!='0'){
			$('#pfun').show();
		}else{
			$('#pfun').hide();
		}
		
		$('#departGrade').change(function(){
			if($(this).val()!='0'){
				$('#pfun').show();
				var t = $('#cc').combotree('tree');
				var nodes = t.tree('getRoots');
				if(nodes.length>0){
					$('#cc').combotree('setValue', nodes[0].id);
					$("#parentdepartId").val(nodes[0].id);
				}
			}else{
				var t = $('#cc').combotree('tree');
				var node = t.tree('getSelected');
				if(node){
					$('#cc').combotree('setValue', null);
				}
				$('#pfun').hide();
			}
		});
	});		
	function save(url){
		$('#myform').form('submit', {
			url : url,
			onSubmit : function() {
				var form=$("#myform").Validform(); 
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try{
					d = $.parseJSON(data);
					if(d.success==true){
					 windowapi.close();
					  W.tip('操作成功');
					  W.reloaddepartList();;
				}
				}catch(ex){
					var emsg = data.substring(data.indexOf('错误描述'),
										data.indexOf('错误信息')); 
			        $.messager.alert('错误',emsg);
					return;
				}
			}
		});
	}
	
</script>
</head>
<body style="overflow-y: hidden" scroll="no">

	<form id="myform" name="myform" method="post">
		<input name="pkId" type="hidden" value="${depart.pkId}">
		<input name="operateFlag" type="hidden" value="${operateFlag}">
		
		<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
			<tr>
	    		 <td align="right" width="30%" nowrap>
	    		  <label class="Validform_label">
	    		<span>组织机构名称:</span>
	    		 </label>
	    		</td>
	    		 <td class="value" width="70%" >
	    			<input onkeypress="notNull(this);" id="departName"
						name="departName" class="inputxt" value="${depart.departName}"
						datatype="s1-50"  nullmsg="请输入组织机构名称！"errormsg="请输入1-50位合法字符"> 
						<span style="color:red">*</span>	
				 </td>
				
	    	</tr>
	    	<tr>
	    		 <td align="right" width="30%" nowrap>
	    		  	<label class="Validform_label">
	    				<span>组织机构等级:</span>
	    			 </label>
	    		</td>
	    		 <td class="value" width="70%" >
	    			<select
					name="departGrade" id="departGrade" datatype="*">
					<option value="0"
						<c:if test="${departLevel eq '0'}">selected="selected"</c:if>>
						一级组织机构</option>
					<option value="1"
						<c:if test="${departLevel ne '0'}">selected="selected"</c:if>>
						下级组织机构</option>
				</select> 
				 </td>
	    	</tr>
			<tr id="pfun">
	    		 <td align="right" width="30%" nowrap>
	    		  	<label class="Validform_label">
	    				<span>上级组织机构: </span>
	    		 	</label>
	    		</td>
	    		 <td class="value" width="70%" >
	    			<input class="easyui-combotree" style="width:260px;height:35px;" name="parentDepartId" id="cc" value="${combotree_pkId}" data-options="url:'departController.do?getCombotree=key',method:'get'">
				<%-- <input id="parentdepartId" name="parentdepartId" style="display: none;"
					value="${depart.parentdepartId}" /> --%>
				 </td>
	    	</tr>
	    	<tr>
		     	<td align="center" colspan="2" >			      
					<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="save(' ${ctx}/departController.do?saveUpdate=key')">保存</a></span>
					<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
			   </td>
		    </tr>
    	</table>
	</form>
</body>
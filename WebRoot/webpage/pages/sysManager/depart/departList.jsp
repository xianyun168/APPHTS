<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<%@ include file="/common/public.jsp"%>
		<%@ include file="/common/head.jsp"%>
		<%-- <script type="text/javascript" src="${ctx}/common/js/easyui/1.4/treegrid-dnd.js"></script> --%>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
		<script type="text/javascript" src="${ctx}/webpage/pages/sysManager/js/sysmanager.js"></script>
	</head>
<body>
<style>
			.datagrid-btable tr,
			.datagrid-btable td,.datagrid-cell, .datagrid-cell-group, .datagrid-header-rownumber, .datagrid-cell-rownumber{
				height:41px !important;
				line-height:41px !important;
			}
			.datagrid-btable tr,
			.datagrid-btable td,.treegrid-tr-tree{
				height:0px !important;
			}
		</style>
<script type="text/javascript">
	$(function(){
	  query();
	});
	function query(){
		$('#departList').treegrid(
						{
							idField : 'pkId',
							treeField : 'departName',
							title : '机构管理',
							url : 'departController.do?departTree=key&pkId =${pkId} ',
							fit : true,
							loadMsg : '数据加载中...',
							pageSize : 10,
							pagination : false,
							pageList : [ 10, 20, 30 ],
							//sortOrder : 'asc',
							rownumbers : true,
							singleSelect : true,
							fitColumns : true,
							showFooter : true,
							frozenColumns : [ [] ],
							columns : [ [
								{field : 'ck',checkbox:true},
									{
										field : 'pkId',
										title : '编号',
										sortable : false,
										hidden : true
									},
									{
										field : 'departName',
										title : '组织机构名称',
										sortable : false,
										width : 100,
										align : 'left'
									},
									{
										field : 'null',
										title : '操作',
										sortable : false,
										width : 100,
										align : 'left',
										formatter : function(value, rec, index) {
											if (!rec.pkId) {
												return '';
											}
											var href = '';
											href += "[<a href='#' onclick=del('departController.do?delete=key','departList','"+rec.pkId+"') style='color:#3f99b1;'>";
											href += "删除</a>]";
											

											var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
											var oper = operCode.split(', ');
											var i = oper.length;
											if(oper !=""){
											  for(var j=0;j<oper.length;j++){
												 if(oper[j] == "delete"){
											 href=href.replace("[<a href='#' onclick=del('departController.do?delete=key','departList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
														} 
											
												}
											}
											
											return href;
										}
									} ] ],
									
							onLoadSuccess : function(row) {
								
								$("#departList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
								$("#departList").treegrid("clearSelections");
								$("#departList").treegrid('enableDnd',row?row.pkId:null);
							},
							onClickRow : function(rowData) {
								rowid = rowData.pkId;
								gridname = 'departList';
							},
							onDrop : function(targetRow, sourceRow,point){
								var parentId=sourceRow._parentId;
								if(parentId==null){
									parentId="0";
								}
								url='${ctx}/departController.do?saveUpdate=key'+'&operateFlag=modify'+
									'&pkId='+sourceRow.pkId+'&parentDepartId='+parentId;
								$.ajax({
									async : false,
									cache : false,
									type : 'POST',
									url : url,// 请求的action路径
									error : function() {// 请求失败处理函数
									},
									success : function(data) {
										var d = $.parseJSON(data);
										if (d.success) {
											var msg = d.msg;
											tip(msg);
											reloadTable();
										}
									}
								});
								return true;
							}
						});
		$('#departList').treegrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		$('#departList').treegrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
	}
	function reloadTable() {
		try {
			$('#' + gridname).datagrid('reload');
			$('#' + gridname).treegrid('reload');
		} catch (ex) {
		}
	}
	function reloaddepartList() {
		$('#departList').treegrid('reload');
	}
	function getdepartListSelected(field) {
		return getSelected(field);
	}
	function getSelected(field) {
		var row = $('#' + gridname).treegrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	function getdepartListSelections(field) {
		var ids = [];
		var rows = $('#departList').treegrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids;
	}
	
	//删除
	function del(url,id,pkId) {
		var rowsData = $('#'+id).datagrid('getSelections');
		url += '&pkId='+pkId;
		window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    	if (r){
        	$.post(url,function(result){
            	if (result.success){
            		if(result.obj =='alert'){
            			tip(result.msg);
            		}else{
                    	$('#'+id).datagrid('reload');    // reload the user data
                    	$('#'+id).treegrid('reload');	 // reload the user data
            		}
            	} else {
                	window.top.$.messager.show({// show error message
                    	title: 'Error',
                    	msg: result.errorMsg
                	});
            	}
        	},'json');
    	}
	});}

$(function() {
	$(".pagination .pagination-num").attr("readonly","readonly");
	var li_east = 0;
});
function operationDetail(departId)
{
	if(li_east == 0){
	   $('#system_depart_departList').layout('expand','east'); 
	}
	$('#operationDetailpanel').panel("refresh", "operationController.do?operationList=key&departId=" +departId);
}
function addFun(title,url, id) {
	var rowData = $('#'+id).datagrid('getSelected');
	if (rowData) {
		url += '&pkId='+rowData.pkId;
	}
	add_win(title,url,'departList',500,360);
}
$(function() {


	noauto();


});
//页面初始化时，设置按钮的权限
function noauto() {
	//取得对应的权限的值(后台返回的是没有的权限的值)
	var operCode = $("#noauto_operationCodes").val().replace("[", "")
			.replace("]", "");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i) {
		$('#' + oper[i]).hide();
	})
}
</script>

<div id="system_depart_departList" class="easyui-layout" fit="true">
<div region="center" style="padding:1px;">
<table width="100%" id="departList" toolbar="#departListtb" class="easyui-treegrid"></table>
<div id="departListtb" >
<!-- 用于权限判断 start -->
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:270px;margin:auto;">			
			<span class="bluebtnhover"><a onclick="addFun('组织机构录入',' ${ctx}/departController.do?add=key','departList')" id="add" plain="true" class="formbtn bluebtn">组织机构录入</a></span>
			<span class="bluebtnhover"><a href="#" class="formbtn bluebtn" plain="true" onclick="update('组织机构编辑',' ${ctx}/departController.do?modify=key','departList',500,360)" id="modify">组织机构编辑</a></span>
		</div>
	</div>
</div>
</div>

</div>

</body>
</html>
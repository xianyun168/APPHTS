<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>IMP</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="WebserviceLogList" toolbar="#demoListtb"></table>
<div id="demoListtb" style="padding:3px; height: auto">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
<div style="text-align:center;">
	<div class="jiekoubtntop">
		<div style="float: left;font-size:14px;margin: 5px;white-space:nowrap;">
			<label class="jiekouname">接口名：</label>
			<input type="text" id="interfacename"  name="interfacename"  style="width: 240px;height:23px; margin-left:10px;" />
		</div>
	</div>
</div>
<div style="clear:both;">
	<div class="leftbtntool">			
		<span class="orangebtnhover" style="float:right;"><a href="javascript:void(0)" class="easyui-linkbutton dialogbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
		<span class="orangebtnhover" style="float:right;"><a onclick="query()" class="easyui-linkbutton dialogbtn orangebtn">查询</a></span>
	</div>
</div>





</div>
<script type="text/javascript">
	function query(){
		$('#WebserviceLogList').datagrid({
			idField : 'pkId',
			title : '访问日志',
		    url : ' ${ctx}/webserviceLogController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
		    	'interfacename':$("#interfacename").val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'interfacename',title:'接口名',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'col1',title:'持续时间(ms)',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'insertTime',title:'开始时间',  
						 formatter:function(val,rec){
				        		if(val != null && val != ''){
				        			return formattimes(val);
				        		}
				        		  },
						 width:fixWidth(0.11),align:'center',sortable:true},
					{field:'endTime',title:'结束时间',  
							 formatter:function(val,rec){
					        		if(val != null && val != ''){
					        			return formattimes(val);
					        		}
					        		  },
							 width:fixWidth(0.11),align:'center',sortable:true},
					{field : 'requestXml',title : '请求报文',align:'center',width : 30,
						formatter : function(value, rec,
										index) {
									if (!rec.pkId) {
										return '';
									}
									var href = '';
									href += "[<a href='#' onclick=update('请求报文','${ctx}/webserviceLogController.do?reqXml=key','WebserviceLogList',600,400) >";
									href += "查看</a>]";
									return href;
								}
							},
					{field : 'responseMsg',title : '返回信息',align:'center',width : 30,
							formatter : function(value, rec,
												index) {
									if (!rec.pkId) {
											return '';
									}
									var href = '';
									href += "[<a href='#' onclick=update('返回信息','${ctx}/webserviceLogController.do?resMsg=key','WebserviceLogList',600,400) >";
									href += "查看</a>]";
									return href;
							}
					}			 
				    ]],
			onLoadSuccess : function(data) {
				//清楚所选的行
				$("#WebserviceLogList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'WebserviceLogList';
			}
		});
		$("#WebserviceLogList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#WebserviceLogList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});
	function reset(){
		
	}
	function reloadTable() {
		try {
			$('#' + WebserviceLogList).datagrid('reload');
			$('#' + WebserviceLogList).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + WebserviceLogList).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#WebserviceLogList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids;
	};
	
		//页面初始化时，设置按钮的权限
	function noauto(){
		//取得对应的权限的值(后台返回的是没有的权限的值)
	    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
		var oper = operCode.split(', ');
		var i = oper.length;
		$(oper).each(function(i){
			$('#' + oper[i]).hide();
		});
	}

</script>
	</body>
</html>


		
			

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>产品信息</title>
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">

</head>
<body>
	<table width="100%" id="ProductInfoList" toolbar="#demoListtb"></table>
	<div id="demoListtb" style="padding: 3px; height: auto">
		<!-- 用于权限判断 start -->
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
		<div style="text-align: center;">

			<div class="jiekoubtntop">
				<div style="height: 0; width: 0">
					<label class="jiekounam"
						style="text-align: left; margin-left: -1144px"></label>
				</div>
				<div class="jiekoubox">
					<div
						style="float: left; font-size: 14px; margin: 5px; white-space: nowrap;">
						<label class="jiekouname" style="width: 92px">产品名称 :</label> <input type="text"
							id="productName" name="productName"
							style="width: 160px; height: 23px; margin-left: 10px; border: 1px solid #999" />
					</div>
					
				</div>
			</div>
		</div>
<div style="clear:both;padding-top:20px;">
	<div class="leftbtntool" style="width:400px;margin:auto;">			
		<span class="orangebtnhover"><a href="javascript:void(0)" class="orangebtn formbtn" onClick="reset()" id="reset">重置</a></span>
		<span class="bluebtnhover"><a onclick="query()" class="bluebtn formbtn">查询</a></span>
	</div>
</div>

	</div>
	<script type="text/javascript">
		function reloadDG() {
			/* alert() */
			//$('#SuperviseProcuratorList').datagrid('reload');
			query();
		}
		function query() {
			$('#ProductInfoList')
					.datagrid(
							{
								idField : 'pkId',
								title : '折标系数管理',
								url : ' ${ctx}/coefficientController.do?datagrid=key',
								fit : true,
								//loadMsg : '数据加载中...',
								pageSize : 10,
								pagination : true,
								pageList : [ 10, 20, 30 ],
								//sortName: 'id',
								sortOrder : 'asc',
								rownumbers : true,
								singleSelect : true,
								fitColumns : true,
								remoteSort : false,
								/* checkOnSelect : false, */
								selectOnCheck : true,
								queryParams : {
									'productname' : $('#productName').val(),
									'pruducttype': $('#pruductType').val(),
									'term':$('#term').val()
								},
								columns : [ [
										{
											field : 'ck',
											checkbox : true
										},
										{
											field : 'pkId',
											title : '主键',
											width : fixWidth(0.11),
											align : 'center',
											sortable : true,
											hidden : true
										},
										/* {
											field : 'productName',
											title : '产品名称',
											formatter : function(value, row,
													index) {
												return "<a href='#' style='color:#3f99b1' onclick=productNameAll('"
														+ row.pkId
														+ "');>"
														+ value + "</a>";
											},
											width : fixWidth(0.11),
											align : 'center',
											sortable : true
										}, */
										{
											field : 'productname',
											title : '产品名称',
											width : fixWidth(0.11),
											align : 'center',
											sortable : true
										},
										{
											field : 'pruducttype',
											title : '类型',
											width : fixWidth(0.11),
											align : 'center',
											sortable : true
										},
										{
											field : 'yield',
											title : '业绩比较基准',
											width : fixWidth(0.11),
											align : 'center',
											sortable : true
										},
										{
											field : 'deadline',
											title : '投资期限',
											width : fixWidth(0.11),
											align : 'center',
											sortable : true
										},
										{
											field : 'perminbalance',
											title : '起投金额',
											width : fixWidth(0.10),
											align : 'center',
											sortable : true
										},
										

										{
											field : 'null',
											title : '操作',
											width : fixWidth(0.16),
											align : 'center',
											sortable : true,
											formatter : function(value, rec,
													index) {
												if (!rec.pkId) {
													return '';
												}
												var href = '';
												href += "[<a href='#' onclick=update1('coefficientController.do?modify=key','"+rec.pkId+"') style='color:#3f99b1;'>";
												href += "配置</a>]";
												href += "[<a href='#' onclick=update('批量配置折标系数','coefficientController.do?modify1=key','ProductInfoList','"+rec.pkId+"',400,260) style='color:#3f99b1;'>";
												href += "批量配置</a>]";
												return href;
											}
										} ] ],
								onLoadSuccess : function(data) {
									//无查询结果提示
									if (data.total == 0) {
										$('.datagrid-view2 .datagrid-body')
												.append(
														'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

										$(this).closest('div.datagrid-wrap')
												.find('div.datagrid-pager')
												.hide();
									}

									//清楚所选的行
									$("#SuperviseProcuratorList").datagrid(
											"clearSelections");
								},
								//rowIndex:行索引 rowData：行数据
								onClickRow : function(rowIndex, rowData) {
									/* alert(rowIndex) */
									rowid = rowData.id;
									gridname = 'SuperviseProcuratorList';
								}
							});
			$("#ProductInfoList").datagrid('getPager').pagination({
				beforePageText : '',
				afterPageText : '/{pages}',
				displayMsg : '显示{from}-{to}共{total}条',
				showPageList : true,
				showRefresh : true
			});

			$("#ProductInfoList").datagrid('getPager').pagination({
				onBeforeRefresh : function(pageNumber, pageSize) {
					$(this).pagination('loading');
					$(this).pagination('loaded');
				}
			});
		};

		$(function() {
			//查询
			query();

			noauto();

		});

		//导出
		function exportList(url) {

			var name = document.getElementById("name").value;
			//var theirunion = document.getElementById("theirunion").value;
			var ssgh = document.getElementById("ssgh").value;
			var ssghf = document.getElementById("ssghf").value;
			var supervisor = document.getElementById("supervisor").value;
			var validState = document.getElementById("validState").value;
			var uplandDateS = document.getElementById("uplandDateS").value;
			var dateEndE = document.getElementById("dateEndE").value;

			window.location.href = url + "&name=" + encodeURI(encodeURI(name))
					+ "&validState=" + encodeURI(encodeURI(validState))
					+ "&supervisor" + supervisor + "&uplandDateS" + uplandDateS
					+ "&dateEndE" + dateEndE + "&ssgh" + ssgh + "&ssghf"
					+ ssghf;
		}
		//导出
		function exportCata(url, id) {

			var rowsData = $('#' + id).datagrid('getSelections');

			if (!rowsData || rowsData.length == 0) {
				tip.AutoPopDelay = 1000000000; 
				tip('请选择导出数据');
				return;
			}
			if (rowsData.length > 1) {
				tip('请选择一条记录再导出');
				return;
			}

			url = url + "&pkId=" + rowsData[0].pkId;
			window.location.href = url

		}
		//修改
		function update1(url,pkId) {
			/*  var rowsData = $('#'+id).datagrid('getSelections'); */
			 url += '&pkId=' + pkId;
				
				add_win('配置',
						'${ctx}/coefficientController.do?modify=key&pkId='+pkId, 1200, 520);

		}

		function add_win(title, addurl, gname, width, height) {
			gridname = gname;
			width = width ? width : 700;
			height = height ? height : 400;
			if (width == "100%" || height == "100%") {
				width = document.body.offsetWidth;
				height = document.body.offsetHeight - 100;
			}
			$.dialog({
				content : 'url:' + addurl,
				lock : true,
				width : 520,
				height : 450,
				title : title,
				opacity : 0.3,
				cache : false
			});
		}
		//批量配置折标系数
		function update(title, url, id,pkId, width, height) {
				url += '&pkId=' + pkId;
				width = width ? width : 700;
				height = height ? height : 450;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + url,
					lock : true,
					width : width,
					height : height,
					title : title,
					opacity : 0.3,
					cache : false
				}); 
			}
		function reset() {
			$('#productName').val("");
		}
		function reloadTable() {
			try {
				$('#' + SuperviseProcuratorList).datagrid('reload');
				$('#' + SuperviseProcuratorList).treegrid('reload');
			} catch (ex) {
			}
		}

		function getdemoListSelected(field) {
			return getSelected(field);
		}

		//选中单行
		function getSelected(field) {
			var row = $('#' + SuperviseProcuratorList).datagrid('getSelected');
			if (row != null) {
				value = row[field];
			} else {
				value = '';
			}
			return value;
		}

		function getUserListSelections(field) {
			var ids = [];
			var rows = $('#SuperviseProcuratorList').datagrid('getSelections');
			for (var i = 0; i < rows.length; i++) {
				ids.push(rows[i][field]);
			}
			ids.join(',');
			return ids
		};

		//页面初始化时，设置按钮的权限
		function noauto() {
			//取得对应的权限的值(后台返回的是没有的权限的值)
			var operCode = $("#noauto_operationCodes").val().replace("[", "")
					.replace("]", "");
			var oper = operCode.split(', ');
			var i = oper.length;
			$(oper).each(function(i) {
				$('#' + oper[i]).hide();
			})
		}
	</script>
</body>
</html>





<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>jQuery带数据的日历插件</title>
<link type="text/css" rel="stylesheet" href="${ctx}/css/calendar-pro.css">
</head>
 <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet"> 
<body>
<!-- 容器布局 -->
<div class="calendar-box demo-box"></div>
<!-- <button id="saveBtn" onclick="getActive('coefficientController.do?add=key')">配置折标系数</button>
 -->
<table style=" width:100%;" cellpadding="0" id="tt" cellspacing="1" class="formtable">
   <tr>
	    		<td align="center" colspan="1">
		      <span class="orangebtnhover"><a id="saveBtn"  class="easyui-linkbutton dialogbtn orangebtn" onclick="getActive('coefficientController.do?add=key')">配置折标系数</a></span>
   	 			</td>
   	 		</tr>
</table>


<input id="dateVal" name="dateVal" value="${dateVal}" type="hidden"/>
<input id="productId" name="productId" value="${productId}" type="hidden"/>
</body>
<script type="text/javascript" src="${ctx}/js/calendar-pro.js"></script>

<script type="text/javascript">

 	$(function() {		
		closeUf();
 });
	function closeUf(){
		var time = formatDate(new Date());
		var dateVal = $("#dateVal").val();
		var datea=dateVal.substring(0,dateVal.length-1)
		//alert(datea);
		var dataArray = new Array();
		var dataArray2 = new Array();

		dataArray = datea.split(":");
		 for(var i=0;i<dataArray.length-1;i++){
			var str = {date:dataArray[i],data:dataArray[i+1]};
			dataArray2.push(str);
			i++;
		} 
		$('.calendar-box').calendar({
			ele : '.demo-box', //依附
			title : '选择时间',
 			beginDate : '2017-05-07',
 			endDate : '2017-07-04',
			data : dataArray2
			           
		});
		
	}
	
function getActive(url){
	var data = $('.calendar-box').calendarGetActive();
	console.log(data);
	//alert("日期："+data.date+"折标系数："+data.money);
	var date = data.date;
	var money = data.money;
	if(date =='undefined' || date ==null){
		tip("请选择日期");
		return;
	}
	var productId = $("#productId").val();
	up('编辑', '${ctx}/coefficientController.do?add=key&date='+date+'&money='+money+'&productId='+productId, 150,450);
}



function up(title, addurl, gname, width, height) {
	gridname = gname;
	width = width ? width : 700;
	height = height ? height : 400;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		zIndex: 1997,
		content : 'url:' + addurl,
		lock : true,
		width : width,
		height : 150,
		title : title,
		opacity : 0.3,
		cache : false
	});
}

function formatDate(now) {
	var year = now.getFullYear();
	var month = now.getMonth() + 1;
	var date = now.getDate();
	if (month < 10) {
		if (date < 10) {
			return year + "-" + "0" + month + "-" + "0" + date;
		} else {
			return year + "-" + "0" + month + "-" + date;
		}
	} else {
		if (date < 10) {
			return year + "-" + month + "-" + "0" + date;
		} else {
			return year + "-" + month + "-" + date;
		}
	}
}

</script>
</html>
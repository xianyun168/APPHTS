<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="RankInfoList" toolbar="#demoListtb"></table>
<div id="demoListtb" style="padding:3px; height: auto">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
<div class="jiekouform">
	<div>
		<label>职级：</label>
		<select id="rank" name="rank" >
            <option value="">全部</option>
            <c:forEach var="code" items="${codeList}">
            	<option value="${code.codeValue}">${code.chineseName}</option>
            </c:forEach>
            <!-- <option value="80">副总经理6(内部晋升)</option>
			<option value="63">副总经理5(内部晋升)</option>
			<option value="45">副总经理4</option>
			<option value="52">副总经理3</option>
			<option value="56">副总经理2</option>
			<option value="41">副总经理1</option>
			<option value="60">部门经理5</option>
			<option value="75">部门经理4</option>
			<option value="58">部门经理3</option>
			<option value="43">部门经理2</option>
			<option value="55">部门经理1</option>
			<option value="59">代理部门经理</option>
			<option value="88">首席理财官4(内部晋升)</option>
			<option value="87">首席理财官3(内部晋升)</option>
			<option value="86">首席理财官2(内部晋升)</option>
			<option value="85">首席理财官1(内部晋升)</option>
			<option value="76">资深理财师4</option>
			<option value="51">资深理财师3</option>
			<option value="50">资深理财师2</option>
			<option value="48">资深理财师1</option>
			<option value="57">理财师5</option>
			<option value="46">理财师4</option>
			<option value="53">理财师3</option>
			<option value="49">理财师2</option>
			<option value="44">理财师1</option>
			<option value="47">助理理财师</option>
			<option value="42">助理理财师(考察期)</option> -->
         </select> 	
	</div>
</div>

<div style="clear:both;padding-top:20px;">
	<div class="leftbtntool" style="width:400px;margin:auto;">			
		<span class="orangebtnhover"><a href="javascript:void(0)" class="orangebtn formbtn" onClick="reset()" id="reset">重置</a></span>
		<span class="bluebtnhover"><a onclick="query()" class="bluebtn formbtn">查询</a></span>
	</div>
</div>

</div>
<script type="text/javascript">
	function query(){
		$('#RankInfoList').datagrid({
			idField : 'pkId',
			title : '业绩目标管理 ',
		    url : ' ${ctx}/rankInfoController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				'rank': $('#rank').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'rank',title:'职级',  width:fixWidth(0.3),align:'center',sortable:false},
					{field : 'opt',title : '操作',width:fixWidth(0.1),align:'center',sortable:false,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
 							href += "[<a href='#' onclick=update2('"+rec.rank+"','"+ rec.pkId+ "') style='color:#3f99b1;'>";
 							href += "设定目标</a>]";
 							href += "[<a href='#' onclick=update1('"+rec.pkId+"','"+rec.rank+"') style='color:#3f99b1;'>";
 							href += "详情</a>]";	
							return href;
					}}
		    ]],
			onLoadSuccess : function(data) {
				$("#RankInfoList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清楚所选的行
				$("#RankInfoList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'companyInfoList';
			}
		});
		$("#RankInfoList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#RankInfoList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
	};

	$(function(){
		//查询
		query();	
		//noauto();	
	});

	function update1(pkId,rank){
		add_win('详情','${ctx}/rankInfoController.do?details=key&pkId='+pkId+'&rank='+rank,'RankInfoList',950,500);
	}	
	
	
	//编辑页面
	function update2(rank,pkId){
		add_win('设定目标', '${ctx}/rankInfoController.do?add=key&rank='+ rank+'&pkId='+pkId ,'RankInfoList', 950,500);
	}
	
	//删除
	function dele(url,id,pkId) {
		var rowsData = $('#'+id).datagrid('getSelections');
		url += '&pkId='+pkId;
		window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    	if (r){
        	$.post(url,function(result){
            	if (result.success){
            		if(result.obj =='alert'){
            			tip(result.msg);
            		}else{
                    	$('#'+id).datagrid('reload');    // reload the user data
                    	$('#'+id).treegrid('reload');	 // reload the user data
            		}
            	} else {
                	window.top.$.messager.show({// show error message
                    	title: 'Error',
                    	msg: result.errorMsg
                	});
            	}
        	},'json');
    	}
	});}
	
	
	function reset(){
		$('#rank').val("");
	}
	
	
	function reloadTable() {
		try {
			$('#' + RankInfoList).datagrid('reload');
			$('#' + RankInfoList).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + RankInfoList).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#RankInfoList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	};
	
		//页面初始化时，设置按钮的权限
	function noauto(){
		//取得对应的权限的值(后台返回的是没有的权限的值)
	    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
		var oper = operCode.split(', ');
		var i = oper.length;
		$(oper).each(function(i){
			$('#' + oper[i]).hide();
		})
	}

</script>
</body>
</html>


		
			

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
		init();
	});
	function saveData(url) {
		var form = $("#myform").Validform();
		
		var start = $("#assessmentStartTime").val();
		var end = $("#assessmentEndTime").val();
		var rank = $("#rank").val();
		if(start=="" || start==null){
			tip("请输入业绩开始时间");
			return;
		}
		
		if(end=="" || end==null){
			tip("请输入业绩结束时间");
			return;
		}
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			// $.dialog.tips('数据保存中...',500,'loading.gif');
			window.top.$.messager.progress({
				text : '数据保存中....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "true");
		}

		$('#myform').form(
				'submit',
				{
					url : url,
					onSubmit : function() {
						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
						
							}
							/*
							 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
							 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
							 */
							else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
							 	return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'), data
									.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
	}
	function add_win(title, addurl, gname, width, height) {
		alert(addurl)
		gridname = gname;
		width = width ? width : 700;
		height = height ? height : 400;
		if (width == "100%" || height == "100%") {
			width = document.body.offsetWidth;
			height = document.body.offsetHeight - 100;
		}
		$.dialog({
			content : 'url:' + addurl,
			lock : true,
			width : width,
			height : height,
			title : title,
			opacity : 0.3,
			cache : false
		});
	}
</script>
 </head>
 <body style="overflow-y: auto" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${performanceGoals.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${performanceGoals.pkId}">
    </c:if>
  	<table style=" width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
		  <td align="right" width="10%" nowrap>
    		  <label class="Validform_label"><span>职级:</span></label>
    	  </td>
    	  <td class="value" width="15%">
			   <input id="rank" name="rank" value="${performanceGoals.rank}" type="hidden"/>
			 
		        <input class="inputxt" onkeypress="notNull(this);" value="${rank}" readonly="readonly"/>
		</td> 
    	      	  </tr>
    	  <tr>
		  <td align="right" width="10%" nowrap>
    		  <label class="Validform_label"><span>业绩开始时间:</span></label>
    	  </td>
    	  <td class="value" width="15%" >
		     <input type="text" id="assessmentStartTime" name="assessmentStartTime" class="Wdate" style="width: 260px; height: 35px;" onClick="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'assessmentEndTime\')}'})" value="${startTime}"></input><span style="color: red;">*</span>
    	  </td>
    	</tr>
   
    	<tr>
		  <td align="right" width="10%" nowrap>
    		  <label class="Validform_label"><span>业绩结束时间:</span></label>
    	  </td>
    	  <td class="value" width="15%" >
		<input type="text" id="assessmentEndTime"  name="assessmentEndTime"  class="Wdate" style="width: 260px; height: 35px;" onClick="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'assessmentStartTime\')}'})" value="${endTime}"></input><span style="color: red;">*</span>
    	  </td>
    	  </tr>
    	  <tr>
		  <td align="right" width="10%" nowrap>
    		  <label class="Validform_label"><span>业绩目标:</span></label>
    	  </td>
    	  <td class="value" width="15%" >
    		  <input class="inputxt"  id=target onkeypress="notNull(this);" name="target" value="${performanceGoals.target}"  datatype="/(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/" nullmsg="请输入业绩目标！" errormsg="请输入正确业绩目标"/><span style="color: red;">*</span>
    	  </td>
    	</tr>
    	
    	<tr>
     		<td align="center" colspan="8">
        		<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/rankInfoController.do?update=key')">保存</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
</body> 
  
  
  
  

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>DZBD</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">

 </head>
 <body style="overflow-y: auto" scroll="no">
 
 	<form id="myform" name="myform" method="post" >
 	<c:if test="${rankInfo.pkId!= null}">
			<!-- 此处id,name,value值为controller对应方法中key的取值    key.变量名(同实体变量名) -->
			<input id="pkId" name="pkId" type="hidden"
				value="${rankInfo.pkId}">
		</c:if>
    	<table style=" width:100%;" cellpadding="0" id="tt" cellspacing="1" class="formtable">
		<tr class="firstpage" align="center" bgcolor="#d9ffff">
			<td align="right" width="10%" nowrap>
	    		  <label class="Validform_label"><span>职级:</span></label>
	    	   </td>
	    	   
			 <td>
			   <input id="rank" name="rank" value="${ranks}" type="hidden"/>
			 
		        <input class="inputxt" style="width: 120px; height: 20px;"onkeypress="notNull(this);" value="${rank}" readonly="readonly"/>
		</td> 
		
		 <td align="right" width="10%" nowrap>
	    		  <label class="Validform_label"><span>业绩开始时间:</span></label>
	      </td>
							
		<td>
		     <input type="text" id="assessmentStartTime" name="assessmentStartTime" class="Wdate" style="width: 160px; height: 20px;" onClick="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd',maxDate:'#F{$dp.$D(\'assessmentEndTime\')}'})"></input>
		</td>
		
			<td align="right" width="10%" nowrap>
	    		  <label class="Validform_label"><span>业绩结束时间:</span></label>
	    	 </td>
		<td>
		<input type="text" id="assessmentEndTime"  name="assessmentEndTime"  class="Wdate" style="width: 160px; height: 20px;" onClick="WdatePicker({readOnly:true,dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'assessmentStartTime\')}'})"></input>
		 </td>
		 
         <td align="right" width="10%" nowrap>
	    		  <label class="Validform_label"><span>业绩目标:</span></label>
	      </td>
							
		<td><input class="inputxt" id="target" onkeypress="notNull(this);" style="width: 120px; height: 20px;"name="target" />
		
		</td>
								
		<td>
		      <span class="orangebtnhover"><a id="saveBtn"  class="easyui-linkbutton dialogbtn orangebtn" onclick="saveList('${ctx}/rankInfoController.do?saveUpdate=key')">新增</a></span>
		  </td>
						
	</tr>
			 		
  	 </table>				
	<table style=" width:100%;" cellpadding="0" id="ttt" cellspacing="1" class="formtable" border="1">
						
			<tr class="firstpage" align="center" bgcolor="#f0f0f0">
							<!-- <td width="25%">职级</td> -->
							<td width="25%">业绩开始时间</td>
							<td width="25%">业绩结束时间</td>
							<td width="25%">业绩目标</td>
							<td width="10%">操作</td>
						</tr>					
   	 		    <c:forEach items="${list}" var="list" varStatus="status">
			<tr class="firstpage" align="center" id="${list.pkId}">
							 <td style="display:none"><input class="inputxt" 
								readOnly="readOnly" style="width: 180px; height: 20px; text-align: center;border: none; "
								onkeypress="notNull(this);"
								value="${list.pkId}" /></td> 
							<td><input type="text" readOnly="readOnly"
								 style="width: 180px; height: 20px; text-align: center;border: none;"
								value="${list.remark1}"></input></td> 
							<td><input type="text" readOnly="readOnly"
								 style="width: 180px; height: 20px; text-align: center;border: none;"
								value="${list.remark2}"></input></td>
								<td><input type="text" readOnly="readOnly"
								 style="width: 180px; height: 20px; text-align: center;border: none;"
								value="${list.target}"></input></td>
								
								<td><span
								 style="width: 180px; height: 20px; text-align: center;border: none;"
							><a href= "#" onclick="dele('rankInfoController.do?delete=key','${list.pkId}','${list.rank}',this)"style='color:#3f99b1;'>删除</a></span>
							<span 
								 style="width: 180px; height: 20px; text-align: center;border: none;"
							><a href= "#" onclick="update('rankInfoController.do?update=key','${list.pkId}')"style='color:#3f99b1;'>编辑</a></span></td>
						
						
						</tr>
					</c:forEach>
					
</table>
<table style=" width:100%;" cellpadding="0" id="tt" cellspacing="1" class="formtable">
   <tr>
	    		<td align="center" colspan="1">
					<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 			</td>
   	 		</tr>
</table>
   <script type="text/javascript">
  
 	$(function() {
		init();
	});
 
	function saveList(url) {
		var form = $("#myform").Validform();

		var start = $("#assessmentStartTime").val();
		var end = $("#assessmentEndTime").val();
		if(start=="" || start==null){
			tip("请输入业绩开始时间");
			return;
		}
		
		if(end=="" || end==null){
			tip("请输入业绩结束时间");
			return;
		}
		
		var reguser = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
		var user = $("#target").val();
		if (reguser.test(user) == false){
			tip("请输入正确的业绩目标")
			return;
		}
		
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			// $.dialog.tips('数据保存中...',500,'loading.gif');
			window.top.$.messager.progress({
				text : '数据保存中....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "false");
		}
	
		$('#myform').form('submit',{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				 var ss  = data.obj;
             	var aa = $.parseJSON(data);
             	var bb = aa.obj;

            	for(var i=0;i<bb.length;i++){
            		 $("#ttt").append("<tr class=\"firstpage\" align=\"center\"><td style=\"display:none\"><input class=\"inputxt\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none; \"onkeypress=\"notNull(this);\"value='"
            		 + bb[i].pkId+ "' /></td><td><input type=\"text\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none;\" value='"
            		 + bb[i].remark1+ "'></input></td><td><input type=\"text\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none;\" value='"
            		 + bb[i].remark2+ "'></input></td><td><input type=\"text\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none;\" value='"
            		 + bb[i].target+ "'></input></td><td><span style=\"width: 180px; height: 20px; text-align: center;border: none;\"><a href= \"#\" onclick=\"dele('rankInfoController.do?delete=key','"+bb[i].pkId+"','"+bb[i].rank+"',this)\"style='color:#3f99b1;'>删除</a></span> <span  style=\"width: 180px; height: 20px; text-align: center;border: none;\"><a href= \"#\" onclick=\"update('rankInfoController.do?update=key','"+bb[i].pkId+"')\"style='color:#3f99b1;'>编辑</a></span></td></tr>");
            	}
            	
            	$('#assessmentStartTime').val("");
            	$('#assessmentEndTime').val("");
            	$('#target').val("");
             	try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager
								.progress('close');
						/* windowapi.close(); */
						W.tip(d.msg);
						W.query();
					}
					
					/*
					 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
					 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
					 */
					else {
						window.top.$.messager
								.progress('close');
						$("#saveBtn").removeAttr(
								"disabled");
						window.top.$.messager.alert(
								'提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data
							.indexOf('错误描述'), data
							.indexOf('错误信息'));
					window.top.$.messager
							.progress('close');
					$("#saveBtn")
							.removeAttr("disabled");
					window.top.$.messager.alert('错误',
							emsg);
					return;
				}
			}
		});
	}
	//删除
	function dele(url,pkId,rank,aa) {
		url += '&pkId='+pkId+'&rank='+rank;
		window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    	if (r){
         	$.post(url,function(result){
            	if (result.success){
            		$(aa).parent().parent().parent().remove();
            		
            		//var ss  = result.obj;
            		/* if(ss.length ==parseInt(0)){
            			return;
            		}else{
            			$("#ttt").empty();	
            			
            			for(var i=0;i<ss.length;i++){
            			
                   		 $("#ttt").append("<tr class=\"firstpage\" align=\"center\"><td style=\"display:none\"><input class=\"inputxt\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none; \"onkeypress=\"notNull(this);\"value='"
                   		 + ss[i].pkId+ "' /></td><td><input type=\"text\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none;\" value='"
                   		 + ss[i].remark1+ "'></input></td><td><input type=\"text\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none;\" value='"
                   		 + ss[i].remark2+ "'></input></td><td><input type=\"text\" readOnly=\"readOnly\" style=\"width: 180px; height: 20px; text-align: center;border: none;\" value='"
                   		 + ss[i].target+ "'></input></td><td><span style=\"width: 180px; height: 20px; text-align: center;border: none;\"><a href= \"#\" onclick=\"dele('rankInfoController.do?delete=key','"+ss[i].pkId+"','"+ss[i].rank+"',this) \"style='color:#3f99b1;'\">删除</a></span><span style=\"width: 180px; height: 20px; text-align: center;border: none;\"><a href= \"#\" onclick=\"update() \"style='color:#3f99b1;'\">编辑</a></span></td></tr>");          		  	 
 
                   	     }
            			$("#ttt").show();
            		} */
       		
            		
            		if(result.obj =='alert'){
            			alert(2);
            			tip(result.msg);
            		}else{
            			//document.getElementsByTagName("*").ttt.deleteRow(a.parentElement.parentElement.rowIndex);

            			alert($("#ttt").find(id).html);
            			$("#ttt").find(id).remove();
            			/* reflash();
                    	$('#'+id).reflash('reload');    // reload the user data
                    	$('#'+id).treegrid('reload');	 // reload the user data */
            		}
            		
            		
            	} else {
                	window.top.$.messager.show({// show error message
                    	title: 'Error',
                    	msg: result.errorMsg
                	});
            	}
        	},'json');
    	}
	});

	}
	
	
	//编辑页面
	function update(url,pkId){
	
		up('编辑', '${ctx}/rankInfoController.do?modify=key&pkId='+ pkId, 500,400);
	}
	function up(title, addurl, gname, width, height) {
		gridname = gname;
		width = width ? width : 700;
		height = height ? height : 400;
		if (width == "100%" || height == "100%") {
			width = document.body.offsetWidth;
			height = document.body.offsetHeight - 100;
		}
		$.dialog({
			zIndex: 1997,
			content : 'url:' + addurl,
			lock : true,
			width : width,
			height : height,
			title : title,
			opacity : 0.3,
			cache : false
		});
	}
	
</script>
   
 </form>
</body> 
  
  
  
  

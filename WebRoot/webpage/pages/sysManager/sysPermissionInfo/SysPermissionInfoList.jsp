<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
	</head>
<body>
<table width="100%" id="SysPermissionInfoList" toolbar="#demoListtb"></table>
<div id="demoListtb" style="padding:3px; height: auto">
<div name="searchColums"  style="float:left;">
	<a href="#"class="easyui-linkbutton" plain="true" icon="icon-add" onclick="add_win('增加',' ${ctx}/sysPermissionInfoController.do?add=key','SysPermissionInfoList',900,500)" id="add" >增加</a>
	<a href="#" class="easyui-linkbutton" plain="true" icon="icon-edit"onclick="update('编辑',' ${ctx}/sysPermissionInfoController.do?modify=key','SysPermissionInfoList',1000,500)" id="update" >用户编辑</a>
</div>

<div style="height:30px;" class="datagrid-toolbar" align="right">
		  <a href="#" class="easyui-linkbutton"	iconCls="icon-search" onclick="query()">查询</a>
</div>

<div style="background-color:white;">
			用户：<input type="text" id="userName"  name="name" style="width:140px"></input>   
</div>
</div>
<script type="text/javascript">
	function query(){
		$('#SysPermissionInfoList').datagrid({
			idField : 'id',
			title : '',
		    url : ' ${ctx}/sysPermissionInfoController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : false,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				'userName': $('#userName').val()
			},
			columns:[[
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'status',title:'有效标志位',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'userPkId',title:'用户表主键',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'rolePkId',title:'角色表主键',  width:fixWidth(0.11),align:'center',sortable:true},
				    ]],
			onLoadSuccess : function(data) {
				//清楚所选的行
				$("#SysPermissionInfoList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'userList';
			}
		});
		$("#SysPermissionInfoList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#SysPermissionInfoList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
});
	
	function reloadTable() {
		try {
			$('#' + gridname).datagrid('reload');
			$('#' + gridname).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + gridname).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#userList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	};

</script>
	</body>
</html>


		
			

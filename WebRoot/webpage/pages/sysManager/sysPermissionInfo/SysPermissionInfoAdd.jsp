<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
	</head>
<body>
<table width="100%" id="MessageLog" toolbar="#demoListtb"></table>
<div id="demoListtb" style="padding:3px; height: auto">
<div name="searchColums"  style="float:left;">
</div>

<div style="height:30px;" class="datagrid-toolbar" align="left">
		 <a href="#"class="easyui-linkbutton" plain="true" icon="icon-add" onclick="getRoles()" >确定</a>
</div>

</div>
<script type="text/javascript">
	var pkIdsArray = new Array();
	var roleNameArray  = new Array();
	
	//var fileview = $.extend({}, $.fn.datagrid.defaults.view, { onAfterRender: function (target) { ischeckItem(); } });  
	  
    var checkedItems = [];  
    
    function ischeckItem() {  
        for (var i = 0; i < checkedItems.length; i++) {  
           // $('#MessageLog').datagrid('selectRecord', checkedItems[i]); //根据id选中行   
        }  
    }  
      
    function findCheckedItem(ID) {  
        for (var i = 0; i < checkedItems.length; i++) {  
            if (checkedItems[i] == ID) return i;  
        }  
        return -1;  
    }  

     function addcheckItem() {  
    	 //记录ID的数组增加一个元素
        var row = $('#MessageLog').datagrid('getChecked');  
        for (var i = 0; i < row.length; i++) {  
            if (findCheckedItem(row[i].pkId) == -1) {  
                checkedItems.push(row[i].pkId);  
            }  
        }  
     }  
     function removeAllItem(rows) {  
		//移除反选的一项
        for (var i = 0; i < rows.length; i++) {  
            var k = findCheckedItem(rows[i].pkId);  
            if (k != -1) {  
                checkedItems.splice(k, 1);  
            }  
        }  
     }  
     function removeSingleItem(rowIndex, rowData) {  
    	 //全部移除
        var k = findCheckedItem(rowData.pkId);  
        if (k != -1) {  
            checkedItems.splice(k, 1);  
        }  
     }  
	
	
	
	$(function(){
		var windowapi = frameElement.api, W = windowapi.opener;
		
		$('#MessageLog').datagrid({
			idField : 'pkId',
			title : '',
		    url : '${ctx}/roleController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : false,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				'userName': $('#userName').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'ID',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'roleName',title:'角色名称',  width:fixWidth(0.11),align:'center',sortable:true},
				    ]],
			onLoadSuccess : function(data) {
				//清楚所选的行
				$("#MessageLog").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.pkId;
				gridname = 'MessageLog';
			},
			onSelect : function(rowIndex, rowData){
				//选择一行则增加一个
				pkIdsArray.push(rowData.pkId);
				roleNameArray.push(rowData.roleName);
			},
			onUnselect:function(rowIndex, rowData){
				//反选去除一个
				pkIdsArray.splice(pkIdsArray.indexOf(rowData.pkId),1);
				roleNameArray.splice(roleNameArray.indexOf(rowData.roleName),1);
			}, 
            onCheck:function(rowIndex,rowData){
                addcheckItem();  
            },  
            onUncheckAll:function(rows){                 
                for (var i = 0; i < checkedItems.length; i++) {  
                	//翻页后根据id选中行
                    $('#MessageLog').datagrid('selectRecord', checkedItems[i]);    
                }      
            },  
            
		});
		$("#MessageLog").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
});

	function reloadTable() {
		try {
			$('#' + gridname).datagrid('reload');
			$('#' + gridname).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + gridname).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#MessageLog').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids;
	};
	
	function getRoles(){
		var roleIds = getIds().join(',');
		var roleNames = getNames().join(',');
		var windowapi = frameElement.api, W = windowapi.opener;
		W.$('#roleIds').val(roleIds);
		W.$('#roleNames').val(roleNames);
		windowapi.close(); 
	}

	//ID去重
	function getIds(){
		 var tmpArr = [];
		 for(var i=0; i<pkIdsArray.length; i++){
		 	if(tmpArr.indexOf(pkIdsArray[i]) == -1){
		  		tmpArr.push(pkIdsArray[i]);
		  	}
		 }
		 return tmpArr;
	}
	
	//角色去重
	function getNames(){
		 var tmpArr = [];
		 for(var i=0; i<roleNameArray.length; i++){
		 	if(tmpArr.indexOf(roleNameArray[i]) == -1){
		  		tmpArr.push(roleNameArray[i]);
		  	}
		 }
		 return tmpArr;
	}
</script>
	</body>
</html>


		
			

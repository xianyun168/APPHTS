<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>LIANGYUAN</title>
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
<link href="${ctx}/resources/js/jdhy/city.css" type="text/css"
	rel="stylesheet">
<link href="${ctx}/resources/js/ssgh/city1.css" type="text/css"
	rel="stylesheet">
<script src="${ctx}/resources/js/jdhy/cityJson.js"></script>
<script src="${ctx}/resources/js/jdhy/Popt.js"></script>
<script src="${ctx}/resources/js/jdhy/citySet.js"></script>
<script src="${ctx}/resources/js/ssgh/cityJson1.js"></script>
<script src="${ctx}/resources/js/ssgh/Popt1.js"></script>
<script src="${ctx}/resources/js/ssgh/citySet1.js"></script>
<script type="text/javascript">
	$(function() {
		init();
	});
	function saveData(url) {
		var form = $("#myform").Validform();
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			// $.dialog.tips('数据保存中...',500,'loading.gif');
			window.top.$.messager.progress({
				text : '数据保存中....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "true");
		}
		$('#myform').form(
				'submit',
				{
					url : url,
					onSubmit : function() {
						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
							}
							/*
							 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
							 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
							 */
							else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
								return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'),
									data.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
	}
</script>
<style type="text/css">
#aaa {
	width: 1400px;
	height: 1200px;
	-webkyt-bok: display
}

input[type="file"] {
	color: transparent;
}

#preview {
	width: 71px;
	height: 99px;
	border: 1px solid #000;
	overflow: hidden;
	position: absolute;
	top: 0px;
	right: 20%;
	border: 1px solid #a5aeb6
}

#imghead {
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=image);
}
</style>
</head>
<body style="overflow-y: scroll">
	<table style="overflow-y: scroll" width="100%" id="overTimeList"
		toolbar="#demoListtb"></table>
	<form id="myform" name="myform" method="post"
		enctype="multipart/form-data">
		<c:if test="${productInfo.pkId!= null}">
			<input id="pkId" name="pkId" type="hidden"
				value="${productInfo.pkId}">
		</c:if>
		<table style="width: 100%;" cellpadding="0" cellspacing="1"
			class="formtable">
			<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>产品名称:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="productName" onkeypress="notNull(this);" name="productName"
					readonly="readonly" value="${productInfo.productName}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>产品类型:</span></label></td>
						<td class="value" width="35%"><input class="inputxt"
					id="pruductType" onkeypress="notNull(this);" name="pruductType"
					readonly="readonly" value="${productInfo.pruductType}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
				<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>管理人:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="productManager" onkeypress="notNull(this);" name="productManager"
					readonly="readonly" value="${productInfo.productManager}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>托管人:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="productCustodian" onkeypress="notNull(this);" name="productCustodian"
					readonly="readonly" value="${productInfo.productCustodian}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>外包服务商:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="outsourcingServiceProvider" onkeypress="notNull(this);" name="outsourcingServiceProvider"
					readonly="readonly" value="${productInfo.outsourcingServiceProvider}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
					
					<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>发行规模:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="issuance" onkeypress="notNull(this);" name="issuance"
					readonly="readonly" value="${productInfo.issuance}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
					<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>产品期限:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="term" onkeypress="notNull(this);" name="term"
					readonly="readonly" value="${productInfo.term}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
					
					<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>订购起点:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="subscriptionSP" onkeypress="notNull(this);" name="subscriptionSP"
					readonly="readonly" value="${productInfo.subscriptionSP}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
					<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>业绩比较基准:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="benchmark" onkeypress="notNull(this);" name="benchmark"
					readonly="readonly" value="${productInfo.benchmark}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
					
					<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>分红方式:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="bonusType" onkeypress="notNull(this);" name="bonusType"
					readonly="readonly" value="${productInfo.bonusType}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
					<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>帐户名:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="accountName" onkeypress="notNull(this);" name="accountName"
					readonly="readonly" value="${productInfo.accountName}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
					<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>账号:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="account" onkeypress="notNull(this);" name="account"
					readonly="readonly" value="${productInfo.account}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
					<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>开户行:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="openingBank" onkeypress="notNull(this);" name="openingBank"
					readonly="readonly" value="${productInfo.openingBank}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
					
					<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>产品投向:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="productInto" onkeypress="notNull(this);" name="productInto"
					readonly="readonly" value="${productInfo.productInto}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
					<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>风险收益特征:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="riskYield" onkeypress="notNull(this);" name="riskYield"
					readonly="readonly" value="${productInfo.riskYield}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
					
					
					<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>产品费率:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="productRate" onkeypress="notNull(this);" name="productRate"
					readonly="readonly" value="${productInfo.productRate}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
					<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>股权质押:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="equityPledge" onkeypress="notNull(this);" name="equityPledge"
					readonly="readonly" value="${productInfo.equityPledge}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
					
					<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>对赌协议:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="vam" onkeypress="notNull(this);" name="vam"
					readonly="readonly" value="${productInfo.vam}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
					<tr>
				<td align="right" width="15%" nowrap><span style="color: red">*</span><label
					class="Validform_label"><span>补偿条款:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="compensationClause" onkeypress="notNull(this);" name="compensationClause"
					readonly="readonly" value="${productInfo.compensationClause}"
					datatype="s1-16" errormsg="请输入1-16位合法的字符" nullmsg="请输入工作单位！" /></td>
			</tr>
		</table>
	</form>
	<script>
		function emailtype() {
			var email = $("#email").val();
			var filter = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
			if (email == "" || filter.test(email)) {
				$(".tip").text("")
				$(".tip").addClass("Validform_right")
				$(".tip").removeClass("Validform_wrong")
				/* $("#email").after("<span class='Validform_checktip Validform_right tip'></span>") */
				return true;
			} else {
				$(".tip").text("请输入正确的邮箱格式!")
				$(".tip").addClass("Validform_wrong")
				$(".tip").removeClass("Validform_right")
				return false;
			}
		}

		function postcodetype() {
			var postcode = $("#postcode").val();
			var filter = /^[0-9]{6,6}$/;
			if (postcode == "" || filter.test(postcode)) {
				$(".postcodetip").text("")
				$(".postcodetip").addClass("Validform_right")
				$(".postcodetip").removeClass("Validform_wrong")
				/* $("#email").after("<span class='Validform_checktip Validform_right tip'></span>") */
				return true;
			} else {
				$(".postcodetip").text("请输入正确的邮编!")
				$(".postcodetip").addClass("Validform_wrong")
				$(".postcodetip").removeClass("Validform_right")
				return false;
			}
		}
	</script>
</body>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>产品信息</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="ProductInfoList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
			<div>
				<label>产品名称：</label> 
				<input type="text" id="productname" name="productname" />
			</div>

			<div>
				<label>产品类型：</label> 
				<input type="text" id="pruducttype" name="pruducttype" />
			</div>

			<div>
				<label>产品期限：</label> 
				<input type="text" id="deadline" name="deadline" />
			</div>
	</div>
	
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#ProductInfoList').datagrid({
			idField : 'pkId',
			title : '产品信息管理',
		    url : ' ${ctx}/productInfoController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:true,
			/* <!--查询参数 --> */
			queryParams:{
				'productname': $('#productname').val(),
				'pruducttype': $('#pruducttype').val(),
				'deadline': $('#deadline').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'productname',title:'产品名称',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'pruducttype',title:'产品类型',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'yield',title:'业绩比较基准',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'deadline',title:'投资期限',   width:fixWidth(0.11),align:'center',sortable:false},
					{field:'perminbalance',title:'起投金额',   width:fixWidth(0.11),align:'center',sortable:true},
					{field:'productthrowup',title:'产品投向',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'status',title:'产品状态',   width:fixWidth(0.12),align:'center',sortable:false,
						formatter:function(value){
							if(value==1){
								return "待发行";
							}else if(value==2){
								return "热销中";
							}else{
								return "发行结束";
							}
						}	
					},
					{field : 'opt',title : '操作',width:fixWidth(0.25),align:'center',sortable:false,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=update('"+rec.pkId+"','"+rec.productname+"') style='color:#3f99b1;'>";
							href += "编辑</a>]";
							href += "[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "详情</a>]";
							href += "[<a href='#' onclick=tofiles('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "文件管理</a>]"; ''
							href += "[<a href='#' onclick=location.href=('profitController.do?profitList=key&pkId='+'"+rec.pkId+"&name='+'"+rec.productname+"') style='color:#3f99b1;'>";
							href += "确认收益及付息</a>]";
							return href;
					}}
				    ]],
			onLoadSuccess : function(data) {
			    $("#ProductInfoList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清除所选的行
				$("#ProductInfoList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'ProductInfoList';
			}
		});
		$("#ProductInfoList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#ProductInfoList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});
//编辑页面
function update(pkId,productName){
	add_win(productName,'${ctx}/productFkController.do?updateProductInfo=key&pkId='+pkId,'ProductInfoList',700,450);
	
}
function reset(){
	$('#productname').val("");
	$('#pruducttype').val("");
	$('#deadline').val("");
}


//详情页面
function detail(pkId){
	add_win('产品详情','${ctx}/productInfoController.do?productInfo=key&pkId='+pkId,'ProductInfoList',1000,450);
	
}	

//附件管理页面
function tofiles(pkId){
	add_win('文件管理','${ctx}/productInfoController.do?tofiles=key&pkId='+pkId,'ProductInfoList',1000,500);
}	
//付息列表
/* function fuxi(url,pkId){
	url += '&pkId='+pkId;	
} */
//删除
function dele(url,id,pkId) {
//var rowsData = $('#'+id).datagrid('getSelections');
url += '&pkId='+pkId;
window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    if (r){
        $.post(url,function(result){
            if (result.success){
            	if(result.obj =='alert'){
            		tip(result.msg);
            	}else{
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
            	}
            } else {
                window.top.$.messager.show({// show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
        },'json');
    }
});
}

function reloadTable() {
	try {
		$('#' + ProductInfoList).datagrid('reload');
		$('#' + ProductInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + ProductInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#ProductInfoList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids;
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>文件管理</title>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
<style type="text/css">
#aaa {
	width: 1400px;
	height: 1200px;
	-webkyt-bok: display
}

input[type="file"] {
	color: transparent;
}

#preview {
	width: 71px;
	height: 99px;
	border: 1px solid #000;
	overflow: hidden;
	position: absolute;
	top: 0px;
	right: 20%;
	border: 1px solid #a5aeb6
}
#myform {
    padding-top: 17px;
}
#imghead {
	filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=image);
}
#demoListtb{
	height:60px;
}
#mainFile{
 width:60px;
}
</style>
</head>
<body style="overflow-y: scroll">
<div id="demoListtb">
	<form id="myform" name="myform" method="post" enctype="multipart/form-data">
		<input id="profileid" type="hidden" name="profileid" value="${productid }" /> 
		<input type="file" id="mainFile" onkeypress="notNull(this);" name="file" /><span style="color: red">*</span> 
		<input id="upFile" type="button" name="upFile" value="上传文件" onclick="uploadFile()" />
	</form>
</div>
	<table width="100%" id="productFileInfo" toolbar="#demoListtb"></table>
	<script>
		$(function() {
			init();
			query();
		});
		
		
		function updateIsAPP(id){
		    $("#myform").ajaxSubmit({  
		        type : 'POST', 
		        dataType: "json",
		        url : '${ctx}/productInfoController.do?appIsLook=key&fileid='+id,  
		        success : function(data) {  
		        	  query();
		        },  
		    });  
		}
		
		//删除
		function removeId(url, id,fileName) {
			url += '&id=' + id +'&fileName='+fileName;
			window.top.$.messager.confirm('Confirm', '你确定要删除吗', function(r) {
				if (r) {
					$.post(url, function(result) {
						if (result.obj=='ok') {
							 query();
						} else {
							window.top.$.messager.show({// show error message
								title : 'Error',
								msg : result.errorMsg
							});
						}
					}, 'json');
				}
			});
		}
		
		function uploadFile(){
			var imagePath = $("#mainFile").val();  
		    if (imagePath == "") {  
		        alert("请上传文件");  
		        return false;  
		    }  
		    $("#myform").ajaxSubmit({  
		        type : 'POST', 
		        dataType: "json",
		        url : '${ctx}/productInfoController.do?uploadFile=key&productId='+$('#profileid').val(),  
		        success : function(data) {  
		        	  $("#url").attr("src", data.path);
		        	  tip("上传成功");
		        	  query();
		        },  
		        error : function() {  
		            alert("上传失败，请检查网络后重试");  
		        }  
		    });  
		}
		
		function query(){
			$('#productFileInfo').datagrid({
				idField : 'pkId',
			    url : ' ${ctx}/productInfoController.do?queryFiles=key',
				fit : true,
				//loadMsg : '数据加载中...',
				pageSize : 10,
				pagination : true,
				pageList : [ 10, 20, 30 ],
				//sortName: 'id',
				sortOrder : 'asc',
				rownumbers : true,
				singleSelect : true,
				fitColumns : true,
				remoteSort:false,
				/* <!--查询参数 --> */
				queryParams:{
					'productId': $('#profileid').val()
				},
				columns:[[
						{field : 'ck',checkbox:true},
						{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
						{field:'productId',title:'产品主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
						{field:'fileName',title:'新文件名',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
						{field:'oldName',title:'文件名',  width:fixWidth(0.15),align:'center',sortable:false},
						{field:'visible',title:'APP可见',  width:fixWidth(0.15),align:'center',sortable:false,
							formatter:function(value){
								 if(value == '00'){
									 return '否';
								 }else{
									 return '是';
								 }
								}
								 },
						{field:'createDate',title:'上传时间',   width:fixWidth(0.15),align:'center',sortable:true,
							formatter:function(val,rec){
				        		if(val != null && val != ''){
				        			return formattimes(val);
				        		}
				        		  },	
						},
						{field : 'opt',title : '操作',width:fixWidth(0.2),align:'center',sortable:false,
							formatter : function(value, rec,index) {
								if (!rec.pkId) {
									return '';
								}
								var href = '';
								href += "[<a href='${ctx}/productInfoController.do?download=key&fileid="+rec.pkId+"' style='color:#3f99b1;'>下载</a>]";
								href += "[<a href='#' onclick=updateIsAPP('"+rec.pkId+"') style='color:#3f99b1;'>可见状态修改</a>]";
								href += "[<a href='#' onclick=removeId('${ctx}/productInfoController.do?delete=key','"+rec.pkId+"','"+rec.fileName+"') style='color:#3f99b1;'>删除</a>]";
								return href;
						}}
					    ]],
				onLoadSuccess : function(data) {
				    $("#productFileInfo").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
					//无查询结果提示
					if (data.total == 0) {
						$('.datagrid-view2 .datagrid-body')
								.append(
										'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

						$(this).closest('div.datagrid-wrap')
								.find('div.datagrid-pager')
								.hide();
					}
					//清除所选的行
					$("#productFileInfo").datagrid("clearSelections");
				},
				//rowIndex:行索引 rowData：行数据
				onClickRow : function(rowIndex, rowData) {
					rowid = rowData.id;
					gridname = 'productFileInfo';
				}
			});
			$("#productFileInfo").datagrid('getPager').pagination({
				beforePageText : '',
				afterPageText : '/{pages}',
				displayMsg : '显示{from}-{to}共{total}条',
				showPageList : true,
				showRefresh : true
			});
			
			$("#productFileInfo").datagrid('getPager').pagination({
				onBeforeRefresh : function(pageNumber, pageSize) {
					$(this).pagination('loading');
					$(this).pagination('loaded');
				}
			});
	};
	</script>
</body>
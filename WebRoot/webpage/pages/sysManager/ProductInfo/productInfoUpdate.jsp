<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  
  <script type="text/javascript">
$(function(){

	 init();
});
function checkAll(){
	var productId=$('#productId').val();
	var pkId = $('#pkId').val();
	saveData('${ctx}/productFkController.do?saveUpdate=key&pkId='+pkId+'&productId='+productId);
} 
</script>
 </head>
 <body style="overflow-y: auto" scroll="no">
 	<input id="pkId" name="pkId" value="${productFk.pkId}" type="hidden"/>
	<input id="productId" name="productId" value="${pkId}" type="hidden"/>
 	<form id="myform" name="myform" method="post" >
                                   
  	<table style=" width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	 	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>股权质押:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	      <textarea rows="4" cols="8" style="width:260px;" onkeypress="notNull(this);" name="compensationClause" class="compensationClause" id="compensationClause" 
    	        datatype="*10-300" errormsg="请输入10-300位内的字符"  nullmsg="请输入10-300位内容！">${productFk.compensationClause}</textarea><span style="color: red;">*</span>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>对赌协议:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	      <textarea rows="4" cols="8" style="width:260px;" onkeypress="notNull(this);" name="bettingAgreement" class="bettingAgreement" id="bettingAgreement" 
    	       datatype="*10-300" errormsg="请输入10-300位内的字符"  nullmsg="请输入10-300位内容！">${productFk.bettingAgreement}</textarea><span style="color: red;">*</span>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>补偿条款:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	      <textarea rows="4" cols="8" style="width:260px;" onkeypress="notNull(this);" name="sharePledge" class="sharePledge" id="sharePledge" 
    	      datatype="*10-300" errormsg="请输入10-300位内的字符"  nullmsg="请输入10-300位内容！">${productFk.sharePledge}</textarea><span style="color: red;">*</span>
    	  </td>
    	</tr>
    	<tr>
     		<td align="center" colspan="8">
        		<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
 
</body> 
<script type="text/javascript">

 </script>
  
<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>产品详情</title>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
<script type="text/javascript">
	$(function() {
		init();
	});
</script>

</head>
<body style="overflow-y: hidden" scroll="no">
	<table width="100%" id=repairInforAdd toolbar="#demoListtb"></table>
	<form id="myform" name="myform" method="post">
		<c:if test="${pkId!= null}">
			<input id="pkId" name="pkId" type="hidden" value="${pkId}">
		</c:if>
		<table style="width: 100%;" cellpadding="0" cellspacing="1" class="formtable">

			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 产品名称: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="getProductName" onkeypress="notNull(this);" name="getProductName" readonly="readonly" value="${getProductName}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 产品类型: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="getPruductType" onkeypress="notNull(this);" name="getPruductType" readonly="readonly" value="${getPruductType}" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 管理人: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="productmanager" onkeypress="notNull(this);" name="productmanager" readonly="readonly" value="${productmanager}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label">托管人: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="productcustodian" onkeypress="notNull(this);" name="productcustodian" readonly="readonly" value="${productcustodian}" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 外包服务商: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="outsourcingserviceprovider" onkeypress="notNull(this);" name="outsourcingserviceprovider" readonly="readonly" value="${outsourcingserviceprovider}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label">募集规模: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="scale" onkeypress="notNull(this);" name="scale" readonly="readonly" value="${scale}" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 存续期限: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="deadline" onkeypress="notNull(this);" name="deadline" readonly="readonly" value="${deadline}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 认购起点: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="perminbalance" onkeypress="notNull(this);" name="perminbalance" readonly="readonly" value="${perminbalance}" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label">业绩比较基准: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="yield" onkeypress="notNull(this);" name="yield" readonly="readonly" value="${yield}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 分红方式: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="bonustype" onkeypress="notNull(this);" name="bonustype" readonly="readonly" value="${bonustype}" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label">募集户名: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="raisename" onkeypress="notNull(this);" name="raisename" readonly="readonly" value="${raisename}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 募集银行: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="raisebank" onkeypress="notNull(this);" name="raisebank" readonly="readonly" value="${raisebank}" /></td>
			</tr>
			
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label">募集账号: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="raiseacco" onkeypress="notNull(this);" name="raiseacco" readonly="readonly" value="${raiseacco}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 产品投向: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="productthrowup" onkeypress="notNull(this);" name="productthrowup" readonly="readonly" value="${productthrowup}" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 风险收益特征: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="riskincome" onkeypress="notNull(this);" name="riskincome" readonly="readonly" value="${riskincome}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 产品费率: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="rate" onkeypress="notNull(this);" name="rate" readonly="readonly" value="${rate}" /></td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 产品实际到期时间: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="actualDate" onkeypress="notNull(this);" name="actualDate" readonly="readonly" value="${actualDate}" /></td>
				<td align="right" width="15%" nowrap><label class="Validform_label"> 产品默认到期时间: </label></td>
				<td class="value" width="35%"><input class="inputxt" id="defaultDate" onkeypress="notNull(this);" name="defaultDate" readonly="readonly" value="${defaultDate}" /></td>
			</tr>

		</table>
	</form>
</body>
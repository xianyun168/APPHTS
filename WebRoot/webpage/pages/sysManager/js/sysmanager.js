/**
 * 按id删除
 * @param url
 * @param id
 * @return
 */
function del(url,id) {
	var rowsData = $('#'+id).datagrid('getSelections');
	if (!rowsData || rowsData.length==0) {
		tip('请选择删除项目');
		return;
	}
	if (rowsData.length>1) {
		tip('请选择一条记录再删除');
		return;
	}
	url += '&pkId='+rowsData[0].pkId;
	window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
        if (r){
            $.post(url,function(result){
                if (result.success){
                	if(result.obj =='alert'){
                		tip(result.msg);
                	}else{
                        $('#'+id).datagrid('reload');    // reload the user data
                        $('#'+id).treegrid('reload');	 // reload the user data
                	}
                } else {
                    window.top.$.messager.show({// show error message
                        title: 'Error',
                        msg: result.errorMsg
                    });
                }
            },'json');
        }
    });
}
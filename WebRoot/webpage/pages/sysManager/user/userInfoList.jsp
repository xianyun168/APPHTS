<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="UserInfoList" toolbar="#demoListtb"></table>
<div id="demoListtb" >
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
		<div>
			<label class="jiekouname">用户名：</label>
			<input type="text" id="userCode"  name="userCode"  />
		</div>
		<div > 
			<label class="jiekouname">用户昵称：</label>
			<input type="text"   name="userName" id="userName" />
		</div>
	</div>
<div style="clear:both;padding-top:20px;">
	<div class="leftbtntool"  style="width:800px;margin:auto;">			
		<span class="bluebtnhover"><a onclick="add_win('用户增加',' ${ctx}/userInfoController.do?add=key','UserInfoList',710,330)" id="add" plain="true" class="formbtn bluebtn">用户增加</a></span> 
		<span class="bluebtnhover"><a href="javascript:void(0)" class="formbtn bluebtn" plain="true" onclick="update('用户编辑',' ${ctx}/userInfoController.do?modify=key','UserInfoList',710,330)" id="modify">用户编辑</a></span> 
		<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" plain="true" onclick="update('重置密码',' ${ctx}/userInfoController.do?resetPassword=key','UserInfoList',700,250)" id="resetPassword">重置密码</a></span>
		<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		<span class="orangebtnhover" ><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
		<span class="orangebtnhover" ><a onclick="removeId(' ${ctx}/userInfoController.do?delete=key','UserInfoList')"id="delete" plain="true" class="formbtn orangebtn">删除</a></span>
	</div>
</div>





</div>
<script type="text/javascript">
	function query(){
		$('#UserInfoList').datagrid({
			idField : 'pkId',
			title : '用户管理',
		    url : ' ${ctx}/userInfoController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				'userName': $('#userName').val(),
				'userCode': $('#userCode').val(),
				'status': $('#status').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'userCode',title:'用户名',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'userName',title:'用户昵称',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'email',title:'EMAIL地址',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'telephone',title:'电话',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'lastloginDate',title:'上次登录时间',  
						 formatter:function(val,rec){
				        		if(val != null && val != ''){
				        			return formattimed(val);
				        		}
				        		
				        		//return formattimes(val);
				        		  },
						 width:fixWidth(0.11),align:'center',sortable:true}
				    ]],
			onLoadSuccess : function(data) {
				$("#UserInfoList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清楚所选的行
				$("#UserInfoList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'userList';
			}
		});
		$("#UserInfoList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#UserInfoList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	$(".pagination .pagination-num").attr("readonly","readonly");
	query();
	
	noauto();
	
});
function removeId(url, id) {
	var rowsData = $('#' + id).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择删除项目');
		return;
	}
	if (rowsData.length > 1) {
		tip('请选择一条记录再删除');
		return;
	}
	url += '&id=' + rowsData[0].pkId;
	window.top.$.messager.confirm('Confirm', '你确定要删除吗', function(r) {
		if (r) {
			$.post(url, function(result) {
				if (result.success) {
					$('#' + id).datagrid('reload'); // reload the user data
					$('#' + id).treegrid('reload'); // reload the user data
				} else {
					window.top.$.messager.show({// show error message
						title : 'Error',
						msg : result.errorMsg
					});
				}
			}, 'json');
		}
	});
}

/* //删除
function removeId(url, id) {
	var rowsData = $('#' + id).datagrid('getSelections');
	if (!rowsData || rowsData.length == 0) {
		tip('请选择删除项目');
		return;
	}
	var ids = [];
	if (rowsData.length > 1) {
		    
			 var roleIds=getUserListSelections("pkId");
			  for(var i=0;i<rowsData.length;i++){
				 var id=rowsData[i].pkId;
			 	 ids.push(id);
			 }
			 }else{
			 ids=rowsData[0].pkId;}
	url += '&pkId=' + ids;
	window.top.$.messager.confirm('Confirm', '你确定要删除吗', function(r) {
		if (r) {
			$.post(url, function(result) {
				if (result.success) {
					query();
				} else {
					window.top.$.messager.show({// show error message
						title : 'Error',
						msg : result.errorMsg
					});
				}
			}, 'json');
		}
	});
					//	query();
} */
	function reset(){
		$('#userName').val("");
		$('#userCode').val("");
		$('#status').val("");
	}
	function reloadTable() {
		try {
			$('#' + UserInfoList).datagrid('reload');
			$('#' + UserInfoList).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + UserInfoList).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#UserInfoList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	};
	
		//页面初始化时，设置按钮的权限
	function noauto(){
		//取得对应的权限的值(后台返回的是没有的权限的值)
	    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
		var oper = operCode.split(', ');
		var i = oper.length;
		$(oper).each(function(i){
			$('#' + oper[i]).hide();
		})
	}

</script>
	</body>
</html>


		
			

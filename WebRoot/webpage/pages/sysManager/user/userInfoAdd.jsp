<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	if($('#depart').val()!="" && $('#depart').val()!=null){
		//读取所属部门
		$('#departs').val($('#depart').val());
		//alert($('#departs').val());
	}
	  init();
});

function chooseRole(){
		var url='${ctx}/userInfoController.do?sysPermissionInfoAdd=key';
	 	var width = width?width:700;
		var height = height?height:400;
		if(width=="100%" || height=="100%"){
			width = document.body.offsetWidth;
			height =document.body.offsetHeight-100;
		}
				$.dialog({
					zIndex: 1997,
					content: 'url:'+url,
					lock : true,
					width:width,
					height:height,
					title:"",
					opacity : 0.3,
					cache:false
				});

}
function checkAll(){
	$('#depart').val($('#departs').val());
	//alert($('#departs').val());
	//alert($('#depart').val());
	var areaCode=$('#areaCode').val();
	var rolename=$('#roleNames').val();
	var departs=$('#depart').val();
	if(departs==''){
		tip("请选择所属机构!");
		return;
	}
	
	if(rolename==''){
		tip("请选择所用戶角色!");
		return;
	}
	
	if(areaCode==''){
		tip("请选择所属地区!");
		return;
	}
	if($('#pkId').val()==""||$('#pkId').val()==null){
	 $.ajax({
             type: "post",
             url: "userInfoController.do?checkUserCode=key",
             data: {"userCode":$('#userCode').val()},
             dataType: "json",
             success: function(data){
                        if(data.obj=="ok"){
                        saveData('${ctx}/userInfoController.do?saveUpdate=key');
                        }else{
                        tip(data.msg);
                        return;
                        }
                      }
         });
        }else{
         saveData('${ctx}/userInfoController.do?saveUpdate=key');
        }
}
</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${userInfo.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${userInfo.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>用户名:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="userCode" onkeypress="notNull(this);" name="userCode" value="${userInfo.userCode}" datatype="s4-16" errormsg="请输入4-16位内的字符"  nullmsg="请输入4-16位用户名！"/><span style="color:red">*</span>
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>用户昵称:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="userName" onkeypress="notNull(this);" name="userName" value="${userInfo.userName}" datatype="s2-10" errormsg="请输入2-10位内的字符" nullmsg="请输入2-10位字符！"/><span style="color:red">*</span>
    	  </td>
    	</tr>
    	<tr>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>密码:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="password" onkeypress="notNull(this);" name="password" type="password" value="${userInfo.password}" datatype="/^[^&lt;&gt;]{6,16}$/"  errormsg="请输入6-16位内的字符"  nullmsg="请输入6-16位密码！"/><span style="color:red">*</span>
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>EMAIL地址:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="email" onkeypress="notNull(this);" name="email" value="${userInfo.email}" datatype="/^(?=\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$).{5,50}$/"  nullmsg="请输入5-50位有效的EMAIL地址！"/><span style="color:red">*</span>
    	  </td>
    	</tr>
		<tr>
			<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>角色:</span></label>
    		</td>
    		<td class="value" width="35%" >
    		 	<input class="inputxt" id="roleNames" name="roleNames" value="${roleNames}"  readonly="readonly" />
    			<input class="inputxt" id="roleIds"  name="roleIds" type="hidden" value="${roleIds}" /><span style="color:red">*</span>
    			<a href="javascript:chooseRole();">选择角色</a>
    		</td>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span> 所属机构:</span></label>
    		</td>
    		 <td class="value" width="35%" >
    			<input class="inputxt" id="depart" name="departPkId" type="hidden" value="${userInfo.departPkId}" nullmsg="请输入所属机构！！"/>
    			<select id="departs" name="departs">
    				<option value="">请选择</option>
    				<c:forEach items="${departs}" var="dps">
    				<option value="${dps.pkId}">${dps.departName}</option>
    				</c:forEach>
    			</select>
    			<span class="Validform_checktip"></span><span style="color:red">*</span>
    		 </td>
    	</tr>
        <tr>
			<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span> 所属地区:</span></label>
    		</td>
    		 <td class="value" width="35%"  >
    			<select id="areaCode" name="areaCode">
    				<option value="">请选择</option>
    				<c:forEach items="${esbmmCodes}" var="esbmmCodes">
    					<option value="${esbmmCodes.codeValue}"
    						<c:if test="${ esbmmCodes.codeValue eq userInfo.areaCode}">Selected</c:if>>
    						${esbmmCodes.chineseName}
    					</option>
    				</c:forEach>
    			</select><span style="color:red">*</span>
    		 </td>
    		 <td align="right" width="15%" nowrap>
    		 <label class="Validform_label"><span>电话:</span></label>
    		 </td>
    		 <td class="value" width="35%" >
    		<input class="inputxt" id="telephone" onkeypress="notNull(this);" name="telephone" value="${userInfo.telephone}" 
    		datatype="/^[0-9]{1,11}$/"  nullmsg="请输入电话号码！"/><span style="color:red">*</span>
    		 </td>
    	</tr>
     <tr>
     <td align="center" colspan="8">
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body> 
  
  
  
  

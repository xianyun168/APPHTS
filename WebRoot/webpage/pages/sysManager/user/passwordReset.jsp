<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	init();
});
//md5 密码加密函数
function checkAll(){

	var newPassword = $('#newPassword').val();
	var conformedPassword = $('#conformedPassword').val();
	
    if(newPassword != conformedPassword){
		alert('“新密码”与“确认密码”不一致。');
		return;
    }    
    saveData('${ctx}/loginController.do?updatePassword=key'); 
}
</script>
 </head>
<body style="overflow-y: hidden" scroll="no">
  <form id="myform" name="myform" method="post" > 
    <input id="pkId" name="pkId" type="hidden" value=${user.pkId }>
      <table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
    	<tr>
    		 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 旧密码:
    		 </label>
    		</td>
    		 <td class="value" width="35%" >
    			<input class="inputxt" id="remark" name="remark" onkeypress="notNull(this);" type="password" datatype="*6-16"  nullmsg="请输入6-16位密码！"/>
    		 <span style="color: red;">*</span>
    		 </td>
    	</tr>
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>新密码:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="newPassword" name="password" type="password" onkeypress="notNull(this);" datatype="*6-16"  nullmsg="请输入6-16位密码！"/><span style="color:red">*</span>
    	  </td>
    	</tr>
		<tr>  
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>确认密码:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="conformedPassword" name="conformedPassword" onkeypress="notNull(this);" type="password" datatype="*6-16"  nullmsg="请输入6-16位密码！"/><span style="color:red">*</span>
    	  </td>
    	</tr>
     <tr>
     <td align="center" colspan="8">
     <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
     </td>
     </tr>
   </table>
 </form>
</body> 
  
  
  
  

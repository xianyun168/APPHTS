<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
    $("#myform").Validform();//表单验证
	$(function(){
		  init();
			$.extend($.Datatype,{
				"z1-10" : /^[\u4E00-\u9FA5\uf900-\ufa2d]{1,10}$/
			});
	});
	
	$.Datatype.NotChina1_128= function(gets,obj,curform,regxp){
	if (!/[\u4e00-\u9fa5]/.test(gets)&&/^[\w\W]{1,100}$/.test(gets)){ 
		return true;
	}
	return "不能输入汉字且须少于100个字符";
};
	
function checkAll(){
	if($('#pkId').val()==""||$('#pkId').val()==null){
	 $.ajax({
             type: "post",
             url: "esbmmCodeTypeController.do?checkExit=key",
             data: {"typeName":$('#typeName').val(),"chineseName":$('#chineseName').val()},
             dataType: "json",
             success: function(data){
                        if(data.obj=="ok"){
                        saveData('${ctx}/esbmmCodeTypeController.do?saveUpdate=key');
                        }else{
                        tip(data.msg);
                        return;
                        }
                      }
         });
        }else{
        
        
        	 $.ajax({
             type: "post",
             url: "esbmmCodeTypeController.do?checkExit=key",
             data: {"typeName":$('#typeName').val(),"chineseName":$('#chineseName').val()},
             dataType: "json",
             success: function(data){
                        if(data.obj=="ok"){
                         saveData(' ${ctx}/esbmmCodeTypeController.do?saveUpdate=key');
                        }else{
                        tip(data.msg);
                        return;
                        }
                      }
         });
        
            
        }
}
 </script>
 </head>
<body style="overflow-y: hidden" scroll="no">
  <form id="myform" name="myform" method="post" >
  	<c:if test="${esbmmCodeType.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${esbmmCodeType.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
		<td align="right" width="15%" nowrap>
    		    <label class="Validform_label"><span>代码名称:</span></label>
    		 </td>
    		 <td class="value" width="35%" >
    			<input class="inputxt" id="chineseName" onkeypress="notNull(this);" name="chineseName" value="${esbmmCodeType.chineseName}" datatype="s1-30" errormsg="请输入1-30内字符" nullmsg="请输入1-30位代码名称！"/><span style="color:red">*</span>
    		 </td>
    		 <td align="right" width="15%" nowrap>
    		    <label class="Validform_label"><span>代码值:</span></label>
    		 </td>
    		 <td class="value" width="35%" >
    		 <input class="inputxt" id="typeName" onkeypress="notNull(this);" name="typeName" value="${esbmmCodeType.typeName}" datatype="s1-30" errormsg="请输入1-30位内字符" nullmsg="请输入1-30位代码类型值！"/><span style="color:red">*</span>
    		 </td>
    		 
    	</tr>
     	<tr>
     		<td align="center" colspan="8">
      			<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   			</td>
    	</tr>
     </table>
  </form>
</body> 
  
  
  
  

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
  $("#myform").Validform();//表单验证
$(function(){
	  init();
	  $.extend($.Datatype,{
				"z1-10" : /^[\u4E00-\u9FA5\uf900-\ufa2d]{1,10}$/
			});
});
	
function checkAll(){
	if($('#pkId').val()==""||$('#pkId').val()==null){
	 $.ajax({
             type: "post",
             url: "esbmmCodeController.do?checkExit=key",
             data: {"codeTypePkId":$('#codeTypePkId').val(),"codeValue":$('#codeValue').val(),"codeName":$('#codeName').val()},
             dataType: "json",
             success: function(data){
                        if(data.obj=="ok"){
                        saveData('${ctx}/esbmmCodeController.do?saveUpdate=key');
                        }else{
                        tip(data.msg);
                        return;
                        }
                      }
         });
        }else{
         saveData(' ${ctx}/esbmmCodeController.do?saveUpdate=key');
        }
}
 </script>
 </head>
 
<body style="overflow-y: hidden" scroll="no">
  <form id="myform" name="myform" method="post" >
  	<c:if test="${esbmmCode.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${esbmmCode.pkId}">
   </c:if>
   <table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	  <tr>
    	 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>代码编码:</span></label>
    	 </td>
    	 <td class="value" width="35%" >
    		  <input class="inputxt" id="codeCode" onkeypress="notNull(this);" name="codeCode" value="${esbmmCode.codeCode}" datatype="s1-30" errormsg="请输入1-30位内字符" nullmsg="请输入1-30位代码编码！"/><span style="color:red">*</span>
    	 </td>
    	 <td align="right" width="15%" nowrap>
    		 <label class="Validform_label"><span>代码英文名称:</span></label>
    	 </td>
    	 <td class="value" width="35%" >
    		 <input class="inputxt" id="codeName" onkeypress="notNull(this);" name="codeName" value="${esbmmCode.codeName}" datatype="en"  nullmsg="请输入1-100代码英文名称！"/><span style="color:red">*</span>

    	 </td>
      </tr>
	  <tr>
    	 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>代码中文名称:</span></label>
    	 </td>
    	 <td class="value" width="35%" >                                                                                                 <!-- ^[\u4e00-\u9fa5] 修改正则表达式将只输入中文改为可输入中文+数字+括号-->
    		  <input class="inputxt" id="chineseName" onkeypress="notNull(this);" name="chineseName" value="${esbmmCode.chineseName}" datatype="/[^(A-Za-z)\(\)]{1,16}$/" errormsg="请输入1-16位内中文" nullmsg="请输入1-16代码中文名称！"/><span style="color:red">*</span>
    	 </td>
    	 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>代码值:</span></label>
    	 </td>
    	 <td class="value" width="35%" colspan="3">
    		  <input class="inputxt" id="codeValue" onkeypress="notNull(this);" name="codeValue" value="${esbmmCode.codeValue}"  datatype="/^[^&lt;&gt;]{1,60}$/"  nullmsg="请输入1-60位代码值！"/><span style="color:red">*</span>
    	 </td>
    	 <td >
    		  <input class="inputxt" id="codeTypePkId" name="codeTypePkId" type="hidden" value="${esbmmCode.codeTypePkId}"  nullmsg="请输入代码类型主键！"/>
    	 </td>
       <tr>
	     <td align="center" colspan="8">
	        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
			<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
	     </td>
      </tr>
    </table>
  </form>
</body> 
  
  
  
  

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
		<style>
			.datagrid-btable tr,
			.datagrid-btable td,.datagrid-cell, .datagrid-cell-group, .datagrid-header-rownumber, .datagrid-cell-rownumber{
				height:41px !important;
				line-height:41px !important;
			}
			.datagrid-btable tr,
			.datagrid-btable td,.treegrid-tr-tree{
				height:0px !important;
			}
		</style>
	</head>
<body>
  <table width="100%" id="functionList" toolbar="#functionListtb"></table>
 	<div id="functionListtb" style="padding-top:20px;">
	 	<div class="leftbtntool" style="width:400px;margin:auto;">			
			<span class="bluebtnhover"><a onclick="addCodeType('代码类型增加','','functionList')" id="add" plain="true" class="formbtn bluebtn">代码类型增加</a></span>
			<span class="bluebtnhover"><a href="javascript:void(0)" class="formbtn bluebtn" plain="true" onclick="addCode('代码值增加','','functionList')" id="update">代码值增加</a></span>
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" plain="true" onclick="editCode('编辑','','functionList',700,400)" id="resetPassword">编辑</a></span>
		</div>
	</div>
<div id="system_function_functionList" class="easyui-layout" fit="true">
</div>
<script type="text/javascript">
	function query(){
		$('#functionList').treegrid(
						{
							idField : 'pkId',
							treeField : 'cnName',
							title : '数据字典',
							url : 'esbmmCodeTypeController.do?datagrid=key',
							fit : true,
							loadMsg : '数据加载中...',
							pageSize : 10,
							pagination : false,
							pageList : [ 10, 20, 30 ],
							sortOrder : 'asc',
							rownumbers : true,
							singleSelect : true,
							fitColumns : true,
							showFooter : true,
							frozenColumns : [ [] ],
							columns : [ [
								{field : 'ck',checkbox:true},
									{
										field : 'cnName',
										title : '代码名称',
										width : 100
									},
									{
										field : 'codeTypePkId',
										title : 'Id',
										hidden : true
									},
									{
										field : 'value',
										title : '代码值',
										width : 100
									},
									{
										field : 'null',
										title : '操作',
										width : 100,
										formatter : function(value, rec, index) {
											if (!rec.pkId) {
												return '';
											}
											var href = '';
											href += "[<a href='#' onclick=removeCode('','functionList')>";
											href += "删除</a>]";
											return href;
										}
									} ] ],
							onLoadSuccess : function(data) {
								$("#functionList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
								$("#functionList").treegrid("clearSelections");
							},
							onClickRow : function(rowData) {
								rowid = rowData.pkId;
								gridname = 'functionList';
							}
						});
		$('#functionList').treegrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		$('#functionList').treegrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
	};
	$(function(){
		//查询
		query();
	});
	
	function reloadTable() {
		try {
			$('#' + functionList).treegrid('reload');
		} catch (ex) {
		}
	}
	function reloadfunctionList() {
		$('#functionList').treegrid('reload');
	}
	function getfunctionListSelected(field) {
		return getSelected(field);
	}
	function getSelected(field) {
		var row = $('#' + gridname).treegrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	function getfunctionListSelections(field) {
		var ids = [];
		var rows = $('#functionList').treegrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	};
	function functionListsearch() {
		var queryParams = $('#functionList').datagrid('options').queryParams;
		$('#functionListtb').find('*').each(function() {
			queryParams[$(this).attr('name')] = $(this).val();
		});
		$('#functionList')
				.treegrid(
						{
							url : 'esbmmCodeTypeController.do?grid=key&field=id,functionName,TSIcon_iconPath,functionUrl,functionOrder,',
							pageNumber : 1
						});
	}
	function dosearch(params) {
		var jsonparams = $.parseJSON(params);
		$('#functionList')
				.treegrid(
						{
							url : 'esbmmCodeTypeController.do?functionGrid=key&field=id,functionName,TSIcon_iconPath,functionUrl,functionOrder,',
							queryParams : jsonparams
						});
	}
	function functionListsearchbox(value, name) {
		var queryParams = $('#functionList').datagrid('options').queryParams;
		queryParams[name] = value;
		queryParams.searchfield = name;
		$('#functionList').treegrid('reload');
	}
	$('#functionListsearchbox').searchbox({
		searcher : function(value, name) {
			functionListsearchbox(value, name);
		},
		menu : '#functionListmm',
		prompt : '请输入查询关键字'
	});
	function searchReset(name) {
		$("#" + name + "tb").find(":input").val("");
		functionListsearch();
	}
</script>


<script type="text/javascript">
$(function() {
	$(".pagination .pagination-num").attr("readonly","readonly");
	var li_east = 0;
});
function operationDetail(functionId)
{
	if(li_east == 0){
	   $('#system_function_functionList').layout('expand','east'); 
	}
	$('#operationDetailpanel').panel("refresh", "esbmmCodeTypeController.do?operation=key&functionId=" +functionId);
}
function addCode(title,url, id) {
	var rowsData = $('#'+id).datagrid('getSelections');
	if (!rowsData || rowsData.length==0) {
		tip('请选择字典类型');
		return;
	}
	if (rowsData.length>1) {
		tip('请选择一条字典类型');
		return;
	}
	url +='${ctx}/esbmmCodeController.do?add=key';
	url += '&codeTypePkId='+rowsData[0].pkId;
	add_win(title,url,'functionList',740,200);
}

function addCodeType(title,url, id) {
	url +='${ctx}/esbmmCodeTypeController.do?add=key';
	add_win(title,url,'functionList',700,110);
}

function editCode(title,url, id) {
	var rowsData = $('#'+id).datagrid('getSelections');
	if (!rowsData || rowsData.length==0) {
		tip('请选择编辑项目');
		return;
	}
	if (rowsData.length>1) {
		tip('请选择一条记录编辑');
		return;
	}
	if (rowsData[0].codeTypePkId==""||rowsData[0].codeTypePkId==null) {
		url +='${ctx}/esbmmCodeTypeController.do?modify=key';
		update(title,url,'functionList',700,110);
	}else{
		url +='${ctx}/esbmmCodeController.do?modify=key';
		update(title,url,'functionList',740,200);
	}
}

function removeCode(url,id) {
	var rowData = $('#'+id).datagrid('getSelections');
	if (!rowData || rowData.length==0) {
		tip('请选择删除项目');
		return;
	}
	if (rowData.length>1) {
		tip('请选择一条记录再删除');
		return;
	}
	if (rowData[0].codeTypePkId==""||rowData[0].codeTypePkId==null) {
		url +='${ctx}/esbmmCodeTypeController.do?delete=key';
		url += '&pkId='+rowData[0].pkId;
	}else{
		url +='${ctx}/esbmmCodeController.do?delete=key';
		url += '&pkId='+rowData[0].pkId;
	}
	
    window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    	
         if (r){
            $.post(url,function(result){
            	
                if (result.success){
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
                } else {
                    window.top.$.messager.show({    // show error message
                        title: 'Error',
                        msg: '含有子节点无法删除',
                    });
                }
            },'json');
        } 
    });
}
</script>
	</body>
</html>
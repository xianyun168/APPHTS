<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
	  var url = $("#imagesUrl").val();
		 if(url !=''){
			 backs();
		 }
});


function backs(){
	var imgurl =document.getElementById("imagesUrl").value;
	var addres = document.getElementById("address").value;
	var sin = imgurl.split(",");
	for(var i=0;i<sin.length;i++){
		if(sin[i] !=""){
			$("#imgDiv1").append('<img width="150px" height="150px" src='+addres+sin[i]+'>');
		}
		
	}
}
</script>

 </head>
 <body style="overflow-y: auto" scroll="yes">
 <table width="100%" id=repairInforAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${activityBasicInfor.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${activityBasicInfor.pkId}">
    </c:if>
  	<table style="width:100%;overflow-y: auto" cellpadding="0" cellspacing="1" class="formtable">
	
    	 <tr>
    	 	<td align="right" width="15%" nowrap>
    		  	<label class="Validform_label"><span>商品名称:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    		  	<input class="inputxt" id="goodsName" onkeypress="notNull(this);" readOnly="readOnly" name="goodsName" value="${appGoodsInfor.goodsName}" />
    	  	</td>
    	
    	  	<td align="right" width="15%" nowrap>
    		  	<label class="Validform_label"><span>积分价格:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    		  	<input class="inputxt" id="price" onkeypress="notNull(this);" readOnly="readOnly" name="price" value="${price}" />
    	  	</td>
       </tr>
    	
    	<tr>
    	  	<td align="right" width="15%" nowrap>
    			<label class="Validform_label"><span> 商品库存:</span></label>
    	  	</td>
   		  	<td class="value" width="35%"  >
   				<input class="inputxt" id="surplus" onkeypress="notNull(this);" readOnly="readOnly" name="surplus" value="${appGoodsInfor.surplus}" />
   		  	</td>
   		  	
   		  	<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>添加时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="createDate" value="${createDate }" readOnly="readOnly"
					name="timingSendDate"  class="Wdate" style="width: 260px;height:36px;"
					onkeypress="notNull(this);"></input>
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	 商品描述:
    		  </label>
    		</td>
    		<td class="value" width="35%">
	    		<textarea class="inputxt" id="describe" readonly="readonly" MaxLength="4000"
							onkeypress="notNull(this);" name="describe" cols="45" rows="5" 
							style="width: 260px; height: 100px;resize:none;">${appGoodsInfor.describe}</textarea>
			</td> 
    	</tr>
    	
    	<tr>
  		   	<td align="right" width="15%" nowrap>
    		  	<label class="Validform_label"><span>商品封面图片:</span></label>
    	   	</td>
    	   	<td class="value" width="35%" >
	    	  	<img  id="url" src="${appGoodsInfor.mainUrl}" width="150px" height="150px" readOnly="readOnly"> 
    	   	</td>
    	   	
    	   	   	<td align="right" width="15%" nowrap>
   		   		<label class="Validform_label"><span>商品展示大图:</span></label>
   		   	</td>
   		 	<td class="value" width="100%" >
   		 		<div id="imgDiv1"></div> 
   		   		<input id="imagesUrl" name="imagesUrl" value="${appGoodsInfor.imagesUrl}" type="hidden" />
   	  	 		<input id="address" name="address" value="${addre}" type="hidden" />
   	  	 	</td>
  			
<!--   			<td> -->
<!--     			<div id="imgDiv1"></div>  -->
<!--     		</td> -->
  		</tr>
    	
<!--     	<tr> -->
<!--   		   	<td align="right" width="15%" nowrap> -->
<!--    		   		<label class="Validform_label"><span>商品展示大图:</span></label> -->
<!--    		   	</td> -->
<!--    		 	<td class="value" width="35%" > -->
<%--    		   		<input id="imagesUrl" name="imagesUrl" value="${appGoodsInfor.imagesUrl}" type="hidden" /> --%>
<%--    	  	 		<input id="address" name="address" value="${addre}" type="hidden" /> --%>
<!--    	  	 	</td> -->
  			
<!--   			<td> -->
<!--     			<div id="imgDiv1"></div>  -->
<!--     		</td> -->
  		
<!--   		</tr> -->
    	
    	<tr>
     <td align="center" colspan="8">
       <!--  <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span> -->
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
    	
   </table>
 </form>
</body>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<%@page import="java.net.*"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
	 var url = $("#imagesUrl").val();
	 if(url !=''){
		 backs();
	 }
	
});

function saveData(url) {
	var form = $("#myform").Validform();
	var imagePath = $("#mainImageUrl").val();
    if (imagePath == "") {  
        alert("请上传图片");  
        return false;  
    } 
    var noContent = $("#imgDiv1").html();
    if(noContent == null || noContent.length == 0) 
    { 
        var imagePath1 = $("#imagesUrl").val();  
    	var obj = document.getElementById("imagesUrl1");
    	var length = obj.files.length;
    	if(parseInt(length)>parseInt(10)){
    		tip("上传图片过多");
    		return;
    	}
        if (parseInt(length) == parseInt(0)) {  
            alert("请上传图片");  
            return false;  
        }
    } 
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#saveBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}

function upGoods(url) {
	var form = $("#myform").Validform();
	var imagePath = $("#mainImageUrl").val();  
    if (imagePath == "") {  
        alert("请上传图片");  
        return false;  
    } 
    var noContent = $("#imgDiv1").html();
    if(noContent == null || noContent.length == 0) 
    { 
        var imagePath1 = $("#imagesUrl").val();  
    	var obj = document.getElementById("imagesUrl1");
    	var length = obj.files.length;
    	if(parseInt(length)>parseInt(10)){
    		tip("上传图片过多");
    		return;
    	}
        if (parseInt(length) == parseInt(0)) {  
            alert("请上传图片");  
            return false;  
        }
    }
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#upBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#upBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#upBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}


function uploadPhoto(){
	var imagePath = $("#mainImageUrl1").val(); 
    if (imagePath == "") {  
        alert("请上传图片");  
        return false;  
    }  
    var strExtension = imagePath.substr(imagePath.lastIndexOf('.') + 1);  
    if (strExtension != 'jpg' && strExtension != 'JPG' 
    	   && strExtension != 'gif' && strExtension != 'GIF'
    	   && strExtension != 'jpeg' && strExtension != 'JPEG'
           && strExtension != 'png'  && strExtension != 'PNG'
           && strExtension != 'bmp' && strExtension != 'BMP') {  
        alert("请上传jpg/gif/png/bmp/jpeg类型的图片");  
        return false;  
    }  
    $("#myform").ajaxSubmit({  
        type : 'POST', 
        dataType: "json",
        url : '${ctx}/appGoodsInforController.do?uploadPhoto=key',  
        success : function(data) {
        	  alert("上传成功");
        	  $("#url").attr("src", data.path);
        	  document.getElementById("mainImageUrl").value = data.path1;
        },  
        error : function() {  
            alert("上传失败，请检查网络后重试");  
        }  
    });  
}

function uploadPhotos(){
	var imagePath = $("#imagesUrl1").val();  
	var obj = document.getElementById("imagesUrl1");
	var length = obj.files.length;
	if(parseInt(length)>parseInt(10)){
		tip("上传图片过多");
		return;
	}
    if (parseInt(length) == parseInt(0)) {  
        alert("请上传图片");  
        return false;  
    }
    for(var i=0;i<obj.files.length;i++){
    	 var temp = obj.files[i].name;
    	 var strExtension = temp.substr(temp.lastIndexOf('.') + 1); 
    	 if (strExtension != 'jpg' && strExtension != 'JPG' 
      	   && strExtension != 'gif' && strExtension != 'GIF'
        	   && strExtension != 'jpeg' && strExtension != 'JPEG'
               && strExtension != 'png'  && strExtension != 'PNG'
               && strExtension != 'bmp' && strExtension != 'BMP') {  
		      alert("请上传jpg/gif/png/bmp/jpeg类型的图片");  
		      return false;  
		  }  
    	 
    }
    $("#myform").ajaxSubmit({  
        type : 'POST',  
        url : '${ctx}/appGoodsInforController.do?uploadPhotos=key',  
        success : function(data) {  
            alert("上传成功");
            $("#imgDiv1").empty();
           	var d = $.parseJSON(data);
           	var imagesUrl1 = d.msg;
           	var arr = new Array();
           	for(var i=0;i<d.obj.length;i++){
           		arr.push(d.obj[i]);
             	$("#imgDiv1").append('<img width="150px" height="150px" src='+d.obj[i]+'>');
           	}
           	document.getElementById("imagesUrl").value = imagesUrl1;
        },  
        error : function() {  
            alert("上传失败，请检查网络后重试");  
        }  
    });  
}

function backs(){
	var imgurl =document.getElementById("imagesUrl").value;
	var addres = document.getElementById("address").value;
	var sin = imgurl.split(",");
	for(var i=0;i<sin.length;i++){
		if(sin[i] !=""){
			$("#imgDiv1").append('<img width="150px" height="150px" src='+addres+sin[i]+'>');
		}
	}
}
</script>

 </head>
 <body style="overflow-y: auto" scroll="yes">
 	<form id="myform" name="myform" method="post" enctype="multipart/form-data">
  	<c:if test="${appGoodsInfor.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${appGoodsInfor.pkId}">
    </c:if>
<!--    <div style="width:1000px;height:700px;overflow-y:scroll"> -->
  	<table style="width:100%;overflow-y: auto" cellpadding="0" cellspacing="1" class="formtable" >
  	
  	   <tr>
    	 	<td align="right" width="15%" nowrap>
    		  	<label class="Validform_label"><span>商品名称:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    		  	<input class="inputxt" id="goodsName" onkeypress="notNull(this);" name="goodsName" value="${appGoodsInfor.goodsName}" datatype="s2-10" errormsg="请输入2-10位内的字符" nullmsg="请输入2-10位字符！"/><span style="color:red">*</span>
    	  	</td>
    	
    	  	<td align="right" width="15%" nowrap>
    		  	<label class="Validform_label"><span>积分价格:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    		  	<input class="inputxt" id="price" onkeypress="notNull(this);" name="price" value="${price}" datatype="number"  errormsg="请输入1-10位内的字符"  nullmsg="请输入1-10位数字！"/><span style="color:red">*</span>
    	  	</td>
       </tr>
       
		<tr>
    	  	<td align="right" width="15%" nowrap>
    			<label class="Validform_label"><span> 商品库存:</span></label>
    	  	</td>
   		  	<td class="value" width="35%"  >
   				<input class="inputxt" id="surplus" onkeypress="notNull(this);" name="surplus" value="${appGoodsInfor.surplus}" datatype="/^[0-9]{1,10}$/"   errormsg="请输入1-10位内的数字"  nullmsg="请输入1-10位数字！"/><span style="color:red">*</span>
   		  	</td>
   		  	
   		  	<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>添加时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="createDate" value="${createDate }"
					name="createDate"  class="Wdate" style="width: 260px;height:36px;"
					datatype="*1-50" nullmsg="请输入时间！" errormsg="请输入正确的时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd',minDate:'#F{$dp.$D(\'createDate\')||\'%y-%M-%d\'}'})"></input><span style="color: red;">*</span>
    		</td>
    	</tr>
    	
    	<tr>
   		 	<td align="right" width="15%" nowrap>
   		   		<label class="Validform_label"><span> 商品描述:</span></label>
   		 	</td>
   		 	<td class="value" width="35%">
	    		<textarea class="inputxt" id="describe" MaxLength="144"
							onkeypress="notNull(this);" errormsg="不能超过144个字符" nullmsg="请输入商品描述！"
							datatype="*1-144" name="describe" cols="45" rows="5" 
							style="width: 260px; height: 100px;resize:none;">${appGoodsInfor.describe}</textarea>
				<span style="color: red;">*</span>
			</td>
    	</tr>
    	<tr>
  		   	<td align="right" width="15%" nowrap>
    		  	<label class="Validform_label"><span>商品封面图片:</span></label>
    	   	</td>
    	   	<td class="value" width="35%" >
	    	  	<img  id="url" src="${appGoodsInfor.mainUrl}" width="150px" height="150px"> 
	    		<input type="file" id="mainImageUrl1" onkeypress="notNull(this);" name="file"/><span style="color:red">*</span>
	    		<input type="button" value="上传" id="upLoadImg" onclick="uploadPhoto()">
	    		<input id="mainImageUrl" name="mainImageUrl" value="${appGoodsInfor.mainImageUrl}" type="hidden" />
    	   	</td>
    	   	 	<td align="right" width="15%" nowrap>
   		   		<label class="Validform_label"><span>商品展示大图:</span></label>
   		   	</td>
   		 	<td class="value" width="100%" >
   		 		<div id="imgDiv1"></div> 
   		   		<input type="file" id="imagesUrl1" onkeypress="notNull(this);" name="file1" multiple="multiple"/><span style="color:red">*</span>
   		   		<input type="button" value="上传" id="upLoadImg" onclick="uploadPhotos()">
   		   		<input id="imagesUrl" name="imagesUrl" value="${appGoodsInfor.imagesUrl}" type="hidden" />
   		   		<input id="address" name="address" value="${addre}" type="hidden" />
   	  	 	</td>
  			
<!--   			<td> -->
<!--     			<div id="imgDiv1"></div>  -->
<!--     		</td> -->
  		</tr>
  		
  		<tr>
<!--   		   	<td align="right" width="15%" nowrap> -->
<!--    		   		<label class="Validform_label"><span>商品展示大图:</span></label> -->
<!--    		   	</td> -->
<!--    		 	<td class="value" width="95%" > -->
<!--    		 		<div id="imgDiv1"></div>  -->
<!--    		   		<input type="file" id="imagesUrl1" onkeypress="notNull(this);" name="file1" multiple="multiple"/><span style="color:red">*</span> -->
<!--    		   		<input type="button" value="上传" id="upLoadImg" onclick="uploadPhotos()"> -->
<%--    		   		<input id="imagesUrl" name="imagesUrl" value="${appGoodsInfor.imagesUrl}" type="hidden" /> --%>
<%--    		   		<input id="address" name="address" value="${addre}" type="hidden" /> --%>
<!--    	  	 	</td> -->
  			
<!--   			<td> -->
<!--     			<div id="imgDiv1"></div>  -->
<!--     		</td> -->
  		
  		</tr>
    	
    <tr>
     <td align="center" colspan="8">
     	<span class="bluebtnhover"><a  id="upBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="upGoods('${ctx}/appGoodsInforController.do?upGoods=key')">上架</a></span>
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/appGoodsInforController.do?saveUpdate=key')" >保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
<!--    </div> -->
 </form>
</body> 
  
  
  
  

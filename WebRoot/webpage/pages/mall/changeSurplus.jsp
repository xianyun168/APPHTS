<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<%@page import="java.net.*"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});

function saveData(url) {
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#saveBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}

</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post">
  	<c:if test="${appGoodsInfor.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${appGoodsInfor.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
    	  	<td align="right" width="15%" nowrap>
    			<label class="Validform_label"><span> 商品库存:</span></label>
    	  	</td>
   		  	<td class="value" width="35%"  >
   				<input class="inputxt" id="surplus" onkeypress="notNull(this);" name="surplus" value="${appGoodsInfor.surplus}" datatype="/^[0-9]{1,10}$/"   errormsg="请输入1-10位内的数字"  nullmsg="请输入1-10位数字！"/><span style="color:red">*</span>
   		  	</td>
    	</tr>
    <tr>
     <td align="center" colspan="8">
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/appGoodsInforController.do?change=key')" >保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body> 
  
  
  
  

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="AppGoodsInforList" toolbar="#demoListtb"></table>
<div id="demoListtb" >
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
		<div>
			<label>商品名称：</label>
			<input type="text" id="goodsName"  name="goodsName"  />
		</div>
		
		<div style="width: 66.66%;"> 
			<label style="width: 20%; float: left;">添加时间：</label>
	   		<input input style="width: 27.5%; float: left; margin-left: 1%;" type="text"
				id="createDate"  name="createDate" class="Wdate" onkeypress="notNull(this);"
				onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></input>
		</div>
		
		<div>
			<label class="jiekouname">商品状态：</label> 
			<select id ="flag" name="type"> 
    		 	<option  value="">--请选择--</option> 
   		 		<option>上架</option>
				<option>下架</option>
 	    	</select>  	
		</div>
		
	</div>

	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<span class="bluebtnhover"><a onclick="add_win('商品信息增加',' ${ctx}/appGoodsInforController.do?add=key','AppGoodsInforList',1000,500)" id="add" plain="true" class="formbtn bluebtn" >商品新增</a></span> 
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#AppGoodsInforList').datagrid({
			idField : 'pkId',
			title : '商品消息管理',
		    url : ' ${ctx}/appGoodsInforController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				'goodsName': $('#goodsName').val(),
				'createDate': $('#createDate').val(),
				'shelves': $('#flag').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'goodsName',title:'商品名称',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'price',title:'积分价格',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'surplus',title:'商品库存',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'shelves',title:'商品状态',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'createDate',title:'创建时间',  
						 formatter:function(val,rec){
				        		if(val != null && val != ''){
				        			return formattimed(val);
				        		}
				         },
						 width:fixWidth(0.15),align:'center',sortable:true},
					 {field : 'opt',title : '操作',width:fixWidth(0.2),align:'center',sortable:false,
							formatter : function(value, rec,index) {
								if (!rec.pkId) {
									return '';
								}
								var href = '';
								href += "[<a href='#' onclick=update('"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>";
								href += "编辑</a>]";
								href += "[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>";
								href += "详情</a>]";
								href += "[<a href='#' onclick=dele('appGoodsInforController.do?delete=key','AppGoodsInforList','"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>";
								href += "删除</a>]";
								href += "[<a href='#' onclick=up1('appGoodsInforController.do?upDown=key','AppGoodsInforList','"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>";
								href += "上架</a>]";
								href += "[<a href='#' onclick=down1('appGoodsInforController.do?upDown=key','AppGoodsInforList','"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>";
								href += "下架</a>]";
								href += "[<a href='#' onclick=changeSurplus('"+rec.pkId+"') style='color:#3f99b1;'>";
								href += "调整库存</a>]";
								var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
								var oper = operCode.split(', ');
								var i = oper.length;
								if(oper !=""){
								  for(var j=0;j<oper.length;j++){
									 if(oper[j] == "modify"){
										 href=href.replace("[<a href='#' onclick=update('"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>编辑</a>]","");
										}
									 if(oper[j] == "detail"){
										 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>详情</a>]","");
										}
									 if(oper[j] == "delete"){
										 href=href.replace("[<a href='#' onclick=dele('appGoodsInforController.do?delete=key','AppGoodsInforList','"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>删除</a>]","");
										}
									 if(oper[j] == "up"){
										 href=href.replace("[<a href='#' onclick=up1('appGoodsInforController.do?upDown=key','AppGoodsInforList','"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>上架</a>]","");
										}
									 if(oper[j] == "down"){
									 href=href.replace("[<a href='#' onclick=down1('appGoodsInforController.do?upDown=key','AppGoodsInforList','"+rec.pkId+"','"+rec.shelves+"') style='color:#3f99b1;'>下架</a>]","");
									    }
									 if(oper[j] == "changeSurplus"){
										 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>调整库存</a>]","");
										}
								
									}
								}
								return href;
							}
							}
				    ]],
			onLoadSuccess : function(data) {
				$("#AppGoodsInforList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清楚所选的行
				$("#AppGoodsInforList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'AppGoodsInforList';
			}
		});
		$("#AppGoodsInforList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#AppGoodsInforList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	noauto();
	
});

function reset(){
	$('#goodsName').val("");
	$('#createDate').val("");
	$('#flag').val("");
}

//编辑页面
function update(pkId,shelves){
	if(shelves=="已上架"){
		tip("已上架的商品不能再次编辑");
		return;
	}
	add_win('编辑', '${ctx}/appGoodsInforController.do?modify=key&pkId='+ pkId,'AppGoodsInforList',1000,500);
}

//详情页面
function detail(pkId){
	add_win('详情','${ctx}/appGoodsInforController.do?specific=key&pkId='+pkId,'AppGoodsInforList',1000,500);
}

//调整库存
function changeSurplus(pkId){
	add_win('调整库存','${ctx}/appGoodsInforController.do?changeSurplus=key&pkId='+pkId,'AppGoodsInforList',350,150);
}

//上架
function up1(url,id,pkId,shelves){
	url += '&pkId='+pkId;
	if(shelves=="上架"){
		tip("已上架的商品不能再次上架");
	}else if(shelves=="下架"){
		window.top.$.messager.confirm('Confirm','你确定要上架吗',function(r){
		    if (r){
		        $.post(url,function(result){
		            if (result.success){
		            	if(result.obj =='ok'){
		            		tip("上架成功");
		            		 $('#'+id).datagrid('reload');    // reload the user data
			                 $('#'+id).treegrid('reload');	 // reload the user data
		            	}
		            } else {
		                window.top.$.messager.show({// show error message
		                    title: 'Error',
		                    msg: result.errorMsg
		                });
		            }
		        },'json');
		    }
		});
	}
	return;
}

//下架
function down1(url,id,pkId,shelves){
	url += '&pkId='+pkId;
	if(shelves=="上架"){
		window.top.$.messager.confirm('Confirm','你确定下架吗',function(r){
		    if (r){
		        $.post(url,function(result){
		            if (result.success){
		            	if(result.obj =='ok'){
		            		tip("下架成功");
		            		$('#'+id).datagrid('reload');    // reload the user data
		                    $('#'+id).treegrid('reload');	 // reload the user data
		            	}
		            } else {
		                window.top.$.messager.show({// show error message
		                    title: 'Error',
		                    msg: result.errorMsg
		                });
		            }
		        },'json');
		    }
		});
	}else if(shelves=="下架"){
		tip("已下架的商品不能再次下架");
	}
	return;
}

//删除
function dele(url,id,pkId,shelves) {
//var rowsData = $('#'+id).datagrid('getSelections');
url += '&pkId='+pkId;
if(shelves=="已上架"){
	tip("已上架的商品不能删除");
	return;
}
window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    if (r){
        $.post(url,function(result){
            if (result.success){
            	if(result.obj =='alert'){
            		tip(result.msg);
            	}else{
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
            	}
            } else {
                window.top.$.messager.show({// show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
        },'json');
    }
});
}
	
function reloadTable() {
	try {
		$('#' + UserInfoList).datagrid('reload');
		$('#' + UserInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + UserInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#UserInfoList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="BatchAddList" toolbar="#demoListtb"></table>
<div id="demoListtb" >
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
		
			<div style="width: 66.66%;">
				<label style="width: 20%; float: left;">姓名：</label> 
				<input style="width: 27.5%; float: left; margin-left: 2%;" type="text"
					id="userName"  name="userName"  ></input>
			</div>
	</div>
<div style="clear:both;padding-top:20px;">
	<div class="leftbtntool"  style="width:450px;margin:auto;">			
		<span class="bluebtnhover"><a onclick="add_win('积分添加',' ${ctx}/batchAddController.do?batchAdd=key','BatchAddList',500,100)" id="add" plain="true" class="formbtn bluebtn">积分增加</a></span> 
		
		<span class="orangebtnhover" ><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
		<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
	</div>
</div>





</div>
<script type="text/javascript">
	function query(){
		$('#BatchAddList').datagrid({
			idField : 'pkId',
			title : '积分管理',
		    url : ' ${ctx}/batchAddController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				'userName': $('#userName').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'userName',title:'用户名称',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'idcardNumber',title:'身份证号码',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'integral',title:'积分数量',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'phoneNumber',title:'手机号码',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'createDate',title:'上次登录时间',  
						 formatter:function(val,rec){
				        		if(val != null && val != ''){
				        			return formattimed(val);
				        		}
				        		
				        		//return formattimes(val);
				        		  },
						 width:fixWidth(0.11),align:'center',sortable:true},
					{field : 'opt',title : '操作',width:fixWidth(0.11),align:'center',sortable:true,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=addOne('用户积分增加','batchAddController.do?toAddOne=key','BatchAddList','"+rec.pkId+"',710,400) style='color:#3f99b1;'>";
							href += "增加</a>]";
							href += "[<a href='#' onclick=update('用户积分编辑','batchAddController.do?modify=key','BatchAddList','"+rec.pkId+"',710,330) style='color:#3f99b1;'>";
							href += "编辑</a>]";
							href += "[<a href='#' onclick=dele('batchAddController.do?delete=key','BatchAddList','"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "清除积分</a>]";
							var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
							var oper = operCode.split(', ');
							var i = oper.length;
							if(oper !=""){
								for(var j=0;j<oper.length;j++){
									if(oper[j] == "detail"){
										 href=href.replace("[<a href='#' onclick=addOne('用户积分增加','batchAddController.do?toAddOne=key','BatchAddList','"+rec.pkId+"',710,400) style='color:#3f99b1;'>增加</a>]","");
									}
									if(oper[j] == "modify"){
										href=href.replace("[<a href='#' onclick=update('用户积分编辑','batchAddController.do?modify=key','BatchAddList','"+rec.pkId+"',710,330) style='color:#3f99b1;'>编辑</a>]","");
									}
									if(oper[j] == "delete"){
										href=href.replace("[<a href='#' onclick=dele('batchAddController.do?delete=key','BatchAddList','"+rec.pkId+"') style='color:#3f99b1;'>清除积分</a>]","");
									}  
								}
							}
							return href;
						}} 
				    ]],
			onLoadSuccess : function(data) {
				$("#BatchAddList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清楚所选的行
				$("#BatchAddList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'BatchAddList';
			}
		});
		$("#BatchAddList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#BatchAddList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	$(".pagination .pagination-num").attr("readonly","readonly");
	query();
	
	noauto();
	
});

//单个用户积分增加
function addOne(title, addurl, gname, pkId, width, height) {
	gridname = gname;
	addurl += '&pkId=' + pkId;
	width = width ? width : 700;
	height = height ? height : 400;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		content : 'url:' + addurl,
		//lock : true,
		resizable:false,
		maximizable:false,
		width : width,
		height : height,
		title : title,
		opacity : 0.3
		//cache : false,
		//modal: true
	});
}
//修改用户积分
function update(title, addurl, gname, pkId, width, height){
	gridname = gname;
	addurl += '&pkId=' + pkId;
	width = width ? width : 700;
	height = height ? height : 400;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		content : 'url:' + addurl,
		//lock : true,
		resizable:false,
		maximizable:false,
		width : width,
		height : height,
		title : title,
		opacity : 0.3
		//cache : false,
		//modal: true
	});
}

function dele(url, gname, pkId) {
	url += '&pkId=' + pkId;
	window.top.$.messager.confirm('Confirm', '你确定要清空吗', function(r) {
		if (r) {
			$.post(url, function(result) {
				if (result.success) {
					if (result.obj == 'ok') {
						tip(result.msg);
						window.location.href="batchAddController.do?batchAddList=key";
					} else {
						$('#' + gname).datagrid('reload'); // reload the user data
						$('#' + gname).treegrid('reload'); // reload the user data
					}
				} else {
					window.top.$.messager.show({// show error message
						title : 'Error',
						msg : result.errorMsg
					});
				}
			}, 'json');
		}
	});
}

	function reset(){
		$('#userName').val("");
	}
	function reloadTable() {
		try {
			$('#' + BatchAddList).datagrid('reload');
			$('#' + BatchAddList).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + BatchAddList).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#BatchAddList').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	};
	
		//页面初始化时，设置按钮的权限
	function noauto(){
		//取得对应的权限的值(后台返回的是没有的权限的值)
	    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
		var oper = operCode.split(', ');
		var i = oper.length;
		$(oper).each(function(i){
			$('#' + oper[i]).hide();
		})
	}

</script>
	</body>
</html>


		
			

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN"><head>
	<title id="title">57优鲜 大叶生菜 </title>
	
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="email=no">
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="X-UA-Compatible" content="IE=10.000">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.ico">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/iconfont2.css">
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css">


	
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>



	
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/modal.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/slider-common.css">

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.touchSlider.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.tabs.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.lazyload.min.js"></script>

<script type="text/javascript" src="${ctx}/common/js/jquery/common.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/json2.js"></script>
	
	<script src="${ctx}/common/js/jquery/dropload.js"></script>
	<script type="text/javascript" src="${ctx}/common/js/jquery/my.js"></script>
	<script src="${ctx}/common/js/jquery/jweixin-1.2.0.js"></script>
	
    <link type="text/javascript" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css">
    <link type="text/javascript" rel="stylesheet" href="${ctx}/common/js/jquery/css/dropload.css">

	<style type="text/css">
		/* box */
		.box{background:#fff;}
		.tab_menu{overflow:hidden; background:#d6d6d6;}
		.tab_menu li{width:33%;float:left;height:30px;line-height:30px;color:#3f3f3f;background:#d6d6d6;text-align:center;cursor:pointer; padding:10px 0; font-size:16px;}
		.tab_menu li.current{color:#333; background:url(${ctx}/common/js/jquery/imgs_init/tab_bj_03.jpg) repeat-x bottom;}
		.tab_menu li a{color:#fff;text-decoration:none;}
		.tab_menu li.current a{color:#333;}
		.tab_box{width:100%; background:#efefef;}
		.tab_box img{width:100%;}
		.tab_box li{height:24px;line-height:24px;overflow:hidden;}
		.tab_box li span{margin:0 5px 0 0;font-family:"宋体";font-size:12px;font-weight:400;color:#ddd;}
		.tab_box .hide{display:none;}

.tab_menu{overflow:hidden; background:#efefef;}
.tab_menu li{width:33%;float:left;height:30px;line-height:30px;color:#3f3f3f;text-align:center;cursor:pointer; padding:10px 0; font-size:16px;}
.tab_menu li.current{color:#333; background:url(${ctx}/common/js/jquery/imgs_init/tab_bj_03.jpg) repeat-x bottom;}
.tab_menu li a{color:#fff;text-decoration:none;}
.tab_menu li.current a{color:#333;}
.tab_box{ width:100%; background:#efefef;}
.tab_box img{width:100%;}
.tab_box li{height:24px;line-height:24px;overflow:hidden;}
.tab_box li span{margin:0 5px 0 0;font-family:"宋体";font-size:12px;font-weight:400;color:#ddd;}
.tab_box .hide{display:none;}
	</style>
	<!--tab样式-->
	<!--tabjs-->
	<script type="text/javascript">
	$(function(){
		details();
	})

	function details(){
		var productId = $("#productId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?goodsDetailsData=key",
    		type:"post",
    		async: false,
    		datatype:"josn",
    		data:{productId:productId},
    		success: function (data) {
    			console.log(data)
    			var data=$.parseJSON(data);
    			$("#goodImg ul").prepend("<li style=' background-color:  #fff;'><img id='logoImg' style='height: 300px;width: 100%;' src='"+data.obj.mainImageUrl+"'><div style='padding:  10px;'><p class='' style='width:100%;    color: #888888; padding-top:10px;padding-bottom:5px; font-size:12px;'><font style='font-size: 18px; font-weight: 600; color:#333;'>产品名称："+data.obj.goodsName+"</font></p><p class=' ' style='width:100%; font-size:12px;'><font style='font-size: 14px; font-weight: 600; color:#ae8f46;'>产品价格："+data.obj.price+" 积分</font></p><p class=' ' style='width:100%;padding-bottom:5px; font-size:12px;    color: #888888;'>产品库存：<font style='font-size: 12px; color:#999;'>"+data.obj.surplus+"</font></p><p class=' ' style='width:97%;    color: #888888;padding-bottom:5px; font-size:12px;'>产品描述：<font style='font-size: 12px; color:#999;'>"+data.obj.describe+"</font></p></div></li>");
    			//var span = '<span class=" " style="width:100%; float:none; font-size:12px;">积分：<font style="font-size: 14px; font-weight: 600; color:#ff0000;">'+3.00+'</font></span>';
    			//$("#priceInfo").append("<p class=' ' style='width:100%; float:none; font-size:12px;'>积分：<font style='font-size: 14px; font-weight: 600; color:#ff0000;'>"+data.obj.price+"</font></p>");
    			var str = new Array();
    			str = data.obj.imagesUrl.split(",");
    			for(var i=0;i<str.length;i++){
    				if(str[i] != ""){
    					$("#box_1").append("<img src='"+str[i]+"' alt=''>");
    				}
    			
    			}
    		}
		})
	}
	
	function addCar(){
		var span = $("#sp").text();
		var userId = $("#userId").val();
		var productId = $("#productId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?addCarList=key",
    		type:"post",
    		async: false,
    		datatype:"josn",
    		data:{productId:productId,userId:userId,span:span},
    		success: function (data) {
    			var data=$.parseJSON(data);
    			if(data.success){
    			queryByCarList(userId);
    			}
    		}
		})
	}
	
	
	function queryByCarList(userId){
		window.location.href = '${ctx}/orderInforController.do?shoppingCarList=key&userId='+userId;
	}
	
	
	</script>
	<style>
		.yjfhr{color:red;background-color: #FF0000;width: 60px;border-radius: 2px;color: #FFFFFF;display: inline-block; line-height: 18px;padding: 4px 7px;text-align: center;}
		.yjfhr2{color:red;background-color: #3C89FF;width: 60px;border-radius: 2px;color: #FFFFFF;display: inline-block; line-height: 18px;padding: 4px 7px;text-align: center;}
		.ggText{text-decoration:none;width:auto; white-space: nowrap; display:block; float:left;height:25px;line-height:25px; padding:0 10px; text-align:center; position:relative;cursor: pointer;}
		.paraTitle{wdith: 973px;height: 25px;text-align: center;background: #F5FAFE;display: block;line-height: 25px;font-size: 14px;font-weight: bold;border-top: 1px #ccc solid;}
		.parameterInfoTr{height: 25px;}
		.paraTh{border-right: 1px solid #ccc;background: #F5FAFE;padding-right: 5px;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;}
		.paraTd{padding-left: 5px;background: #fff;border-top: 1px solid #ccc;border-bottom: 1px solid #ccc;}
		#yinProduct_1{background: #fff;padding-top: 15px;}
		#askProduct_1{background: #fff;padding-top: 15px;}
		.discProd{width: 220px;white-space: nowrap;text-overflow: ellipsis;-o-text-overflow: ellipsis;overflow: hidden;}
		.shpinxiq_left_detail{width: 360px;height: auto;float: left;}
		.yinDefault{border: 1px solid #ccc;padding: 0px 6px 1px 6px;margin: 0 5px 5px 0;font-size:12px;}
		.yinBor{border: 1px solid #f00 ;}
		.hide{display: none;}
		.tuweizi{width: 1002px;height: auto;overflow: hidden;}
		.tuweizi p{width: 619px;height: auto;line-height: 24px;margin: 0 auto;}
		.tuweizi ul li{width: 292px;float: left;overflow: hidden;height: 27px;margin-left: 10px;}
		.tuweizi ul li span.nameYin{text-align: left;}
		.tuweizi ul li span{float: left;display: block;}
		.curr_base{border: 2px solid #ccc;padding: 1px;}
		.cur_on{border: 2px solid #edbec9;}
		#pd_lists {width:100%;position: relative;overflow-x: auto;overflow-y: hidden;}
		body{font-family: 微软雅黑;}
		.yinBor_i{background:url(${ctx}/common/js/jquery/imgs_init/gouY.png)no-repeat;   position:absolute; width:15px; height:15px; z-index: 9; top:35px; right:0;}
		.yinBor_i2{background:url(${ctx}/common/js/jquery/imgs_init/gouY.png)no-repeat;   position:absolute; width:15px; height:15px; z-index: 9; top:14px; right:0;}
		.hyds_jr{ display: block; width:226px; height:36px; border:3px #d01743 solid; background: #ed3164; line-height:36px; text-align: center; color:#fff; font-size:17px; float:left; margin-right:20px; text-decoration: none;}


	.wxw-shuoming {top:22px; right: 15%; text-align: right;}
	.wxw-shuoming >p{line-height: 22px;}
	@media screen and (max-width:480px) {
		.wxw-shuoming >p{ font-size: 12px; }
		.wxw-shuoming { right: 29%; }
	}
	@media screen and (min-width:375px) and (max-width:400px){
		.wxw-shuoming >p{ font-size: 12px; }
		.wxw-shuoming { right: 29%; }
	}
	@media screen and (min-width:320px) and (max-width:374px) {
		.wxw-shuoming >p{ font-size: 12px; }
		.wxw-shuoming { right: 29%; }
	}
	.lines{
	letter-spacing:-1px;
	height:3px;
	border-top:3px solid #021;
	border:none;
	
	}
	</style>
</head>

<body>
<input name="productId" value="${productId}" id="productId" type="hidden">
<!-- <input name="productInfo.shopInfoId" value="1" id="shopInfoId" type="hidden"> -->
<!-- <input name="productInfo.sku" value="1A00212" id="sku" type="hidden"> -->
<!-- <input id="goods" value="181" type="hidden"> -->

<!--产品详情main开始-->
<div class="main" style="overflow-x:hidden;">
<!-- <input type="button" value="测试" onclick="details();"> -->
<input type="hidden" value="${userId}" id="userId" name="userId"> 
	<!-- 轮播图 -->
	<div class="main_visual" style="height: auto;">
		<div class="flicking_con">
			<div class="flicking_inner" style="">
				
			</div>
		</div>
		<div class="" id="goodImg">
			<ul style="width:100%;">
				<li style="background-color:#FFF;padding: 15px 10px;border-top:10px solid #f8f8f8;border-bottom:10px solid #f8f8f8;color:#333;font-size:14px;">
					<span style="height:  22px;line-height:  22px;">	兑换数量</span>
					<span style="float: right;">
					<span  class="add iconfont icon-jia" style=" font-size: 20px;line-height: 24px;"> 	</span>
					<span style="    padding: 0 15px;" class="num" id="sp"> 	1</span>
					<span class="reduce iconfont icon-jian1" style='font-size: 20px;line-height: 24px;'></span>
					<!-- <img id="logoImg" src="http://www.cunetong.cn/images/cyt/shop/image/product/20171113/2017111313590003229297.jpg"> -->
					</span>
				</li>
			</ul>
		</div>
		 <div class="p_mod_h">
	    <!--商品详情-->
	        <div class="box demo2">
				<div class="tab_box">
					<div class="lines">
						<!-- <hr align="center" width=500 color=#21 size=3></hr> -->
					</div>
					<div style="padding:15px; background-color: #fff;    text-align: center;color:#101010;">--商品详情--</div>
					<div id="box_1" style="background: #fff;">
					  <!-- <img src="http://www.cunetong.cn/images/cyt/kefile/shop/image/20170928/20170928141200047_916.jpg" alt=""><img src="http://www.cunetong.cn/images/cyt/kefile/shop/image/20171114/20171114194200000_728.jpg" alt=""> -->
			        </div>
		        </div>
	        </div>
		</div>
	</div>
	
	<div style="width:100%; height:auto;  position: fixed; top:0; z-index:9999;">
		<img src="${ctx}/common/js/jquery/imgs_init/fh_h.png" style="width:40px; height:40px; padding:2% 0 2% 10px;" onclick="javascript:history.back();">
			<span style="float:right; width:auto; padding:2% 10px 2% 0px; display: none;">
				<a href="/phone/shopInfo/phoneGotoShopInfoPage.action?shopInfoId=1&amp;orderBy=normal">
					<img src="${ctx}/common/js/jquery/imgs_init/sp_h.png" style="width:40px; height:40px;">
				</a>
			</span>
	</div>
	
       
		<!--商品推荐end--！>
	<!-- 购买收藏按钮 -->
	

	<div class="bottom_margin" style="margin-bottom: 60px;"></div>
	<div class="spxq_dibu">
		
			<!-- 关注商品 -->
			<!-- <a href="javascript:void(0);" onclick="javascript:saveCustomerCollectProduct();" style="width:40%;background:url('http://www.cunetong.cn/resource/phone_front/phone/img/xin.png') center 6px no-repeat; padding-top:1.8em; line-height:2.2em;">
				关注
			</a> -->
			<!-- 进入购物车 -->
			<a href="javascript:addCar();"   style="width:50%;color:#fff;background-color:#caab71;">
			立即兑换
			</a>
			<!-- 加入购物车 -->
			<!-- <a href="javascript:void(0);" style="width:0.2%;background-color: #ccc;">&nbsp;</a> -->
			<a href="javascript:addCar();" class="addProToShopCart btn_common" style="width:50%;color:#fff;">加入购物车</a>
		
              
		
	</div>
<%-- 	<a href="${ctx}/orderInforController.do?sendGoodsList=key&userId=${userId}" style="display:block;width:50px;position:fixed;bottom:20%;left:0;z-index:9999;"><img src="${ctx}/common/js/jquery/imgs_init/fanhui.png" style="width:100%;"></a> --%>

<script>
var n=1
$(".add").click(function(){
	//alert()
	n++
	$('.num').html(n)
})
/* var m=1 */
$(".reduce").click(function(){
	//alert()
	n--
	if(n<=0){
		n=1
	}
	$('.num').html(n)
	
})
</script>

</body>
</html>
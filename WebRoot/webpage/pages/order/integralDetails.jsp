<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="zh-CN" >
<head>
<title>分类列表页面</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="email=no">
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="X-UA-Compatible" content="IE=10.000">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.ico">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/iconfont.css">
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css">



<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>



<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/m_category.css">
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/m_gwc.css">
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/m_global_site_base.css">

</head>

<body style="">
	<div>
	<!-- 隐藏的用户id -->
	<input type="hidden" value="${userId}" id="userId" name="userId">
		<div class="new-header bg_common">
		 <A class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></A>							 
		 <h2>积分明细</h2>
		</div>
	</div>
	
	<div id="container">
		<div class="touchweb_page-goodsCategory">
			<div class="tab_wrap">
				<input class="typeIndex" value="2124" type="hidden">
				<!-- *********************上下分开******************** -->
				<div class="tab_box" data-tpa="H5_PROD_CATEGORY_RIGHT">
					<div class="list cur">
					<div class="list_box sec_2124">
					<div class=" integral">
									<%-- <img alt="" class="bg" src="${ctx}/common/js/jquery/imgs_init/bg.png"> --%>
									<div class="con" style="position: relative;z-index: 1;text-align: center;">
										<div style="padding-top:10px;padding-bottom: 10px;">
											<img id="levelImg" alt="" src="${ctx}/common/js/jquery/imgs_init/one.png" style="width: 19%;">
											<p style="font-size: 12px; color: #78484f;padding-top: 5px;"> <span id="level"></span>星用户 </p>
											<p style="font-size: 24px;font-weight: bold; color: #78484f;padding-top: 12px;" id="integra">  </p>
											<p style="font-size: 10px; color: #ae8b6f;padding-top: 4px;"> 我的积分 </p>
										</div>
										<div style="width: 100%; text-align: center;">
											<ul>
												<li style="float: left;width: 43%;padding: 3%; border-top: 1px solid #e6d0a5;border-right: 1px solid #e6d0a5;"><a href="${ctx}/orderInforController.do?IntegralRules=key&title=01" style="color:#78482f;">积分计算规则</a></li>
												<li style="float: left;width: 43%;padding: 3%; border-top: 1px solid #e6d0a5;"><a href="${ctx}/orderInforController.do?returnRules=key&title=02" style="color:#78482f;">退换货规则提示</a></li>
											</ul>
										</div>
										<div style="    clear: both;"></div>
									</div>
									
										
									</div>
								</div>
								
							
								<div class="content" style="padding-top:10px;">
						<div class="block_title_bg2">
							<ul>
								<li class="btn btn_xz" id="leftTitleText"  onclick="switchTab(this);">全部</li>
								<li class="btn" id="centerTitleText"  onclick="switchTab(this);">获取记录</li>
								<li class="btn" id="rightTitleText"  onclick="switchTab(this);">消耗记录</li>
							</ul>
							<!-- <input type="button" class="btn btn_xz" id="leftTitleText" onclick="switchTab(this);" value="全部" /> 
							<input type="button" class="btn" id="centerTitleText" onclick="switchTab(this);" value="获取记录" />
							<input type="button" class="btn " id="rightTitleText" onclick="switchTab(this);" value="消耗记录" /> -->
						</div>
						<input id="userId" name="userId" type="hidden" value="${userId}">
						<div align="center">
							<label class="Validform_label"><span id="txt"
								style="color: red;"></span></label>
						</div>
						<div class="box" id="downinfo">
							<div class="mytitle"></div>
							<ul class="lis" id="tabs">
<!-- 								<li> -->
<!-- 									<p class="tit">gbdwakjsnjakhn</p> -->
<!-- 									<p class="int"><span style="font-size: 12px;">获取</span>积分：<span style="font-size: 16px;color: #c8a65a;">+99</span></p> -->
<!-- 									<p class="time">时间：<span>2018-0-2</span></p> -->
<!-- 								</li> -->
							</ul>
<!-- 							<table cellpadding="0" cellspacing="0" class="formtable" id="tab"> -->
<!-- 								<thead> -->
<!-- 									<tr> -->
<!-- 										<td width="50" class="tdfirst">序号</td> -->
<!-- 										<td align="center">名称</td> -->
<!-- 										<td align="center">积分数量</td> -->
<!-- 										<td align="center">积分状态</td> -->
<!-- 									</tr> -->
<!-- 								</thead> -->
<!-- 							</table> -->
						</div>
					</div>
							
							
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--底部浮层begin-->

<script type="text/javascript">
	$(function(){
		userInfo();
		queryAll();
	});	
	function queryAll(){
		var user = $("#userId").val();
		 $.ajax({
			  url:"${ctx}/orderInforController.do?queryIntegralDetails=key",
	            dataType:"json",
	            type:"post",
	            data:{"userId":user},
	            success:function(data){
	            	var list = data.obj;
	            	for(var i=0;i<list.length;i++){
	            		var pri;
		            	var date;
	            		/* if(list[i].integralState=='01'){
	            			list[i].integralState ='消耗';
	            		}else{
	            			list[i].integralState = '增加';
	            		}
	            		$("#tab").append("<tr id='tr"+i+"'><td width='50' class='tdfirst'>" + (i + 1) + "</td><td align='center'>"+ list[i].name+ "</td><td align='center'>"+list[i].integralNum+ "</td><td align='center'>"+ list[i].integralState+ "</td></tr>"); */
	            		if(list[i].integralState=='01'){
	            			list[i].integralState ='消耗';
	            			pri = "-"+list[i].integralNum;
	            		}else{
	            			list[i].integralState = '增加';
	            			pri = "+"+list[i].integralNum;
	            		}
	            		date = getMyDate(list[i].recordDate);
	            		$("#tabs").append("<li><p class='tit'>"+list[i].name+"</p><p class='int'><span style='font-size: 12px;'>获取</span>积分：<span style='font-size: 16px;color: #c8a65a;'>"+pri+"</span></p><p class='time'>时间：<span>"+date+"</span></p></li>");
	            	}
	            	
	            }
		 })
	}
	
	 function getMyDate(str){  
         var oDate = new Date(str),  
         oYear = oDate.getFullYear(),  
         oMonth = oDate.getMonth()+1,  
         oDay = oDate.getDate(),  
         oHour = oDate.getHours(),  
         oMin = oDate.getMinutes(),  
         oSen = oDate.getSeconds(),  
         oTime = oYear +'-'+ getzf(oMonth) +'-'+ getzf(oDay) +' '+ getzf(oHour) +':'+ getzf(oMin) +':'+getzf(oSen);//最后拼接时间  
         return oTime;  
     };  
    
    function getzf(num){  
        if(parseInt(num) < 10){  
            num = '0'+num;  
        }  
        return num;  
    }  
	
	function queryState(state){
		var user = $("#userId").val();
		 $.ajax({
			  url:"${ctx}/orderInforController.do?queryIntegralDetails=key",
	            dataType:"json",
	            type:"post",
	            data:{"userId":user,"integralState":state},
	            success:function(data){
	            	var list = data.obj;
	            	for(var i=0;i<list.length;i++){
	            		var pri;
		            	var date;
	            		/* if(list[i].integralState=='01'){
	            			list[i].integralState ='消耗';
	            		}else{
	            			list[i].integralState = '增加';
	            		}
	            		$("#tab").append("<tr id='tr"+i+"'><td width='50' class='tdfirst'>" + (i + 1) + "</td><td align='center'>"+ list[i].name+ "</td><td align='center'>"+list[i].integralNum+ "</td><td align='center'>"+ list[i].integralState+ "</td></tr>"); */
	            		if(list[i].integralState=='01'){
	            			list[i].integralState ='消耗';
	            			pri = "-"+list[i].integralNum;
	            		}else{
	            			list[i].integralState = '增加';
	            			pri = "+"+list[i].integralNum;
	            		}
	            		date = getMyDate(list[i].recordDate);
	            		$("#tabs").append("<li><p class='tit'>"+list[i].name+"</p><p class='int'><span style='font-size: 12px;'>获取</span>积分：<span style='font-size: 16px;color: #c8a65a;'>"+pri+"</span></p><p class='time'>时间：<span>"+date+"</span></p></li>");
	            	}
	            	
	            }
		 })
	}
	function switchTab(obj) {

		var tab = obj.id;
		if (tab == "leftTitleText") {
			$("#tabs li").remove();
			$("#leftTitleText").addClass('btn_xz')
			$("#centerTitleText").removeClass('btn_xz')
			$("#rightTitleText").removeClass('btn_xz')
			queryAll()

		}else if(tab == 'centerTitleText'){
			$("#tabs li").remove();
			var state = '00'
				$("#centerTitleText").addClass('btn_xz')
				$("#leftTitleText").removeClass('btn_xz')
				$("#rightTitleText").removeClass('btn_xz')
			queryState(state);
		}else {
			$("#tabs li").remove();
			var state = '01'
				$("#rightTitleText").addClass('btn_xz')
				$("#centerTitleText").removeClass('btn_xz')
				$("#leftTitleText").removeClass('btn_xz')
			queryState(state);

		}

	}
	function userInfo(){
		var user = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryUserInfo=key",
    		type:"post",
    		datatype:"josn",
    		async: false,
    		data:{userId:user},
    		success: function (data) {
    			var data=$.parseJSON(data);
    			var usr = data.obj;
    			if(usr != null){
    				if(usr.customerLevel =='1'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/one.png'); 
    				}else if(usr.customerLevel =='2'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/two.png'); 
    				}else if(usr.customerLevel =='3'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/three.png'); 
    				}else if(usr.customerLevel =='4'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/four.png');
    				}else{
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/five.png');
    				}
    			$("#level").html(usr.customerLevel);
    			$("#integra").html(usr.integral);
    			}
    		}
		})
	}
</script>


</body></html>
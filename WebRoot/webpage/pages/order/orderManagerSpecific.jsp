<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=OrderManagerSpecific toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${orderManagerInfo.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${orderManagerInfo.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	订单号:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="orderId" onkeypress="notNull(this);" name="orderId" readonly="readonly" value="${orderManagerInfo.orderId}" />
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	购买人姓名:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="purchaseName" onkeypress="notNull(this);" name="purchaseName"  readonly="readonly" value="${orderManagerInfo.purchaseName}" />
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>购买时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="purchaseTime" readOnly="readOnly" value="${purchaseTime }"
					name="purchaseTime"  class="inputxt" style="width: 260px;height:36px;"
				    onkeypress="notNull(this);"></input>
    		</td>
    		
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	订单状态:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="orderState" name="orderState" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="未发货"<c:if test="${orderManagerInfo.orderState=='未发货'}">selected</c:if>>常规</option>
					<option value="已发货"<c:if test="${orderManagerInfo.orderState=='已发货'}">selected</c:if>>强制</option>
    		 	</select>
    		 </td>
    		
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	订单金额:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="orderprice" onkeypress="notNull(this);" name="orderprice" readonly="readonly" value="${orderManagerInfo.orderprice}" />
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	快递单号:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="expressNumber" onkeypress="notNull(this);" name="expressNumber"  readonly="readonly" value="${orderManagerInfo.expressNumber}" />
    		</td>
    	</tr>
    	
    	<tr>
     <td align="center" colspan="8">
       <!--  <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span> -->
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
    	
   </table>
 </form>
</body>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
function saveData(url) {
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#saveBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}


// function loadingList(){
// 	var start = 1;
// 	var end = 10;
// 	$.ajax({
// 		url:"${ctx}/orderInforController.do?queryGoodsList=key",
// 		type:"post",
// 		datatype:"josn",
// 		data:{start:start,end:end},
// 		success: function (data) {
//             var data=$.parseJSON(data);
            
//             $("#aaa").append(
//             		"<a href='"+aaa+"' title='店长推荐 冰川七号有机西红柿 [一份]' style='height:auto; text-align:left; padding-bottom:9%;'>'"+
//             		+"'</a>"
//             		);

//          }
//      });
// }
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=OrderManagerAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	快递单号:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="expressNumber" onkeypress="notNull(this);" name="expressNumber" datatype="courierNum" errormsg="请输入1-20位内的字符！"  nullmsg="请输入快递单号！"/><span style="color: red;">*</span>
    		 </td>
    	</tr>
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	快递名称:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="expressName" onkeypress="notNull(this);" name="expressName" datatype="s4-12" errormsg="请输入4-12位内的字符！"  nullmsg="请输入快递名称！"/><span style="color: red;">*</span>
    		 </td>
    	</tr>
    	
     <tr>
     <td align="center" colspan="8">
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData(' ${ctx}/orderInforController.do?saveUpdate=key')" >保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML lang="zh-CN">
<head>
<title>订单详情</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<meta name="format-detection" content="email=no" />
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="X-UA-Compatible" content="IE=10.000" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.png"/>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css"  />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css" />
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></SCRIPT>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>

<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/modal.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/slider-common.css"/>

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.touchSlider.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.tabs.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.lazyload.min.js"></script>

<script type="text/javascript" src="${ctx}/common/js/jquery/common.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/json2.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/login.css"/>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/order.css"/>

</head>

<body>
    <div>
    	<!-- 隐藏的用户id -->
	<input type="hidden" value="${userId}" id="userId" name="userId">
		<div class="new-header bg_common">
		 <A class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></A>
		 <h2>
		 	订单详情
		 </h2>
		</div>
	</div>
<div class="margin-center PublicWidth1004">
	<ul class="orders">
    		<li>
    			<div class="hx"></div>
    			<div class="center1">
    				<p>订单编号：${orderList[0].orderId}</p>
    				<p>订单金额：${money}</p>
    				<p>订单日期：${purchaseTime}</p>
    				<p>订单状态：
						${appOrderInfor.orderState}
    				</p>
    			</div>
    		</li>
    		<c:forEach var="appOrderInfor" items="${orderList}">
    			<li>
    				<div class="hx"></div>
	    			<div class="center2">
	    				<div class="left">
	    					<img src="${appOrderInfor.mainImageUrl}">
	    				</div>
	    				<div class="right">
	    					<p style="color: #000;">${appOrderInfor.goodsName}</p>
		    				<p class="text_color_main">${appOrderInfor.price}</p>
		    				<p>x ${appOrderInfor.goodNumber}</p>
	    				</div>
	    				<div style="clear: both;"></div>
	    			</div>
    			</li>
    		</c:forEach>
    		
    		<li>
    			<div class="hx"></div>
    			<div class="center3">
    				<div class="top">
    					<span class="left">${appOrderInfor.receiptName}</span>
    					<span class="right">${appOrderInfor.receiptPhone}</span>
    				</div>
    				<div class="down">
						${appOrderInfor.address}
    				</div>

    			</div>
    		</li>
    	</ul>
    	<div style="width:95%;margin:10px auto;height:auto;">
	    	<a class="btn_common" href="javascript:history.go(-1)" style="display:block;padding:8px 0;text-align: center;width:100%;border-radius:5px;">返回</a>
    	</div>
</div>
	<!--底部浮层begin-->
	

<div style="margin-bottom: 80px;"></div>
<script type="text/javascript">
	//判断导航选中
	$(function(){
		cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	});

	function cartNum(){
		var userId = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryByCarInfo=key",
		    type:"POST",
		    async:"false",
		    datatype:"JOSN",
		    data:{userId:userId},
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.obj;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
						width:"22px",
						height:"22px",
						padding:"10px"
					});
		    		$("#cartNum").animate({
						width:"20px",
						height:"20px",
						padding:"0"
					});
		    	}
		    }
		});
	}
	
</script>
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/jquery.scrollUp.js" ></script>
<link rel="stylesheet" type="text/css" href="http://www.cunetong.cn/resource/phone_front/phone/css/scrollup.css" />
<script type="text/javascript">
    /*回到顶部JS*/
    $.scrollUp();
</script>
</body>
</html>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HEAD>
    <TITLE>订单确认</TITLE>
    
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<meta name="format-detection" content="email=no" />
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="X-UA-Compatible" content="IE=10.000" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.png"/>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css"  />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css" />
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></SCRIPT>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>

<LINK rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/mainAddress.css">	 
    <script type="text/javascript">
        //确认订单提交
        function toSuccess() {
        	var addres = $("#address").val();
        	//alert(addres);
        	if(addres ==''){
        		sj_alert("您的地址为空啦！！！");
        		return;
        	}
            $(".submitImg").attr("disabled", "disabled");
            var htm = "<img src='${ctx}/common/js/jquery/imgs_init/loader.gif'/>";
             htm += "&nbsp;&nbsp;正在提交，请稍等...";
             $(".submitImgLoad").html(htm);
             var user = $("#userId").val();
             var tot = $("#total").val();
             $.ajax({
            	 url:"${ctx}/orderInforController.do?checkOrder=key",
         		type:"post",
         		datatype:"josn",
         		async: false,
         		data:{userId:user,total:tot},
         		success: function (data) {
         			var da=$.parseJSON(data);
         			if(da.obj=='0'){
         				sj_alert("您的积分不足啦！！！");
         			}else{
         				 $("#form1").submit();
         			}
         			
         		}
             })
        }
        
        $(function () {
        	cartNum();
        });
        //带着商品列表跳转到地址列表页面
        function toAddressList(){
        	var total = $("#total").val();
        	var userId = $("#userId").val();
        	//商品编号
        	var ids="";
        	//商品数量
        	var nums = "";
        	var names = "";
        	var urls = "";
        	var pris = "";
        	var goodsIds=document.getElementsByName("goodsId");//获取所有选中的商品id
        	var goodsNums=document.getElementsByName("num");//商品数量
        	var goodsNames=document.getElementsByName("goodsName");//商品名称
        	var mainImageUrls=document.getElementsByName("pictureURL");//商品图片地址
        	var prices=document.getElementsByName("price");//商品价格
        	for(var i=0;i<goodsIds.length;i++){
        		if(goodsIds[i].value!=""){
        			if(ids!=""){
        				ids=ids+","+goodsIds[i].value;
        				nums=nums+","+goodsNums[i].value;
        				names=names+","+goodsNames[i].value;
        				urls=urls+","+mainImageUrls[i].value;
        				pris=pris+","+prices[i].value;
        			}else{
        				ids=goodsIds[i].value;
        				nums=goodsNums[i].value;
        				names=goodsNames[i].value;
        				urls=mainImageUrls[i].value;
        				pris=prices[i].value;
        			}
        		}
        	}
        	window.location.href="${ctx}/goddsAddressController.do?toGoddsAddressList=key&goodsIds="+ids+"&goodsNums="+
        			nums+"&goodsNames="+names+"&mainImageUrls="+urls+"&prices="+pris+"&userId="+userId+"&total="+total;
        }
      	
    </script>
    <style type="text/css">
        .zfffs {
            clear: both;
            width: 100%;
        }

        .zfffs li.ok_pay {
            margin-top: 16px;
            width: auto;
            height: 58px;
        }

        .zfffs li.ok_pay a {
            transition: none 0.2s;
            cursor: pointer;
            border: 1px solid #EEE;
            display: block;
        }

        .zfffs li.ok_pay .select_pay {
            display: inline-block;
            float: left;
            margin-right: 10px;
            padding: 5px;
        }

        .zfffs li.ok_pay .select_pay1 {
            display: inline-block;
            float: right;
            margin-right: 10px;
            padding: 5px;
        }
    </style>
</HEAD>
<BODY style="overflow-x:hidden">
<div class="new-header bg_common" style="position: fixed;width:100%; top:0;">
    <A class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></A>
    <h2 style="border-bottom: 1px solid #dedede;">提交订单</h2>
</div>

<!-- 添加收货地址 end-->
<!--主体-->
<div id="orderConfirm" class="main" style="margin-top: 45px;">
                                             <!--将商品添加到订单列表  -->
    <form id="form1" method="post" action="${ctx}/orderInforController.do?countSubmit=key">
    	<input id="userId" name="userId" type="hidden" value="${userId}">
    	<input type="hidden" id="total" name ="total" value="${total}"/>
        <input type="hidden" name="struts.token.name" value="token" />
		<input type="hidden" name="token" value="OT6BI8VQO7JT1SJKU5NKU59IOD18SYCU" />
        <!-- 收货方式 -->
      <!--   <div id="qbdd_libj" class="wxw-addresstitle">
           
            <p style="margin-left:28px;">收货地址</p>
        </div> -->
        <a id="wxw-addresscontent"  onclick="toAddressList()">
       <!--  <a id="wxw-addresscontent" href="goddsAddressController.do?toGoddsAddressList=key"> -->
<!--             <input type="hidden" name="userId" value="1"/> -->
            <div class="qbdd_first" style="line-height:20px;font-size:16px;">
             <img src="${ctx}/common/js/jquery/imgs_init/01.png" style="float: left; padding-right: 10px; padding-top: 10px;">
                <p style="width:100%; line-height:18px;font-size:12px;">
	                <span id="shopName" style="font-size: 16px;line-height: 30px;">${goddsAddress.shopName}</span>
	                <input type="hidden" name ="shopName" value="${goddsAddress.shopName}"/>
	                <input type="hidden" id='phone' name ="shopPhone" value="${goddsAddress.shopPhone}"/>
	                <input type="text" name ="address" id ="address" value="${goddsAddress.remark1}"/>
	                <span class = "phone text_color_main" style="font-size: 14px;color: #777;"  id="shopPhone">${goddsAddress.shopPhone}</span> 
	                <img style="float:right;     width: 10px;padding-top: 16px;" src="${ctx}/common/js/jquery/imgs_init/right.png">
	                <br/>
	                <input type="hidden" id="regionLocations" value="1"/>
	                <span id="ShippingToAddress" style="font-size: 14px;color: #555;">${goddsAddress.remark1}</span>
                </p>
            </div>
        </a>
        <!-- 彩线 -->
       <%--  <div class="wxw-caixian"><img src="${ctx}/common/js/jquery/imgs_init/wxw02.jpg" style="width:100%;"/></div> --%>
        <!-- 彩线 -->
        
        <!-- 显示商品 -->
        <div class="wxw-product" id = "counts">
        	<c:forEach var = "appGoodsInfor" items="${list}" varStatus="i">
	        	<ul class="wxw-product-ul">
					<li>				
						<a href="${ctx}/orderInforController.do?goodsDetails=key&productId=${appGoodsInfor.goodsId}" >
							<div class="wxw-fl">
								<IMG style="width:106px;height:100px;" src="${appGoodsInfor.mainImageUrl}" >
							</div>
							<div class="wxw-fr" style="    width: 60%;">
								<%-- <h2>${appGoodsInfor.goodsName}</h2> --%>
								<p style="font-size:16px;    margin-bottom: 40px;">${appGoodsInfor.goodsName}</p>
								<p class="price" style="font-size: 16px;color: #ae8f46;"><span style="color: #ae8f46;font-size: 16px;">${appGoodsInfor.remark3}</span>积分 <span style="float: right;color: #777;font-size: 16px;">*${appGoodsInfor.num}</span></span></p>
								<!-- <span class="number">数量:</span> -->
								<input type="hidden" id="goodsId" name = "goddsList[${i.index}].goodsId" value="${appGoodsInfor.goodsId}"/>
								<input type="hidden" id="goodsName" name = "goddsList[${i.index}].goodsName" value="${appGoodsInfor.goodsName}"/>
								<input type="hidden" id="price" name = "goddsList[${i.index}].price" value="${appGoodsInfor.remark3}"/>
								<input type="hidden" id="num" name = "goddsList[${i.index}].num" value="${appGoodsInfor.num}"/>
								<input type="hidden" id="pictureURL" name="goddsList[${i.index}].pictureURL"  value="${appGoodsInfor.mainImageUrl}" />
								<input type="hidden" id="goodsId" name = "goodsId" value="${appGoodsInfor.goodsId}"/>
								<input type="hidden" id="goodsName" name = "goodsName" value="${appGoodsInfor.goodsName}"/>
								<input type="hidden" id="price" name = "price" value="${appGoodsInfor.remark3}"/>
								<input type="hidden" id="num" name = "num" value="${appGoodsInfor.num}"/>
								<input type="hidden" id="pictureURL" name="pictureURL"  value="${appGoodsInfor.mainImageUrl}" />
							</div>
						</a>
					</li>
				</ul>
			</c:forEach>
        </div>
        <!-- 显示商品 -->
     
<style type="text/css">
	#yOrder a:hover{text-decoration:none;}
</style>
       <%--  <div id="qbdd_libj">
            <img src="${ctx}/common/js/jquery/imgs_init/06.png">
            <p style="margin-left:35px;">付款明细</p>
        </div> --%>
      
        <!--去结算-->
        <div class="jiesuan">
        	 <p style="font-size:16px;  float:left;width: 68%;text-align: left; padding-left:2%;background-color:#fff;color: #333;">合计：
                <span style=" font-size:16px; color:#ff0000;">
                	<span id="subtotal" style="color:#Ae8F44; font-size: 16px; font-weight: 600";>
                		${total}
                	</span>
                </span>积分
            </p>
            <a href="javascript:void(0);" onclick="toSuccess()">
            <p class="btn_common" style="width: 30%;float: right;">提交订单</p></a>           
        </div>
    </form>
</div>
	<!--底部浮层begin-->
	

<script type="text/javascript">
	/* //判断导航选中
	$(function(){
		cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	}); */
	$(function(){
		//phone
		var i=$("img")
		if($('#phone').val()==''){
			$(".qbdd_first").empty()
			$(".qbdd_first").append("<div class='btn_common' style='padding: 10px;text-align: center;'>请选择地址<div>")
			console.log($('#phone').val())
		}
	})
	function cartNum(){
		var userId = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryByCarInfo=key",
		    type:"POST",
		    async:"false",
		    data:{userId:userId},
		    datatype:"JOSN",
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.obj;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
    					width:"22px",
    					height:"22px",
    					padding:"10px"
    				});
		    		$("#cartNum").animate({
    					width:"20px",
    					height:"20px",
    					padding:"0"
    				});
		    	}
		    }
		});
	}
</script> 
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.scrollUp.js" ></script>
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/scrollup.css" />
<script type="text/javascript">
    /*回到顶部JS*/
    $.scrollUp();
</script>
<!--回到顶部end-->
</BODY>
</HTML>

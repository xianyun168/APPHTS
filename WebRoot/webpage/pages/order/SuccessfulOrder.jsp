<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HEAD>
    <TITLE>订单确认</TITLE>
    
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<meta name="format-detection" content="email=no" />
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="X-UA-Compatible" content="IE=10.000" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.png"/>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css"  />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css" />
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></SCRIPT>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>

<LINK rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/mainAddress.css">	 
    <script type="text/javascript">
        //确认订单提交
        function toSuccess() {
        	window.location.href = '${ctx}/orderInforController.do?sendGoodsList=key&userId='+$("#userId").val();
        }
        
    </script>
    <style type="text/css">
        .zfffs {
            clear: both;
            width: 100%;
        }

        .zfffs li.ok_pay {
            margin-top: 16px;
            width: auto;
            height: 58px;
        }

        .zfffs li.ok_pay a {
            transition: none 0.2s;
            cursor: pointer;
            border: 1px solid #EEE;
            display: block;
        }

        .zfffs li.ok_pay .select_pay {
            display: inline-block;
            float: left;
            margin-right: 10px;
            padding: 5px;
        }

        .zfffs li.ok_pay .select_pay1 {
            display: inline-block;
            float: right;
            margin-right: 10px;
            padding: 5px;
        }
    </style>
</HEAD>
<BODY style="overflow-x:hidden">
<div class="new-header bg_common" style="position: fixed;width:100%; top:0;">
  <!--   <A class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></A> -->
    <h2>提交订单</h2>
</div>

<!-- 添加收货地址 end-->
<!--主体-->
<div id="orderConfirm" class="main" style="margin-top: 45px;">
                                             <!--将商品添加到订单列表  -->
        <input id="userId" name="userId" type="hidden" value="${userId}">
        <!-- 彩线 -->
      <%--   <div class="wxw-caixian"><img src="${ctx}/common/js/jquery/imgs_init/wxw02.jpg" style="width:100%;"/></div> --%>
        <!-- 彩线 -->
        
        <!-- 显示商品 -->
        <div class="wxw-product" id = "counts">
        	<span style="color:#6e6e6e;text-align: center; display: block;font-size: 18px;padding:20%;">订单提交成功~~</span>
        </div>
        <!-- 显示商品 -->
     
<style type="text/css">
	#yOrder a:hover{text-decoration:none;}
</style>
    
        <!--去结算-->
        <div class="jiesuan">
            <a href="javascript:void(0);" onclick="toSuccess()">
            <p class="btn_common" style=" width:100%; float: none;">返回首页</p></a>           
        </div>
    
</div>
	<!--底部浮层begin-->
	

<div style="margin-bottom: 80px;"></div>
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.scrollUp.js" ></script>
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/scrollup.css" />
<script type="text/javascript">
    /*回到顶部JS*/
    $.scrollUp();
</script>
<!--回到顶部end-->
</BODY>
</HTML>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html lang="zh-cn"><head>
<title>我的购物车</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="email=no">
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="X-UA-Compatible" content="IE=10.000">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/imgs_init/sj.ico">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css">
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css">



<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>

</head>
<body>
	
	<div class="new-header bg_common" style="position: fixed;width:100%; top:0;">
		<a class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></a>
		<h2 style=" border-bottom:1px #dedede solid;">购物车</h2>
		<a class="new-a-jd" href="javascript:deleteAll();" style="margin-right:-10px;">
			<img src="${ctx}/common/js/jquery/imgs_init/shanchu.png" style="width:30px; height:30px;">
		</a>
	</div>
<!--主体-->
<div class="main" style="margin-top: 45px;">
<!-- <input type="button" value="测试" onclick="selectAllCheckBox('checkAll');"> -->
<input id="userId" name="userId" type="hidden" value="${userId}">
<section class="f_list i_list i_list_h">
	<ul class="clearfix" id="f_list">
			
	</ul>
</section>
<a class="more" id="show_more" style="display:none;" href="javascript:void(0);" page="2">加载更多</a>
<!--去结算-->
</div>
<!--底部-->
		<div class="bottom_margin"></div>
		<div class="spxq_dibu">
			<a id="checkAll_fc" href="javascript:selectAllCheckBox('checkAll');" flag="true" style="width:20%;">
				<img id="img_fc" src="${ctx}/common/js/jquery/imgs_init/check_box_unfocused.png" style="width:16px; height:16px;line-height: 16px;border: 0.02em solid rgb(255, 255, 255);border-radius: 8px;vertical-align: middle; ">
				<span style="font-weight:bold;font-size:14px;">全选</span>
			</a>
			<a href="javascript:void(0);" style="width:40%;display: block;">
				<ul style="text-align: left;">
					<li style="">
					总计：<span id="totalPrice_fc"></span>
					</li>
					
				</ul>
			</a>
			<a href="javascript:void(0);" style="width:0.2%;">&nbsp;</a>
			<%-- <a href="${ctx}/orderInforController.do?sendGoodsList=key&userId=${userId}" style="width:19.8%;"><p class="btn_common">继续购物</p></a>--%>
			<a href="javascript:void(0);" style="width:20%;">&nbsp;</a> 
			<a href="javascript:void(0);" style="width:19.8%;" onclick="toCheckOut();"><p class="btn_common">结算</p></a>
		</div>
</body>
<script type="text/javascript">
$(function(){
	selectAllList();
});

//修改购物车中商品数量
//修改购物车中商品数量
function updateProductNum(productId,sku) {
	var user = $("#userId").val();
    if(productId!=null){
        $.ajax({
            url:"${ctx}/appShoppingCarInforController.do?updateNum=key",
            dataType:"json",
            type:"post",
            data:{"productId":productId,"sku":sku,userId:user},
            success:function(data){
                if(data.bool == "ok") {
                    //修改购物车数据库中商品数量
                    
                
                }
            }
        });
    }else{
        // sj_alert("很抱歉，当前商品不存在！");
        return ;
    }
}

//添加商品到购物车
function add(productId,num,shopInfoId,sku){
	$(function(){
			var amount=$("#amount_"+productId).val();//当前数量
			var storeNumber = $("#storeNumber"+productId).val();
			var a=/^[1-9]?[0-9]?[0-9]?$/;
			if(!a.test(amount)){
				sj_alert("只能输入小于1000的正整数！");
				$("#amount_"+productId).val(1);
				amount=1;
			}else{
                if(num=="-1"){//减少商品数量
                    if(amount>1){
                        if(amount>0&&amount<1000){
                        amount=Number(amount)-Number(1);
                        }
                        $("#amount_"+productId+"").val(amount);
                        // 修改购物车中商品数量
                        updateProductNum(productId,"0");
                    }else{
                        amount=1;
                    }
                }else if(num=="-2"){//增加商品数量
                    if(amount>0&&amount<1000){
                        amount=Number(amount)+Number(1);
                    }
                    $("#amount_"+productId+"").val(amount);
                    // 修改购物车中商品数量
                    updateProductNum(productId,"1");
                }else if(num=="0"){//失去焦点是触发
                    if(amount==0){
                        sj_alert("购买数量不可为0！");
                        $("#amount_"+productId+"").val(1);
                        amount=1;
                    }
                }
            }
		var total="";//当前商品总金额
		var salesPrice= $("#salesPrice"+productId).val();//单价
		total=salesPrice*amount;
		total = total.toFixed(2);
		$("#total"+productId).html("￥"+total);//此商品的价格小计
		//amountMoney();//金额和数量
	});
	countAmountAndNumber();//计算总金额和商品数量
	
}
//统计金额和数量
function amountMoney(){
	var amountItme = document.getElementsByName("amountItme");
	var costPriceTotal =0;//市场价总额
	var Subtotal=0;//销售总价格
	//var SubNum=0;//商品总数
	for(var i=0;i<amountItme.length;i++){
		var priceId=amountItme[i].id;
		var array = new Array();
		array=priceId.split("_");
		var productId=array[1];//购物车id
		if(document.getElementById("checkbox_"+productId).checked){
			var storeNumber = $("#storeNumber"+productId).val();
			if(storeNumber>0){
				var price=$("#salesPrice"+productId).val();//单价
				var costPrice=$("#costPrice"+productId).val();//市场价
				var OneAmount=amountItme[i].value;//当前商品数量
				var total =Number(price*OneAmount);
				total = formatCurrency(total);
				$("#total"+productId).html("￥"+total);//当前商品价格小计
				//SubNum=Number(OneAmount)+Number(SubNum);//商品总数
				Subtotal=Number(price*OneAmount)+Number(Subtotal);//销售总价格
				Subtotal=Subtotal.toFixed(2);
				costPriceTotal=Number(costPrice*OneAmount)+Number(costPriceTotal);//市场价总额
				costPriceTotal=costPriceTotal.toFixed(2);
				//$("#SubtotalItems"+shopInfoId).html("Subtotal("+SubNum+" items): $"+costPriceTotal);//市场价总额
			}
		}
	}
// 	$("#SubtotalItems").html("折扣前总价:￥"+formatCurrency(costPriceTotal));//市场价总额
// 	$("#Subtotal").html("￥"+formatCurrency(Subtotal));//销售价总额
	var Discount = Number(Subtotal)-Number(costPriceTotal);//差额
	Discount=formatCurrency(Discount);//小数点后两位
// 	$("#Discount").html("折扣: ￥"+Discount);
	$("#hiddenDiscount").val(Discount);
// 	$("#StatusSubNum").html(SubNum+"/50");
}
//删除
function deleteCart(){
	var ids="";
		var carts=document.getElementsByName("cartInfo");//获取所有选中的商品id
		for(var i=0;i<carts.length;i++){
			if(carts[i].value!=""){
				if(carts[i].checked){//如果选中
				if(ids!=""){
					ids=ids+","+carts[i].value;
				}else{
					ids=carts[i].value;
				}
				}
			}
		}
	if(ids==''){
		sj_alert("请选择要删除的商品!");
		return;
	}
	var url ="/loginCustomer/checkLogin.action";
	$.post(url,{},function(data){//验证是否登录
	if(data.bool == true){//已登录操作
		if(ids!=""){//有选中的商品时
			sj_confirm("确认删除吗？", null, null, function(){
				var url="/phone/shoppingCar/deleteShoppingCart.action";
				$.post(url,{ids:ids},function(data){
					if(data){
						window.location.reload(); //删除后重新加载列表
					}
				},"JSON");
			});
		}else{
			sj_alert("提醒:请选择商品！");
		}
	}else{
		//没有登录时，删除cookie中的数据
		if(undefined!=$.cookie('customerCar')){
			var carCookie = eval($.cookie('customerCar'));
			sj_confirm("确定删除吗？", null, null, function(){
				var selectNum=0;//当前选中商品个数
				$("input[name='cartInfo']").each(function(){
					if(this.checked){
						selectNum++;
					}
				});
				if(selectNum==carCookie.length){//如果当前选中商品个数等于cookie中的个数
					$.cookie("customerCar",null,{expires: -1 ,path:"/"});//全部清除
				}else{//如果当前选中商品个数不等于cookie个数
					var value="";
					$("input[name='cartInfo']").each(function(){
						for(var i=0;i<carCookie.length;i++){
							if(!this.checked){//如果当前商品没有选中，说明不删除此商品，cookie中保存
								if(this.value==carCookie[i].productId){
									value +=",{'productId':'"+carCookie[i].productId+"','num':'"+carCookie[i].num+"','shopInfoId':'"+carCookie[i].shopInfoId+"'}";//以json格式存放，方便维护和取
								}
							}
						}
					});
					value = value.substring(1,value.length);
					value ="["+value+"]";
					$.cookie('customerCar', value , {expires: 7, path:"/"}); //设置带时间的cookie 7天，加path属性时，只能其他页面使用，且其他页面取值条件为当前页面的上级路径包含path一致
				}
				window.location.href=window.location.href; //删除后重新加载列表
			});
		}
	}
	},"JSON");
}

function deleteAll(){
	var ids="";
	var carts=document.getElementsByName("cartInfo");//获取所有选中的商品id
	for(var i=0;i<carts.length;i++){
		if(carts[i].value!=""){
			if(carts[i].checked){//如果选中
				if(ids!=""){
					ids=ids+","+carts[i].value;
				}else{
					ids=carts[i].value;
				}
			}
		}
	}
	if(ids==''){
		sj_alert("请选择要删除的商品!");
		return;
	}
		if(ids!=""){//有选中的商品时
			sj_confirm("确认删除吗？", null, null, function(){
				var url="${ctx}/orderInforController.do?deleteAll=key&userId="+$("#userId").val();
				$.post(url,{ids:ids},function(data){
					if(data){
						window.location.reload(); //删除后重新加载列表
					}
				},"JSON");
			});
		}else{
			sj_alert("提醒:请选择商品！");
		}
}


/**
 * 将数值四舍五入(保留2位小数)后格式化成金额形式
 *
 * @param num 数值(Number或者String)
 * @return 金额格式的字符串,如'1,234,567.45'
 * @type String
 */
function formatCurrency(num) {
	num = num.toString().replace(/$|\,/g,'');
	if(isNaN(num))
	num = "0";
	sign = (num == (num = Math.abs(num)));
	num = Math.floor(num*100+0.50000000001);
	cents = num%100;
	num = Math.floor(num/100).toString();
	if(cents<10)
	cents = "0" + cents;
	for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
	num = num.substring(0,num.length-(4*i+3))+','+
	num.substring(num.length-(4*i+3));
	return (((sign)?'':'-') + num + '.' + cents);
}
//onclickCheckOut提交
function toCheckOut(){
	var user = $("#userId").val();
	var goodsIds=document.getElementsByName("cartInfo");
	if(goodsIds == null){
		sj_alert("没有选择商品,不能去结算！");
		return;
	}else{
		
	    //商品编号
		var ids="";
		//商品数量
		var nums = "";
		var names = "";
		var urls = "";
		var pris = "";
		var goodsIds=document.getElementsByName("cartInfo");//获取所有选中的商品id
		var goodsNums=document.getElementsByName("amount");//商品数量
		var goodsNames=document.getElementsByName("goodsName");//商品名称
		var mainImageUrls=document.getElementsByName("mainImageUrl");//商品图片地址
		var prices=document.getElementsByName("price");//商品价格
		for(var i=0;i<goodsIds.length;i++){
			if(goodsIds[i].value!=""){
				if(goodsIds[i].checked){//如果选中
					if(ids!=""){
						ids=ids+","+goodsIds[i].value;
						nums=nums+","+goodsNums[i].value;
						names=names+","+goodsNames[i].value;
						urls=urls+","+mainImageUrls[i].value;
						pris=pris+","+prices[i].value;
					}else{
						ids=goodsIds[i].value;
						nums=goodsNums[i].value;
						names=goodsNames[i].value;
						urls=mainImageUrls[i].value;
						pris=prices[i].value;
					}
					
				}
			}
		}
		if(ids==""){
			sj_alert("没有选择商品,不能去结算！");
			return;
		}
		 $.ajax({
             type: "post",
             url: "orderInforController.do?checkGoodsNum=key&goodsIds="+ids+"&goodsNums="+nums,
             dataType: "json",
             success: function(data){
                        if(data.obj=="ok"){
	                        window.location.href="${ctx}/orderInforController.do?orderCountList=key&goodsIds="+ids+"&goodsNums="+
	        				nums+"&goodsNames="+names+"&mainImageUrls="+urls+"&prices="+pris+"&userId="+user;
                        }else{
                        	sj_alert(data.msg);
                        	return;
                        }
                      }
         });
	}
}
//计算总金额和数量
function countAmountAndNumber(){
	//选中商品总价
	var totalPrice=0;
	//商品种类数量
	var productCount = 0;
	var unSelected = 0;//未选中的复选框个数
	$(".checkboxClass").each(function(){
		if(this.checked){//如果选择商品
			var productId = this.value;
			var count= $("#amount_"+productId).val();//当前商品数量
			var salesPrice= $("#salesPrice"+productId).val();//单价
			totalPrice=Number(totalPrice)+Number(salesPrice*count);
			productCount = Number(productCount)+Number(1);
		}else{
			unSelected = Number(unSelected)+Number(1);
		}
	});
	//底部全选样式
	if(unSelected=='0'){//全部选中
		document.getElementById("img_fc").src = "${ctx}/common/js/jquery/imgs_init/ok.png";
	}else{
		document.getElementById("img_fc").src = "${ctx}/common/js/jquery/imgs_init/check_box_unfocused.png";
	}
	//底部统计数
	$("#totalPrice_fc").html(totalPrice);
	$("#productNumber_fc").html(productCount);
}
//
function selectCheckbox(productId){
	var options = document.getElementById("checkbox_"+productId);
	if(options.checked){
		document.getElementById("img_"+productId).src = "${ctx}/common/js/jquery/imgs_init/ok.png";
	}else{
		document.getElementById("img_"+productId).src = "${ctx}/common/js/jquery/imgs_init/check_box_unfocused.png";
	}
	doCheckbox();
}
//点击一个复选框时计算金额
function doCheckbox(){
	amountMoney();//全选商品后统计金额
	countAmountAndNumber();//计算总金额和商品数量
}
//全选属性
function selectAllCheckBox(id){
	var b = document.getElementsByName("cartInfo");
	var img = document.getElementsByName("checkbox_img");
	var all = $("#checkAll_fc").attr("flag");
	if(all!=null){
		if(all=='true'){
			$("#checkAll_fc").attr("flag","false");
			document.getElementById("img_fc").src = "${ctx}/common/js/jquery/imgs_init/ok.png";
			for(var i=0;i<b.length;i++){
				b[i].checked=true;
			}
			for(var i=0;i<img.length;i++){
				img[i].src = "${ctx}/common/js/jquery/imgs_init/ok.png"
			}
		}else{
			$("#checkAll_fc").attr("flag","true");
			document.getElementById("img_fc").src = "${ctx}/common/js/jquery/imgs_init/check_box_unfocused.png";
			for(var i=0;i<b.length;i++){
				b[i].checked=false;
			}
			for(var i=0;i<img.length;i++){
				img[i].src = "${ctx}/common/js/jquery/imgs_init/check_box_unfocused.png"
			}
		}
		amountMoney();//全选商品后统计金额
		countAmountAndNumber();//计算总金额和商品数量
	}
}
//查询购物车中所有的商品
function selectAllList(){
	var userId = $("#userId").val();
	$.ajax({
		url:"${ctx}/orderInforController.do?queryByCarInfoList=key",
		type:"post",
		async: false,
		data:{userId:userId},
		datatype:"josn",
		success:function(data){
			
			
			var da=$.parseJSON(data);
			console.log(da.obj.length)
			if(da.obj.length=="0"){
				$('#f_list').append("<img src='${ctx}/common/js/jquery/imgs_init/kong.png' style='width: 50%;padding: 25%;padding-bottom:2%;'><p style='    text-align: center;color: #999;'>购物车是空的</p>");
			};
			for(var i=0;i<da.obj.length;i++){
			//var div1 = '<div class="qjd_name" style="background:#eee;"><div id="qbdd_libj" style="width:60%; float:left; background:none;"><img src="${ctx}/common/js/jquery/imgs_init/dd_img_03.png"><p>积分商城</p></div></div>';	
				
			var input = '<input class="checkboxClass" id="checkbox_'+da.obj[i].goodsId+'" name="cartInfo" flag="son" value="'+da.obj[i].goodsId+'" style="width:16px; height:16px;line-height: 16px;margin-top:50px;float: left;display:none;" type="checkbox">';	
			var label = '<label for="checkbox_'+da.obj[i].goodsId+'"><img id="img_'+da.obj[i].goodsId+'" name="checkbox_img" src="${ctx}/common/js/jquery/imgs_init/check_box_unfocused.png" style="width:20px; height:20px;margin-top:50px;float: left;"></label>';
			var a = '<a href="${ctx}/orderInforController.do?goodsDetails=key&productId='+da.obj[i].goodsId+'"><img src="'+da.obj[i].mainImageUrl+'" style="margin-top:20px;"></a>';
			var div2 = '<div class="f_list_l" style="padding-left:10px; width:45%;"><div onclick="selectCheckbox('+da.obj[i].goodsId+');">'+input+label+'</div>'+a+'</div>';
			
			//var span = '<span style="float:right; padding-left:10px; padding-top:10px; padding-bottom:10px;"/>';
			
			
			var span2 = '<span id="total3761" style="margin-top:8px;font-size:14px; color:#333;     display: inline-block; padding-right:40px;">'+da.obj[i].goodsName+'</span><br><span id="total3761" style="margin-top:8px;font-size:14px; color:#ae8f46; margin-bottom: 20px;    display: inline-block; padding-right:40px;">'+da.obj[i].price+'</span><br>';
			var a2 = '<a href="javascript:;" onclick="add('+da.obj[i].goodsId+','+(-1)+')" style="width: 10px; text-align: center; text-decoration: none;background: none repeat scroll 0 0 #fff;border: 2px solid #ccc;display:block;padding:0px 10px; float:left; ">-</a>'; 
			var input2 = '<input name="amount" maxlength="3" id="amount_'+da.obj[i].goodsId+'" class="gm_sl" value="'+da.obj[i].remark1+'" style="width:34px; display: block; float: left; height:26px; line-height:26px; margin-left:0px;" onblur="javascript: add('+da.obj[i].goodsId+','+0+');" type="text">';
			var a3 = '<a href="javascript:;" onclick="add('+da.obj[i].goodsId+','+(-2)+')" style="width: 10px; text-align: center; text-decoration: none;background: none repeat scroll 0 0 #fff;border: 2px solid #ccc;display:block;padding:0px 10px; float:left; margin-left:0px;">+</a>';
			var span3 = '<span style="font-size:14px; color:#333;margin-top:2px;"></span>';
			var p = '<p class="qb_dd"><input id="goodsName" name="goodsName" value="'+da.obj[i].goodsName+'" type="hidden"><input id="mainImageUrl" name="mainImageUrl" value="'+da.obj[i].mainImageUrl+'" type="hidden">';
			p += '<input id="discount'+da.obj[i].goodsId+'" value="" type="hidden"><input id="salesPrice'+da.obj[i].goodsId+'" name="price" value="'+da.obj[i].price+'" type="hidden"><input id="hiddenDiscount" value="0.00" type="hidden"><input id="storeNumber'+da.obj[i].goodsId+'" name="storeNumber" value="'+da.obj[i].remark2+'" type="hidden">'+span2+a2+input2+a3+span3+'</p>';
			var div3 ='<div class="f_list_r" style="padding-left:10px; width:45%;"><h3 style="line-height:1.5em;word-wrap:break-word;overflow:hidden; padding-top:10px;"></h3>'+p+'</div>';
			if(i == 0){
				$('#f_list').append("<li id='dd_lipadd' style='border-bottom: 10px solid #f8f8f8;'>"+div2+div3+"</li>");
			}else{
				$('#f_list').append("<li id='dd_lipadd' style='border-bottom: 10px solid #f8f8f8;'>"+div2+div3+"</li>");
			}
			}
		}
	});
	selectAllCheckBox('checkAll');//默认全选
	countAmountAndNumber();//计算总金额和商品数量
}
</script>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HEAD>
    <TITLE>选地址</TITLE>
    
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<meta name="format-detection" content="email=no" />
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="X-UA-Compatible" content="IE=10.000" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.png"/>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css"  />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css" />
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></SCRIPT>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>

<LINK rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/mainAddress.css">	 
    <script type="text/javascript">
    $(function(){
    	$("#counts").hide();
    	changeAddress();
    	cartNum();
    })
        //进入更改收获地址div
        function changeAddress() {
    		var userId = $("#userId").val();
            $.ajax({
                type: "POST",
                url: "goddsAddressController.do?getGoddsAddressList=key",
                dataType: "json",
                data: {"userId":userId},
                success: function (data) {
                    $("#orderConfirm").hide("slow");
                    $("#addAddress").hide("slow");
                    $("#address").show("slow");
                    var $spanAddress = $("#spanAddress");
                    var dataObj=eval(data);
                    $spanAddress.html("");
                    if (dataObj.length > 0) {
                        for (var i = 0; i < dataObj.length; i++) {
                            var html = "";
                            html += "<div class=\"address\"  onclick='getGoddsAddress(" + dataObj[i].pkId+ ")'>";
                            html += "<div class=\"add_row\">";
                            html += "<div class=\"up\">";
                            html += "<span class=\"name\" style=\"width:30%;    font-size: 16px; color: #333;line-height: 34px;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;font-family:Microsoft Yahei;font-size:12px;\">" + dataObj[i].shopName + "</span>";
                            html += "<span class=\"phone\" style=\"width:30%;line-height: 34px;\">" + dataObj[i].shopPhone + "</span>";
                           // html += "<span style=\"width:30%;padding-left:30px;\"><a style=\"color: #fff;padding:2%;font-size:12px;border-radius:6px;background-color:#edd5a6;margin-top:2%;\" href='javascript:void(0);' onclick='getGoddsAddress(" + dataObj[i].pkId+ ")'>使用地址</a></span>";
                            //html += "<span style=\"width:30%;padding-left:30px;\"><a style=\"color: #fff;padding:2%;font-size:12px;border-radius:6px;background-color:#21b960;margin-top:2%;\" href='appShoppingCarInforController.do?getGoddsAddress=key&pkId=" + dataObj[i].pkId+ "'>使用地址</a></span>";
                            html += "<span class=\"opt\" ></span>";
                            html += "</div>";
                            html += "<div class=\"mid\">";
                            html += dataObj[i].province+","+ dataObj[i].city+","+ dataObj[i].area+","+ dataObj[i].shopAddress;
                            html += "</div> ";
                            /* html+="<div class=\"down\"><span style=\"padding-right:10px;width: auto;\"><a style=\"padding: 0;\" href='appShoppingCarInforController.do?modify1=key&pkId="+dataObj[i].pkId+"'\">编辑</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style=\"padding: 0px 0px;\" href='javascript:deleteAddress("+dataObj[i].pkId +","+dataObj[i].addressState+");'>删除</a></span></div>"; */
                            /* html+="<div class=\"down\"><span style=\"padding-right:10px;width: auto;\"><a style=\"padding: 0;\" href='javascript:updateAddress("+dataObj[i].pkId+")'\">编辑</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a style=\"padding: 0px 0px;\" href='javascript:deleteAddress("+dataObj[i].pkId +","+dataObj[i].addressState+");'>删除</a></span></div>"; */
                            html += "</div>";
                            html += "</div>";
                            $spanAddress.append(html);
                        }
                    }
                   /*  var html2 = "";
                    html2 += "<div class=\"jiesuan\">";                  
                    html2 += "<a href='javascript:goddsAddressAdd();'><p class=\"btn_common\" style=\"font-size:16px;\">添加新地址</p></a>";
                    html2 += "</div>";
                    $spanAddress.append(html2); */
                }
            });
        }
    //新增地址
    function goddsAddressAdd(){
    	var total = $("#total").val();
    	var userId = $("#userId").val();
    	//商品编号
    	var ids="";
    	//商品数量
    	var nums = "";
    	var names = "";
    	var urls = "";
    	var pris = "";
    	var goodsIds=document.getElementsByName("goodsId");//获取所有选中的商品id
    	var goodsNums=document.getElementsByName("num");//商品数量
    	var goodsNames=document.getElementsByName("goodsName");//商品名称
    	var mainImageUrls=document.getElementsByName("pictureURL");//商品图片地址
    	var prices=document.getElementsByName("price");//商品价格
    	for(var i=0;i<goodsIds.length;i++){
    		if(goodsIds[i].value!=""){
    			if(ids!=""){
    				ids=ids+","+goodsIds[i].value;
    				nums=nums+","+goodsNums[i].value;
    				names=names+","+goodsNames[i].value;
    				urls=urls+","+mainImageUrls[i].value;
    				pris=pris+","+prices[i].value;
    			}else{
    				ids=goodsIds[i].value;
    				nums=goodsNums[i].value;
    				names=goodsNames[i].value;
    				urls=mainImageUrls[i].value;
    				pris=prices[i].value;
    			}
    		}
    	}
    	window.location.href="${ctx}/goddsAddressController.do?goddsAddressAdd1=key&goodsIds="+ids+"&goodsNums="+
    			nums+"&goodsNames="+names+"&mainImageUrls="+urls+"&prices="+pris+"&userId="+userId+"&total="+total;
    }
    //编辑地址
    function updateAddress(pkId){
    	var total = $("#total").val();
    	var userId = $("#userId").val();
    	//商品编号
    	var ids="";
    	//商品数量
    	var nums = "";
    	var names = "";
    	var urls = "";
    	var pris = "";
    	var goodsIds=document.getElementsByName("goodsId");//获取所有选中的商品id
    	var goodsNums=document.getElementsByName("num");//商品数量
    	var goodsNames=document.getElementsByName("goodsName");//商品名称
    	var mainImageUrls=document.getElementsByName("pictureURL");//商品图片地址
    	var prices=document.getElementsByName("price");//商品价格
    	for(var i=0;i<goodsIds.length;i++){
    		if(goodsIds[i].value!=""){
    			if(ids!=""){
    				ids=ids+","+goodsIds[i].value;
    				nums=nums+","+goodsNums[i].value;
    				names=names+","+goodsNames[i].value;
    				urls=urls+","+mainImageUrls[i].value;
    				pris=pris+","+prices[i].value;
    			}else{
    				ids=goodsIds[i].value;
    				nums=goodsNums[i].value;
    				names=goodsNames[i].value;
    				urls=mainImageUrls[i].value;
    				pris=prices[i].value;
    			}
    		}
    	}
    	window.location.href="${ctx}/appShoppingCarInforController.do?modify1=key&goodsIds="+ids+"&goodsNums="+
    			nums+"&goodsNames="+names+"&mainImageUrls="+urls+"&prices="+pris+"&pkId="+pkId+"&userId="+userId+"&total="+total;
    }
   //选中地址(使用地址)
    function  getGoddsAddress(pkId){
    	var total = $("#total").val();
    	//var userId = $("#userId").val();
    	//商品编号
    	var ids="";
    	//商品数量
    	var nums = "";
    	var names = "";
    	var urls = "";
    	var pris = "";
    	var goodsIds=document.getElementsByName("goodsId");//获取所有选中的商品id
    	var goodsNums=document.getElementsByName("num");//商品数量
    	var goodsNames=document.getElementsByName("goodsName");//商品名称
    	var mainImageUrls=document.getElementsByName("pictureURL");//商品图片地址
    	var prices=document.getElementsByName("price");//商品价格
    	for(var i=0;i<goodsIds.length;i++){
    		if(goodsIds[i].value!=""){
    			if(ids!=""){
    				ids=ids+","+goodsIds[i].value;
    				nums=nums+","+goodsNums[i].value;
    				names=names+","+goodsNames[i].value;
    				urls=urls+","+mainImageUrls[i].value;
    				pris=pris+","+prices[i].value;
    			}else{
    				ids=goodsIds[i].value;
    				nums=goodsNums[i].value;
    				names=goodsNames[i].value;
    				urls=mainImageUrls[i].value;
    				pris=prices[i].value;
    			}
    		}
    	}
    	window.location.href="${ctx}/appShoppingCarInforController.do?getGoddsAddress=key&goodsIds="+ids+"&goodsNums="+
    			nums+"&goodsNames="+names+"&mainImageUrls="+urls+"&prices="+pris+"&pkId="+pkId+"&total="+total;
    }
  //删除地址
	function deleteAddress(pkId,addressState){
	  var userId = $("#userId").val();
		//商品编号
    	var ids="";
    	//商品数量
    	var nums = "";
    	var names = "";
    	var urls = "";
    	var pris = "";
    	var goodsIds=document.getElementsByName("goodsId");//获取所有选中的商品id
    	var goodsNums=document.getElementsByName("num");//商品数量
    	var goodsNames=document.getElementsByName("goodsName");//商品名称
    	var mainImageUrls=document.getElementsByName("pictureURL");//商品图片地址
    	var prices=document.getElementsByName("price");//商品价格
    	for(var i=0;i<goodsIds.length;i++){
    		if(goodsIds[i].value!=""){
    			if(ids!=""){
    				ids=ids+","+goodsIds[i].value;
    				nums=nums+","+goodsNums[i].value;
    				names=names+","+goodsNames[i].value;
    				urls=urls+","+mainImageUrls[i].value;
    				pris=pris+","+prices[i].value;
    			}else{
    				ids=goodsIds[i].value;
    				nums=goodsNums[i].value;
    				names=goodsNames[i].value;
    				urls=mainImageUrls[i].value;
    				pris=prices[i].value;
    			}
    		}
    	}
		if(addressState!=null&&addressState=="1"){
			sj_alert("默认地址不能删除！");
		}else if(addressState!=null&&addressState=="0"){
			sj_confirm('您确定要删除该地址吗？', null, null, function(){
				$.ajax({
					type: "POST",
					dataType: "JSON",
					url:"goddsAddressController.do?delete=key",
					data:{pkId:pkId},//  data:$("#form2").serialize(),//得到表单内的所有数据
					success: function(data){
						if(data.obj=="ok"){
							window.location.href="goddsAddressController.do?toGoddsAddressList=key&goodsIds="+ids+"&goodsNums="+
			    			nums+"&goodsNames="+names+"&mainImageUrls="+urls+"&prices="+pris+"&userId="+userId;
						}else{
							sj_alert("删除失败，请重试！");
						}
					}
				});
			});
		}
	}
       
    </script>
    <style type="text/css">
        .zfffs {
            clear: both;
            width: 100%;
        }

        .zfffs li.ok_pay {
            margin-top: 16px;
            width: auto;
            height: 58px;
        }

        .zfffs li.ok_pay a {
            transition: none 0.2s;
            cursor: pointer;
            border: 1px solid #EEE;
            display: block;
        }

        .zfffs li.ok_pay .select_pay {
            display: inline-block;
            float: left;
            margin-right: 10px;
            padding: 5px;
        }

        .zfffs li.ok_pay .select_pay1 {
            display: inline-block;
            float: right;
            margin-right: 10px;
            padding: 5px;
        }
    </style>
</HEAD>
<BODY style="overflow-x:hidden">
<div class="new-header bg_common" style="position: fixed;width:100%; top:0;">
    <A class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></A>
    <h2 style="border-bottom: 1px solid #dedede;">地址列表</h2>
</div>

<!-- 添加收货地址 end-->
<!-- 收获地址 -->
<div id="address" class="main" style="display: none;margin-top: 45px;">
<!-- 隐藏的用户id -->
		<input type="hidden" value="${userId}" id="userId" name="userId">
    <!-- <DIV class="p_floatbox">
        <DIV class="f_list_car p_buy clearfix">
            <div class="gwc_h">
                <div class="wdsc_h dpj_dd">
                    <span style="color:#ff6a00; font-size:16px;">地址列表</span>
                </div>
                <div class="gwc_fxk">
                </div>
            </div>
        </DIV>
    </DIV> -->
     <div class="jiesuan" style="position: fixed;">    
                    <a href='javascript:goddsAddressAdd();'><p class="btn_common" style="font-size:16px;">添加新地址</p></a>
                   </div>
<span id="spanAddress">
</span>
</div>
<!-- 显示商品 -->
        <div class="wxw-product" id = "counts" >
        
        	<input type="hidden" name = "total" id = "total" value="${total}"/>
        	<c:forEach var = "appGoodsInfor" items="${list}">
	        	<ul class="wxw-product-ul">
					<li>
						<a href="/phone/gotoProductInfo.action?productId=${appGoodsInfor.goodsId}" >
							<div class="wxw-fl">
								<IMG style="width:106px;height:100px;" src="${appGoodsInfor.mainImageUrl}" >
							</div>
							<div class="wxw-fr">
								<h2>${appGoodsInfor.goodsName}</h2>
								<p class="price">积分：<span>${appGoodsInfor.price}</span></p>
								<span class="number">数量:${appGoodsInfor.num}</span>
								<input type="hidden" id="goodsId" name = "goodsId" value="${appGoodsInfor.goodsId}"/>
								<input type="hidden" id="goodsName" name = "goodsName" value="${appGoodsInfor.goodsName}"/>
								<input type="hidden" id="price" name = "price" value="${appGoodsInfor.price}"/>
								<input type="hidden" id="num" name = "num" value="${appGoodsInfor.num}"/>
								<input type="hidden" id="pictureURL" name="pictureURL"  value="${appGoodsInfor.mainImageUrl}" />
							</div>
						</a>
					</li>
				</ul>
			</c:forEach>
        </div>
        <!-- 显示商品 -->

<div style="margin-bottom: 80px;"></div>
<!--底部浮层begin-->
<script type="text/javascript">
	/* //判断导航选中
	$(function(){
		cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	}); */

	function cartNum(){
		var userId = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryByCarInfo=key",
		    type:"POST",
		    async:"false",
		    datatype:"JOSN",
		    data:{userId:userId},
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.obj;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
    					width:"22px",
    					height:"22px",
    					padding:"10px"
    				});
		    		$("#cartNum").animate({
    					width:"20px",
    					height:"20px",
    					padding:"0"
    				});
		    	}
		    }
		});
	}
</script> 
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.scrollUp.js" ></script>
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/scrollup.css" />
<script type="text/javascript">
    /*回到顶部JS*/
    $.scrollUp();
</script>
<!--回到顶部end-->
</BODY>
</HTML>

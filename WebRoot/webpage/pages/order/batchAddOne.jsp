<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});

</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
			<td class="value" width="35%" >
    		  <input type="hidden" id="pkId" name="pkId" value="${appUserInfor.pkId}" />
    	  </td>
		</tr>
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>用户名称:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="userName" readonly="readonly" name="userName" value="${appUserInfor.userName}"/>
    	  </td>
    	   <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>身份证号:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="idcardNumber" readonly="readonly" name="idcardNumber" value="${appUserInfor.idcardNumber}"/>
    	  </td>
    	</tr>
    	<tr>
		  	<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>原积分数量:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    		  <input class="inputxt" id="integral" readonly="readonly" name="integral" value="${appUserInfor.integral}"/>
    	  	</td>
    	  	<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>增加积分数量:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    		  <input class="inputxt" id="remark2" onkeypress="notNull(this);" name="remark2"  datatype="n1-10" errormsg="请输入1-10位的数字"  nullmsg="请输入1-10位的数字"/><span style="color:red">*</span>
    	  	</td>
    	</tr>
    	<tr>
		  	<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>积分类型:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    	  		<select name = "remark1" id = "remark1" >
    	  			<c:forEach items="${codeList}" var="code">
    	  				<option value="${code.codeValue}">${code.chineseName}</option>
    	  			</c:forEach>
    	  		</select><span style="color:red">*</span>
    	  	</td>
    	  		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>产品/活动名称:</span></label>
    	  	</td>
    	  	<td class="value" width="35%" >
    		  <input class="inputxt" id="remark4"  name="remark4" onkeypress="notNull(this);"datatype="*4-100" errormsg="请输入4-100位内的字符"  nullmsg="请输入4-100位内容！"/><span style="color:red">*</span>
    	  	</td>
    	  	
    	</tr>
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>增加原因:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	      <textarea rows="4" cols="8" style="width:260px;" onkeypress="notNull(this);" name="remark3" class="content" id="remark3" 
    	       datatype="*4-100" errormsg="请输入4-100位内的字符"  nullmsg="请输入4-100位内容！">${helpDocument.content}</textarea>
    	      <span style="color:red">*</span>
    	  </td>	
    	</tr>
 	    <tr>
           <td align="center" colspan="8">
              <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/batchAddController.do?saveUpdateOne=key')">保存</a></span>
		      <span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	       </td>
        </tr>
    </table>
  </form>
</body> 
</html>

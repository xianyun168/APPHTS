<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN"><head>
	
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="email=no">
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="X-UA-Compatible" content="IE=10.000">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="format-detection" content="telephone=yes">

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.ico">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css">
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css">
	<title>全部订单</title>
	<script type="text/javascript">
		function getOrderList(){
			var user = $("#userId").val();
			$.ajax({
				type : "POST",
				url :  "${ctx}/orderInforController.do?getOrderList=key",
				data:{userId:user},		
				dataType : "json",
				success : function(data) {
					var dataObj=eval(data);
					console.log(data)
					var html = "";
					for(var i=0;i<dataObj.length;i++){
						if(dataObj[i].expressName =="" || dataObj[i].expressNumber ==""){
							dataObj[i].expressName = "快递单号";
							dataObj[i].expressNumber = "暂未生成";
						}
						html +="<li id='dd_lipadd' style='background-color: #fff;'>";
						html +="<div id='qbdd_libj' style='padding: 2%;width: 96%;background-color:#fff;border-bottom:1px solid #dedede;'>";
						/* html +="<img src='${ctx}/common/js/jquery/imgs_init/dd_img_03.png'>"; */
						html +="<p></p><div style='float: left;line-height: 22px;font-size:14px;color: #333;'>"+dataObj[i].expressName+"：<span style='color: #333;'>"+dataObj[i].expressNumber+"</span></div>";
						html +="<div style='float: right;margin-right: 10px;line-height: 22px;color: #ae8f46;font-size:14px;'> "+dataObj[i].orderState+"</div><p></p>";
						html +="</div>";
						html +="<input id='totalOrdersNo' value='' type='hidden'>";
						html +="<div style='width:100%; height:auto;'>";
						html +="<div class='f_list_l' style='padding-left:10px; width:22%;'>";
						html +="<a href='${ctx}/orderInforController.do?orderDel=key&ordersId="+dataObj[i].orderId+"&userId="+$("#userId").val()+"' style='position: relative;'>";
						html +="<img src='"+dataObj[i].mainImageUrl+"' style='margin-top:10px; width:68px; height:68px;'>";
						html +="</a></div>";
						html +="<div class='f_list_r' style='padding-right:10px; height:auto; width:63%;'>";
						html +="<h3 style='word-wrap:break-word;overflow:hidden; font-weight: normal;font-size:16px;'>"+dataObj[i].goodsName+"</h3>";
						html +="<p class='qb_dd' style='font-size:14px;color:#ae8f46;margin-top: 20px;'><span class='qbdd_jge text_color_main' style='color: #555;'>＊"+dataObj[i].goodsNum+"</span>"+dataObj[i].orderprice+"积分</p>";
						html +="</div><div style='clear:both;'></div></div>";
						html +="<div class='qbding_anniu' id='operates_0' style='     border-top: 1px solid #dedede; margin-bottom: 20px;padding-top: 10px;'>";
						html +="<div style='padding: 0 2%;color: #777;    color: #777;'>订单号:<span>"+dataObj[i].orderId+"</span></div>";
						html +="</div>";
						html +="<div style='clear:both;'></div>";
						html +="</li>";
					}
					$("#f_list").html(html);
				}
			})
		}
		$(function() {
			getOrderList();
			cartNum();
		});
		function turnCheck(_this){//取消订单
			window.location.href="/phone/customer/order/cancelOrder.action?orders.ordersId="+$(_this).attr("ordersId")+"&ordersState="+$(_this).attr("ordersState");
		}
		function affirmOption(_thisAffirm){//确认收货
			window.location.href="/phone/customer/order/confirmOrder.action?orders.ordersId="+$(_thisAffirm).attr("ordersId")+"&ordersState=5";
		}
		function toReturnProduct(_thisReturn){
			window.location.href="/phone/customer/phoneCustomerComplaints/gotoCustomerComplaintsPage.action?ordersId="+$(_thisReturn).attr("ordersId");
		}
	</script>
</head>
<body>
<div>
	<div class="new-header bg_common">
		<a class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></a>
		<h2 style=" border-bottom:1px #dedede solid;">		
			全部订单
		</h2>
<!-- 		<p class="new-a-kefu" id="kefu"><span ></span></p> -->
	</div>
</div>
<!--主体-->
<div class="main">
	<!-- 隐藏的用户id -->
	<input type="hidden" value="${userId}" id="userId" name="userId">
	<input name="ordersState" value="" id="ordersState" type="hidden">
	<section class="f_list i_list i_list_h">
		<ul class="clearfix" id="f_list">
				
		</ul>
	</section>
	<form id="formModule" action="/phone/customer/gotoOrder.action" method="post">
		<input id="currentPage" name="currentPage" value="1" type="hidden">
		<input name="ordersState" value="" id="ordersState" type="hidden">
		<input name="settlementStatus" value="" id="settlementStatus" type="hidden">
		<input name="ordersId" value="" id="ordersId" type="hidden">
	</form>
	<!-- 翻页 -->
	<!-- <a class="more" id="show_more" href="javascript:void(0);" page="1">加载更多</a> -->
</div>

<div style="margin-bottom: 80px;"></div>
<!--底部浮层begin-->
<footer style="width:100%;left:0;padding-top:7px;padding-bottom:6px;">
     <!-- 首页 -->
      <a class="add_bj" href="${ctx}/orderInforController.do?sendGoodsList=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/home.png" style="width: 30%;">
       	<p style="font-size:12px;color:#777;">首页</p>
    </a>
     
    <a class="add_bj" href="${ctx}/goddsAddressController.do?goddsAddressList=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/add.png" style="width: 30%;">
       	<p style="font-size:12px;color:#777;">地址簿</p>
    </a>
    <!-- 订单 -->
    <a class="indent_bj" href="${ctx}/orderInforController.do?add=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/indent.png" style="width: 30%;"/>
        <p style="font-size:12px;color:#777;">我的订单</p>
    </a>
	<!-- 图标 -->
	<a class="shopping_bj car_bj" href="${ctx}/orderInforController.do?shoppingCarList=key&userId=${userId}">
		<img src="${ctx}/common/js/jquery/imgs_init/car.png" style="width: 30%;" />
		<p style="font-size:12px;color:#777;">购物车</p>
		<span id="cartNum"></span>
	</a>
	
</footer>
<script type="text/javascript">
	//判断导航选中
	$(function(){
		//cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	});

	function cartNum(){
		var userId = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryByCarInfo=key",
		    type:"POST",
		    async:"false",
		    data:{userId:userId},
		    datatype:"JOSN",
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.obj;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
    					width:"22px",
    					height:"22px",
    					padding:"10px"
    				});
		    		$("#cartNum").animate({
    					width:"20px",
    					height:"20px",
    					padding:"0"
    				});
		    	}
		    }
		});
	}
	
	$('#kefu').click(function(){
		sj_alert("400-177-888",'',['<a href="tel://400177888">一键呼叫</a>','qux'],function(){
			$('.hide').style.display='none'
			$('.hide').style.opacity='0'
		})
	})
</script>
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/jquery.scrollUp.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.cunetong.cn/resource/phone_front/phone/css/scrollup.css">
<script type="text/javascript">
    /*回到顶部JS*/
    //$.scrollUp();
</script><a id="scrollUp" href="#top" title="回到顶部" style="display: none; position: fixed; z-index: 20000;"></a>
<!--回到顶部end-->

</body></html>
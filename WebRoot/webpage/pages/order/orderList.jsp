<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<title>分类列表页面</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="email=no">
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="X-UA-Compatible" content="IE=10.000">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.ico">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css">
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/iconfont.css">
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css">



<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>



<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/m_category.css">
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/m_gwc.css">
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/m_global_site_base.css">
<style>
	.disno{
		display:none;
	}
	
</style>
<script type="text/javascript">
	$(function(){
		userInfo();
		loadingList();
	});
// 	$(function(){
// 		$(".wxw-gwc").click(function(event){
// 			event.stopPropagation();
// 			event.preventDefault();
// 			//产品ID
// 			var productId = $(this).attr("data-id");
// 			//商品信息ID
// 			var shopInfoId = $(this).attr("data-shopInfoId");
// 			var num = "1";
// 			//库存量单位
// 			var sku = $(this).attr("data-sku");

// 			$.ajax({
// 	    		url:"/phone/shoppingCar/ajaxProductToShoppCart.action",
// 	    		type:"post",
// 	    		datatype:"josn",
// 	    		data:{productId:productId,num:num,shopInfoId:shopInfoId,sku:sku},
// 	    		success: function (data) {
//                     var data=$.parseJSON(data);
// 			        var result=data.result;
// 			        var msg=data.msg;
// 	    			if(result){
// 	    				cartNum();
// 	    				$(".wxw-ts").css("display","block");
// 	    				setTimeout("timeCount()",2000);
// 	    			}else{
// 	    				sj_alert(msg,"请您登录后购买");
// 	    			}
// 	             }
//     	     });
// 		});
		
// 		 function timeCount(){
// 		    	$(".wxw-ts").css("display","none");
// 		 }

// 	 	$(".list_box_hidd").attr("style","display:none;");
// 		var i=$(".typeIndex").val();
// 		if($(".typeIndex").val()==2185){

// 			goFirstType(2185,2185);
// 		}
// 		if($(".typeIndex").val()==2125){

// 			goFirstType(2125,2125);
// 		}
// 		if($(".typeIndex").val()==2126){

// 			goFirstType(2126,2126);
// 		}
// 		if($(".typeIndex").val()==2127){

// 			goFirstType(2127,2127);
// 		}
// 		if($(".typeIndex").val()==2189){

// 			goFirstType(2189,2189);
// 		}
// 		if($(".typeIndex").val()==2190){

// 			goFirstType(2190,2190);
// 		}
// 		if($(".typeIndex").val()==2205){

// 			goFirstType(2205,2205);
// 		}

// 	}); 
// 	//点击一级分类
// 	 function goFirstType(productTypeId,index){
// 		$(".cur").attr("class","item");
// 		$("#productTypeId_"+productTypeId).attr("class","item cur");//设置选中一级分类

// 		/************设置二、三级分类显示 begin*******************/
// 		$(".list_box").each(function(){
// 			$(this).addClass("list_box_hidd");
// 			$(this).removeAttr("style");
// 		});
// 		$(".sec_"+productTypeId).each(function(){
// 			$(this).removeClass("list_box_hidd");
// 		});
// 		$(".list_box_hidd").attr("style","display:none;");
// 		/************设置二、三级分类显示 end*******************/
// 	} 
	
	function loadingList(){
// 		var start = 1;
// 		var end = 10;
		var userId = $("#userId").val();
		$.ajax({
    		url:"${ctx}/orderInforController.do?queryGoodsList=key",
    		type:"post",
    		datatype:"josn",
    		async: false,
    		data:{userId:userId},
    		success: function (data) {
    			 $(document).ready(function(){
    				 $('.box').empty();
    				 var da=$.parseJSON(data);
    	                for(var i=0;i<da.obj.length;i++){
    	                var p = '<p class="wxw-shoppinglistH" style="position:relative;width:98%; overflow: hidden; text-align: center;    border: 1px solid #dedede; line-height:24px;   ">'+ '<img src="'+da.obj[i].mainImageUrl+'" alt="" style="height:85%;width:100%; display:inline-block;" onerror="this.src=${ctx}/common/js/jquery/imgs_init/mrbj.png"></p>';
    	                var div1 = '<div style="padding-left: 3%; text-align:left; white-space: normal;height:20px; line-height:20px; font-size:12px; display: -webkit-box;-webkit-box-orient: vertical;-webkit-line-clamp: 1;overflow: hidden;display:none;">'+567+'</div>';
    	                var div2 = '<div style="font-size: 12px; font-weight:600; padding-left:6%;margin-top:2px; color:#999; line-height: 2em;"></div>';
    	                var strong = '<p style="font-size: 12px; color:#ff0000; line-height: 2em; position:absolute; left:6%;"><span style="color: #333;">'+da.obj[i].goodsName+'</span><br><span style="color: #ae8f46;">'+da.obj[i].price+' 积分</span></p>';
    	                var img = "<img style='width:46%;position:relative;top:0px;right:-20px;opacity:0.6;' src='${ctx}/common/js/jquery/imgs_init/che.png' alt='che'  onerror='this.src=${ctx}/common/js/jquery/imgs_init/mrbj.png'>";

    					//var div3 = '<div class="wxw-gwc" id="wxw-che" data-id="'+da.obj[i].pkId+'" data-title="" data-pic="'+da.obj[i].mainImageUrl+'" data-guige="约600g/份" data-price="16.80" data-shopinfoid="'+da.obj[i].goodsId+'" data-sku="'+da.obj[i].surplus+'" style="width:60px;height:40px;position:absolute;bottom:0;right:0px;">'+img+'</div>';
    					$("#aaa").append("<a href='${ctx}/orderInforController.do?goodsDetails=key&productId="+da.obj[i].goodsId+"&userId="+$('#userId').val()+"' title='' style='height:auto; text-align:left; padding-bottom:9%;height: 154px;'>"+p+div1+div2+strong+"</a>");
    					$(".wxw-gwc").unbind("click").click(function(event){
    						event.stopPropagation();
    						event.preventDefault();

    						var productId = $(this).attr("data-id");
    						var shopInfoId = $(this).attr("data-shopInfoId");
    						//var num = "1";
    						var sku = $(this).attr("data-sku");
    						var userId = $("#userId").val();
    						$.ajax({
    				    		url:"${ctx}/orderInforController.do?queryCarList=key",
    				    		type:"post",
    				    		datatype:"josn",
    				    		data:{productId:productId,shopInfoId:shopInfoId,sku:sku,userId:userId},
    				    		success: function (data) {
    			                    var data=$.parseJSON(data);
    						        var result=data.success;
    						        var msg=data.msg;
    				    			if(result){
    				    				cartNum();
    				    				$(".wxw-ts").css("display","block");
    				    				setTimeout("timeCount()",2000);
    				    			}else{
    				    				sj_alert(msg,"请您登录后购买");
    				    			}
    				             }
    			    	     });
    					});
    	                }
    			
    		     });

             }
	     });
	};
	
	//查询
	function queryGoodsName(){
		var goods = $("#goodsName").val();
		$.ajax({
    		url:"${ctx}/orderInforController.do?queryGoodsList=key",
    		type:"post",
    		datatype:"josn",
    		async: false,
    		data:{goodsName:goods},
    		success: function (data) {
    			 $(document).ready(function(){
    				 var da=$.parseJSON(data);
    				 if(parseInt(da.obj.length) == parseInt(0)){
    					 $("#aaa").empty();
    					 return;
    				 }
    				 $("#aaa").empty();
    	                for(var i=0;i<da.obj.length;i++){
    	                	 var p = '<p class="wxw-shoppinglistH" style="position:relative;width:99%; overflow: hidden; text-align: center;    border: 1px solid #dedede; line-height:24px;">'+ '<img src="'+da.obj[i].mainImageUrl+'" alt="" style="height:85%;width:100%; display:inline-block;" onerror="this.src=${ctx}/common/js/jquery/imgs_init/mrbj.png"></p>';
    	    	                var div1 = '<div style="padding-left: 3%; text-align:left; white-space: normal;height:20px; line-height:20px; font-size:12px; display: -webkit-box;-webkit-box-orient: vertical;-webkit-line-clamp: 1;overflow: hidden;display:none;">'+567+'</div>';
    	    	                var div2 = '<div style="font-size: 12px; font-weight:600; padding-left:6%;margin-top:2px; color:#999; line-height: 2em;"></div>';
    	    	                var strong = '<p style="font-size: 12px; color:#ff0000; line-height: 2em; position:absolute; left:6%;"><span style="color: #333;">'+da.obj[i].goodsName+'</span><br><span style="color: #ae8f46;">'+da.obj[i].price+' 积分</span></p>';
    	    	                var img = "<img style='width:46%;position:relative;top:0px;right:-20px;opacity:0.6;' src='${ctx}/common/js/jquery/imgs_init/che.png' alt='che'  onerror='this.src=${ctx}/common/js/jquery/imgs_init/mrbj.png'>";

    	    					//var div3 = '<div class="wxw-gwc" id="wxw-che" data-id="'+da.obj[i].pkId+'" data-title="" data-pic="'+da.obj[i].mainImageUrl+'" data-guige="约600g/份" data-price="16.80" data-shopinfoid="'+da.obj[i].goodsId+'" data-sku="'+da.obj[i].surplus+'" style="width:60px;height:40px;position:absolute;bottom:0;right:0px;">'+img+'</div>';
    	    					//$("#aaa").append("<a href=${ctx}/orderInforController.do?goodsDetails=key&productId='"+da.obj[i].goodsId+"' title='店长推荐 冰川七号有机西红柿 [一份]' style='height:auto; text-align:left; padding-bottom:9%;'>"+p+div1+div2+strong+div3+"</a>");
    	    					//var ccc = "<a href='${ctx}/orderInforController.do?goodsDetails=key&productId="+da.obj[i].goodsId+"' title='店长推荐 冰川七号有机西红柿 [一份]' style='height:auto; text-align:left; padding-bottom:9%;'>"+p+div1+div2+strong+div3+"</a>";
    	    					$("#aaa").append("<a href='${ctx}/orderInforController.do?goodsDetails=key&productId="+da.obj[i].goodsId+"&userId="+$('#userId').val()+"' title='' style='height:auto; text-align:left; padding-bottom:9%;height: 154px;'>"+p+div1+div2+strong+"</a>");
    	    					$(".wxw-gwc").unbind("click").click(function(event){
    	    						event.stopPropagation();
    						event.preventDefault();

    						var productId = $(this).attr("data-id");
    						var shopInfoId = $(this).attr("data-shopInfoId");
    						//var num = "1";
    						var sku = $(this).attr("data-sku");
    						var userId = $("#userId").val();
    						$.ajax({
    				    		url:"${ctx}/orderInforController.do?queryCarList=key",
    				    		type:"post",
    				    		datatype:"josn",
    				    		data:{productId:productId,shopInfoId:shopInfoId,sku:sku,userId:userId},
    				    		success: function (data) {
    			                    var data=$.parseJSON(data);
    						        var result=data.success;
    						        var msg=data.msg;
    				    			if(result){
    				    				cartNum();
    				    				$(".wxw-ts").css("display","block");
    				    				setTimeout("timeCount()",2000);
    				    			}else{
    				    				sj_alert(msg,"请您登录后购买");
    				    			}
    				             }
    			    	     });
    					});
    	                }
    			
    		     });

             }
	     });
	};
	
	function userInfo(){
		var user = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryUserInfo=key",
    		type:"post",
    		datatype:"josn",
    		async: false,
    		data:{userId:user},
    		success: function (data) {
    			var data=$.parseJSON(data);
    			var usr = data.obj;
    			if(usr != null){
    				if(usr.customerLevel =='1'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/one.png'); 
    				}else if(usr.customerLevel =='2'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/two.png'); 
    				}else if(usr.customerLevel =='3'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/three.png'); 
    				}else if(usr.customerLevel =='4'){
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/four.png');
    				}else{
    					$("#levelImg").attr('src','${ctx}/common/js/jquery/imgs_init/five.png');
    				}
    			$("#level").html(usr.customerLevel);
    			$("#integra").html(usr.integral);
    			}
    		}
		})
	}
</script>
</head>

<body>
<!-- 	<input type="button" value="测试" onclick="userInfo();"> -->
	<input id="userId" name="userId" type="hidden" value="${userId}">
	<div class="wxw-ts">已成功加入购物车</div>
	
	<div id="container">
		<div class="touchweb_page-goodsCategory">
			<div class="tab_wrap">
				<input class="typeIndex" value="2124" type="hidden">
				<!-- *********************上下分开******************** -->
				<div class="tab_box" data-tpa="H5_PROD_CATEGORY_RIGHT">
					<div class="list cur">
					<div class="list_box sec_2124">
					<div class=" integral">
								<%-- 	<img alt="" class="bg" src="${ctx}/common/js/jquery/imgs_init/bg.png"> --%>
									<div class="con" style="text-align: center;">
										<div style="padding-top:10px;    padding-bottom: 10px;">
											<img id="levelImg" alt="" src="${ctx}/common/js/jquery/imgs_init/one.png" style="width: 19%;">
											<p style="font-size: 12px; color: #78484f;padding-top: 5px;"> <span id="level"></span>星用户 </p>
											<p style="font-size: 24px;font-weight: bold; color: #78484f;padding-top: 12px;" id="integra">  </p>
											<p style="font-size: 10px; color: #ae8b6f;padding-top: 4px;"> 我的积分 </p>
											<div style="position: absolute;right: 0px;top: 22px; width: 100%;">
													<a href="${ctx}/orderInforController.do?integralDetails=key&userId=${userId}"><img style="position: absolute;right: 0px;top: 22px;width: 25%;" alt="" src="${ctx}/common/js/jquery/imgs_init/mc.png">
													<p style="position: absolute; right: 14px;top: 34px;color: #78482f;">积分明细</p></a>
												
											</div>
										</div>
										<div style="width: 100%; text-align: center;">
											<ul>
												<li style="float: left;width: 43%;padding: 3%; border-top: 1px solid #e6d0a5;border-right: 1px solid #e6d0a5;"><a href="${ctx}/orderInforController.do?IntegralRules=key&title=01" style="color:#78482f;">积分计算规则</a></li>
												<li style="float: left;width: 43%;padding: 3%; border-top: 1px solid #e6d0a5;"><a href="${ctx}/orderInforController.do?returnRules=key&title=02" style="color:#78482f;">退换货规则提示</a></li>
											</ul>
										</div>
										<div style="    clear: both;"></div>
									</div>
									
										
									</div>
								</div>
								<div class="list_box sec_2124 ">
								
									<div style="text-align: center;padding: 10px;font-size: 16px;color: #101010;font-weight: bold;">--积分好礼--</div>
										<!-- <label>商品名称：</label> -->
										<div class="qbdd_first" style="padding:10px; position: relative;height: 30px;">
											<div style="width:100%;text-align:left;">
												<i class="iconfont icon-02" style="position: absolute;font-size: 20px; color: #777;top: 15px;left: 15px;"></i><input type="text" id="goodsName" placeholder="请输入商品名称或关键字" name="goodsName" style="border-radius: 3px; border: 0px;background-color: #efefef;width: 78%;padding: 15px 30px 15px;color: #777;"/><i class="iconfont icon-guanbi" id="sc" style="position: absolute;font-size: 22px; padding-top: 4px;color: #777;left: 69%;"></i>
												<input type="button" value="搜索" onclick="queryGoodsName();" style="    background-color: #fff; color: #777;font-size: 14px;padding: 4% 5%;line-height: 1px;"/>
											</div>
											
										</div>
										 
									<div class="box" data-tpc="category3_0" id="aaa">
								
										
									</div>
								</div>
							
								<div class="list_box sec_2124 ">
									
									<div class="box" data-tpc="category3_0">

										
									</div>
								</div>
							
							
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--底部浮层begin-->


<script>
     $(function(){	   
		   var price=$("#price_des").val();
		   var cot=$("#itemAmount").val();
		   var contprice=Number(price*cot).toFixed(2);
	       $("#price").html(contprice);
	 })
    //cart-close
    $(".cart-close").click(function(){
        $(".wxw-cartmask").css("display","none");
    });
    //wxw-cartmask
    $(".wxw-cartmask").click(function(){
        $(".wxw-cartmask").css("display","none");
    });
    $(".cart").click(function(){
        event.stopPropagation();
    });
    //input增加
    $(".item-amount span").click(function() {
        var count=$(this).attr("data");
        console.log(count);
        $("#itemAmount").val(function() {
            var value = $(this).val();
            count=="-"?value--:value++;
            if ( value>1 ) {
                $(".item-amount .minus").removeClass("no-minus");
                return value;
            }
            else if ( value=1 ){
                $(".item-amount .minus").addClass("no-minus");
                var value = 1;
                return value;
            }
        })
         var cont=$("#itemAmount").val();
         var price=$("#price_des").val();   
         var contprice=Number(price*cont).toFixed(2);
          $("#price").html(contprice);
    });
    function gotoCart(){
    	addtoCart();
    	window.location.href ="/phone/gotoCart.action";
    }
    function addtoCart(){
    	var productId=$("#quick_productId").val();
    	var num=$("#itemAmount").val();
    	var shopInfoId=$("#quick_shopId").val();
    	var sku=$("#quick_sku").val();
    	$.ajax({
    		url:"/phone/shoppingCar/ajaxProductToShoppCart.action",
    		type:"post",
    		datatype:"josn",
    		data:{productId:productId,num:num,shopInfoId:shopInfoId,sku:sku},
    		success: function (data) {
    			if(data){
    				cartNum();
    				$("#itemAmount").val(1);
    				$(".wxw-cartmask").css("display","none");
    				$(".wxw-ts").css("display","block");
    				t=setTimeout("timeCount()",2000); 
    				//stopCount()
    			}
    		}
    	});
    	
    }
    function timeCount(){
    	$(".wxw-ts").css("display","none");
    	
    }
    function stopCount() {
		clearTimeout(t);
	}
</script>
	

<div style="margin-bottom: 80px;"></div>
<!--底部浮层begin-->
<footer style="width:100%;left:0;padding-top:7px;padding-bottom:6px;">
     <!-- 首页 -->
      <a class="add_bj" href="${ctx}/orderInforController.do?sendGoodsList=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/home.png" style="width: 30%;">
       	<p style="font-size:12px;color:#777;">首页</p>
    </a>
     
    <a class="add_bj" href="${ctx}/goddsAddressController.do?goddsAddressList=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/add.png" style="width: 30%;">
       	<p style="font-size:12px;color:#777;">地址簿</p>
    </a>
    <!-- 订单 -->
    <a class="indent_bj" href="${ctx}/orderInforController.do?add=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/indent.png" style="width: 30%;">
        <p style="font-size:12px;color:#777;">我的订单</p>
    </a>
	<!-- 图标 -->
	<a class="shopping_bj car_bj" href="${ctx}/orderInforController.do?shoppingCarList=key&userId=${userId}">
		<img src="${ctx}/common/js/jquery/imgs_init/car.png"style="width: 30%;" >
		<p style="font-size:12px;color:#777;">购物车</p>
		<span id="cartNum"></span>
	</a>
	
</footer>
<script type="text/javascript">
	//判断导航选中
	$(function(){
		cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	});

	function cartNum(){
		var userId = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryByCarInfo=key",
		    type:"POST",
		    async:"false",
		    data:{userId:userId},
		    datatype:"JOSN",
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.obj;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
    					width:"22px",
    					height:"22px",
    					padding:"10px"
    				});
		    		$("#cartNum").animate({
    					width:"20px",
    					height:"20px",
    					padding:"0"
    				});
		    	}
		    }
		});
	}
</script>
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.scrollUp.js"></script>
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/scrollup.css">
<script type="text/javascript">
    /*回到顶部JS*/
    //$.scrollUp();
</script><a id="scrollUp" href="#top" title="回到顶部" style="display: none; position: fixed; z-index: 20000;"></a>
<!--回到顶部end-->
<script>
$(document).ready(function () {
	   $('body').height($('body')[0].clientHeight);
	   
	/*   $("#goodsName").focus(function(){
		  var t = $(window).scrollTop();
		    $('body,html').animate({'scrollTop':t+200},100)
		 }); */
	  /* $("#goodsName").blur(function(){
		   $("footer").removeClass("disno");
		 }); */
	   $('#sc').click(function(){
		  // $('#goodsName').value='';
		   $('#goodsName').val("");
		// loadingList();
		
	   })
	});

$(window).resize(function(){
/* alert($(window).height()) */
if($(window).height() < 400){
	 $("footer").addClass('disno');
}else{
	 $("footer").removeClass("disno");
}

});


</script>
<!--底部 新闻及介绍暂时隐藏-->
	

	<!--底部浮层end-->


</body></html>
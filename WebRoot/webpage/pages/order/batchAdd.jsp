<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});

</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>积分数量:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="integral" onkeypress="notNull(this);" name="integral"  datatype="n1-10" errormsg="请输入1-10位内的数字"  nullmsg="请输入1-10位数字！"/><span style="color:red">*</span>
    	  </td>
    	
    	</tr>
     <tr>
     <td align="center" colspan="8">
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/batchAddController.do?saveUpdate=key')">保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body> 
  
  
  
  

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN"><head>
<title>订单详情</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="format-detection" content="telephone=no">
<meta name="format-detection" content="email=no">
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta http-equiv="X-UA-Compatible" content="IE=10.000">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/common/js/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="http://www.cunetong.cn/resource/phone_front/common/imgs_init/sj.ico">
<link type="text/css" rel="stylesheet" href="http://www.cunetong.cn/resource/phone_front/phone/css/base2013.css">
<link type="text/css" rel="stylesheet" href="http://www.cunetong.cn/resource/phone_front/phone/css/style.css">
<script type="text/javascript" src="https://js.users.51.la/19416443.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/plugin/dialog/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="http://www.cunetong.cn/resource/phone_front/plugin/dialog/common.css">



<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/common/js/jquery.cookie.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/main.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/jquery.form.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/plugin/validate/jquery.metadata.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/plugin/validate/jquery.validate.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/plugin/validate/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/plugin/validate/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/plugin/validate/jquery.validate.CardIdNo.js"></script>




<link type="text/css" rel="stylesheet" href="http://www.cunetong.cn/resource/phone_front/phone/css/modal.css">
<link type="text/css" rel="stylesheet" href="http://www.cunetong.cn/resource/phone_front/phone/css/slider-common.css">

<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/jquery.touchSlider.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/jquery.tabs.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/jquery.lazyload.min.js"></script>

<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/common/js/common.js"></script>
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/common/js/json2.js"></script>

	<style type="text/css">
	.p_iconYin{text-align:center;background:url(phone/img/titleModle.jpg);color:#fff;line-height:28px;width:172px;height:36px;}
	table{border:1px #D8D8D8 solid;padding:10px; width:100%;}
	table tr{border:1px #D8D8D8 solid;padding:10px;}
	table tr th{border:1px #D8D8D8 solid;padding:8px;vertical-align: middle;background-color: #F2F2F2; font-weight: normal;}
	table tr td{border:1px #D8D8D8 solid;padding:8px;vertical-align: middle;text-align: center;}
	table tr td a{color:#084B8A;}
	table tr td a:hover{color:red;}
	.ClearBoth{width:auto;margin-right:20px;}
	</style>
<link type="text/css" rel="stylesheet" href="http://www.cunetong.cn/resource/phone_front/phone/css/login.css">
<link type="text/css" rel="stylesheet" href="http://www.cunetong.cn/resource/phone_front/phone/css/order.css">

	<script type="text/javascript">
		$(document).ready(function() {
			//通过浏览器类型更改支付方式
			var useragent = navigator.userAgent;//获取浏览器类型
			if (useragent.match(/MicroMessenger/i) == 'MicroMessenger') {//判断是否是微信内置浏览器  如果是则进入
				$("#pay_wx").css("display","block");
			}else{
				$("#pay_zfb").css("display","block");
			}
			$("#shipping").click(function(){
				$('#shipping-overlay').overlay({
							mask: {
					        color: '#ebecff',
					        loadSpeed: 200,
					        opacity: 0.4
					      }, closeOnClick: false
				});
				$('#shipping-overlay').overlay().load();//加载浮层
	 		});
	 		//跳转到商品详情页
			$(".productImg").click(function(){
	   			var id = this.id;
	   			var pType = this.name;
	   			window.location="/shopFront/queryProductInfo.do?pType="+pType+"&productId="+id;
	   		});
	 	});
		//去支付
		function toPay(orderNo){
			var htm="";
			htm+="&nbsp;&nbsp;正在提交，请稍等...";
			$("#submitImgLoad").html(htm);
			$.ajax({
				url:"/phone/customer/getOrders.do",
				dataType:"json",
				type:"post",
				data:{ordersNo:orderNo},
				success:function(data){
					$("#WIDout_trade_no").val(data.ordersNo);
					$("#WIDsubject").val(unescape(data.subject));
					$("#WIDtotal_fee").val(data.payAmount);
					//$("#WIDbody").val(data.subject);
					//$("#WIDreceive_name").val(data.consignee);
					//$("#WIDreceive_address").val(data.address);
					//$("#WIDreceive_zip").val(data.postCode);
					//$("#WIDreceive_phone").val(data.phone);
					//$("#WIDreceive_mobile").val(data.mobilePhone);
					$("#alipayment").submit();
				}
			 });
		}

		//微信支付
		function toWxPay(orderNo){
			var openid = '';
	    	if(""==openid||null==openid){
	    		sj_alert("获取用户标识失败，请重新登录！");
	    		location.href = "/phone/gotoLogin.do";
	    	}else{
	    		var htm="";
	    		htm+="&nbsp;&nbsp;正在提交，请稍等...";
	    		$("#submitImgLoad").html(htm);
				window.location="/phone/customer/shoppingOnline/toWxPay.do?ordersNo="+orderNo;
	    	}
		}
	</script>
</head>

<body>
    <div>
		<div class="new-header bg_common">
		 <a class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></a>
		 <h2>
		 	订单详情
		 </h2>
		</div>
	</div>
<div class="margin-center PublicWidth1004">
	<ul class="orders">
    		<li>
    			<div class="hx"></div>
    			<div class="center1">
    				<p>订单编号：20180411202652181607</p>
    				<p>订单金额：￥28.60</p>
    				<p>订单日期：2018-4-11 20:26:52</p>
    				<p>订单状态：
						订单取消
    				</p>
    			</div>
    		</li>

    		<li>
    			<div class="hx"></div>
    			
	    			<div class="center2">
	    				<div class="left">
	    					<img src="http://www.cunetong.cn/images/cyt/shop/image/product/20180327/2018032713330005871544.jpg">
	    				</div>
	    				<div class="right">
	    					<p style="color: #000;">店长推荐 冰川七号有机西红柿 [一份]</p>
		    				<p class="text_color_main">￥16.80</p>
		    				<p>x1</p>
	    				</div>
	    				<div style="clear: both;"></div>
	    			</div>
    			
	    			<div class="center2">
	    				<div class="left">
	    					<img src="http://www.cunetong.cn/images/cyt/shop/image/product/20180320/2018032014360000060512.jpg">
	    				</div>
	    				<div class="right">
	    					<p style="color: #000;">57优鲜 大叶生菜 </p>
		    				<p class="text_color_main">￥11.80</p>
		    				<p>x1</p>
	    				</div>
	    				<div style="clear: both;"></div>
	    			</div>
    			
    		</li>
          
    		 
    		<li>
    			<div class="hx"></div>
    			<div class="center3">
    				<div class="top">
    					<span class="left">车晓娜</span>
    					<span class="right">17610320838</span>
    				</div>
    				<div class="down">
						北京市,东城区,内环到三环里,海淀区门头馨村北二区11号楼二单元401室
    				</div>

    			</div>
    		</li>
    		
    		
			
		

    		<li>
    			<div class="hx"></div>
    			<div class="center3" style="height: auto;">
    				<div class="top">
    					<span class="left">付款方式：</span>
    					<span class="right">
    						支付宝
							
    					</span>
    				</div>
    				<div class="down">
						<p>折扣前总金额：
							￥28.60
							
						</p>
						<p>-商品折扣金额：
							￥0.00
							
						</p>
						<p>-优惠券抵扣金额：
							￥0.00
							
						</p>
	    				<p>+税费：
	    					￥0.00
							
	    				</p>
						<p>+运费：￥0.00</p>
	    				<p>-金币抵扣金额：
							￥0.00 
							
	    				</p>
	    				<div class="ClearBoth">
							<p class="widthpx150 text-right FontSizeB" style="line-height:40px;"><span style="font-size:16px; color:#000; font-weight: bold;">支付金额：</span><span class="text_color_main" style="font-size:20px;">￥28.60</span></p>
						</div>
    				</div>
    			</div>
    		</li>
    	</ul>
    	<div style="width:95%;margin:10px auto;height:auto;">
	    	<a class="btn_common" href="/phone/customer/gotoOrder.action?ordersState=3&amp;settlementStatus=1" style="display:block;padding:8px 0;text-align: center;width:100%;border-radius:5px;">返回</a>
    	</div>
</div>
	<!--底部浮层begin-->
	

<div style="margin-bottom: 80px;"></div>
<!--底部浮层begin-->
<footer style="width:100%;left:0;">
    <!-- 首页 -->
    <a class="home_bj" href="/phone/index.html">
        <img src="http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home.png">
    </a>
    <!-- 分类搜索 -->
    <a class="gys_bj" href="/phone/gotoProductType.action">
        <img src="http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry.png">
    </a>
	<!-- 团购 -->
	<a class="tuan_bj" href="http://m.cunetong.cn/phone/tuan/gotoTuanType.action">
		<img src="http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-tuan.png">
	</a>
	<!-- <a class="tuan_bj" href="http://m.cunetong.cn/phone/gotoProductInfo.action?productId=3743">
		<img src="http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-tuan.png" />
	</a> -->
    <!--购物车-->
    <a class="shopping_bj" href="/phone/gotoCart.action">
        <img src="http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart.png">
		<span id="cartNum" style="display: block; width: 20px; height: 20px; padding: 0px;">2</span>
    </a>
    <!--我的-->
    <a class="me_bj" href="/phone/customer/gotoCustomer.action">
        <img src="http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png">
    </a>
</footer>
<script type="text/javascript">
	//判断导航选中
	$(function(){
		cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	});

	function cartNum(){
		$.ajax({
			url:"/phone/shoppingCar/cartNum.action",
		    type:"POST",
		    datatype:"JOSN",
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.cartNum;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
    					width:"22px",
    					height:"22px",
    					padding:"10px"
    				});
		    		$("#cartNum").animate({
    					width:"20px",
    					height:"20px",
    					padding:"0"
    				});
		    	}
		    }
		});
	}
</script>
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="http://www.cunetong.cn/resource/phone_front/phone/js/jquery.scrollUp.js"></script>
<link rel="stylesheet" type="text/css" href="http://www.cunetong.cn/resource/phone_front/phone/css/scrollup.css">
<script type="text/javascript">
    /*回到顶部JS*/
    $.scrollUp();
</script><a id="scrollUp" href="#top" title="回到顶部" style="display: none; position: fixed; z-index: 20000;"></a>
<!--回到顶部end-->

<!--底部 新闻及介绍暂时隐藏-->
	

	<!--底部浮层end-->


</body></html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="OrderManagerList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
			<div>
				<label>购买人姓名：</label> 
				<input type="text" id="remark1" name="remark1" />
			</div>

			<div style="width: 66.66%;">
				<label style="width: 20%; float: left;">订单号：</label> 
				<input style="width: 27.5%; float: left; margin-left: 2%;"  type="text" id="orderId" name="orderId" />
			</div>

			<div> 
				<label class="jiekouname">购买时间：</label>
		   		<input style="width: 55.5%; margin-left: 2%;" type="text"
					id="purchaseTime"  name="purchaseTime" class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></input>
			</div>
		    
		    <div> 
 				<label >订单状态：</label>
 			   	<select id ="flag" name="type"> 
	    		 	<option  value="">--请选择--</option> 
	   		 		<option value="已发货"<c:if test="${appMessageSend.orderState=='已发货'}">selected</c:if>>已发货</option>
					<option value="未发货"<c:if test="${appMessageSend.orderState=='未发货'}">selected</c:if>>未发货</option>
 	    		</select>  
			</div>
		    
	</div>
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<%-- <span class="bluebtnhover"><a onclick="add_win('订单增加',' ${ctx}/orderInforController.do?add=key','OrderManagerList',1000,400)" id="add" plain="true" class="formbtn bluebtn">订单增加</a></span> 
			<span class="bluebtnhover"><a onclick="add_win('订单列表',' ${ctx}/orderInforController.do?sendGoodsList=key','OrderManagerList',1000,400)" id="add" plain="true" class="formbtn bluebtn">订单列表</a></span>
			<span class="bluebtnhover"><a onclick="add_win('订单增加',' ${ctx}/orderInforController.do?test=key','OrderManagerList',1000,400)" id="add" plain="true" class="formbtn bluebtn">测试</a></span>  
			<span class="bluebtnhover"><a href="${ctx}/goddsAddressController.do?goddsAddressList=key" id="add" plain="true" class="formbtn bluebtn">地址</a></span>  --%>
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#OrderManagerList').datagrid({
			idField : 'pkId',
			title : '订单管理',
		    url : ' ${ctx}/orderInforController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			/* <!--查询参数 --> */
			queryParams:{
				'remark1': $('#remark1').val(),
				'orderId': $('#orderId').val(),
				'purchaseTime': $('#purchaseTime').val(),
			    'flag': $('#flag').val() 
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'orderId',title:'订单号',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'purchaseTime',title:'购买时间',
						formatter:function(val,rec){
			        		if(val != null && val != ''){
			        			return formattimed(val);
			        			//return formattimes(val);
			        		}
			      		},
						width:fixWidth(0.15),align:'center',sortable:false},
					{field:'remark1',title:'购买人姓名',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'orderState',title:'订单状态',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'orderprice',title:'订单金额',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'expressNumber',title:'快递单号',   width:fixWidth(0.15),align:'center',sortable:false},
					{field : 'opt',title : '操作',width:fixWidth(0.2),align:'center',sortable:false,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=sendGoods('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "发货</a>]";
// 							href += "[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>";
// 							href += "详情</a>]";
									var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
										var oper = operCode.split(', ');
										var i = oper.length;
										if(oper !=""){
										  for(var j=0;j<oper.length;j++){
											
											  if(oper[j] == "send"){
													 href=href.replace("[<a href='#' onclick=sendGoods('"+rec.pkId+"') style='color:#3f99b1;'>发货</a>]","");
													}
											  /*if(oper[j] == "detail"){
												 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>详情</a>]","");
												}
											  if(oper[j] == "delete"){
												 href=href.replace("[<a href='#' onclick=dele('orderInforController.do?delete=key','OrderManagerList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
												}   */
										
											}
										}
							return href;
					}}
				    ]],
			onLoadSuccess : function(data) {
			    $("#OrderManagerList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清除所选的行
				$("#OrderManagerList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'certificateInfoList';
			}
		});
		$("#OrderManagerList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#OrderManagerList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});

function reset(){
	$('#remark1').val("");
	$('#orderId').val("");
	$('#purchaseTime').val("");
	$('#flag').val("");
}

//编辑页面
function sendGoods(pkId){
	/* var rowsData = $('#OrderManagerList').datagrid('getSelections');
	if (rowsData.length > 1) {
		tip('请选择一条数据');
		return;
	} */
	add_win('发货', '${ctx}/orderInforController.do?test=key&pkId='+ pkId,'OrderManagerList',600,200);
}

//详情页面
function detail(pkId){
	add_win('详情','${ctx}/orderInforController.do?specific=key&pkId='+pkId,'OrderManagerList',1000,400);
	
}	

//删除
function dele(url,id,pkId) {
//var rowsData = $('#'+id).datagrid('getSelections');
url += '&pkId='+pkId;
window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    if (r){
        $.post(url,function(result){
            if (result.success){
            	if(result.obj =='alert'){
            		tip(result.msg);
            	}else{
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
            	}
            } else {
                window.top.$.messager.show({// show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
        },'json');
    }
});
}

function reloadTable() {
	try {
		$('#' + MailInfoList).datagrid('reload');
		$('#' + MailInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + MailInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#OrderManagerList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

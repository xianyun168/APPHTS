<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<%@page import="java.net.*"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
function saveData(url){
	var startDate = $('#startDate').val();
	var endDate = $('#endDate').val();
	//window.location.href = url+"&startDate="+startDate+"&endDate="+endDate;
	 $.ajax({
     	type: "post",
     	url: url,
     	data: {"startDate":startDate,"endDate":endDate},
     	dataType: "json",
     	success: function(data){
     		var windowapi = frameElement.api, W = windowapi.opener;
               if(data.obj=="ok"){
            	   window.location.href ="${ctx}/readMessageController.do?excel1=key&startDate="+startDate+"&endDate="+endDate;
               }else{
            	 window.top.$.messager.progress('close');
            	 windowapi.close();
				 W.tip(data.msg);
                return;
             } 
         } 
 	}); 
	
}
</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post">
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
  		<div style="width: 90%;">
				<label style="width: 30%; float: left;">开始时间：</label> <input
					style="width: 60%; float: left; margin-left: 1%;" type="text"
					id="startDate"  name="startDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'%y-%M-%d\'}'})"></input>
					<br>
				<label style="width: 30%; float: left;">结束时间：</label>
				<input style="width: 60%; float: left; margin-left: 1%;"
					type="text" id="endDate" name="endDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'%y-%M-%d'})"></input>
			</div>
    <tr>
     <td align="center" colspan="8">
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/readMessageController.do?exportList=key')" >导出</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body> 
  
  
  
  

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="HTmessageList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
			<div> 
				<label class="jiekouname">消息提醒状态：</label>
		   		<select id ="flag" name="type">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="已读">已读</option>
					<option value="未读">未读</option>
    		 	</select> 
			</div>
	</div>
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">	
		<span class="bluebtnhover"><a onclick="exportList('${ctx}/readMessageController.do?exports=key')" id="export"  class="formbtn bluebtn">导出</a></span>
			<%-- <span class="bluebtnhover"><a onclick="exportList('${ctx}/readMessageController.do?exportList=key')" id="export"  class="formbtn bluebtn">导出</a></span> --%>		
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
function query(){
	$('#HTmessageList').datagrid({
		idField : 'pkId',
		title : '消息中心管理',
	    url : ' ${ctx}/readMessageController.do?datagrid=key',
		fit : true,
		//loadMsg : '数据加载中...',
		pageSize : 10,
		pagination : true,
		pageList : [ 10, 20, 30 ],
		//sortName: 'id',
		sortOrder : 'asc',
		rownumbers : true,
		singleSelect : true,
		fitColumns : true,
		remoteSort:false,
		/* <!--查询参数 --> */
		queryParams:{
			'state': $('#flag').val()
		},
		columns:[[
				{field : 'ck',checkbox:true},
				{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
				{field:'message',title:'消息内容',  width:fixWidth(0.4),align:'center',sortable:false},
				{field:'state',title:'消息提醒状态',  width:fixWidth(0.15),align:'center',sortable:false},
				{field : 'opt',title : '操作',width:fixWidth(0.2),align:'center',sortable:false,
					formatter : function(value, rec,index) {
						if (!rec.pkId) {
							return '';
						}
						var href = '';
						if(rec.state == '已读'){
							href += "[<button href='#' style='border:none;color:#C0C0C0;background-color:Transparent;text-decoration:underline;outline:none;'>";
							href += "已读</button>]";
						}else{
							href += "[<a href='#' onclick=query1('readMessageController.do?query1=key','HTmessageList','"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "已读</a>]";
						}
						href += "[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>";
						href += "查看</a>]";
								var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
									var oper = operCode.split(', ');
									var i = oper.length;
									if(oper !=""){
									  for(var j=0;j<oper.length;j++){
										 if(oper[j] == "query1"){
											 href=href.replace("[<a href='#' onclick=query1('activiteController.do?query1=key','HTmessageList','"+rec.pkId+"') style='color:#3f99b1;'>已读</a>]","");
											}
										 if(oper[j] == "detail"){
											 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>查看</a>]","");
											}
									
										}
									}
						return href;
				}}
			    ]],
		onLoadSuccess : function(data) {
		    $("#HTmessageList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
			//无查询结果提示
			if (data.total == 0) {
				$('.datagrid-view2 .datagrid-body')
						.append(
								'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

				$(this).closest('div.datagrid-wrap')
						.find('div.datagrid-pager')
						.hide();
			}
			//清除所选的行
			$("#HTmessageList").datagrid("clearSelections");
		},
		//rowIndex:行索引 rowData：行数据
		onClickRow : function(rowIndex, rowData) {
			rowid = rowData.id;
			gridname = 'certificateInfoList';
		}
	});
	$("#HTmessageList").datagrid('getPager').pagination({
		beforePageText : '',
		afterPageText : '/{pages}',
		displayMsg : '显示{from}-{to}共{total}条',
		showPageList : true,
		showRefresh : true
	});
	
	$("#HTmessageList").datagrid('getPager').pagination({
		onBeforeRefresh : function(pageNumber, pageSize) {
			$(this).pagination('loading');
			$(this).pagination('loaded');
		}
	});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});

function reset(){
	$('#flag').val("");
}
//全部导出
/* function exportList(url){
		window.location.href = url;
		
	} */
	function exportList(url){
		add_win('导出客户生日',url,'HTmessageList',400,150);
		
	}

//已读
function query1(url,id,pkId){
	var windowapi = frameElement.api, W = windowapi.opener; 
	url += '&pkId='+pkId;
	var num;
    $.post(url,function(result){
        if (result.success){
        	 parent.$("#readMessageNum").load(window.parent.location.href+" #readMessageNum",function(){
        		 $.ajax({
    	             type: "post",
    	             url: "${ctx}/readMessageController.do?countMessage=key",
    	             dataType: "json",
    	             success: function(data){
                        if(data.obj=="0"){
                        	parent.$("#count").hidden();
                        }else{
                        	parent.$("#count").html(data.obj).show();
                        } 
                   }
    	         });
        	 });
        	if(result.obj =='alert'){
        		tip(result.obj);
        	}else{
                $('#'+id).datagrid('reload');    // reload the user data
                $('#'+id).treegrid('reload');	 // reload the user data
        	} 
        } else {
            window.top.$.messager.show({// show error message
                title: 'Error',
                msg: result.errorMsg
            });
        }
    },'json');
       
}



//查看页面
function detail(pkId){
	add_win2('查看','${ctx}/readMessageController.do?specific=key&pkId='+pkId,'HTmessageList',800,300);
}


function add_win2(title, addurl, gname, width, height) {
	gridname = gname;
	width = width ? width : 700;
	height = height ? height : 400;
	if (width == "100%" || height == "100%") {
		width = document.body.offsetWidth;
		height = document.body.offsetHeight - 100;
	}
	$.dialog({
		content : 'url:' + addurl,
		lock : true,
		width : width,
		height : height,
		title : title,
		opacity : 0.3,
		cache : false
	});
}


function reloadTable() {
	try {
		$('#' + MailInfoList).datagrid('reload');
		$('#' + MailInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + MailInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#HTmessageList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

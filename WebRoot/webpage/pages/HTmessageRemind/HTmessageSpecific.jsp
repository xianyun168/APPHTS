<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=repairInforAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${hTMessageInfo.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${hTMessageInfo.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	用户姓名:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="name" onkeypress="notNull(this);" name="name" readonly="readonly" value="${hTMessageInfo.name}" />
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	用户手机号:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="phone" onkeypress="notNull(this);" name="phone"  readonly="readonly" value="${hTMessageInfo.phone}" />
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	用户生日:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="birth" onkeypress="notNull(this);" name="birth"  readonly="readonly" value="${hTMessageInfo.birth}" />
    		</td>
    		
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	消息提醒状态:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="state" name="state" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="已读"<c:if test="${hTMessageInfo.state=='已读'}">selected</c:if>>已读</option>
					<option value="未读"<c:if test="${hTMessageInfo.state=='未读'}">selected</c:if>>未读</option>
    		 	</select>
    		 </td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	消息提醒内容:
    		  </label>
    		</td>
    		<td class="value" width="35%">
	    		<textarea class="inputxt" id="message" readonly="readonly" MaxLength="4000"
							onkeypress="notNull(this);" name=""message"" cols="45" rows="5" 
							style="width: 260px; height: 100px;resize:none;">${hTMessageInfo.message}</textarea>
			</td> 
    	</tr>
    	
    	<tr>
     <td align="center" colspan="8">
       <!--  <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span> -->
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
    	
   </table>
 </form>
</body>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
	  init();
	});
	//保存修改
	function checkAll() {
		var url ="${ctx}/companyInfoController.do?saveUpdate=key";
		window.top.$.messager.confirm('Confirm', '你确定要修改吗', function(r) {
			if (r) {
				$.post(url,$("#myform").serialize(), function(result) {
					if (result.success) {
						if (result.obj == 'ok') {
							tip(result.msg);
						} else {
							$('#' + id).datagrid('reload'); // reload the user data
							$('#' + id).treegrid('reload'); // reload the user data
						}
					} else {
						window.top.$.messager.show({// show error message
							title : 'Error',
							msg : result.errorMsg
						});
					}
				}, 'json');
			}
		});
	}
	
</script>
<style type="text/css">
	.panel-title {
    font-size: 14px;
    height: 35px;
    line-height: 35px;
    padding-left: 20px;
    color: #1c6eab;
    background: url('images/blank.gif') no-repeat;
    }
    #saveBtn{
    	border-radius: 4px;
    }
</style>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<div class="panel-herder" style="width: 1170px;height: 40px">
 		<div class="panel-title">公司基本信息</div>
 		<div class="panel-tool"></div>
 	</div >
 	<div style="width: 1170px;" >
 		<form id="myform" name="myform" method="post" >
  			<c:if test="${companyInfo.pkId!= null}">
        		<input id="pkId" name="pkId" type="hidden" value="${companyInfo.pkId}">
    		</c:if>
  			<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公司名称:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<input class="companyName" id="companyName" onkeypress="notNull(this);" name="companyName" value="${companyInfo.companyName}" maxlength="30"
    		  			style="width:260px;height:35px;" datatype="s4-30" errormsg="请输入4-30位内的字符"  nullmsg="请输入公司名称！"/><span style="color:red">*</span>
    	  			</td>
    			</tr>
    			<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公司电话:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<input class="companyPhone" id="companyPhone" onkeypress="notNull(this);" name="companyPhone" value="${companyInfo.companyPhone}" maxlength="30"
    		  			style="width:260px;height:35px;" datatype="*4-30" errormsg="请输入正确电话！"  nullmsg="请输入公司电话！"/><span style="color:red">*</span>
    	  			</td>
    			</tr>
    			<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>来电时间:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<input class="callTime" id="callTime" onkeypress="notNull(this);" name="callTime" value="${companyInfo.callTime}" maxlength="30"
    		  			style="width:260px;height:35px;" datatype="*4-30" errormsg="请输入4-30个字符！"  nullmsg="请输入来电时间！"/><span style="color:red">*</span>
    	  			</td>
    			</tr>
    			<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公司邮箱:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<input type="text" class="companyEmail" id="companyEmail" onkeypress="notNull(this);"  name="companyEmail" value="${companyInfo.companyEmail}" 
    		  			style="width:260px;height:35px;" datatype="/^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$/" errormsg="请输入正确邮箱格式！"  nullmsg="请输入邮箱！" /><span  style="color:red">*</span>
    	  			</td>
    			</tr>
    			<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公司地址:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<input type="text" class="address" id="address" onkeypress="notNull(this);"  name="address" value="${companyInfo.address}" 
    		  			style="width:260px;height:35px;" datatype="*4-30" errormsg="请输入4-30个字符！"  nullmsg="请输入公司地址！" /><span  style="color:red">*</span>
    	  			</td>
    			</tr>
     			<tr>
     				<td align="center" colspan="8">
        				<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
   	 				</td>
   	 				
    			</tr>
   			</table>
 		</form>
 	</div>
</body> 
  
  
  
  

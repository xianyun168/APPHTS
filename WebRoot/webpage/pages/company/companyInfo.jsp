<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <link href="${ctx}/ueditor/themes/default/css/ueditor.css" type="text/css" rel="stylesheet">
  <!-- 配置文件 -->
    <script type="text/javascript" src="ueditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="ueditor/ueditor.all.js"></script>
  <script type="text/javascript">
	$(function(){
	  init();
	});
	//保存修改
	/* function checkAll() {
		
		var url ="${ctx}/companyInfoController.do?saveUpdate=key";
		window.top.$.messager.confirm('Confirm', '你确定要修改吗', function(r) {
			if (r) {
				$.post(url,$("#myform").serialize(), function(result) {
					if (result.success) {
						if (result.obj == 'ok') {
							tip(result.msg);
						} else {
							$('#' + id).datagrid('reload'); // reload the user data
							$('#' + id).treegrid('reload'); // reload the user data
						}
					} else {
						window.top.$.messager.show({// show error message
							title : 'Error',
							msg : result.errorMsg
						});
					}
				}, 'json');
			}
		});
	} */
function checkAll() {
		var url ="${ctx}/companyInfoController.do?saveUpdate=key";
		var form = $("#myform").Validform();
		$('#myform').form(
				'submit',
				{
					url : url,
					onSubmit : function() {
						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
							} else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
								return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'),
									data.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
	}
</script>
 <!-- 实例化编辑器 -->
    <script type="text/javascript">
      	var ue = UE.getEditor('companyInfo', {
      	    toolbars: [
      	        ['fullscreen', 'source', 'undo', 'redo', 'bold']
      	    ],
      	    autoHeightEnabled: false,
      	    autoFloatEnabled: true,
      		//是否启用元素路径，默认是显示
            elementPathEnabled : false,
            wordCount:true,          //是否开启字数统计
            maximumWords:2000       //允许的最大字符数
      	});
    </script>
<style type="text/css">
	.panel-title {
    font-size: 14px;
    height: 35px;
    line-height: 35px;
    padding-left: 20px;
    color: #1c6eab;
    background: url('images/blank.gif') no-repeat;
    }
    #saveBtn{
    	border-radius: 4px;
    }
</style>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<div class="panel-herder" style="width: 1170px;height: 40px">
 		<div class="panel-title">公司简介</div>
 		<div class="panel-tool"></div>
 	</div >
 	<div style="width: 1170px;" >
 		<form id="myform" name="myform" enctype="multipart/form-data" method="post" >
  			<c:if test="${companyInfo.pkId!= null}">
        		<input id="pkId" name="pkId" type="hidden" value="${companyInfo.pkId}">
    		</c:if>
  			<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
		  			<td align="left" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公司简介:</span></label>
    	  			</td>
    	  		</tr>
    	  		<tr>
    	  			<td class="value" width="75%" >
    	      			<textarea rows="8" cols="16" style="width:1160px;height: 300px;" onkeypress="notNull(this);" name="companyInfo" class="companyInfo" id="companyInfo" 
    	       			datatype="*4-4000" errormsg="请输入4-4000位内的字符"  nullmsg="请输入4-4000位内容！">${companyInfo.companyInfo}</textarea>
    	      			<span style="color:red">*</span>
    	  			</td>
    			</tr>
     			<tr>
     				<td style="margin:auto;text-align: center;">
        				<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
   	 				</td>
   	 				
    			</tr>
   			</table>
 		</form>
 	</div>
</body> 
  
  
  
  

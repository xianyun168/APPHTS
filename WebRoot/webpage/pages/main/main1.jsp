<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>秀实投资后台管理系统</title>
	<%@ include file="/common/public.jsp"%>
	<%-- <%@ include file="/common/head.jsp"%> --%>
	<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	<!-- <link rel="shortcut icon" href="images/favicon.ico"> -->
<style type="text/css">
* {
	margin: 0px;
	padding: 0px;
}

#menu {
	padding: 0;
}

.menuUI li {
	padding-left:30px;
	list-style: none;
	height:55px;
	line-height:55px;
	color:#fff;
	font-size:14px;
	background: #373d4b url(${ctx}/resources/images/main/img_26.jpg) no-repeat 40px center;
	cursor:pointer;
}
</style>
	</head>
	<body class="easyui-layout">
	   <div style="width: 100%;background-color: #fff;height: 300px;position: absolute;overflow:hidden;"></div>
			<div data-options="region:'north',split:false"
				style="height: 90px;background:#0255a3;border:none;overflow:hidden;">
			<div style="margin-top:12px;width:400px;float:left;">
				<span class="logotxt">秀实投资后台管理系统</span>
			</div>
			<div class="headerright">
				<span class="systxt">您好，${userName}</span>
				<span  onclick="modifyPassword()" class="modifypwd" id="updatePWD">修改密码</span>
				<span class="logout" onClick="logout()">退出</span>
			</div> 
			</div>
			<div id="menuParent" data-options="region:'west',split:false"
				style="width: 250px;">
			<div id="menu"  style="width: 100%; height: 100%;">

			</div>
			<div class="menunav"></div>
			</div>
			<div data-options="region:'center'" id="center" style="background:#f4f4f4 url(${ctx}/resources/images/main/welcome.jpg) no-repeat 170px 80px;">
	
			</div>
			<div id="updatePWDMsg">
				<h1>请稍等。。。。</h1>
			</div>
</body>
	
		<%-- <div data-options="region:'north',border:false" style="height: 128px; background: #1d74bb">
			<table id="head" width="100%" height="100%" border="0" cellpadding="0" cellspacing="2">
				<tr>
					<td align="left" style="padding-left: 20px">
						<!--<h1>ESBMM</h1>-->
                        <img src="${ctx}/webpage/pages/main/logo.jpg">
                    </td>
					<td align="right" valign="bottom">
						<table>
							<tr>
								<td align="right" colspan="3">
									<span><span id="clock"></span><img src="${ctx}/webpage/pages/main/date.gif"></span>
								</td>
							</tr>
							<tr>
								<td align="right">
									<span>当前用户:${userCode}</span>
								</td>
								<td>
									<span>&nbsp;&nbsp;</span>
								</td>
								<td align="right">
									<span>姓名:${userName}</span>
								</td>
							</tr>
							<tr>
<!-- 								<td align="right"> -->
<!-- 									<div onclick="modifyPassword('修改密码',' ${ctx}/loginController.do?modifyPassword=key',700,140)">[修改密码]</div> -->
<!-- 								</td> -->
								<td>
									<span>&nbsp;&nbsp;</span>
								</td>
								<td>
									<span>&nbsp;&nbsp;</span>
								</td>
								<td align="right">
									<div onclick="exit('${ctx}/loginController.do?logout=key','确定退出该系统吗 ?',1);"><img src="${ctx}/webpage/pages/main/exit.gif">[安全退出]</div>
									
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
                    <td align="left" colspan="2">
						${nav}
                    </td>
				</tr>
			</table>
		</div>

		<div data-options="region:'west',split:true,title:'导航菜单'"
			style="width: 210px;" id="menu11">
			${menuMap}
		</div>
		<div data-options="region:'center'" style="overflow: hidden"
			id="maintabs">
			<iframe frameborder="0" height="100%" width="100%" scrolling="auto"
				id ="page" name="page" src="${ctx}/resources/images/main/welcome.jpg"></iframe>
		</div>
        
		<div region="south" style="height:31px; line-height:31px; overflow:hidden;text-align: center" id="footer" >  ©2014  中国保信ESB监控管理平台系统   版权所有 </div> --%>
		<script type="text/javascript">
		
		$(function(){
			$("#menu > .panel").eq(1).children("div.panel-header").children("div.panel-title").addClass("panel-title1");
			$("#menu > .panel").eq(2).children("div.panel-header").children("div.panel-title").addClass("panel-title2");
			$("#menu > .panel").eq(3).children("div.panel-header").children("div.panel-title").addClass("panel-title3");
			$("#menu > .panel").eq(4).children("div.panel-header").children("div.panel-title").addClass("panel-title4");
		});
		$(function(){
			$('#menu .panel-title').click(function(){
				$('#menu .panel-title').removeClass('borderLeft');
				$(this).addClass('borderLeft');
			})
		});
		//点击菜单时改变当前位置
		function changeTitle(title){
			$('body').layout('panel', 'center').panel({ title: '您所在的位置：' +  title});
		}
		//更新时钟
		function tickTock(){
			var now = new Date();
			var gd=now.getDay();
			var week = "";
            switch(gd){
                case 1:
                    week="星期一";
                    break;
                case 2:
                    week="星期二";
                    break;
                case 3:
                    week="星期三";
                    break;
                case 4:
                    week="星期四";
                    break;
                case 5:
                    week="星期五";
                    break;
                case 6:
                    week="星期六";
                    break;
                case 0:
                    week="星期天";
                    break;
                default:
                    week="什么情况!";
             }       
			var timestamp = now.getFullYear() + '年' + (now.getMonth()+1) + '月' + now.getDate() + '日' + ' ' + week;
			$("#clock").html(timestamp);
		}
		
		//退出登录
		function logout(){
			$.ajax({
				url:"${ctx}/loginController.do?logout=key",
				async:false
			});
			window.location.href="${ctx}/loginController.do?main=key";
		}
		/* //退出
		function exit(url, content) {
			$.dialog.confirm(content, function(){
				window.location = url;
				//window.opener=null;
				//window.open('','_self');
				//window.close();
			}, function(){
			});
		} */
	//修改密码
		function modifyPassword(title, addurl, width, height) {
			width = width ? width : 700;
			height = height ? height : 400;
			if (width == "100%" || height == "100%") {
				width = document.body.offsetWidth;
				height = document.body.offsetHeight - 100;
			}
			$.dialog({
				content : 'url:' + addurl,
				lock : true,
				width : width,
				height : height,
				title : title,
				opacity : 0.3,
				cache : false
			});
		}
	</script>
	</body>
</html>
<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="renderer" content="ie-stand">
	<title>秀实投资管理系统</title>
	<%@ include file="/common/public.jsp"%>
	<%@ include file="/common/head.jsp"%>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">

<style type="text/css">
a {color: Black;text-decoration: none;}
a:hover {color: black;text-decoration: none;}
.layout-panel-west{
	background-image:url(${ctx}/webpage/pages/login/images/navBg.png);
}
.accordion .accordion-header,
.accordion .panel-body,
.accordion{
	background:none !important;
}
.panel-header{
	padding:0px !important;
}
#nav .panel-title{
	font-size:16px !important;
	border-bottom:1px solid #294365;
}
#nav .accordion-collapse{
	background:none !important;
}
#menu .panel-title{
	background:url(${ctx}/webpage/pages/login/images/navSystem.png) no-repeat 18px 9px  !important;
}
#menu .panel-title2{
	background: url(${ctx}/webpage/pages/login/images/navConfigure.png) no-repeat 18px 9px  !important;
}
#menu .panel-title3{
	background: url(${ctx}/webpage/pages/login/images/navEnquir.png) no-repeat 18px 9px  !important;
}
#menu .panel-title4{
	background: url(${ctx}/webpage/pages/login/images/navAudit.png) no-repeat 18px 9px  !important;
}
.accordion .tree-title{
	font-size:14px !important;
}
.accordion .easyui-tree li div,
.tree-title{
	height:30px !important;
	line-height:30px !important;
	padding:0px !important;
	margin-top:0px !important;
}
.easyui-tree li a{
	width:23px !important;
	height:35px !important;
	line-height:35px !important;
	background: url(${ctx}/webpage/pages/login/images/treeIcon.png) no-repeat 5px -7px !important;
	text-indent:20px !important;
}
.borderLeft{
	border:none !important;
}
.accordion .accordion-header-selected{
	background:rgba(255,255,255,.15) !important;
}
.panel-header{
	height: 36px;
}
</style>
	</head>
	<body class="easyui-layout">
		<div data-options="region:'north',border:false" style="height:69px;background:#fff;border-bottom:1px solid #d6d6d6;overflow:hidden;">
			<div class="logo">
				<img src="${ctx}/webpage/pages/login/images/logo.png">
			</div>
			<div class="headerright">
				<div class="logout" onClick="logout()">
					<div class="headIcon"><img src="${ctx}/webpage/pages/login/images/headLogout.png"></div>
					<span>退出</span>
				</div>
				<div  onclick="modifyPassword('修改密码',' ${ctx}/loginController.do?modifyPassword=key',700,230)" class="modifypwd" id="updatePWD">
					<div class="headIcon"><img src="${ctx}/webpage/pages/login/images/headPassword.png"></div>
					<span>修改密码</span>
				</div>
				<div class="systxt">
					<div class="headIcon"><img src="${ctx}/webpage/pages/login/images/headWelcome.png" title="${userName}"></div>
					<span title="${userName}">${userName}</span>
				</div>
				<%-- <div onclick="exportList('${ctx}/equitiesUseDetailsController.do?exportList=key')" class="export" id="export">
					<div class="headIcon" style="position:relative;">
						<img src="${ctx}/webpage/pages/login/images/table.png" width="23px" height="21px">
					</div>
					<span>权益导出</span>
				</div>  --%>
				<div onclick="readMessage('消息中心列表',' ${ctx}/readMessageController.do?readMessage=key',1000,400)" class="readMessage" id="readMessageNum">
					<div class="headIcon" style="position:relative;">
						<img src="${ctx}/webpage/pages/login/images/messages.png" width="19px" height="16px">
						<span id="count" type="button"  style="width: 2px;height:5px;color:red;position:absolute; right:23px; top:-8px;"></span>
					</div>
					<span>消息中心</span>
				</div>
				<!-- <div><input id="count" type="button" onclick="countMessage();" /></div> -->
			</div> 
		</div>

		<div data-options="region:'west'" style="width: 17%;" id="menu11">
			<div id="menu"  style="width: 100%; height: 100%;">
				${menuMap}
			</div>
		</div>
		<div data-options="region:'center'" style="overflow: hidden;width:83%" id="maintabs">
			<iframe frameborder="0" height="100%" width="100%" scrolling="auto"
				id ="page" name="page" src="${ctx}/webpage/pages/main/welcome.jsp"></iframe>
		</div>
        
		
		<script type="text/javascript">
		
		$(function(){
			countMessage();
			setTimeout(function(){
				window.top.$.messager.progress('close');
				window.top.$.messager.show({
					title:'欢迎',
					msg:'欢迎您使用秀实投资后台管理系统！'
				});
			}, 1500);
			//时钟
			setInterval("tickTock()", 1000);
			var menus = $("#nav > .panel").children("div.panel-header").children("div.panel-title");
			if(menus.length==0){
				alert("该用户没有配置菜单权限!");
			}
		for(var menuOne=0;menuOne<menus.length;menuOne++){
			if($(menus[menuOne]).text()=='系统管理'){
				$(menus[menuOne]).addClass("panel-title")
			}else if($(menus[menuOne]).text()=='配置管理'){
				$(menus[menuOne]).addClass("panel-title2")
			}else if($(menus[menuOne]).text()=='查询管理'){
				$(menus[menuOne]).addClass("panel-title3")
			}else if($(menus[menuOne]).text()=='审核管理'){
				$(menus[menuOne]).addClass("panel-title4")
			}else{
				$(menus[menuOne]).addClass("panel-title4")
			}
		}
		
	 });
		
		$(function(){
			$('#menu .panel-title').click(function(){
				$('#menu .panel-title').removeClass('borderLeft');
				$(this).addClass('borderLeft');
			})
		})
		//点击菜单时改变当前位置
		function changeTitle(title){
			$('body').layout('panel', 'center').panel({ title: '您所在的位置：' +  title});
		}
		//更新时钟
		function tickTock(){
			var now = new Date();
			var gd=now.getDay();
			var week = "";
            switch(gd){
                case 1:
                    week="星期一";
                    break;
                case 2:
                    week="星期二";
                    break;
                case 3:
                    week="星期三";
                    break;
                case 4:
                    week="星期四";
                    break;
                case 5:
                    week="星期五";
                    break;
                case 6:
                    week="星期六";
                    break;
                case 0:
                    week="星期天";
                    break;
                default:
                    week="什么情况!";
             }       
			var timestamp = now.getFullYear() + '年' + (now.getMonth()+1) + '月' + now.getDate() + '日' + ' ' + week;
			$("#clock").html(timestamp);
		}
		//退出
		function exit(url, content) {
			$.dialog.confirm(content, function(){
				window.location = url;
				//window.opener=null;
				//window.open('','_self');
				//window.close();
			}, function(){
			});
		}
		function logout(){
			$.ajax({
				url:"${ctx}/loginController.do?logout=key",
				async:false
			});
			window.location.href="${ctx}/loginController.do?main=key";
		}
	//消息弹框
		function readMessage(title, addurl, width, height) {
			width = width ? width : 700;
			height = height ? height : 400;
			if (width == "100%" || height == "100%") {
				width = document.body.offsetWidth;
				height = document.body.offsetHeight - 100;
			}
			$.dialog({
				content : 'url:' + addurl,
				lock : true,
				width : width,
				height : height,
				title : title,
				opacity : 0.3,
				cache : false
			});
		}
	//显示消息数量
		function countMessage(){
			$.ajax({
	             type: "post",
	             url: "${ctx}/readMessageController.do?countMessage=key",
	             dataType: "json",
	             success: function(data){
                    if(data.obj=="0"){
                    	$("#count").hide();
                    }else{
                    	$("#count").html(data.obj).show();
                    } 
               }
	         });
		}
	
		//修改密码
		function modifyPassword(title, addurl, width, height) {
			width = width ? width : 1000;
			height = height ? height : 400;
			if (width == "100%" || height == "100%") {
				width = document.body.offsetWidth;
				height = document.body.offsetHeight - 100;
			}
			$.dialog({
				content : 'url:' + addurl,
				lock : true,
				width : width,
				height : height,
				title : title,
				opacity : 0.3,
				cache : false
			});
		}
function goThisJsp(obj) {
			
			var browser = getBrowser();

			if (browser == "firefox") {
				//只有a标签的href值不为javascript:void(0)才进行记录
				if ("javascript:void(0)" != obj.href) {
					var arrayA = new Array();
					var As = $('a');
					for (var i = 0; i < As.length; i++) {
						arrayA[i] = As[i].href;
					}
				}
				//如果跳转用的href为空则赋值
				if ("javascript:void(0)" == obj.href) {
					for (var i = 0; i < As.length; i++) {
						As[i].href = arrayA[i];
					}
				}
				//点击跳转时延时定时0.001秒后设置href为javascript:void(0)
				window.setTimeout(function() {
					$('a').attr("href", "javascript:void(0)")
				}, 1);
				//点击跳转时延时定时0.7秒后设置href为原地址
				window.setTimeout(function() {
					for (var i = 0; i < As.length; i++) {
						As[i].href = arrayA[i];
					}
				}, 700);
			}

		}

		//判断是否为火狐浏览器
		function getBrowser() {
			var userAgent = navigator.userAgent, rFirefox = /(firefox)\/([\w.]+)/;
			var browser;
			var version;
			var ua = userAgent.toLowerCase();
			function uaMatch(ua) {
				var match = rFirefox.exec(ua);
				if (match != null) {
					return match[1];					
				}
				
			}
			var browserMatch = uaMatch(userAgent.toLowerCase());
			if (browserMatch == "firefox") {
				return "firefox";
			}
		}

		function exportList(url){
			location.href=url;
		}
	//左侧手风琴菜单背景图
	//$(".panel,.layout-panel,.layout-panel-west,.layout-panel,. layout-panel-center").css("top","70px");
	//$(".layout-panel-west").css("width","100%");
	</script>
	</body>
</html>
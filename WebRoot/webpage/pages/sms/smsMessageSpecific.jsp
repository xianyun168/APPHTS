<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=SmsMessageSendSpecific toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${smsSend.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${smsSend.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	手机号:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="phoneNumber" onkeypress="notNull(this);" name="phoneNumber" readonly="readonly" value="${smsSend.phoneNumber}" />
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	操作人:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="smsOperateName" onkeypress="notNull(this);" name="smsOperateName"  readonly="readonly" value="${smsSend.smsOperateName}" />
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>发送时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="sendDate" readOnly="readOnly" value="${smsSend.sendDate }"
					name="sendDate"  class="inputxt" style="width: 260px;height:36px;"
				    onkeypress="notNull(this);"></input>
    		</td>
    		
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	发送类型:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="smsSendType" name="smsSendType" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="系统"<c:if test="${smsSend.smsSendType=='系统'}">selected</c:if>>系统</option>
					<option value="人工"<c:if test="${smsSend.smsSendType=='人工'}">selected</c:if>>人工</option>
    		 	</select>
    		 </td>
    		
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	发送状态:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="smsSendState" name="smsSendState" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="已发送"<c:if test="${smsSend.smsSendState=='已发送'}">selected</c:if>>已发送</option>
					<option value="未发送"<c:if test="${smsSend.smsSendState=='未发送'}">selected</c:if>>未发送</option>
    		 	</select>
    		 </td>
    		
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	消息内容:
    		  </label>
    		</td>
    		<td class="value" width="35%">
	    		<textarea class="inputxt" id="smsMessageContent" readonly="readonly" MaxLength="4000"
							onkeypress="notNull(this);" name="smsMessageContent" cols="45" rows="5" 
							style="width: 260px; height: 100px;resize:none;">${smsSend.smsMessageContent}
				</textarea>
			</td> 
    	</tr>
    	
    <tr>
     <td align="center" colspan="8">
       <!--  <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span> -->
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
    	
   </table>
 </form>
</body>

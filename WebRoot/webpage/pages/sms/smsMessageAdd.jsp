<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
function saveData(url) {
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#saveBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=SmsMessageSendAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${smsSend.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${smsSend.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	手机号:
    		 </label>
    		</td>
    		<td class="value" width="35%">
    			<textarea class="inputxt" id="phoneNumber" MaxLength="500"
							onkeypress="notNull(this);" errormsg="请输入1开头的合法手机号！"  nullmsg="多个手机号以逗号隔开！"
							datatype="/^(1\d{10}[\,])*(1\d{10})$/" name="phoneNumber" cols="45" rows="5" 
							style="width: 260px; height: 100px;resize:none;">${smsSend.phoneNumber}</textarea>
				<span style="color: red;">*</span>
    		 </td>
 		</tr>
    	
    	<tr>
 			<td align="right" width="15%" nowrap>
	    		  <label class="Validform_label">
	    		 	消息内容:
	    		  </label>
    		</td>
    		<td class="value" width="35%">
	    		<textarea class="inputxt" id="smsMessageContent" MaxLength="500"
							onkeypress="notNull(this);" errormsg="不能超过500个字符" nullmsg="请输入消息内容！"
							datatype="*1-500" name="smsMessageContent" cols="45" rows="5" 
							style="width: 260px; height: 100px;resize:none;">${smsSend.smsMessageContent}</textarea>
				<span style="color: red;">*</span>
			</td> 
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>定时发送时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%">
    		  <input type="text" id="timingSendDate" value="${timingSendDate }"
					name="timingSendDate"  class="Wdate" style="width: 260px;height:36px;"
					datatype="*1-50" nullmsg="请输入发送时间！" errormsg="请输入正确的发送时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d'})"></input><span style="color: red;">*</span>
    		</td>
    	</tr>
     <tr>
     <td align="center" colspan="8">
     	<span class="bluebtnhover"><a  id="sendBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="saveData(' ${ctx}/smsMessageController.do?sendMessage=key')">发送</a></span>
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData(' ${ctx}/smsMessageController.do?saveUpdate=key')" >保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
	  init();
	});
	function checkAll(){
		saveData('${ctx}/opinionController.do?saveUpdate=key');
	}

</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${opinion.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${opinion.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
  		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>提交人:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${opinion.remark1}" 
    		  style="width:260px;height:35px;" readonly="readonly" />
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>联系电话:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${opinion.phoneNumber}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	</tr>
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>状态:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${opinion.flag}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>提交时间:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${submitTime}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>意见:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	      <textarea rows="4" cols="8" style="width:260px;" onkeypress="notNull(this);" name="content" class="content" id="content" 
    	       readonly="readonly">${opinion.opinion}</textarea>
    	  </td>
    	</tr>
     	<tr>
     		<td align="center" colspan="8">
     			<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
</body> 
  
  
  
  

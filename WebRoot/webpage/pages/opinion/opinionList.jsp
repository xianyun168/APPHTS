<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>KFCRM</title>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet" />
<style type="text/css">
	 .jiekouform label {
    
    float: left;
    text-align: right;
    } 
    .jiekouform > div {
    width: 25%;
    float: left;
    height: 30px;
    line-height: 30px;
    margin-top: 10px;
}

    
</style>
</head>
<body>
	<table width="100%" id="AtomicAuditList" toolbar="#demoListtb"></table>
	<div id="demoListtb" style="padding:3px; height: auto">
		<!-- 用于权限判断 start -->
		 <input id="pkId" name="pkId" type="hidden" value=${knowledgeBase.pkId}/>
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
		
		<div class="jiekouform" >
			<div>
				<label class="jiekouname" >关键字：</label>
				<input type="text" id="opinion"  name="opinion"  />
			</div>
			<div>
				<label class="jiekouname" >状态：</label>
				<select name="flag" id="flag">
					<option value="" >请选择</option>
					<option value="00">未读</option>
					<option value="01">已读</option>
				</select>
			</div>
			<div style="width: 50%;">
				<label style="width: 20%; float: left;">时间区间：</label> <input
					style="width: 30.5%; float: left; margin-left: 1%;" type="text"
					id="startDate"  name="startDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'%y-%M-%d\'}'})"></input>

				<label
					style="width: 2%; float: left; margin-left: 1%; text-align: center;">-</label>
				<input style="width: 30.5%; float: left; margin-left: 1%;"
					type="text" id="endDate" name="endDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'%y-%M-%d'})"></input>
			</div>
		</div>	
		<div  class="leftbtntool" style="padding-top:60px; width:270px;margin:auto;">
			
			<span class="orangebtnhover" ><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
		
		<div style="background-color:white;"></div>
	</div>
	 <script type="text/javascript">
		function query() {
			$('#AtomicAuditList').datagrid({
				idField : 'pkId',
				title : '意见建议',
				url : ' ${ctx}/opinionController.do?getOpinionList=key',
				fit : true,
				//loadMsg : '数据加载中...',
				pageSize : 10,
				pagination : true,
				pageList : [ 10, 20, 30 ],
				//sortName: 'id',
				sortOrder : 'asc',
				striped : true,
				rownumbers : true,
				singleSelect : true,
				fitColumns : true,
				remoteSort : false,
				queryParams : {
				'startDate': $('#startDate').val(),
				'endDate': $('#endDate').val(),
				'opinion': $('#opinion').val(),
				'flag': $('#flag').val()
				},
				columns : [ [ {
					field : 'ck',
					checkbox : true
				}, {
					field : 'pkId',
					title : '主键',
					width : fixWidth(0.11),
					align : 'center',
					sortable : true,
					hidden : true
				}, {
						field : 'flag',
						title : '状态',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					}, {
						field : 'submitTime',
						title : '提交时间',
						formatter : function(val, rec) {
							if (val != null && val != '') {
								return formattimed(val);
							}
						},
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					}, 
					 {
						field : 'remark1',
						title : '提交人',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					},{
						field : 'phoneNumber',
						title : '联系电话',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					},
					{field : 'opt',title : '操作',width:fixWidth(0.11),align:'center',sortable:true,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							/* href += "[<a href='#' onclick=dele('helpDocumentController.do?delete=key','AtomicAuditList','"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "删除</a>]"; */
							href += "[<a href='#' onclick=details('意见建议详情','opinionController.do?details=key','AtomicAuditList','"+rec.pkId+"','"+rec.flag+"',710,330) style='color:#3f99b1;'>";
							href += "查看</a>]";
							var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
							var oper = operCode.split(', ');
							var i = oper.length;
							if(oper !=""){
							  for(var j=0;j<oper.length;j++){
								 if(oper[j] == "details"){
									 href=href.replace("[<a href='#' onclick=details('意见建议详情','opinionController.do?details=key','AtomicAuditList','"+rec.pkId+"','"+rec.flag+"',710,330) style='color:#3f99b1;'>查看</a>]","");
									}
								}
							}
							return href;
						}} ] ],
					onLoadSuccess : function(data) {
						$("#AtomicAuditList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
						//清楚所选的行
						$("#AtomicAuditList").datagrid("clearSelections");
					},
					onLoadError : function(){
						history.go(0);
					},
					//rowIndex:行索引 rowData：行数据
					onClickRow : function(rowIndex, rowData) {
						rowid = rowData.id;
						gridname = 'AtomicAuditList';
					}
				});
				$("#AtomicAuditList").datagrid('getPager').pagination({
					beforePageText : '',
					afterPageText : '/{pages}',
					displayMsg : '显示{from}-{to}共{total}条',
					showPageList : true,
					showRefresh : true
				});

				$("#AtomicAuditList").datagrid('getPager').pagination({
					onBeforeRefresh : function(pageNumber, pageSize) {
						$(this).pagination('loading');
						$(this).pagination('loaded');
					}
				});
			};
			$(function() {
				//查询
				query();
				noauto();

			});
			function reset() {
			$('#opinion').val('');
			$('#startDate').val('');
			$('#endDate').val('');
			$('#flag').val('');
			}
			function reloadTable() {
				try {
					$('#' + AtomicAuditList).datagrid('reload');
					$('#' + AtomicAuditList).treegrid('reload');
				} catch (ex) {
				}
			}

			function getdemoListSelected(field) {
				return getSelected(field);
			}

			//选中单行
			function getSelected(field) {
				var row = $('#' + AtomicAuditList).datagrid('getSelected');
				if (row != null) {
					value = row[field];
				} else {
					value = '';
				}
				return value;
			}

			function getUserListSelections(field) {
				var ids = [];
				var rows = $('#AtomicAuditList').datagrid('getSelections');
				for ( var i = 0; i < rows.length; i++) {
					ids.push(rows[i][field]);
				}
				ids.join(',');
				return ids
			};

			//页面初始化时，设置按钮的权限
			function noauto() {
				//取得对应的权限的值(后台返回的是没有的权限的值)
				var operCode = $("#noauto_operationCodes").val().replace("[", "")
						.replace("]", "");
				var oper = operCode.split(', ');
				var i = oper.length;
				$(oper).each(function(i) {
					$('#' + oper[i]).hide();
				})
			}
			//详情列
			function details( title,url, id,rec,flag,width,height) {
				if(flag == "未读"){
					flag = "00";
				}else{
					flag = "01";
				}
				url += '&pkId=' + rec+"&flag="+encodeURI(encodeURI(flag));
				width = width ? width : 720;
				height = height ? height : 400;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + url,
					lock : true,
					width : width,
					height : height,
					title : title,
					opacity : 0.3,
					cache : false,
					modal: true
				});
			}
			
	</script> 
</body>
</html>
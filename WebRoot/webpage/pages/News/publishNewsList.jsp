<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>新闻发布</title>
<meta http-equiv="Content-Type" content="multipart/form-data; charset=utf-8" />
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
</head>
<body>
	<table width="100%" id="publishNewsList" toolbar="#demoListtb"></table>
	<div id="demoListtb">
		<!-- 用于权限判断 start -->
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
		<!-- 根据条件查询 -->
		<div class="jiekouform">
			<div>
				<label>新闻标题：</label> <input type="text" id="newsTitle"
					name="newsTitle" />
			</div>
				<div style="width: 66.66%;">
				<label style="width: 20%; float: left;">发布时间：</label> <input
					style="width: 27.5%; float: left; margin-left: 1%;" type="text"
					id="startDate"  name="startDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"></input>

				<label
					style="width: 2%; float: left; margin-left: 1%; text-align: center;">-</label>
				<input style="width: 27.5%; float: left; margin-left: 1%;"
					type="text" id="endDate" name="endDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startDate\')}'})"></input>
			</div>
			<div>
			   <label class="jiekouname">是否置顶：</label> <select id="newsTop"
					name="newsTop" >
					<option value="">请选择</option>
					<option value="是">是</option>
					<option value="否">否</option>
				</select>
				</div>

			<div>
				<label class="jiekouname">审核状态：</label> <select id="auditState"
					name="auditState" style="width: 56.5%;float: left;margin-left: 2.5%;">
					<option value="">请选择</option>
					<option value="未提交">未提交</option>
					<option value="待审核">待审核</option>
					<option value="通过">通过</option>
					<option value="未通过">未通过</option>
				</select>
			</div>
			<div>
				<label class="jiekouname">发布状态：</label> <select id="publishState"
					name="publishState" >
					<option value="">请选择</option>
					<option value="未发布">未发布</option>
					<option value="已发布">已发布</option>
				</select>
			</div>
		</div>
		<div style="clear: both; padding-top: 20px;">
			<div class="leftbtntool" style="width: 400px; margin: auto;">
				<span class="bluebtnhover"><a
					onclick="add_win('新闻新增','${ctx}/publishNewsController.do?add=key','publishNewsList',1200,660)"
					id="add" class="formbtn bluebtn">新增</a></span> <span
					class="orangebtnhover"><a href="javascript:void(0)"
					class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span> <span
					class="bluebtnhover"><a onclick="query()"
					class="formbtn bluebtn">查询</a></span>
			</div>
		</div>

	</div>
	<script type="text/javascript">
		function query() {
			$('#publishNewsList')
					.datagrid(
							{
								idField : 'pkId',
								title : '新闻发布管理',
								url : ' ${ctx}/publishNewsController.do?datagrid=key',
								fit : true,
								//loadMsg : '数据加载中...',
								pageSize : 10,
								pagination : true,
								pageList : [ 10, 20, 30 ],
								//sortName: 'id',
								sortOrder : 'asc',
								rownumbers : true,
								singleSelect : true,
								fitColumns : true,
								remoteSort : false,
								/* <!--查询参数 --> */
								queryParams : {
									'newsTitle' : $('#newsTitle').val(),
									'auditState' : $('#auditState').val(),
									'publishState' : $('#publishState').val(),
									'newsTop' : $('#newsTop').val(),
									'startDate': $('#startDate').val(),
									'endDate': $('#endDate').val()
									},
								columns : [ [
										{
											field : 'ck',
											checkbox : true
										},
										{
											field : 'pkId',
											title : '主键',
											width : fixWidth(0.11),
											align : 'center',
											sortable : true,
											hidden : true
										},
										{
											field : 'newsTitle',
											title : '新闻标题',
											width : fixWidth(0.15),
											align : 'center',
											sortable : false
										},
										{
											field : 'newsAuthor',
											title : '作者',
											width : fixWidth(0.15),
											align : 'center',
											sortable : false
										},
										{
											field : 'newsPublishConfig',
											title : '发布时间',
											width : fixWidth(0.18),
											formatter : function(val, rec) {
												if (val != null && val != '') {
													return formattimes(val);
												}
											},
											align : 'center',
											sortable : true
										},
										{
											field : 'newsType',
											title : '新闻类型',
											width : fixWidth(0.12),
											align : 'center',
											sortable : false
										},
										{
											field : 'newsTop',
											title : '是否置顶',
											width : fixWidth(0.08),
											align : 'center',
											sortable : false
										},
										{
											field : 'publishState',
											title : '发布状态',
											width : fixWidth(0.11),
											align : 'center',
											sortable : false
										},
										{
											field : 'auditState',
											title : '审核状态',
											width : fixWidth(0.11),
											align : 'center',
											sortable : false
										},
										{
											field : 'newsAuditor',
											title : '审核人',
											width : fixWidth(0.11),
											align : 'center',
											sortable : false
										},
										{
											field : 'opt',
											title : '操作',
											width : fixWidth(0.28),
											align : 'center',
											sortable : false,
											formatter : function(value, rec,
													index) {
												if (!rec.pkId) {
													return '';
												}
												var href = '';
												href += "[<a href='#' onclick=update('publishNewsController.do?modify=key','publishNewsList','"
														+ rec.pkId
														+ "','"
														+ rec.auditState
														+ "',2000,1200) style='color:#3f99b1;'>";
												href += "编辑</a>]";
												href += "[<a href='#' onclick=dele('publishNewsController.do?delete=key','publishNewsList','"
														+ rec.pkId
														+ "','"
														+ rec.publishState
														+ "') style='color:#3f99b1;'>";
												href += "删除</a>]";
												href += "[<a href='#' onclick=details('" + rec.pkId + "',1000,500) style='color:#3f99b1;'>";
												href += "详情</a>]";
												href += "[<a href='#' onclick=auditw('publishNewsController.do?auditw=key','"
														+ rec.pkId
														+ "','"
														+ rec.auditState
														+ "') style='color:#3f99b1;'>";
												href += "提交审核</a>]";
												href += "[<a href='#' onclick=cancelpublish('publishNewsController.do?cancelpublish=key','"
														+ rec.pkId
														+ "','"
														+ rec.publishState
														+ "','"
														+ rec.auditState
														+ "') style='color:#3f99b1;'>";
												href += "撤销发布</a>]";
												//设置操作里面按钮权限		
												/* var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
															var oper = operCode.split(', ');
															var i = oper.length;
															if(oper !=""){
															  for(var j=0;j<oper.length;j++){
															
																  if(oper[j] == "modify"){
																		 href=href.replace("[<a href='#' onclick=update('"+rec.pkId+"') style='color:#3f99b1;'>编辑</a>]","");
																		}
																 if(oper[j] == "detail"){
																	 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>详情</a>]","");
																	}
																 if(oper[j] == "delete"){
																	 href=href.replace("[<a href='#' onclick=dele('publishNewsController.do?delete=key','publishNewsList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
																	}  
														
																}
															} */
												return href;
											}
										} ] ],
								onLoadSuccess : function(data) {
									$("#publishNewsList").parent().find(
											"div .datagrid-header-check")
											.children(
													"input[type=\"checkbox\"]")
											.eq(0).attr("style",
													"display:none;");
									//无查询结果提示
									if (data.total == 0) {
										$('.datagrid-view2 .datagrid-body')
												.append(
														'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

										$(this).closest('div.datagrid-wrap')
												.find('div.datagrid-pager')
												.hide();
									}
									//清除所选的行
									$("#publishNewsList").datagrid(
											"clearSelections");
								},
								//rowIndex:行索引 rowData：行数据
								onClickRow : function(rowIndex, rowData) {
									rowid = rowData.id;
									gridname = 'publishNewsList';
								}
							});
			$("#publishNewsList").datagrid('getPager').pagination({
				beforePageText : '',
				afterPageText : '/{pages}',
				displayMsg : '显示{from}-{to}共{total}条',
				showPageList : true,
				showRefresh : true
			});

			$("#publishNewsList").datagrid('getPager').pagination({
				onBeforeRefresh : function(pageNumber, pageSize) {
					$(this).pagination('loading');
					$(this).pagination('loaded');
				}
			});
		};

		$(function() {
			//查询
			query();

			noauto();

		});

		function reset() {
			$('#newsTitle').val("");
			$('#auditState').val("");
			$('#publishState').val("");
			$('#newsTop').val("");
			$('#startDate').val('');
			$('#endDate').val('');
		}

		//修改页面
		function update(url, id, pkId, auditStatue) {
			//var rowsData = $('#' + id).datagrid('getSelections');
			//var rows = $('#' + id).datagrid('getSelections');
			var rowsData = $('#' + id).datagrid('getSelections');
			if (rowsData.length > 1) {
				tip('请选择一条数据修改');
				return;
			}
			if (auditStatue == "待审核") {
				tip('该数据已经提交审核，不能进行修改');
				return;
			}
			if (auditStatue == "通过") {
				tip('该数据已经通过审核，不能进行修改');
				return;
			}

			url += '&pkId=' + pkId;

			add_win('修改', '${ctx}/publishNewsController.do?modify=key&pkId='
					+ pkId, 'publishNewsList', 1200, 660);

		}

		//详情页面
		function details(pkId) {
			add_win('详情', 'publishNewsController.do?detials=key&pkId='
					+ pkId, 'publishNewsList', 1200, 660);

		}
        //提交审核
		function auditw(url, pkId, auditState) {
			if (auditState == "待审核") {
				tip('该数据已经提交，不能重复提交');
				return;
			}
			if (auditState == "通过") {
				tip('该数据已经审核通过，不能重复审核');
				return;
			}
			url += '&pkId=' + pkId;
			window.top.$.messager.defaults = {
				ok : "是",
				cancel : "否"
			};
			window.top.$.messager.confirm('提示信息', '你确定要提交审核吗', function(r) {
				if (r) {

					$.post(url, function(result) {
						if (result.success) {
							$('#' + id).datagrid('reload'); // reload the user data
							$('#' + id).treegrid('reload'); // reload the user data
						} else {
							window.top.$.messager.show({// show error message
								title : 'Error',
								msg : result.errorMsg
							});
						}
					}, 'json');
					query();
				}
			});
		}
		//新闻撤销发布
		function cancelpublish(url, pkId, publishState, auditState) {
			if (publishState != "已发布") {
				tip('该数据没有进行发布，请重新选择');
				return;
			}
			url += '&pkId=' + pkId;
			window.top.$.messager.defaults = {
				ok : "是",
				cancel : "否"
			};
			window.top.$.messager.confirm('提示信息', '你确定要撤销发布吗', function(r) {
				if (r) {

					$.post(url, function(result) {
						if (result.success) {
							$('#' + id).datagrid('reload'); // reload the user data
							$('#' + id).treegrid('reload'); // reload the user data
						} else {
							window.top.$.messager.show({// show error message
								title : 'Error',
								msg : result.errorMsg
							});
						}
					}, 'json');
					query();
				}
			});
		}

		//删除
		function dele(url, id, pkId, publishState) {

			var rowsData = $('#' + id).datagrid('getSelections');
			if (rowsData.length > 1) {
				tip('请选择一条删除数据');
				return;
			}
			if (publishState=="已发布") {
				tip('已经发布的新闻不能进行删除');
				return;
			}
			url += '&pkId=' + pkId;
			window.top.$.messager.defaults = {
				ok : "是",
				cancel : "否"
			};
			window.top.$.messager.confirm('提示信息', '你确定要删除吗', function(r) {
				if (r) {

					$.post(url, function(result) {
						if (result.success) {
							$('#' + id).datagrid('reload'); // reload the user data
							$('#' + id).treegrid('reload'); // reload the user data
						} else {
							window.top.$.messager.show({// show error message
								title : 'Error',
								msg : result.errorMsg
							});
						}
					}, 'json');
				}
			});
		}

		function reloadTable() {
			try {
				$('#' + publishNewsList).datagrid('reload');
				$('#' + publishNewsList).treegrid('reload');
			} catch (ex) {
			}
		}

		function getdemoListSelected(field) {
			return getSelected(field);
		}

		//选中单行
		function getSelected(field) {
			var row = $('#' + publishNewsList).datagrid('getSelected');
			if (row != null) {
				value = row[field];
			} else {
				value = '';
			}
			return value;
		}

		function getUserListSelections(field) {
			var ids = [];
			var rows = $('#publishNewsList').datagrid('getSelections');
			for ( var i = 0; i < rows.length; i++) {
				ids.push(rows[i][field]);
			}
			ids.join(',');
			return ids;
		};

		//页面初始化时，设置按钮的权限
		function noauto() {
			//取得对应的权限的值(后台返回的是没有的权限的值)
			var operCode = $("#noauto_operationCodes").val().replace("[", "")
					.replace("]", "");
			var oper = operCode.split(', ');
			var i = oper.length;
			$(oper).each(function(i) {
				$('#' + oper[i]).hide();
			});
		}
	</script>
</body>
</html>





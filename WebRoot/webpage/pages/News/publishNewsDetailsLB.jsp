<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>新闻轮播图详情页面</title>
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
<link href="${ctx}/ueditor/themes/default/css/ueditor.css"
	type="text/css" rel="stylesheet">
<script src="${ctx}/ueditor/ueditor.config.js"></script>
<script src="${ctx}/ueditor/ueditor.all.js"></script>
<script type="text/javascript">
	$(function() {
		init();
	});

	function saveData(url) {
        
		var form = $("#myform").Validform();
		var newsType = $('#newsType').val();
		var newsTop = $('#newsTop').val();
		var roleIds= $('#roleIds').val();
		if (newsType == '' || newsType == '请选择') {
			tip("请选择新闻类型!");
			return;
		}
		if (newsTop == '' || newsTop == '请选择') {
			tip("请选择是否置顶!");
			return;
		}
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			window.top.$.messager.progress({
				text : '数据保存中....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "true");
		}

		$('#myform').form(
				'submit',
				{
					url : url+"&roleIds"+roleIds,
					onSubmit : function() {
						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
							} else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
								return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'),
									data.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
	}
	function chooseNewsRelated(){
		var url='${ctx}/publishNewsController.do?sysPermissionInfoAddLB=key';
	 	var width = width?width:700;
		var height = height?height:400;
		if(width=="100%" || height=="100%"){
			width = document.body.offsetWidth;
			height =document.body.offsetHeight-100;
		}
				$.dialog({
					zIndex: 1997,
					content: 'url:'+url,
					lock : true,
					width:width,
					height:height,
					title:"",
					opacity : 0.3,
					cache:false
				});

}
</script>
<style type="text/css">
input[type="file"] {
	color: transparent;
}
#imghead {
    width: 100%;
    height: 100%;
    }
#preview {
	width: 200px;
	height: 150px;
	/* border: 1px solid #000;
	overflow: hidden;
	position: absolute;
	top: 0px;
	right: 11.6%;
	border: 1px solid #a5aeb6; */
}
</style>
</head>
<body style="overflow-y: scroll">
	<table style="overflow-y: scroll" width="100%" id="publishNewsListLB"
		toolbar="#demoListtb"></table>
	<form id="myform" name="myform" enctype="multipart/form-data" method="post">
		<c:if test="${publishNewsInfo.pkId!= null}">
			<input id="pkId" name="pkId" type="hidden"
				value="${publishNewsInfo.pkId}">
		</c:if>
		<table style="width: 100%;" cellpadding="0" cellspacing="1"
			class="formtable">

			<tr>
				<td align="right" width="15%" nowrap><label
					class="Validform_label">标题:
				</label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="newsTitle" onkeypress="notNull(this);" name="newsTitle"
					value="${publishNewsInfo.newsTitle}" readonly="readonly" /></td>
					<%-- <td class="value" width="10%">
					<div id="preview">
						<img id="imghead" width=120 height=80  border=0 
						src='<%=request.getContextPath()%>${publishNewsInfo.newsImage }'>
					</div> 
				</td> --%>
			    <td align="right" width="15%" nowrap><label
					class="Validform_label"><span>发布时间配置:</span></label>
				</td>
				<td class="value" width="35%"><input type="text" id="newsPublishConfig"
					readonly="readonly" name="newsPublishConfig" value="${aaa}"
					class="Wdate" style="width: 260px; height: 36px;"
					onkeypress="notNull(this);"></td>
			</tr>
			<tr>
			<td align="right" width="10%" nowrap><label
					class="Validform_label"><span>是否轮播:</span></label></td>
					<td class="value" width="35%"><input class="inputxt"
					id="newsCarousel" onkeypress="notNull(this);"
					name="newsCarousel" value="${publishNewsInfo.newsCarousel}"
					readonly="readonly" /></td>
				<%-- <td align="right" width="5%" nowrap><label
					class="Validform_label"><span>新闻配图:</span></label></td>
				<td   class="value" width="15%"><input type="file"  style="width: 72px"
					id="photo" class="form-control-input"
					onchange="previewImage(this)" name="pictureUpload"
					value="${publishNewsInfo.newsImage }" /><span style="color: red;">*&nbsp;</span><span style="font-size: 12px;color: #ccc;">推荐长宽比为<span style="color:red;">1.5:1</span>例：<span style="color:red;">720*480</span></span></td> --%>
			<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>关联新闻:</span></label>
    		</td>
    		<td class="value" width="35%" >
    		 	<input class="inputxt" id="newsRelated" name="newsRelated" value="${publishNewsInfo.newsRelated}"  readonly="readonly" />
    		</td>
    		</tr>
    		<tr><td align="right" width="15%" nowrap><label
					class="Validform_label">审核意见: </label></td>
				<td class="value" width="35%"><textarea 
						class="inputxt" id="newsAuditOpinion" MaxLength="100"
						onkeypress="notNull(this);" name="newsAuditOpinion" cols="45" rows="5" readonly="readonly"						
						style="outline: none; width: 260px; height: 100px; font-size: 14px;">${publishNewsInfo.newsAuditOpinion}</textarea></td>
				<td align="right" width="5%" nowrap><label
					class="Validform_label"><span>新闻配图:</span></label></td>
					<td class="value" width="35%">
					<div id="preview">
						<img id="imghead" width=120 height=80  border=0 
						src='${publishNewsInfo.newsImage }'>
					</div>
<!-- 					<input type="file"  style="width: 72px" -->
<!-- 					id="photo" class="form-control-input" -->
<!-- 					onchange="previewImage(this)" name="pictureUpload" -->
<%-- 					value="${publishNewsInfo.newsImage }" /><span style="color: red;">*&nbsp;</span>  --%>
				</td> 
			</tr>

			<tr>
				<td align="center" colspan="8">
				<span class="bluebtnhover"><a href="javascript:void(0)"
						id="restBtn" class="easyui-linkbutton dialogbtn bluebtn"
						onclick="closeWindow()">关闭</a>
				</span></td>
			</tr>
		</table>
	</form>
</body>
<script type="text/javascript">
	//图片上传预览    IE是用了滤镜。
	function previewImage(file) {
		//判断图片大小
		var fileSize = 0; 
		var filetypes =[".jpg",".jpeg",".png",]; 
		var filepath = file.value; 
		var filemaxsize = 1024*0.1;
		var filemaxsizemax = 1024*1
		var MAXWIDTH = 120;
		var MAXHEIGHT = 80;
		var div = document.getElementById('preview');
		if (file.files && file.files[0]) {
			div.innerHTML = '<img id=imghead>';
			var img = document.getElementById('imghead');
			img.onload = function() {
				var rect = clacImgZoomParam(MAXWIDTH,
						MAXHEIGHT, img.offsetWidth,
						img.offsetHeight);
				img.width = rect.width;
				img.height = rect.height;
				//                 img.style.marginLeft = rect.left+'px';
				img.style.marginTop = rect.top + 'px';
			}
			fileSize = file.files[0].size; 
			var size = fileSize / 1024; 
			var strExtension = filepath.substr(filepath.lastIndexOf('.') + 1);  
		    if (strExtension != 'jpg' && strExtension != 'gif'  
		            && strExtension != 'png' && strExtension != 'bmp') {  
		        alert("请上传jpg/gif/png/bmp类型的图片");  
		        return false;  
		    }
			/* if(size>filemaxsizemax){ 
			alert("附件大小不能大于1M！"); 
			file.value =""; 
			return false; 
			} 
			if(size<filemaxsize){ 
			alert("附件大小不能小于100kb！"); 
			file.value =""; 
			return false; 
			}  */
			var reader = new FileReader();
			reader.onload = function(evt) {
				img.src = evt.target.result;
			}
			
			reader.readAsDataURL(file.files[0]);
		} else //兼容IE
		{
			var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
			file.select();
			var src = document.selection.createRange().text;
			div.innerHTML = '<img id=imghead>';
			var img = document.getElementById('imghead');
			img.filters
					.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
			var rect = clacImgZoomParam(MAXWIDTH,
					MAXHEIGHT, img.offsetWidth,
					img.offsetHeight);
			status = ('rect:' + rect.top + ',' + rect.left
					+ ',' + rect.width + ',' + rect.height);
			div.innerHTML = "<div id=divhead style='width:"+rect.width+"px;height:"+rect.height+"px;margin-top:"+rect.top+"px;"+sFilter+src+"\"'></div>";
		}
	}
	function clacImgZoomParam(maxWidth, maxHeight, width,
			height) {
		var param = {
			top : 0,
			left : 0,
			width : width,
			height : height
		};
		if (width > maxWidth || height > maxHeight) {
			rateWidth = width / maxWidth;
			rateHeight = height / maxHeight;

			if (rateWidth > rateHeight) {
				param.width = maxWidth;
				param.height = Math.round(height
						/ rateWidth);
			} else {
				param.width = Math
						.round(width / rateHeight);
				param.height = maxHeight;
			}
		}

		param.left = Math
				.round((maxWidth - param.width) / 2);
		param.top = Math
				.round((maxHeight - param.height) / 2);
		return param;

		if (file.value == "") {
			alert("请上传图片");
			return false;
		} else {
			if (!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/
					.test(file.value)) {
				alert("图片类型必须是,jpeg,jpg,png,bmp类型");
				file.value = "";
				return false;
			}

			else {
				var img = new Image();
				img.src = filepath;
				while (true) {
					if (img.fileSize > 0) {
						if (img.fileSize > 0.04 * 1024) {
							alert("图片不大于40KB。");
							return false;
						}
						break;
					}

				}
			}
		}
		return true;
	}
</script>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>具体新闻审核页面</title>
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
<link href="${ctx}/ueditor/themes/default/css/ueditor.css"
	type="text/css" rel="stylesheet">
<script src="${ctx}/ueditor/ueditor.config.js"></script>
<script src="${ctx}/ueditor/ueditor.all.js"></script>
<script type="text/javascript">
	$(function() {
		init();
	});
    
	//驳回
	function saveData(url) {

		var form = $("#myform").Validform();
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			// $.dialog.tips('数据保存中...',500,'loading.gif');
			window.top.$.messager.progress({
				text : '驳回....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "true");
		}

		$('#myform').form(
				'submit',
				{
					url : url,
					onSubmit : function() {

						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
								//			W.query();
							}
							/*
							 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
							 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
							 */
							else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
								return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'),
									data.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
		zTree = $.fn.zTree.init($("#menuId"), setting, data);
	}
	//通过审核
	function saveData1(url) {

		var form = $("#myform").Validform();
		//alert(url);
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			// $.dialog.tips('数据保存中...',500,'loading.gif');
			window.top.$.messager.progress({
				text : '通过审核....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "true");
		}

		$('#myform').form(
				'submit',
				{
					url : url,
					onSubmit : function() {

						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
								//			W.query();
							}
							/*
							 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
							 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
							 */
							else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
								return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'),
									data.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
		query();
	}
	
</script>

<style type="text/css">
input[type="file"] {
	color: transparent;
}
#preview {
	width: 200px;
	height: 150px;
	/* border: 1px solid #000;
	overflow: hidden;
	position: absolute;
	top: 13px;
	right: 18.1%;
	border: 1px solid #a5aeb6; */
}

#aaa{
	width: 900px;
	height: 100px;
}
</style>
</head>
<body style="overflow-y: scroll">
	<table style="overflow-y: scroll" width="100%" id="assessmentInfoList"
		toolbar="#demoListtb"></table>
	<form id="myform" name="myform" enctype="multipart/form-data" method="post">
		<c:if test="${publishNewsInfo.pkId!= null}">
			<input id="pkId" name="pkId" type="hidden"
				value="${publishNewsInfo.pkId}">
		</c:if>
		<table style="width: 100%;" cellpadding="0" cellspacing="1"
			class="formtable">

			<tr>
				<td align="right" width="15%" nowrap><label
					class="Validform_label">标题:
				</label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="newsTitle" onkeypress="notNull(this);" name="newsTitle"
				 value="${publishNewsInfo.newsTitle}" readonly="readonly" /><span style="color: red;">*</span></td>
				
				<td align="right" width="15%" nowrap>
				<label
					class="Validform_label"><span>新闻类型:</span></label></td>
				<td class="value" width="35%" >
		   		<input class="inputxt"
					id="newsType" onkeypress="notNull(this);"
					name="newsType" value="${publishNewsInfo.newsType}"
					readonly="readonly" /><span style="color: red;">*</span>
    		 </td>
				<%-- --%>

			</tr>
			
			<tr>
				<td align="right" width="15%" nowrap><label
					class="Validform_label"><span>作者:</span>
				</label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="newsAuthor" onkeypress="notNull(this);" name="newsAuthor"
					value="${publishNewsInfo.newsAuthor}" readonly="readonly"/><span style="color: red;">*</span></td>
					
					<td align="right" width="15%" nowrap><label
					class="Validform_label"><span>是否置顶:</span></label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="newsTop" onkeypress="notNull(this);"
					name="newsTop" value="${publishNewsInfo.newsTop}"
					readonly="readonly" /><span style="color: red;">*</span></td>
					
					<%--  --%>
			</tr>
			<tr>
			    <td align="right" width="15%" nowrap><label
					class="Validform_label"><span>发布时间配置:</span></label>
				</td>
				<td class="value" width="35%"><input type="text" id="newsPublishConfig"
					readonly="readonly" name="newsPublishConfig" value="${aaa}"
					class="Wdate" style="width: 260px; height: 36px;"
					onkeypress="notNull(this);"><span style="color: red;">*</span></td>
					
					<td align="right" width="15%" nowrap><label class="Validform_label">新闻来源:</label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="newsSource" onkeypress="notNull(this);" name="newsSource"
					value="${publishNewsInfo.newsSource}" readonly="readonly" /><span style="color: red;">*</span></td>
			</tr>
			<tr>
			
			</tr>
			<tr>
				
						
				<td align="right" width="15%" nowrap><label
					class="Validform_label"><span>新闻配图:</span></label></td>
				<td class="value" width="35%">
					<div id="preview">
						<img id="imghead" width="200px" height="150px"  border=0 
						src='${publishNewsInfo.newsImage }'>
					</div> 
					<input type="file"  style="width: 72px"
					id="photo" class="form-control-input"
					onchange="previewImage(this)" name="pictureUpload"
					value="${publishNewsInfo.newsImage}"/><span style="color: red;">*</span>
				</td>
				<td align="right" width="15%" nowrap><label
					class="Validform_label">审核意见: </label></td>
				<td class="value" width="35%"><textarea 
						class="inputxt" id="newsAuditOpinion" MaxLength="100"
						onkeypress="notNull(this);" name="newsAuditOpinion" datatype="*1-100"  nullmsg="请输入审核意见" cols="45" rows="5"
						style="outline: none; width: 260px; height: 100px; font-size: 14px;"></textarea><span style="color: red;">*</span></td>	
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label
					class="Validform_label">新闻内容: </label></td>
				<td  id = "aaa" colspan="3" width="600" height="400" align="left" bgcolor="#FFFFFF">
				<!-- <script id="newsContent"  type="text/plain">${publishNewsInfo.newsContent}</script>  -->
				<textarea rows="3"  style="width:900px;height: 200px;" onkeypress="notNull(this);" name="newsContent" class="newsContent" id="newsContent" >${publishNewsInfo.newsContent}</textarea>
				<script
						type="text/javascript">
							var editor = UE.getEditor('newsContent');
							editor.ready(function() {
								//不可编辑
								editor.setDisabled();
								});
						</script></td>
			</tr>

			<tr>
				<td align="center" colspan="8">
				<span class="orangebtnhover">
						<a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn"
						onclick="saveData1('${ctx}/publishNewsController.do?through=key')">通过</a>
				</span>
				<span class="orangebtnhover">
						<a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn"
						onclick="saveData('${ctx}/publishNewsController.do?rejected=key')">不通过</a>
				</span> 
				</td>
			</tr>
		</table>
	</form>
</body>
<script type="text/javascript">
//图片上传预览    IE是用了滤镜。
function previewImage(file) {
	//判断图片大小
	var fileSize = 0; 
	var filetypes =[".jpg",".jpeg",".png",]; 
	var filepath = file.value; 
	var filemaxsize = 1024*0.1;
	var filemaxsizemax = 1024*1
	var MAXWIDTH = 120;
	var MAXHEIGHT = 80;
	var div = document.getElementById('preview');
	if (file.files && file.files[0]) {
		div.innerHTML = '<img id=imghead>';
		var img = document.getElementById('imghead');
		img.onload = function() {
			var rect = clacImgZoomParam(MAXWIDTH,
					MAXHEIGHT, img.offsetWidth,
					img.offsetHeight);
			img.width = rect.width;
			img.height = rect.height;
			//                 img.style.marginLeft = rect.left+'px';
			img.style.marginTop = rect.top + 'px';
		}
		fileSize = file.files[0].size; 
		var size = fileSize / 1024; 
		var strExtension = filepath.substr(filepath.lastIndexOf('.') + 1);  
	    if (strExtension != 'jpg' && strExtension != 'gif'  
	            && strExtension != 'png' && strExtension != 'bmp') {  
	        alert("请上传jpg/gif/png/bmp类型的图片");  
	        return false;  
	    }
		/* if(size>filemaxsizemax){ 
		alert("附件大小不能大于1M！"); 
		file.value =""; 
		return false; 
		} 
		if(size<filemaxsize){ 
		alert("附件大小不能小于100kb！"); 
		file.value =""; 
		return false; 
		}  */
		var reader = new FileReader();
		reader.onload = function(evt) {
			img.src = evt.target.result;
		}
		
		reader.readAsDataURL(file.files[0]);
	} else //兼容IE
	{
		var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
		file.select();
		var src = document.selection.createRange().text;
		div.innerHTML = '<img id=imghead>';
		var img = document.getElementById('imghead');
		img.filters
				.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
		var rect = clacImgZoomParam(MAXWIDTH,
				MAXHEIGHT, img.offsetWidth,
				img.offsetHeight);
		status = ('rect:' + rect.top + ',' + rect.left
				+ ',' + rect.width + ',' + rect.height);
		div.innerHTML = "<div id=divhead style='width:"+rect.width+"px;height:"+rect.height+"px;margin-top:"+rect.top+"px;"+sFilter+src+"\"'></div>";
	}
}
function clacImgZoomParam(maxWidth, maxHeight, width,
		height) {
	var param = {
		top : 0,
		left : 0,
		width : width,
		height : height
	};
	if (width > maxWidth || height > maxHeight) {
		rateWidth = width / maxWidth;
		rateHeight = height / maxHeight;

		if (rateWidth > rateHeight) {
			param.width = maxWidth;
			param.height = Math.round(height
					/ rateWidth);
		} else {
			param.width = Math
					.round(width / rateHeight);
			param.height = maxHeight;
		}
	}

	param.left = Math
			.round((maxWidth - param.width) / 2);
	param.top = Math
			.round((maxHeight - param.height) / 2);
	return param;

	if (file.value == "") {
		alert("请上传图片");
		return false;
	} else {
		if (!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/
				.test(file.value)) {
			alert("图片类型必须是,jpeg,jpg,png,bmp类型");
			file.value = "";
			return false;
		}

		else {
			var img = new Image();
			img.src = filepath;
			while (true) {
				if (img.fileSize > 0) {
					if (img.fileSize > 0.04 * 1024) {
						alert("图片不大于40KB。");
						return false;
					}
					break;
				}

			}
		}
	}
	return true;
}
</script>
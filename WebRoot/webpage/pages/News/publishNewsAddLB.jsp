<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>新闻轮播图新增</title>
<link href="${ctx}/resources/css/layout.css" type="text/css"
	rel="stylesheet">
<link href="${ctx}/ueditor/themes/default/css/ueditor.css"
	type="text/css" rel="stylesheet">
<script src="${ctx}/ueditor/ueditor.config.js"></script>
<script src="${ctx}/ueditor/ueditor.all.js"></script>
<style type="text/css">
	#imghead {
    width: 100%;
    height: 100%;
    }
</style>
<script type="text/javascript">
	$(function() {
		init();
	});

	function saveData(url) {
        
		var form = $("#myform").Validform();
		var newsType = $('#newsType').val();
		var newsTop = $('#newsTop').val();
		var newsCarousel = $('#newsCarousel').val();
		var roleIds= $('#roleIds').val();
		var newsRelated = $('#newsRelated').val();
		var remark2=$('#remark2').val();
		var remark3=$('#remark3').val();
		if(remark2!='是'){
            if (newsRelated == '' || newsRelated == '请选择') {
                tip("请选择关联新闻!");
                return;
            }
		}else{
            if (remark3 == '' || remark3 == null) {
                tip("请填写url!");
                return;
            }
		}

	
		if (newsType == '' || newsType == '请选择') {
			tip("请选择新闻类型!");
			return;
		}
		 if (newsCarousel == '' || newsCarousel == '请选择') {
			tip("请选择是否轮播!");
			return;
		} 
		if (newsTop == '' || newsTop == '请选择') {
			tip("请选择是否置顶!");
			return;
		}
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			window.top.$.messager.progress({
				text : '数据保存中....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "true");
		}

		$('#myform').form(
				'submit',
				{
					url : url+"&roleIds"+roleIds,
					onSubmit : function() {
						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
							} else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
								return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'),
									data.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
	}
function saveData1(url) {
        
		var form = $("#myform").Validform();
		var newsType = $('#newsType').val();
		var newsTop = $('#newsTop').val();
		var newsCarousel = $('#newsCarousel').val();
		var roleIds= $('#roleIds').val();
		var newsRelated = $('#newsRelated').val();
		if (newsRelated == '' || newsRelated == '请选择') {
			tip("请选择关联新闻!");
			return;
		}
		if (newsType == '' || newsType == '请选择') {
			tip("请选择新闻类型!");
			return;
		}
		if (newsCarousel == '' || newsCarousel == '请选择') {
			tip("请选择是否轮播!");
			return;
		}
		if (newsCarousel == '否') {
			tip("是否轮播状态为否的数据不能直接提交审核!");
			return;
		}
		if (newsTop == '' || newsTop == '请选择') {
			tip("请选择是否置顶!");
			return;
		}
		if (form.check()) {
			$.dialog.setting.zIndex = 1997;
			window.top.$.messager.progress({
				text : '数据保存中....',
				interval : 300
			});
			$("#saveBtn").attr("disabled", "true");
		}

		$('#myform').form(
				'submit',
				{
					url : url+"&roleIds"+roleIds,
					onSubmit : function() {
						var form = $("#myform").Validform();
						return form.check();
					},
					success : function(data) {
						var windowapi = frameElement.api, W = windowapi.opener;
						var d;
						try {
							d = $.parseJSON(data);
							if (d.success == true) {
								window.top.$.messager.progress('close');
								windowapi.close();
								W.tip(d.msg);
								W.query();
							} else {
								window.top.$.messager.progress('close');
								$("#saveBtn").removeAttr("disabled");
								window.top.$.messager.alert('提示', d.msg);
								return;
							}
						} catch (ex) {
							var emsg = data.substring(data.indexOf('错误描述'),
									data.indexOf('错误信息'));
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('错误', emsg);
							return;
						}
					}
				});
	}
	function chooseNewsRelated(){
		var url='${ctx}/publishNewsController.do?sysPermissionInfoAddLB=key';
	 	var width = width?width:700;
		var height = height?height:400;
		if(width=="100%" || height=="100%"){
			width = document.body.offsetWidth;
			height =document.body.offsetHeight-100;
		}
				$.dialog({
					zIndex: 1997,
					content: 'url:'+url,
					lock : true,
					width:width,
					height:height,
					title:"",
					opacity : 0.3,
					cache:false
				});

}
</script>
<style type="text/css">
input[type="file"] {
	color: transparent;
}
#preview {
	width: 200px;
	height: 150px;
	/* border: 1px solid #000;
	overflow: hidden;
	position: absolute;
	top: 0px;
	right: 11.6%;
	border: 1px solid #a5aeb6; */
}
</style>
</head>
<body style="overflow-y: scroll">
	<table style="overflow-y: scroll" width="100%" id="publishNewsListLB"
		toolbar="#demoListtb"></table>
	<form id="myform" name="myform" enctype="multipart/form-data" method="post">
		<c:if test="${publishNewsInfo.pkId!= null}">
			<input id="pkId" name="pkId" type="hidden"
				value="${publishNewsInfo.pkId}">
		</c:if>
		<table style="width: 100%;" cellpadding="0" cellspacing="1"
			class="formtable">

			<tr>
				<td align="right" width="15%" nowrap><label
					class="Validform_label">标题:
				</label></td>
				<td class="value" width="35%"><input class="inputxt"
					id="newsTitle" onkeypress="notNull(this);" name="newsTitle"
					datatype="*1-100" errormsg="请输入1-100位合法的字符" nullmsg="请输入新闻标题" /><span style="color: red;">*</span></td>
				<td align="right" width="10%" nowrap><label
					class="Validform_label"><span>是否轮播:</span></label></td>
				<td class="value" width="35%"><select id="newsCarousel"
					name="newsCarousel" onkeypress="notNull(this);">
						<option value="">--请选择--</option>
						<option value="是"
							<c:if test="${publishNewsInfo.newsCarousel=='是'}">selected</c:if>>是</option>
						<option value="否"
							<c:if test="${publishNewsInfo.newsCarousel=='否'}">selected</c:if>>否</option>
				</select><span style="color: red;">*</span></td>
			</tr>
			<tr>
				<td align="right" width="10%" nowrap><label
						class="Validform_label"><span>是否外链:</span></label></td>
				<td class="value" width="35%"><select id="remark2"
													  name="remark2" >
					<option value="">--请选择--</option>
					<option value="是"
							<c:if test="${publishNewsInfo.remark2=='是'}">selected</c:if>>是</option>
					<option value="否"
							<c:if test="${publishNewsInfo.remark2=='否'}">selected</c:if>>否</option>
				</select><span style="color: red;">*</span></td>
				<td align="right" width="15%" nowrap><label
						class="Validform_label">url:
				</label></td>
				<td class="value" width="35%"><input class="inputxt"
													 id="remark3"  name="remark3"
													/><span style="color: red;">*</span></td>

			</tr>
			<tr>
			    <td align="right" width="15%" nowrap><label
					class="Validform_label"><span>发布时间配置:</span></label>
				</td>
				<td class="value" width="35%"><input type="text" id="newsPublishConfiglb"
				    name="newsPublishConfiglb" 
					class="Wdate" style="width: 260px; height: 36px;"
					datatype="*1-9999" nullmsg="请选择发布时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-#{%d}'})"></input><span style="color: red;">*</span></td>
					<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>关联新闻:</span></label>
    		</td>
    		<td class="value" width="35%" >
    		 	<input class="inputxt" id="newsRelated" name="newsRelated" value="${publishNewsInfo.newsRelated}"  readonly="readonly" />
    			<input class="inputxt" id="roleIds"  name="roleIds" type="hidden" value="${roleIds}" /><span style="color:red">*</span>
    			<a href="javascript:chooseNewsRelated();">请选择</a>
    		</td>
			</tr>
			<tr>
				<td align="right" width="15%" nowrap><label
					class="Validform_label"><span>新闻配图:</span></label>
				</td>
				<td   class="value" width="5%">
					<div id="preview">
						<img id="imghead" width="300px" height="200px"  border=0 src='<%=request.getContextPath()%>/images/defaul.jpg'><br>
					</div> 
					<input type="file"  style="width: 72px" id="photo" class="form-control-input" onchange="previewImage(this)" name="pictureUpload" value="${publishNewsInfo.newsImage }"/>
					</td>
			</tr>
			<tr>
				<td align="center" colspan="8">
				<span class="orangebtnhover">
						<a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn"
						onclick="saveData('${ctx}/publishNewsController.do?saveUpdateLB=key')">保存</a>
				</span>
				<span class="orangebtnhover">
						<a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn"
						onclick="saveData1('${ctx}/publishNewsController.do?auditLB=key')">提交审核</a>
				</span> 
				<span class="bluebtnhover"><a href="javascript:void(0)"
						id="restBtn" class="easyui-linkbutton dialogbtn bluebtn"
						onclick="closeWindow()">关闭</a>
				</span></td>
			</tr>
		</table>
	</form>
</body>
<script type="text/javascript">
	//图片上传预览    IE是用了滤镜。
	function previewImage(file) {
		//判断图片大小
		var fileSize = 0; 
		var filetypes =[".jpg",".jpeg",".png","bmp","gif",".JPG",".JPEG",".PNG","BMP","GIF",]; 
		var filepath = file.value; 
		var filemaxsize = 1024*0.1;
		var filemaxsizemax = 1024*1
		var MAXWIDTH = 120;
		var MAXHEIGHT = 80;
		var div = document.getElementById('preview');
		if (file.files && file.files[0]) {
			div.innerHTML = '<img id=imghead>';
			var img = document.getElementById('imghead');
			img.onload = function() {
				var rect = clacImgZoomParam(MAXWIDTH,
						MAXHEIGHT, img.offsetWidth,
						img.offsetHeight);
				img.width = rect.width;
				img.height = rect.height;
				//                 img.style.marginLeft = rect.left+'px';
				img.style.marginTop = rect.top + 'px';
			}
			fileSize = file.files[0].size; 
			var size = fileSize / 1024; 
			var strExtension = filepath.substr(filepath.lastIndexOf('.') + 1);  
		    if (strExtension != 'jpg' && strExtension != 'gif'  
		            && strExtension != 'png' && strExtension != 'bmp'
		            && strExtension != 'jpeg' && strExtension != 'JPG' 
		            && strExtension != 'JPEG' && strExtension != 'PNG'
				    && strExtension != 'BMP' && strExtension != 'GIF') {  
		        alert("请上传jpg/gif/png/bmp/gif类型的图片");  
		        return false;  
		    }
			/* if(size>filemaxsizemax){ 
			alert("附件大小不能大于1M！"); 
			file.value =""; 
			return false; 
			}  */
			/* if(size<filemaxsize){ 
			alert("附件大小不能小于100kb！"); 
			file.value =""; 
			return false; 
			}  */
			var reader = new FileReader();
			reader.onload = function(evt) {
				img.src = evt.target.result;
			}
			
			reader.readAsDataURL(file.files[0]);
		} else //兼容IE
		{
			var sFilter = 'filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod=scale,src="';
			file.select();
			var src = document.selection.createRange().text;
			div.innerHTML = '<img id=imghead>';
			var img = document.getElementById('imghead');
			img.filters
					.item('DXImageTransform.Microsoft.AlphaImageLoader').src = src;
			var rect = clacImgZoomParam(MAXWIDTH,
					MAXHEIGHT, img.offsetWidth,
					img.offsetHeight);
			status = ('rect:' + rect.top + ',' + rect.left
					+ ',' + rect.width + ',' + rect.height);
			div.innerHTML = "<div id=divhead style='width:"+rect.width+"px;height:"+rect.height+"px;margin-top:"+rect.top+"px;"+sFilter+src+"\"'></div>";
		}
	}
	function clacImgZoomParam(maxWidth, maxHeight, width,
			height) {
		var param = {
			top : 0,
			left : 0,
			width : width,
			height : height
		};
		if (width > maxWidth || height > maxHeight) {
			rateWidth = width / maxWidth;
			rateHeight = height / maxHeight;

			if (rateWidth > rateHeight) {
				param.width = maxWidth;
				param.height = Math.round(height
						/ rateWidth);
			} else {
				param.width = Math
						.round(width / rateHeight);
				param.height = maxHeight;
			}
		}

		param.left = Math
				.round((maxWidth - param.width) / 2);
		param.top = Math
				.round((maxHeight - param.height) / 2);
		return param;

		if (file.value == "") {
			alert("请上传图片");
			return false;
		} else {
			if (!/\.(gif|jpg|jpeg|png|GIF|JPG|PNG)$/
					.test(file.value)) {
				alert("图片类型必须是,jpeg,jpg,png,bmp类型");
				file.value = "";
				return false;
			}

			else {
				var img = new Image();
				img.src = filepath;
				while (true) {
					if (img.fileSize > 0) {
						if (img.fileSize > 0.04 * 1024) {
							alert("图片不大于40KB。");
							return false;
						}
						break;
					}

				}
			}
		}
		return true;
	}
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>查找轮播图关联新闻</title>
	</head>
<body>
<table width="100%" id="MessageLog" toolbar="#demoListtb"></table>
<div id="demoListtb" style="padding:3px; height: auto">
<div name="searchColums"  style="float:left;">
</div>
<div style="text-align:center;">
	<div class="jiekoubtntop">
		<div style="float: left;font-size:14px;margin: 5px;white-space:nowrap;">
			<label class="jiekouname">新闻标题：</label>
			<input type="text" id="newsTitle"  name="newsTitle"  style="width: 240px;height:23px; margin-left:10px;" />
		</div>
	</div>
</div>
<div style="clear:both;">
	<div class="leftbtntool">			
		<span class="orangebtnhover" style="float:right;"><a href="#"class="easyui-linkbutton" plain="true" icon="icon-Remove" onClick="reset()" >重置</a></span>
		<span class="orangebtnhover" style="float:right;"><a href="#"class="easyui-linkbutton" plain="true" icon="icon-search" onclick="query()" >查询</a></span>
	</div>
</div>
<div style="height:30px;" class="datagrid-toolbar" align="left">
		 <a href="#"class="easyui-linkbutton" plain="true" icon="icon-add" onclick="getRoles()" >确定</a>
</div>

</div>
<script type="text/javascript">
	function query(){
		var windowapi = frameElement.api, W = windowapi.opener;
		$('#MessageLog').datagrid({
			idField : 'pkId',
			title : '',
		    url : '${ctx}/publishNewsController.do?datagrids=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				'newsTitle' : $('#newsTitle').val()
			},
			columns:[[
						{field : 'ck',checkbox:true},
						{field:'pkId',title:'ID',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
						{field:'newsTitle',title:'新闻标题',  width:fixWidth(0.11),align:'center',sortable:true},
						{field:'newsAuthor',title:'作者',  width:fixWidth(0.11),align:'center',sortable:true},
						{field:'newsType',title:'新闻类型',  width:fixWidth(0.11),align:'center',sortable:true},
					    ]],
			onLoadSuccess : function(data) {
				//清楚所选的行
				$("#MessageLog").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.pkId;
				gridname = 'MessageLog';
			}
		});
		$("#MessageLog").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
};

$(function(){
	query();
});
	
	function reset(){
		
		 $('#newsTitle').val("");
	}
	
	
	function reloadTable() {
		try {
			$('#' + gridname).datagrid('reload');
			$('#' + gridname).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + gridname).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#MessageLog').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids;
	};
	
	function getRoles(){
		//获取框中的字段
		var roleIds=getUserListSelections("pkId");
		if(roleIds.length == 0){
			tip('请勾选一条数据');
			return;
		}
		var newsTitle=getUserListSelections("newsTitle");
		//var newsAuthor=getUserListSelections("newsAuthor"); 
		//var newsType=getUserListSelections("newsType"); 
		var windowapi = frameElement.api, W = windowapi.opener;
		//newsRelated为轮播图配置jsp页面的id,newsTitle为当前页面的新闻标题，意思是将新闻标题显示到轮播图配置页面的关联新闻字段中
		W.$('#roleIds').val(roleIds);
		W.$('#newsRelated').val(newsTitle);
		windowapi.close();
	}
	//ID去重
	function getIds(){
		 var tmpArr = [];
		 for(var i=0; i<pkIdsArray.length; i++){
		 	if(tmpArr.indexOf(pkIdsArray[i]) == -1){
		  		tmpArr.push(pkIdsArray[i]);
		  	}
		 }
		 return tmpArr;
	}
	
	//角色去重
	function getNames(){
		 var tmpArr = [];
		 for(var i=0; i<roleNameArray.length; i++){
		 	if(tmpArr.indexOf(roleNameArray[i]) == -1){
		  		tmpArr.push(roleNameArray[i]);
		  	}
		 }
		 return tmpArr;
	}
</script>
	</body>
</html>


		
			

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
	  init();
	});
	//保存修改
	function checkAll() {
		var url ="${ctx}/noticeController.do?saveUpdate=key";
		window.top.$.messager.confirm('Confirm', '你确定要修改吗', function(r) {
			if (r) {
				$.post(url,$("#myform").serialize(), function(result) {
					if (result.success) {
						if (result.obj == 'ok') {
							tip(result.msg);
						} else {
							$('#' + id).datagrid('reload'); // reload the user data
							$('#' + id).treegrid('reload'); // reload the user data
						}
					} else {
						window.top.$.messager.show({// show error message
							title : 'Error',
							msg : result.errorMsg
						});
					}
				}, 'json');
			}
		});
	}
	
</script>
<style type="text/css">
	.panel-title {
    font-size: 14px;
    height: 35px;
    line-height: 35px;
    padding-left: 20px;
    color: #1c6eab;
    background: url('images/blank.gif') no-repeat;
    }
    #saveBtn{
    	border-radius: 4px;
    }
</style>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<div class="panel-herder" style="width: 1170px;height: 40px">
 		<div class="panel-title">公告</div>
 		<div class="panel-tool"></div>
 	</div >
 	<div style="width: 1170px;" >
 		<form id="myform" name="myform" method="post" >
  			<c:if test="${notice.pkId!= null}">
        		<input id="pkId" name="pkId" type="hidden" value="${notice.pkId}">
    		</c:if>
  			<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公告一:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<textarea rows="4" cols="8" style="width:260px;"  name="notice1" class="notice1" id="notice1" maxlength="20"
    	       				onkeypress="notNull(this);" datatype="*0-20" errormsg="请输入0-20位内的字符"  >${notice.notice1}</textarea>
    	  			</td>
    			</tr>
    			<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公告二:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<textarea rows="4" cols="8" style="width:260px;"  name="notice2" class="notice2" id="notice2" maxlength="20"
    	       				onkeypress="notNull(this);" datatype="*0-20" errormsg="请输入0-20位内的字符"  >${notice.notice2}</textarea>
    	  			</td>
    			</tr>
    			<tr>
		  			<td align="center" width="5%" nowrap>
    		  			<label class="Validform_label"><span>公告三:</span></label>
    	  			</td>
    	  			<td class="value" width="35%" >
    		  			<textarea rows="4" cols="8" style="width:260px;"  name="notice3" class="notice3" id="notice3" maxlength="20"
    	       				onkeypress="notNull(this);" datatype="*0-20" errormsg="请输入0-20位内的字符"  >${notice.notice3}</textarea>
    	  			</td>
    			</tr>
     			<tr>
     				<td align="center" colspan="8">
        				<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
   	 				</td>
   	 				
    			</tr>
   			</table>
 		</form>
 	</div>
</body> 
  
  
  
  

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="SignInfoList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
		<div>
			<label>手机号：</label> 
			<input type="text" id="userPhoneNumber" name="userPhoneNumber" />
			<input id="pkIds" name="pkIds" value="${pkIds}" type="hidden"/>
		</div>
		
	</div>
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">
			<span class="bluebtnhover"><a onclick="exportList('${ctx}/activiteController.do?exportList=key')" id="export"  class="formbtn bluebtn">导出</a></span> 
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#SignInfoList').datagrid({
			idField : 'pkId',
			title : '报名人员信息查询',
		    url : ' ${ctx}/activiteController.do?datagrid1=key&activityId='+$("#pkIds").val(),
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : false,
			fitColumns : true,
			remoteSort:false,
			/* <!--查询参数 --> */
			queryParams:{
				'userPhoneNumber': $('#userPhoneNumber').val(),
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'userName',title:'用户姓名',  width:fixWidth(0.11),align:'center',sortable:false},
					{field:'userPhoneNumber',title:'用户手机号',  width:fixWidth(0.11),align:'center',sortable:false},
					{field:'signDate',title:'报名时间',
						formatter:function(val,rec){
			        		if(val != null && val != ''){
			        			return formattimed(val);
			        		}
			      		},
						width:fixWidth(0.15),align:'center',sortable:false},
					{field:'financialName',title:'理财师姓名',  width:fixWidth(0.11),align:'center',sortable:false},
					{field:'financialPhoneNumber',title:'理财师电话',  width:fixWidth(0.11),align:'center',sortable:false},
					{field:'remark1',title:'状态',  width:fixWidth(0.11),align:'center',sortable:false},
				    ]],
			onLoadSuccess : function(data) {
			  //  $("#SignInfoList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清除所选的行
				$("#SignInfoList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'certificateInfoList';
			}
		});
		$("#SignInfoList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#SignInfoList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
});

function reset(){
	$('#userPhoneNumber').val("");
}

function reloadTable() {
	try {
		$('#' + MailInfoList).datagrid('reload');
		$('#' + MailInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + MailInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#SignInfoList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

//全部导出
function exportList(url){
		var phoneNumber = $('#userPhoneNumber').val();
		$.ajax({
	     	type: "post",
	     	url: "${ctx}/activiteController.do?exports=key",
	     	data: {"phoneNumber":phoneNumber,"activityId":$("#pkIds").val()},
	     	dataType: "json",
	     	success: function(data){
	     		var windowapi = frameElement.api, W = windowapi.opener;
	               if(data.obj=="ok"){
	            	   window.location.href = url + "&phoneNumber=" + phoneNumber+"&activityId=" + $("#pkIds").val();
	               }else{
					 W.tip(data.msg);
	                return;
	             } 
	         } 
	 	});
		/* if(phoneNumber != null){
			window.location.href = url + "&phoneNumber=" + phoneNumber+"&activityId=" + $("#pkIds").val();
		}else{
			alert("数据为空！");
		} */
		
	}
//活动确认
function confirm(url, id,pkId,remark1) {
	//alert(remark1);
	if(remark1!="未参加"){
		tip("请勿重复点击！")
		return;
	}
	url += '&pkId=' + pkId;
	$.post(url, function(result) {
		if (result.success) {
			if (result.obj == 'ok') {
				tip(result.msg);
				query();
			} else {
				$('#' + id).datagrid('reload'); // reload the user data
				$('#' + id).treegrid('reload'); // reload the user data
			}
		} else {
			window.top.$.messager.show({// show error message
				title : 'Error',
				msg : result.errorMsg
			});
		}
	}, 'json');
}
</script>
	</body>
</html>


		
			

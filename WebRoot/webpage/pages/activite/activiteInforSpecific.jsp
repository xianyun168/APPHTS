<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <!-- 配置文件 -->
    <script type="text/javascript" src="ueditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="ueditor/ueditor.all.js"></script>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
</script>
<!-- 实例化编辑器 -->
    <script type="text/javascript">
      	var ue = UE.getEditor('activityContent', {
      	    toolbars: [
      	    	['undo', 'redo', '|',
      	            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', /*'autotypeset', 'blockquote',*/ 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
      	            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
      	            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
      	            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
      	          'horizontal', 'date', 'time', 'spechars', '|',
      	            'preview'
      	        ]
      	    ],
      	    autoHeightEnabled: false,
      	    autoFloatEnabled: true,
      		//是否启用元素路径，默认是显示
            elementPathEnabled : false,
            wordCount:true,          //是否开启字数统计
            maximumWords:4000       //允许的最大字符数
      	});
    </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=repairInforAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${activityBasicInfor.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${activityBasicInfor.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	活动名称:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="activityName" onkeypress="notNull(this);" name="activityName" readonly="readonly" value="${activityBasicInfor.activityName}" />
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	活动地点:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="activityPlace" onkeypress="notNull(this);" name="activityPlace"  readonly="readonly" value="${activityBasicInfor.activityPlace}" />
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>报名开始时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activitySignStart" readOnly="readOnly" value="${activitySignStart }"
					name="activitySignStart"  class="inputxt" style="width: 260px;height:36px;"
				    onkeypress="notNull(this);"
					></input>
    		</td>
    	
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>报名结束时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activitySignEnd" readOnly="readOnly"
					name="activitySignEnd"  class="inputxt" style="width: 260px;height:36px;" value="${activitySignEnd }"
					onkeypress="notNull(this);"
					></input>
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>活动开始时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activityStartDate" readOnly="readOnly" value="${activityStartDate }"
					name="activityStartDate"  class="inputxt" style="width: 260px;height:36px;"
				    onkeypress="notNull(this);"
					></input>
    		</td>
    	
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>活动结束时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activityEndDate" readOnly="readOnly"
					name="activityEndDate"  class="inputxt" style="width: 260px;height:36px;" value="${activityEndDate }"
					onkeypress="notNull(this);"
					></input>
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	客户参与条件:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="activityAttendCondition" name="activityAttendCondition" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="一星级"<c:if test="${activityBasicInfor.activityAttendCondition=='一星级'}">selected</c:if>>一星级</option>
					<option value="二星级"<c:if test="${activityBasicInfor.activityAttendCondition=='二星级'}">selected</c:if>>二星级</option>
    		 		<option value="三星级"<c:if test="${activityBasicInfor.activityAttendCondition=='三星级'}">selected</c:if>>三星级</option>
					<option value="四星级"<c:if test="${activityBasicInfor.activityAttendCondition=='四星级'}">selected</c:if>>四星级</option>
    		 		<option value="五星级"<c:if test="${activityBasicInfor.activityAttendCondition=='五星级'}">selected</c:if>>五星级</option>
    		 	</select>
    		 </td>
    	
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	活动状态:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="activityState" name="activityState" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="00"<c:if test="${activityBasicInfor.activityState=='00'}">selected</c:if>>未开始</option>
					<option value="01"<c:if test="${activityBasicInfor.activityState=='01'}">selected</c:if>>进行中</option>
					<option value="02"<c:if test="${activityBasicInfor.activityState=='02'}">selected</c:if>>已结束</option>
    		 	</select>
    		 </td>
    	</tr>
    	<tr>
			<td align="right" width="15%" nowrap>
    			<label class="Validform_label">
    		 	 活动配图: 
    		   </label>
    		</td>
    		<td width="35%">
    			<img  id="url" src="${activityBasicInfor.URL}" width="200px" height="150px" readOnly="readOnly">
    		</td> 
    	</tr>
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	活动内容:
    		  </label>
    		</td>
    		<td class="value" width="75%" colspan="3">
	    		<textarea class="inputxt" id="activityContent" readonly="readonly" MaxLength="4000"
							onkeypress="notNull(this);" name="activityContent" cols="45" rows="5" 
							style="width: 750px; height: 160px;resize:none;">${activityBasicInfor.activityContent}</textarea>
			</td>
			</tr>
    <tr>
     <td align="center" colspan="8" style="padding-top: 200px;">
       <!--  <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span> -->
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
    	
   </table>
 </form>
</body>

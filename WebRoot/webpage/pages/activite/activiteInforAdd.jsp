<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
   <!-- 配置文件 -->
    <script type="text/javascript" src="ueditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="ueditor/ueditor.all.js"></script>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
function saveData(url) {
	var activityAttendCondition=$('#activityAttendCondition').val();
	var activityState=$('#activityState').val();
	var imagePath = $("#pictureURL").val();
    if (imagePath == "") {
        alert("请上传图片");
        return false;
    }
	if(activityAttendCondition==''){
		tip("请选择客户参与条件!");
		return;
	}
	
	if(activityState==''){
		tip("请选择活动状态!");
		return;
	}
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#saveBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}


function uploadPic(){
   	var imagePath = $("#pic").val();
       if (imagePath == "") {
    	   alert("请上传图片");
           return false;
       }
       var strExtension = imagePath.substr(imagePath.lastIndexOf('.') + 1);
       if (strExtension != 'jpg' && strExtension != 'JPG' 
    	   && strExtension != 'gif' && strExtension != 'GIF'
    	   && strExtension != 'jpeg' && strExtension != 'JPEG'
           && strExtension != 'png'  && strExtension != 'PNG'
           && strExtension != 'bmp' && strExtension != 'BMP') {
    	   alert("请上传jpg/gif/png/bmp/jpeg类型的图片");
           return false;
       }
       // 上传设置  
       var options = {  
               // 规定把请求发送到那个URL  
               url: '${ctx}/activiteController.do?uploadPic=key',  
               // 请求方式  
               type: "post",  
               // 服务器响应的数据类型  
               dataType: "json",  
               // 请求成功时执行的回调函数  
               success: function(data, status, xhr) {  
                   // 图片显示地址  
                   $("#url").attr("src", data.path); 
                   document.getElementById("pictureURL").value = data.path1;
                   //alert($("#remark1").val());
               }  
       };  
       $("#myform").ajaxSubmit(options);  
   }  
</script>
<!-- 实例化编辑器 -->
    <script type="text/javascript">
      	var ue = UE.getEditor('activityContent', {
      	    toolbars: [
      	        //['fullscreen', 'source', 'undo', 'redo', 'bold']
      	        ['undo', 'redo', '|',
      	            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', /*'autotypeset', 'blockquote',*/ 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
      	            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
      	            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
      	            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
      	          'horizontal', 'date', 'time', 'spechars', '|',
      	            'preview'
      	        ]
      	    ],
      	    autoHeightEnabled: false,
      	    autoFloatEnabled: true,
      		//是否启用元素路径，默认是显示
            elementPathEnabled : false,
            wordCount:true,          //是否开启字数统计
            maximumWords:4000       //允许的最大字符数
      	});
    </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=activityInforAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" enctype="multipart/form-data" method="post" >
  	<c:if test="${activityBasicInfor.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${activityBasicInfor.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	活动名称:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="activityName" onkeypress="notNull(this);" name="activityName" value="${activityBasicInfor.activityName}" datatype="*2-20" errormsg="请输入2-20位内的字符！"  nullmsg="请输入活动名称！"/><span style="color: red;">*</span>
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	活动地点:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="activityPlace" onkeypress="notNull(this);" name="activityPlace"  value="${activityBasicInfor.activityPlace}" datatype="*2-40" errormsg="请输入2-40位内的字符！"  nullmsg="请输入活动地点！" /><span style="color: red;">*</span>
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>报名开始时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activitySignStart" readOnly="readOnly" value="${activitySignStart }"
					name="activitySignStart"  class="Wdate" style="width: 260px;height:36px;"
					datatype="*1-50" nullmsg="请输入报名开始时间！" errormsg="请输入正确的报名开始时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'activitySignEnd\')}'})"></input><span style="color: red;">*</span>
    		</td>
    	
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>报名结束时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activitySignEnd" readOnly="readOnly"
					name="activitySignEnd"  class="Wdate" style="width: 260px;height:36px;" value="${activitySignEnd }"
					datatype="*1-50" nullmsg="请输入报名结束时间！" errormsg="请输入正确的报名结束时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'activitySignStart\')||\'%y-%M-%d\'}'})"></input><span style="color: red;">*</span>
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>活动开始时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activityStartDate" readOnly="readOnly" value="${activityStartDate }"
					name="activityStartDate"  class="Wdate" style="width: 260px;height:36px;"
					datatype="*1-50" nullmsg="请输入活动开始时间！" errormsg="请输入正确的活动开始时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'activitySignEnd\')}',maxDate:'#F{$dp.$D(\'activityEndDate\')}'})"></input><span style="color: red;">*</span>
    		</td>
    	
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>活动结束时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="activityEndDate" readOnly="readOnly"
					name="activityEndDate"  class="Wdate" style="width: 260px;height:36px;" value="${activityEndDate }"
					datatype="*1-50" nullmsg="请输入活动结束时间！" errormsg="请输入正确的活动结束时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'activityStartDate\')||\'%y-%M-%d\'}'})"></input><span style="color: red;">*</span>
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	客户参与条件:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="activityAttendCondition" name="activityAttendCondition" onkeypress="notNull(this);">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="一星级"<c:if test="${activityBasicInfor.activityAttendCondition=='一星级'}">selected</c:if>>一星级</option>
					<option value="二星级"<c:if test="${activityBasicInfor.activityAttendCondition=='二星级'}">selected</c:if>>二星级</option>
    		 		<option value="三星级"<c:if test="${activityBasicInfor.activityAttendCondition=='三星级'}">selected</c:if>>三星级</option>
					<option value="四星级"<c:if test="${activityBasicInfor.activityAttendCondition=='四星级'}">selected</c:if>>四星级</option>
    		 		<option value="五星级"<c:if test="${activityBasicInfor.activityAttendCondition=='五星级'}">selected</c:if>>五星级</option>
    		 	</select><span style="color: red;">*</span>
    		 </td>
    	
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	活动状态:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="activityState" name="activityState" onkeypress="notNull(this);">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="00"<c:if test="${activityBasicInfor.activityState=='00'}">selected</c:if>>未开始</option>
					<option value="01"<c:if test="${activityBasicInfor.activityState=='01'}">selected</c:if>>进行中</option>
					<option value="02"<c:if test="${activityBasicInfor.activityState=='02'}">selected</c:if>>已结束</option>
    		 	</select><span style="color: red;">*</span>
    		 </td>
    	</tr>
			<tr>
			<td align="right" width="15%" nowrap>
    			<label class="Validform_label">
    		 	 活动配图: 
    		   </label>
    		</td>
			<td width="35%">
				<img  id="url" src="${activityBasicInfor.URL}" width="200px" height="150px">
	            <input type="file" name="pic" id="pic" style="width: 150px;">
				<input type="button" value="上传图片" onclick="uploadPic();"/><span style="color: red;">*</span>
				<input id="pictureURL" name="pictureURL" type="hidden" value="${activityBasicInfor.pictureURL}" />
			</td> 
    	</tr>
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	活动内容:
    		  </label>
    		</td>
    		<td class="value" width="75%" colspan="3" >
	    		<textarea class="inputxt" id="activityContent" onkeypress="notNull(this);"  name="activityContent" cols="45" rows="5" 
							style="width: 750px; height: 160px;resize:none;">${activityBasicInfor.activityContent}</textarea>
				<span style="color: red;">*</span>
			</td> 
			</tr>
     <tr>
     <td align="center" colspan="8" style="padding-top: 176px;">
        <span class="orangebtnhover">
        <a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData(' ${ctx}/activiteController.do?saveUpdate=key')" >保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr> 
   </table>
 </form>
</body>

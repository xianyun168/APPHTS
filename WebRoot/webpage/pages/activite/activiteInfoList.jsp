<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="ActivityInfoList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
			<div>
				<label>活动名称：</label> 
				<input type="text" id="activityName" name="activityName" />
			</div>

			<div style="width: 66.66%;">
				<label style="width: 20%; float: left;">活动起止时间：</label> 
				<input style="width: 27.5%; float: left; margin-left: 1%;" type="text"
					id="activityStartDate"  name="activityStartDate" class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"></input>

				<label style="width: 2%; float: left; margin-left: 1%; text-align: center;">-</label>
				<input style="width: 27.5%; float: left; margin-left: 1%;"
					type="text" id="activityEndDate" name="activityEndDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'activityStartDate\')||\'%y-%M-%d\'}'})"></input>
			</div>

			<div> 
				<label class="jiekouname">活动状态：</label>
		   		<select id ="flag" name="type">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="00"<c:if test="${activityBasicInfor.activityState=='未开始'}">selected</c:if>>未开始</option>
					<option value="01"<c:if test="${activityBasicInfor.activityState=='进行中'}">selected</c:if>>进行中</option>
					<option value="02"<c:if test="${activityBasicInfor.activityState=='已结束'}">selected</c:if>>已结束</option>
    		 	</select> 
			</div>
	</div>
	
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<span class="bluebtnhover"><a onclick="add_win('活动信息增加',' ${ctx}/activiteController.do?add=key','ActivityInfoList',1000,650)" id="add" plain="true" class="formbtn bluebtn">新增</a></span> 
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#ActivityInfoList').datagrid({
			idField : 'pkId',
			title : '活动信息管理',
		    url : ' ${ctx}/activiteController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			/* <!--查询参数 --> */
			queryParams:{
				'activityName': $('#activityName').val(),
				'activityState': $('#flag').val(),
				'activityStartDate': $('#activityStartDate').val(),
			    'activityEndDate': $('#activityEndDate').val() 
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'activityName',title:'活动名称',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'remark1',title:'活动开始时间',
						/* formatter:function(val,rec){
			        		if(val != null && val != ''){
			        			return formattimed(val);
			        		}
			      		}, */
						width:fixWidth(0.15),align:'center',sortable:false},
					{field:'remark2',title:'活动结束时间',
							/* formatter:function(val,rec){
				        		if(val != null && val != ''){
				        			return formattimed(val);
				        		}
				      		}, */
							width:fixWidth(0.15),align:'center',sortable:false},
					{field:'activityState',title:'活动状态',   width:fixWidth(0.15),align:'center',sortable:false},
					{field : 'opt',title : '操作',width:fixWidth(0.2),align:'center',sortable:false,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=update('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "编辑</a>]";
							href += "[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "详情</a>]";
							href += "[<a href='#' onclick=dele('activiteController.do?delete=key','ActivityInfoList','"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "删除</a>]";
							href += "[<a href='#' onclick=sign('"+rec.pkId+"','"+rec.activityState+"') style='color:#3f99b1;'>";
							href += "报名信息</a>]";
									var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
										var oper = operCode.split(', ');
										var i = oper.length;
										if(oper !=""){
										  for(var j=0;j<oper.length;j++){
											
											  if(oper[j] == "modify"){
													 href=href.replace("[<a href='#' onclick=update('"+rec.pkId+"') style='color:#3f99b1;'>编辑</a>]","");
													}
											 if(oper[j] == "detail"){
												 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>详情</a>]","");
												}
											 if(oper[j] == "delete"){
												 href=href.replace("[<a href='#' onclick=dele('activiteController.do?delete=key','ActivityInfoList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
												}  
										
											}
										}
							return href;
					}}
				    ]],
			onLoadSuccess : function(data) {
			    $("#ActivityInfoList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清除所选的行
				$("#ActivityInfoList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'certificateInfoList';
			}
		});
		$("#ActivityInfoList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#ActivityInfoList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});

function reset(){
	$('#companyCode').val("");
	$('#activityName').val("");
	$('#flag').val("");
	$('#activityStartDate').val("");
    $('#activityEndDate').val("") 
}

//编辑页面
function update(pkId){
	add_win('编辑', '${ctx}/activiteController.do?modify=key&pkId='+ pkId,'ActivityInfoList',1000,650);
}

//详情页面
function detail(pkId){
	add_win('详情','${ctx}/activiteController.do?specific=key&pkId='+pkId,'ActivityInfoList',1000,650);
	
}	

//报名人员页面
function sign(pkId,activityState){
	add_win('报名信息','${ctx}/activiteController.do?signQuery=key&pkIds='+pkId+'&activityState='+encodeURI(encodeURI(activityState)),'ActivityInfoList',1000,400);
	
}	

//删除
function dele(url,id,pkId) {
//var rowsData = $('#'+id).datagrid('getSelections');
url += '&pkId='+pkId;
window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    if (r){
        $.post(url,function(result){
            if (result.success){
            	if(result.obj =='alert'){
            		tip(result.msg);
            	}else{
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
            	}
            } else {
                window.top.$.messager.show({// show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
        },'json');
    }
});
}

function reloadTable() {
	try {
		$('#' + MailInfoList).datagrid('reload');
		$('#' + MailInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + MailInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#ActivityInfoList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

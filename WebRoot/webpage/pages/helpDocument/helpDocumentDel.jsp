<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
   <!-- 配置文件 -->
    <script type="text/javascript" src="ueditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="ueditor/ueditor.all.js"></script>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
	  init();
	});

</script>
<!-- 实例化编辑器 -->
    <script type="text/javascript">
      	var ue = UE.getEditor('content', {
      	    toolbars: [
      	    	['undo', 'redo', '|',
      	            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', /*'autotypeset', 'blockquote',*/ 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
      	            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
      	            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
      	            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
      	          'horizontal', 'date', 'time', 'spechars', '|',
      	            'preview'
      	        ]
      	    ],
      	    autoHeightEnabled: false,
      	    autoFloatEnabled: true,
      		//是否启用元素路径，默认是显示
            elementPathEnabled : false,
            wordCount:true,          //是否开启字数统计
            maximumWords:1000       //允许的最大字符数
      	});
    </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${helpDocument.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${helpDocument.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
  		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>标题:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${helpDocument.title}" 
    		  style="width:260px;height:35px;" readonly="readonly" />
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>创建人:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="publisher" id="publisher" onkeypress="notNull(this);" name="publisher" value="${helpDocument.publisher}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	</tr>
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>访问次数:</span></label>
    	  </td>
    	 <td class="value" width="35%" >
    		  <input class="counts" id="counts" onkeypress="notNull(this);" name="counts" value="${helpDocument.counts}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>创建时间:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="createdDate" id="createdDate" onkeypress="notNull(this);" name="createdDate" value="${createdDate}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap >
    		  <label class="Validform_label"><span>内容:</span></label>
    	  </td>
    	  <td class="value" width="75%" colspan="3">
    	      <textarea rows="4" cols="8" style="width:630px;height: 200px" onkeypress="notNull(this);" name="content" class="content" id="content" 
    	       readonly="readonly">${helpDocument.content}</textarea>
    	  </td>
    	</tr>
     	<tr>
     		<td align="center" colspan="8">
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
</body> 
  
  
  
  

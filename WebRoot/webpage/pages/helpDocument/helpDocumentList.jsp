<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>KFCRM</title>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet" />
<style type="text/css">
	.formbtn{
		display: inline-block;
		float: auto;
	}
	.bluebtnhover,.orangebtnhover{
		display: inline-block;
	}
	
</style>

</head>
<body>
	<table width="100%" id="AtomicAuditList" toolbar="#demoListtb"></table>
	<div id="demoListtb" style="padding:3px; height: auto">
		<!-- 用于权限判断 start -->
		 <input id="pkId" name="pkId" type="hidden" value=${knowledgeBase.pkId}/>
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
		
		<div class="jiekouform">
			<div>
				<label class="jiekouname">标题：</label>
				<input type="text" id="title"  name="title"  />
			</div>
			<div style="width: 66.66%;">
				<label style="width: 20%; float: left;">时间区间：</label> <input
					style="width: 27.5%; float: left; margin-left: 1%;" type="text"
					id="startDate"  name="startDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'%y-%M-%d\'}'})"></input>

				<label
					style="width: 2%; float: left; margin-left: 1%; text-align: center;">-</label>
				<input style="width: 27.5%; float: left; margin-left: 1%;"
					type="text" id="endDate" name="endDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'%y-%M-%d'})"></input>
			</div>
		</div>	
		<div  style="clear:both;padding-top:20px;">
			<div class="leftbtntool"  style="margin:auto;text-align: center;">			
				<span class="bluebtnhover"><a onclick="add_win('常见问题新增',' ${ctx}/helpDocumentController.do?helpDocumentAdd=key','AtomicAuditList',710,450)" id="add" plain="true" class="formbtn bluebtn">新增</a></span> 
				<%-- <span class="bluebtnhover"><a href="javascript:void(0)" class="formbtn bluebtn" plain="true" onclick="update('帮助文档编辑',' ${ctx}/helpDocumentController.do?modify=key','AtomicAuditList',710,330)" id="modify">编辑</a></span>  --%>
				
				<span class="orangebtnhover" ><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
				<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
				
			</div>
		</div>
		
		<div style="background-color:white;"></div>
	</div>
	 <script type="text/javascript">
		function query() {
			$('#AtomicAuditList').datagrid({
				idField : 'pkId',
				title : '常见问题管理',
				url : ' ${ctx}/helpDocumentController.do?getHelpDocumentList=key',
				fit : true,
				//loadMsg : '数据加载中...',
				pageSize : 10,
				pagination : true,
				pageList : [ 10, 20, 30 ],
				//sortName: 'id',
				sortOrder : 'asc',
				striped : true,
				rownumbers : true,
				singleSelect : true,
				fitColumns : true,
				remoteSort : false,
				queryParams : {
				'startDate': $('#startDate').val(),
				'endDate': $('#endDate').val(),
				'title': $('#title').val()
				},
				columns : [ [ {
					field : 'ck',
					checkbox : true
				}, {
					field : 'pkId',
					title : '主键',
					width : fixWidth(0.11),
					align : 'center',
					sortable : true,
					hidden : true
				}, {
						field : 'title',
						title : '标题',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					}, {
						field : 'createdDate',
						title : '创建时间',
						formatter : function(val, rec) {
							if (val != null && val != '') {
								return formattimed(val);
							}
						},
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					}, 
					 {
						field : 'publisher',
						title : '创建人',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					},{
						field : 'counts',
						title : '查看次数',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					},
					{field : 'opt',title : '操作',width:fixWidth(0.11),align:'center',sortable:true,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=update('常见问题编辑','helpDocumentController.do?modify=key','AtomicAuditList','"+rec.pkId+"',710,450) style='color:#3f99b1;'>";
							href += "编辑</a>]";
							href += "[<a href='#' onclick=dele('helpDocumentController.do?delete=key','AtomicAuditList','"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "删除</a>]";
							href += "[<a href='#' onclick=details('常见问题详情','helpDocumentController.do?details=key','AtomicAuditList','"+rec.pkId+"',710,450) style='color:#3f99b1;'>";
							href += "详情</a>]";
							var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
							var oper = operCode.split(', ');
							var i = oper.length;
							if(oper !=""){
							  for(var j=0;j<oper.length;j++){
								
								  if(oper[j] == "modify"){
										 href=href.replace("[<a href='#' onclick=update('常见问题编辑','helpDocumentController.do?modify=key','AtomicAuditList','"+rec.pkId+"',710,450) style='color:#3f99b1;'>编辑</a>]","");
										}
								 if(oper[j] == "detail"){
									 href=href.replace("[<a href='#' onclick=details('常见问题详情','helpDocumentController.do?details=key','AtomicAuditList','"+rec.pkId+"',710,450) style='color:#3f99b1;'>详情</a>]","");
									}
								 if(oper[j] == "delete"){
									 href=href.replace("[<a href='#' onclick=dele('helpDocumentController.do?delete=key','AtomicAuditList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
									}  
							
								}
							}
							return href;
						}} ] ],
					onLoadSuccess : function(data) {
						$("#AtomicAuditList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
						//清楚所选的行
						$("#AtomicAuditList").datagrid("clearSelections");
					},
					onLoadError : function(){
						history.go(0);
					},
					//rowIndex:行索引 rowData：行数据
					onClickRow : function(rowIndex, rowData) {
						rowid = rowData.id;
						gridname = 'AtomicAuditList';
					}
				});
				$("#AtomicAuditList").datagrid('getPager').pagination({
					beforePageText : '',
					afterPageText : '/{pages}',
					displayMsg : '显示{from}-{to}共{total}条',
					showPageList : true,
					showRefresh : true
				});

				$("#AtomicAuditList").datagrid('getPager').pagination({
					onBeforeRefresh : function(pageNumber, pageSize) {
						$(this).pagination('loading');
						$(this).pagination('loaded');
					}
				});
			};
			$(function() {
				//查询
				query();
				noauto();

			});
			function reset() {
			$('#title').val('');
			$('#startDate').val('');
			$('#endDate').val('');
			}
			function update(title, url, id,pkId, width, height) {
				url += '&pkId=' + pkId;
				width = width ? width : 700;
				height = height ? height : 450;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + url,
					lock : true,
					width : width,
					height : height,
					title : title,
					opacity : 0.3,
					cache : false
				}); 
			}
			function reloadTable() {
				try {
					$('#' + AtomicAuditList).datagrid('reload');
					$('#' + AtomicAuditList).treegrid('reload');
				} catch (ex) {
				}
			}

			function getdemoListSelected(field) {
				return getSelected(field);
			}

			//选中单行
			function getSelected(field) {
				var row = $('#' + AtomicAuditList).datagrid('getSelected');
				if (row != null) {
					value = row[field];
				} else {
					value = '';
				}
				return value;
			}

			function getUserListSelections(field) {
				var ids = [];
				var rows = $('#AtomicAuditList').datagrid('getSelections');
				for ( var i = 0; i < rows.length; i++) {
					ids.push(rows[i][field]);
				}
				ids.join(',');
				return ids
			};

			//页面初始化时，设置按钮的权限
			function noauto() {
				//取得对应的权限的值(后台返回的是没有的权限的值)
				var operCode = $("#noauto_operationCodes").val().replace("[", "")
						.replace("]", "");
				var oper = operCode.split(', ');
				var i = oper.length;
				$(oper).each(function(i) {
					$('#' + oper[i]).hide();
				})
			}
			/**
			 * 打开一个增加页面
			 */
			function add_win(title, addurl, gname, width, height) {
				gridname = gname;
				width = width ? width : 700;
				height = height ? height : 450;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + addurl,
					//lock : true,
					resizable:false,
					maximizable:false,
					width : width,
					height : height,
					title : title,
					opacity : 0.3
					//cache : false,
					//modal: true
				});
			}
			//删除
			function dele(url, id, pkId) {
				var rowsData = $('#' + id).datagrid('getSelections');
				url += '&pkId=' + pkId;
				window.top.$.messager.confirm('Confirm', '你确定要删除吗', function(r) {
					if (r) {
						$.post(url, function(result) {
							if (result.success) {
								if (result.obj == 'alert') {
									tip(result.msg);
								} else {
									$('#' + id).datagrid('reload'); // reload the user data
									$('#' + id).treegrid('reload'); // reload the user data
								}
							} else {
								window.top.$.messager.show({// show error message
									title : 'Error',
									msg : result.errorMsg
								});
							}
						}, 'json');
					}
				});
			}
			//详情列
			function details( title,url, id,rec,width,height) {
				url += '&pkId=' + rec;
				width = width ? width : 720;
				height = height ? height : 450;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + url,
					lock : true,
					width : width,
					height : height,
					title : title,
					opacity : 0.3,
					cache : false,
					modal: true
				});
			}
			
	</script> 
</body>
</html>
<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <!-- 配置文件 -->
    <script type="text/javascript" src="ueditor/ueditor.config.js"></script>
    <!-- 编辑器源码文件 -->
    <script type="text/javascript" src="ueditor/ueditor.all.js"></script>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
	  init();
	});

	function checkAll(){
		var title=$('#title').val();
		var content=$('#content').val();
		var pkId = $('#pkId').val();
		if(pkId==""||pkId==null){
	 		$.ajax({
             	type: "post",
             	url: "helpDocumentController.do?checkCode=key",
             	data: {"title":title},
             	dataType: "json",
             	success: function(data){
                     if(data.obj=="ok"){
                    	 var url ="${ctx}/helpDocumentController.do?saveUpdate=key";
                 		var form = $("#myform").Validform();
                 		$('#myform').form(
                 				'submit',
                 				{
                 					url : url,
                 					onSubmit : function() {
                 						var form = $("#myform").Validform();
                 						return form.check();
                 					},
                 					success : function(data) {
                 						var windowapi = frameElement.api, W = windowapi.opener;
                 						var d;
                 						try {
                 							d = $.parseJSON(data);
                 							if (d.success == true) {
                 								window.top.$.messager.progress('close');
                 								windowapi.close();
                 								W.tip(d.msg);
                 								W.query();
                 							} else {
                 								window.top.$.messager.progress('close');
                 								$("#saveBtn").removeAttr("disabled");
                 								window.top.$.messager.alert('提示', d.msg);
                 								return;
                 							}
                 						} catch (ex) {
                 							var emsg = data.substring(data.indexOf('错误描述'),
                 									data.indexOf('错误信息'));
                 							window.top.$.messager.progress('close');
                 							$("#saveBtn").removeAttr("disabled");
                 							window.top.$.messager.alert('错误', emsg);
                 							return;
                 						}
                 					}
                 				});
                        //saveData('${ctx}/helpDocumentController.do?saveUpdate=key');
                     }else{
                        tip(data.msg);
                        return;
                     }
                 }
         	});
        }else{
        	$.ajax({
             	type: "post",
             	url: "helpDocumentController.do?checkCode=key",
             	data: {"title":title,"pkId":pkId},
             	dataType: "json",
             	success: function(data){
                     if(data.obj=="ok"){
                    	 var url ="${ctx}/helpDocumentController.do?saveUpdate=key";
                  		var form = $("#myform").Validform();
                  		$('#myform').form(
                  				'submit',
                  				{
                  					url : url,
                  					onSubmit : function() {
                  						var form = $("#myform").Validform();
                  						return form.check();
                  					},
                  					success : function(data) {
                  						var windowapi = frameElement.api, W = windowapi.opener;
                  						var d;
                  						try {
                  							d = $.parseJSON(data);
                  							if (d.success == true) {
                  								window.top.$.messager.progress('close');
                  								windowapi.close();
                  								W.tip(d.msg);
                  								W.query();
                  							} else {
                  								window.top.$.messager.progress('close');
                  								$("#saveBtn").removeAttr("disabled");
                  								window.top.$.messager.alert('提示', d.msg);
                  								return;
                  							}
                  						} catch (ex) {
                  							var emsg = data.substring(data.indexOf('错误描述'),
                  									data.indexOf('错误信息'));
                  							window.top.$.messager.progress('close');
                  							$("#saveBtn").removeAttr("disabled");
                  							window.top.$.messager.alert('错误', emsg);
                  							return;
                  						}
                  					}
                  				});
                        //saveData('${ctx}/helpDocumentController.do?saveUpdate=key');
                     }else{
                        tip(data.msg);
                        return;
                     }
                 }
         	});
        }  
	} 
</script>
<!-- 实例化编辑器 -->
    <script type="text/javascript">
      	var ue = UE.getEditor('content', {
      	    toolbars: [
      	    	['undo', 'redo', '|',
      	            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', /*'autotypeset', 'blockquote',*/ 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
      	            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
      	            'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
      	            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
      	          'horizontal', 'date', 'time', 'spechars', '|',
      	            'preview'
      	        ]
      	    ],
      	    autoHeightEnabled: false,
      	    autoFloatEnabled: true,
      		//是否启用元素路径，默认是显示
            elementPathEnabled : false,
            wordCount:true,          //是否开启字数统计
            maximumWords:1000       //允许的最大字符数
      	});
    </script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" enctype="multipart/form-data" method="post" >
  	<c:if test="${helpDocument.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${helpDocument.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>标题:</span></label>
    	  </td>
    	  <td class="value" width="75%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${helpDocument.title}" 
    		  style="width:460px;height:35px;" datatype="*4-100" errormsg="请输入4-100位内的字符"  nullmsg="请输入4-100位标题！"/><span style="color:red">*</span>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>内容:</span></label>
    	  </td>
    	  <td class="value" width="75%" >
    	      <textarea rows="4" cols="8" style="width:460px;height: 200px;" onkeypress="notNull(this);" name="content" class="content" id="content" 
    	       datatype="*4-4000" errormsg="请输入4-4000位内的字符"  nullmsg="请输入4-4000位内容！">${helpDocument.content}</textarea>
    	      <span style="color:red">*</span>
    	  </td>
    	</tr>
     	<tr>
     		<td align="center" colspan="8">
        		<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
</body> 
  
  
  
  

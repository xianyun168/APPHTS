<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<HTML lang="zh-CN">
<head>
<TITLE>收货地址列表</TITLE>	 

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<meta name="format-detection" content="email=no" />
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="X-UA-Compatible" content="IE=10.000" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.png"/>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css"  />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css" />
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css" />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/iconfont.css" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></SCRIPT>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>

<LINK rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/mainAddress.css">	 
<style type="text/css">
    /*清除默认样式*/
    html, body, div, span, applet, object, iframe,
    h1, h2, h3, h4, h5, h6, p, blockquote, pre,
    a, abbr, acronym, address, big, cite, code,
    del, dfn, em, font, img, ins, kbd, q, s, samp,
    small, strike, strong, sub, sup, tt, var,
    b, u, i, center,
    dl, dt, dd, ol, ul, li,
    fieldset, form, label, legend,
    table, caption, tbody, tfoot, thead, tr, th, td,p {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 100%;
    vertical-align: baseline;
    background: transparent;
    }
    .cur {
    font-size: 14px;
    float: right;
    padding-right: 20px;
  	width: 106px;
  	text-align: right;
    }
</style>
</head> 
<BODY  style="background:#efefef;">
	<div>
	<!-- 隐藏的用户id -->
	<input type="hidden" value="${userId}" id="userId" name="userId">
		<div class="new-header bg_common" >
		 <A class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></A>							 
		 <h2 style=" border-bottom:1px #dedede solid;">地址列表</h2>
		</div>
	</div>
	<div id="counts">
	</div>

<div class="add" style="position: fixed;bottom: 14px;width: 100%;">
	<a href="${ctx}/goddsAddressController.do?goddsAddressAdd=key&userId=${userId}" class="btn_common">添加新地址</a>
</div>
<!--主体-->

	<!--底部浮层begin-->
	

<div style="margin-bottom: 80px;"></div>
<!--底部浮层begin-->
<script type="text/javascript">
	$(function(){
		query();
		cartNum();
		//query1();
	})
	/* function query1(){
		
		$("#address")
	}; */
	/* //判断导航选中
	$(function(){
		query();
		cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	}); */
	//异步请求列表显示
	function query(){
		var userId = $("#userId").val();
		$.ajax({
			url:"goddsAddressController.do?getGoddsAddressList=key",
		    type:"POST",
		    datatype:"JOSN",
		    data: {"userId":userId},
		    success: function (data) {
		    	var dataObj=eval(data);//处理，将json字符串转换为对象   
		    	var count = "";
                for(var i=0;i<dataObj.length;i++){        
                    count += "<div class='address'>";
                    count += "<div class='add_row'>";
            			
            		count += "<div class='down'>";
            		if(dataObj[i].addressState=="1"){
            			count += "<span class='cur text_color_main ' style='padding-right: 0px;' onclick='javascript:changeAddressState("+dataObj[i].pkId+","+dataObj[i].addressState+");'>设为默认地址</span>";		
            		}else{
            			count += "<span class='opt text_color_main ' style='padding-right: 0px;' onclick='javascript:changeAddressState("+dataObj[i].pkId+","+dataObj[i].addressState+");'>设为默认地址</span>";		
            		}
            		count += "<span style='padding-right:10px;width: auto;float: right;'><a style='padding: 0;' href='goddsAddressController.do?modify=key&pkId="+dataObj[i].pkId+"'><i class='iconfont icon-bianji'></i>&nbsp;&nbsp;编辑</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style='padding: 0px 0px;' href='javascript:deleteAddress("+dataObj[i].pkId+","+dataObj[i].addressState+");'><i class='iconfont icon-shanchu'></i>&nbsp;&nbsp;删除</a></span></div>";
            		count += "<div class='up'>";
            		count += "<span class='name'>"+dataObj[i].shopName+"</span>";
            		count += "<span class='phone text_color_main'>"+dataObj[i].shopPhone+"</span>";
            		//count += "<input class='address_inputtt'  id='address' type='hidden' name='address' value='"+dataObj[i].addressState+"'/>";
            		
            		count += "</div><div style='' class='mid'>"+dataObj[i].province+","+dataObj[i].city+","+dataObj[i].area+","+dataObj[i].shopAddress+"</div>";
            		count += "</div></div>";
                }
                $("#counts").html(count);
		    }
		});
	}
 
	function cartNum(){
		var userId = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryByCarInfo=key",
		    type:"POST",
		    async:"false",
		    datatype:"JOSN",
		    data:{userId:userId},
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.obj;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
    					width:"22px",
    					height:"22px",
    					padding:"10px"
    				});
		    		$("#cartNum").animate({
    					width:"20px",
    					height:"20px",
    					padding:"0"
    				});
		    	}
		    }
		});
	} 
	//删除地址
	function deleteAddress(pkId,addressState){
		var userId = $("#userId").val();
		if(addressState!=null&&addressState=="1"){
			sj_alert("默认地址不能删除！");
		}else if(addressState!=null&&addressState=="0"){
			sj_confirm('您确定要删除该地址吗？', null, null, function(){
				$.ajax({
					type: "POST",
					dataType: "JSON",
					url:"goddsAddressController.do?delete=key",
					data:{pkId:pkId},//  data:$("#form2").serialize(),//得到表单内的所有数据
					success: function(data){
						if(data.obj=="ok"){
							window.location.href="goddsAddressController.do?goddsAddressList=key&userId="+userId;
						}else{
							sj_alert("删除失败，请重试！");
						}
					}
				});
			});
		}
	}
	//设置默认地址
	function changeAddressState(pkId,addressState){
		//alert(pkId);
		if(addressState!=null&&addressState==0){
			$.ajax({
				type: "POST",
				dataType: "JSON",
				url:"goddsAddressController.do?details=key",
				data:{pkId:pkId},//得到表单内的所有数据
				success: function(data){
					if(data.obj=="ok"){
						window.location.href="goddsAddressController.do?goddsAddressList=key&userId="+$("#userId").val();
					}else{
						sj_alert("设置默认地址失败，请重试！");
					}
				}
			});
		}else if(addressState!=null&&addressState==1){
			sj_alert("该地址已经设置为默认地址了！");
		}
		
	}
</script>
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.scrollUp.js" ></script>
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/scrollup.css" />
<script type="text/javascript">
    /*回到顶部JS*/
    $.scrollUp();
</script>
<!--回到顶部end-->
</BODY>
</HTML>
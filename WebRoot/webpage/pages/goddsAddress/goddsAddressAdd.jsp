<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<HTML lang="zh-CN">
<HEAD>
<TITLE>地址信息</TITLE>	 

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="format-detection" content="telephone=no" />
<meta name="format-detection" content="email=no" />
<meta name="GENERATOR" content="MSHTML 10.00.9200.16750" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<meta http-equiv="X-UA-Compatible" content="IE=10.000" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<script type="text/javascript" src="${ctx}/common/js/jquery/jquery-1.7.2.min.js"></script>

<link rel="shopjsp icon" href="${ctx}/common/js/jquery/imgs_init/sj.png"/>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/base2013.css"  />
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/style.css" />
<script type="text/javascript" src="${ctx}/common/js/jquery/19416443.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/alertPopShow.js"></script>
<link type="text/css" rel="stylesheet" href="${ctx}/common/js/jquery/css/common.css" />



<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/main.js"></SCRIPT>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.metadata.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMessage.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate_customerMethod.js"></script>
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.validate.CardIdNo.js"></script>
<style>
	.disno{
		display:none;
	}
	
</style>


<script type="text/javascript">
//添加地址
function toSubmit(){
	$("#msg").html("");
	var yi = $("#yiji").val();
	if(yi == ''){
		sj_alert("省份为空了！！！");
		return;
	}
	var er = $("#erji").val();
	if(er == ''){
		sj_alert("城市为空了！！！");
		return;
	}
	var san = $("#sanji").val();
	if(san ==''){
		sj_alert("区县为空了！！！");
		return;
	}
	var str = "";
	var address = $("#shopAddress").val();
	if($.trim(address)==""){
		str += "街道不能为空！";
	}
	/* var postCode = $("#postcode").val();
	var re= /^[1-9][0-9]{5}$/;
	if($.trim(postCode)==""||!re.test($.trim(postCode))){
		str += "邮编不能为空或格式错误！";
	} */
	var consignee = $("#shopName").val();
	if($.trim(consignee)==""){
		str += "收货人姓名不能为空！";
	}
	var mobilePhone = $("#shopPhone").val();
	var re1 = /^[1][0-9][0-9]{9}$/;
	if($.trim(mobilePhone)==""||!re1.test($.trim(mobilePhone))){
		str += "手机不能为空或号码错误！";
	}
/* 	var email = $("#email").val();
	var re2 = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
	if($.trim(email)==""||!re2.test($.trim(email))){
		str += "邮箱不能为空或格式错误！";
	}*/
	if(str==""){
		$("#form1").submit();
	}else{
		$("#msg").html(str);
	} 
}
$(function(){
	cartNum();
	//alert(list.size);
	var isSend=$("#addressState").val();
	if(isSend==1){
		//$("#checkbox").attr("checked",true);
		$("#address").hide();
	}
});

//操作一级区域
var optionYiji=function(v){
	$.post("${ctx}/goddsAddressController.do?getCityList=key",{provinceid:v},function(data){
		if(data){
			var dataObj=eval(data);
			if(dataObj.length>0){
				var erjiHtml="<option value=''>请选择</option>";
				for(var i=0;i<dataObj.length;i++){
					erjiHtml+="<option value=\""+dataObj[i].cityid+"\">"+dataObj[i].city+"</option>";
				}
				$("#erji").html(erjiHtml);
			}
		}
	},'json');
};

//操作二级区域
var optionErji=function(v){
	$.post("${ctx}/goddsAddressController.do?getAreaList=key",{cityid:v},function(data){
		if(data){
			var dataObj=eval(data);
			//alert(dataObj.list.length);
			if(dataObj.length>0){
				var sanjiHtml="<option value=''>请选择</option>";
				for(var i=0;i<dataObj.length;i++){
					sanjiHtml+="<option value='"+dataObj[i].areaid+"'>"+dataObj[i].area+"</option>";
				}
				$("#sanji").html(sanjiHtml);
			}
		}
	},'json');
}
</script>
</HEAD> 
<BODY>
	<div>
	
		<div class="new-header bg_common">
		 <A class="new-a-back" id="backUrl" href="javascript:history.back();"><span>返回</span></A>							 
		 <h2>地址详情</h2>
		</div> 
	</div>
<!--主体-->
<div class="main">
<form id="form1" method="post" action="${ctx}/goddsAddressController.do?saveUpdate=key">
  <!-- 隐藏的用户id -->
  <input type="hidden" value="${userId}" id="userId" name="userId">
  <input type="hidden" name="struts.token.name" value="token" />
<input type="hidden" name="token" value="9G4BCQSVNL43WZ8CZZBP9516MLUP1OOH" />
  <input type="hidden" name="custAddress.customerAcceptAddressId" value="" id="custAddress_customerAcceptAddressId"/>
  
  <div class="qbdd_first bor_bor_bottomm">
      <div style="width:100%;text-align:left;">
         <span style="width:70px; display:inline-block; height:33px; line-height:33px;">收货人：</span>
          <input class="address_inputtt"  id="shopName" type="text" name="shopName" value="${goddsAddress.shopName}"/>
      </div>
  </div>
  <div class="qbdd_first bor_bor_bottomm">
      <div style="width:100%;text-align:left;">
          <span style="width:70px; display:inline-block; height:33px; line-height:33px;">联系电话：</span>
           <input class="address_inputtt"  id="shopPhone" type="text" name="shopPhone" value="${goddsAddress.shopPhone}"/>
      </div>
  </div>
  
   <div class="qbdd_first bor_bor_bottomm">
      <div style=" width:100%;text-align:left;">
        <span style="width:70px; display:inline-block; height:33px; line-height:33px;">省份：</span>
        <select id="yiji" class="address_inputtt" onchange="optionYiji(this.value)" name="province">
       		<option value="" >请选择</option>
       		<c:forEach var = "province" items = "${list}" >
       			
       			<option value="${province.provinceid}" 
       				<c:if test="${province.provinceid eq goddsAddress.province}">selected = "selected"</c:if>>
       					${province.province}
       			</option>
       		</c:forEach>
       </select> 
      </div>
  </div>
  <div class="qbdd_first bor_bor_bottomm">
      <div style=" width:100%;text-align:left;">
            <span style="width:70px; display:inline-block; height:33px; line-height:33px;">城市：</span>
            <select id="erji" class="address_inputtt" onchange="optionErji(this.value)" name="city">
            	<c:forEach var = "city" items = "${list1}" >
       			<option value="${city.cityid}" 
       				<c:if test="${city.cityid eq goddsAddress.remark2}">selected = "selected"</c:if>>
       					${city.city}
       			</option>
       		</c:forEach>
       		</select>
      </div>
  </div>
  <div class="qbdd_first bor_bor_bottomm">
      <div style=" width:100%;text-align:left;">
        <span style="width:58px; display:inline-block; height:33px; line-height:33px;">区县：</span>
         <select id="sanji" class="address_inputtt" name="area">
         	<c:forEach var = "area" items = "${list2}" >
       			<option value="${area.areaid}" 
       				<c:if test="${area.areaid eq goddsAddress.remark1}">selected = "selected"</c:if>>
       					${area.area}
       			</option>
         	</c:forEach>
       </select>
      </div>
  </div>
 
  <div class="qbdd_first ">
      <div style="width:100%;text-align:left;">
         <input  class="address_inputtt"  id="pkId"  type="hidden" name="pkId" value="${goddsAddress.pkId}"/>
      </div>
  </div>
  <div class="qbdd_first bor_bor_bottomm">
      <div style="width:100%;text-align:left;">
         <span style="width:70px; display:inline-block; height:33px; line-height:33px;">详细地址：</span>
         <input  class="address_inputtt"  id="shopAddress" type="text" name="shopAddress" value="${goddsAddress.shopAddress}"/>
      </div>
  </div>

 
  <div class="qbdd_first bor_bor_bottomm" id ="address">
      <div style="width:100%;text-align:left;">
          <span style="width:110px; display:inline-block; height:33px; line-height:33px;">设置默认地址:</span>
          <input class="address_inputtt"  id="addressState" type="hidden" name="address" value="${goddsAddress.addressState}"/>
          <input id="checkbox" name="addressState" type="checkbox" value="1" style="width:24px;height:24px;line-height:24px; position: relative;top:8px;">
      </div>
  </div>
    <div class="qbdd_first" style="height:58px;">
      <p style="width: 100%; line-height:18px; font-size:16px;"><span id="msg" class="Cred"></span></p>
  </div>
  <div class="jiesuan">
  	<a href="javascript:void(0);" onclick="toSubmit()"><p class="btn_common" style="position: fixed;    bottom: 60px;">保存</p></a>
   <!--  <a href="javascript:history.go(-1)"><p class="btn_common" style="margin-left:10px;">返回</p></a> -->
  </div>
</form>	 
</div>
	<!--底部浮层begin-->
	

<div style="margin-bottom: 80px;"></div>
<!--底部浮层begin-->
<footer style="width:100%;left:0;">
      <!-- 首页 -->
       <a class="add_bj" href="${ctx}/orderInforController.do?sendGoodsList=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/home.png" style="width: 30%;">
       	<p style="font-size:12px;color:#777;">首页</p>
    </a>
      
    <a class="add_bj" href="${ctx}/goddsAddressController.do?goddsAddressList=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/add.png" style="width: 30%;">
       	<p style="font-size:12px;color:#777;">地址簿</p>
    </a>
    <!-- 订单 -->
    <a class="indent_bj" href="${ctx}/orderInforController.do?add=key&userId=${userId}">
        <img src="${ctx}/common/js/jquery/imgs_init/indent.png"  style="width: 30%;">
        <p style="font-size:12px;color:#777;">我的订单</p>
    </a>
	<!-- 图标 -->
	<a class="shopping_bj car_bj" href="${ctx}/orderInforController.do?shoppingCarList=key&userId=${userId}">
		<img src="${ctx}/common/js/jquery/imgs_init/car.png" style="width: 30%;" />
		<p style="font-size:12px;color:#777;">购物车</p>
		<span id="cartNum"></span>
	</a>
</footer>
<script type="text/javascript">
	/* //判断导航选中
	$(function(){
		cartNum();
		var pathname=window.location.pathname;
		if(pathname!=null){
			if(pathname=="/phone/index.html"||pathname=="/"){//首页
				$(".home_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-a-home-select.png");
			}else if(pathname.indexOf("/phone/gotoProductType")>=0){//分类搜索
				$(".gys_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-catergry-select.png");
			}else if(pathname=="/phone/gotoCart.action"){//购物车
				$(".shopping_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-cart-select.png");
			}else if(pathname.indexOf("/phone/customer")>=0||pathname=="/phone/gotoLogin.action"||pathname=="/phone/gotoMallCoinDetailPage.action"){//用户中心
				$(".me_bj img").attr("src","http://www.cunetong.cn/resource/phone_front/phone/img/index-memu-n-me-select.png");
			}
		}
	}); */

	function cartNum(){
		var userId = $("#userId").val();
		$.ajax({
			url:"${ctx}/orderInforController.do?queryByCarInfo=key",
		    type:"POST",
		    async:"false",
		    datatype:"JOSN",
		    data:{userId:userId},
		    success: function (data) {
		    	var data=$.parseJSON(data);
		    	var num=data.obj;
		    	if(num>0){
		    		$("#cartNum").html(num);
		    		$("#cartNum").css("display","block");
		    		$("#cartNum").animate({
    					width:"22px",
    					height:"22px",
    					padding:"10px"
    				});
		    		$("#cartNum").animate({
    					width:"20px",
    					height:"20px",
    					padding:"0"
    				});
		    	}
		    }
		});
	}
	

	$(window).resize(function(){
	/* alert($(window).height()) */
	if($(window).height() < 400){
		 $("footer").addClass('disno');
		 $(".jiesuan").addClass('disno');
	}else{
		 $("footer").removeClass("disno");
		 $(".jiesuan").removeClass('disno');
	}

	});
</script> 
<!--底部浮层end-->

<!--回到顶部start-->
<script type="text/javascript" src="${ctx}/common/js/jquery/jquery.scrollUp.js" ></script>
<link rel="stylesheet" type="text/css" href="${ctx}/common/js/jquery/css/scrollup.css" />
<script type="text/javascript">
    /*回到顶部JS*/
    $.scrollUp();
</script>
<!--回到顶部end-->
</BODY>
</HTML>

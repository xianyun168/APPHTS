<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
	  init();
	});

	function checkAll(status){
		var title=$('#title').val();
		var content=$('#content').val();
		var pkId = $('#pkId').val();
		var keyWord = $('#keyWord').val();
		if(pkId==""||pkId==null){
	 		$.ajax({
             	type: "post",
             	url: "knowledgeBaseController.do?checkCode=key",
             	data: {"title":title},
             	dataType: "json",
             	success: function(data){
                     if(data.obj=="ok"){
                        saveData('${ctx}/knowledgeBaseController.do?saveUpdate=key&status='+status);
                     }else{
                        tip(data.msg);
                        return;
                     }
                 }
         	});
        }else{
        	$.ajax({
             	type: "post",
             	url: "knowledgeBaseController.do?checkCode=key",
             	data: {"title":title,"pkId":pkId},
             	dataType: "json",
             	success: function(data){
                     if(data.obj=="ok"){
                        saveData('${ctx}/knowledgeBaseController.do?saveUpdate=key&status='+status);
                     }else{
                        tip(data.msg);
                        return;
                     }
                 }
         	});
        }  
	} 
</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${knowledgeBase.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${knowledgeBase.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>标题:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${knowledgeBase.title}" 
    		  style="width:260px;height:35px;" datatype="*4-16" errormsg="请输入4-16位内的字符"  nullmsg="请输入4-16位标题！"/><span style="color:red">*</span>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>关键字:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="keyWord" id="keyWord" onkeypress="notNull(this);" name="keyWord" value="${knowledgeBase.keyWord}" 
    		  style="width:260px;height:35px;" datatype="*2-16" errormsg="请输入2-16位内的字符"  nullmsg="请输入2-16位关键字！"/><span style="color:red">*</span>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>内容:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	      <textarea rows="4" cols="8" style="width:260px;" onkeypress="notNull(this);" name="content" class="content" id="content" 
    	       datatype="*4-4000" errormsg="请输入4-4000位内的字符"  nullmsg="请输入4-4000位内容！">${knowledgeBase.content}</textarea>
    	      <span style="color:red">*</span>
    	  </td>
    	</tr>
     	<tr>
     		<td align="center" colspan="8">
     			<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll('01')">发布</a></span>
        		<span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll('00')">保存</a></span>
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
</body> 
  
  
  
  

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>KFCRM</title>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet" />
</head>
<style type="text/css">
	 .jiekouform label {
    
    float: left;
    text-align: right;
    } 
    .jiekouform > div {
    width: 25%;
    float: left;
    height: 30px;
    line-height: 30px;
    margin-top: 10px;
}

    
</style>
<body>
	<table width="100%" id="AtomicAuditList" toolbar="#demoListtb"></table>
	<div id="demoListtb" style="padding:3px; height: auto">
		<!-- 用于权限判断 start -->
		 <input id="pkId" name="pkId" type="hidden" value=${knowledgeBase.pkId}/>
		<input type="hidden" name="noauto_operationCodes"
			id="noauto_operationCodes" value='${noauto_operationCodes}' />
		<!-- 用于权限判断  end -->
		
		<div class="jiekouform" >
			<div>
				<label class="jiekouname" >关键字：</label>
				<input type="text" id="keyWord"  name="keyWord"  />
			</div>
			<div>
				<label class="jiekouname" >状态：</label>
				<select name="status" id="status">
					<option value="" >请选择</option>
					<option value="00">未发布</option>
					<option value="01">已发布</option>
				</select>
			</div>
			<div style="width: 50%;">
				<label style="width: 20%; float: left;">时间区间：</label> <input
					style="width: 30.5%; float: left; margin-left: 1%;" type="text"
					id="startDate"  name="startDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'endDate\')||\'%y-%M-%d\'}'})"></input>

				<label
					style="width: 2%; float: left; margin-left: 1%; text-align: center;">-</label>
				<input style="width: 30.5%; float: left; margin-left: 1%;"
					type="text" id="endDate" name="endDate"
					class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'startDate\')}',maxDate:'%y-%M-%d'})"></input>
			</div>
		</div>
		<div style="clear:both;padding-top:20px;">
			<div class="leftbtntool" style="width:400px;margin:auto;">			
				<span class="bluebtnhover"><a onclick="add_win('知识库条目新增',' ${ctx}/knowledgeBaseController.do?knowledgeBaseAdd=key','AtomicAuditList',710,330)" id="add" plain="true" class="formbtn bluebtn">新增</a></span> 
				<%-- <span class="bluebtnhover"><a href="javascript:void(0)" class="formbtn bluebtn" plain="true" onclick="update('知识条目编辑',' ${ctx}/knowledgeBaseController.do?modify=key','AtomicAuditList',710,330)" id="modify">编辑</a></span>  --%>
				
				<span class="orangebtnhover" ><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
				<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
			</div>
		</div>
		
		<div style="background-color:white;"></div>
	</div>
	 <script type="text/javascript">
		function query() {
			$('#AtomicAuditList').datagrid({
				idField : 'pkId',
				title : '知识库管理',
				url : ' ${ctx}/knowledgeBaseController.do?getKnowledgeBaseList=key',
				fit : true,
				loadMsg : '数据加载中...',
				pageSize : 10,
				pagination : true,
				pageList : [ 10, 20, 30 ],
				//sortName: 'id',
				//sortOrder : 'asc',
				//striped : true,
				rownumbers : true,
				singleSelect : true,
				fitColumns : true,
				//remoteSort : false,
				showFooter : true,
				frozenColumns : [ [] ],
				queryParams : {
					'startDate': $('#startDate').val(),
					'endDate': $('#endDate').val(),
					'keyWord': $('#keyWord').val(),
					'status': $('#status').val()
					},
				columns : [ [  {
					field : 'ck',
					checkbox : true
				},  {
					field : 'pkId',
					title : '主键',
					width : fixWidth(0.11),
					align : 'center',
					sortable : true,
					hidden : true
				}, {
						field : 'title',
						title : '标题',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					}, {
						field : 'status',
						title : '发布状态',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					}, {
						field : 'createdDate',
						title : '创建时间',
						formatter : function(val, rec) {
							if (val != null && val != '') {
								return formattimed(val);
							}
						},
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					}, 
					 {
						field : 'publisher',
						title : '创建人',
						width : fixWidth(0.11),
						align : 'center',
						sortable : true
					},{field : 'opt',title : '操作',width:fixWidth(0.11),align:'center',sortable:true,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=update('知识条目编辑','knowledgeBaseController.do?modify=key','AtomicAuditList','"+rec.pkId+"',710,330) style='color:#3f99b1;'>";
							href += "编辑</a>]";
							href += "[<a href='#' onclick=dele('knowledgeBaseController.do?delete=key','AtomicAuditList','"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "删除</a>]";
							href += "[<a href='#' onclick=details('知识详情','knowledgeBaseController.do?details=key','AtomicAuditList','"+rec.pkId+"',710,330) style='color:#3f99b1;'>";
							href += "详情</a>]";
							href += "[<a href='#' onclick=releaseCel('knowledgeBaseController.do?releaseCel=key','AtomicAuditList','"+rec.pkId+"','"+rec.status+"') style='color:#3f99b1;'>";
							href += "取消发布</a>]";
							var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
							var oper = operCode.split(', ');
							var i = oper.length;
							if(oper !=""){
							  for(var j=0;j<oper.length;j++){
								
								  if(oper[j] == "modify"){
										 href=href.replace("[<a href='#' onclick=update('知识条目编辑','knowledgeBaseController.do?modify=key','AtomicAuditList','"+rec.pkId+"',710,330) style='color:#3f99b1;'>编辑</a>]","");
										}
								 if(oper[j] == "detail"){
									 href=href.replace("[<a href='#' onclick=details('知识详情','knowledgeBaseController.do?details=key','AtomicAuditList','"+rec.pkId+"',710,330) style='color:#3f99b1;'>详情</a>]","");
									}
								 if(oper[j] == "delete"){
									 href=href.replace("[<a href='#' onclick=dele('knowledgeBaseController.do?delete=key','AtomicAuditList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
									}
								 if(oper[j] == "releaseCel"){
									 href=href.replace("[<a href='#' onclick=releaseCel('knowledgeBaseController.do?releaseCel=key','AtomicAuditList','"+rec.pkId+"','"+rec.status+"') style='color:#3f99b1;'>取消发布</a>]","");
									}
							
								}
							}
							return href;
						}} ] ],
					onLoadSuccess : function(data) {
						$("#AtomicAuditList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
						//清楚所选的行
						$("#AtomicAuditList").datagrid("clearSelections");
					},
					onLoadError : function(){
						history.go(0);
					},
					//rowIndex:行索引 rowData：行数据
					onClickRow : function(rowIndex, rowData) {
						rowid = rowData.id;
						gridname = 'AtomicAuditList';
					}
				});
				$("#AtomicAuditList").datagrid('getPager').pagination({
					beforePageText : '',
					afterPageText : '/{pages}',
					displayMsg : '显示{from}-{to}共{total}条',
					showPageList : true,
					showRefresh : true
				});

				$("#AtomicAuditList").datagrid('getPager').pagination({
					onBeforeRefresh : function(pageNumber, pageSize) {
						$(this).pagination('loading');
						$(this).pagination('loaded');
					}
				});
			};
			$(function() {
				//查询
				query();
				noauto();

			});
			function reset() {
				$('#keyWord').val('');
				$('#status').val('');
				$('#startDate').val('');
				$('#endDate').val('');
			}
			function update(title, url, id, pkId, width, height) {
				url += '&pkId=' + pkId;
				width = width ? width : 700;
				height = height ? height : 400;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + url,
					lock : true,
					width : width,
					height : height,
					title : title,
					opacity : 0.3,
					cache : false
				}); 
			}
			function reloadTable() {
				try {
					$('#' + AtomicAuditList).datagrid('reload');
					$('#' + AtomicAuditList).treegrid('reload');
				} catch (ex) {
				}
			}

			function getdemoListSelected(field) {
				return getSelected(field);
			}

			//选中单行
			function getSelected(field) {
				var row = $('#' + AtomicAuditList).datagrid('getSelected');
				if (row != null) {
					value = row[field];
				} else {
					value = '';
				}
				return value;
			}

			function getUserListSelections(field) {
				var ids = [];
				var rows = $('#AtomicAuditList').datagrid('getSelections');
				for ( var i = 0; i < rows.length; i++) {
					ids.push(rows[i][field]);
				}
				ids.join(',');
				return ids
			};

			//页面初始化时，设置按钮的权限
			function noauto() {
				//取得对应的权限的值(后台返回的是没有的权限的值)
				var operCode = $("#noauto_operationCodes").val().replace("[", "")
						.replace("]", "");
				var oper = operCode.split(', ');
				var i = oper.length;
				$(oper).each(function(i) {
					$('#' + oper[i]).hide();
				})
			}
			/**
			 * 打开一个增加页面
			 */
			function add_win(title, addurl, gname, width, height) {
				gridname = gname;
				width = width ? width : 700;
				height = height ? height : 400;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + addurl,
					//lock : true,
					resizable:false,
					maximizable:false,
					width : width,
					height : height,
					title : title,
					opacity : 0.3
					//cache : false,
					//modal: true
				});
			}
			//删除
			function dele(url, id, pkId) {
				var rowsData = $('#' + id).datagrid('getSelections');
				url += '&pkId=' + pkId;
				window.top.$.messager.confirm('Confirm', '你确定要删除吗', function(r) {
					if (r) {
						$.post(url, function(result) {
							if (result.success) {
								if (result.obj == 'alert') {
									tip(result.msg);
								} else {
									$('#' + id).datagrid('reload'); // reload the user data
									$('#' + id).treegrid('reload'); // reload the user data
								}
							} else {
								window.top.$.messager.show({// show error message
									title : 'Error',
									msg : result.errorMsg
								});
							}
						}, 'json');
					}
				});
			}
			//详情列
			function details( title,url, id,rec,width,height) {
				url += '&pkId=' + rec;
				width = width ? width : 720;
				height = height ? height : 400;
				if (width == "100%" || height == "100%") {
					width = document.body.offsetWidth;
					height = document.body.offsetHeight - 100;
				}
				$.dialog({
					content : 'url:' + url,
					lock : true,
					width : width,
					height : height,
					title : title,
					opacity : 0.3,
					cache : false,
					modal: true
				});
			}
			//取消发布
			function releaseCel(url, id,pkId,status) {
			 	if(status!="已发布"){
					tip("所选知识是未发布状态")
					return;
				}
				url += '&pkId=' + pkId;
				window.top.$.messager.confirm('Confirm', '你确定要取消发布吗', function(r) {
					if (r) {
						$.post(url, function(result) {
							if (result.success) {
								if (result.obj == 'alert') {
									tip(result.msg);
								} else {
									$('#' + id).datagrid('reload'); // reload the user data
									$('#' + id).treegrid('reload'); // reload the user data
								}
							} else {
								window.top.$.messager.show({// show error message
									title : 'Error',
									msg : result.errorMsg
								});
							}
						}, 'json');
					}
				});
			}
	</script> 
</body>
</html>
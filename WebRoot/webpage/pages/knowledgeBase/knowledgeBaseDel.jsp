<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
	$(function(){
	  init();
	});

</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${knowledgeBase.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${knowledgeBase.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
  		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>标题:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${knowledgeBase.title}" 
    		  style="width:260px;height:35px;" readonly="readonly" />
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>创建人:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${knowledgeBase.publisher}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	</tr>
		<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>发布状态:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	  	<input class="title" id="title" onkeypress="notNull(this);" name="title" value="${knowledgeBase.status}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  	 <%--  <c:if test="${knowledgeBase.status=='00'}">
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="未发布" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    		  </c:if>
    		  <c:if test="${knowledgeBase.status=='01'}">
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="已发布" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    		  </c:if> --%>
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>创建时间:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="title" id="title" onkeypress="notNull(this);" name="title" value="${createdDate}" 
    		  style="width:260px;height:35px;" readonly="readonly"/>
    	  </td>
    	</tr>
    	<tr>
		  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>内容:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    	      <textarea rows="4" cols="8" style="width:260px;" onkeypress="notNull(this);" name="content" class="content" id="content" 
    	       readonly="readonly">${knowledgeBase.content}</textarea>
    	  </td>
    	</tr>
     	<tr>
     		<td align="center" colspan="8">
				<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 		</td>
    	</tr>
   </table>
 </form>
</body> 
  
  
  
  

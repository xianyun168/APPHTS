<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="AppCustomerInforList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
			<div>
				<label>售后订单号：</label> 
				<input type="text" id="customerId" name="customerId" />
			</div>

			<div style="width: 66.66%;">
				<label style="width: 20%; float: left;">订单日期：</label> 
				<input style="width: 27.5%; float: left; margin-left: 1%;" type="text"
					id="customerTime"  name="customerTime" class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd'})"></input>
			</div>

			<div> 
				<label class="jiekouname">售后类型：</label>
		   		<select id ="customerState" name="customerState">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="01"<c:if test="${appCustomerInfor.customerState=='01'}">selected</c:if>>退货</option>
					<option value="02"<c:if test="${appCustomerInfor.customerState=='02'}">selected</c:if>>换货</option>
    		 	</select> 
			</div>
	</div>
	
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<span class="bluebtnhover"><a onclick="add_win('售后订单增加',' ${ctx}/appCustomerInforController.do?add=key','AppCustomerInforList',1000,400)" id="add" plain="true" class="formbtn bluebtn">新增</a></span> 
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#AppCustomerInforList').datagrid({
			idField : 'pkId',
			title : '售后管理',
		    url : ' ${ctx}/appCustomerInforController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			/* <!--查询参数 --> */
			queryParams:{
				'customerId': $('#customerId').val(),
				'customerTime': $('#customerTime').val(),
				'customerState': $('#customerState').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'customerId',title:'售后单号',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'orderId',title:'订单单号',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'customerName',title:'购买人姓名',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'customerState',title:'订单状态',  width:fixWidth(0.15),align:'center',
						 formatter:function(value){
							 if(value == '01'){
								 return '退货';
							 }else{
								 return '换货';
							 }
						 },
						sortable:false},
					{field:'customerTime',title:'售后日期',
						 formatter:function(val,rec){
			        		if(val != null && val != ''){
			        			return formattimed(val);
			        		}
			      		}, 
						width:fixWidth(0.15),align:'center',sortable:false},
					{field:'orderPrice',title:'订单金额',   width:fixWidth(0.15),align:'center',sortable:false},
					{field : 'opt',title : '操作',width:fixWidth(0.2),align:'center',sortable:false,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=update('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "修改</a>]";
									var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
										var oper = operCode.split(', ');
										var i = oper.length;
										if(oper !=""){
										  for(var j=0;j<oper.length;j++){
											 if(oper[j] == "update"){
												 
											 }
											}
										}
							return href;
					}}
				    ]],
			onLoadSuccess : function(data) {
			    $("#AppCustomerInforList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清除所选的行
				$("#AppCustomerInforList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'certificateInfoList';
			}
		});
		$("#AppCustomerInforList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#AppCustomerInforList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});

function reset(){
	$('#customerId').val("");
	$('#customerTime').val("");
	$('#customerState').val("");
}


//详情页面
function detail(pkId){
	add_win('详情','${ctx}/appCustomerInforController.do?specific=key&pkId='+pkId,'AppCustomerInforList',1000,400);
	
}

//编辑页面
function update(pkId){

	add_win('编辑', '${ctx}/appCustomerInforController.do?afterUpdate=key&pkId='+ pkId,'AppCustomerInforList',1000,400);
}

function reloadTable() {
	try {
		$('#' + MailInfoList).datagrid('reload');
		$('#' + MailInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + MailInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#AppCustomerInforList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

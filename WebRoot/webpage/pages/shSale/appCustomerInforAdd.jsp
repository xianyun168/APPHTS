<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});

function chooseRole(){
		var url='${ctx}/appCustomerInforController.do?orderList=key';
	 	var width = width?width:700;
		var height = height?height:400;
		if(width=="100%" || height=="100%"){
			width = document.body.offsetWidth;
			height =document.body.offsetHeight-100;
		}
				$.dialog({
					zIndex: 1997,
					content: 'url:'+url,
					lock : true,
					width:width,
					height:height,
					title:"",
					opacity : 0.3,
					cache:false
				});

}

function chooseGoods(){
	var order = $("#orderId").val();
	var url='${ctx}/appCustomerInforController.do?goodsList=key&orderId='+order;
 	var width = width?width:700;
	var height = height?height:400;
	if(width=="100%" || height=="100%"){
		width = document.body.offsetWidth;
		height =document.body.offsetHeight-100;
	}
			$.dialog({
				zIndex: 1997,
				content: 'url:'+url,
				lock : true,
				width:width,
				height:height,
				title:"",
				opacity : 0.3,
				cache:false
			});

}

function saveData(url) {
	var orderId=$('#orderId').val();
	var goodsNames=$('#goodsNames').val();
	if(orderId ==''){
		tip("请选择订单");
		return;
	}
	
	if(goodsNames==''){
		tip("请选择商品!");
		return;
	}
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		// $.dialog.tips('数据保存中...',500,'loading.gif');
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
			'submit',
			{
				url : url,
				onSubmit : function() {
					var form = $("#myform").Validform();
					return form.check();
				},
				success : function(data) {
					var windowapi = frameElement.api, W = windowapi.opener;
					var d;
					try {
						d = $.parseJSON(data);
						if (d.success == true) {
							window.top.$.messager.progress('close');
							windowapi.close();
							W.tip(d.msg);
							W.query();
						}
						/*
						 * modify by houwei 在存储数据时，如果发现重复，或者异常，可以在这里抛出。
						 * 需要在Controller层的saveUpdate函数内判断后插入j.setSuccess(false);
						 */
						else {
							window.top.$.messager.progress('close');
							$("#saveBtn").removeAttr("disabled");
							window.top.$.messager.alert('提示', d.msg);
							return;
						}
					} catch (ex) {
						var emsg = data.substring(data.indexOf('错误描述'), data
								.indexOf('错误信息'));
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('错误', emsg);
						return;
					}
				}
			});
}
</script>
 </head>
 <body style="overflow-y: hidden" scroll="no">
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${appCustomerInfor.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${appCustomerInfor.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
		<tr>
			<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>订单号:</span></label>
    		</td>
    		<td class="value" width="35%" >
    		 	<input class="inputxt" id="orderId" name="orderId" value="${appCustomerInfor.orderId}"  readonly="readonly" />
    			<a href="javascript:chooseRole();">选择订单</a>
    		</td>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>商品:</span></label>
    		</td>
    		<td class="value" width="35%" >
    		 	<input class="inputxt" id="goodsNames" name="goodsNames" value="${appCustomerInfor.goodsIds}"  readonly="readonly" />
    			<input class="inputxt" id="goodsIds"  name="goodsIds" type="hidden" /><span style="color:red">*</span>
    			<a href="javascript:chooseGoods();">选择商品</a>
    		</td>
    	</tr>
    	<tr>
    	
    	 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>售后时间:</span></label>
    	 </td>
    	 <td class="value" width="35%" >
    		  <input type="text" id="customerTime" value="${customerDate}" name="customerTime"  class="Wdate" style="width: 260px;height:36px;" onkeypress="notNull(this);"onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d'})"></input>
    	 </td>
    	
    	
<!--     	  <td align="right" width="15%" nowrap> -->
<!--     		  <label class="Validform_label"><span>购买人姓名:</span></label> -->
<!--     	  </td> -->
<!--     	  <td class="value" width="35%" > -->
<%--     		  <input class="inputxt" id="customerName" onkeypress="notNull(this);" name="customerName" value="${userInfo.password}" datatype="/^[^&lt;&gt;]{6,16}$/"  errormsg="请输入6-16位内的字符"  nullmsg="请输入6-16位密码！"/><span style="color:red">*</span> --%>
<!--     	  </td> -->
    	</tr>
		<tr>
		 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>积分调整:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <input class="inputxt" id="integralAdjust" onkeypress="notNull(this);" name="integralAdjust" value="${appCustomerInfor.integralAdjust}" datatype="n1-10"  errormsg="请输入正确的积分"  nullmsg="请输入积分"/><span style="color:red">*</span>
    	  </td>
    	  <td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>售后类型:</span></label>
    	  </td>
    	  <td class="value" width="35%" >
    		  <select id ="customerState" name="customerState"> 
	   		 		<option value="01"<c:if test="${appCustomerInfor.customerState=='01'}">selected</c:if>>退货</option>
					<option value="02"<c:if test="${appCustomerInfor.customerState=='02'}">selected</c:if>>换货</option>
 	    		</select>
    	  </td>
    	</tr>
     <tr>
     <td align="center" colspan="8">
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData('${ctx}/appCustomerInforController.do?saveUpdate=key')">保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body> 
  
  
  
  

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
	</head>
<body>
<table width="100%" id="MessageLog" toolbar="#demoListtb"></table>
<div id="demoListtb" style="padding:3px; height: auto">
<div name="searchColums"  style="float:left;">
</div>

<div style="height:30px;" class="datagrid-toolbar" align="left">
		<input id="orderId" name="orderId" type="hidden" value="${orderId}">
		 <a href="#"class="easyui-linkbutton" plain="true" icon="icon-add" onclick="getRoles()" >确定</a>
</div>

</div>
<script type="text/javascript">
	$(function(){
		var windowapi = frameElement.api, W = windowapi.opener;
		var order = $("#orderId").val();
		$('#MessageLog').datagrid({
			idField : 'pkId',
			title : '',
		    url : '${ctx}/appCustomerInforController.do?goodsDatagrid=key&orderId='+order,
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : false,
			fitColumns : true,
			remoteSort:false,
			queryParams:{
				//'userName': $('#userName').val()
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'ID',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'goodsName',title:'商品名称',  width:fixWidth(0.11),align:'center',sortable:true},
					{field:'price',title:'商品价格',  width:fixWidth(0.11),align:'center',sortable:true}
				    ]],
			onLoadSuccess : function(data) {
				//清楚所选的行
				$("#MessageLog").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.pkId;
				gridname = 'MessageLog';
			}
		});
		$("#MessageLog").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
});

	function reloadTable() {
		try {
			$('#' + gridname).datagrid('reload');
			$('#' + gridname).treegrid('reload');
		} catch (ex) {
		}
	}
	
	function getdemoListSelected(field) {
		return getSelected(field);
	}
	
	//选中单行
	function getSelected(field) {
		var row = $('#' + gridname).datagrid('getSelected');
		if (row != null) {
			value = row[field];
		} else {
			value = '';
		}
		return value;
	}
	
	function getUserListSelections(field) {
		var ids = [];
		var rows = $('#MessageLog').datagrid('getSelections');
		for ( var i = 0; i < rows.length; i++) {
			ids.push(rows[i][field]);
		}
		ids.join(',');
		return ids
	};
	
	function getRoles(){
		var pkid=getUserListSelections("pkId");
		var goodsNames=getUserListSelections("goodsName");
		var windowapi = frameElement.api, W = windowapi.opener;
		W.$('#goodsIds').val(pkid);
		W.$('#goodsNames').val(goodsNames);
		windowapi.close();
	}
</script>
	</body>
</html>


		
			

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>登录页</title>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<script src="${ctx}/webpage/pages/login/login.js"></script>
<script>
$(function(){
	
	initCode();
});

function initCode(){
	
    $("#code_img").attr("src","${ctx}/verification.do?rmd="+new Date().getTime())//如果需要点击图片改变图片的内容，则必须添加时间搓
    .click(function(){
     $(this).attr("src","${ctx}/verification.do?rmd="+new Date().getTime());
    }); 
}
//显示错误提示
function showError(str) {
	initCode();
	$('#loginMsg').addClass('error').html(str).stop(true, true).show().animate({

		opacity : 1,
		right : '0'
	}, 500);
}
$(document).ready(function() {
    //each遍历文本框
    $("#userCode").each(function() {
        //保存当前文本框的值
        var vdefault = this.value;
        $(this).focus(function() {
            //获得焦点时，如果值为默认值，则设置为空
            if (this.value == vdefault) {
                this.value = "";
            }
            $(this).css("color","#fff");
        });
        $(this).blur(function() {
            //失去焦点时，如果值为空，则设置为默认值
            if (this.value == "") {
                this.value = vdefault;
            }
            $(this).css("color","#d1d1d1");
        });
    });
    $("#password").each(function() {
        //保存当前文本框的值
        var vdefault = this.value;
        $(this).focus(function() {
            //获得焦点时，如果值为默认值，则设置为空
            if (this.value == vdefault) {
                this.value = "";
            }
            $(this).css("color","#fff");
        });
        $(this).blur(function() {
            //失去焦点时，如果值为空，则设置为默认值
            if (this.value == "") {
                this.value = vdefault;
            }
            $(this).css("color","#d1d1d1");
        });
    });
});
</script>
<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet" />
<style>
*{
	margin:0;
	padding:0;
	font-family:'Microsoft Yahei';
}
ul{
	list-style:none;
}
#wholeBg{
	background:url(${ctx}/webpage/pages/login/images/wholeBg.png) ;
	background-size:100%;
	width:100%;
	height:100%;
	position:fixed;
	top:0;
	left:0;
}
.loginlogo{
	width:291px;
	height:200px;
	margin:auto;
	margin-top:50px;
	position:reletive;
}
.loginlogo p{
	color:#fff;
	font-size:30px;
	font-weight:bold;
	text-align:center;
	position:absolute;
	top:150px;
	left:0%;
	right:0%;
}
.loginformbox{
	width:520px;
	height:365px;
	background:url(${ctx}/webpage/pages/login/images/loginBoxbg.png) repeat-y;
	position:absolute;
	top:50%;
	left:50%;
	margin-left:-260px;
	margin-top:-140px;
	border:1px solid #fff;
	border-radius:4px;
}
.loginformtitle{
	color:#ffffff;
	font-size:30px;
	font-weight:bold;
	text-align:center;
	margin-top:20px;
}
.logintable{
	width:400px;
	height:auto;
	margin:auto;
	margin-top:30px;
}
.tdtitle{
	display:block;
	width:64px;
	height:40px;
	line-height:40px;
	text-align:left;
	margin-right:40px;
	margin-bottom:25px;
	color:#fff;
	font-size:16px;
	float:left;
}

.pwdbtn{
	width:238px;
	height:38px;
	float:left;
	border:1px solid #fff;
	background:rgba(0,0,0,0) url(${ctx}/webpage/pages/login/images/loginUser.png) no-repeat 10px 10px;
	color:#fff;
	padding-left:40px;
	margin-bottom:25px;
	line-height:38px;
	font-size:16px;
}
}
#userCode{
	background:url(${ctx}/webpage/pages/login/images/loginUser.png) no-repeat 10px 10px;
}
#password{
	background:url(${ctx}/webpage/pages/login/images/loginLock.png) no-repeat 10px 10px;
}
#code{
    width:110px;
    background:none;
    margin-right:10px;
}
.go{
	background:#e88439;
	height: 40px;
	width: 384px;
	color: #fff;
	font-size: 16px;
	cursor: pointer;
	border:none;
	outline:none;
}
.loginfooter{
	position:absolute;
	bottom:30px;
	left:0%;
	right:0%;
	color:#fff;
	text-align:center;
	font-size:14px;
}
.imges{
    position: relative;
    right: 105px;
    bottom: -19%;
}
</style>
</head>

<body>
<div id="wholeBg">
	<div class="loginlogo">
		<img  class="imges" src="${ctx}/webpage/pages/login/images/loginLogo.png " />
	</div>
    <div class="loginformbox">
      <form name="formLogin" id="formLogin" action="loginController.do?login=key" check="loginController.do?checkuser=key" method="post">
    	<p class="loginformtitle">秀实投资后台管理系统</p>
    	<table border="0" cellpadding="0" cellspacing="0" class="logintable">
            <tr>
             	<td>
             		<span class="tdtitle">用户名：</span>
             		<input type="text" id="userCode" datatype="s" name="userCode" class="pwdbtn" placeholder="请输入用户名" />
             	</td>
			</tr>
			<tr>
            	<td>
            		<span class="tdtitle">密&nbsp;&nbsp;&nbsp;码：</span>
            		<input type="password" id="password" name="password" class="pwdbtn" placeholder="请输入密码" />
            	</td>
			</tr>
			<tr style="line-height:60px;">    
                    <td>
                    	<span class="tdtitle">验证码：</span>  
                    	<span  class="orangebtnhover"><input    type="text" id="code" name="code" value="" class="pwdbtn" placeholder="验证码"/></span>
                        <span  class="orangebtnhover"><img class="" id="code_img" onclick="initCode()" /></span>
                    </td>
                </tr>
			<tr>
				<td >
            		<input type="button" value="登&nbsp;&nbsp;录" class="go" id="login_btn" onclick="return checkForm();"/></td>
			</tr>
            </table>
            <span id="loginMsg" style="color:#f00; height:20px;display:block;margin:auto; margin-top:10px;margin-left:67px; width:384px; font-size:13px; text-align:right;"></span>
        </form>
    	</div>
    	<p class="loginfooter">技术支持&nbsp;&nbsp;&nbsp;&nbsp;中科软科技股份有限公司</p>
	</div>
</body>
<%-- <script src="${ctx}/webpage/pages/login/jquery.min.js" type="text/javascript"></script> --%>
<script src="${ctx}/webpage/pages/login/placeholder.js" type="text/javascript"></script>
</html>

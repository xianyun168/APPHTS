/**
 *登陆控制 
*/

function checkForm(){
	submit();
}


//回车登录
$(document).keydown(function(e){
	if(e.keyCode == 13) {
		submit();
	}
});

//表单提交
function submit()
{
	var submit = true;
	if($("#userCode").val()==""){
	   // alert("请输入用户名");
		document.getElementById("loginMsg").innerHTML = "请输入用户名！";
		submit = false;
		return false;
		
	}	else if($("#password").val()==""){
		document.getElementById("loginMsg").innerHTML = "请输入密码！";
	//	alert("请输入密码");
		submit = false;
		return false;
	}
	if (submit) {
		Login();
	}
}


//登录处理
function Login() {
	var actionurl=$('form').attr('action');//提交路径
	var checkurl=$('form').attr('check');//验证路径
	 var formData = new Object();
	var data=$(":input").each(function() {
		 formData[this.name] =$("#"+this.name ).val();
	});
	$.ajax({
		async : false,
		cache : false,
		type : 'POST',
		url : checkurl,// 请求的action路径
		data : formData,
		error : function() {// 请求失败处理函数
		},
		success : function(data) {
			var d = $.parseJSON(data);
			if (d.success) {
				//loginsuccess();
				//不能回退到登录页面
				setTimeout("window.location.replace('"+actionurl+"')", 1000);
				
				$.dialog.tips('登录中...',500,'loading.gif');
				$('#userCode').attr("readonly","readonly");
				$('#password').attr("readonly","readonly");
				$('#remark').attr("readonly","readonly");

			} else {
				$.dialog.tips('登录中...',1,'loading.gif');
				showError(d.msg);
			}
		}
	});
}

//显示错误提示
function showError(str) {
	$('#userCode').removeAttr("readonly");
	$('#password').removeAttr("readonly");
	$('#remark').removeAttr("readonly");
	$('#loginMsg').addClass('error').html(str).stop(true, true).show().animate({
		opacity : 1,
		right : '0'
	}, 500);
}
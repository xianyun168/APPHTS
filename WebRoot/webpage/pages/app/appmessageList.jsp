<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="AppMessageSendList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
			<div>
				<label>消息标题：</label> 
				<input type="text" id="messageTitle" name="messageTitle" />
			</div>

			<div style="width: 66.66%;">
				<label style="width: 20%; float: left;">推送时间：</label> 
				<input style="width: 27.5%; float: left; margin-left: 2%;" type="text"
					id="sendDate"  name="sendDate" class="Wdate" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss'})"></input>
			</div>

			<div> 
			<label class="jiekouname">消息类型：</label>
		   		<select id ="flag" name="type">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="常规消息"<c:if test="${appMessageSend.messageType=='常规消息'}">selected</c:if>>常规消息</option>
					<option value="强制提醒"<c:if test="${appMessageSend.messageType=='强制提醒'}">selected</c:if>>强制提醒</option>
    		 	</select> 
			</div>
		    
		    <div> 
			<label class="jiekouname">推送状态：</label>
		   		<select id ="flag1" name="type">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="已推送"<c:if test="${appMessageSend.sendState=='已推送'}">selected</c:if>>已推送</option>
					<option value="未推送"<c:if test="${appMessageSend.sendState=='未推送'}">selected</c:if>>未推送</option>
    		 	</select> 
			</div>
	</div>
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<span class="bluebtnhover"><a onclick="add_win('消息推送增加',' ${ctx}/appMessageController.do?add=key','AppMessageSendList',1000,500)" id="add" plain="true" class="formbtn bluebtn">新增</a></span> 
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#AppMessageSendList').datagrid({
			idField : 'pkId',
			title : 'APP消息推送管理',
		    url : ' ${ctx}/appMessageController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			/* <!--查询参数 --> */
			queryParams:{
				'messageTitle': $('#messageTitle').val(),
				'sendDate': $('#sendDate').val(),
				'messageType': $('#flag').val(),
			    'sendState': $('#flag1').val() 
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'messageTitle',title:'消息标题',  width:fixWidth(0.15),align:'center',sortable:false},
					{field:'sendDate',title:'推送时间',width:fixWidth(0.15),align:'center',sortable:false},
					{field:'sendType',title:'推送类型',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'messageType',title:'消息类型',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'sendState',title:'推送状态',   width:fixWidth(0.15),align:'center',sortable:false},
					{field:'operateName',title:'操作人',   width:fixWidth(0.15),align:'center',sortable:false},
					{field : 'opt',title : '操作',width:fixWidth(0.2),align:'center',sortable:false,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=update('"+rec.pkId+"','"+rec.sendState+"') style='color:#3f99b1;'>";
							href += "编辑</a>]";
							href += "[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "详情</a>]";
							href += "[<a href='#' onclick=dele('appMessageController.do?delete=key','AppMessageSendList','"+rec.pkId+"','"+rec.sendState+"') style='color:#3f99b1;'>";
							href += "删除</a>]";
									var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
										var oper = operCode.split(', ');
										var i = oper.length;
										if(oper !=""){
										  for(var j=0;j<oper.length;j++){
											
											  if(oper[j] == "modify"){
													 href=href.replace("[<a href='#' onclick=update('"+rec.pkId+"','"+rec.sendState+"') style='color:#3f99b1;'>编辑</a>]","");
													}
											 if(oper[j] == "detail"){
												 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>详情</a>]","");
												}
											 if(oper[j] == "delete"){
												 href=href.replace("[<a href='#' onclick=dele('appMessageController.do?delete=key','AppMessageSendList','"+rec.pkId+"','"+rec.sendState+"') style='color:#3f99b1;'>删除</a>]","");
												}  
										
											}
										}
							return href;
					}}
				    ]],
			onLoadSuccess : function(data) {
			    $("#AppMessageSendList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清除所选的行
				$("#AppMessageSendList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'certificateInfoList';
			}
		});
		$("#AppMessageSendList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#AppMessageSendList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});

function reset(){
	$('#messageTitle').val("");
	$('#sendDate').val("");
	$('#flag').val("");
	$('#flag1').val("");
}

//编辑页面
function update(pkId,sendState){
	if(sendState=="已推送"){
		tip("已推送的消息不能编辑");
		return;
	}
	add_win('编辑', '${ctx}/appMessageController.do?modify=key&pkId='+ pkId,'AppMessageSendList',1000,500);
}

//详情页面
function detail(pkId){
	add_win('详情','${ctx}/appMessageController.do?specific=key&pkId='+pkId,'AppMessageSendList',1000,500);
}	

//删除
function dele(url,id,pkId,sendState) {
	url += '&pkId='+pkId;
	if(sendState=="已推送"){
		tip("已推送的消息不能删除");
		return;
	}
	window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
	    if (r){
	        $.post(url,function(result){
	            if(result.success){
	            	if(result.obj =='alert'){
	            		tip(result.msg);
	            	}else{
	                    $('#'+id).datagrid('reload');    // reload the user data
	                    $('#'+id).treegrid('reload');	 // reload the user data
	            	}
	            }else{
	                window.top.$.messager.show({// show error message
	                    title: 'Error',
	                    msg: result.errorMsg
	                });
	            }
	        },'json');
	    }
	});
}

function reloadTable() {
	try {
		$('#' + MailInfoList).datagrid('reload');
		$('#' + MailInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + MailInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#AppMessageSendList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

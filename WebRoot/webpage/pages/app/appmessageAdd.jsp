<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
function saveData(url) {
	var messageType=$('#messageType').val();
	var c1=document.getElementsByName("sendObject");
	if(messageType==''){
		tip("请选择消息类型!");
		return;
	}
	if(c1[0].value==''){
		tip("请选择推送对象!");
		return;
	} 
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#saveBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}

function uploadPic(){
   	var imagePath = $("#pic").val();
       if (imagePath == "") {
           alert("请上传图片");
           return false;
       }
       var strExtension = imagePath.substr(imagePath.lastIndexOf('.') + 1);
       if (strExtension != 'jpg' && strExtension != 'JPG' 
    	   && strExtension != 'gif' && strExtension != 'GIF'
    	   && strExtension != 'jpeg' && strExtension != 'JPEG'
           && strExtension != 'png'  && strExtension != 'PNG'
           && strExtension != 'bmp' && strExtension != 'BMP') {
           alert("请上传jpg/gif/png/bmp类型的图片");
           return false;
       }
       // 上传设置  
       var options = {  
               // 规定把请求发送到那个URL  
               url: '${ctx}/appMessageController.do?uploadPic=key',  
               // 请求方式  
               type: "post",  
               // 服务器响应的数据类型  
               dataType: "json",  
               // 请求成功时执行的回调函数  
               success: function(data, status, xhr) {  
                   // 图片显示地址  
                   $("#url").attr("src", data.path); 
                   document.getElementById("pictureURL").value = data.path1;
                   //alert($("#remark1").val());
               }  
       };  
       $("#myform").ajaxSubmit(options);  
   }  

</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=AppMessageSendAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${appMessageSend.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${appMessageSend.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	消息标题:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="messageTitle" onkeypress="notNull(this);" name="messageTitle" value="${appMessageSend.messageTitle}" datatype="*4-25" errormsg="请输入4-25位内的字符！"  nullmsg="请输入消息标题！"/><span style="color: red;">*</span>
    		 </td>
 
    		 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	推送对象:
    		  </label>
    		 </td>
    		 <td class="value" width="35%" >
	    		<input class="easyui-combotree" style="width:260px;height:35px;" name="sendObject" id="cc" value="${appMessageSend.sendObject}" errormsg="请选择推送对象！"  nullmsg="请输入推送对象！" data-options="url:'appMessageController.do?getCombotree=key',method:'get'"/><span style="color: red;">*</span>
			 </td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>定时推送时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="timingSendDate" value="${timingSendDate }"
					name="timingSendDate"  class="Wdate" style="width: 260px;height:36px;"
					datatype="*1-50" nullmsg="请输入推送时间！" errormsg="请输入正确的推送时间！" onkeypress="notNull(this);"
					onClick="WdatePicker({dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'%y-%M-%d'})"></input>
			  <span style="color: red;">*</span>
    		</td>
    		
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	消息类型:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="messageType" name="messageType" onkeypress="notNull(this);">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="常规消息"<c:if test="${appMessageSend.messageType=='常规消息'}">selected</c:if>>常规消息</option>
					<option value="强制提醒"<c:if test="${appMessageSend.messageType=='强制提醒'}">selected</c:if>>强制提醒</option>
    		 	</select><span style="color: red;">*</span>
    		 </td>
    		
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
	    		  <label class="Validform_label">
	    		 	消息内容:
	    		  </label>
    		</td>
    		<td class="value" width="35%">
	    		<textarea class="inputxt" id="messageContent" MaxLength="144"
							onkeypress="notNull(this);" errormsg="不能超过144个字符" nullmsg="请输入推送内容！"
							datatype="*1-144" name="messageContent" cols="45" rows="5" 
							style="width: 260px; height: 160px;resize:none;">${appMessageSend.messageContent}</textarea>
				<span style="color: red;">*</span>
			</td>
			<%-- <td align="right" width="15%" nowrap>
    			<label class="Validform_label">
    		 	 消息内容配图: 
    		   </label>
    		</td>
			<td>
				<img  id="url" src="${appMessageSend.URL}" width="200px" height="150px">
	            <input type="file" name="pic" id="pic" style="width: 150px;">
				<input type="button" value="上传图片" onclick="uploadPic();"/>
				<input id="pictureURL" name="pictureURL" type="hidden" value="${appMessageSend.pictureURL}" />
			</td>  --%>
    	</tr>
     <tr>
     <td align="center" colspan="8">
     	<span class="bluebtnhover"><a  id="sendBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="saveData(' ${ctx}/appMessageController.do?sendMessage=key')">推送</a></span>
        <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData(' ${ctx}/appMessageController.do?saveUpdate=key')" >保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=AppMessageSendSpecific toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${appMessageSend.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${appMessageSend.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	消息标题:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="messageTitle" onkeypress="notNull(this);" name="messageTitle" readonly="readonly" value="${appMessageSend.messageTitle}" />
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	推送对象:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="sendObject" onkeypress="notNull(this);" name="sendObject"  readonly="readonly" value="${appMessageSend.sendObject}" />
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label"><span>推送时间:</span></label>
    	  	</td>
    	   	<td class="value" width="35%" >
    		  <input type="text" id="sendDate" readOnly="readOnly" value="${appMessageSend.sendDate }"
					name="sendDate"  class="inputxt" style="width: 260px;height:36px;"
				    onkeypress="notNull(this);"></input>
    		</td>
    		
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	消息类型:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="activityState" name="messageType" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="常规消息"<c:if test="${appMessageSend.messageType=='常规消息'}">selected</c:if>>常规消息</option>
					<option value="强制提醒"<c:if test="${appMessageSend.messageType=='强制提醒'}">selected</c:if>>强制提醒</option>
    		 	</select>
    		 </td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	推送状态:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="sendState" name="sendState" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="已推送"<c:if test="${appMessageSend.sendState=='已推送'}">selected</c:if>>已推送</option>
					<option value="未推送"<c:if test="${appMessageSend.sendState=='未推送'}">selected</c:if>>未推送</option>
    		 	</select>
    		 </td>
    		 
    		 <td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	操作人:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="operateName" onkeypress="notNull(this);" name="operateName"  readonly="readonly" value="${appMessageSend.operateName}" />
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	推送类型:
    		  </label>
    		</td>
    		 
    		<td class="value" width="35%" >
		   		<select id ="sendType" name="sendType" onkeypress="notNull(this);" disabled="disabled">
	    		 	<option  value="">--请选择--</option>
	    		 	<option value="系统"<c:if test="${appMessageSend.sendType=='系统'}">selected</c:if>>系统</option>
					<option value="人工"<c:if test="${appMessageSend.sendType=='人工'}">selected</c:if>>人工</option>
    		 	</select>
    		 </td>
    	</tr>
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">消息内容:</label>
    		</td>
    		<td class="value" width="35%">
    			<textarea class="inputxt" id="messageContent" readonly="readonly" MaxLength="4000"
						onkeypress="notNull(this);" name="messageContent" cols="45" rows="5" 
						style="width: 260px; height: 160px;resize:none;">${appMessageSend.messageContent}</textarea>
			</td>
    		<%-- <td align="right" width="15%" nowrap>
    			<label class="Validform_label">
    		 	 消息内容配图: 
    		   </label>
    		</td>
    		<td width="35%">
    			<img  id="url" src="${appMessageSend.URL}" width="200px" height="150px" readOnly="readOnly">
    		</td> --%>
    	</tr>
    	
    <tr>
      <td align="center" colspan="8">
       <!--  <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span> -->
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	  </td>
    </tr>
    	
   </table>
 </form>
</body>

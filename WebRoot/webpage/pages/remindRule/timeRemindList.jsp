<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>ESBMM</title>
		<link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
	</head>
<body>
<table width="100%" id="TimeRemindRuleList" toolbar="#demoListtb"></table>
<div id="demoListtb">
<!-- 用于权限判断 start -->
<input type="hidden" name="noauto_operationCodes" id="noauto_operationCodes" value='${noauto_operationCodes}'/>
<!-- 用于权限判断  end -->
	<div class="jiekouform">
			<div>
				<label>消息提醒规则名称：</label> 
				<input type="text" id="messageRemindRuleName" name="messageRemindRuleName" />
			</div>
	</div>
	<div style="clear:both;padding-top:20px;">
		<div class="leftbtntool" style="width:400px;margin:auto;">			
			<%-- <span class="bluebtnhover"><a onclick="add_win('时间提醒规则配置',' ${ctx}/timeRemindRuleController.do?add=key','TimeRemindRuleList',1000,400)" id="add" plain="true" class="formbtn bluebtn">时间提醒配置</a></span>  --%>
			<span class="orangebtnhover"><a href="javascript:void(0)" class="formbtn orangebtn" onClick="reset()" id="reset">重置</a></span>
			<span class="bluebtnhover" ><a onclick="query()" class="formbtn bluebtn">查询</a></span>
		</div>
	</div>

</div>
<script type="text/javascript">
	function query(){
		$('#TimeRemindRuleList').datagrid({
			idField : 'pkId',
			title : '时间提醒规则配置',
		    url : ' ${ctx}/timeRemindRuleController.do?datagrid=key',
			fit : true,
			//loadMsg : '数据加载中...',
			pageSize : 10,
			pagination : true,
			pageList : [ 10, 20, 30 ],
			//sortName: 'id',
			sortOrder : 'asc',
			rownumbers : true,
			singleSelect : true,
			fitColumns : true,
			remoteSort:false,
			/* <!--查询参数 --> */
			queryParams:{
				'messageRemindRuleName': $('#messageRemindRuleName').val(),
			},
			columns:[[
					{field : 'ck',checkbox:true},
					{field:'pkId',title:'主键',  width:fixWidth(0.11),align:'center',sortable:true,hidden:true},
					{field:'messageRemindRuleName',title:'消息提醒规则名称',  width:fixWidth(0.15),align:'center',sortable:false},
					{field : 'opt',title : '操作',width:fixWidth(0.15),align:'center',sortable:false,
						formatter : function(value, rec,index) {
							if (!rec.pkId) {
								return '';
							}
							var href = '';
							href += "[<a href='#' onclick=update('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "编辑</a>]";
							href += "[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "详情</a>]";
							/* href += "[<a href='#' onclick=dele('timeRemindRuleController.do?delete=key','TimeRemindRuleList','"+rec.pkId+"') style='color:#3f99b1;'>";
							href += "删除</a>]"; */
									var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
										var oper = operCode.split(', ');
										var i = oper.length;
										if(oper !=""){
										  for(var j=0;j<oper.length;j++){
											
											  if(oper[j] == "modify"){
													 href=href.replace("[<a href='#' onclick=update('"+rec.pkId+"') style='color:#3f99b1;'>编辑</a>]","");
													}
											 if(oper[j] == "detail"){
												 href=href.replace("[<a href='#' onclick=detail('"+rec.pkId+"') style='color:#3f99b1;'>详情</a>]","");
												}
											 if(oper[j] == "delete"){
												 href=href.replace("[<a href='#' onclick=dele('timeRemindRuleController.do?delete=key','TimeRemindRuleList','"+rec.pkId+"') style='color:#3f99b1;'>删除</a>]","");
												}  
										
											}
										}
							return href;
					}}
				    ]],
			onLoadSuccess : function(data) {
			    $("#TimeRemindRuleList").parent().find("div .datagrid-header-check").children("input[type=\"checkbox\"]").eq(0).attr("style", "display:none;");
				//无查询结果提示
				if (data.total == 0) {
					$('.datagrid-view2 .datagrid-body')
							.append(
									'<div style="width:600px;margin:0 auto;"><img src="${ctx}/webpage/pages/login/images/nothing.png" style="height:100%;margin:0 auto;"></div>');

					$(this).closest('div.datagrid-wrap')
							.find('div.datagrid-pager')
							.hide();
				}
				//清除所选的行
				$("#TimeRemindRuleList").datagrid("clearSelections");
			},
			//rowIndex:行索引 rowData：行数据
			onClickRow : function(rowIndex, rowData) {
				rowid = rowData.id;
				gridname = 'certificateInfoList';
			}
		});
		$("#TimeRemindRuleList").datagrid('getPager').pagination({
			beforePageText : '',
			afterPageText : '/{pages}',
			displayMsg : '显示{from}-{to}共{total}条',
			showPageList : true,
			showRefresh : true
		});
		
		$("#TimeRemindRuleList").datagrid('getPager').pagination({
			onBeforeRefresh : function(pageNumber, pageSize) {
				$(this).pagination('loading');
				$(this).pagination('loaded');
			}
		});
};

$(function(){
	//查询
	query();
	
	noauto();
	
});

function reset(){
	$('#messageRemindRuleName').val("");
}

//编辑页面
function update(pkId){
	var rowsData = $('#TimeRemindRuleList').datagrid('getSelections');
	if (rowsData.length > 1) {
		tip('请选择一条数据修改');
		return;
	}
	add_win('编辑', '${ctx}/timeRemindRuleController.do?modify=key&pkId='+ pkId,'TimeRemindRuleList',1000,400);
}

//详情页面
function detail(pkId){
	add_win('详情','${ctx}/timeRemindRuleController.do?specific=key&pkId='+pkId,'TimeRemindRuleList',1000,400);
	
}	


//删除
function dele(url,id,pkId) {
var rowsData = $('#'+id).datagrid('getSelections');
url += '&pkId='+pkId;
window.top.$.messager.confirm('Confirm','你确定要删除吗',function(r){
    if (r){
        $.post(url,function(result){
            if (result.success){
            	if(result.obj =='alert'){
            		tip(result.msg);
            	}else{
                    $('#'+id).datagrid('reload');    // reload the user data
                    $('#'+id).treegrid('reload');	 // reload the user data
            	}
            } else {
                window.top.$.messager.show({// show error message
                    title: 'Error',
                    msg: result.errorMsg
                });
            }
        },'json');
    }
});
}

function reloadTable() {
	try {
		$('#' + MailInfoList).datagrid('reload');
		$('#' + MailInfoList).treegrid('reload');
	} catch (ex) {
	}
}

function getdemoListSelected(field) {
	return getSelected(field);
}

//选中单行
function getSelected(field) {
	var row = $('#' + MailInfoList).datagrid('getSelected');
	if (row != null) {
		value = row[field];
	} else {
		value = '';
	}
	return value;
}

function getUserListSelections(field) {
	var ids = [];
	var rows = $('#TimeRemindRuleList').datagrid('getSelections');
	for ( var i = 0; i < rows.length; i++) {
		ids.push(rows[i][field]);
	}
	ids.join(',');
	return ids
};

	//页面初始化时，设置按钮的权限
function noauto(){
	//取得对应的权限的值(后台返回的是没有的权限的值)
    var operCode = 	$("#noauto_operationCodes").val().replace("[","").replace("]","");
	var oper = operCode.split(', ');
	var i = oper.length;
	$(oper).each(function(i){
		$('#' + oper[i]).hide();
	})
}

</script>
	</body>
</html>


		
			

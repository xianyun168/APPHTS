<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=collateMarkSpecific toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${messageRemindRule.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${messageRemindRule.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	消息提醒规则名称:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="messageRemindRuleName" onkeypress="notNull(this);" name="messageRemindRuleName" readonly="readonly" value="${messageRemindRule.messageRemindRuleName}" />
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	通知方式:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="remark1" onkeypress="notNull(this);" name="remark1"  readonly="readonly" value="实时通知" />
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	消息模板:
    		  </label>
    		</td>
    		<td class="value" width="35%">
    			<textarea class="inputxt" id="messageMould" readonly="readonly" MaxLength="400"
						onkeypress="notNull(this);" name="messageMould" cols="45" rows="5" 
						style="width: 260px; height: 100px;resize:none;">${messageRemindRule.messageMould}</textarea>
			</td> 
    	</tr>
    	
     <td align="center" colspan="8">
       <!--  <span class="orangebtnhover"><a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="checkAll()">保存</a></span> -->
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
   </table>
 </form>
</body>

<%@ page language="java" pageEncoding="utf-8"%>
<%@ include file="/common/public.jsp"%>
<%@ include file="/common/head.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ESBMM</title>
  <link href="${ctx}/resources/css/layout.css" type="text/css" rel="stylesheet">
  <script type="text/javascript">
$(function(){
	  init();
});
function saveData(url) {
	var form = $("#myform").Validform();
	if (form.check()) {
		$.dialog.setting.zIndex = 1997;
		window.top.$.messager.progress({
			text : '数据保存中....',
			interval : 300
		});
		$("#saveBtn").attr("disabled", "true");
	}

	$('#myform').form(
		'submit',
		{
			url : url,
			onSubmit : function() {
				var form = $("#myform").Validform();
				return form.check();
			},
			success : function(data) {
				var windowapi = frameElement.api, W = windowapi.opener;
				var d;
				try {
					d = $.parseJSON(data);
					if (d.success == true) {
						window.top.$.messager.progress('close');
						windowapi.close();
						W.tip(d.msg);
						W.query();
					}else {
						window.top.$.messager.progress('close');
						$("#saveBtn").removeAttr("disabled");
						window.top.$.messager.alert('提示', d.msg);
						return;
					}
				} catch (ex) {
					var emsg = data.substring(data.indexOf('错误描述'), data.indexOf('错误信息'));
					window.top.$.messager.progress('close');
					$("#saveBtn").removeAttr("disabled");
					window.top.$.messager.alert('错误', emsg);
					return;
				}
			}
		});
}
</script>

 </head>
 <body style="overflow-y: hidden" scroll="no">
 <table width="100%" id=timeRemindAdd toolbar="#demoListtb"></table>
 	<form id="myform" name="myform" method="post" >
  	<c:if test="${messageRemindRule.pkId!= null}">
        <input id="pkId" name="pkId" type="hidden" value="${messageRemindRule.pkId}">
    </c:if>
  	<table style="width:100%;" cellpadding="0" cellspacing="1" class="formtable">
	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		  	消息提醒规则名称:
    		 </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" readonly="readonly" id="messageRemindRuleName" onkeypress="notNull(this);" name="messageRemindRuleName" value="${messageRemindRule.messageRemindRuleName}" datatype="*3-30" errormsg="请输入3-30位内的字符！"  nullmsg="请输入活动名称！"/>
    		 </td>
 
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	预提醒时间:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="days" onkeypress="notNull(this);" name="days"  value="${messageRemindRule.days}" datatype="n1-9" errormsg="请输入1-9位内的数字！"  nullmsg="请输入1-9位内的数字！" /> 天<span style="color: red;">*</span>
    		</td>
    	</tr>
    	
    	<tr>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	提醒对象:
    		  </label>
    		</td>
    		<td class="value" width="35%" >
    			<input class="inputxt" id="remindObject" onkeypress="notNull(this);" name="remindObject"  value="${messageRemindRule.remindObject}" readonly="readonly"/> 
    		</td>
    		<td align="right" width="15%" nowrap>
    		  <label class="Validform_label">
    		 	消息模板:
    		  </label>
    		</td>
    		<td class="value" width="35%">
	    		<textarea class="inputxt" id="messageMould" MaxLength="400"
							onkeypress="notNull(this);" errormsg="不能超过400个字符" nullmsg="请输入活动内容！"
							datatype="*1-400" name="messageMould" cols="45" rows="5" 
							style="width: 260px; height: 100px;resize:none;">${messageRemindRule.messageMould}</textarea>
				<span style="color: red;">*</span>
			</td> 
    	</tr>
		
     <tr>
     <td align="center" colspan="8">
        <span class="orangebtnhover">
        <a id="saveBtn" class="easyui-linkbutton dialogbtn orangebtn" onclick="saveData(' ${ctx}/timeRemindRuleController.do?saveUpdate=key')" >保存</a></span>
		<span class="bluebtnhover"><a href="javascript:void(0)" id="restBtn" class="easyui-linkbutton dialogbtn bluebtn" onclick="closeWindow()">关闭</a></span>
   	 </td>
    </tr>
   </table>
 </form>
</body>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%> 
<%@ taglib prefix="fm" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:set var="ctx" value="${pageContext.request.scheme}${'://'}${pageContext.request.serverName}${':'}${pageContext.request.serverPort}${pageContext.request.contextPath}"></c:set>
<html>
<head>
<title></title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8">
	<!-- jquery begin -->
   	<script src="${ctx}/resources/js/jquery/1.11.1/jquery.min.js"></script>
   	<%-- <script src="${ctx}/resources/js/jquery/jquery.validate.min.js"></script> --%>
   	<%-- <script src="${ctx}/resources/js/jquery/jquery-ui.min.js"></script> --%>
	<%-- <script src="${ctx}/resources/js/jquery/jquery.format.js"></script> --%>
	<!-- jquery end -->
	
	<!-- easyui 1.4 begin -->
	<script src="${ctx}/resources/js/easyui/1.4/jquery.easyui.min.js"></script>
	<script src="${ctx}/resources/js/easyui/1.4/locale/easyui-lang-zh_CN.js"></script>
	<link rel="stylesheet" href="${ctx}/resources/js/easyui/1.4/themes/default/easyui.css" />
	<link rel="stylesheet" href="${ctx}/resources/js/easyui/1.4/themes/default/linkbutton.css" />
	<link rel="stylesheet" href="${ctx}/resources/js/easyui/1.4/themes/icon.css" />
	<link rel="stylesheet" href="${ctx}/resources/js/easyui/1.4/themes/color.css" />
	<!-- easyui 1.4 end -->
	
	<!-- easyui 1.4.2 begin -->
	<%--<script type="text/javascript" src="${ctx }/resources/js/easyui/jquery-easyui-1.4.2/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="${ctx }/resources/js/easyui/jquery-easyui-1.4.2/locale/easyui-lang-zh_CN.js"></script>
	<link rel="stylesheet" href="${ctx}/resources/js/easyui/jquery-easyui-1.4.2/themes/default/easyui.css" />
	<link rel="stylesheet" href="${ctx}/resources/js/easyui/jquery-easyui-1.4.2/themes/icon.css" />
	<link rel="stylesheet" href="${ctx}/resources/js/easyui/jquery-easyui-1.4.2/themes/color.css" /> --%>
	<!-- easyui 1.4.2 end -->
	
	
	
	<!-- Validform begin -->
	<%-- <script src="${ctx}/resources/js/Validform/js/Validform_v5.3.2_min.js"></script>
	<script src="${ctx}/resources/js/Validform/js/datatype.js"></script>
	<link rel="stylesheet" href="${ctx}/resources/js/Validform/css/style.css" />
	<link rel="stylesheet" href="${ctx}/resources/js/Validform/css/tablefrom.css" /> --%>
	<!-- Validform end -->

	
	<!-- zTree begin -->
	<%-- <script src="${ctx}/resources/js/zTree/jquery.ztree.core-3.5.js"></script>
	<script src="${ctx}/resources/js/zTree/jquery.ztree.excheck-3.5.js"></script>
	<script src="${ctx}/resources/js/zTree/jquery.ztree.exedit-3.5.js"></script>
	<link rel="stylesheet" href="${ctx}/resources/js/zTree/zTreeStyle.css"/> --%>
	<!-- zTree end -->

	<%-- <!-- my97 begin -->
	<script src="${ctx}/resources/js/My97DatePicker/WdatePicker.js"></script>
	<!-- my97 end --> --%>
	
	<!-- plupload begin -->
       <%-- <script type="text/javascript" src="${ctx}/resources/js/plupload/js/plupload.full.js"></script>
       <script type="text/javascript" src="${ctx}/resources/js/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"></script>
       <script type="text/javascript" src="${ctx}/resources/js/plupload/js/i18n/cn.js"></script>
	<link rel="stylesheet" href="${ctx}/resources/js/plupload/js/jquery.plupload.queue/css/jquery.plupload.queue.css" type="text/css"></link> --%>
	<!-- plupload end -->
	
	
	<script type="text/javascript">
		var ctx = '${ctx}';
		//æ¥ææ ¼å¼è½¬æ¢
		function formattimed(val) {
			/*
			 *modiy by houwei 2014 11 30   ä¸å¯¹ç©ºå¼è¿è¡æ ¼å¼å
			*/
			if (val == null || val == '') 
				return val;
			/***************************/
			var year=parseInt(val.year)+1900;
			var month=(parseInt(val.month)+1);
			month=month>9?month:('0'+month);
			var date=parseInt(val.date);
			date=date>9?date:('0'+date);
			var hours=parseInt(val.hours);
			hours=hours>9?hours:('0'+hours);
			var minutes=parseInt(val.minutes);
			minutes=minutes>9?minutes:('0'+minutes);
			var seconds=parseInt(val.seconds);
			seconds=seconds>9?seconds:('0'+seconds);
			var time=year+'-'+month+'-'+date;
			  return time;
		}
		
		
		//æ¶é´æ ¼å¼åyyyy-MM-dd HH:mm:ss
		function formattimes(val) {
			if (val == null || val == '') 
				return val;
			var year = parseInt(val.year) + 1900;
			var month = (parseInt(val.month) + 1);
			month = month > 9 ? month : ('0' + month);
			var date = parseInt(val.date);
			date = date > 9 ? date : ('0' + date);
			var hours = parseInt(val.hours);
			hours = hours > 9 ? hours : ('0' + hours);
			var minutes = parseInt(val.minutes);
			minutes = minutes > 9 ? minutes : ('0' + minutes);
			var seconds = parseInt(val.seconds);
			seconds = seconds > 9 ? seconds : ('0' + seconds);
			var time = year + '-' + month + '-' + date + ' ' + hours + ':'
					+ minutes + ':' + seconds;
			return time;
		}
	
	</script>
	<style type="text/css">
		select {
		  /*Chrome和Firefox里面的边框是不一样的，所以复写了一下*/
		  border: solid 1px #000;
		
		  /*很关键：将默认的select选择框样式清除*/
		  appearance:none;
		  -moz-appearance:none;
		  -webkit-appearance:none;
		
		  /*在选择框的最右侧中间显示小箭头图片*/
		  background: url("http://ourjs.github.io/static/2015/arrow.png") no-repeat scroll right center transparent;
		
		
		  /*为下拉小箭头留出一点位置，避免被文字覆盖*/
		  padding-right: 14px;
		}
		
		
		/*清除ie的默认选择框样式清除，隐藏下拉箭头*/
		select::-ms-expand { display: none; }
	</style>
</head>
<body>

</body>
</html>
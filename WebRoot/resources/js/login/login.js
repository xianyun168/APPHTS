
//避免session超时出现页面出现嵌套情况
if (top != window)
		top.location.href = window.location.href;

$(function() {
	
	// 回车登录
	$(document).keydown(function(e) {
		if (e.keyCode == 13) {
			if (valid())
				checkUser();
		}
	});

	function valid() {
		if ($("#username").val() == "") {
			$("#loginMsg").text("账户名不能为空");
			return false;
		} else {
			$("#loginMsg").text("");
		}

		if ($("#password").val() == "") {
			$("#loginMsg").text("密码不能为空");
			return false;
		} else {
			$("#loginMsg").text("");
		}
		return true;
	}

	$("#login_btn").click(function() {
		if (valid())
			checkUser();
	});

	function checkUser() {
		$.ajax({
			url : ctx + '/loginController.do',
			data : $("#formLogin").serialize(),
			success : function(data) {
				var d = $.parseJSON(data);
				if (d.success) {
					window.location.href = ctx + "/loginController/main.do";
				} else {
					$("#loginMsg").text(d.msg);
				}
			}
		});
	}
});
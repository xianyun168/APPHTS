package com.ciitc.webServices.webService;
import javax.jws.WebService;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.beans.factory.annotation.Autowired;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.service.AppUserInforService;
@WebService
public class KF_LoginServiceImpl implements KF_LoginService {

	@Autowired(required = false)
	private AppUserInforService<AppUserInfor> appUserInforService;
	/* 
	 * 登陆接口
	 */
	@Override
	public String Login(String phoneNumber, String verifiCode) {
		String state = "";
		AppUserInfor appUserInfor = null;
		try {
			appUserInfor = appUserInforService.queryByPhone(phoneNumber);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(null != appUserInfor&&null != appUserInfor.getRemark1()){
			if(verifiCode.equals(appUserInfor.getRemark1())){
				state = "00";
			}else{
				state = "01";
			}
			appUserInfor.setRemark1("");
			try {
				appUserInforService.update1(appUserInfor);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return state;
	}
	/* 
	 * 发送验证码
	 */
	@Override
	public String sandSMS(String PhoneNumber) {
		String state = "";
		AppUserInfor appUserInfor=null;
		AppUserInfor appUserInfor1=null;
		//FontProcessorWs  要调的短信平台的接口  url接口的地址
		//FontProcessorWs service = CxfUtil.getService(FontProcessorWs.class, url);
		// 设置自动超时判断
		//Client proxy = ClientProxy.getClient(service);
		//HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
		HTTPClientPolicy policy = new HTTPClientPolicy();
		policy.setConnectionTimeout(10000); // 6000毫秒
		policy.setReceiveTimeout(10000); // 6000毫秒
		//.setClient(policy);
		// 调用接口中的方法   ，pdf参数
		//String response = service.getResult(PhoneNumber);
		//Log.getLogger().info("电子保单平台调用保单登记接口返回报文为" + response);
		
	/*	if(response == "成功"){
			
			if(appUserInfor1!=null){
				appUserInfor1 = appUserInforService.queryByPhone(PhoneNumber);
				appUserInfor1.setRemark1("短信平台传过来的验证码");
				appUserInforService.update(appUserInfor1);
			}else{
				//随机获取uuid
				//appUserInfor.setPkId(UUID);
				appUserInfor.setRemark1("短信平台传过来的验证码");
				appUserInforService.insertInfor(appUserInfor);
			}
		}*/
		return state;
	}
	/* 
	 * 注册
	 */
	@Override
	public String Regidter(String PhoneNumber, String VerifiCode) {
		String state = "";
		AppUserInfor appUserInforPhone = null;
		if (PhoneNumber==null||"".equals(PhoneNumber)||"".equals(PhoneNumber.trim())) {
			state = "01";
		}else if(VerifiCode==null||"".equals(VerifiCode)||"".equals(VerifiCode.trim())) {
			state = "01";
		}else {
			appUserInforPhone =	appUserInforService.queryByPhone(PhoneNumber);
			if(appUserInforPhone != null){
				appUserInforPhone.setUserpwd(VerifiCode);
				appUserInforPhone.setPhoneNumber(PhoneNumber);
				appUserInforService.updateRegistration(appUserInforPhone);
				state = "00";
			}else{
				state = "01";
			}
		}
		return state;
	}
	/* 
	 * 实名制注册
	 */
	@Override
	public String realNameRegistration(String Name, String IDCardType,
			String IDcardNumber, String Address, String Region,
			String PhoneNumber) {
		String state="";
		AppUserInfor appUserInforPhone = null;
		if(Name==null||"".equals(Name)||"".equals(Name.trim())){
			state = "01";
		}else if(IDCardType==null||"".equals(IDCardType)||"".equals(IDCardType.trim())){
			state = "01";
		}else if(IDcardNumber==null||"".equals(IDcardNumber)||"".equals(IDcardNumber.trim())){
			state = "01";
		}else if(Address==null||"".equals(Address)||"".equals(Address.trim())){
			state = "01";
		}else if(Region==null||"".equals(Region)||"".equals(Region.trim())){
			state = "01";
		}else if(PhoneNumber==null||"".equals(PhoneNumber)||"".equals(PhoneNumber.trim())){
			state = "01";
		}else{
			appUserInforPhone =	appUserInforService.queryByPhone(PhoneNumber);
			if(appUserInforPhone != null){
				appUserInforPhone.setUserName(Name);
				appUserInforPhone.setIdcardNumber(IDcardNumber);
				appUserInforPhone.setIdcardType(IDCardType);
				appUserInforPhone.setCustomerAddress(Address);
				appUserInforPhone.setRegion(Region);
				appUserInforPhone.setPhoneNumber(PhoneNumber);
				appUserInforService.updateRegistration(appUserInforPhone);
				state = "00";
			}else{
				state = "01";
			}
		}
		return state;
	}

}

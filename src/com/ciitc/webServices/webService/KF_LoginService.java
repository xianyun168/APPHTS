package com.ciitc.webServices.webService;

import javax.jws.WebService;


@WebService
public interface KF_LoginService {
	
    //登陆
	public String Login(String phoneNumber,String verifiCode);
	//发送验证码
	public String sandSMS(String PhoneNumber);
	//注册
	public String Regidter(String PhoneNumber,String VerifiCode);
	//实名制注册
	public String realNameRegistration(String Name,String IDCardType,String IDcardNumber,String Address,String Region,String PhoneNumber);
}

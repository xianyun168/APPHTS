package com.ciitc.webServices.webService;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;

import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.service.AppUserInforService;
@WebService
public class CRM_LoginServiceImpl implements CRM_LoginService {

	@Autowired(required = false)
	private AppUserInforService<AppUserInfor> appUserInforService;
	@Override
	public String Login(String phoneNumber, String password) {
		String state="";
		AppUserInfor appUserInfor = null;
		try {
			appUserInfor = appUserInforService.queryByPhone(phoneNumber);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(null != appUserInfor&&null != appUserInfor.getRemark1()){
			if(password.equals(appUserInfor.getRemark1())){
				state = "00";
			}else{
				state = "01";
			}
			appUserInfor.setRemark1("");
			try {
				appUserInforService.update1(appUserInfor);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return state;
		
	}

}

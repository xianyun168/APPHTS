package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.PublishNewsDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.PublishNewsInfo;
import com.ciitc.webServices.util.Log;

@Controller
@RequestMapping("/kfCrmFirstNewsController.do")
public class KF_CRM_FirstNewsContorller extends BaseController{
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");	
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private PublishNewsDao<PublishNewsInfo> publishNewsDao = (PublishNewsDao) SpringContextUtil.getBean("publishNewsDao");
	
	/*
	 * 新闻列表查询、详情接口
	 */
	@RequestMapping(value = "firstNewsQueryList",method=RequestMethod.GET)
	@ResponseBody
	public String getFirstNewsQueryList(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		String pkId = request.getParameter("pkId");
		if(json1.has("pkId")) {
			pkId = json1.getString("pkId");
		}
		String pkid = esbmmCodeTypeDao.queryByTypeName("host").get(0).getPkId();
        String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
		if(null != pkId && !"".equals(pkId)) {
			//根据pkId查询详情
			PublishNewsInfo publishNewsInfo =  publishNewsDao.selectByPrimaryKey(pkId);
			if(null != publishNewsInfo) {
				publishNewsInfo.setNewsImageInternet(fwqdkh+publishNewsInfo.getNewsImageInternet());
				publishNewsInfo.setRemark3(sdf.format(publishNewsInfo.getNewsPublishConfig()));
				map.put("publishNewsInfo", publishNewsInfo);
				state = "00,查询新闻头条详情成功";
				Log.getLogger().info("查看新闻新闻头条详情");		
			}else {
				state = "01,新闻头条无查询结果";
				Log.getLogger().info("新闻头条无查询结果");
			}
		}else {
			try {
				//查询新闻类型为新闻头条的所有新闻数据列表
				List<PublishNewsInfo> publishNewsInfoList2 =  publishNewsDao.queryByListj2();
				if(publishNewsInfoList2.size() > 0) {
					for (PublishNewsInfo publishNewsInfo2 : publishNewsInfoList2) {
						publishNewsInfo2.setNewsImageInternet(fwqdkh+publishNewsInfo2.getNewsImageInternet());
						publishNewsInfo2.setRemark3(sdf.format(publishNewsInfo2.getNewsPublishConfig()));
					}
					state = "00,查询新闻头条成功";
					Log.getLogger().info("查看新闻头条列表");
					map.put("publishNewsInfoList2", publishNewsInfoList2);
				}else {
					state = "01,新闻头条无查询结果";
					Log.getLogger().info("新闻头条无查询结果");
				}
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,查询异常";
			}
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

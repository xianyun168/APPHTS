package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.PublishNewsDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.PublishNewsInfo;
import com.ciitc.webServices.util.Log;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfCrmPublishNewsController.do")
public class KF_CRM_PublishNewsContorller extends BaseController{
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private PublishNewsDao<PublishNewsInfo> publishNewsDao = (PublishNewsDao) SpringContextUtil.getBean("publishNewsDao");
	
	/*
	 * 新闻轮播列表查询、详情接口
	 */
	@RequestMapping(value = "newsListQuery",method=RequestMethod.GET)
	@ResponseBody
	public String getNewsListQuery(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		String pkId = request.getParameter("pkId");
		if(json1.has("pkId")) {
			pkId = json1.getString("pkId");
		}	
		if(null != pkId && !"".equals(pkId)) {
			//根据pkId查询详情
			PublishNewsInfo publishNewsInfolb = publishNewsDao.selectByPrimaryKeylb(pkId);//轮播图publishNewsInfolb.getNewsImageInternet()//内容里取pp.getNewsImageInternet()
			if(null != publishNewsInfolb) {
				String pkid = esbmmCodeTypeDao.queryByTypeName("host").get(0).getPkId();
	            String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
	            publishNewsInfolb.setNewsImageInternet(fwqdkh+publishNewsInfolb.getNewsImageInternet());
				//publishNewsInfolb.setRemark3(sdf.format(publishNewsInfolb.getNewsPublishConfiglb()));
				PublishNewsInfo pp =publishNewsDao.selectByPrimaryKey(publishNewsInfolb.getNewsRelationId());
	            pp.setNewsImageInternet(fwqdkh+pp.getNewsImageInternet());
	            String remark2=publishNewsInfolb.getRemark2();
	            if(!"是".equals(remark2)){
					pp.setRemark2(publishNewsInfolb.getNewsCarousel());
					pp.setRemark3(sdf.format(publishNewsInfolb.getNewsPublishConfiglb()));
				}
				pp.setNewsTitle(publishNewsInfolb.getNewsTitle());
				//pp.setNewsImage(publishNewsInfolb.getNewsImageInternet());
				map.put("pp", pp);
				state = "00,查询新闻轮播图详情成功";
				Log.getLogger().info("查看轮播图详情");		
			}else {
				state = "01,新闻轮播图无查询结果";
				Log.getLogger().info("轮播图无查询结果");
			}
			
		}else {
			try {
				//查询新闻类型为轮播新闻的所有新闻数据列表
				List<PublishNewsInfo> publishNewsInfoListlb =  publishNewsDao.queryByListjlb();
				if(publishNewsInfoListlb.size()>0){
					for (PublishNewsInfo publishNewsInfolb : publishNewsInfoListlb) {
						String remark2=publishNewsInfolb.getRemark2();
						if(!"是".equals(remark2)){
							publishNewsInfolb.setRemark3(sdf.format(publishNewsInfolb.getNewsPublishConfiglb()));
						}
						String pkid = esbmmCodeTypeDao.queryByTypeName("host").get(0).getPkId();
			            String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
			            publishNewsInfolb.setNewsImageInternet(fwqdkh+publishNewsInfolb.getNewsImageInternet());
					}
					state = "00,查询轮播新闻列表成功";
					Log.getLogger().info("查看轮播新闻列表");
					map.put("publishNewsInfoListlb", publishNewsInfoListlb);
				}else {
					state = "01,轮播新闻无查询结果";
					Log.getLogger().info("轮播新闻无查询结果");
				}
				
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,查询异常";
			}
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

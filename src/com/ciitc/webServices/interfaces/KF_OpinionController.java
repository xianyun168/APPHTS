package com.ciitc.webServices.interfaces;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.OpinionDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.Opinion;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfOpinionController")
public class KF_OpinionController extends BaseController {
	private OpinionDao opinionDao = (OpinionDao) SpringContextUtil.getBean("opinionDao");
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	/*
	 * 意见建议接口
	 */
	@RequestMapping(value = "saveOpinion",method=RequestMethod.GET)
	@ResponseBody
	public String saveOpinion(HttpServletRequest request,JSONObject json) {
		//生成一个uuid字符串
		String uuid = UUID.randomUUID().toString();
		String opinion = "";
		String userId = "";
		//创建map集合
		Map<String, Object> map = new HashMap<String, Object>();
		//创建opinion对象
		Opinion opinions = new Opinion();
		String state = null;
		//判断json字符串里是否包含opinion和userId
		if(json.has("opinion") && json.has("userId")) {
			opinion = json.getString("opinion");
			userId = json.getString("userId");
		}else {
			state = "01,必传字段不能为空";
			map.put("state", state);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if((null != opinion && !"".equals(opinion)) && (null != userId && !"".equals(userId))) {
			try {
				AppUserInfor appUserInfor = appUserInforDao.selectByPrimaryKey(userId);
				if(null != appUserInfor) {
					String userName="";
					if(!"".equals(appUserInfor.getUserName())) {
						userName = appUserInfor.getUserName();
					}else {
						userName = appUserInfor.getPhoneNumber();
					}
					String phoneNumber  = appUserInfor.getPhoneNumber();
					opinions.setPhoneNumber(phoneNumber);
					opinions.setRemark1(userName);
				}else {
					state = "01,没找到此用户";
					map.put("state", state);
					String jsons = JSONArray.fromObject(map).toString();
					return jsons;
				}
				opinions.setOpinion(opinion);
				opinions.setUserId(userId);
				opinions.setFlag("00");
				opinions.setPkId(uuid);
				opinions.setSubmitTime(new Date());
				opinionDao.insertSelective(opinions);
				state = "00,提交成功";
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,提交失败";
			}
		}else {
			state = "01,必传字段不能为空";
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

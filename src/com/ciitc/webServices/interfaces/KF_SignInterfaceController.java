package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.ActivityBasicInforDao;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.dao.HTMessageInfoDao;
import com.ciitc.webServices.dao.SignMessageQueryDao;
import com.ciitc.webServices.model.ActivityBasicInfor;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.HTMessageInfo;
import com.ciitc.webServices.model.SignMessageQuery;
import com.ciitc.webServices.util.CrmFunctionUtil;
import com.ciitc.webServices.util.Log;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfSignInterfaceController")
public class KF_SignInterfaceController extends BaseController{
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao =  (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private SignMessageQueryDao signMessageQueryDao = (SignMessageQueryDao) SpringContextUtil.getBean("signMessageQueryDao");
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	/*private FinancialInfoDao financialInfoDao = (FinancialInfoDao) SpringContextUtil.getBean("financialInfoDao");*/
	private CPTfundbrokerDao cpTfundbrokerDao = (CPTfundbrokerDao) SpringContextUtil.getBean("CPTfundbrokerDao");
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private ActivityBasicInforDao activityBasicInforDao =  (ActivityBasicInforDao) SpringContextUtil.getBean("activityBasicInforDao");
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private HTMessageInfoDao hTMessageInfoDao =  (HTMessageInfoDao) SpringContextUtil.getBean("HTMessageInfoDao");
	
	@RequestMapping(value = "getSignQuery",method=RequestMethod.GET)
	@ResponseBody
	public String getSignQuery(HttpServletRequest request,JSONObject jsonParameter) {
		CrmFunctionUtil crmFunctionUtil = new CrmFunctionUtil();
		crmFunctionUtil.getFinancial();
		Map<String, Object> map = new HashMap<String, Object>();
		SignMessageQuery signMessageQuery = new SignMessageQuery();
		JSONObject json = JSONObject.fromObject(jsonParameter);
		AppUserInfor appUserInfor = null;
		CPTfundbroker financialInfo = null;
		ActivityBasicInfor activityBasicInfor = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String state = "";
		String pkId = "";
		String activityId = "";
		if(json.has("userId") && json.has("activityId")) {
			pkId = json.getString("userId");
			 activityId = json.getString("activityId");
		}else {
			state = "01,报名失败";
			Log.getLogger().info("报名失败,用户ID或理财师ID为空,请核实后再请求");
			map.put("state", state);
			String json1 = JSONArray.fromObject(map).toString();
			return json1;
		}
		if(pkId != null && !"".equals(pkId)) {
			appUserInfor = appUserInforDao.selectByPrimaryKey(pkId);
			if(appUserInfor != null) {
				Log.getLogger().info("已查到用户信息");
				if(appUserInfor.getFinancialId() != null && !"".equals(appUserInfor.getFinancialId())) {
					financialInfo = cpTfundbrokerDao.selectByPrimaryKey(appUserInfor.getFinancialId());
					if(financialInfo != null) {
						Log.getLogger().info("已查到理财师信息");
						signMessageQuery.setFinancialName(financialInfo.getcBrokername());
						if(null != financialInfo.getcMobile() && !"".equals(financialInfo.getcMobile())) {
							signMessageQuery.setFinancialPhoneNumber(financialInfo.getcMobile());
						}else {
							signMessageQuery.setFinancialPhoneNumber("-");
						}
						Log.getLogger().info("理财师信息已保存");
					}else {
						state = "01,理财师信息查询无结果，报名失败，请核实";
						Log.getLogger().info("理财师信息查询无结果，报名失败，请核实");
						map.put("state", state);
						String json1 = JSONArray.fromObject(map).toString();
						return json1;
					}
				}else {
					state = "01,理财师ID为空或不合法，报名失败，请核实";
					Log.getLogger().info("理财师ID为空或不合法，报名失败，请核实");
					map.put("state", state);
					String json1 = JSONArray.fromObject(map).toString();
					return json1;
				}
				if(appUserInfor.getUserName() != null && !"".equals(appUserInfor.getUserName())){
					signMessageQuery.setUserName(appUserInfor.getUserName());
					signMessageQuery.setUserPhoneNumber(appUserInfor.getPhoneNumber());
					Log.getLogger().info("用户信息已保存");
				}else {
					state = "01,用户名为空或不合法，报名失败，请核实";
					Log.getLogger().info("用户名为空或不合法，报名失败，请核实");
					map.put("state", state);
					String json1 = JSONArray.fromObject(map).toString();
					return json1;
				}
			}else {
				state = "01,用户信息查询无结果，报名失败，请核实";
				Log.getLogger().info("用户信息查询无结果，报名失败，请核实");
				map.put("state", state);
				String json1 = JSONArray.fromObject(map).toString();
				return json1;
			}
		}else {
			state = "01,用户ID不合法，报名失败，请核实";
			Log.getLogger().info("用户ID不合法，报名失败，请核实");
			map.put("state", state);
			String json1 = JSONArray.fromObject(map).toString();
			return json1;
		}
		if(activityId != null && !"".equals(activityId)) {
			activityBasicInfor = activityBasicInforDao.selectByPrimaryKey(activityId);
			if(activityBasicInfor != null) {
				signMessageQuery.setActivityId(activityId);
				Log.getLogger().info("活动ID已保存");
			}else {
				 state = "01,没有查询到相关活动信息，报名失败";
				 Log.getLogger().info("没有查询到相关活动信息，报名失败");
				 map.put("state", state);
				 String json1 = JSONArray.fromObject(map).toString();
				 return json1;
			}
		}else {
			state = "01,活动ID不合法,报名失败,请核实";
			Log.getLogger().info("活动ID不合法,报名失败,请核实");
			map.put("state", state);
			String json1 = JSONArray.fromObject(map).toString();
			return json1;
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("activityId", signMessageQuery.getActivityId());
		paramMap.put("userId", pkId);
		@SuppressWarnings("unchecked")
		List<SignMessageQuery> listUserInfo = signMessageQueryDao.queryByUserInfo(paramMap);
		if(listUserInfo.size()>0) {
			Log.getLogger().info("报名人员信息已存在");
			state = "01,不能重复报名";
			Log.getLogger().info("不能重复报名，报名失败");
			map.put("state", state);
			String json1 = JSONArray.fromObject(map).toString();
			return json1;
		}else {
			signMessageQuery.setPkId(UUID.randomUUID().toString());
			signMessageQuery.setSignDate(new Date());
			signMessageQuery.setRemark1("01");
			signMessageQuery.setRemark2(pkId);//用户ID
			signMessageQueryDao.insert(signMessageQuery);
			//-------------------------------------
			HTMessageInfo hTMessageInfo = new HTMessageInfo();
			hTMessageInfo.setName(appUserInfor.getUserName());
			hTMessageInfo.setPhone(appUserInfor.getPhoneNumber());
			hTMessageInfo.setState("未读");
			hTMessageInfo.setFlag(0);
			hTMessageInfo.setRemark1(sdf.format(new Date()));
			hTMessageInfo.setPkId(UUID.randomUUID().toString()); 
			hTMessageInfo.setMessage("用户"+appUserInfor.getUserName()+"报名"+
					activityBasicInfor.getActivityName()+"活动成功,请通知理财师"+financialInfo.getcBrokername());
			hTMessageInfoDao.insertSelective(hTMessageInfo);
			//--------------------------------------------
			Log.getLogger().info("报名信息已保存");
			state = "00,报名成功";
			Log.getLogger().info("报名成功啦");
			map.put("state", state);
			String json1 = JSONArray.fromObject(map).toString();
			return json1;
		}
	}
}

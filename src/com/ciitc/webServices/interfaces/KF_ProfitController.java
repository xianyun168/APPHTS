package com.ciitc.webServices.interfaces;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import cn.com.ins.sinosoft.framework.base.util.StringUtil;
import com.ciitc.webServices.model.CPTtrustcontractReqV2;
import com.ciitc.webServices.util.MyDateUtils;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTtrustcontractReqDao;
import com.ciitc.webServices.dao.CPcfResultDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTtrustcontractReq;
import com.ciitc.webServices.model.CPcfResult;
import com.ciitc.webServices.util.CrmFunctionUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

public class KF_ProfitController {

	Map<String, Object> map = new HashMap<String, Object>();
	@SuppressWarnings("rawtypes")
	private CPTtrustcontractReqDao CPTtrustcontractReqDao = (CPTtrustcontractReqDao) SpringContextUtil
			.getBean("CPTtrustcontractReqDao");
	@SuppressWarnings("rawtypes")
	private CPcfResultDao CPcfResultDao = (CPcfResultDao) SpringContextUtil.getBean("CPcfResultDao");
	@SuppressWarnings("rawtypes")
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");

	/*
	 * 持仓产品，累计收益，持仓收益,持仓金额查询
	 */
	@SuppressWarnings("unchecked")
	public @ResponseBody String getPosition(HttpServletRequest request, JSONObject json)
			 {
		String state = "";
		double positionMoney = 0.00;// 持仓总金额
		double chizongMoney = 0.00;// 持仓总收益
		double leijiMoney = 0.00;// 累计收益

		 List<CPTtrustcontractReqV2> cPTtrustcontractReq = new ArrayList<CPTtrustcontractReqV2>();// 根据手机号查询产品信息
		 List<CPTtrustcontractReqV2> chicanglist = new ArrayList<CPTtrustcontractReqV2>();// 查询持仓产品信息
		 List<CPTtrustcontractReq> suoyoulist = new ArrayList<CPTtrustcontractReq>();// 查询持仓收益信息
		 List<CPTtrustcontractReq> leijilist = new ArrayList<CPTtrustcontractReq>();// 查询累计收益信息
		// 请求Id
		String pkId = json.getString("pkId");
		// 根据id查询客户Id
		AppUserInfor khId = appUserInforDao.querykhId(pkId);

		try {
			String customerId=khId.getCustomerId();
			cPTtrustcontractReq=CPTtrustcontractReqDao.getMyDeShouYiList(customerId);
			// 计算累计收益
			leijiMoney=CPTtrustcontractReqDao.getLeiJiShouYi(customerId);
			// 计算持仓总金额
			for (CPTtrustcontractReqV2 cpTtrustcontractReq2 : cPTtrustcontractReq) {
				if(!"已到期".equals(cpTtrustcontractReq2.getcStatus())) {
					positionMoney += Double.parseDouble(cpTtrustcontractReq2.getMoney());
				}
			}
			// 计算持仓总收益
			chizongMoney=CPTtrustcontractReqDao.getChiCangShouYi(customerId);
			for (CPTtrustcontractReqV2 cpt : cPTtrustcontractReq) {
				if("已到期".equals(cpt.getcStatus())){
					cpt.setDaoqiTian(cpt.getcStatus());
				}
				cpt.setZhuangtai(cpt.getcStatus());
				cpt.setProserialno(cpt.getlProserialno()+"");
				cpt.setdCreatedate(cpt.getDefaultDate());
				if(cpt.getdContractsigndate().length()>10){
					cpt.setQuerenDate(cpt.getdContractsigndate().substring(0,10));
				}else{
					cpt.setQuerenDate(cpt.getdContractsigndate());
				}

				chicanglist.add(cpt);
			}

			state = "00";
		} catch (Exception e1) {
			e1.printStackTrace();
			state = "01";
		}

		DecimalFormat df = new DecimalFormat("0.00");
		map.put("state", state);
		map.put("chicanglist", chicanglist);
		map.put("leijiMoney", df.format(leijiMoney));
		map.put("chizongMoney", df.format(chizongMoney));
		map.put("positionMoney", df.format(positionMoney));
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	//原来的sql，废弃

	public @ResponseBody String getPositionOLd(HttpServletRequest request, JSONObject json)
			throws UnsupportedEncodingException, IOException {
		CrmFunctionUtil util = new CrmFunctionUtil();
		//util.updateTsaleProjectInfo();
		//util.getProfit();// 与crm数据库同步收益
		util.getHTInfo();// 与crm数据库同步合同
		String state = "";
		double positionMoney = 0.00;// 持仓总金额
		double chizongMoney = 0.00;// 持仓总收益
		double leijiMoney = 0.00;// 累计收益
		List<CPTtrustcontractReq> cPTtrustcontractReq = new ArrayList<CPTtrustcontractReq>();// 根据手机号查询产品信息
		List<CPTtrustcontractReq> chicanglist = new ArrayList<CPTtrustcontractReq>();// 查询持仓产品信息
		List<CPTtrustcontractReq> suoyoulist = new ArrayList<CPTtrustcontractReq>();// 查询持仓收益信息
		List<CPTtrustcontractReq> leijilist = new ArrayList<CPTtrustcontractReq>();// 查询累计收益信息
		// 请求Id
		String pkId = json.getString("pkId");
		// 根据id查询客户Id
		AppUserInfor khId = appUserInforDao.querykhId(pkId);

		try {
			// 根据用户id查询购买产品合同信息
			cPTtrustcontractReq = CPTtrustcontractReqDao.queryChiCangList1(khId.getCustomerId());
			// 计算累计收益
			for (CPTtrustcontractReq cPTtrustcontractReq2 : cPTtrustcontractReq) {
				suoyoulist = CPTtrustcontractReqDao.queryChiCangMoney(cPTtrustcontractReq2.getcTrustcontractid());
				for (CPTtrustcontractReq cpTtrustcontractReq2 : suoyoulist) {
					leijiMoney += cpTtrustcontractReq2.getAmount();//现金流
				}
			}
			for (CPTtrustcontractReq cpt : cPTtrustcontractReq) {
				/*
				 * if (cpt.getActualDate() == null) { // 如果到期时间为空，到期天数为-
				 * cpt.setDaoqiTian("-"); chicanglist.add(cpt); }
				 */
				if (cpt.getActualDate() != null && !"".equals(cpt.getActualDate())) {

					// 根据当前时间与默认到期时间相减得到到期天数
					Date time = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					String a = dateFormat.format(time);
					Date newDate = dateFormat.parse(a);

					Date oldTime = cpt.getActualDate();
					String b = dateFormat.format(oldTime);
					Date oldDate = dateFormat.parse(b);
					if (newDate.getTime() <= oldDate.getTime()) {

						Calendar cal = Calendar.getInstance();
						cal.setTime(newDate);
						long time1 = cal.getTimeInMillis();
						//System.out.println(time1);
						cal.setTime(oldDate);
						long time2 = cal.getTimeInMillis();
						//System.out.println(time2);
						long between_days = (time2 - time1) / (1000 * 3600 * 24);
						String days = String.valueOf(between_days);
						cpt.setDaoqiTian(days);
						cpt.setProserialno(cpt.getlProserialno().toString());
						chicanglist.add(cpt);
					}else {
						cpt.setDaoqiTian("已到期");
						cpt.setProserialno(cpt.getlProserialno().toString());
						chicanglist.add(cpt);
					}
				}
			}
			// 计算持仓总金额
			for (CPTtrustcontractReq cpTtrustcontractReq2 : chicanglist) {
				if(!"已到期".equals(cpTtrustcontractReq2.getDaoqiTian())) {
					DecimalFormat df = new DecimalFormat("0.00");
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					String qianshu = format.format(cpTtrustcontractReq2.getdContractsigndate());
					// 签署日期与份额确认日期相同
					cpTtrustcontractReq2.setQuerenDate(qianshu);
					cpTtrustcontractReq2.setQianshu(qianshu);
					cpTtrustcontractReq2.setZhuangtai("未到期");
					String money = df.format(cpTtrustcontractReq2.getfContractsignbalance());
					cpTtrustcontractReq2.setMoney(money);
					// 确认份额与购买金额相同
					int fene = (int) cpTtrustcontractReq2.getfContractsignbalance();
					cpTtrustcontractReq2.setFene(fene);
					positionMoney += cpTtrustcontractReq2.getfContractsignbalance();
				}
			}
			// 计算持仓总收益
			for (CPTtrustcontractReq cPTtrustcontractReq2 : chicanglist) {
				if(!"已到期".equals(cPTtrustcontractReq2.getDaoqiTian())) {
					leijilist = CPTtrustcontractReqDao.queryChiCangMoney(cPTtrustcontractReq2.getcTrustcontractid());
					for (CPTtrustcontractReq cpTtrustcontractReq3 : leijilist) {
						chizongMoney += cpTtrustcontractReq3.getAmount();
					}
				}
			}
			state = "00";
		} catch (Exception e1) {
			e1.printStackTrace();
			state = "01";
		}

		DecimalFormat df = new DecimalFormat("0.00");
		map.put("state", state);
		map.put("chicanglist", chicanglist);
		map.put("leijiMoney", df.format(leijiMoney));
		map.put("chizongMoney", df.format(chizongMoney));
		map.put("positionMoney", df.format(positionMoney));
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

	/*
	 * 交易记录查询
	 */
	@SuppressWarnings("unchecked")
	public @ResponseBody String getTransaction(HttpServletRequest request, JSONObject json)
			throws UnsupportedEncodingException, IOException {
		String state = "";
		String msg = "";
		List<CPTtrustcontractReq> mairuList1 = new ArrayList<CPTtrustcontractReq>();// 根据手机号查询买入产品信息列表
		List<CPTtrustcontractReq> daoqiList = new ArrayList<CPTtrustcontractReq>();// 查询到期赎回列表
		List<CPTtrustcontractReq> tiqianList = new ArrayList<CPTtrustcontractReq>();// 查询提前赎回列表
		List<CPTtrustcontractReq> mairuList = new ArrayList<CPTtrustcontractReq>();// 根据手机号查询买入产品信息列表

		// 请求Id
		String pkId = json.getString("pkId");
		// 根据id回去客户id
		AppUserInfor khId = appUserInforDao.querykhId(pkId);

		try {
			// 根据客户Id查询购买产品合同信息
			mairuList1 = CPTtrustcontractReqDao.queryChiCangList(khId.getCustomerId());
			for (CPTtrustcontractReq cpTtrustcontractReq : mairuList1) {
				DecimalFormat df = new DecimalFormat("0.00");
				String money = df.format(cpTtrustcontractReq.getfContractsignbalance());
				cpTtrustcontractReq.setMoney(money);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				String qianshu = format.format(cpTtrustcontractReq.getdContractsigndate());
				// 买入时间
				cpTtrustcontractReq.setQuerenDate(qianshu);
				mairuList.add(cpTtrustcontractReq);
			}
			//到期赎回
			daoqiList=CPTtrustcontractReqDao.queryDaoQiShuHui(khId.getCustomerId());
			for (CPTtrustcontractReq cpTtrustcontractReq : daoqiList) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date defaultDate=cpTtrustcontractReq.getDefaultDate();
				String daoqi=dateFormat.format(defaultDate);
				cpTtrustcontractReq.setTiqianDate(daoqi);
			}
			/*for (CPTtrustcontractReq cpTtrustcontractReq : mairuList1) {
				if (cpTtrustcontractReq.getActualDate() != null && !"".equals(cpTtrustcontractReq.getActualDate())) {
					DecimalFormat df = new DecimalFormat("0.00");
					String money = df.format(cpTtrustcontractReq.getfContractsignbalance());
					cpTtrustcontractReq.setMoney(money);
					// 根据当前时间与合同到期时间判断是否到期
					// Date newDate = new Date();
					// Date oldDate = cpTtrustcontractReq.getActualDate();
					
					Date time = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
					String a = dateFormat.format(time);
					Date newDate = dateFormat.parse(a);
					
					Date oldTime = cpTtrustcontractReq.getActualDate();
					String b = dateFormat.format(oldTime);
					Date oldDate = dateFormat.parse(b);
					if (newDate.getTime() >= oldDate.getTime()
							&& oldDate.getTime() == cpTtrustcontractReq.getDefaultDate().getTime()) {
						
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						// 将到期赎回的数据存入list
						String daoqi = format.format(cpTtrustcontractReq.getActualDate());
						cpTtrustcontractReq.setTiqianDate(daoqi);
						// 将已到期的数据存入list
						daoqiList.add(cpTtrustcontractReq);
						
					}
				}
			}*/
			//提前赎回
			tiqianList=CPTtrustcontractReqDao.queryTiQianShuHui(khId.getCustomerId());
			for (CPTtrustcontractReq cpTtrustcontractReq : tiqianList) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date defaultDate=cpTtrustcontractReq.getDefaultDate();
				String daoqi=dateFormat.format(defaultDate);
				cpTtrustcontractReq.setTiqianDate(daoqi);
			}
			/*for (CPTtrustcontractReq cpt : mairuList1) {
				DecimalFormat df = new DecimalFormat("0.00");
				String money = df.format(cpt.getfContractsignbalance());
				cpt.setMoney(money);
				Date newDate = new Date();
				// 根据实际赎回时间与默认赎回时间判断是否提前赎回状态
				if (cpt.getActualDate() != null && !"".equals(cpt.getActualDate())) {
					if (cpt.getActualDate().getTime() < cpt.getDefaultDate().getTime()
							&& newDate.getTime() >= cpt.getActualDate().getTime()) {
						SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						// 将提前赎回的数据存入list
						String tiqian = format.format(cpt.getActualDate());
						cpt.setTiqianDate(tiqian);
						tiqianList.add(cpt);
					}
				}
			}*/
			state = "00";
			msg = "查询成功";
			/*
			 * for (CPTtrustcontractReq cpTtrustcontractReq : daoqiList) {
			 * SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			 * String qianshu =
			 * format.format(cpTtrustcontractReq.getActualDate()); // 赎回时间
			 * cpTtrustcontractReq.setQuerenDate(qianshu); }
			 */
		} catch (Exception e1) {
			e1.printStackTrace();
			state = "01";
			msg = "系统异常";
		}
		map.put("state", state);
		map.put("daoqiList", daoqiList);
		map.put("mairuList", mairuList);
		map.put("tiqianList", tiqianList);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

	/*
	 * 产品历史累计收益
	 */
	@SuppressWarnings("unchecked")
	public @ResponseBody String getHistoryProfit(HttpServletRequest request, JSONObject json)
			throws UnsupportedEncodingException, IOException {
		String state = "";
		String msg = "";
		double leijiMoney = 0.00;
		List<CPTtrustcontractReq> cPTtrustcontractReq = new ArrayList<CPTtrustcontractReq>();// 根据手机号查询产品信息
		List<CPTtrustcontractReq> suoyoulist = new ArrayList<CPTtrustcontractReq>();// 根据手机号查询产品信息
		List<CPTtrustcontractReq> shouyilist = new ArrayList<CPTtrustcontractReq>();// 根据手机号查询收益列表
		// 获取请求id
		String pkId = json.getString("pkId");
		// 根据id查询客户iD
		AppUserInfor khId = appUserInforDao.querykhId(pkId);
		try {
			// 产品累计收益
			cPTtrustcontractReq = CPTtrustcontractReqDao.queryChiCangList(khId.getCustomerId());
			// 计算累计收益
			for (CPTtrustcontractReq cPTtrustcontractReq2 : cPTtrustcontractReq) {
				suoyoulist = CPTtrustcontractReqDao.queryChiCangMoney(cPTtrustcontractReq2.getcTrustcontractid());
				for (CPTtrustcontractReq cpTtrustcontractReq2 : suoyoulist) {
					leijiMoney += cpTtrustcontractReq2.getAmount();
				}
			}
			// 产品累计收益列表
			shouyilist = CPTtrustcontractReqDao.queryshoyiList(khId.getCustomerId());

			for (CPTtrustcontractReq cpTtrustcontractReq2 : shouyilist) {
				DecimalFormat df = new DecimalFormat("0.00");
				String money = df.format(cpTtrustcontractReq2.getAmount());
				cpTtrustcontractReq2.setMoney(money);
				if (cpTtrustcontractReq2.getActualDate() != null && !"".equals(cpTtrustcontractReq2.getActualDate())) {

					// 根据当前时间与实际到期时间判断是持仓收益还是到期收益
					Date newDate = new Date();
					Date oldDate = cpTtrustcontractReq2.getActualDate();
					if (newDate.getTime() <= oldDate.getTime()) {
						cpTtrustcontractReq2.setZhuangtai("持有资产累计收益");
					} else {
						cpTtrustcontractReq2.setZhuangtai("到期收益");
					}
				}
			}
			state = "00";
			msg = "查询成功";

		} catch (Exception e1) {
			e1.printStackTrace();
			state = "01";
			msg = "系统异常";
		}
		DecimalFormat df = new DecimalFormat("0.00");
		map.put("state", state);
		map.put("msg", msg);
		map.put("leijiMoney", df.format(leijiMoney));
		map.put("shouyilist", shouyilist);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

	/*
	 * 每期详细收益
	 */
	@SuppressWarnings("unchecked")
	public @ResponseBody String getProfitDetailed(HttpServletRequest request, JSONObject json)
			throws UnsupportedEncodingException, IOException {
		String msg = "";
		String state = "";
		double shouyi = 0.00;// 产品总收益
		List<CPcfResult> mingxiList = new ArrayList<CPcfResult>();// 查询每期收益明细
		String contractId = json.getString("contractId");
		try {
			// 详细收益
			mingxiList = CPcfResultDao.querymingxiList(contractId);
			for (CPcfResult cPcfResult : mingxiList) {
				DecimalFormat df = new DecimalFormat("0.00");
				String money = df.format(cPcfResult.getAmount());
				cPcfResult.setMoney(money);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				String shouyiDate = format.format(cPcfResult.getData_date());
				cPcfResult.setShouyiDate(shouyiDate);
				shouyi += cPcfResult.getAmount();
			}
			state = "00";
			msg = "查询成功";
		} catch (Exception e1) {
			e1.printStackTrace();
			state = "01";
			msg = "系统异常";
		}
		DecimalFormat df = new DecimalFormat("0.00");
		map.put("state", state);
		map.put("mingxiList", mingxiList);
		map.put("shouyi", df.format(shouyi));
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

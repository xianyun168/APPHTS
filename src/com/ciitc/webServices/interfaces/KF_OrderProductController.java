package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.ProductInfoDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.ProductInfo;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.CrmFunctionUtil;
import com.ciitc.webServices.util.KfcrmCustor;
import com.ciitc.webServices.util.SMSPublicMessageUntil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import cn.emay.util.SmsSingleRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
@Controller
@RequestMapping("/kfOrderProduct")
public class KF_OrderProductController extends BaseController {
	
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");
	
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;
	private CPTfundbrokerDao cpTfundbrokerDao = (CPTfundbrokerDao) SpringContextUtil.getBean("CPTfundbrokerDao");
	// 产品信息
	private ProductInfoDao productInfoDao = (ProductInfoDao) SpringContextUtil.getBean("productInfoDao");
	/*
	 * 客户预约产品，短信通知理财师
	 */
	@RequestMapping(value = "orderProductSMS", method = RequestMethod.GET)
	public @ResponseBody String orderProductSMS(HttpServletRequest request,
			JSONObject json) throws Exception {
		AppUserInfor appUserInfor =null;
		SmsSingleRequest smsSingleRequest = new SmsSingleRequest();
		Map<String, Object> map = new HashMap<String, Object>();
		String state = "";
		//接收到用户的id和要购买的产品的id
		String pkId = "";
		String productId = "";
		if(json.has("userId") && json.has("productId")) {
			pkId = json.getString("userId");
			productId = json.getString("productId");
			boolean isGzip = false;
			//AppId
			String appId = "";
			//密匙
			String secretKey = "";
			//接口地址
			String host = "";
			//加密算法
			String algorithm = "";
			//短信签名
			String sign = "";
			// --------------------------------------
			List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao.queryByTypeName("SMSConfiguration");
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
			List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
			for (EsbmmCode esbmmCode : listCode) {
				if (esbmmCode.getCodeName().equals("AppId")) {
					appId = esbmmCode.getCodeValue();// AppId
				}
				if (esbmmCode.getCodeName().equals("secretKey")) {
					secretKey = esbmmCode.getCodeValue();// 密匙
				}
				if (esbmmCode.getCodeName().equals("interfaceHost")) {
					host = esbmmCode.getCodeValue();// 接口地址
				}
				if (esbmmCode.getCodeName().equals("algorithm")) {
					algorithm = esbmmCode.getCodeValue();// 加密算法
				}
				if (esbmmCode.getCodeName().equals("SMSSignature")) {
					sign = esbmmCode.getCodeValue();// 短信签名
				}
			}
			//根据用户的id查询出理财师的id
			appUserInfor = appUserInforDao.queryByPkIdOne(pkId);
			//根据产品的id查询产品的信息
			ProductInfo productInfo = productInfoDao.selectByPrimaryKey(productId);
			if(null != appUserInfor && null != productInfo ) {
				//产品名称
				String productName = productInfo.getProductname();
				//客户的名称
				String userName = appUserInfor.getUserName();
				//客户的电话
				String userPhone = appUserInfor.getPhoneNumber();
				//理财师的id
				String financialId = appUserInfor.getFinancialId();
				//-------------------------------------------
				//调用存储过程，更新理财师表信息
				CrmFunctionUtil crmFunctionUtil = new CrmFunctionUtil();
				crmFunctionUtil.getFinancial();
				//--------------------------------------------
				//根据理财师的id查询理财师的姓名和电话
				CPTfundbroker financialInfo  = cpTfundbrokerDao.selectByPrimaryKey(financialId);
				if(null != financialInfo) {
					String financialName = financialInfo.getcBrokername();
					String phoneNumber = financialInfo.getcMobile();
					//预约的时间
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String date = sdf.format(new java.util.Date());
					//根据客户的电话查询所属公司
					KfcrmCustor kfcrmCustor = new KfcrmCustor();
					String companyName = kfcrmCustor.queryCompanyByPhone(userPhone);
					//编辑短信内容
					String signMessage = sign +"您的客户"+userName+"电话("+userPhone+"),所属分公司("+companyName+")在"+date+"预约了"+productName+"产品,请尽快与您的客户联系,了解客户详细需求。";
					smsSingleRequest.setContent(signMessage);
					//需要发送的手机号码
					//查询数据字典
					String pkid = esbmmCodeTypeDao.queryByTypeName("testPhone").get(0).getPkId();
		            String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
					smsSingleRequest.setMobile(fwqdkh);
					smsSingleRequest.setCustomSmsId("");
					smsSingleRequest.setExtendCode("");
					smsSingleRequest.setRequestTime(System.currentTimeMillis());
					smsSingleRequest.setRequestValidPeriod(30);
					smsSingleRequest.setTimerTime("");
					// 发送短信
					SMSPublicMessageUntil smsPublicMessageUntil = new SMSPublicMessageUntil();
					String falg = smsPublicMessageUntil.setSingleSms(smsSingleRequest,
							isGzip, appId, secretKey, host, algorithm);
					if (null != falg && falg.equals("成功")) {
						//短信发送成功
						state = "00";
					} else {
						//短信发送失败
						state = "01";
					}
				}else {
					//理财师信息为null
					state = "01";
				}
			}else {
				//根据userId查询的用户信息为null
				state = "01";
			}
		}else {
			//app传来的参数缺少
			state = "01";
		}
	map.put("state",state );
	String jsons = JSONArray.fromObject(map).toString();
	return jsons;
	}
}

package com.ciitc.webServices.interfaces;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CompanyInfoDao;
import com.ciitc.webServices.model.CompanyInfo;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfCompanyInfoController")
public class KF_CompanyInfoController extends BaseController {
	private CompanyInfoDao companyInfoDao = (CompanyInfoDao) SpringContextUtil.getBean("companyInfoDao");
	/*
	 * 公司基本信息接口
	 */
	@RequestMapping(value = "getCompanyInfo",method=RequestMethod.GET)
	@ResponseBody 
	public String getCompanyInfo(HttpServletRequest request,JSONObject json) {
		//创建map集合
		Map<String, Object> map = new HashMap<String, Object>();
		String state = null;
		try {
			//查询公司基本信息
			CompanyInfo companyInfo = companyInfoDao.selectByPrimaryKey();
			//判断查询结果是否为空，如果不为空则把对象存进map集合
			if(null != companyInfo) {
				map.put("companyInfo", companyInfo);
				state = "00,查询成功";
				
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			state = "01,查询失败";
		}
		map.put("state", state);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}


}

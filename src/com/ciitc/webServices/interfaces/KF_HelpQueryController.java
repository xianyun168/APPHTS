package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.HelpDocumentDao;
import com.ciitc.webServices.model.HelpDocument;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfHelpQueryController")
public class KF_HelpQueryController extends BaseController{
	 private HelpDocumentDao helpDocumentDao = (HelpDocumentDao) SpringContextUtil.getBean("helpDocumentDao");
	
	/*
	 * 帮助文档接口
	 */
	@RequestMapping(value = "getHelpQuery",method=RequestMethod.GET)
	@ResponseBody
	public String getHelpQuery(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String state = "";
		String title ="";
		String pkId ="";
		//判断json字符串里是否包含title和pkId
		if(json.has("title")) {
			title = json.getString("title");
		}
		if(json.has("pkId")) {
			pkId = json.getString("pkId");
		}		
		//判断pkId是否为空，如果不为空则查询帮助文档详情，如果为空则查询帮助文档列表
		if(null != pkId && !"".equals(pkId)) {
			//根据pkId查询帮助文档详细信息
			HelpDocument helpDocument = helpDocumentDao.selectByPrimaryKey(pkId);
			if(null != helpDocument) {
				//每查询一次帮助文档，则帮助文档的被访问次数加1(先加1返回给APP在去数据库加1)
				helpDocument.setCounts(helpDocument.getCounts()+1);
				helpDocument.setRemark1(sdf.format(helpDocument.getCreatedDate()));
				if(null != helpDocument.getReleaseDate()) {
					helpDocument.setRemark2(sdf.format(helpDocument.getReleaseDate()));
				}
				if(null != helpDocument.getUpdatedDate() ) {
					helpDocument.setRemark3(sdf.format(helpDocument.getUpdatedDate()));
				}
				map.put("helpDocument", helpDocument);
				state = "00,查询成功";
				HelpDocument helpDocument1 = new HelpDocument();
				helpDocument1.setPkId(helpDocument.getPkId());
				helpDocument1.setCounts(helpDocument.getCounts());
				helpDocumentDao.updateByKey(helpDocument1);
				
			}else {
				state = "01,无查询结果";
			}
			
		}else {
			try {
				//查询帮助文档列表(如果title不为空则按照title模糊查询)
				List<HelpDocument> helpDocumentList =  helpDocumentDao.queryByListAll(title);
				if(helpDocumentList.size() > 0) {
					for (HelpDocument helpDocument : helpDocumentList) {
						helpDocument.setRemark1(sdf.format(helpDocument.getCreatedDate()));
						if(null != helpDocument.getReleaseDate()) {
							helpDocument.setRemark2(sdf.format(helpDocument.getReleaseDate()));
						}
						if(null != helpDocument.getUpdatedDate() ) {
							helpDocument.setRemark3(sdf.format(helpDocument.getUpdatedDate()));
						}
					}
					state = "00,查询成功";
					map.put("helpDocumentList", helpDocumentList);
				}else {
					state = "01,无查询结果";
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,查询异常";
			}
		}
		map.put("state", state);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

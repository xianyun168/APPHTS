package com.ciitc.webServices.interfaces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import com.ciitc.webServices.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.CRM_CustomInfoUntil;
import com.ciitc.webServices.util.CalculationIntegral;
import com.ciitc.webServices.util.CrmFunctionUtil;
import com.ciitc.webServices.util.SMSPublicMessageUntil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import cn.emay.util.SmsSingleRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfLoginController.do")
public class KF_LoginController extends BaseController {

	public static Map<String, Object> userToken = new HashMap<String, Object>();
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil
			.getBean("appUserInforDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private EsbmmCodeTypeDao esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil
			.getBean("esbmmCodeTypeDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private EsbmmCodeDao esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil
			.getBean("esbmmCodeDao");
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;

	/*
	 * 登陆
	 */
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public @ResponseBody String Login(HttpServletRequest request,
			JSONObject json) {
		String state = "";
		String userId = "";
		String token = "";
		AppUserInfor appUserInfor = null;
		String phoneNumber = json.getString("phoneNumber");
		String verifiCode = json.getString("verifiCode");
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			//查询是否存在多个有效手机号
			int num = appUserInforDao.queryByPhoneNum(phoneNumber);
			if(num>1) {
				throw new Exception(phoneNumber); 
			}
			appUserInfor = appUserInforDao.queryByPhone(phoneNumber);
			userId = appUserInfor.getPkId();
			token = UUID.randomUUID().toString().replaceAll("-", "");
		} catch (Exception e1) {
			state = "03,账户异常请联系客服";
			map.put("state", state);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if("18911987122".equals(phoneNumber) && "123456".equals(verifiCode)) {
			CrmFunctionUtil  crmFunctionUtil=new CrmFunctionUtil();
			AppUserInfor appUserInfor1 = crmFunctionUtil.queryByStatus(appUserInfor);
			if (appUserInfor.getNameStatus()!=null&&!"".equals(appUserInfor.getNameStatus())) {
				if ("1".equals(appUserInfor.getNameStatus())) {
					appUserInfor.setFinancialId(appUserInfor1.getFinancialId());
					appUserInfor.setNameStatus(appUserInfor1.getNameStatus());
				}else if("2".equals(appUserInfor.getNameStatus())) {
					appUserInfor.setFinancialId(appUserInfor1.getFinancialId());
					appUserInfor.setNameStatus(appUserInfor1.getNameStatus());
				}
			}else{
				appUserInfor.setNameStatus("0");
				appUserInfor.setCustomerLevel("1");
			}
			appUserInfor.setCustomerLevel("1");
			state = "00";
		}else {
			if (null != appUserInfor && null != appUserInfor.getRemark1()) {
				long nowDate = System.currentTimeMillis();
				long createDate = appUserInfor.getCreateDate().getTime();
				if (appUserInfor.getCustomerLevel()==null || "".equals(appUserInfor.getCustomerLevel())) {
					appUserInfor.setCustomerLevel("1");
				}
				if (verifiCode.equals(appUserInfor.getRemark1())) {
					CrmFunctionUtil  crmFunctionUtil=new CrmFunctionUtil();
					AppUserInfor appUserInfor1 = crmFunctionUtil.queryByStatus(appUserInfor);
					if (appUserInfor.getNameStatus()!=null&&!"".equals(appUserInfor.getNameStatus())) {
						if ("1".equals(appUserInfor.getNameStatus())) {
							appUserInfor.setFinancialId(appUserInfor1.getFinancialId());
							appUserInfor.setNameStatus(appUserInfor1.getNameStatus());
						}else if("2".equals(appUserInfor.getNameStatus())) {
							appUserInfor.setFinancialId(appUserInfor1.getFinancialId());
							appUserInfor.setNameStatus(appUserInfor1.getNameStatus());
						}
					}else{
						appUserInfor.setNameStatus("0");
						appUserInfor.setCustomerLevel("1");
					}
					appUserInfor.setcIsDelete(appUserInfor1.getcIsDelete());
					appUserInfor.setRemark1("");
					state = "00";
				} else if (nowDate > (createDate + 5 * 60 * 1000)) {
					state = "01,验证码输入超时";
				} else if (verifiCode != appUserInfor.getRemark1()) {
					state = "02,验证码输入错误";
				}
				try {
					appUserInforDao.update1(appUserInfor);
					if(null != appUserInfor.getCustomerId() && !"".equals(appUserInfor.getCustomerId())) {
						//计算客户等级
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						String nowDate1 = sdf.format(new Date());
						/*Integer a = Integer.parseInt(nowDate1.substring(0, 4))-1;
						String nowDateYear =String.valueOf(a);
						String dates = nowDateYear+"1231";
						String datey = nowDateYear+"0101";*/
						CalculationIntegral.CustomerLevelBycustoNo(appUserInfor.getCustomerId());
						//CalculationIntegral.CustomerLevelBycustoNo(appUserInfor.getCustomerId());
						//计算客户积分
						CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
						//调用客户积分
						//1.先算出当前时间减上365之后的时间
						Date date = new Date();//当前时间
						Calendar ca = Calendar.getInstance();
						ca.add(Calendar.DATE, -365);
						date = ca.getTime();//减后时间
						//2.再判断当前时间和减后时间年份是否一样
						if(sdf.format(new Date()).substring(0, 4).equals(sdf.format(date).substring(0, 4))) {
							//3.如果年份一样则查询这一段时间的合同，按照客户表里的等级计算积分。
							Date yesterDay = new Date(new Date().getTime()-86400000L);
							List<Integration> integrationList  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay,date);
							CalculationIntegral.tradingIntegral(integrationList);
						}else {
							try {
								//4.如果年份不一样，则查询减后时间到年底的合同，等级按照加后时间的前一年年底的时间算，
								Date yesterDay = sdf.parse(sdf.format(date).substring(0, 4)+"-12-31");
								List<Integration> integrationList  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay,date);
								String yesterYear = String.valueOf((Integer.parseInt(sdf.format(date).substring(0, 4))-1))+"1231";
								String yesterYears = String.valueOf((Integer.parseInt(sdf.format(date).substring(0, 4))-1))+"0101";
								String level1 = CalculationIntegral.CustomerLevelBycustoNo1(appUserInfor.getCustomerId());
								//String level1 = CalculationIntegral.CustomerLevelBycustoNo1(appUserInfor.getCustomerId(),yesterYear,yesterYears);
								CalculationIntegral.tradingIntegral2(integrationList,level1);
								//当前时间到此年份之初的合同按照客户表里的等级计算
								Date yesterDay2 = sdf.parse(sdf.format(new Date()).substring(0, 4)+"-01-01");
								Date yesterDay1 = new Date(new Date().getTime()-86400000L);
								List<Integration> integrationList1  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay1,yesterDay2);
								CalculationIntegral.tradingIntegral(integrationList1);
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						/*Date date = new Date();
						Date yesterDay = new Date(date.getTime()-86400000L);
						List<Integration> integrationList  = customInfoUntil.queryByCustNo2(appUserInfor.getCustomerId(),yesterDay);
						--CalculationIntegral.tradingIntegral(integrationList);	*/
						
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				state = "01";
			}
		}
		String customerLevel=appUserInfor.getCustomerLevel();
		String customerId = appUserInfor.getCustomerId();
		String idcardNumber = appUserInfor.getIdcardNumber();
		String userName = appUserInfor.getUserName();
		//记录登录日志
		LoginLog loginLog=new LoginLog();
		loginLog.setPkId(userId);
		loginLog.setPhoneNumber(phoneNumber);
		appUserInforDao.addLoginLog(loginLog);
		if (state=="00") {
			map.put("state", state);
			map.put("userId", userId);
			map.put("token", token);
			map.put("customerId", customerId);
			map.put("customerLevel", customerLevel);
			map.put("nameStatus", appUserInfor.getNameStatus());
			map.put("phoneNumber", phoneNumber);
			map.put("idcardNumber", idcardNumber);
			map.put("userName", userName);
			// 返回给前台的json
			userToken.put(userId, token);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}else {
			map.put("state", state);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
	}


	/*
	 * 实名制注册
	 */
	@RequestMapping(value = "realNameRegistration", method = RequestMethod.GET)
	public @ResponseBody String realNameRegistration(
			HttpServletRequest request, JSONObject json) {
		String state = "";
		AppUserInfor appUserInforPhone = null;
		CrmFunctionUtil test = new CrmFunctionUtil();
		String userName = json.getString("userName");
		String idcardType = json.getString("idcardType");
		String idcardNumber = json.getString("idcardNumber");
		String customerAddress = json.getString("customerAddress");
		String phoneNumber = json.getString("phoneNumber");
		/*String region = json.getString("region");*/
		String pkId = json.getString("pkId");
		//短信发送配置
		boolean isGzip = false;
		String appId = "";
		String secretKey = "";
		String host = "";
		String algorithm = "";
		String sign = "";
		SmsSingleRequest smsSingleRequest = new SmsSingleRequest();
		List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao.queryByTypeName("SMSConfiguration");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
		List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
		for (EsbmmCode esbmmCode : listCode) {
			if (esbmmCode.getCodeName().equals("AppId")) {
				appId = esbmmCode.getCodeValue();// AppId
			}
			if (esbmmCode.getCodeName().equals("secretKey")) {
				secretKey = esbmmCode.getCodeValue();// 密匙
			}
			if (esbmmCode.getCodeName().equals("interfaceHost")) {
				host = esbmmCode.getCodeValue();// 接口地址
			}
			if (esbmmCode.getCodeName().equals("algorithm")) {
				algorithm = esbmmCode.getCodeValue();// 加密算法
			}
			if (esbmmCode.getCodeName().equals("SMSSignature")) {
				sign = esbmmCode.getCodeValue();// 短信签名
			}
		}
		
		//从数据字典查询服务分支机构
		/*List<EsbmmCodeType> esbmmCodeType1 = esbmmCodeTypeDao.queryByTypeName("AddressAll");
		Map<String, Object> paramMap1 = new HashMap<String, Object>();
		paramMap1.put("codeTypePkId", esbmmCodeType1.get(0).getPkId());
		List<EsbmmCode> listCode1 = esbmmCodeDao.selectByContion(paramMap1);
		String regionName ="";
		String code="";
		for (EsbmmCode esbmmCode : listCode1) {
			if (esbmmCode.getChineseName().equals(region)) {
				regionName =esbmmCode.getChineseName(); 
				code=esbmmCode.getCodeValue();
			}
		}*/
		//从数据字典查询证件类型
		List<EsbmmCodeType> esbmmCodeType2 = esbmmCodeTypeDao.queryByTypeName("IdcardType");
		Map<String, Object> paramMap2 = new HashMap<String, Object>();
		paramMap2.put("codeTypePkId", esbmmCodeType2.get(0).getPkId());
		List<EsbmmCode> listCode2 = esbmmCodeDao.selectByContion(paramMap2);
		String idcardTypeName ="";
		for (EsbmmCode esbmmCode : listCode2) {
			if (esbmmCode.getCodeValue().equals(idcardType)) {
				idcardTypeName =esbmmCode.getChineseName(); 
			}
		}
		//接收短信的手机号
		List<EsbmmCodeType> esbmmCodeType1 = esbmmCodeTypeDao.queryByTypeName("testPhone");
		Map<String, Object> paramMap1 = new HashMap<String, Object>();
		paramMap1.put("codeTypePkId", esbmmCodeType1.get(0).getPkId());
		List<EsbmmCode> listCode1 = esbmmCodeDao.selectByContion(paramMap1);
		String code = listCode1.get(0).getCodeValue();
		String signMessage = sign + "有新客户在我司客服APP进行注册，客户姓名："+userName+"，联系方式："+phoneNumber+"，证件类型："+idcardTypeName+"，证件号码："+idcardNumber+"，客户地址："+customerAddress+"，请尽快安排理财师跟客户联系，以便及时了解客户需求。";
		smsSingleRequest.setContent(signMessage);
		smsSingleRequest.setMobile(code);
		smsSingleRequest.setCustomSmsId("");
		smsSingleRequest.setExtendCode("");
		smsSingleRequest.setRequestTime(System.currentTimeMillis());
		smsSingleRequest.setRequestValidPeriod(30);
		smsSingleRequest.setTimerTime("");
		Map<String, Object> map = new HashMap<String, Object>();
		if (userName == null || "".equals(userName)
				|| "".equals(userName.trim())) {
			state = "01";
		} else if (idcardType == null || "".equals(idcardType)
				|| "".equals(idcardType.trim())) {
			state = "01";
		} else if (idcardNumber == null || "".equals(idcardNumber)
				|| "".equals(idcardNumber.trim())) {
			state = "01";
		} else if (customerAddress == null || "".equals(customerAddress)
				|| "".equals(customerAddress.trim())) {
			state = "01";
		} /*else if (region == null || "".equals(region)
				|| "".equals(region.trim())) {
			state = "01";
		}*/ else if (phoneNumber == null || "".equals(phoneNumber)
				|| "".equals(phoneNumber.trim())) {
			state = "01";
		} else {
			    appUserInforPhone = appUserInforDao.queryByPhoneId(pkId);
			if (appUserInforPhone != null) {
				appUserInforPhone.setUserName(userName);// 姓名
				appUserInforPhone.setIdcardNumber(idcardNumber);// 证件号码
				/*appUserInforPhone.setRegion(region);*/
				//前台发请求将实名认证状态改为2
				appUserInforPhone.setNameStatus("2");
				
				//================================================
				List<EsbmmCodeType> IdcardTypeList = esbmmCodeTypeDao.queryByTypeName("IdcardType");
				EsbmmCodeType IdcardTypePkId = IdcardTypeList.get(0);
				EsbmmCode codePkId2=new EsbmmCode();
				codePkId2.setCodeTypePkId(IdcardTypePkId.getPkId());
				//根据codeTypePkId查找对应的类型
				List<EsbmmCode> IdcardTypecodeList = esbmmCodeDao.queryByIdcard(codePkId2);
					for (EsbmmCode esbmmCode : IdcardTypecodeList) {
						if (idcardType.equals(esbmmCode.getChineseName())) {
							appUserInforPhone.setIdcardType(esbmmCode.getCodeCode());// 证件类型
						}
					}
					//=============================================
				appUserInforPhone.setCustomerAddress(customerAddress);// 地址
				//根据数据字典查询服务分支机构****//
				/*List<EsbmmCodeType> RegionList = esbmmCodeTypeDao.queryByTypeName("AddressAll");
				EsbmmCodeType RegionListPkId = RegionList.get(0);
				EsbmmCode codePkId3=new EsbmmCode();
				codePkId3.setCodeTypePkId(RegionListPkId.getPkId());
				//根据codeTypePkId查找对应的类型
				List<EsbmmCode> RegioncodeList = esbmmCodeDao.queryByTypeIds(codePkId3);
					for (EsbmmCode esbmmCode : RegioncodeList) {
						if (region.equals(esbmmCode.getChineseName())) {
							appUserInforPhone.setRegion(esbmmCode.getCodeCode());
						}
						
				}*/
				appUserInforPhone.setPhoneNumber(phoneNumber);// 手机号
				AppUserInfor  queryinfo=test.queryByAll(appUserInforPhone);
				if (queryinfo.getRemark2()==null||queryinfo.getRemark3()==null) {
					// 发送短信
					SMSPublicMessageUntil smsPublicMessageUntil = new SMSPublicMessageUntil();
					String falg = smsPublicMessageUntil.setSingleSms(smsSingleRequest,
							isGzip, appId, secretKey, host, algorithm);
					if (null != falg && falg.equals("成功")) {
						System.out.println("短信发送成功！已通知相关负责人");
					}
				}else {
					//从耀莱库里查客户ID，理财师ID
					AppUserInfor responsecustno=test.queryByCustno(appUserInforPhone);
					AppUserInfor fincalId = test.queryFicinalId(appUserInforPhone);
					test.queryByStatus(appUserInforPhone);
					appUserInforPhone.setNameStatus("1");
					//1：已实名认证
					queryinfo.setRemark2("");
					queryinfo.setRemark3("");
				}
				appUserInforDao.updateRegistration(appUserInforPhone);
				state = "00";
			} else {
				state = "01";
			}
		}
		map.put("state", state);
		map.put("nameStatus",appUserInforPhone.getNameStatus());
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	/*
	 * 发送验证码
	 */
	@RequestMapping(value = "sandSMS", method = RequestMethod.GET)
	public @ResponseBody String sandSMS(HttpServletRequest request,
			JSONObject json) throws Exception {
		String state = "";
		String PhoneNumber = json.getString("phoneNumber");
		AppUserInfor appUserInfor = null;
		SmsSingleRequest smsSingleRequest = new SmsSingleRequest();
		Map<String, Object> map = new HashMap<String, Object>();
		boolean isGzip = false;
		String appId = "";
		String secretKey = "";
		String host = "";
		String algorithm = "";
		String sign = "";
		// --------------------------------------
		List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao
				.queryByTypeName("SMSConfiguration");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
		List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
		for (EsbmmCode esbmmCode : listCode) {
			if (esbmmCode.getCodeName().equals("AppId")) {
				appId = esbmmCode.getCodeValue();// AppId
			}
			if (esbmmCode.getCodeName().equals("secretKey")) {
				secretKey = esbmmCode.getCodeValue();// 密匙
			}
			if (esbmmCode.getCodeName().equals("interfaceHost")) {
				host = esbmmCode.getCodeValue();// 接口地址
			}
			if (esbmmCode.getCodeName().equals("algorithm")) {
				algorithm = esbmmCode.getCodeValue();// 加密算法
			}
			if (esbmmCode.getCodeName().equals("SMSSignature")) {
				sign = esbmmCode.getCodeValue();// 短信签名
			}
		}
		// 随机数验证码生成
		int number = (int) ((Math.random() * 9 + 1) * 100000);
		String content = Integer.toString(number);
		String signMessage = sign + "您的手机验证码是：" + content + "，5分钟内有效。为了您的账户安全，请勿将验证码透露给他人。如非本人操作请忽略。";
		smsSingleRequest.setContent(signMessage);
		smsSingleRequest.setMobile(PhoneNumber);
		smsSingleRequest.setCustomSmsId("");
		smsSingleRequest.setExtendCode("");
		smsSingleRequest.setRequestTime(System.currentTimeMillis());
		smsSingleRequest.setRequestValidPeriod(30);
		smsSingleRequest.setTimerTime("");
		// 发送短信
		SMSPublicMessageUntil smsPublicMessageUntil = new SMSPublicMessageUntil();
		String falg = smsPublicMessageUntil.setSingleSms(smsSingleRequest,
				isGzip, appId, secretKey, host, algorithm);
		try {
			//查询是否存在多个有效手机号
			int num = appUserInforDao.queryByPhoneNum(PhoneNumber);
			if(num>1) {
				throw new Exception(PhoneNumber); 
			}
			appUserInfor = appUserInforDao.queryByPhone(PhoneNumber);
			if (null != falg && falg.equals("成功")) {
				if (appUserInfor != null) {
					// 当前系统时间
					appUserInfor.setCreateDate(new Date());
					appUserInfor.setRemark1(content);
					state = "00";
					appUserInforDao.update1(appUserInfor);
				} else {
					appUserInfor = new AppUserInfor();
					String uuid = UUID.randomUUID().toString();
					appUserInfor.setPkId(uuid);
					appUserInfor.setRemark1(content);
					appUserInfor.setPhoneNumber(PhoneNumber);
					appUserInfor.setCreateDate(new Date());
					appUserInfor.setIntegral(0);
					appUserInfor.setcIsDelete("0");
					state = "00";
					appUserInforDao.insertInfor(appUserInfor);
				}
			} else {
				state = "01";
			}
		} catch (Exception e) {
			state = "00";
			e.printStackTrace();
		}
		map.put("state",state );
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

	/*
	 * 获取 地区 证件号 组织分支机构
	 */
	
	@SuppressWarnings("unchecked")
	public @ResponseBody String addressShow(HttpServletRequest request,
			JSONObject json) {
		Map<String,Object> mapList = new HashMap<String, Object>();
		LinkedList< String> IdcardlinkedList=new LinkedList<String>();
		LinkedList< String> regionlinkedList=new LinkedList<String>();
		String state = "";
		try {
			//根据数据字典查询证件类型****//
			List<EsbmmCodeType> IdcardTypeList = esbmmCodeTypeDao.queryByTypeName("IdcardType");
			EsbmmCodeType IdcardTypePkId = IdcardTypeList.get(0);
			EsbmmCode codePkId2=new EsbmmCode();
			codePkId2.setCodeTypePkId(IdcardTypePkId.getPkId());
			//根据codeTypePkId查找对应的类型
			List<EsbmmCode> IdcardTypecodeList = esbmmCodeDao.queryByIdcard(codePkId2);
				for (EsbmmCode esbmmCode : IdcardTypecodeList) {
					if (esbmmCode.getCodeCode()==null&&esbmmCode.getChineseName()==null) {
						state="01";
					}else {
						IdcardlinkedList.add(esbmmCode.getChineseName());
					}
				}
			//-----------------------------
			//根据数据字典查询服务分支机构****//
			List<EsbmmCodeType> RegionList = esbmmCodeTypeDao.queryByTypeName("AddressAll");
			EsbmmCodeType RegionListPkId = RegionList.get(0);
			EsbmmCode codePkId3=new EsbmmCode();
			codePkId3.setCodeTypePkId(RegionListPkId.getPkId());
			//根据codeTypePkId查找对应的类型
			List<EsbmmCode> RegioncodeList = esbmmCodeDao.queryByIdcard(codePkId3);
			if (RegioncodeList==null) {
				state="01";
			}
			for (EsbmmCode esbmmCode : RegioncodeList) {
						regionlinkedList.add(esbmmCode.getChineseName());
			}
			//-----------------------------
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		state="00";
		mapList.put("state",state );
		mapList.put("IdcardType", IdcardlinkedList);
		mapList.put("regionmap", regionlinkedList);
		String jsons = JSONArray.fromObject(mapList).toString();
		return jsons;
	}
}

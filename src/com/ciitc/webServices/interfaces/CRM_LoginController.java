package com.ciitc.webServices.interfaces;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.ins.sinosoft.framework.base.util.MethodUtil;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.alibaba.druid.sql.visitor.functions.If;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.ViewEmplRelationDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.ViewEmplRelation;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.CRM_Login;
import com.ciitc.webServices.util.CrmFunctionUtil;
import com.ciitc.webServices.util.DESEncrypt;

@Controller
@RequestMapping("/kfLoginController.do")
public class CRM_LoginController extends BaseController {

	public static Map<String, Object> userToken = new HashMap<String, Object>();
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil
			.getBean("appUserInforDao");
	@Autowired(required = false)
	private EsbmmCodeTypeDao esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil
			.getBean("esbmmCodeTypeDao");
	@Autowired(required = false)
	private EsbmmCodeDao esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil
			.getBean("esbmmCodeDao");
	
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;

	/*
	 * 登陆
	 */
	@RequestMapping(value = "login", method = RequestMethod.GET)
	public @ResponseBody String login(HttpServletRequest request,
			JSONObject json) {
		String state = "";
		//pkId
		String userId = "";
		String token = "";
		String msg="";
		//理财师用户名
		String userName = json.getString("userName");
		String userPwd = json.getString("userPwd");
		String PwdEncryption="";
		try {
			PwdEncryption=DESEncrypt.hsEncrypt(userPwd);
		} catch (Exception e) {
			// TODO: handle exception
		}		
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			token = UUID.randomUUID().toString().replaceAll("-", "");
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		CRM_Login test=new CRM_Login();
		Map<String, Object> statusMap=new HashMap<String, Object>();
		//判断登陆用户账号状态
		String userState = test.queryByUserState(userName);
		//根据用户名和密码校验是否正确
		statusMap= test.queryByStatus(userName,PwdEncryption);
		if("1".equals(userState)) {
			state="01";
			msg="账号已注销";
		}else if("2".equals(userState)){
			state="01";
			msg="账号已锁定";
		}else {
			if ("00".equals(statusMap.get("isLogin"))) {
				state="00";
				//msg="登陆成功";
				msg="该App已下线,请下载秀实投资App使用";
			}else {
				state="01";
				msg="登陆失败，用户名或密码错误";
			}
		}
		if (state.equals("00")) {
	/*		map.put("state", state);
			map.put("msg", msg);
			map.put("userId", userId);
			map.put("token", token);
			map.put("brokeId", statusMap.get("brokeId"));
			map.put("brokeName", statusMap.get("brokeName"));
			// 返回给前台的json
			userToken.put(userId, token);
			//所属部门编号
			String c_deptNodeCode = test.queryByDeptNo(statusMap.get("brokeId").toString());
			map.put("c_deptNodeCode", c_deptNodeCode);
			String jsons = JSONArray.fromObject(map).toString();*/
			map.put("state", "01");
			map.put("msg", msg);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}else {
			map.put("state", state);
			map.put("msg", msg);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
	}
}

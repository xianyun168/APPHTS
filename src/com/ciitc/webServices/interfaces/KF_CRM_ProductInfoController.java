package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ciitc.webServices.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.controller.ProductInfoController;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTtrustcontractReqDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.ProductAppendixInfoDao;
import com.ciitc.webServices.dao.ProductFkDao;
import com.ciitc.webServices.dao.ProductInfoDao;
import com.ciitc.webServices.util.Log;
import com.ciitc.webServices.util.ProductLimtUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfCrmProductInfoController.do")
public class KF_CRM_ProductInfoController extends BaseController{
@SuppressWarnings("rawtypes")
@Autowired(required = false)
private ProductFkDao productFkDao = (ProductFkDao) SpringContextUtil.getBean("productFkDao");
@SuppressWarnings("rawtypes")
@Autowired(required = false)
private EsbmmCodeTypeDao esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");
@SuppressWarnings("rawtypes") 
@Autowired(required = false)
private EsbmmCodeDao esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");	
@SuppressWarnings({ "unchecked", "rawtypes" })
@Autowired(required = false)
private ProductInfoDao<ProductInfo> productInfoDao = (ProductInfoDao) SpringContextUtil.getBean("productInfoDao");
@SuppressWarnings({ "rawtypes" })
@Autowired(required = false)
private ProductAppendixInfoDao productAppendixInfoDao = (ProductAppendixInfoDao) SpringContextUtil.getBean("productAppendixInfoDao");
@SuppressWarnings("rawtypes")
@Autowired(required = false)
private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
@SuppressWarnings({ "unchecked", "rawtypes" })
@Autowired(required = false)

private CPTtrustcontractReqDao<CPTtrustcontractReq> CPTtrustcontractReqDao = (CPTtrustcontractReqDao) SpringContextUtil.getBean("CPTtrustcontractReqDao");

	
	/*
	 * 产品查询列表、详情接口
	 */
	@RequestMapping(value = "getProductList",method=RequestMethod.GET)
	@ResponseBody
	public String getProductList(HttpServletRequest request,JSONObject json) {
		//ProductInfoController productInfoController = new ProductInfoController();
		//productInfoController.updateProductInfo();
		HashMap<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		String filestate="";
		String statefj="";
		//产品编号，把产品编号放到pkId里面
		String pkId = "";
		//用户pkId
		String userPkId = "";
		//产品名称
		String productname ="";
		//产品类型
		String pruducttype ="";
		//存续期限
		String deadline="";
		if(json1.has("pkId")){
			pkId = json1.getString("pkId");
		}
		if(json1.has("userPkId")) {
			userPkId = json1.getString("userPkId");
					
		}	
		if(json1.has("productname")) {
			productname = json1.getString("productname");
					
		}	
		if(json1.has("pruducttype")) {
			pruducttype = json1.getString("pruducttype");
					
		}	
		if(json1.has("deadline")) {
			deadline = json1.getString("deadline");
					
		}	
		AppUserInfor appUserInfor =appUserInforDao.selectByPrimaryKey(userPkId);
		//根据用户id查询用户是否存在
		if (null != appUserInfor && !"".equals(appUserInfor)) {
			if(null != pkId && !"".equals(pkId)) {
				//根据产品编号查询详情
				ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
				ProductFk productFk = productFkDao.selectBypid(pkId);
				if(null != productInfo) {
					if(null != productFk) {
						productInfo.setBettingAgreement(productFk.getBettingAgreement());
						productInfo.setCompensationClause(productFk.getCompensationClause());
						productInfo.setRiskManagement(productFk.getRiskManagement());
						productInfo.setSharePledge(productFk.getSharePledge());
					}
					if("1".equals(appUserInfor.getNameStatus())){
						state ="0,已实名认证";
						Map<String, Object> para2 = new HashMap<String, Object>();
						para2.put("cCustno", appUserInfor.getCustomerId());
						para2.put("lPROSERIALNO", Integer.parseInt(productInfo.getPkId()));//去掉此条件限制
						List<CPTtrustcontractReq> CPTtrustcontractReq = CPTtrustcontractReqDao.selectByCustomerId1(para2);
						if(CPTtrustcontractReq.size()>0){
							filestate="000,曾经购买过产品";
							@SuppressWarnings("unchecked")
							List<ProductAppendixInfo> productAppendInfoList =  productAppendixInfoDao.selectByProductid(productInfo.getPkId());
							//取出数据字典里产品附件的地址
							List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao.queryByTypeName("fileURL");
							Map<String, Object> paramMap = new HashMap<String, Object>();
							paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
							List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
							String URL = listCode.get(0).getCodeValue();
							if(productAppendInfoList.size() > 0) {
								for (ProductAppendixInfo productAppendixInfo : productAppendInfoList) {
									productAppendixInfo.setRemark3(sdf.format(productAppendixInfo.getCreateDate()));
									productAppendixInfo.setUploadAddress(URL+productAppendixInfo.getUploadAddress());
								}
								statefj = "00,查询产品附件成功";
								Log.getLogger().info("查询产品附件列表");
							}else {
								statefj = "01,产品附件无查询结果";
								Log.getLogger().info("产品附件无查询结果");
							}
							map.put("productAppendInfoList", productAppendInfoList);
							
						}else{
							filestate="001,没有购买过产品";
						}
						
					}else{
						state ="0,未实名认证";
						//state ="2,待审核中";
					}
					map.put("productInfo", productInfo);
					state = "00,查询成功";
					Log.getLogger().info("查看产品详情");				
				}else {
					state = "01,无查询结果";
					Log.getLogger().info("无查询结果");
				}
				
			}else {
				try {
					//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
					Map<String, Object> map1 = new HashMap<String, Object>();
					map1.put("productname",productname );
					map1.put("pruducttype", pruducttype);
					map1.put("deadline", deadline);
					if("72cef2d6-c762-44c8-8b35-72114b6dc24c".equals(userPkId)){
						map1.put("noproductname","青山");
					}
					List<ProductInfo> productInfoList =  productInfoDao.queryByListj(map1);
					//秀实汇隐藏掉产品列表功能
					List<ProductInfo> productInfoListNull =  new ArrayList<>();
					if(productInfoList.size() >0) {
						for (ProductInfo productInfo2 : productInfoList) {
							ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
							if(null != productFk) {
								productInfo2.setBettingAgreement(productFk.getBettingAgreement());
								productInfo2.setCompensationClause(productFk.getCompensationClause());
								productInfo2.setRiskManagement(productFk.getRiskManagement());
								productInfo2.setSharePledge(productFk.getSharePledge());
							}
							// 计算产品剩余额度
							int limit =0;
							//int limit = ProductLimtUtil.productEndDate(productInfo2.getPkId());
							//productInfo2.setRemark3(Integer.toString(limit));
						}
						state = "00,查询成功else";
						Log.getLogger().info("查询产品信息列表");
					}else {
						state = "01,产品无查询结果";
						Log.getLogger().info("产品无查询结果");
					}
					//map.put("productInfoList", productInfoList);
					map.put("productInfoList", productInfoListNull);

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					state = "01,产品信息查询异常";
				}
			}
		}else{
			state= "01,没有客户ID";
		}
		map.put("state", state);
		map.put("filestate", filestate);
		map.put("statefj", statefj);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}




	//根据收益查询产品表所有字段
	 
	@RequestMapping(value = "getProductProfit",method=RequestMethod.GET)
	@ResponseBody
	public String getProductProfit(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		//产品编号，把产品编号放到pkId里面
		String pkId = "";
		//用户pkId
		String userPkId = "";
		
		if(json1.has("pkId")){
			pkId = json1.getString("pkId");
		}
		if(json1.has("userPkId")) {
			userPkId = json1.getString("userPkId");
					
		}	
		AppUserInfor appUserInfor =appUserInforDao.selectByPrimaryKey(userPkId);
		if (null != appUserInfor && !"".equals(appUserInfor)) {
			if(null != pkId && !"".equals(pkId)) {
				//根据产品编号查询详情
				/*ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
				if(null != productInfo) {
					if("1".equals(appUserInfor.getNameStatus())){
						state ="0,已实名认证";
						Map<String, Object> para = new HashMap<String, Object>();
						para.put("cCustno", appUserInfor.getCustomerId());
						para.put("lPROSERIALNO", Integer.parseInt(productInfo.getPkId()));
						List<CPTtrustcontractReq> CPTtrustcontractReq = CPTtrustcontractReqDao.selectByCustomerId1(para);
						if(CPTtrustcontractReq.size()>0){
							state="000,曾经购买过产品";
						}else{
							state="001,没有购买过产品";
						}
					}else{
						state ="1,未实名认证";
					}
					map.put("productInfo", productInfo);
					state = "00,查询成功";
					Log.getLogger().info("查看产品详情");				
				}else {
					state = "01,无查询结果";
					Log.getLogger().info("无查询结果");
				}*/
			}else {
				try {
					//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
					Map<String, Object> map1 = new HashMap<String, Object>();
					if("72cef2d6-c762-44c8-8b35-72114b6dc24c".equals(userPkId)){
						map1.put("noproductname","青山");
					}
					List<ProductInfo> productInfoList =  productInfoDao.queryByProfit(map1);
					List<ProductInfo> productInfoListNull =  new ArrayList<>();
					if(productInfoList.size() > 0) {
						for (ProductInfo productInfo2 : productInfoList) {
							ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
							if(null != productFk) {
								productInfo2.setBettingAgreement(productFk.getBettingAgreement());
								productInfo2.setCompensationClause(productFk.getCompensationClause());
								productInfo2.setRiskManagement(productFk.getRiskManagement());
								productInfo2.setSharePledge(productFk.getSharePledge());
							}
							// 计算产品剩余额度
							//int limit = ProductLimtUtil.productEndDate(productInfo2.getPkId());
							//productInfo2.setRemark3(Integer.toString(limit));
						}
						state = "00,查询成功";
						Log.getLogger().info("查询产品信息列表");
						//map.put("productInfoList", productInfoList);
						map.put("productInfoList", productInfoListNull);
					}else {
						state = "01,产品无查询结果";
						Log.getLogger().info("产品无查询结果");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					state = "01,产品信息查询异常";
				}
			}
		}else{
			state= "01,没有客户ID";
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	 //根据投资金额查询产品表所有字段
	 
	@RequestMapping(value = "getProductDeadline",method=RequestMethod.GET)
	@ResponseBody
	public String getProductDeadline(HttpServletRequest request,JSONObject json) {
			Map<String, Object> map = new HashMap<String, Object>();
			JSONObject json1 = JSONObject.fromObject(json);
			String state = "";
			//产品编号，把产品编号放到pkId里面
			String pkId = "";
			//用户pkId
			String userPkId = "";
			
			if(json1.has("pkId")){
				pkId = json1.getString("pkId");
			}
			if(json1.has("userPkId")) {
				userPkId = json1.getString("userPkId");
						
			}	
			AppUserInfor appUserInfor =appUserInforDao.selectByPrimaryKey(userPkId);
			if (null != appUserInfor && !"".equals(appUserInfor)) {
				if(null != pkId && !"".equals(pkId)) {
					//根据产品编号查询详情
					/*ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
					if(null != productInfo) {
						if("1".equals(appUserInfor.getNameStatus())){
							state ="0,已实名认证";
							Map<String, Object> para = new HashMap<String, Object>();
							para.put("cCustno", appUserInfor.getCustomerId());
							para.put("lPROSERIALNO", Integer.parseInt(productInfo.getPkId()));
							List<CPTtrustcontractReq> CPTtrustcontractReq = CPTtrustcontractReqDao.selectByCustomerId1(para);
							if(CPTtrustcontractReq.size()>0){
								state="000,曾经购买过产品";
							}else{
								state="001,没有购买过产品";
							}
							
						}else{
							state ="1,未实名认证";
						}
						map.put("productInfo", productInfo);
						state = "00,查询成功";
						Log.getLogger().info("查看产品详情");				
					}else {
						state = "01,无查询结果";
						Log.getLogger().info("无查询结果");
					}*/
					
				}else {
					try {
						//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
						Map<String, Object> map1 = new HashMap<String, Object>();
						if("72cef2d6-c762-44c8-8b35-72114b6dc24c".equals(userPkId)){
							map1.put("noproductname","青山");
						}
						List<ProductInfo> productInfoList =  productInfoDao.queryByDeadline(map1);
						List<ProductInfo> productInfoListNull =  new ArrayList<>();
						if(productInfoList.size() > 0) {
							for (ProductInfo productInfo2 : productInfoList) {
								ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
								if(null != productFk) {
									productInfo2.setBettingAgreement(productFk.getBettingAgreement());
									productInfo2.setCompensationClause(productFk.getCompensationClause());
									productInfo2.setRiskManagement(productFk.getRiskManagement());
									productInfo2.setSharePledge(productFk.getSharePledge());
								}
								// 计算产品剩余额度
								//int limit = ProductLimtUtil.productEndDate(productInfo2.getPkId());
								//productInfo2.setRemark3(Integer.toString(limit));
							}
							state = "00,查询成功";
							Log.getLogger().info("查询产品信息列表");
							//map.put("productInfoList", productInfoList);
							map.put("productInfoList", productInfoListNull);
						}else {
							state = "01,产品无查询结果";
							Log.getLogger().info("产品无查询结果");
						}
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						state = "01,产品信息查询异常";
					}
				}
			}else{
				state= "01,没有客户ID";
			}
			map.put("state", state);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		
	
	 //根据投资期限查询产品表所有字段
	 
	@RequestMapping(value = "getProductPermin",method=RequestMethod.GET)
	@ResponseBody
	public String getProductPermin(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		//产品编号，把产品编号放到pkId里面
		String pkId = "";
		//用户id
		String userPkId = "";
		
		if(json1.has("pkId")){
			pkId = json1.getString("pkId");
		}
		if(json1.has("userPkId")) {
			userPkId = json1.getString("userPkId");
					
		}
		AppUserInfor appUserInfor =appUserInforDao.selectByPrimaryKey(userPkId);
		if(null != appUserInfor && !"".equals(appUserInfor)) {
			if(null != pkId && !"".equals(pkId)) {
				//根据产品编号查询详情
				/*ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
				if(null != productInfo) {
					if("1".equals(appUserInfor.getNameStatus())){
						state ="0,已实名认证";
						Map<String, Object> para = new HashMap<String, Object>();
						para.put("cCustno", appUserInfor.getCustomerId());
						para.put("lPROSERIALNO", Integer.parseInt(productInfo.getPkId()));
						List<CPTtrustcontractReq> CPTtrustcontractReq = CPTtrustcontractReqDao.selectByCustomerId1(para);
						if(CPTtrustcontractReq.size()>0){
							state="000,曾经购买过产品";
						}else{
							state="001,没有购买过产品";
						}
						
					}else{
						state ="1,未实名认证";
					}
					map.put("productInfo", productInfo);
					state = "00,查询成功";
					Log.getLogger().info("查看产品详情");				
				}else {
					state = "01,无查询结果";
					Log.getLogger().info("无查询结果");
				}*/
				
			}else {
				try {
					/*//查询所有，遍历产品列表将产品存续期限“个月”去掉进行排序 
					List<ProductInfo>list1 =productInfoDao.queryALL();
					for (ProductInfo qq : list1){
						String s =qq.getDeadline();
						String removes ="个月";
						qq.setRemark1(s.replaceAll(removes, ""));
					}map.put("list1", list1);*/
					//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
					Map<String, Object> map1 = new HashMap<String, Object>();
					if("72cef2d6-c762-44c8-8b35-72114b6dc24c".equals(userPkId)){
						map1.put("noproductname","青山");
					}
					List<ProductInfo> productInfoList =  productInfoDao.queryByPermin(map1);
					List<ProductInfo> productInfoListNull =  new ArrayList<>();
					if(productInfoList.size() > 0) {
						for (ProductInfo productInfo2 : productInfoList) {
							ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
							if(null != productFk) {
								productInfo2.setBettingAgreement(productFk.getBettingAgreement());
								productInfo2.setCompensationClause(productFk.getCompensationClause());
								productInfo2.setRiskManagement(productFk.getRiskManagement());
								productInfo2.setSharePledge(productFk.getSharePledge());
							}
							// 计算产品剩余额度
							//int limit = ProductLimtUtil.productEndDate(productInfo2.getPkId());
							//productInfo2.setRemark3(Integer.toString(limit));
						}
						state = "00,查询成功";
						Log.getLogger().info("查询产品信息列表");
						//map.put("productInfoList", productInfoList);
						map.put("productInfoList", productInfoListNull);
					}else {
						state = "01,产品无查询结果";
						Log.getLogger().info("产品无查询结果");
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					state = "01,产品信息查询异常";
				}
			}
		}else {
			state= "01,没有客户ID";
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

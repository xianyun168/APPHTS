package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.PublishNewsDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.PublishNewsInfo;
import com.ciitc.webServices.util.Log;

@Controller
@RequestMapping("/kfCrmCompanyNewsController.do")
public class KF_CRM_CompanyNewsContorller extends BaseController{
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");	
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private PublishNewsDao<PublishNewsInfo> publishNewsDao = (PublishNewsDao) SpringContextUtil.getBean("publishNewsDao");
	
	/*
	 * 新闻列表查询、详情接口
	 */
	@RequestMapping(value = "companyNewsQueryList",method=RequestMethod.GET)
	@ResponseBody
	public String getCompanyNewsQueryList(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		String pkId = request.getParameter("pkId");
		if(json1.has("pkId")) {
			pkId = json1.getString("pkId");
		}
		String pkid = esbmmCodeTypeDao.queryByTypeName("host").get(0).getPkId();
        String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
		if(null != pkId && !"".equals(pkId)) {
			//根据pkId查询详情
			PublishNewsInfo publishNewsInfo =  publishNewsDao.selectByPrimaryKey(pkId);
			if(null != publishNewsInfo) {
				publishNewsInfo.setNewsImageInternet(fwqdkh+publishNewsInfo.getNewsImageInternet());
				publishNewsInfo.setRemark3(sdf.format(publishNewsInfo.getNewsPublishConfig()));
				map.put("publishNewsInfo", publishNewsInfo);
				state = "00,查询公司新闻详情成功";
				Log.getLogger().info("查看公司新闻详情");		
			}else {
				state = "01,新闻发布无查询结果";
				Log.getLogger().info("新闻发布无查询结果");
			}
			
		}else {
			try {
				//查询新闻类型为公司新闻的所有新闻数据列表
				List<PublishNewsInfo> publishNewsInfoList =  publishNewsDao.queryByListj();
				if(publishNewsInfoList.size() > 0) {
					for (PublishNewsInfo publishNewsInfo : publishNewsInfoList) {
						publishNewsInfo.setNewsImageInternet(fwqdkh+publishNewsInfo.getNewsImageInternet());
						publishNewsInfo.setRemark3(sdf.format(publishNewsInfo.getNewsPublishConfig()));
					}
					state = "00,查询公司新闻成功";
					Log.getLogger().info("查看公司新闻列表");
					map.put("publishNewsInfoList", publishNewsInfoList);
				}else {
					state = "01,公司新闻无查询结果";
					Log.getLogger().info("公司新闻无查询结果");
				}				
			}
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,查询异常";
			}
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

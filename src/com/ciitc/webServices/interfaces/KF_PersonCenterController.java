package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.util.CrmFunctionUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfPersonCenterController")
public class KF_PersonCenterController extends BaseController {
	@Autowired(required = false)
	private EsbmmCodeTypeDao esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil
			.getBean("esbmmCodeTypeDao");
	@Autowired(required = false)
	private EsbmmCodeDao esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil
			.getBean("esbmmCodeDao");
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	private CPTfundbrokerDao cpTfundbrokerDao = (CPTfundbrokerDao) SpringContextUtil.getBean("CPTfundbrokerDao");
	@RequestMapping(value = "getUserInFo",method=RequestMethod.GET)
	@ResponseBody 
	public String getUserInFo(HttpServletRequest request,JSONObject json) {
	/*	CrmFunctionUtil crmFunctionUtil = new CrmFunctionUtil();
		crmFunctionUtil.getFinancial();*/
		//---------------------------------------
		String state = null;
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String pkId = "";
		if(json.has("pkId")) {
			pkId = json.getString("pkId");
		}
		
		if(null != pkId && !"".equals(pkId)) {
			AppUserInfor userInfor = appUserInforDao.selectByPrimaryKey(pkId);
			if(null != userInfor) {
				//从数据字典查询服务分支机构
				List<EsbmmCodeType> esbmmCodeType1 = esbmmCodeTypeDao.queryByTypeName("AddressAll");
				Map<String, Object> paramMap1 = new HashMap<String, Object>();
				paramMap1.put("codeTypePkId", esbmmCodeType1.get(0).getPkId());
				List<EsbmmCode> listCode1 = esbmmCodeDao.selectByContion(paramMap1);
				for (EsbmmCode esbmmCode : listCode1) {
					if (esbmmCode.getCodeCode().equals(userInfor.getRegion())) {
						userInfor.setRegion(esbmmCode.getChineseName()); 
					}
				}
				//从数据字典查询证件类型机构
				List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao.queryByTypeName("IdcardType");
				Map<String, Object> paramMap = new HashMap<String, Object>();
				paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
				List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
				for (EsbmmCode esbmmCode : listCode) {
					if (esbmmCode.getCodeValue().equals(userInfor.getIdcardType())) {
						userInfor.setIdcardType(esbmmCode.getChineseName()); 
					}
				}
				//TODO 根据理财师id查询出理财师电话和姓名
				String financialId = userInfor.getFinancialId();
				CPTfundbroker financialInfo  = cpTfundbrokerDao.selectByPrimaryKey(financialId);
				if(null != financialInfo) {
					//理财师姓名
					userInfor.setRemark1(financialInfo.getcBrokername());
					//理财师电话
					if(null != financialInfo.getcMobile() && !"".equals(financialInfo.getcMobile())) {
						userInfor.setRemark2(financialInfo.getcMobile());
					}else {
						userInfor.setRemark2("-");
					}
				}
				if(null != userInfor.getCreateDate()) {
					userInfor.setRemark3(sdf.format(userInfor.getCreateDate()));
				}
				if(null != userInfor.getCustomerBirthday()) {
					userInfor.setRemark4(sdf.format(userInfor.getCustomerBirthday()));
				}
				map.put("userInfor", userInfor);
				state = "00,查询成功";
			}else {
				state = "00,查询无结果";
			}
		}else {
			state = "01,必传字段不能为空";
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

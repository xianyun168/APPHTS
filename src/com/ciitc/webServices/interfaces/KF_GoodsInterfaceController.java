package com.ciitc.webServices.interfaces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

import com.ciitc.webServices.dao.AppGoodsInforDao;
import com.ciitc.webServices.model.AppGoodsInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.service.EsbmmCodeService;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年5月9日 下午2:22:54
 * 修 改 人: 
 ******************************************************************************/
public class KF_GoodsInterfaceController{

	@SuppressWarnings("unchecked")
	private AppGoodsInforDao<AppGoodsInfor> appGoodsInforDao =  (AppGoodsInforDao<AppGoodsInfor>) SpringContextUtil.getBean("appGoodsInforDao");
	
	@SuppressWarnings("unchecked")
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService<EsbmmCode>) SpringContextUtil.getBean("esbmmCodeService");
	
	@RequestMapping(params = "queryGoodsList=key")
	@ResponseBody
	public String queryGoodsList(Integer end,Integer start,String goodsName){
		Map<String,Object> map = new HashMap<String,Object>();
		//map.put("start", start);
		//map.put("end", end);
		map.put("goodsName", goodsName);
		List<AppGoodsInfor> list = new ArrayList<AppGoodsInfor>();
		List<AppGoodsInfor> li = new ArrayList<AppGoodsInfor>();
		String json="";
		try {
			list = appGoodsInforDao.selectList(map);
			List<EsbmmCode> code = esbmmCodeService.getAddress();
			String addre = code.get(0).getCodeValue();
			if(list.size() !=0){
				if(list.size() > 5){
					for(int i=0;i<5;i++){
						String url =addre + list.get(i).getMainImageUrl();
						list.get(i).setMainImageUrl(url);
						li.add(list.get(i));
						json = JSONArray.fromObject(li).toString();
					}
				}else{
					for(AppGoodsInfor good:list){
						String url =addre + good.getMainImageUrl();
						good.setMainImageUrl(url);
					}
					li.addAll(list);
					json = JSONArray.fromObject(li).toString();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return json;
	}
	
}

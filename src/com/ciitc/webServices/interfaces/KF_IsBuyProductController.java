package com.ciitc.webServices.interfaces;

import java.util.Calendar;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.util.GK_AppUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class KF_IsBuyProductController extends BaseController{
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");

	/*
	 * 是否已经购买过产品
	 */
	@RequestMapping(value = "isBuyProduct",method=RequestMethod.GET)
	@ResponseBody
	public String isBuyProduct(HttpServletRequest request,JSONObject json) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		GK_AppUtil gkAppUtil = new GK_AppUtil();
		String state = "";
		//客户编号
		String customerId = "";
		if(json.has("customerId")&&null!=json.getString("customerId")&&!"".equals(json.getString("customerId"))) {
			customerId = json.getString("customerId");
		}else {
			state = "01,客户编号为空";
			map.put("state", state);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		AppUserInfor appUserInfor =appUserInforDao.selectByCustomerId(customerId);
		//根据用户id查询用户是否存在
		if (null != appUserInfor && !"".equals(appUserInfor)) {
			if("1".equals(appUserInfor.getNameStatus())){
				Calendar calendar = Calendar.getInstance();
				Integer nowYear = calendar.get(Calendar.YEAR);
				Integer month = calendar.get(Calendar.MONTH) + 1;
				String startDate = null;
				String endDate = null;
				if (month == 1 || month == 2 || month == 3) {
					startDate = String.valueOf(nowYear - 1) + "1001";
					endDate = String.valueOf(nowYear - 1) + "1231";
				} else if (month == 4 || month == 5 || month == 6) {
					startDate = String.valueOf(nowYear) + "0101";
					endDate = String.valueOf(nowYear) + "0331";
				} else if (month == 7 || month == 8 || month == 9) {
					startDate = String.valueOf(nowYear) + "0401";
					endDate = String.valueOf(nowYear) + "0630";
				} else if (month == 10 || month == 11 || month == 12) {
					startDate = String.valueOf(nowYear) + "0701";
					endDate = String.valueOf(nowYear) + "0930";
				}
				String htState = gkAppUtil.getContractStateByCustomerId(customerId, startDate, endDate);
				System.out.println("hState========================================"+htState);
				//判断是否存在有效合同
				if("00".equals(htState)) {
					state = htState+",已购买产品";
				}else {
					state = htState+",未购买产品";
				}
				if(customerId.equals("200000283997")){
					state = "00"+",已购买产品";
				}
			}else{
				state ="01,未实名认证";
			}
		}else{
			state= "01,客户ID错误";
		}
		System.out.println("state========================================"+state);
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

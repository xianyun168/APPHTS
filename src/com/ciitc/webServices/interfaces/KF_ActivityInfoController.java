package com.ciitc.webServices.interfaces;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.ActivityBasicInforDao;
import com.ciitc.webServices.model.ActivityBasicInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.Log;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/kfActivityInfoController")
public class KF_ActivityInfoController extends BaseController{
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private ActivityBasicInforDao activityBasicInforDao =  (ActivityBasicInforDao) SpringContextUtil.getBean("activityBasicInforDao");
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	
	@RequestMapping(value = "queryActivityInfo",method=RequestMethod.GET)
	@ResponseBody
	public String queryActivityInfo(HttpServletRequest request,JSONObject jsonParameter) {
		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject json = JSONObject.fromObject(jsonParameter);
		String state = "";
		String pkId = "";
		if(json.has("pkId")) {
			 pkId = json.getString("pkId");
			 if(pkId != null && !"".equals(pkId)) {
				 ActivityBasicInfor activityBasicInfor = activityBasicInforDao.selectByPrimaryKey(pkId);
				 if(activityBasicInfor != null) {
					 SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH");
					 String activitySignStart = sdf.format(activityBasicInfor.getActivitySignStart());
					 String activitySignEnd = sdf.format(activityBasicInfor.getActivitySignEnd());
					 String activityEndDate = sdf.format(activityBasicInfor.getActivityEndDate());
					 String activityStartDate = sdf.format(activityBasicInfor.getActivityStartDate());
					 activityBasicInfor.setRemark1(activitySignStart);
					 activityBasicInfor.setRemark2(activitySignEnd);
					 activityBasicInfor.setRemark3(activityEndDate);
					 activityBasicInfor.setRemark4(activityStartDate);
					 if("一星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("1");
					 }else if("二星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("2");
					 }else if("三星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("3");
					 }else if("四星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("4");
					 }else if("五星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("5");
					 }
					// 本机地址
					List<EsbmmCode> code = esbmmCodeService.getAddress();
					String address = code.get(0).getCodeValue();
					String URL = address+activityBasicInfor.getPictureURL();
					activityBasicInfor.setPictureURL(URL);
					 map.put("activityBasicInfor", activityBasicInfor);
					 state = "00,活动信息详情查询成功";
					 Log.getLogger().info("活动信息详情查询成功");
					 map.put("state", state);
					 String json1 = JSONArray.fromObject(map).toString();
					 return json1;
				 }else {
					 state = "01,没有查询到相关活动信息，查询失败";
					 Log.getLogger().info("没有查询到相关活动信息，查询失败");
					 map.put("state", state);
					 String json1 = JSONArray.fromObject(map).toString();
					 return json1;
				 }
			 }else {
				 state = "01,活动ID为空或不合法，查询失败，请核实";
				 Log.getLogger().info("活动ID为空或不合法，查询失败，请核实");
				 map.put("state", state);
				 String json1 = JSONArray.fromObject(map).toString();
				 return json1;
			 }
		}else {
			@SuppressWarnings("unchecked")
			List<ActivityBasicInfor> activityInforList = activityBasicInforDao.queryByList1();
			if(activityInforList.size()>0) {
				for (ActivityBasicInfor activityBasicInfor : activityInforList) {
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH");
					 String activitySignStart = sdf.format(activityBasicInfor.getActivitySignStart());
					 String activitySignEnd = sdf.format(activityBasicInfor.getActivitySignEnd());
					 String activityEndDate = sdf.format(activityBasicInfor.getActivityEndDate());
					 String activityStartDate = sdf.format(activityBasicInfor.getActivityStartDate());
					 if("一星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("1");
					 }else if("二星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("2");
					 }else if("三星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("3");
					 }else if("四星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("4");
					 }else if("五星级".equals(activityBasicInfor.getActivityAttendCondition())) {
						 activityBasicInfor.setCustomerLevel("5");
					 }
					 activityBasicInfor.setRemark1(activitySignStart);
					 activityBasicInfor.setRemark2(activitySignEnd);
					 activityBasicInfor.setRemark3(activityEndDate);
					 activityBasicInfor.setRemark4(activityStartDate);
					// 本机地址
					List<EsbmmCode> code = esbmmCodeService.getAddress();
					String address = code.get(0).getCodeValue();
					String URL = address+activityBasicInfor.getPictureURL();
					activityBasicInfor.setPictureURL(URL);
				}
				map.put("activityInforList", activityInforList);
				 state = "00,活动信息查询成功";
				 Log.getLogger().info("活动信息查询成功");
				 map.put("state", state);
				 String json1 = JSONArray.fromObject(map).toString();
				 return json1;
			}else {
				 state = "01,活动信息查询无结果，请核实";
				 Log.getLogger().info("活动信息查询无结果，请核实");
				 map.put("state", state);
				 String json1 = JSONArray.fromObject(map).toString();
				 return json1;
			}
		}
	}
}

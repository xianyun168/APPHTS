package  com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.DepartDao;

@Service("departService")
public class DepartService<T> extends BaseService<T> {

	@Autowired
	private DepartDao<T> dao;

	public DepartDao<T> getDao() {
		return dao;
	}

	public List<T> queryByParentId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("parentDepartId", id);
		return getDao().queryByParentId(paramMap);
	}	
	public List<T> query() throws Exception{
		return getDao().query();
	}
	
	public String queryJK(String pkId){
		return getDao().queryJK(pkId);
	}
}

package com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.RoleDepartDao;
import com.ciitc.webServices.dao.SysPermissionInfoDao;
import com.ciitc.webServices.model.RoleDepartInfo;
import com.ciitc.webServices.model.SysPermissionInfo;

@Service("roleDepartService")
public class RoleDepartService<T> extends BaseService<T>  {
	@Autowired
	private RoleDepartDao<T> dao;

	public RoleDepartDao<T> getDao() {
		return dao;
	}
	

	@Autowired
	private SysPermissionInfoDao<T> sysdao;

	public SysPermissionInfoDao<T> getSysdao() {
		return sysdao;
	}
	
	public List<T> queryByRoleId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roleId", id);
		return getDao().queryByRoleId(paramMap);
	}
	
	
	public List<T> queryRoleUserByRoleId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roleId", id);
		return getSysdao().queryRoleUserByRoleId(paramMap);
	}
	
	public List<T> queryByDepartId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("departId", id);
		return getDao().queryByDepartId(paramMap);
	}
	
	public List<T> queryByRoleIdAndDepartId(String roleId,String departId) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("departId", departId);
		paramMap.put("roleId", roleId);
		return getDao().queryByRoleIdAndDepartId(paramMap);
	}
	/**
	 * 根据roleId 删除角色关联数据
	 * @param roleId
	 */
	public void deleteByRoleId(String roleId) {
		List<RoleDepartInfo> departOfRoleList = null;
		List<SysPermissionInfo> roleUsers = null;
		try {
			// 角色菜单关联
			departOfRoleList = (List<RoleDepartInfo>) this.queryByRoleId(roleId);
			if (departOfRoleList != null && departOfRoleList.size()>0) {
				for (RoleDepartInfo roleDepart : departOfRoleList) {
					dao.delete(roleDepart.getPkId());	
				}
			}
			// 角色用户关联
			roleUsers = (List<SysPermissionInfo>) queryRoleUserByRoleId(roleId);
		    for (SysPermissionInfo roleUser : roleUsers) {
		    	sysdao.delete(roleUser.getPkId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

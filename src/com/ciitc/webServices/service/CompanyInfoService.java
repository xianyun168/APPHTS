package com.ciitc.webServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.CompanyInfoDao;
import com.ciitc.webServices.model.CompanyInfo;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
@Service("companyInfoService")
public class CompanyInfoService<T> extends BaseService<T> {
	@Autowired
	private CompanyInfoDao<T> dao;
	public CompanyInfoDao<T> getDao() {
		return dao;
	}
	//查询公司信息
	public CompanyInfo selectByPrimaryKey() {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKey();
	}
	//修改公司基本信息
	public void updateByPrimaryKeySelective(CompanyInfo companyInfo) {
		// TODO Auto-generated method stub
		dao.updateByPrimaryKeySelective(companyInfo);
	}
	//添加公司信息
	public void insertSelective(CompanyInfo companyInfo) {
		// TODO Auto-generated method stub
		dao.insertSelective(companyInfo);
	}

}

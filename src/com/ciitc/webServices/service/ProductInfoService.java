package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.ProductInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.ProductInfoDao;
import com.ibm.disthub2.impl.formats.Envelop.Constants.pubendsTable_type;

import cfca.com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("productInfoService")
public class ProductInfoService<T> extends BaseService<T> {

	@Autowired
	private ProductInfoDao<T> dao;

	@Override
	public BaseDao<T> getDao() {
		// TODO Auto-generated method stub
		return dao;
	}

	public List<T> queryList(Map<String, Object> paramMap) throws Exception {
		return dao.queryList(paramMap);
	}
	
	public List<T> queryByProfit(Map<String, Object> paramMap) throws Exception {
		return dao.queryByProfit(paramMap);
	}
	
	public List<T>queryByPermin(Map<String, Object> paramMap) throws Exception{
		return dao.queryByPermin(paramMap);
	}
	
	public List<T> queryByDeadline(Map<String, Object> paramMap) throws Exception{
		return dao.queryByDeadline(paramMap);
	}

	public int queryByCount(Map<String, Object> paramMapr) throws Exception {
		return dao.queryByCount(paramMapr);
	}

	public void deleteALL() {
		dao.deleteALL();
	}

	public List<T> queryALL() {
		return dao.queryALL();
	}
	//根据产品编号查询产品名称
	public String queryByPid(String pkId) {
		// TODO Auto-generated method stub
		return dao.queryByPid(pkId);
	}
	//更新crm折标系数
	public int updateCrmCoefficient(ProductInfo record){
		return dao.updateCrmCoefficient(record);
	}
}

package com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.CPTbrokertreeDao;
import com.ciitc.webServices.model.CPTbrokertree;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("cPTbrokertreeService")
public class CPTbrokertreeService<T> extends BaseService<T>{

	@Autowired
	private CPTbrokertreeDao<T> dao;

	public CPTbrokertreeDao<T> getDao() {
		return dao;
	}
	
	int deleteByPrimaryKey(String cNodecode) {
		return getDao().deleteByPrimaryKey(cNodecode);
	};

    int insert(CPTbrokertree record) {;
    return getDao().insert(record);
    }

    int insertSelective(CPTbrokertree record) {
    	return getDao().insertSelective(record);
    };


    int updateByPrimaryKeySelective(CPTbrokertree record) {
    	return getDao().updateByPrimaryKeySelective(record);
    };

    int updateByPrimaryKey(CPTbrokertree record) {
    	return getDao().updateByPrimaryKey(record);
    };


	public List<T> queryByParentId(Object object) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("cParentcode", object);
		return getDao().queryByParentId(paramMap);
	}	

}
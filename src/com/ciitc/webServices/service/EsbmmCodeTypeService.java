



































































































































































































package  com.ciitc.webServices.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.vo.CodeFactoryVo;

@Service("esbmmCodeTypeService")
public class EsbmmCodeTypeService<T> extends BaseService<T> {

	@Autowired
	private EsbmmCodeTypeDao<T> dao;

	public EsbmmCodeTypeDao<T> getDao() {
		return dao;
	}
	
	@Autowired
	private EsbmmCodeDao<T> codeDao;
	

	
	public EsbmmCodeDao<T> getCodeDao() {
		return codeDao;
	}

	
	public List<EsbmmCodeType> queryAll(){
		return  getDao().queryAll();
	}
	public EsbmmCode getCodeName(Map<String, Object> paramMap){
		EsbmmCode esbmmCode=this.getCodeDao().getCodeName(paramMap);
		return esbmmCode;
	}
	
	public EsbmmCode queryByCodeName(Map<String, Object> paramMap){
		EsbmmCode esbmmCode=this.getCodeDao().queryByCodeName(paramMap);
		return esbmmCode;
	}
	
	public List<CodeFactoryVo> getCodeFactory(Map<String, Object> paramMap) {
		List<CodeFactoryVo> list = new ArrayList<CodeFactoryVo>();
		list = (List<CodeFactoryVo>)this.getDao().getCodeFactory(paramMap);
		return list;
	}
	
	/***
	 *级联删除字典表
	 *先删除子表的内容
	 *在删除主表的值 
	 */
	public void deletecascade(Object id,Object id1){
		getCodeDao().deleteByTypeId(id);
		getDao().delete(id1);
	}
	
	/***
	 * 查询数据字典子节点是否存在
	 * @param obj 数据字典ID
	 * @return
	 */
	public List<EsbmmCode> queryCodeByTypeId(Object obj){
		List<EsbmmCode> codeList = getCodeDao().queryByTypeId(obj);
		return codeList;
	}
	
	public List<EsbmmCodeType> queryCodeTypeByTypeId(String obj){
		List<EsbmmCodeType> codeTypeList =  getDao().queryById(obj);
		return codeTypeList;
	}
	
	public List<EsbmmCodeType> checkCodeType(Map<String, Object> paramMap){
		return  getDao().checkCodeType(paramMap);
	}
	
	/**
	 * yutao 2014-12-16
	 * @param typeName
	 * @return
	 */
	public List<EsbmmCodeType> queryByTypeName(String typeName){
		return getDao().queryByTypeName(typeName);
	}
}

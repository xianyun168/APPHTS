package  com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

import com.ciitc.webServices.dao.UserInfoDao;
import com.ciitc.webServices.model.UserInfo;

@Service("userInfoService")
public class UserInfoService<T> extends BaseService<T> {

	public static final String USER_ADMIN = "admin";
	@Autowired
	private UserInfoDao<T> dao;

	public UserInfoDao<T> getDao() {
		return dao;
	}

	public List<T> checkUserExits(String userCode,String password) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userCode", userCode);
		paramMap.put("password", password);
		return getDao().getUserExits(paramMap);
	}
	
	/**
	 * Add By LiuYing 服务路由，查询所有未分配某一服务ID的用户
	 * @param paramMap
	 * @return
	 * @throws Exception
	 */
	public List<T> getUserForRoute(Map<String, Object> paramMap) throws Exception{
		return getDao().getUserForRoute(paramMap);
	}
	public List<T> queryByCode(Map<String, Object> paramMap) throws Exception{
		return getDao().queryByCode(paramMap);
	}
	/**
	 * @param userCode
	 * @return exist 1:管理员,0:其它
	 */
	public int checkUser(String userCode) {
		
		int exist = 0;
		UserInfo user = (UserInfo) dao.queryById(userCode);
		if(USER_ADMIN.equals(user.getUserName())){
			exist = 1;
		}
		return exist;
	}
	
	/**
	 * Add By LiuYing 查询所有记录
	 * @return
	 * @throws Exception
	 */
	public List<T> queryAll() throws Exception{
		return getDao().queryAll();
	}
	
	/**
	 * Add By LiuYing 查询所有属于当前所选路由规则的用户信息（内部或外部）
	 * @param paramMap
	 * @return
	 */
	public List<T> queryByRoutePkId(Map<String, Object> paramMap){
		return dao.queryByRoutePkId(paramMap);
	}
	
	/**
	 * Add Mengyanbing 查询所有用户信息（内部或外部）
	 * @param paramMap
	 * @return
	 */
	public List<T> queryUsers(){
		return dao.queryUsers();
	}

	/*******************************************************************************
	 * 函数名:  queryUserCode
	 * 功能描述:  获取为给该服务授权的用户名
	 * 创 建 人: renqingqing
	 * 日 期: 2015年10月12日 下午5:09:44
	 * 修 改 人: 
	 ******************************************************************************/
	public List<T> queryUserCode(Map<String, Object> paramMap) {
		return getDao().queryUserCode(paramMap);
		
	}

	/*******************************************************************************
	 * 函数名:  queryExistUserList
	 * 功能描述:  获取该服务已经授权的用户信息
	 * 创 建 人: renqingqing
	 * 日 期: 2015年10月12日 下午5:27:48
	 * 修 改 人: 
	 ******************************************************************************/
	public List<T> queryExistUserList(Map<String, Object> paramMap) {
		return getDao().queryExistUserList(paramMap);
		
	}

	/*******************************************************************************
	 * 函数名:  getTokenByCode
	 * 功能描述:  根据usercode得到用户的token
	 * 创 建 人: renqingqing
	 * 日 期: 2015年10月19日 下午4:50:26
	 * 修 改 人: 
	 ******************************************************************************/
	public String getTokenByCode(Map<String, Object> paramMap) {
		return getDao().getTokenByCode(paramMap);
	}
	
	public UserInfo getTokenByCodeJK(String userCode){
		return getDao().getTokenByCodeJK(userCode);
	}

	public List<T> queryByName(Map<String, Object> paramMap) {
		return dao.queryByName(paramMap);
	}
	public List<T> queryByDepartPkId(Map<String, Object> paramMap) {
		return dao.queryByDepartPkId(paramMap);
	}


}	

package com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.ProvinceDao;
import com.ciitc.webServices.model.GoddsAddress;
import com.ciitc.webServices.model.Province;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("provinceService")
public class ProvinceService<T> extends BaseService<T> {
	@Autowired
	private ProvinceDao<T> dao;

	public ProvinceDao<T> getDao() {
		return dao;
	}
	public void deleteByPrimaryKey(Long pkId) {
		getDao().deleteByPrimaryKey(pkId);
		
	};

	public void insert(Province record) {
    	getDao().insert(record);
    }

    public void insertSelective(Province record) {
    	getDao().insertSelective(record);
    }

    public Province selectByPrimaryKey(Long pkId) {
    	return getDao().selectByPrimaryKey(pkId);
    }

    public void updateByPrimaryKeySelective(Province record) {
    	getDao().updateByPrimaryKeySelective(record);
    }

    public void updateByPrimaryKey(Province record) {
    	getDao().updateByPrimaryKey(record);
    }
    public List<Province> selectAll(){
    	return getDao().selectAll();
    }
    
	public Province selectById(String provinceid) {
		// TODO Auto-generated method stub
		return getDao().selectById(provinceid);
	}

}

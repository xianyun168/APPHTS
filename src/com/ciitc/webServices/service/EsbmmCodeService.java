package  com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.model.EsbmmCode;

@Service("esbmmCodeService")
public class EsbmmCodeService<T> extends BaseService<T> {

	@Autowired
	private EsbmmCodeDao<T> dao;

	public EsbmmCodeDao<T> getDao() {
		return dao;
	}
	@Autowired
	private EsbmmCodeTypeDao<T> codeTypeDao;

	public EsbmmCodeTypeDao<T> getCodeTypeDao(){
		return codeTypeDao;
	}
	
	/**
	 * yutao
	 * 2014-12-26
	 * 根据codeTypePkId获取符合条件的esbmmCode集合
	 * @param codeTypePkId
	 * @return
	 */
	public  List<EsbmmCode>  queryByTypeId(Object codeTypePkId){
		return (List<EsbmmCode>)getDao().queryByTypeId(codeTypePkId);
	};
	
	public  List<EsbmmCode>  queryByTypeIds(Object codeTypePkId){
		return (List<EsbmmCode>)getDao().queryByTypeIds(codeTypePkId);
	};
	
	public  List<EsbmmCode>  queryByIdcard(Object codeTypePkId){
		return (List<EsbmmCode>)getDao().queryByIdcard(codeTypePkId);
	};
	
	public void deleteByTypeId(Object id){
		getDao().deleteByTypeId(id);
	};
	
	public List<EsbmmCode> checkCode(Map<String, Object> paramMap){
		return  getDao().checkCode(paramMap);
	}

	/**
	 * yutao 按条件查询Esbmm_Code表
	 * @param paramMap
	 * @return
	 */
	public List<EsbmmCode> selectByContion(Map<String, Object> paramMap){
		return getDao().selectByContion(paramMap);
	}
	
	public EsbmmCode getCodeName(Map<String, Object> paramMap){
		return getDao().getCodeName(paramMap);
	}
	
	public EsbmmCode queryByCodeName(Map<String, Object> paramMap){
		return getDao().queryByCodeName(paramMap);
	}
	
	public EsbmmCode queryByCodeCode(Map<String, Object> paramMap){
		return  getDao().queryByCodeCode(paramMap);
	}
	
	public EsbmmCode findByCodeName(Map<String, Object>paramMap){
		return getDao().findByCodeName(paramMap);
	}
	public List<EsbmmCode> queryTypeName(Map<String, Object> paramMap){
		return getDao().queryTypeName(paramMap);
	}
	public List<EsbmmCode> queryByChineseName(Map<String, Object> paramMap){
		return getDao().queryByChineseName(paramMap);
	}
	/**
	 * yutao 
	 * 2015-1-7
	 * @param codeValue
	 * @return 省份名称[省份代码]
	 */
	public String getProvinceName(String codeValue){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("areaCode").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		paramMap.put("codeValue", codeValue);
		String codeChineseName=selectByContion(paramMap).get(0).getChineseName();
		return codeChineseName+"["+codeValue+"]";
	}
	/**
	 * yutao 
	 * 2015-1-7
	 * @param codeValue
	 * @return 所属系统[系统代码]
	 */
	public String getSystemName(String codeValue){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("itsSystem").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		paramMap.put("codeValue", codeValue);
		String codeChineseName=selectByContion(paramMap).get(0).getChineseName();
		return codeChineseName;
	}
	/**
	 * yutao 
	 * 2015-1-7
	 * @param codeValue
	 * @return 状态[状态代码]
	 */
	public String getFlagName(String codeValue){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("atomicserviceStatus").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		paramMap.put("codeValue", codeValue);
		String codeChineseName=selectByContion(paramMap).get(0).getChineseName();
		return codeChineseName;
	}
	
	public String getKsName(String codeValue){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("atomicKs").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		paramMap.put("codeValue", codeValue);
		String codeChineseName=selectByContion(paramMap).get(0).getChineseName();
		return codeChineseName;
	}
	/**
	 * yutao 
	 * 2015-4-8
	 *
	 * @return esbmmCode的list
	 */
	public List<EsbmmCode> getProvinceList(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("areaCode").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		
		return esbmmCodes;
		
	}
	public List<EsbmmCode> getcheckTypeList(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("checkType").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		
		return esbmmCodes;
		
	}
	public List<EsbmmCode> getitsSystemList(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("itsSystem").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		
		return esbmmCodes;
		
	}
	public List<EsbmmCode> getFlagNameList(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("atomicserviceStatus").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		
		return esbmmCodes;
		
	}
	public List<EsbmmCode> getAtomicKsList(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("atomicKs").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		
		return esbmmCodes;
		
	}
	
	public List<EsbmmCode> getCompanyNameList(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("companyEnglishCode").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		
		return esbmmCodes;
		
	}
	
	public List<EsbmmCode> getAddseList(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("ImgUploadFile").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		
		return esbmmCodes;
		
	}
	
	public List<EsbmmCode> getAddress(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("host").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
		
	}
	
	public List<EsbmmCode> getUploadAddress(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("UploadFile").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
		
	}
	//短信平台参数配置
	public List<EsbmmCode> getSMSAppId(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("SMSConfiguration").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
		
	}
	
	//CRM附件上传参数配置
	public List<EsbmmCode> getCrmUploadFile() {
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("CrmUploadFile").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
	}
	//APP版本号
		public List<EsbmmCode> getVersion() {
			//EsbmmCodeType表中省份 记录的主键
			String pkid = getCodeTypeDao().queryByTypeName("AppVesion").get(0).getPkId();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("codeTypePkId", pkid);
			List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
			return esbmmCodes;
		}
		//APP下载地址
		public List<EsbmmCode> getAppAddress() {
			//EsbmmCodeType表中省份 记录的主键
			String pkid = getCodeTypeDao().queryByTypeName("uploadAddress").get(0).getPkId();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("codeTypePkId", pkid);
			List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
			return esbmmCodes;
		}
	
	//消息推送参数配置
	public List<EsbmmCode> getCrmAppJpush() {
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("jpushConfig").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
	}
	
	//查询客户区间
	public List<EsbmmCode> getLevel(){
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("level").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
		
	}
	
	
	public String CodeByName(String codeName){
		
		return getDao().getCodeByName(codeName);
	}

	public List<EsbmmCode> connCRMscDb() {
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("CRMscDb").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
				
	}

	public List<EsbmmCode> connCRMsyDb() {
		//EsbmmCodeType表中省份 记录的主键
		String pkid = getCodeTypeDao().queryByTypeName("CRMsyDb").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
	}

	public void update1(EsbmmCode esbmmCode1) {
		getDao().update1(esbmmCode1);
		
	}

	public List<EsbmmCode> connKfCrm() {
		String pkid = getCodeTypeDao().queryByTypeName("KfCrm").get(0).getPkId();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", pkid);
		List<EsbmmCode> esbmmCodes=selectByContion(paramMap);
		return esbmmCodes;
	}

	
	
}

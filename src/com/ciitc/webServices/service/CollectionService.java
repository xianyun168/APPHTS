package com.ciitc.webServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.CollectionDao;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
@Service("collectionService")
public class CollectionService<T> extends BaseService<T> {
	@Autowired
	private CollectionDao<T> dao;

	public CollectionDao<T> getDao() {
		return dao;
	}
	//删除收藏列表里的这条数据
	public void deleteByCid(String pkId) {
		// TODO Auto-generated method stub
		dao.deleteByCid(pkId);
	}

}

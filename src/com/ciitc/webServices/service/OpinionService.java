package com.ciitc.webServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.OpinionDao;
import com.ciitc.webServices.model.Opinion;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
@Service("opinionService")
public class OpinionService<T> extends BaseService<T> {

	@Autowired
	private OpinionDao<T> dao;
	public OpinionDao<T> getDao() {
		return dao;
	}
	//根据id查询详细信息
	public Opinion selectByPrimaryKey(String pkId) {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKey(pkId);
	}
	//根据id修改意见建议的状态
	public void updateByPrimaryKeySelective(Opinion opinion2) {
		// TODO Auto-generated method stub
		dao.updateByPrimaryKeySelective(opinion2);
	}
}

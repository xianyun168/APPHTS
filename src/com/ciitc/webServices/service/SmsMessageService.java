package com.ciitc.webServices.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.SmsSendDao;
import com.ciitc.webServices.model.SmsSend;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("smsMessageService")
public class SmsMessageService<T> extends BaseService<T>{
	
	@Autowired
	private SmsSendDao<T> dao;

	public SmsSendDao<T> getDao() {
		return dao;
	}
	 public SmsSend queryByPhone(Map<String, Object> map) {
		 return getDao().queryByPhone(map);
	 }
	 //发送成功之后删除未发送
	public void deleteByPhone(Map<String, Object> map) {
		// TODO Auto-generated method stub
		getDao().deleteByPhone(map);
	}
}

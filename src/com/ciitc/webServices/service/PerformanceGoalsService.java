package  com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.PerformanceGoalsDao;
import com.ciitc.webServices.model.PerformanceGoals;


@Service("performanceGoalsService")
public class PerformanceGoalsService<T> extends BaseService<T> {

	@Autowired
	private PerformanceGoalsDao<T> dao;

	public PerformanceGoalsDao<T> getDao() {
		return dao;
	}
	public List<PerformanceGoals> queryList(String rank) {
		return dao.queryList(rank);
	}
	
	public List<PerformanceGoals> selectById(String ids) {
		return dao.selectById(ids);
	}
	public List<PerformanceGoals> selectByRank(String rank) {
		return dao.selectByRank(rank);
	}
	public void  insert(PerformanceGoals performanceGoals){
		 getDao().insert(performanceGoals);
	}
	public void  delete(String pkId){
		 getDao().delete(pkId);
	}
}

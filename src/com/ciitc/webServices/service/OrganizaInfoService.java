package com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.OrganizaInfoDao;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("organizaInfoService")
public class OrganizaInfoService<T> extends BaseService<T>{
	
	@Autowired
	private OrganizaInfoDao<T> dao;

	public OrganizaInfoDao<T> getDao() {
		return dao;
	}
	
	public List<T> queryByParentId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("parentId", id);
		return getDao().queryByParentId(paramMap);
	}	
	
}

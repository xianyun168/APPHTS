package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AppShoppingCarInforDao;
import com.ciitc.webServices.model.AppShoppingCarInfor;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月10日 上午11:17:20
 * 修 改 人: 
 ******************************************************************************/
@Service("appShoppingCarInforService")
public class AppShoppingCarInforService<T> extends BaseService<T>{

	@Autowired
	private AppShoppingCarInforDao<T> dao;
	
	public AppShoppingCarInforDao<T> getDao() {
		return dao;
	}

	public List<T> queryByinfo(Map<String,Object> map){
		return  getDao().queryByinfo(map);
	}
	
	public Integer queryCarCount(String userId){
		return getDao().queryCarCount(userId);
	}
	
	public Integer queryBynum(Map<String,Object> map){
		return getDao().queryBynum(map);
	}
	
	public void deleteAll(Map<String, Object> map){
		getDao().deleteAll(map);
	}
	//查询要提交的商品列表
	public List<AppShoppingCarInfor> queryByIds(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return getDao().queryByIds(paramMap);
	}

	//根据商品的id和用户的id查询购物车里的商品
	public List<AppShoppingCarInfor> queryByGoodsId(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return getDao().queryByGoodsId(map);
	}
	//根据购物车商品的id删除这条信息
	public void deleteById(String pkId) {
		// TODO Auto-generated method stub
		getDao().deleteById(pkId);
	}
}

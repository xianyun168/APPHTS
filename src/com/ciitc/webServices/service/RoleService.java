package  com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.RoleInfoDao;
import com.ciitc.webServices.model.RoleInfo;

@Service(" roleService")
public class RoleService<T> extends BaseService<T> {

	public static final String ROLE_ADMIN = "admin";
	@Autowired
	private RoleInfoDao<T> dao;

	public RoleInfoDao<T> getDao() {
		return dao;
	}

	/**
	 * @param roleId
	 * @return exist 1:管理员,0:其它
	 */
	public int checkRole(String roleId) {
		
		int exist = 0;
		RoleInfo role = (RoleInfo) dao.queryById(roleId);
		if(ROLE_ADMIN.equals(role.getRoleName())){
			exist = 1;
		}
		return exist;
	}
	
	/**
	 * @param roleId
	 * @return exist 1:管理员,0:其它
	 */
	public List<T> queryByName(Map<String, Object> paramMap) {
		return dao.queryByName(paramMap);
	}

	public List<RoleInfo> queryAll() {
		return dao.queryAll();
	}
	

}

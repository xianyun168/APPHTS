package com.ciitc.webServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.MessageRemindRuleDao;
import com.ciitc.webServices.model.MessageRemindRule;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("timeRemindRuleService")
public class TimeRemindRuleService<T> extends BaseService<T> {
	
	@Autowired
	private MessageRemindRuleDao<T> dao;

	public MessageRemindRuleDao<T> getDao() {
		return dao;
	}
	
	public MessageRemindRule queryByBirth(String flag) {
		return getDao().queryByBirth(flag);
	}
}

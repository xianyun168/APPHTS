package  com.ciitc.webServices.service;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
import cn.com.ins.sinosoft.framework.base.util.ClassReflectUtil;
import cn.com.ins.sinosoft.framework.base.util.DateUtil;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;

import com.ciitc.webServices.dao.OperateLogDao;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.UserInfo;

@Service("operateLogService")
public class OperateLogService<T> extends BaseService<T> {

	@Autowired
	private OperateLogDao<T> dao;

	public OperateLogDao<T> getDao() {
		return dao;
	}
	
	/*************************************************************************
     *  函数名:   addLog    
     *  功能描述: 保存日志的公用方法
     *  输入参数:  operateMode 操作模块，      LogContent 日志内容， operatetype 操作类型
     *  返 回 值:       
     *  异    常：IOException
     *  创 建 人: 张瑞
     *  日    期:  2014-11-24
     *  修 改 人:
     *  日    期:
   ************************************************************************ */
	public void addLog(String operateMode,String LogContent, String operatetype) {
	  // String id =   (String) ClassReflectUtil.setIdKeyValue(t, "pkId", UUID.randomUUID().toString());
		UserInfo userInfo=ResourceUtil.getSessionUserName();
		OperateLog  operateLog =new OperateLog();
		//主键
		operateLog.setPkId(UUID.randomUUID().toString());
		//操作内容
		operateLog.setOperateTrack(LogContent);
		//操作模块
		operateLog.setOperateMode(operateMode);
		//操作类型
		operateLog.setOperateType(operatetype);
		//操作人
		operateLog.setUserPkId(userInfo.getPkId());
		operateLog.setUserName(userInfo.getUserName());
		operateLog.setFlag("1");
//		operateLog.setRemark("11");
		//操作时间
		Date  date= 	DateUtil.getDate();
		operateLog.setOperateTime(date);
		try {
			ClassReflectUtil.setFieldValueNotNull(operateLog,"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.getDao().add(operateLog);
		
	}

}

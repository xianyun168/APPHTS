package com.ciitc.webServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

import com.ciitc.webServices.dao.OperateLogSelectDao;
@Service("operateLogSelectService")
public class OperateLogSelectService<T> extends BaseService<T>{

	@Autowired
	private OperateLogSelectDao<T> dao;
	
	public OperateLogSelectDao<T> getDao() {
		return dao;
	}

}

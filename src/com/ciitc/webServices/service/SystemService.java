package com.ciitc.webServices.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.MenuInfoDao;
import com.ciitc.webServices.dao.OperationDao;
import com.ciitc.webServices.dao.RoleDetailInfoDao;
import com.ciitc.webServices.dao.RoleInfoDao;
import com.ciitc.webServices.dao.SysPermissionInfoDao;
import com.ciitc.webServices.model.MenuInfo;
import com.ciitc.webServices.model.Operation;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.RoleInfo;
import com.ciitc.webServices.model.SysPermissionInfo;

@Service("systemService")
public class SystemService<T> extends BaseService<T>  {
	
	@Autowired
	private OperationDao<Operation> operationDao;
	@Autowired
	private RoleDetailInfoDao<RoleDetailInfo> roleDetailInfoDao;
	@Autowired
	private RoleInfoDao<RoleInfo> roleInfoDao;
	@Autowired
	private SysPermissionInfoDao<SysPermissionInfo> sysPermissionInfoDao;
	@Autowired
	private MenuInfoDao<MenuInfo> menuInfoDao;

	@Override
	public BaseDao<T> getDao() {
		return null;
	}
	
	/**
	 * 根据用户ID 和 菜单Id 获取 具有操作权限的按钮Codes
	 * @param roleId
	 * @param menuId
	 * @return
	 */
	public  Set<String> getOperationCodesByUserIdAndFunctionId(String userId,String menuId) {
		
		
		Set<String> operationCodes = new HashSet<String>();
		List<SysPermissionInfo> rUsers = sysPermissionInfoDao.queryByUserPkId(userId);
		for (SysPermissionInfo ru : rUsers) {
			RoleInfo role = roleInfoDao.queryById(ru.getRolePkId());
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("menuId", menuId);
			paramMap.put("roleId", role.getPkId());
			List<RoleDetailInfo> rFunctions = roleDetailInfoDao.queryByRoleIdAndMenuId(paramMap);
			if (null != rFunctions && rFunctions.size() > 0) {
				RoleDetailInfo tsRoleFunction = rFunctions.get(0);
				/// TODO getDescription 表字段修改
				if (null != tsRoleFunction.getDescription()) {
					String[] operationArry = tsRoleFunction.getDescription().split(",");
					for (int i = 0; i < operationArry.length; i++) {
						operationCodes.add(operationArry[i]);
					}
				}
			}
		}
		return operationCodes;
	}
	
	public List<String> findOperationCodes(String menuId){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("menuId", menuId);
		List<Operation> operations = operationDao.queryByMenuId(paramMap);
		List<String> list = new ArrayList<String>();
		if(operations != null && operations.size() > 0){
			for(Operation operation : operations){
				list.add(operation.getOperationCode());
			}
		} else {
			list = null;
		}
		
		return list;
	}
	
	public List<String> findOperationList(String userId,String menuId){
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("menuId", menuId);
		List<RoleDetailInfo> hasOperList = roleDetailInfoDao.queryOperationList(paramMap);
		List<String> list = new ArrayList<String>();
		if(hasOperList != null && hasOperList.size() > 0){
			for(RoleDetailInfo oper : hasOperList){
				list.add(oper.getDescription());
			}
		} else {
			list = null;
		}
		
		return list;
	}
	
	public boolean checkAuth(String userId,String requestPath) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("userId", userId);
		paramMap.put("requestPath", requestPath);

		int menuAuthCount = menuInfoDao.queryMenuAuthCount(paramMap);
		if(menuAuthCount==0){
			return false;
		}else{
			return true;
		}
	}
	public MenuInfo getMenuId(String path){
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("requestPath", path);
		List<MenuInfo> list = menuInfoDao.queryMenuByPath(paramMap);
		if(list != null && list.size() > 0){
			return menuInfoDao.queryMenuByPath(paramMap).get(0);
		}
		
		return null;
	}
}

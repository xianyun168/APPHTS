package com.ciitc.webServices.service;


import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.HTMessageInfoDao;
import com.ciitc.webServices.model.HTMessageInfo;
import com.ciitc.webServices.model.SignMessageQuery;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("hTMessageInfoService")
public class HTMessageInfoService<T> extends BaseService<T>{
	@Autowired
	private HTMessageInfoDao<T> dao;

	public HTMessageInfoDao<T> getDao() {
		return dao;
	}
	public void insertSelective(HTMessageInfo hTMessageInfo) {
		// TODO Auto-generated method stub
		dao.insertSelective(hTMessageInfo);
	}
	public List<HTMessageInfo> queryByBirthday(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return dao.queryByBirthday(map);
	}
	
}

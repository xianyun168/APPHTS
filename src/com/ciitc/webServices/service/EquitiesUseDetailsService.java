package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.EquitiesUseDetailsDao;
import com.ciitc.webServices.model.EquitiesUseDetails;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("equitiesUseDetailsService")
public class EquitiesUseDetailsService<T> extends BaseService<T> {
	@Autowired
	private EquitiesUseDetailsDao<T> dao;

	public EquitiesUseDetailsDao<T> getDao() {
		return dao;
	}

	public List<EquitiesUseDetails> queryByCustomerIds(String customerId) {
		// TODO Auto-generated method stub
		return dao.queryByCustomerIds(customerId);
	}

	public Integer queryByCustomerId(Map<String, Object> mapGK) {
		// TODO Auto-generated method stub
		return dao.queryByCustomerId(mapGK);
	}

}

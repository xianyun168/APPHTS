package  com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.CPTtrustcontractReqDao;
import com.ciitc.webServices.model.CPTtrustcontractReq;

@Service("cPTtrustcontractReqService")
public class CPTtrustcontractReqService<T> extends BaseService<T> {

	@Autowired
	private CPTtrustcontractReqDao<T> dao;

	public CPTtrustcontractReqDao<T> getDao() {
		return dao;
	}
	 //获取持仓产品列表
	public List<CPTtrustcontractReq> queryChiCangList(String cMobileno) {
		return dao.queryChiCangList(cMobileno);
	}
	//获取持仓总收益
			public List<CPTtrustcontractReq> queryChiCangMoney(String cMobileno) {
				return dao.queryChiCangMoney(cMobileno);
			}
	//获取收益列表
	public List<CPTtrustcontractReq> queryshoyiList(String cMobileno) {
				return dao.queryshoyiList(cMobileno);
		}	
			
			
}

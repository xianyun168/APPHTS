package com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.ActivityBasicInforDao;
import com.ciitc.webServices.model.ActivityBasicInfor;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("activiteService")
public class ActiviteService<T> extends BaseService<T>{

	@Autowired
	private ActivityBasicInforDao<T> dao;

	public ActivityBasicInforDao<T> getDao() {
		return dao;
	}
	//根据状态查询活动列表
	public List<ActivityBasicInfor> queryByState(String state) {
		// TODO Auto-generated method stub
		return dao.queryByState(state);
	}
	//更新活动状态
	public void updateByState(ActivityBasicInfor activityBasicInfor) {
		// TODO Auto-generated method stub
		dao.updateByState(activityBasicInfor);
	}
}

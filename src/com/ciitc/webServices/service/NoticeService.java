package com.ciitc.webServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.NoticeDao;
import com.ciitc.webServices.model.Notice;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("noticeService")
public class NoticeService<T> extends BaseService<T>  {
	@Autowired
	private NoticeDao<T> dao;

	public NoticeDao<T> getDao() {
		return dao;
	}

	public Notice selectAll() {
		// TODO Auto-generated method stub
		return dao.selectAll();
	}

	public void insertSelective(Notice notice) {
		// TODO Auto-generated method stub
		dao.insertSelective(notice);
	}

	public void updateByPrimaryKeySelective(Notice notice) {
		// TODO Auto-generated method stub
		dao.updateByPrimaryKeySelective(notice);
	}

}

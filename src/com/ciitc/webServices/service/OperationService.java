package  com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.OperationDao;

@Service("operationService")
public class OperationService<T> extends BaseService<T> {

	@Autowired
	private OperationDao<T> dao;

	public OperationDao<T> getDao() {
		return dao;
	}
	
	public List<T> queryByMenuId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("menuId", id);
		return getDao().queryByMenuId(paramMap);
	}

}

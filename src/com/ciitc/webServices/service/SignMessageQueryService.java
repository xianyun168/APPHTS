package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.SignMessageQueryDao;
import com.ciitc.webServices.model.SignMessageQuery;

import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("signMessageQueryService")
public class SignMessageQueryService<T> extends BaseService<T>{
	
	@Autowired
	private SignMessageQueryDao<T> dao;

	public SignMessageQueryDao<T> getDao() {
		return dao;
	}
	
	 public int queryByCount1(Map<String, Object> paramMap) throws Exception
	   {
	     return getDao().queryByCount1(paramMap);
	   }
	 
	 
	   public List<T> queryByList1(Map<String, Object> paramMap)
	     throws Exception
	   {
	     Integer rowCount = Integer.valueOf(queryByCount1(paramMap));
	     BasePage page = (BasePage)paramMap.get("page");
	     page.getPager().setRowCount(rowCount.intValue());
	     paramMap.put("page", page);
	     return getDao().queryByList1(paramMap);
	   }

	public List<SignMessageQuery> queryByList2(Map<String, Object> paramMap) {
		
		return getDao().queryByList2(paramMap);
	}

	public void updateByPkId(Map<String, Object> map) {
		// TODO Auto-generated method stub
		getDao().updateByPkId(map);
	}

}

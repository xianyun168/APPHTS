package com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.BatchAddDao;
import com.ciitc.webServices.model.AppUserInfor;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月26日 下午7:43:45
 * 修 改 人: 
 ******************************************************************************/
@Service("batchAddService")
public class BatchAddService<T> extends BaseService<T>{

	@Autowired
	private BatchAddDao<T> dao;
	@Override
	public BatchAddDao<T> getDao() {
		return dao;
	}
	
	public T queryByPkId(String userId){
		return getDao().queryByPkId(userId);
	}
	
	public void updatebyuserId(AppUserInfor appUserInfor){
		getDao().updatebyuserId(appUserInfor);
	}

	public T queryByuserId(String pkId){
		return getDao().queryByuserId(pkId);
	}
	//根据pkId增加单个用户的积分
	public void addUserInfoOne(AppUserInfor appUserInfor) {
		// TODO Auto-generated method stub
		getDao().addUserInfoOne(appUserInfor);
	}
	//
	public List<AppUserInfor> queryByAll() {
		// TODO Auto-generated method stub
		return getDao().queryByAll();
	}
}

package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.HelpDocumentDao;
import com.ciitc.webServices.model.HelpDocument;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("helpDocumentService")
public class HelpDocumentService<T> extends BaseService<T> {

	@Autowired
	private HelpDocumentDao<T> dao;
	public HelpDocumentDao<T> getDao() {
		return dao;
	}
	//检查标题是否已经存在
	public List<HelpDocument> queryByTitle(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return dao.queryByTitle(paramMap);
	}
	//新增帮助文档
	public void insertSelective(HelpDocument helpDocument) {
		// TODO Auto-generated method stub
		dao.insertSelective(helpDocument);
	}
	//修改帮助文档
	public void updateByPrimaryKeySelective(HelpDocument helpDocument) {
		// TODO Auto-generated method stub
		dao.updateByPrimaryKeySelective(helpDocument);
	}
	//根据id查询帮助文档信息
	public HelpDocument selectByPrimaryKey(String pkId) {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKey(pkId);
	}
	//根据id删除帮助文档
	public void deleteByPrimaryKey(String pkId) {
		// TODO Auto-generated method stub
		dao.deleteByPrimaryKey(pkId);
	}
	

}

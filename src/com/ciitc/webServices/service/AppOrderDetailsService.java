package com.ciitc.webServices.service;

import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AppOrderDetailsDao;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月26日 上午11:04:24
 * 修 改 人: 
 ******************************************************************************/
@Service("appOrderDetailsService")
public class AppOrderDetailsService<T> extends BaseService<T>{
	
	@Autowired
	private AppOrderDetailsDao<T> dao;

	public AppOrderDetailsDao<T> getDao() {
		return dao;
	}
	
	public List<T> queryByorderId (String orderId){
		return getDao().queryByorderId(orderId);
	}

}

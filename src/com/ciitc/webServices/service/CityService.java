package com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.CityDao;
import com.ciitc.webServices.model.Area;
import com.ciitc.webServices.model.City;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
@Service("cityService")
public class CityService<T> extends BaseService<T> {
	@Autowired
	private CityDao<T> dao;

	public CityDao<T> getDao() {
		return dao;
	}
	public void deleteByPrimaryKey(Long pkId) {
		getDao().deleteByPrimaryKey(pkId);
		
	};

	public void insert(City record) {
    	getDao().insert(record);
    };

    public void insertSelective(City record) {
    	getDao().insertSelective(record);
    };

    public List<City> selectByPrimaryKey(String father) {
    	return getDao().selectByPrimaryKey(father);
    };

    public void updateByPrimaryKeySelective(City record) {
    	getDao().updateByPrimaryKeySelective(record);
    };

    public void updateByPrimaryKey(City record) {
    	getDao().updateByPrimaryKey(record);
    }
	public City selectById(String areaid) {
		// TODO Auto-generated method stub
		return getDao().selectById(areaid);
	};

}

package  com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.ProfitDao;
import com.ciitc.webServices.model.Profit;

@Service("profitService")
public class ProfitService<T> extends BaseService<T> {

	@Autowired
	private ProfitDao<T> dao;

	public ProfitDao<T> getDao() {
		return dao;
	}
	public List<Profit> queryByPhoneList(String phoneNumber) {
		return dao.queryByPhoneList(phoneNumber);
	}
	public List<Profit> queryPositionList(String phoneNumber) {
		return dao.queryByPhoneList(phoneNumber);
	}
	public List<Profit> queryByHoldingIncomeList(String phoneNumber) {
		return dao.queryByHoldingIncomeList(phoneNumber);
	}
	public List<Profit> queryByTotalAmountList(String phoneNumber) {
		return dao.queryByHoldingIncomeList(phoneNumber);
	}
	public List<Profit> queryTransactionList(String phoneNumber) {
		return dao.queryTransactionList(phoneNumber);
	}
	public List<Profit> queryHistoryProfit(String phoneNumber) {
		return dao.queryHistoryProfit(phoneNumber);
	}
	public List<Profit> queryProfitDetailed(String productId) {
		return dao.queryProfitDetailed(productId);
	}
	
}

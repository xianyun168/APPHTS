package com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AppOrderInforDao;
import com.ciitc.webServices.model.AppOrderInfor;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("appOrderInforService")
public class AppOrderInforService<T> extends BaseService<T>{
	
	@Autowired
	private AppOrderInforDao<T> dao;

	public AppOrderInforDao<T> getDao() {
		return dao;
	}

	public void insertSelective(AppOrderInfor appOrderInfor) {
		// TODO Auto-generated method stub
		dao.insertSelective(appOrderInfor);
	}
	//查询订单列表
	public List<AppOrderInfor> queryByuser(String purchaseId) {
		// TODO Auto-generated method stub
		return dao.queryByuser(purchaseId);
	}
	//根据订单编号查询订单信息
	public List<AppOrderInfor> queryByOrderId(String orderId) {
		// TODO Auto-generated method stub
		return dao.queryByOrderId(orderId);
	}
	
	public void updateByPrimaryKey(AppOrderInfor appOrderInfor){
		dao.updateByPrimaryKey(appOrderInfor);
	}

	public AppOrderInfor selectByPrimaryKey(String ordersId){
		return dao.selectByPrimaryKey(ordersId);
	}
	
	public AppOrderInfor queryOrderId(String orderId){
		return  dao.queryOrderId(orderId);
	}
}

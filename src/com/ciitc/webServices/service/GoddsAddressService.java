package com.ciitc.webServices.service;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.GoddsAddressDao;
import com.ciitc.webServices.model.GoddsAddress;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("goddsAddressService")
public class GoddsAddressService<T> extends BaseService<T>{

	@Autowired
	private GoddsAddressDao<T> dao;
	public GoddsAddressDao<T> getDao() {
		return dao;
	}
	public void deleteByPrimaryKey(String pkId) {
		getDao().deleteByPrimaryKey(pkId);
		
	};

	public void insert(GoddsAddress record) {
    	getDao().insert(record);
    };

    public void insertSelective(GoddsAddress record) {
    	getDao().insertSelective(record);
    };

    public GoddsAddress selectByPrimaryKey(String pkId) {
    	return getDao().selectByPrimaryKey(pkId);
    };

    public void updateByPrimaryKeySelective(GoddsAddress record) {
    	getDao().updateByPrimaryKeySelective(record);
    };

    public void updateByPrimaryKey(GoddsAddress record) {
    	getDao().updateByPrimaryKey(record);
    }
	public GoddsAddress selectByState(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return getDao().selectByState(map);
	};

}

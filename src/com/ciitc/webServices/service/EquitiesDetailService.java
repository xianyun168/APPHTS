package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.EquitiesDetailDao;
import com.ciitc.webServices.model.EquitiesDetail;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("equitiesDetailService")
public class EquitiesDetailService<T> extends BaseService<T> {

	@Autowired
	private EquitiesDetailDao<T> dao;

	public EquitiesDetailDao<T> getDao() {
		return dao;
	}

	public List<EquitiesDetail> queryByLevel(Map<String, Object> paramMap) {
		// TODO Auto-generated method stub
		return dao.queryByLevel(paramMap) ;
	}

}

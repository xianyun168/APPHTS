package com.ciitc.webServices.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AreaDao;
import com.ciitc.webServices.model.Area;
import com.ciitc.webServices.model.GoddsAddress;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("areaService")
public class AreaService<T> extends BaseService<T> {
	@Autowired
	private AreaDao<T> dao;

	public AreaDao<T> getDao() {
		return dao;
	}
	public void deleteByPrimaryKey(Long pkId) {
		getDao().deleteByPrimaryKey(pkId);
		
	};

	public void insert(Area record) {
    	getDao().insert(record);
    };

    public void insertSelective(Area record) {
    	getDao().insertSelective(record);
    };

    public List<Area> selectByPrimaryKey(String father) {
    	return getDao().selectByPrimaryKey(father);
    };

    public void updateByPrimaryKeySelective(Area record) {
    	getDao().updateByPrimaryKeySelective(record);
    };

    public void updateByPrimaryKey(Area record) {
    	getDao().updateByPrimaryKey(record);
    }
	public Area selectById(String areaid) {
		// TODO Auto-generated method stub
		return getDao().selectById(areaid);
	};

}

package  com.ciitc.webServices.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.CoefficientDao;
import com.ciitc.webServices.model.Coefficient;

@Service("coefficientService")
public class CoefficientService<T> extends BaseService<T> {

	@Autowired
	private CoefficientDao<T> dao;

	public CoefficientDao<T> getDao() {
		return dao;
	}
	public String queryById(String pkId) {
	return dao.queryById(pkId);
  }
	public List<Coefficient> queryByDate(String productId) {
		return dao.queryByDate(productId);
	}
	
	public List<Coefficient> selectByDate(Map<String, Object> paramMap) {
		return dao.selectByDate(paramMap);
	}
	 public void insert(Coefficient coefficient) {
		// TODO Auto-generated method stub
		 dao.insert(coefficient);
	}
	public void updateCoefficient(Date scalinDate) {
		 dao.updateCoefficient(scalinDate);
		
	}
 }

package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AppCustomerInforDao;

import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.service.BaseService;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年3月22日 下午2:41:34
 * 修 改 人: 
 ******************************************************************************/
@Service("appCustomerInforService")
public class AppCustomerInforService<T> extends BaseService<T>{
	
	@Autowired
	private AppCustomerInforDao<T> dao;

	@Override
	public AppCustomerInforDao<T> getDao() {
		
		return dao;
	}
	
   public int queryByCount2(Map<String, Object> paramMap) throws Exception
   {
     return getDao().queryByCount(paramMap);
   }
 
 
   public List<T> queryByList2(Map<String, Object> paramMap)
     throws Exception
   {
     Integer rowCount = Integer.valueOf(queryByCount2(paramMap));
     BasePage page = (BasePage)paramMap.get("page");
     page.getPager().setRowCount(rowCount.intValue());
     paramMap.put("page", page);
     return getDao().queryByList2(paramMap);
   }

}

package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.model.AppUserInfor;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
@Service("appUserInforService")
public class AppUserInforService <T> extends BaseService<T>{

	@Autowired
	private AppUserInforDao<T> dao;
	public AppUserInforDao<T> getDao() {
		return dao;
	}
	public AppUserInfor queryByPhone(String phoneNumber){
		return dao.queryByPhone(phoneNumber);
	}
	public void insertInfor(AppUserInfor appUserInfor) {
		dao.insertInfor(appUserInfor);
	}
	
	public void updateRegistration(AppUserInfor appUserInfor) {
		dao.updateRegistration(appUserInfor);
	}
	
   public List<T> queryByList1(Map<String, Object> paramMap)throws Exception{
     return getDao().queryByList1(paramMap);
   }
   
   public AppUserInfor selectByPrimaryKey(String pkId){
	   return dao.selectByPrimaryKey(pkId);
			   
   }
   
   public AppUserInfor querykhId(String pkId){
		return dao.querykhId(pkId);
	}
   
   public AppUserInfor queryCrmId(String pkId){
		return dao.queryCrmId(pkId);
	}
	public List<AppUserInfor> queryByPkId(String pkId){
		return dao.queryByPkId(pkId);
	}
	
	public AppUserInfor queryByPhoneId(String pkId){
		return dao.queryByPhoneId(pkId);
	}
	//查询生日
	public List<AppUserInfor> queryByBirthday(String birthday) {
		// TODO Auto-generated method stub
		return dao.queryByBirthday(birthday);
	}
	public List<AppUserInfor> queryAll() {
		// TODO Auto-generated method stub
		return dao.queryALL();
	}
	public Integer queryByCounts(String customerId) {
		// TODO Auto-generated method stub
		return dao.queryByCounts(customerId);
	}
	public List<AppUserInfor> queryTest() {
		// TODO Auto-generated method stub
		return dao.queryTest();
	}
	public void update1(AppUserInfor appUserInfor) {
	     getDao().update1(appUserInfor);
	   }
}

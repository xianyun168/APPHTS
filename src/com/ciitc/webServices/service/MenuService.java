package  com.ciitc.webServices.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

import com.ciitc.webServices.dao.MenuInfoDao;
import com.ciitc.webServices.model.MenuInfo;

@Service("menuService")
public class MenuService<T> extends BaseService<T> {

	@Autowired
	private MenuInfoDao<T> dao;

	public MenuInfoDao<T> getDao() {
		return dao;
	}

	public List<T> queryByParentId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("parentMenuId", id);
		return getDao().queryByParentId(paramMap);
	}
	public List<T> query() throws Exception{
		return getDao().query();
	}
	
	public String initMenu(Map<String, List<MenuInfo>> menuMap) {
		StringBuffer sb = new StringBuffer();
		sb.append("<div id=\"nav\" class=\"easyui-accordion\" fit=\"true\" border=\"false\">");
		sb.append(getEasyuiMultistageTree(menuMap));
		sb.append("</div>");
		
		return sb.toString();
	}
	
	public String getEasyuiMultistageTree(Map<String, List<MenuInfo>> map) {
		StringBuffer menuString = new StringBuffer();
		List<MenuInfo> list = map.get("0");
		if(list!=null && list.size()>0){
			for (MenuInfo menu : list) {
				
				menuString.append("<div   title=\"" + menu.getMenuName()
						+ "\">"  );
				List<MenuInfo> child = new ArrayList<MenuInfo>();
				try {
					child = (List<MenuInfo>) this.queryByParentId(menu.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				int submenusize = child.size();
				if (submenusize == 0) {
					menuString.append("</div>");
				}
				if (submenusize > 0) {
					menuString.append("<ul class=\"easyui-tree\"  fit=\"false\" border=\"false\">");
				}
				menuString.append(getChildOfTree(menu,"1",map));
				if (submenusize > 0) {
					menuString.append("</ul></div>");
				}
			}
		}
		return menuString.toString();
	}
	
	/**
	 * 获取树形菜单
	 * @param parent
	 * @param level
	 * @param map
	 * @return
	 */
	private String getChildOfTree(MenuInfo parent,String level,Map<String, List<MenuInfo>> map){
		StringBuffer menuString = new StringBuffer();
		List<MenuInfo> list = map.get(level);
		for (MenuInfo menu : list) {
			if (menu.getParentMenuId().equals(parent.getPkId())){
				
				List<MenuInfo> child = null;
				try {
					child = (List<MenuInfo>) this.queryByParentId(menu.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				int lev=Integer.parseInt(level)+1;
				if(child.size()==0||!map.containsKey(String.valueOf(lev))){
					menuString.append(getLeafOfTree(menu));
				}else if(map.containsKey(String.valueOf(lev))){
					menuString.append("<li state=\"closed\""+" ><span>"+menu.getMenuName()+"</span>");
					menuString.append("<ul >");
					menuString.append(getChildOfTree(menu,String.valueOf(lev),map));
					menuString.append("</ul></li>");
				}
			}
		}
		return menuString.toString();
	}
	/**
	 * 获取叶子节点  ------树形菜单的叶子节点
	 * @param function
	 * @return
	 */
	private String getLeafOfTree(MenuInfo function){
		StringBuffer menuString = new StringBuffer();
		//menuString.append("<li iconCls=\"pictures\"> <a class=\""+function.getMenuName()+"\"  target=\"page\"  href=\""+function.getMenuPath() + "&clickMenuId=" + function.getPkId() +"\"><span class=\"nav\">"+function.getMenuName()+"</span></a></li>");
		menuString.append("<li iconCls=\"pictures\"> <a class=\""+function.getMenuName()+"\" onclick=\"goThisJsp(this);\"  target=\"page\"  href=\""+function.getMenuPath() + "&clickMenuId=" + function.getPkId() +"\"><span class=\"nav\">"+function.getMenuName()+"</span></a></li>");		

//		menuString.append("<li>");
//		menuString.append(" <a onclick=\"addTab(\'");
//		menuString.append(function.getMenuName());
//		menuString.append("\',\'");
//		menuString.append(function.getMenuPath());
//		menuString.append("&clickFunctionId=");
//		menuString.append(function.getPkId());
//		menuString.append("\')\"  title=\"");
//		menuString.append(function.getMenuName());
//		menuString.append("\" url=\"");
//		menuString.append(function.getMenuPath());
//		menuString.append("\" href=\"#\" ><span class=\"nav\" >");
//		menuString.append(function.getMenuName());
//		menuString.append("</span></a></li>");
		return menuString.toString();
	}
	public String initNav(Map<String, List<MenuInfo>> menuMap) {
		StringBuffer sb = new StringBuffer();
		sb.append(getEasyuiMenu(menuMap));
		return sb.toString();
	}
	
	public String getEasyuiMenu(Map<String, List<MenuInfo>> map) {
		StringBuffer menuString = new StringBuffer();
		StringBuffer childMenuString = new StringBuffer();
		menuString.append("<div class=\"easyui-panel\" style=\"padding:5px;\">");
		List<MenuInfo> list = map.get("0");
		if(list!=null &&list.size()>0){
			for (MenuInfo menu : list) {
				
				List<MenuInfo> child = new ArrayList<MenuInfo>();
				try {
					child = (List<MenuInfo>) this.queryByParentId(menu.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				int submenusize = child.size();
				if (submenusize == 0) {
					menuString.append("<a href=\"#\" class=\"easyui-linkbutton\" data-options=\"plain:true\">" + menu.getMenuName() + "</a>");
				}
				if (submenusize > 0) {
					menuString.append("<a href=\"#\" class=\"easyui-menubutton\" data-options=\"menu:'#"+menu.getPkId()+"'\">" + menu.getMenuName() + "</a>");
				}
				if (submenusize > 0) {
					childMenuString.append("<div id=\""+ menu.getPkId() +"\" style=\"width:auto;\">");
					childMenuString.append(getChildMenu(menu,"1",map));
					childMenuString.append("</div>");
				}
			}
		}
		menuString.append("</div>");
		menuString.append(childMenuString);
		return menuString.toString();
	}
	private String getChildMenu(MenuInfo parent,String level,Map<String, List<MenuInfo>> map){
		StringBuffer menuString = new StringBuffer();
		List<MenuInfo> list = map.get(level);
		if(list!=null && list.size()>0){
			for (MenuInfo menu : list) {
				if (menu.getParentMenuId().equals(parent.getPkId())){
					
					List<MenuInfo> child = null;
					try {
						child = (List<MenuInfo>) this.queryByParentId(menu.getPkId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					int lev=Integer.parseInt(level)+1;
					if(child.size()==0||!map.containsKey(String.valueOf(lev))){
						menuString.append("<div><a class=\""+menu.getMenuName()+"\"  target=\"page\"  href=\""+menu.getMenuPath() + "&clickMenuId=" + menu.getPkId() +"\"><span class=\"nav\">"+menu.getMenuName()+"</span></a></div>");
					}else if(map.containsKey(String.valueOf(lev))){
						menuString.append("<div><span>" + menu.getMenuName() + "</span><div>");
						menuString.append(getChildMenu(menu,String.valueOf(lev),map));
						menuString.append("</div></div>");
					}
				}
			}
		}
		return menuString.toString();
	}
	
	public List<T> getMenuName(String userCode) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("USER_CODE", userCode);
		return getDao().getMenuName(paramMap);
	}
	
	public String getMenuPath(String pkid) throws Exception{
		return getDao().getMenuPath(pkid);
	}
	
	
}

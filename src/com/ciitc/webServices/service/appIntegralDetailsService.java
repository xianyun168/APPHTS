package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AppGoodsInforDao;
import com.ciitc.webServices.dao.appIntegralDetailsDao;
import com.ciitc.webServices.model.appIntegralDetails;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月29日 下午6:53:57
 * 修 改 人: 
 ******************************************************************************/
@Service("appIntegralDetailsService")
public class appIntegralDetailsService<T> extends BaseService<T>{

	@Autowired
	private appIntegralDetailsDao<T> dao;
	
	@Override
	public appIntegralDetailsDao<T> getDao() {
		return dao;
	}
	
	public List<T> selectByuserId(Map<String,Object> map){
		return getDao().selectByuserId(map);
	}

	public List<T> queryByList1(Map<String, Object> paramMap)throws Exception{
	     return getDao().queryByList1(paramMap);
	}

	public void insertSelective(appIntegralDetails appIntegralDetails) {
		// TODO Auto-generated method stub
		getDao().insertSelective(appIntegralDetails);
	}

	public List<appIntegralDetails> queryByYX(String pkId) {
		// TODO Auto-generated method stub
		return getDao().queryByYX(pkId);
	}

	public appIntegralDetails queryByYXMin(String pkId) {
		// TODO Auto-generated method stub
		return getDao().queryByYXMin(pkId);
	}


	
}

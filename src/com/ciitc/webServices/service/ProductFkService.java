package com.ciitc.webServices.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.ProductFkDao;
import com.ciitc.webServices.model.ProductFk;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
@Service("productFkService")
public class ProductFkService<T> extends BaseService<T> {

	@Autowired
	private ProductFkDao<T> dao;

	public ProductFkDao<T> getDao() {
		return dao;
	}

	public ProductFk selectByPrimaryKey(String pkId) {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKey(pkId);
	}

	public void insertSelective(ProductFk productFk1) {
		// TODO Auto-generated method stub
		dao.insertSelective(productFk1);
	}

	public void updateByPrimaryKeySelective(ProductFk productFk1) {
		// TODO Auto-generated method stub
		dao.updateByPrimaryKeySelective(productFk1);
	}

	public ProductFk selectBypid(String pkId) {
		// TODO Auto-generated method stub
		return dao.selectBypid(pkId);
	}
	


}

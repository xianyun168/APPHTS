package com.ciitc.webServices.service;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.KnowledgeBaseDao;
import com.ciitc.webServices.model.KnowledgeBase;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("knowledgeBaseService")
public class KnowledgeBaseService<T> extends BaseService<T>{

	@Autowired
	private KnowledgeBaseDao<T> dao;
	public KnowledgeBaseDao<T> getDao() {
		return dao;
	}
	//检查标题是否已经存在
	public List<KnowledgeBase> queryByTitle(Map<String, Object> paramMap) {
		
		return dao.queryByTitle(paramMap);
	}
	//新增知识条目
	public void insertSelective(KnowledgeBase knowledgeBase) {
		// TODO Auto-generated method stub
		dao.insertSelective(knowledgeBase);
	}
	public KnowledgeBase selectByPrimaryKey(String pkId) {
		// TODO Auto-generated method stub
		return dao.selectByPrimaryKey(pkId);
	}
	//修改知识条目
	public void updateByPrimaryKeySelective(KnowledgeBase knowledgeBase) {
		// TODO Auto-generated method stub
		dao.updateByPrimaryKeySelective(knowledgeBase);
	}
	//根据id删除知识条目
	public void deleteByPrimaryKey(String pkId) {
		// TODO Auto-generated method stub
		dao.deleteByPrimaryKey(pkId);
	}
	

}

package com.ciitc.webServices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.model.CPTfundbroker;

import cn.com.ins.sinosoft.framework.base.service.BaseService;

@Service("cPTfundbrokerService")
public class CPTfundbrokerService<T> extends BaseService<T> {
	@Autowired
	private CPTfundbrokerDao<T> dao;

	public CPTfundbrokerDao<T> getDao() {
		return dao;
	}

	public CPTfundbroker queryByFid(String sendObject) {
		// TODO Auto-generated method stub
		return dao.queryByFid(sendObject);
	}
	public String selectNameByKey(String financialId) {
		// TODO Auto-generated method stub
		return dao.selectNameByKey(financialId);
	}

}

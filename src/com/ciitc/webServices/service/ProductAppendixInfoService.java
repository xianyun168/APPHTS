package com.ciitc.webServices.service;

import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;

import com.ciitc.webServices.dao.ProductAppendixInfoDao;
import com.ciitc.webServices.model.ProductAppendixInfo;

@Service("productappendixinfoService")
public class ProductAppendixInfoService<T> extends BaseService<T> {

	@Autowired
	private ProductAppendixInfoDao<T> dao;

	@Override
	public BaseDao<T> getDao() {
		return dao;
	}

	public ArrayList<ProductAppendixInfo> selectByPrimaryKey(String pkid) {
		return dao.selectByPrimaryKey(pkid);
	}
	
	public ArrayList<ProductAppendixInfo> selectByProductid(String id) {
		return dao.selectByProductid(id);
	}

	public int queryByCount(Map<String, Object> paramMapr) throws Exception {
		return dao.queryByCount(paramMapr);
	}

	public void deleteFile(String pkId) throws Exception{
		dao.deleteFile(pkId);
	}
}

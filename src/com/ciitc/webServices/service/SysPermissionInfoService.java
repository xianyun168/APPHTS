package  com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.SysPermissionInfoDao;
import com.ciitc.webServices.model.SysPermissionInfo;

@Service("sysPermissionInfoService")
public class SysPermissionInfoService<T> extends BaseService<T> {

	@Autowired
	private SysPermissionInfoDao<T> dao;

	public SysPermissionInfoDao<T> getDao() {
		return dao;
	}
	public List<SysPermissionInfo> queryByUserPkId(String userPkId){
		return getDao().queryByUserPkId(userPkId);
	}
	public void deleteByUserPkId(String userPkId){
		getDao().deleteByUserPkId(userPkId);
	}
	public List<T> queryRoleUserByRoleId(Map<String, Object> paramMap){
		return getDao().queryRoleUserByRoleId(paramMap);
	}
	public String getRoleNameByUserPkId(String userPkId){
		return getDao().getRoleNameByUserPkId(userPkId);
	}
	
	public List<SysPermissionInfo> getRoleIdByUserPkId(String userPkId){
		return getDao().getRoleIdByUserPkId(userPkId);
	}
}

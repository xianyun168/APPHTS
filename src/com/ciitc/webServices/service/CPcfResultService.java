package  com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.CPcfResultDao;
import com.ciitc.webServices.model.CPcfResult;
@Service("cPcfResultService")
public class CPcfResultService<T> extends BaseService<T> {

	@Autowired
	private CPcfResultDao<T> dao;

	public CPcfResultDao<T> getDao() {
		return dao;
	}
	//获取收益列表
		public List<CPcfResult> querymingxiList(String Contract_Id) {
					return dao.querymingxiList(Contract_Id);
			}	
		 //返息时间列表
/*		public List<CPcfResult> queryDateList(Map<String, Object> paramMap) {
					return dao.queryDateList(paramMap);
			}*/
		   public List<CPcfResult> queryDateList(Map<String, Object> paramMap)
				     throws Exception
				   {
				     Integer rowCount = Integer.valueOf(queryByCount(paramMap));
				     BasePage page = (BasePage)paramMap.get("page");
				     page.getPager().setRowCount(rowCount.intValue());
				     paramMap.put("page", page);
				     return getDao().queryDateList(paramMap);
				   }
		
		 //导出
		  public List<CPcfResult> queryfanxiList1(Map<String, Object> paramMap) {
					return dao.queryfanxiList(paramMap);
			}
		
		   public List<CPcfResult> queryfanxiList(Map<String, Object> paramMap)
				     throws Exception
				   {
				     Integer rowCount = Integer.valueOf(queryByCount1(paramMap));
				     BasePage page = (BasePage)paramMap.get("page");
				     page.getPager().setRowCount(rowCount.intValue());
				     paramMap.put("page", page);
				     return getDao().queryfanxiList(paramMap);
				   }
		 //根据id查询
		public CPcfResult queryById(String pkId) {
					 
					return dao.queryById(pkId);
			}
		
		public void updat(CPcfResult cPcfResult) {
			 getDao().updat(cPcfResult);
			}
		public int queryByCount1(Map<String, Object> paramMap) throws Exception
		   {
		     return getDao().queryByCount1(paramMap);
		   }
		public void deleteAll() {
			dao.deleteAll();
			
		}
}

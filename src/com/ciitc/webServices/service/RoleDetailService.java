package  com.ciitc.webServices.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.RoleDetailInfoDao;
import com.ciitc.webServices.dao.SysPermissionInfoDao;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.SysPermissionInfo;

@Service("roleDetailService")
public class RoleDetailService<T> extends BaseService<T> {

	@Autowired
	private RoleDetailInfoDao<T> dao;

	public RoleDetailInfoDao<T> getDao() {
		return dao;
	}
	

	@Autowired
	private SysPermissionInfoDao<T> sysdao;

	public SysPermissionInfoDao<T> getSysdao() {
		return sysdao;
	}
	
	public List<T> queryByRoleId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roleId", id);
		return getDao().queryByRoleId(paramMap);
	}
	
	
	public List<T> queryRoleUserByRoleId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("roleId", id);
		return getSysdao().queryRoleUserByRoleId(paramMap);
	}
	
	public List<T> queryByMenuId(Object id) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("menuId", id);
		return getDao().queryByMenuId(paramMap);
	}
	
	public List<T> queryByRoleIdAndMenuId(String roleId,String menuId) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("menuId", menuId);
		paramMap.put("roleId", roleId);
		return getDao().queryByRoleIdAndMenuId(paramMap);
	}
	/**
	 * 根据roleId 删除角色关联数据
	 * @param roleId
	 */
	public void deleteByRoleId(String roleId) {
		List<RoleDetailInfo> menuOfRoleList = null;
		List<SysPermissionInfo> roleUsers = null;
		try {
			// 角色菜单关联
			menuOfRoleList = (List<RoleDetailInfo>) this.queryByRoleId(roleId);
			if (menuOfRoleList != null && menuOfRoleList.size()>0) {
				for (RoleDetailInfo roleMenu : menuOfRoleList) {
					dao.delete(roleMenu.getPkId());	
				}
			}
			// 角色用户关联
			roleUsers = (List<SysPermissionInfo>) queryRoleUserByRoleId(roleId);
		    for (SysPermissionInfo roleUser : roleUsers) {
		    	sysdao.delete(roleUser.getPkId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public List<T> getDescription(String userCode,String menuName) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("USER_CODE", userCode);
		paramMap.put("MENU_PATH", menuName);
		return getDao().getDescription(paramMap);
	}
	
	public List<RoleDetailInfo> getMenuIds(String roIds) throws Exception{
		return getDao().getMenuIds(roIds);
	}
	public List<RoleDetailInfo> getDescriptionByMenu(String userName,String menuPkId) throws Exception{
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("USER_NAME", userName);
		paramMap.put("MENU_PK_ID", menuPkId);
		return getDao().getDescriptionByMenu(paramMap);
	}
	
	
}

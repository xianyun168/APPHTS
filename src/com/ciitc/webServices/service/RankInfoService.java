package  com.ciitc.webServices.service;

import java.util.List;

import org.apache.poi.ss.formula.functions.Rank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.RankInfoDao;

@Service("rankInfoService")
public class RankInfoService<T> extends BaseService<T> {

	@Autowired
	private RankInfoDao<T> dao;

	public RankInfoDao<T> getDao() {
		return dao;
	}
	public String queryById(String pkId) {
	return dao.queryById(pkId);
}
}

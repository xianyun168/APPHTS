package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ciitc.webServices.dao.AppGoodsInforDao;
import com.ciitc.webServices.model.AppGoodsInfor;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.service.BaseService;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年3月8日 下午3:41:30
 * 修 改 人: 
 ******************************************************************************/
@Service("appGoodsInforService")
public class AppGoodsInforService<T> extends BaseService<T>{

	@Autowired
	private AppGoodsInforDao<T> dao;
	
	public AppGoodsInforDao<T> getDao() {
		return dao;
	}

	public List<T> selectList(Map<String,Object> map){
		return getDao().selectList(map);
	}
	
	public Integer selectgoodsCount(Map<String,Object> map){
		return getDao().selectgoodsCount(map);
	}

	public AppGoodsInfor queryByGoodsId(String productId) {
		// TODO Auto-generated method stub
		return getDao().queryByGoodsId(productId);
	}
	
	public void updateBygoodsId(AppGoodsInfor appGoods) {
		 getDao().updateBygoodsId(appGoods);
	}
}

package com.ciitc.webServices.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.service.BaseService;
import com.ciitc.webServices.dao.PublishNewsDao;

@Service(" publishNewsService")
public class PublishNewsService<T> extends BaseService<T> {

	@Autowired
	private PublishNewsDao<T> dao;

	public PublishNewsDao<T> getDao() {
		return dao;
	}
	 public List<T> queryByList1(Map<String, Object> paramMap)
		     throws Exception
		   {
		     Integer rowCount = Integer.valueOf(queryByCount1(paramMap));
		     BasePage page = (BasePage)paramMap.get("page");
		     page.getPager().setRowCount(rowCount.intValue());
		     paramMap.put("page", page);
		     return getDao().queryByList1(paramMap);
		   }
	 public List<T> queryByList2(Map<String, Object> paramMap)
		     throws Exception
		   {
		     Integer rowCount = Integer.valueOf(queryByCount2(paramMap));
		     BasePage page = (BasePage)paramMap.get("page");
		     page.getPager().setRowCount(rowCount.intValue());
		     paramMap.put("page", page);
		     return getDao().queryByList2(paramMap);
		   }
	 public int queryByCount1(Map<String, Object> paramMap) throws Exception
	   {
	     return getDao().queryByCount1(paramMap);
	   }
	 public int queryByCount2(Map<String, Object> paramMap) throws Exception
	   {
	     return getDao().queryByCount2(paramMap);
	   }
	 public int queryByCount3(Map<String, Object> paramMap) throws Exception
	   {
	     return getDao().queryByCount3(paramMap);
	   }
	 public int queryByCounts(Map<String, Object> paramMap) throws Exception
	   {
	     return getDao().queryByCounts(paramMap);
	   }
	 public List<T> queryByNews(){
			return dao.queryByNews();
		}
	 public List<T> queryByList3(Map<String, Object> paramMap)
		     throws Exception
		   {
		     Integer rowCount = Integer.valueOf(queryByCount3(paramMap));
		     BasePage page = (BasePage)paramMap.get("page");
		     page.getPager().setRowCount(rowCount.intValue());
		     paramMap.put("page", page);
		     return getDao().queryByList3(paramMap);
		   }
	 public List<T> queryByLists(Map<String, Object> paramMap)
		     throws Exception
		   {
		     Integer rowCount = Integer.valueOf(queryByCounts(paramMap));
		     BasePage page = (BasePage)paramMap.get("page");
		     page.getPager().setRowCount(rowCount.intValue());
		     paramMap.put("page", page);
		     return getDao().queryByLists(paramMap);
		   }
}
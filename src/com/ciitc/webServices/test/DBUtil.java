package com.ciitc.webServices.test;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 数据库连接工具类
 */
public class DBUtil {
    /**
     *  连接工厂
     */
    public static Connection makeConnection(){
        Connection conn = null;
        try {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace(); 
            }
            conn = DriverManager.getConnection("jdbc:oracle:thin:@10.0.100.235:1521:ylcfcrmdb", "ylcrm", "ylcrm");
        } catch (SQLException e) {
            System.out.println("通过JDBC获取Oralce数据库链接对象出错!");
            e.printStackTrace();
        }
        return conn;
    }
    /**
    * @Description: 测试环境JDBC链接
    * @Author: zhengyunfei
    * @Date: 2018/11/15
    * @time:9:41
    */
    public static Connection makeTestConnection(){
        Connection conn = null;
        try {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            conn = DriverManager.getConnection("jdbc:oracle:thin:@10.0.100.230:1521:orcl", "testkfcrm", "testkfcrm");
        } catch (SQLException e) {
            System.out.println("通过JDBC获取Oralce数据库链接对象出错!");
            e.printStackTrace();
        }
        return conn;
    }
    /**
     *  连接测试
     */
    public static void  main(String args[]){
        if(DBUtil.makeConnection()==null){
            System.out.println("获取数据库链接失败！");
        }else{
            System.out.println("正式环境JDBC获取Oracle数据库链接成功！");
        }
        if(DBUtil.makeTestConnection()==null){
            System.out.println("测试环境获取数据库链接失败！");
        }else{
            System.out.println("测试环境JDBC获取Oracle数据库链接成功！");
        }

    }
}

package com.ciitc.webServices.test;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.BatchAddDao;
import com.ciitc.webServices.dao.appIntegralDetailsDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.Integration;
import com.ciitc.webServices.model.appIntegralDetails;
import com.ciitc.webServices.util.DBUtil;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

public class TestUserInfo {
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao =  (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	@SuppressWarnings("rawtypes")
	private static BatchAddDao satchAddDao = (BatchAddDao) SpringContextUtil.getBean("batchAddDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static appIntegralDetailsDao<appIntegralDetails> IntegraDetailsDao = (appIntegralDetailsDao) SpringContextUtil.getBean("appIntegralDetailsDao");
	
	
	//定时客户列表查询
		public void addIntegrationaa() {
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			// 备份，异常恢复使用
			List<AppUserInfor> queryALL = appUserInforDao.queryALL();
			AppUserInfor appUserInfor =  new AppUserInfor();
			ResultSet rs = null;
			try {
				appUserInforDao.deleteAll();
				call = conn.prepareCall(" SELECT * FROM TUNREGISTER");
				// 执行
	  			rs = call.executeQuery();
	  			System.out.println("初始等级计算开始："+new Date());
	  			while (rs.next()) {
	  				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));//客户编号
	  				appUserInfor.setPhoneNumber(rs.getString("C_MOBILENO"));//手机号
	  				appUserInfor.setIdcardType(rs.getString("C_IDENTITYTYPE")); //证件类型
	  				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO")); //证件号码
	  				appUserInfor.setCreateDate(rs.getDate("D_CREATEDATE")); ;//创建时间
	  				appUserInfor.setUserName(rs.getString("C_CUSTNAME")); //客户名称
	  				appUserInfor.setCustomerSex(rs.getString("C_SEX")); //性别
	  				//appUserInfor.setRegion(rs.getString("C_MARKETREGION")); //服务分支机构---
	  				//根据客户编号查询理财师
	  				String financialId =brokeraccount1(rs.getString("C_CUSTNO"));
	  				appUserInfor.setFinancialId(financialId);//理财师ID
	  				String nameStatus = getStateBycustNo(rs.getString("C_CUSTNO"));
	  				if(null != nameStatus && "已认证".equals(nameStatus)) {
	  					appUserInfor.setNameStatus("1");//已认证
	  				}else if(null != nameStatus && "待审核".equals(nameStatus)){
	  					appUserInfor.setNameStatus("2");//审核中
	  				}else if(null != nameStatus && "已驳回".equals(nameStatus)){
	  					appUserInfor.setNameStatus("3");
	  				}else {
	  					appUserInfor.setNameStatus("0");//未认证
	  				}
	  				
	  				String  birthday = rs.getString("C_BIRTHDAY");//生日
	  				if(null != birthday && !"".equals(birthday)) {
	  					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
	  					Date customerBirthday = null;
	  					try {
	  						customerBirthday = sdf.parse(birthday);
	  					} catch (ParseException e) {
	  						// TODO Auto-generated catch block
	  						e.printStackTrace();
	  					}
	  	  				
	  	  				appUserInfor.setCustomerBirthday(customerBirthday);
	  				}
	  				appUserInfor.setIntegral(0);//客户积分
	  				appUserInfor.setCustomerLevel("1");//客户等级
	  				appUserInfor.setPkId(UUID.randomUUID().toString());
	  				appUserInfor.setCustomerAddress(rs.getString("C_ADDRESS")); //地址
	  				appUserInforDao.insertSelective(appUserInfor);
	  				System.out.println(rs.getString("C_CUSTNO")+"新增" );
	  				CustomerLevelBycustoNo(rs.getString("C_CUSTNO"));
	  			}
	  			System.out.println("初始等级计算结束："+new Date());
	  			//初始化查询所有合同，增加积分
	  			//1、先查询2018-01-01之前的合同。等级全部为1
	  			List<Integration> integrationlist = getContract();
				if(integrationlist.size()>0) {
					tradingIntegral(integrationlist);
				}
	  			//2、再查询2018-01-01到现在的合同。等级按照客户表里的等级
				List<Integration> integrationlist1 = getContract1();
				if(integrationlist1.size()>0) {
					tradingIntegral1(integrationlist1);
				}
				//积分过期
				integralOverdue();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				// 清空数据表(删除已经插入的部分数据)
				appUserInforDao.deleteAll();
				// 恢复数据
				for (int i = 0; i < queryALL.size(); i++) {
					try {
						appUserInforDao.insertInfor(queryALL.get(i));
					} catch (Exception e1) {
						e1.printStackTrace();
					}
				}
			}finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(call != null) {
					try {
						call.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		
		}
		//根据客户编号计算客户等级
		public void CustomerLevelBycustoNo(String custoNo){
			 Connection conn = DBUtil.makeConnection();
			 CallableStatement call = null;
			String sql = " select cvo.c_custno,nvl(cuc.f_contractsignbalance, 0) f_contractsignbalance from  " + 
					" ((select distinct c_custno from cust_view_position) cvo \r\n" + 
					" left join (select cvp.c_custno, cvp.f_contractsignbalance from cust_view_position cvp  " + 
					" where cvp.d_caldate = (select max(cv.d_caldate) from cust_view_position cv " + 
					" where cv.d_caldate <= to_date('20171231', 'yyyymmdd') and cv.c_custno = cvp.c_custno) and " + 
					" (select max(cp.d_caldate) from cust_view_position cp " + 
					" where cp.c_custno = cvp.c_custno) >= to_date('20171231', 'yyyymmdd')) cuc  on cvo.c_custno = cuc.c_custno) where cvo.c_custno = '"+custoNo+"'";
			try {
				// 组织架构表
				call = conn.prepareCall(sql);
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					AppUserInfor appUserInfor = new AppUserInfor();
					
					String val = rs.getString("F_CONTRACTSIGNBALANCE");
					if(0 <= Integer.valueOf(val) && Integer.valueOf(val)< 1000000){
						appUserInfor.setCustomerLevel("1");
					}else if(1000000 <= Integer.valueOf(val) && Integer.valueOf(val) <3000000){
						appUserInfor.setCustomerLevel("2");
					}else if(3000000 <= Integer.valueOf(val) && Integer.valueOf(val) <10000000){
						appUserInfor.setCustomerLevel("3");
					}else if(10000000 <= Integer.valueOf(val) && Integer.valueOf(val) <50000000){
						appUserInfor.setCustomerLevel("4");
					}else if(50000000 <= Integer.valueOf(val)){
						appUserInfor.setCustomerLevel("5");
					}
					System.out.println(rs.getString("C_CUSTNO")+"==等级=="+appUserInfor.getCustomerLevel());
					appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));
					satchAddDao.updateBycustomer(appUserInfor);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		public void tradingIntegral1(List<Integration> list) {
			if(list.size()>0){
				for(Integration inte:list){
					AppUserInfor appUser = (AppUserInfor) satchAddDao.queryBycustomer(inte.getCustno());
					if(appUser !=null){
						@SuppressWarnings("unused")
						double integration =0.0;
						if(appUser.getCustomerLevel().equals("1") || appUser.getCustomerLevel().equals("2")){
							//持仓天数数
							double position = inte.getPositionDate();
							double positions = position/365;
							BigDecimal b = new BigDecimal(positions);
							//保留两位小数
							double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
							BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1);
							integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
						}else if(appUser.getCustomerLevel().equals("3")){
							//持仓天数数
							double position = inte.getPositionDate();
							double positions = position/365;
							BigDecimal b = new BigDecimal(positions);
							//保留两位小数
							double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
							BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.1);
							integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
						}else if(appUser.getCustomerLevel().equals("4")){
							//持仓天数数
							double position = inte.getPositionDate();
							double positions = position/365;
							BigDecimal b = new BigDecimal(positions);
							//保留两位小数
							double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
							BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.2);
							integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
						}else if(appUser.getCustomerLevel().equals("5")){
							//持仓天数数
							double position = inte.getPositionDate();
							double positions = position/365;
							BigDecimal b = new BigDecimal(positions);
							//保留两位小数
							double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
							BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.3);
							integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
						}
						Map<String,Object> map = new HashMap<String, Object>();
						map.put("userId", appUser.getPkId());
						//查询积分明细
						//List<appIntegralDetails> details = IntegraDetailsDao.selectByuserId(map);
						//判断是否存在这个用户
						//if(details.size()==0){
							appIntegralDetails appIntegral = new appIntegralDetails();
							String IntegralId = UUID.randomUUID().toString();
							appIntegral.setPkId(IntegralId);
							appIntegral.setUserId(appUser.getPkId());
							appIntegral.setRecordDate(inte.getContractsignDate());
							appIntegral.setIntegralNum(integration+"");
							appIntegral.setValidity(365);
							appIntegral.setIntegralType("00");
							appIntegral.setName(inte.getProtuctName());
							appIntegral.setIntegralState("00");
							appIntegral.setContractNum(inte.getReserveno());
							appIntegral.setConsumptionIntegral(0);
							appIntegral.setExpireIntegral(0);
							appIntegral.setValidIntegral(integration);
							String time = getYears(inte.getContractsignDate());
							SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
							try {
								Date da = sf.parse(time);
								appIntegral.setExpireDate(da);
								IntegraDetailsDao.insertSelective(appIntegral);
								AppUserInfor newAppUser = new AppUserInfor();
								newAppUser.setPkId(appUser.getPkId());
								//根据客户编号查询客户有效积分
								Map<String,Object> map1 = new HashMap<String, Object>();
								map1.put("userId", appUser.getPkId());
								//统计此用户有效积分总和
								long sum = IntegraDetailsDao.selectUserIdSum(map1);
								newAppUser.setIntegral(sum);
								satchAddDao.updatebyuserId(newAppUser);
								System.out.println(inte.getCustno()+"--积分--"+newAppUser.getIntegral());
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						//}
					}
					
				}
				System.out.println("*************************结束*****************************");
			}
		}
		public void tradingIntegral(List<Integration> list) {
			if(list.size()>0){
				for(Integration inte:list){
					AppUserInfor appUser = (AppUserInfor) satchAddDao.queryBycustomer(inte.getCustno());
					if(appUser !=null){
						@SuppressWarnings("unused")
						double integration =0.0;
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
						Map<String,Object> map = new HashMap<String, Object>();
						map.put("userId", appUser.getPkId());
						appIntegralDetails appIntegral = new appIntegralDetails();
						String IntegralId = UUID.randomUUID().toString();
						appIntegral.setPkId(IntegralId);
						appIntegral.setUserId(appUser.getPkId());
						appIntegral.setRecordDate(inte.getContractsignDate());
						appIntegral.setIntegralNum(integration+"");
						appIntegral.setValidity(365);
						appIntegral.setIntegralType("00");
						appIntegral.setName(inte.getProtuctName());
						appIntegral.setIntegralState("00");
						appIntegral.setContractNum(inte.getReserveno());
						appIntegral.setConsumptionIntegral(0);
						appIntegral.setExpireIntegral(0);
						appIntegral.setValidIntegral(integration);
						String time = getYears(inte.getContractsignDate());
						SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
						try {
							Date da = sf.parse(time);
							appIntegral.setExpireDate(da);
							IntegraDetailsDao.insertSelective(appIntegral);
							AppUserInfor newAppUser = new AppUserInfor();
							newAppUser.setPkId(appUser.getPkId());
							//根据客户编号查询客户有效积分
							Map<String,Object> map1 = new HashMap<String, Object>();
							map1.put("userId", appUser.getPkId());
							//统计此用户有效积分总和
							long sum = IntegraDetailsDao.selectUserIdSum(map1);
							newAppUser.setIntegral(sum);
							satchAddDao.updatebyuserId(newAppUser);
							System.out.println(inte.getCustno()+"--积分--"+newAppUser.getIntegral());
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
							
					}
					
				}
				System.out.println("*************************结束*****************************");
			}
		}
		//查询理财师
		public String brokeraccount1(String custNo) {
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			String financialId = null;
			ResultSet rs = null;
			try {
				call = conn.prepareCall("select C_BROKERACCOUNT from tfundbrokerrelation where C_CUSTNO= '" + custNo+"'");
				// 执行
				rs = call.executeQuery();
				while (rs.next()) {
					financialId = rs.getString("C_BROKERACCOUNT");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				if(rs != null) {
					try {
						rs.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(call != null) {
					try {
						call.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				if(conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			return financialId;
		}
		
		//查询实名认证状态
		public String getStateBycustNo(String customerId) {
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeKfCrm();
			CallableStatement call = null;
			ResultSet rs = null;
			String state = "";
			try {
				call=conn.prepareCall(" select FN_TCUSTREGISTERREQ('"+customerId+"') states from dual");
				rs = call.executeQuery();
	  			while (rs.next()) {
	  				state = rs.getString("states");
	  				
	  			}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				try {
					if( null != rs) {
					rs.close();
					}
					if(null != call) {
						call.close();
					}
					if(null != conn){
						conn.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
		}
			return state;
		}
		
		//年份
		public  String getYears(Date d)
		{
			//Date d=new Date();
			GregorianCalendar gc =new GregorianCalendar();
			SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
			gc.setTime(d);
			gc.add(1,+1);
			gc.set(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH),gc.get(Calendar.DATE));

		return sf.format(gc.getTime());
		}
		//查询合同计算积分(2018-01-01之前的合同)
		public List<Integration> getContract() {
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			List<Integration> integrationList = new ArrayList<Integration>();
			try {
				call = conn.prepareCall("select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE  from ttrustcontract_req q ,tfundinfo o where q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS = '2' and D_FUNDOPENDATE <= to_date('20171231','yyyymmdd')");
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					Integration integration = new Integration();
					integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
					integration.setCustno(rs.getString("C_CUSTNO"));
					integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
					integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
					integration.setProtuctName(rs.getString("C_FUNDNAME"));
					if(null !=rs.getString("D_FUNDOPENDATE") ) {
						Date aa = rs.getDate("D_CONTRACTENDDATE");
						long a =  aa.getTime();
						Date bb = rs.getDate("D_FUNDOPENDATE");
						long b =  bb.getTime();
						long day = ((a-b)/(24*60*60*1000));
						integration.setPositionDate((int)day);
					}
					integrationList.add(integration);
				}
				if(rs !=null){
					rs.close();
				}
				if(call !=null){
					call.close();
				}
				if(conn !=null){
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return integrationList;
		}
		//查询合同计算积分(2018-01-01之后的合同)
		public List<Integration> getContract1() {
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			List<Integration> integrationList = new ArrayList<Integration>();
			try {
				call = conn.prepareCall("select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE  from ttrustcontract_req q ,tfundinfo o where q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS = '2'and   to_date('20180101','yyyymmdd') <= D_FUNDOPENDATE");
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					Integration integration = new Integration();
					integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
					integration.setCustno(rs.getString("C_CUSTNO"));
					integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
					integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
					integration.setProtuctName(rs.getString("C_FUNDNAME"));
					if(null !=rs.getString("D_FUNDOPENDATE") ) {
						Date aa = rs.getDate("D_CONTRACTENDDATE");
						long a =  aa.getTime();
						Date bb = rs.getDate("D_FUNDOPENDATE");
						long b =  bb.getTime();
						long day = ((a-b)/(24*60*60*1000));
						integration.setPositionDate((int)day);
					}
					integrationList.add(integration);
				}
				if(rs !=null){
					rs.close();
				}
				if(call !=null){
					call.close();
				}
				if(conn !=null){
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return integrationList;
		}
		//积分过期定时
		public static void integralOverdue(){
			System.out.println("定时开始："+new Date());
			//根据有效期查询所有用户积分明细
			List<appIntegralDetails> details = IntegraDetailsDao.selectOverdue();
			Map<String,Object> map = new HashMap<String, Object>();
			if(details.size()>0){
				for(int i=0;i<details.size();i++){
					SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					//当前日期
					String current = df.format(new Date());
					//有效日期
					String valid = df.format(details.get(i).getExpireDate());
					//调运日期比较方法
					int a= compare_date(current, valid);
					//判断返回-1或0 积分到期
					if(a == 1){
						if(details.get(i).getValidIntegral()>0){
							//修改到期积分有效值
							appIntegralDetails newDetails = new appIntegralDetails();
							newDetails.setExpireIntegral(details.get(i).getValidIntegral());
							newDetails.setValidIntegral(0);
							newDetails.setPkId(details.get(i).getPkId());
							IntegraDetailsDao.updateByPrimaryKeySelective(newDetails);
							map.put("userId", details.get(i).getUserId());
							//统计此用户有效积分总和
							long sum = IntegraDetailsDao.selectUserIdSum(map);
							//修改用户积分值
							AppUserInfor appUserInfor = new AppUserInfor();
							appUserInfor.setPkId(details.get(i).getUserId());
							appUserInfor.setIntegral(sum);
							satchAddDao.updatebyuserId(appUserInfor);
							
						}
					}
				}
			}
			System.out.println("定时结束："+new Date());
		}
		//比较两日期大小
		 public static int compare_date(String DATE1, String DATE2) {
		       
		       
		        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
		        try {
		            Date dt1 = df.parse(DATE1);
		            Date dt2 = df.parse(DATE2);
		            if (dt1.getTime() > dt2.getTime()) {
		                System.out.println("dt1 在dt2前");
		                return 1;
		            } else if (dt1.getTime() < dt2.getTime()) {
		                System.out.println("dt1在dt2后");
		                return -1;
		            } else {
		                return 0;
		            }
		        } catch (Exception exception) {
		            exception.printStackTrace();
		        }
		        return 0;
		    }
}

package com.ciitc.webServices.datasource;
/**
 *类名：DataSourceContextHolder.java
 **@author 张瑞
 *@date 2015-02-11
 *功能：获得和设置上下文环境的类，主要负责改变上下文数据源的名称
 */
public class DataSourceContextHolder {

	
	//定义本地线程变量
	private static final ThreadLocal contextHolder=new ThreadLocal();
	
	//动态设置数据源
	public static void setDataSourceType(DataSourceType dataSourceType){
		contextHolder.set(dataSourceType);
	}
	
	public static DataSourceType getDataSourceType(){
		return (DataSourceType) contextHolder.get();
	}
	
	//清空数据源
	public static void clearDataSourceType(){
		contextHolder.remove();
	}
	
}

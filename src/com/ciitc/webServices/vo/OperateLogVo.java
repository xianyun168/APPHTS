package com.ciitc.webServices.vo;

import java.util.Date;

public class OperateLogVo {
	/** 操作日志pkId */
	private String pkId;
	
	/** 用户pkId */
	private String userPkId;
	
	/** 用户编码 */
	private String userCode;
	
	/** 操作模块 */
	private String operateMode;
	
	/** 操作类型 */
	private String operateType;
	
	/** 操作时间 */
	private Date operateTime;
	
	/** 操作轨迹 */
	private String operateTrack;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getUserPkId() {
		return userPkId;
	}

	public void setUserPkId(String userPkId) {
		this.userPkId = userPkId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getOperateMode() {
		return operateMode;
	}

	public void setOperateMode(String operateMode) {
		this.operateMode = operateMode;
	}

	public String getOperateType() {
		return operateType;
	}

	public void setOperateType(String operateType) {
		this.operateType = operateType;
	}

	public Date getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}

	public String getOperateTrack() {
		return operateTrack;
	}

	public void setOperateTrack(String operateTrack) {
		this.operateTrack = operateTrack;
	}
	
	
}

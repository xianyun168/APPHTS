package com.ciitc.webServices.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import com.ciitc.webServices.dao.PublishNewsDao;
import com.ciitc.webServices.model.PublishNewsInfo;

/**
 * 定时器
 * 
 * @author lty
 * 
 */
@Component
public class NewsTimer implements Job {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private PublishNewsDao<PublishNewsInfo> publishNewsDao = (PublishNewsDao) SpringContextUtil
			.getBean("publishNewsDao");

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		List<PublishNewsInfo> list = new ArrayList<PublishNewsInfo>();
		try {
			list = publishNewsDao.queryQuartzList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!"0".equals(list)){
		for (PublishNewsInfo pns : list) {
				if (pns.getNewsPublishConfig() != null && !"".equals(pns.getNewsPublishConfig())) {
				    Date d = new Date();
					long newDate = d.getTime();//System.currentTimeMillis();
					long publishDate = pns.getNewsPublishConfig().getTime();
					if (newDate >= publishDate) {
						pns.setPublishState("已发布");
						pns.setAuditState("通过");
						publishNewsDao.update(pns);
						Log.getLogger().info("------------非轮播新闻定时发布成功------------");
					}
					
				}
				else{
					Log.getLogger().info("------------非轮播新闻定时发布失败，请检查定时时间------------");
				}
		}
		
	}
		
	}
}

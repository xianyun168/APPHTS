package com.ciitc.webServices.util;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.ciitc.webServices.dao.AppPerformanceInterfaceDao;
import com.ciitc.webServices.dao.AppPerformamcePrivInfoDao;
import com.ciitc.webServices.model.AppPerformamcePrivInfo;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
//业绩统计
public class CRM_AchievementUntil{

private AppPerformanceInterfaceDao dao = (AppPerformanceInterfaceDao) SpringContextUtil.getBean("appPerformanceInterfaceDao");
private AppPerformamcePrivInfoDao privinfodao = (AppPerformamcePrivInfoDao) SpringContextUtil.getBean("appPerformamcePrivInfoDao");
	String state="00";
	String msg="成功";
	
	@SuppressWarnings("unchecked")
	public Map<String,Object> reservat(String userid){
		  DecimalFormat df = new DecimalFormat("0.00%");
		  NumberFormat nf = NumberFormat.getInstance();//设置Double类型的格式规范，不转换科学计数法
		  nf.setGroupingUsed(false);
		  //返回对象
		  Map<String,Object> map = new HashMap<String,Object>();
		try{	
			  String username =usernameId(userid); 
			  if(username==null){
				  state="01";
				  msg="用户不存在";
				  map.put("state", state);
				  map.put("msg", msg);
				  return map;	  
			  } 
			//查询用户权限等级
			 List<AppPerformamcePrivInfo> lvltypList = privinfodao.queryBy(username);			
			 if(lvltypList==null || lvltypList.size()==0){
				 Map<String,Object> m = new HashMap<String,Object>();
				 List<Map<String,Object>> lis = new ArrayList<Map<String,Object>>();
				 //查询该用户的业绩数据
				 Map<String,Object> querymap = dao.queryByList(username);
				 if(querymap==null){
					 state="01";
					  msg="该用户无业绩数据";
					  map.put("state", state);
					  map.put("msg", msg);
					  return map;	  
				 }
				Double indaims =Double.parseDouble(querymap.get("indaims").toString());
				Double indcontract = Double.parseDouble(querymap.get("indcontract").toString());	
				if(indaims==null || indaims<=0){
					  state="01";
					  msg="该用户无目标业绩数据";
					  map.put("state", state);
					  map.put("msg", msg);
					  return map;
				}
				map.put("state", state);
				map.put("msg", msg);
				m.put("indtage",df.format(indcontract/indaims));
				m.put("indaims", DoubleDiv(nf.format(indaims).toString()));
				m.put("indcontract", DoubleDiv(nf.format(indcontract).toString()));
				m.put("lvltyp",0);	
				lis.add(m);
				map.put("0",lis);
				return map;
			 }else{
				 List<Map<String,Object>> list=null;
				 Map<String,Object> querymap = dao.queryByList(username);//查询个人业绩数据
				 Map<String,Object> mapp = new HashMap<String,Object>();
				 if(querymap != null){
					Double indaims =Double.parseDouble(querymap.get("indaims").toString());
					Double indcontract = Double.parseDouble(querymap.get("indcontract").toString());	
					if(indaims>0){
						list=new ArrayList<Map<String,Object>>();
						mapp.put("indtage",df.format(indcontract/indaims));
						mapp.put("indaims", DoubleDiv(nf.format(indaims)).toString());
						mapp.put("indcontract", DoubleDiv(nf.format(indcontract).toString()));
						mapp.put("lvltyp",0);  
						list.add(mapp);
						map.put("0", list);
				}
			 }	 
			 //遍历权限个数，分别查询对应的部门业绩金额
				Map<String,Object> privinfomap;
			 for(AppPerformamcePrivInfo privinfo:lvltypList){
				 privinfomap = new HashMap<String,Object>();
				 list= new ArrayList<Map<String,Object>>();
				 //根据权限号码和团队编号查询对应的业绩
				Map<String,Object> privinmap = privinfodao.queryCarCount(privinfo);
				if(privinmap!=null){
						Double indaims=0.00;
						if(privinmap.get("TARGET")!=null){
						indaims = Double.parseDouble(privinmap.get("TARGET").toString());
						}
						Double indcontract=0.00;
						if(privinmap.get("REAL_AMT")!=null){
							indcontract =Double.parseDouble(privinmap.get("REAL_AMT").toString());
						} 	
				String indtage="";
				if(indaims !=null && indaims>0){
				 indtage = df.format(indcontract/indaims);
				}
				String lvltyp = privinfo.getLvltyp();
				privinfomap.put("lvltyp", lvltyp);
				privinfomap.put("departmentname", privinfo.getDepartmentname());	
				privinfomap.put("indtage",indtage);
				privinfomap.put("indaims", DoubleDiv(nf.format(indaims).toString()));
				privinfomap.put("indcontract", DoubleDiv(nf.format(indcontract).toString()));
				if(map.containsKey(lvltyp)){
					List<Map<String,Object>> li =  (List<Map<String,Object>>) map.get(lvltyp);
					li.add(privinfomap);	
				}else{
					list.add(privinfomap);
					map.put(lvltyp,list);
					
				}
				
				}
		}
			 if(map.size()==0){
				  state="01";
				  msg="该用户无业绩数据";
				  map.put("state", state);
				  map.put("msg", msg);
				  return map;
			 }
			 map.put("state", state);
			 map.put("msg", msg);
		}	 
		
		}catch(Exception e){
			  state ="01";
			  msg="获取数据错误";
			  map.put("state", state);
			  map.put("msg", msg);
			e.printStackTrace();
		}
		
		return map;
		
	}
	
	/**
	 * 置换id置换用户名
	 * @param id
	 * @return
	 */
	public String usernameId(String userid){
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String str =null;
		try {
			 call = conn.prepareCall(" select C_USERCODE from tfundbroker where c_brokeraccount='"+userid+"' ");
			 rs = call.executeQuery();      
			while (rs.next()){
			str =rs.getString("C_USERCODE");	
			}
		} catch (SQLException e) {
				e.printStackTrace();
		}finally {
		
			if(rs!=null){
				try {
					rs.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(call!=null){
				try {
					call.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
		}
		return str;
	}
	/**
	 * 进行除法转换
	 * @param str
	 * @return
	 */
	public  String DoubleDiv(String str){
		BigDecimal a = new BigDecimal(str);
		BigDecimal b= new BigDecimal("10000");
		BigDecimal s = a.divide(b);
		return s.toString();
	}
}	
	
	



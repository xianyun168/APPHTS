package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductLimtUtil {

	// 计算产品额度
	public static int productEndDate(String productNo) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		int money = 0;
		try {
			// 某产品预约金额限制
			call = conn.prepareCall("select fn_amtleft('" + productNo + "') money from dual");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				// 产品剩余额度
				money = rs.getInt("money");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (null != rs) {
					rs.close();
				}
				if (null != call) {
					call.close();
				}
				if (null != conn) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		}
		return money;

	}
	public String  getRelation(String aa) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String status = null;
		try {
			
			// 用户和理财师关联表
			call = conn.prepareCall(" select tr.C_RESERVENO ,td.C_CAPTION     " + "      from treserve tr "
					+ "      left join tdictionary td " + "        on tr.c_status=td.c_keyvalue "
					+ "       and td.l_keyno = 3059   " + "     where tr.c_reserveno = '" + aa + "' " + "");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				// 产品剩余额度
				status = rs.getString("C_CAPTION");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return status;
	}
}

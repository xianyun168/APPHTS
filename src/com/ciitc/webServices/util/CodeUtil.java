package com.ciitc.webServices.util;
/*******************************************************************************
 * 模块名称:  字典
 * 模块功能描述:  
 * 创 建 人: renqingqing
 * 日 期: 2015年9月30日 上午9:23:56
 * 修 改 人: 
 ******************************************************************************/
public class CodeUtil {
	/*---------------审核状态----------------*/
	//  02-待审核
	public static final String STATUS_AUDITREADY = "02";
	
	//  03-正在审核
	public static final String STATUS_AUDITING = "03";
	
	//  04-审核通过
	public static final String STATUS_PASSAUDIT = "04";
	
	//  05-审核不通过
	public static final String STATUS_AUDITREFUSED = "05";
	
	//  06-放弃审核
	public static final String STATUS_DROPAUDIT = "06";
	
	//  07-草稿
	public static final String STATUS_DRAFT = "07";
	
	/*---------------草稿类型----------------*/
	//  01-服务申请
	public static final String DRAFTTYPE_SERVICEAPPLY = "01";
	
	//  02-服务变更
	public static final String DRAFTTYPE_SERVICECHANGE = "02";
	
	//  03-服务注销
	public static final String DRAFTTYPE_SERVICECANCEL = "03";
	
	//	04-服务定制
	public static final String DRAFTTYPE_NEWSERVICE = "04";
	
	/*---------------服务类型----------------*/
	//  01-服务
	public static final String FLAG_SERVICE = "01";
	
	//  02-原子服务
	public static final String FLAG_ATOMICSERVICE = "02";
	
	/*---------------角色类型----------------*/
	//平台管理方
	public static final String ROLE_ADMIN = "1";
	
	//服务管理方
	public static final String ROLE_SERVICE_ADMIN = "0";
	
	/*---------------服务生效类型----------------*/
	//未生效
	public static final String STATUS_WSX = "00";
	//生效
	public static final String STATUS_SX = "01";
}

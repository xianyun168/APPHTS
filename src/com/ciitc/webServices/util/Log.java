package com.ciitc.webServices.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 日志记录类 与log4j.properties的配置相对应
 * 
 * @author 崔骥才
 * @日期 2014年8月13日-下午5:55:11
 */
public class Log {
	private static Logger logger = LoggerFactory.getLogger("");

	private Log() {
	}

	/**
	 * 返回Logger对象，记录日志
	 * 
	 * @author 崔骥才
	 * @日期 2014年8月13日-下午5:55:43
	 * @return
	 */
	public static synchronized Logger getLogger() {
		if (null == logger) {
			logger = LoggerFactory.getLogger("");
		}
		return logger;
	}
}

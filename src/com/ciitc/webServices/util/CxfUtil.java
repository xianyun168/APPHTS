package com.ciitc.webServices.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;


public class CxfUtil {

	private static Map<String, Object> serviceMap = new HashMap<String, Object>();
	@SuppressWarnings("unchecked")
	public static <T> T getService(Class<? extends T> type, String serviceUrl) {
		Object client = serviceMap.get(serviceUrl);
		if (client == null) {
			synchronized (serviceMap) {
				JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
				factory.setServiceClass(type);
				factory.setAddress(serviceUrl);
				factory.getInInterceptors().add(new LoggingInInterceptor());  
		        factory.getOutInterceptors().add(new LoggingOutInterceptor());
				client = factory.create();
				serviceMap.put(serviceUrl, client);
			}
		}
		if (client == null) {
			throw new IllegalArgumentException(String.format(
					"The url:%s is not a service url", serviceUrl));
		}
		if (!type.isAssignableFrom(client.getClass())) {
			throw new IllegalArgumentException(String.format(
					"The service's type should be %s", client.getClass()));
		}
		return (T) client;
	}
}

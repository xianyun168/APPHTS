package com.ciitc.webServices.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import com.ciitc.webServices.dao.PublishNewsDao;
import com.ciitc.webServices.model.PublishNewsInfo;

/**
 * 定时器
 * 
 * @author lty
 * 
 */
@Component
public class NewsTimerlb implements Job {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private PublishNewsDao<PublishNewsInfo> publishNewsDao = (PublishNewsDao) SpringContextUtil
			.getBean("publishNewsDao");

	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		List<PublishNewsInfo> listlb = new ArrayList<PublishNewsInfo>();
		try {
			listlb = publishNewsDao.queryQuartzListlb();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(!"0".equals(listlb)){
		for(PublishNewsInfo pnslb : listlb){	
			 if (pnslb.getNewsPublishConfiglb() != null && !"".equals(pnslb.getNewsPublishConfiglb())) {
				 Date d = new Date();
				long newDate = d.getTime();//System.currentTimeMillis();
				long publishDatelb = pnslb.getNewsPublishConfiglb().getTime();
				if (newDate >= publishDatelb) {
					pnslb.setPublishStatelb("已发布");
					pnslb.setAuditStatelb("通过");
					publishNewsDao.update(pnslb);
					Log.getLogger().info("------------轮播新闻定时发布成功------------");
				}
				
			 }
			 else{ Log.getLogger().info("------------轮播新闻定时发布失败，请检查定时时间------------");}
		}
	}		 
	}
}

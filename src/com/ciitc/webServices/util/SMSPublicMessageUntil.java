package com.ciitc.webServices.util;

import java.util.HashMap;
import java.util.Map;

import cn.emay.util.AES;
import cn.emay.util.EmayHttpClient;
import cn.emay.util.EmayHttpRequestBytes;
import cn.emay.util.EmayHttpResponseBytes;
import cn.emay.util.EmayHttpResponseBytesPraser;
import cn.emay.util.EmayHttpResultCode;
import cn.emay.util.GZIPUtils;
import cn.emay.util.JsonHelperUntil;
import cn.emay.util.ResultModel1;
import cn.emay.util.SmsBatchRequest1;
import cn.emay.util.SmsResponse;
import cn.emay.util.SmsSingleRequest;

public class SMSPublicMessageUntil {
	
	/**
	 * 发送单条短信
	 * @param isGzip 是否压缩
	 */
	public  String setSingleSms(SmsSingleRequest smsSingleRequest,boolean isGzip,String appId,String secretKey,String host,String algorithm) {
		System.out.println("=============begin setSingleSms==================");
		ResultModel1 result = request(smsSingleRequest, "http://" + host + "/inter/sendSingleSMS",isGzip,appId,secretKey,algorithm);
		System.out.println("result code :" + result.getCode());
		String state = null;
		if("SUCCESS".equals(result.getCode())){
			SmsResponse response = JsonHelperUntil.fromJson(SmsResponse.class, result.getResult());
			if (response != null) {
				System.out.println("data : " + response.getMobile() + "," + response.getSmsId() + "," + response.getCustomSmsId());
				state="成功";
				return state;
			}
		}
		System.out.println("=============end setSingleSms==================");
		return null;
		
	}
	
	/**
	 * 发送批次短信
	 * @param isGzip 是否压缩
	 */
	public  void setBatchOnlySms(SmsBatchRequest1 smsBatchRequest1,boolean isGzip,String appId,String secretKey,String host,String algorithm) {
		System.out.println("=============begin setBatchOnlySms==================");
		ResultModel1 result = request(smsBatchRequest1, "http://" + host + "/inter/sendBatchOnlySMS",isGzip,appId,secretKey,algorithm);
		System.out.println("result code :" + result.getCode());
		if("SUCCESS".equals(result.getCode())){
			SmsResponse[] response = JsonHelperUntil.fromJson(SmsResponse[].class, result.getResult());
			if (response != null) {
				for (SmsResponse d : response) {
					System.out.println("data:" + d.getMobile() + "," + d.getSmsId() + "," + d.getCustomSmsId());
				}
			}
		}
		System.out.println("=============end setBatchOnlySms==================");
	}
	
	/**
	 * 公共请求方法
	 */
	public  ResultModel1 request(Object content, String url,final boolean isGzip,String appId,String secretKey,String algorithm) {
		EmayHttpRequestBytes request = null;
		Map<String, String> headers = new HashMap<String, String>();
		try {
			headers.put("appId", appId);
			String requestJson = JsonHelperUntil.toJsonString(content);
			System.out.println("result json: " + requestJson);
			byte[] bytes = requestJson.getBytes("UTF-8");
			System.out.println("request data size : " + bytes.length);
			if (isGzip) {
				headers.put("gzip", "on");
				bytes = GZIPUtils.compress(bytes);
				System.out.println("request data size [com]: " + bytes.length);
			}
			byte[] parambytes = AES.encrypt(bytes, secretKey.getBytes("UTF-8"), algorithm);
			System.out.println("request data size [en] : " + parambytes.length);
			request = new EmayHttpRequestBytes(url, "UTF-8", "POST", headers, null, parambytes);
		} catch (Exception e) {
			System.out.println("加密异常");
			e.printStackTrace();
		}
		EmayHttpClient client = new EmayHttpClient();
		String code = null;
		String result = null;
		try {
			EmayHttpResponseBytes res = client.service(request, new EmayHttpResponseBytesPraser());
			if (res.getResultCode().equals(EmayHttpResultCode.SUCCESS)) {
				if (res.getHttpCode() == 200) {
					code = res.getHeaders().get("result");
					if (code.equals("SUCCESS")) {
						byte[] data = res.getResultBytes();
						System.out.println("response data size [en and com] : " + data.length);
						data = AES.decrypt(data, secretKey.getBytes("UTF-8"), algorithm);
						if (isGzip) {
							data = GZIPUtils.decompress(data);
						}
						System.out.println("response data size : " + data.length);
						result = new String(data, "UTF-8");
						System.out.println("response json: " + result);
					}
				} else {
					System.out.println("请求接口异常,请求码:" + res.getHttpCode());
				}
			} else {
				System.out.println("请求接口网络异常:" + res.getResultCode().getCode());
			}
		} catch (Exception e) {
			System.out.println("解析失败");
			e.printStackTrace();
		}
		ResultModel1 re = new ResultModel1(code, result);
		return re;
	}
	
}

package com.ciitc.webServices.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class HttpPostUtil {
	
	public String post(String url, String pdl){
		OutputStreamWriter out = null;
		String sCurrentLine = "";
		try {
			URL urlTemp = new URL(url);
			URLConnection connection = urlTemp.openConnection();
			connection.setDoOutput(true);
			out = new OutputStreamWriter(connection.getOutputStream(), "UTF-8");
			out.write(pdl);
			out.flush();
			InputStream l_urlStream;
			l_urlStream = connection.getInputStream();// 请求
			BufferedReader l_reader = new BufferedReader(new InputStreamReader(l_urlStream));
			while ((sCurrentLine = l_reader.readLine()) != null) {
				sCurrentLine += sCurrentLine;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
  		return sCurrentLine;
	}

}

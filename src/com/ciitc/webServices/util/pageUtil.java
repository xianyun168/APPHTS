package com.ciitc.webServices.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Controller;

import cn.com.ins.sinosoft.framework.base.util.JsonTurn;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

/***
 * 公共方法，向页面返回easyui的Grid的数据
 * 
 * @author zhang.rui
 */
@Controller
public class pageUtil extends BaseController {
	public static void responseDatagrid(HttpServletResponse response,
			JSONObject jObject) {
		response.setContentType("application/json");
		response.setHeader("Cache-Control", "no-store");
		try {
			PrintWriter pw = response.getWriter();
			pw.write(jObject.toString());
			pw.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// 数据变换的统一接口
	public interface IMyDataExchanger {
		public Object exchange(Object value);
	}

	// 页面表示数据与数据库字段的对应关系
	public static class Db2Page {
		String fieldPage; // 页面的fieldID
		String columnDB; // 数据库的字段名
		IMyDataExchanger dataExchanger; // 数据变换

		// 构造函数1：当页面的fieldID与数据库字段一致时（数据也不用变换）
		public Db2Page(String fieldPage) {
			this.fieldPage = fieldPage;
			this.columnDB = fieldPage;
			this.dataExchanger = null;
		}

		// 构造函数2：当页面的fieldID与数据库字段不一致时（数据不用变换）
		public Db2Page(String fieldPage, String columnDB) {
			this.fieldPage = fieldPage;
			if (columnDB == null) {// 与fieldPage相同
				this.columnDB = fieldPage;
			} else {
				this.columnDB = columnDB;
			}
			this.dataExchanger = null;
		}

		// 构造函数3：当页面的fieldID与数据库字段不一致，且数据要进行变换（当然都用这个构造函数也行）
		public Db2Page(String fieldPage, String columnDB,
				IMyDataExchanger dataExchanger) {
			this.fieldPage = fieldPage;
			if (columnDB == null) {// 与fieldPage相同
				this.columnDB = fieldPage;
			} else {
				this.columnDB = columnDB;
			}
			this.dataExchanger = dataExchanger;
		}

		/**
		 * 取页面表示绑定的fieldID
		 */
		public String getKey() {
			return fieldPage;
		}

		/**
		 * 取页面表示对应的值
		 * 
		 * @param mapDB
		 *            : 从数据库直接取得的结果集(一条数据的MAP)
		 * @return Object : 页面表示对应的值
		 */
		public Object getData(Map mapDB) {
			Object objValue = mapDB.get(columnDB);
			if (objValue == null) {
				return null;
			} else {
				if (dataExchanger != null) {
					return dataExchanger.exchange(objValue);
				} else {
					return objValue;
				}
			}
		}
	}

	/***
	 * mapList转为json
	 * 
	 * @author zhang.rui
	 */
	public static JSONObject getJsonDatagridEasyUI(
			List<Map<String, Object>> mapList, int iTotalCnt,
			Db2Page[] dataExchanger) {
		String jsonTemp = "{\'total\':" + iTotalCnt + ",\'rows\':[";
		for (int j = 0; j < mapList.size(); j++) {
			Map<String, Object> m = mapList.get(j);
			if (j > 0) {
				jsonTemp += ",";
			}
			jsonTemp += "{";
			for (int i = 0; i < dataExchanger.length; i++) {
				if (i > 0) {
					jsonTemp += ",";
				}
				jsonTemp += "'" + dataExchanger[i].getKey() + "'" + ":";
				Object objValue = dataExchanger[i].getData(m);
				if (objValue == null) {
					jsonTemp += "null";
				} else {
					jsonTemp += "'" + objValue + "'";
				}
			}
			jsonTemp += "}";
		}
		jsonTemp += "]}";
		JSONObject jObject = JSONObject.fromObject(jsonTemp);
		return jObject;
	}

	/**
	 * list转为json
	 */
	public static JSONObject getJsonForPage(List list, int totalRow) {
		String json = JsonTurn.map2json(PageForEasyUi(list, totalRow));
		JSONObject jObject = JSONObject.fromObject(json);
		return jObject;

	}

	
	
	/***
	 *前台分页，后台不分页 
	 */
	public static Map<String, Object> PageForEasyUi(List list, int totalRow) {
		Map<String, Object> josnMap = new HashMap<String, Object>();
		josnMap.put("total", totalRow);
		josnMap.put("rows", list);
		return josnMap;
	}
}

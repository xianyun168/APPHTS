package com.ciitc.webServices.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class QuarterTime {
private final static SimpleDateFormat shortSdf = new SimpleDateFormat("yyyy-MM-dd");  
private final static SimpleDateFormat longSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");;  
public static Date getCurrentQuarterStartTime() {  
		
        Calendar c = Calendar.getInstance();  
        int currentMonth = c.get(Calendar.MONTH) + 1;  
        Date now = null;  
        try {  
            if (currentMonth >= 1 && currentMonth <= 3)  
                c.set(Calendar.MONTH, 0);  
            else if (currentMonth >= 4 && currentMonth <= 6)  
                c.set(Calendar.MONTH, 3);  
            else if (currentMonth >= 7 && currentMonth <= 9)  
                c.set(Calendar.MONTH, 4);  
            else if (currentMonth >= 10 && currentMonth <= 12)  
                c.set(Calendar.MONTH, 9);  
            c.set(Calendar.DATE, 1);  
            now = longSdf.parse(shortSdf.format(c.getTime()) + " 00:00:00");  
        } catch (Exception e) {  
            e.printStackTrace();  
        }  
        return now;  
    }  
	 public static Date getCurrentQuarterEndTime() {  
	        Calendar c = Calendar.getInstance();  
	        int currentMonth = c.get(Calendar.MONTH) + 1;  
	        Date now = null;  
	        try {  
	            if (currentMonth >= 1 && currentMonth <= 3) {  
	                c.set(Calendar.MONTH, 2);  
	                c.set(Calendar.DATE, 31);  
	            } else if (currentMonth >= 4 && currentMonth <= 6) {  
	                c.set(Calendar.MONTH, 5);  
	                c.set(Calendar.DATE, 30);  
	            } else if (currentMonth >= 7 && currentMonth <= 9) {  
	                c.set(Calendar.MONTH, 8);  
	                c.set(Calendar.DATE, 30);  
	            } else if (currentMonth >= 10 && currentMonth <= 12) {  
	                c.set(Calendar.MONTH, 11);  
	                c.set(Calendar.DATE, 31);  
	            }  
	            now = longSdf.parse(shortSdf.format(c.getTime()) + " 23:59:59");  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	        return now;  
	    }  
  

}

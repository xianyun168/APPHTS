package com.ciitc.webServices.util;

import java.text.ParseException;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

@SuppressWarnings("serial")
public class QuartzServlet extends HttpServlet {

	private static Scheduler sched;
	private static SchedulerFactory schedFact;

	public void init() throws ServletException {

		try {
			startQuartz();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}

	}

	public void startQuartz() throws SchedulerException {
		schedFact = new StdSchedulerFactory();
		try {
			sched = schedFact.getScheduler();
			Log.getLogger().info("初始化定时器成功");
		} catch (SchedulerException e) {
			Log.getLogger().error("初始化定时器失败" + e.toString());
		}
		newsTimer();
		newsTimerlb();
	}

	//新闻发布定时(非轮播新闻发布定时)
	@SuppressWarnings("static-access")
	public void newsTimer() {
		// 设置每分钟轮播一次
		String timeconfig="0 */1 * * * ?";
		if (timeconfig == null || timeconfig.length() == 0) {
			return;
		}
		Log.getLogger().info("新闻轮询数据库间隔配置:=" + timeconfig);
		try {
			if (sched.getTrigger("newsTrigger", "newsJob") == null) {
				JobDetail dJobDetail = new JobDetail("newsJob",sched.DEFAULT_GROUP, NewsTimer.class);
				CronTrigger dateTrigger = new CronTrigger("newsTrigger","newsJob", timeconfig);
				sched.scheduleJob(dJobDetail, dateTrigger);
			} else {
				sched.pauseTrigger("newsTrigger", "newsJob");
				sched.unscheduleJob("newsTrigger", "newsJob");
				sched.deleteJob("newsJob", sched.DEFAULT_GROUP);
				JobDetail dJobDetail = new JobDetail("newsJob",sched.DEFAULT_GROUP, NewsTimer.class);
				CronTrigger dateTrigger = new CronTrigger("newsTrigger","newsJob", timeconfig);
				sched.scheduleJob(dJobDetail, dateTrigger);
			}
			sched.start();
		} catch (SchedulerException e) {
			e.printStackTrace();
			Log.getLogger().error("非轮播新闻定时失败"+ e.getMessage());
		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	
	//轮播新闻发布定时
		@SuppressWarnings("static-access")
		public void newsTimerlb() {
			// 设置每分钟轮播一次
			String timeconfig="0 */1 * * * ?";
			if (timeconfig == null || timeconfig.length() == 0) {
				return;
			}
			Log.getLogger().info("轮播图轮询间隔配置:=" + timeconfig);
			try {
				if (sched.getTrigger("newslbTrigger", "newslbJob") == null) {
					JobDetail dJobDetail = new JobDetail("newslbJob",sched.DEFAULT_GROUP, NewsTimerlb.class);
					CronTrigger dateTrigger = new CronTrigger("newslbTrigger","newslbJob", timeconfig);
					sched.scheduleJob(dJobDetail, dateTrigger);
				} else {
					sched.pauseTrigger("newslbTrigger", "newslbJob");
					sched.unscheduleJob("newslbTrigger", "newslbJob");
					sched.deleteJob("newslbJob", sched.DEFAULT_GROUP);
					JobDetail dJobDetail = new JobDetail("newslbJob",sched.DEFAULT_GROUP, NewsTimerlb.class);
					CronTrigger dateTrigger = new CronTrigger("newslbTrigger","newslbJob", timeconfig);
					sched.scheduleJob(dJobDetail, dateTrigger);
				}
				sched.start();
			} catch (SchedulerException e) {
				e.printStackTrace();
				Log.getLogger().error("轮播新闻定时失败"+ e.getMessage());
			} catch (ParseException e) {
				e.printStackTrace();
			}

		}


	/**
	 * 删掉所有定时任务
	 * @throws SchedulerException 
	 */
	@SuppressWarnings("static-access")
	public void colesQuartz(Map<String, String> quartzs) throws SchedulerException {
		for (Map.Entry<String, String> mapsub : quartzs.entrySet()) {
			String jobDetail = mapsub.getValue();
			String cronTrigger = mapsub.getKey();
				Scheduler sched = schedFact.getScheduler();
				if (sched.getTrigger(cronTrigger, jobDetail) != null) {
					Log.getLogger().info("关闭定时任务: " + cronTrigger + "   " + jobDetail);
					sched.pauseTrigger(cronTrigger, jobDetail);// 停止触发器
					sched.unscheduleJob(cronTrigger, jobDetail);// 移除触发器
					sched.deleteJob(jobDetail, sched.DEFAULT_GROUP);// 删除任务
				}

		}

	}
	/**
	 * 定时器销毁方法
	 */
	public void destroy() {
		try {
			sched.shutdown();
		} catch (SchedulerException e) {
			e.printStackTrace();
		}
	}
}

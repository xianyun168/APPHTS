package com.ciitc.webServices.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.ciitc.webServices.model.CPcfResult;


public class ExcelUtil {

	// 导出
	public static void excel(List<CPcfResult> list,HttpServletRequest request,HttpServletResponse response,String fileName) {
		try {
			HSSFWorkbook wb=new HSSFWorkbook();
			
			//nameTime = URLEncoder.encode(nameTime, "UTF-8");
			HSSFSheet sheet=wb.createSheet("付息列表");
			//生成行
			HSSFRow row=sheet.createRow((int) 0);
			//设置表格默认列宽度
			sheet.setDefaultColumnWidth(25);
			HSSFCellStyle style=wb.createCellStyle();
			style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
			
			HSSFCell cell=row.createCell(0);
			cell.setCellValue("客户姓名");
			cell.setCellStyle(style);
			
			cell=row.createCell(1);
			cell.setCellValue("产品名称");
			cell.setCellStyle(style);
			
			cell=row.createCell(2);
			cell.setCellValue("客户手机号");
			cell.setCellStyle(style);
			
			cell=row.createCell(3);
			cell.setCellValue("合同编号");
			cell.setCellStyle(style);
			
			cell=row.createCell(4);
			cell.setCellValue("本期收益");
			cell.setCellStyle(style);
			
			cell=row.createCell(5);
			cell.setCellValue("现金流类型");
			cell.setCellStyle(style);
			
			cell=row.createCell(6);
			cell.setCellValue("付息时间");
			cell.setCellStyle(style);
			
			cell=row.createCell(7);
			cell.setCellValue("收益确认状态");
			cell.setCellStyle(style);
			
			cell=row.createCell(8);
			cell.setCellValue("付息确认状态");
			cell.setCellStyle(style);
			
		
			// 在相应的单元格进行赋值

			for (int i = 0; i < list.size(); i++) {
				row=sheet.createRow((int)i+1);

				CPcfResult cPcfResult = list.get(i);
				// 在相应的单元格进行赋值
				   if(cPcfResult.getData_date()!=null && !cPcfResult.getData_date().equals("")){
				        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					    String cancellationTime  =  sdf.format(cPcfResult.getData_date());
					    //客户姓名
					    row.createCell(0).setCellValue(cPcfResult.getC_custnm());
					    //产品名称
					    row.createCell(1).setCellValue(cPcfResult.getL_proserialnm());
					    //客户手机号
					    row.createCell(2).setCellValue(cPcfResult.getC_mobile());
					    //合同编号
					    row.createCell(3).setCellValue(cPcfResult.getContract_Id());
					    //本期收益
					    row.createCell(4).setCellValue(cPcfResult.getAmount());
			      	if (cPcfResult.getFin_element_ID().equals("A101")) {
				    	cPcfResult.setFin_element_ID("本金");
					}else if(cPcfResult.getFin_element_ID().equals("A102")){
						cPcfResult.setFin_element_ID("利息");
					}
			      	row.createCell(5).setCellValue(cPcfResult.getFin_element_ID());
			       	
				 if (cPcfResult.getConfirm_Flag().equals("A")) {
				    	cPcfResult.setConfirm_Flag("已确认");
					}else if(cPcfResult.getConfirm_Flag().equals("N")){
						cPcfResult.setConfirm_Flag("未确认");
					}else if(cPcfResult.getConfirm_Flag().equals("Y")){
						cPcfResult.setConfirm_Flag("已确认");
					}
				row.createCell(6).setCellValue(cancellationTime);
				row.createCell(7).setCellValue(cPcfResult.getConfirm_Flag());
				 if (cPcfResult.getFlag().equals("Y")) {
				    	cPcfResult.setFlag("已确认");
					}else if(cPcfResult.getFlag().equals("N")){
						cPcfResult.setFlag("未确认");
					}else if(cPcfResult.getFlag().equals("A")){
						cPcfResult.setFlag("未确认");
					}		
				row.createCell(8).setCellValue(cPcfResult.getFlag());		
				}
				
			}

			// 将产生的excel以流方式输出
			response.setContentType("application/binary;charset=ISO8859_1");
			response.setHeader("Content-disposition", "attachment; filename=" + fileName+".xls");
			ServletOutputStream outputStream = response.getOutputStream();
			wb.write(outputStream);
			outputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
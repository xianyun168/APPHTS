package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.ciitc.webServices.model.Application;
import com.ciitc.webServices.model.CPReservation;
import com.ciitc.webServices.model.Contract;
import com.ciitc.webServices.model.FileUp;
import com.ciitc.webServices.model.Integration;
import com.ciitc.webServices.model.Reservation;
import com.ciitc.webServices.model.ReservationDtl;

import oracle.jdbc.OracleTypes;
import org.junit.Test;

public class CRM_ReservationUtil {
	
	// 连接耀莱CRM数据库
//	Connection conn = DBUtil.makeConnection();
//	CallableStatement call = null;
	//认购预约
	public String reservation(CPReservation cpreservation) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String code = null;
		try {
			call=conn.prepareCall("{call PKG_YL_HIS_DATA.sp_appaddtreserve"+
					"(?,?,?,?,?,?,?,?,?)}");
			call.registerOutParameter(1, OracleTypes.VARCHAR);// 0:失败 1成功
			call.registerOutParameter(2, OracleTypes.VARCHAR);
			call.setString(3, cpreservation.getpCustno());//客户编号
			call.setInt(4, cpreservation.getpProserialno());//项目编号
			call.setString(5, cpreservation.getpFundcode());//产品编号
			call.setDouble(6, cpreservation.getpBalance());//预约金额
			call.setString(7, cpreservation.getpExpirationdate());//预约有效截止日期
			call.setString(8, cpreservation.getpRemitdate());//预计缴款日期
			call.setString(9, cpreservation.getpBrokeraccount());//客户经理
			
			// 执行
			call.execute();
			String state = call.getObject(1).toString();
			String reason =call.getObject(2).toString();
			code = state+","+reason;
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;
	}
	//根据项目编码查询产品编码
	public String getFundcode(String p_proserialno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String code = null;
		try {
			call = conn.prepareCall("select * from tsaleprofundrelation where L_PROSERIALNO = '" + p_proserialno+"' ");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				code = rs.getString("C_FUNDCODE");//产品代码
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return code;
	}
	//根据产品编码查询项目编码
		public String getProserialno(String c_fundcode) {
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			String code = null;
			try {
				call = conn.prepareCall("select * from tsaleprofundrelation where C_FUNDCODE = '" + c_fundcode+"' ");
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					code = rs.getString("L_PROSERIALNO");//产品代码
				}
				if(rs !=null){
					rs.close();
				}
				if(call !=null){
					call.close();
				}
				if(conn !=null){
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return code;
		}
	//根据理财师的id查询理财师的部门
		public String getNodecode(String financialId) {
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			String code = null;
			try {
				call = conn.prepareCall("select C_DEPTNODECODE  from tfundbroker where C_BROKERACCOUNT = '" + financialId+"' ");
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					code = rs.getString("C_DEPTNODECODE");//理财师的部门编号
				}
				if(rs !=null){
					rs.close();
				}
				if(call !=null){
					call.close();
				}
				if(conn !=null){
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return code;
		}
	
	//取消预约
	public String cellReservation(String reserveno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String code = null;
		try {
			call=conn.prepareCall("{call PKG_CRM_INTERFACE.sp_cancelreserve(?,?,?,?)}");
			call.setString(1, reserveno);
			call.setString(2, "000");
			call.registerOutParameter(3, OracleTypes.VARCHAR);
			call.registerOutParameter(4, OracleTypes.VARCHAR);
			// 执行
			call.execute();
			String state = call.getObject(3).toString();
			String reason = call.getObject(4).toString();
			code = state+","+reason;
			
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;
	}
	/*//查询预约列表
	public String getReservationList(String reserveno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String code = null;
		try {
			call=conn.prepareCall("{call PKG_CRM_INTERFACE.sp_cancelreserve(?,?,?,?)}");
			call.setString(1, reserveno);
			call.setString(2, "000");
			call.registerOutParameter(3, OracleTypes.VARCHAR);
			call.registerOutParameter(4, OracleTypes.VARCHAR);
			// 执行
			call.execute();
			String state = call.getObject(1).toString();
			String reason = call.getObject(2).toString();
			code = state+","+reason;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;
	}*/
	//查询预约列表
	public List<Reservation> getReservationList() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		List<Reservation> reservationList = new ArrayList<Reservation>();
		try {
			//call=conn.prepareCall("select C_FUNDCODE,sum(F_BALANCE),count(C_CUSTNO)  from treserve where C_STATUS in('0','3','4','5','6','7','a','b','c','d')  group by C_FUNDCODE");
			call=conn.prepareCall("SELECT tr.C_FUNDCODE as C_FUNDCODE ,sum(F_BALANCE), count(C_CUSTNO)" +
					"  FROM treserve tr left join tsaleproject ts on tr.l_proserialno=ts.l_proserialno" +
					"  WHERE tr.C_STATUS IN ('0', '3', '4', '5', '6', '7', 'a', 'b', 'c', 'd')" +
					"  GROUP BY tr.C_FUNDCODE,ts.c_name,ts.c_status,ts.l_proserialno" +
					"  order by ts.c_status asc,ts.l_proserialno desc");
			// 执行
			ResultSet rs = call.executeQuery();
			while(rs.next()) {
				Reservation reservation = new Reservation();
				reservation.setC_fundcode(rs.getString("C_FUNDCODE"));
				//根据产品的编号查询产品的名称
				String productName = getProductName(rs.getString("C_FUNDCODE"));
				reservation.setC_fundname(productName);
				reservation.setCountMoney(rs.getInt("sum(F_BALANCE)"));
				reservation.setNum(rs.getInt("count(C_CUSTNO)"));
				reservationList.add(reservation);
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reservationList;
	}
	//查询预约详情列表
	public List<ReservationDtl> getReservationDtlList(String financialId,String funCode) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<ReservationDtl> reservationDtlList = new ArrayList<ReservationDtl>();
		try {
			call=conn.prepareCall(" select * from treserve where C_FUNDCODE = '"+funCode+"' and C_BROKERACCOUNT = '"+financialId+"' order by  D_RESERVEDATE desc");
			// 执行
			ResultSet rs = call.executeQuery();
			while(rs.next()) {
				ReservationDtl reservationDtl = new ReservationDtl();
				reservationDtl.setC_custno(rs.getString("C_CUSTNO"));
				String custno = rs.getString("C_CUSTNO");
				//根据客户编号查询客户的名称
				String custnoName =  getCustName(custno);
				reservationDtl.setC_custname(custnoName);
				//预约状态
				reservationDtl.setC_status(rs.getString("C_STATUS"));
				//预约时间
				reservationDtl.setD_reservedate(sdf.format(rs.getTimestamp("D_RESERVEDATE")));
				//预约金额
				reservationDtl.setF_balance(rs.getInt("F_BALANCE"));
				//预约编号
				reservationDtl.setC_reserveno(rs.getString("C_RESERVENO"));
				//受益级别
				reservationDtl.setC_profitclass(rs.getString("C_PROFITCLASS"));
				//产品编号
				reservationDtl.setC_fundcode(rs.getString("C_FUNDCODE"));
				reservationDtlList.add(reservationDtl);
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reservationDtlList;
	}
	//缴款申请
	public String application(Application application){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String code = null;
		try {
			call=conn.prepareCall("{call PKG_YL_PAY_DATA.SP_PAYMENT(?,?,?,?,?,?,?,?,?)}");
			//C_RESERVENO_IN 预约号
			call.setString(1, application.getReserveno());
			//PAYDATE 付款日期
			call.setString(2, application.getPaydate());
			//PAYAMT 付款金额
			call.setInt(3, application.getPayamt());
			//BANKNO 银行编码
			call.setString(4, application.getBankNo());
			//BANKBRUNNM 支行名称
			call.setString(5, application.getBankbrunnm());
			//ACCTNM 付款账号名
			call.setString(6, application.getAcctnm());
			//ACCTNO 付款账号
			call.setString(7, application.getAcctno());
			call.registerOutParameter(8, OracleTypes.VARCHAR);
			call.registerOutParameter(9, OracleTypes.VARCHAR);
			// 执行
			call.execute();
			String state = call.getObject(8).toString();
			String reason = call.getObject(9).toString();
			code = state+"@#"+reason;
	
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;	 	
	}
	//保存附件
	public String saveFile(FileUp fileUp) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		CallableStatement call1 = null;
		String l_storageid = null;
		try {
			call1 = conn.prepareCall("select seq_tblobstorage.nextval from dual");
			ResultSet rs = call1.executeQuery();
			while(rs.next()) {
				l_storageid = rs.getString("NEXTVAL");
			}
			call = conn.prepareCall("insert into tblobstorage( " + 
					"            l_storageid        ,c_catalog        ,c_subkeyid      ,c_filename         ,c_creator        ,d_createdate    , " + 
					"            c_securitylevel    ,c_isuse          ,l_filegroupno   ,l_fileno           ,c_filepath       ,c_filepathtype  , " + 
					"            C_NODECODE,L_FILESIZE,C_UUID " + 
					"          ) " + 
					"        select  "+l_storageid+",'"+fileUp.getC_catalog()+"' ,"+fileUp.getV_serialno()+" ,'"+fileUp.getP_filename()+"' ,'"+fileUp.getV_creator()+"' ,sysdate , " + 
					"             0,null ,null ,null ,null ,null, " + 
					"             '"+fileUp.getV_nodecode()+"',"+fileUp.getP_filesize()+",sys_guid() from dual");
			// 执行
			call.executeUpdate();
			if(rs !=null){
				rs.close();
			}
			if(call1 !=null){
				call1.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_storageid;
	}
	//实名认证第一张附件保存
	public String saveFile1(FileUp fileUp) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String l_storageid = fileUp.getL_storageid();
		try {
			call = conn.prepareCall("insert into tblobstorage( " + 
					"            l_storageid  ,c_catalog ,c_subkeyid ,c_filename ,L_FILESIZE ,c_creator  ,d_createdate  , " + 
					"            c_securitylevel  ,c_memo ,  C_NODECODE,C_UUID " + 
					"          ) " + 
					"        VALUES(  "+l_storageid+",'"+fileUp.getC_catalog()+"' ,"+fileUp.getV_serialno()+" ,'"+fileUp.getP_filename()+"',"+fileUp.getP_filesize()+" ,'"+fileUp.getV_creator()+"' ,sysdate , " + 
					"             0, '证件扫描件',  '"+fileUp.getV_nodecode()+"',sys_guid())");
			// 执行
			call.executeUpdate();
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_storageid;
	}
	//实名认证剩余附件保存
	public String saveFile2(FileUp fileUp) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		CallableStatement call1 = null;
		String l_storageid = null;
		try {
			call1 = conn.prepareCall("select seq_tblobstorage.nextval from dual");
			ResultSet rs = call1.executeQuery();
			while(rs.next()) {
				l_storageid = rs.getString("NEXTVAL");
			}
			call = conn.prepareCall("insert into tblobstorage( " + 
					"            l_storageid  ,c_catalog ,c_subkeyid ,c_filename ,L_FILESIZE ,c_creator  ,d_createdate  , " + 
					"            c_securitylevel ,  C_NODECODE,C_UUID " + 
					"          ) " + 
					"        VALUES(  "+l_storageid+",'"+fileUp.getC_catalog()+"' ,"+fileUp.getV_serialno()+" ,'"+fileUp.getP_filename()+"',"+fileUp.getP_filesize()+" ,'"+fileUp.getV_creator()+"' ,sysdate , " + 
					"             0,  '"+fileUp.getV_nodecode()+"',sys_guid())");
			// 执行
			call.executeUpdate();
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(call1 !=null){
				call1.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_storageid;
	}
	//查询保存图的编号
	/*public String queryNo(FileUp fileUp) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String l_storageid = null;
		try {
			call = conn.prepareCall("select l_storageid from tblobstorage where "
					+ " c_catalog = '"+fileUp.getC_catalog()+"' and c_subkeyid ="+fileUp.getV_serialno()+ " and c_filename = '"+fileUp.getP_filename()+"' and c_creator = '"+fileUp.getV_creator()+
					"' and  C_NODECODE = '"+fileUp.getV_nodecode()+"' and L_FILESIZE = "+fileUp.getP_filesize());
			// 执行
			ResultSet rs = call.executeQuery();
			while(rs.next()){
				l_storageid = rs.getString("l_storageid");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return l_storageid;
	}*/
	//合同申请
	public String appcreatecontract(Contract contract) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String code = null;
		try {
			call=conn.prepareCall("{call PKG_YL_HIS_DATA.sp_appcreatecontract(?,?,?,?,?,?,?,?,?,?,?,?)}");
			//预约编号
			call.setString(1, contract.getP_reserveno());
			//项目编号
			call.setInt(2, contract.getP_proserialno());
			//合同金额
			call.setInt(3, contract.getP_contractbalance());
			//合同签署日期
			call.setDate(4, new Date(contract.getP_contractsigndate().getTime()));
			//合同起息日期
			call.setDate(5, new Date(contract.getP_fundopendate().getTime()));
			//开户行号
			call.setString(6, contract.getP_bankname());
			//开户支行
			call.setString(7, contract.getP_subbranchname());
			//开户账号
			call.setString(8, contract.getP_bankacco());
			//开户姓名
			call.setString(9, contract.getP_nameinbank());
			//合同顺序号
			call.setString(10, contract.getC_trustcontractid());
			//错误代码
			call.registerOutParameter(11, OracleTypes.VARCHAR);
			//错误信息
			call.registerOutParameter(12, OracleTypes.VARCHAR);
			// 执行
			call.execute();
			String state = call.getObject(11).toString();
			String reason = call.getObject(12).toString();
			code = state+","+reason;
			
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;	
	}
	//根据银行名称查询出银行的编号
	public String getBankNo(String bankName) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String code = null;
		try {
			call = conn.prepareCall("select * from tdictionary where l_keyno='3010' and c_sysname='FUNDCRM' and C_CAPTION = '"+bankName+"'");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				code = rs.getString("C_KEYVALUE");//产品代码
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return code;
	}
	//查询合同计算积分(2018-01-01之前的合同)
	public List<Integration> getContract() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		List<Integration> integrationList = new ArrayList<Integration>();
		try {
			call = conn.prepareCall("select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE  from ttrustcontract_req q ,tfundinfo o where q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS = '2' and D_FUNDOPENDATE <= to_date('20171231','yyyymmdd')");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				Integration integration = new Integration();
				integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
				integration.setCustno(rs.getString("C_CUSTNO"));
				integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
				integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
				integration.setProtuctName(rs.getString("C_FUNDNAME"));
				if(null !=rs.getString("D_FUNDOPENDATE") ) {
					Date aa = rs.getDate("D_CONTRACTENDDATE");
					long a =  aa.getTime();
					Date bb = rs.getDate("D_FUNDOPENDATE");
					long b =  bb.getTime();
					long day = ((a-b)/(24*60*60*1000));
					integration.setPositionDate((int)day);
				}
				integrationList.add(integration);
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return integrationList;
	}
	//查询合同计算积分(2018-01-01之后的合同)
	public List<Integration> getContract1() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		List<Integration> integrationList = new ArrayList<Integration>();
		try {
			call = conn.prepareCall("select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE  from ttrustcontract_req q ,tfundinfo o where q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS = '2'and   to_date('20180101','yyyymmdd') <= D_FUNDOPENDATE");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				Integration integration = new Integration();
				integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
				integration.setCustno(rs.getString("C_CUSTNO"));
				integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
				integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
				integration.setProtuctName(rs.getString("C_FUNDNAME"));
				if(null !=rs.getString("D_FUNDOPENDATE") ) {
					Date aa = rs.getDate("D_CONTRACTENDDATE");
					long a =  aa.getTime();
					Date bb = rs.getDate("D_FUNDOPENDATE");
					long b =  bb.getTime();
					long day = ((a-b)/(24*60*60*1000));
					integration.setPositionDate((int)day);
				}
				integrationList.add(integration);
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return integrationList;
	}
	//查询合同计算积分(初始化)
	/*public List<Integration> getContract1() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		List<Integration> integrationList = new ArrayList<Integration>();
		try {
			call = conn.prepareCall("select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE  from ttrustcontract_req q ,tfundinfo o where q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS = '2'");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				Integration integration = new Integration();
				integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
				integration.setCustno(rs.getString("C_CUSTNO"));
				integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
				integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
				integration.setProtuctName(rs.getString("C_FUNDNAME"));
				if(null !=rs.getString("D_FUNDOPENDATE") ) {
					Date aa = rs.getDate("D_CONTRACTENDDATE");
					long a =  aa.getTime();
					Date bb = rs.getDate("D_FUNDOPENDATE");
					long b =  bb.getTime();
					long day = ((a-b)/(24*60*60* 1000))-1;
					integration.setPositionDate((int)day);
				}
				integrationList.add(integration);
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return integrationList;
	}*/
	//查询合同计算积分(定时每天查询当天的合同)
	public List<Integration> getContractNow(String nowDay) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		List<Integration> integrationList = new ArrayList<Integration>();
		try {
			/*call = conn.prepareCall("select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE  from ttrustcontract_req q ,tfundinfo o where q.C_FUNDCODE = o.C_FUNDCODE " + 
					" and q.C_STATUS = '2' and q.c_fundcode not in (select tsp.prod_id from t_spci_prod@odslink tsp where tsp.spci_tye='1') and to_char(q.D_AUDITDATE,'yyyy-MM-dd') like '"+nowDay+"'");*/
			call = conn.prepareCall("select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE ,q.D_AUDITDATE from " + 
					"   (select * from ttrustcontract_req@crmlink trq where trq.c_status='2' and not exists " + 
					"  (select 1 from APP_INTEGRAL_DETAILS aid where trq.c_trustcontractid=aid.contract_num))  q ,tfundinfo@crmlink o where q.C_FUNDCODE = o.C_FUNDCODE " + 
					"  and q.c_fundcode not in (select tsp.prod_id from t_spci_prod@odslink tsp where tsp.spci_tye='1') and q.D_FUNDOPENDATE is not null");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				Integration integration = new Integration();
				integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
				integration.setCustno(rs.getString("C_CUSTNO"));
				integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
				integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
				integration.setProtuctName(rs.getString("C_FUNDNAME"));
				if(null !=rs.getString("D_FUNDOPENDATE") ) {
					Date aa = rs.getDate("D_CONTRACTENDDATE");
					long a =  aa.getTime();
					Date bb = rs.getDate("D_FUNDOPENDATE");
					long b =  bb.getTime();
					long day = ((a-b)/(24*60*60*1000));
					integration.setPositionDate((int)day);
				}
				integrationList.add(integration);
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return integrationList;
	}
	//根据客户的编号查询客户的姓名
	public String getCustName(String custno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String custName = null;
		try {
			call = conn.prepareCall(" select C_CUSTNAME from tunregister where C_CUSTNO =  '" + custno+"'");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				custName = rs.getString("C_CUSTNAME");
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return custName;
	}
	//根据产品的编号查询产品的名称
	public String getProductName(String productCode) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String productName = null;
		try {
			call = conn.prepareCall("select C_FUNDNAME from tfundinfo where C_FUNDCODE = '" + productCode+"'");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				productName = rs.getString("C_FUNDNAME");
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productName;
	}

	//根据项目的编号查询项目的名称
	public String getProductName1(String productCode) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String productName = null;
		try {
			call = conn.prepareCall("select C_NAME from tsaleproject where L_PROSERIALNO = '" + productCode+"'");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				productName = rs.getString("C_NAME");
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return productName;
	}

}

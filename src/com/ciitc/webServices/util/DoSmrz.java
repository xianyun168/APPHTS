package com.ciitc.webServices.util;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DoSmrz {
    /**
     * @param username 用户名
     * @param idCard   身份证号
     * @param serialNo L_serialNo
     * @return
     * @date 2018/6/27 17:29
     * @Description校验用户合法性
     */
    public static String smrz(String username, String idCard, String serialNo) {
        Connection conn = null;
        PreparedStatement pstmt = null; //通常利用PreparedStatement进行操作，性能得到优化
        PreparedStatement pstmt1 = null; //通常利用PreparedStatement进行操作，性能得到优化
        PreparedStatement pstmtTEMP = null; //通常利用PreparedStatement进行操作，性能得到优化
        ResultSet rs = null;//定义存放查询结果的结果集
        String returnStatus = "";
        String returnStatusChina = returnStatus = HttpUtils.getStatus(username, idCard);
        conn = DbUtils.getConnection();
        if (returnStatusChina.equals("01")) {
            returnStatusChina = "实名认证通过";
        }
        if (returnStatusChina.equals("02")) {
            returnStatusChina = "实名认证不通过";
        }
        if (returnStatusChina.equals("202")) {
            returnStatusChina = "无法验证";
        }
        if (returnStatusChina.equals("203")) {
            returnStatusChina = "异常情况";
        }
        if (returnStatusChina.equals("204")) {
            returnStatusChina = "姓名格式不正确";
        }
        if (returnStatusChina.equals("205")) {
            returnStatusChina = "身份证格式不正确";
        }
        if (returnStatus == null || returnStatus.equals("")) {
            System.out.println("剩余查询次数为零,请购买!");
        } else {
            //更新表
            String updateSql = "update TCUSTREGISTERREQ set C_STATUS=?,C_REMARK=? where L_SERIALNO=?";
            //根据验证数据，插入日志
            String insertLog = "INSERT INTO TCUSTREGISTERREQCHANGE  (L_ID,L_SERIALNO,C_STATUS,C_REMARK,C_OPERATOR,D_OPDATE) VALUES" +
                    " (SEQ_TCUSTREGISTERREQCHANGE.nextval, ?, ?,? , '000000', SYSDATE)";
            try {
                pstmt = conn.prepareStatement(updateSql);
                pstmt1 = conn.prepareStatement(insertLog);

                String c_custno = "";
                String getCustemno = "select c_custno from tcustregisterreq where l_serialno = ?";
                pstmtTEMP = conn.prepareStatement(getCustemno);
                pstmtTEMP.setString(1, serialNo);
                rs = pstmtTEMP.executeQuery();
                if (rs.next()) {
                    c_custno = rs.getString("c_custno");
                }
                if (returnStatus.equals("01")) {
                    String passUpdate = "update TUNREGISTER set c_isdelete = '0',c_idnocheckflag = '1' where c_custno = ?";
                    pstmtTEMP = conn.prepareStatement(passUpdate);
                    pstmtTEMP.setString(1, c_custno);
                    pstmtTEMP.executeUpdate();
                    pstmt.setString(1, "2");
                    pstmt1.setString(2, "2");
                } else {
                    pstmt.setString(1, "3");
                    pstmt1.setString(2, "3");
                }
                pstmt.setString(2, returnStatusChina);
                pstmt.setString(3, serialNo);
                pstmt.executeUpdate();
                pstmt1.setString(1, serialNo);
                pstmt1.setString(3, "[客户注册申请审核]" + returnStatusChina);
                pstmt1.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        DbUtils.close(rs);
        DbUtils.close(pstmt);
        DbUtils.close(pstmt1);
        DbUtils.close(pstmtTEMP);
        DbUtils.closeTheConn();
        return returnStatusChina;
    }
}
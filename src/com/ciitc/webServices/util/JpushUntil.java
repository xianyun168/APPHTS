package com.ciitc.webServices.util;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ciitc.webServices.model.AppMessageSend;
import com.ciitc.webServices.model.JpushUntilVo;

import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.ServiceHelper;
import cn.jiguang.common.connection.HttpProxy;
import cn.jiguang.common.connection.NativeHttpClient;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import cn.jpush.api.schedule.ScheduleResult;

public class JpushUntil {
	
	public   Logger LOG = LoggerFactory.getLogger(JpushUntil.class);
	
   	public  JPushClient jpushClient;
   	
  //所有平台所有人全部推送
  	@SuppressWarnings("deprecation")
  	public  void SendPushAll(JpushUntilVo jpushUntilVo) {
  		ClientConfig clientConfig = ClientConfig.getInstance();
          JPushClient jpushClient = new JPushClient(jpushUntilVo.getMasterSecret(),jpushUntilVo.getAppKey(),null,clientConfig);
          String authCode = ServiceHelper.getBasicAuthorization(jpushUntilVo.getAppKey(),jpushUntilVo.getMasterSecret());
          NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
          jpushClient.getPushClient().setHttpClient(httpClient);
          AppMessageSend appMessageSend =jpushUntilVo.getMessageVo();
          SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  		  String time=sdf.format(appMessageSend.getTimingSendDate());
          PushPayload payload=sendToAllAlert(jpushUntilVo);
         try {
         	  // System.out.println(payload.toString());
             PushResult result = jpushClient.sendPush(payload);
             if(result!=null){
          	   appMessageSend.setSendDate(time);
          	   appMessageSend.setSendState("已推送");
          	   LOG.info("Got result - " + result);
                 System.out.println("推送成功！！！！！！");
              }else{
                 System.out.println("result is null..........");
              }
         } catch (APIConnectionException e) {
             LOG.error("Connection error. Should retry later. ", e);
         } catch (APIRequestException e) {
             LOG.error("Error response from JPush server. Should review and fix it. ", e);
             LOG.info("HTTP Status: " + e.getStatus());
             LOG.info("Error Code: " + e.getErrorCode());
             LOG.info("Error Message: " + e.getErrorMessage());
             LOG.info("Msg ID: " + e.getMsgId());
         }
  	}
   	
	//生成推送的内容，全部推送（可以用来推送给所有理财师）
	@SuppressWarnings("deprecation")
	public  void SendPushTag(JpushUntilVo jpushUntilVo) {
		ClientConfig clientConfig = ClientConfig.getInstance();
        JPushClient jpushClient = new JPushClient(jpushUntilVo.getMasterSecret(),jpushUntilVo.getAppKey(),null,clientConfig);
        String authCode = ServiceHelper.getBasicAuthorization(jpushUntilVo.getAppKey(),jpushUntilVo.getMasterSecret());
        NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
        jpushClient.getPushClient().setHttpClient(httpClient);
        AppMessageSend appMessageSend =jpushUntilVo.getMessageVo();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=sdf.format(appMessageSend.getTimingSendDate());
		LOG.info("-------全部推送开始---------" );
        PushPayload payload=sendAllByTag(jpushUntilVo);
       try {
       	   //System.out.println(payload.toString());
           PushResult result = jpushClient.sendPush(payload);
           if(result!=null){
        	   appMessageSend.setSendDate(time);
        	   appMessageSend.setSendState("已推送");
        	   LOG.info("Got result - " + result);
        	   LOG.info(result+"...........成功推送............");
               System.out.println("推送成功！！！！！！");
            }else{
            	LOG.info(result+"...........推送失败............");
               System.out.println("result is null..........");
            }
       } catch (APIConnectionException e) {
           LOG.error("Connection error. Should retry later. ", e);
       } catch (APIRequestException e) {
           LOG.error("Error response from JPush server. Should review and fix it. ", e);
           LOG.info("HTTP Status: " + e.getStatus());
           LOG.info("Error Code: " + e.getErrorCode());
           LOG.info("Error Message: " + e.getErrorMessage());
           LOG.info("Msg ID: " + e.getMsgId());
       }
	}
	
	
	//定时分组发送（后台）
	public  String timSingleSchedule(JpushUntilVo jpushUntilVo) {
		//System.out.println(jpushUntilVo.getTag());
		ClientConfig clientConfig = ClientConfig.getInstance();
        JPushClient jpushClient = new JPushClient(jpushUntilVo.getMasterSecret(),jpushUntilVo.getAppKey(),null,clientConfig);
        String authCode = ServiceHelper.getBasicAuthorization(jpushUntilVo.getAppKey(),jpushUntilVo.getMasterSecret());
        NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
        jpushClient.getPushClient().setHttpClient(httpClient);
        AppMessageSend appMessageSend =jpushUntilVo.getMessageVo();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=sdf.format(appMessageSend.getTimingSendDate());
        String name = "定时推送";
        String state = "";
        LOG.info("-------定时分组推送开始---------" );
        PushPayload push = sendAllByTag(jpushUntilVo);
        try {
            ScheduleResult result = jpushClient.createSingleSchedule(name, time, push);
            if(result != null) {
            	appMessageSend.setSendDate(time);
                appMessageSend.setSendState("已推送");
                LOG.info("schedule result is " + result);
                LOG.info(result+"...........成功推送............");
                System.out.println(result+"...........成功推送............");
                state = "推送成功";
                return state;
               
            }else {
            	LOG.info(result+"...........推送失败............");
            	System.out.println(result+"...........推送失败............");
            	state = "推送失败";
            	return state;
            }
        } catch (APIConnectionException e) {
            LOG.error("Connection error. Should retry later. ", e);
            LOG.info("无法连接到Jpush服务器 ");
        } catch (APIRequestException e) {
            LOG.error("Error response from JPush server. Should review and fix it. ", e);
            LOG.info("HTTP Status: " + e.getStatus());
            LOG.info("Error Code: " + e.getErrorCode());
            LOG.info("Error Message: " + e.getErrorMessage());
        }
		return state;
    }
	
	
	//单个用户推送
	@SuppressWarnings("deprecation")
	public  void SendPushAlias(JpushUntilVo jpushUntilVo) {
		ClientConfig clientConfig = ClientConfig.getInstance();
        JPushClient jpushClient = new JPushClient(jpushUntilVo.getMasterSecret(),jpushUntilVo.getAppKey(),null,clientConfig);
        String authCode = ServiceHelper.getBasicAuthorization(jpushUntilVo.getAppKey(),jpushUntilVo.getMasterSecret());
        NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
        jpushClient.getPushClient().setHttpClient(httpClient);
        AppMessageSend appMessageSend =jpushUntilVo.getMessageVo();
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String time=sdf.format(appMessageSend.getTimingSendDate());
		LOG.info("-------单个用户推送开始---------" );
        PushPayload payload=sendAllByAlias(jpushUntilVo);
       try {
       	   //System.out.println(payload.toString());
           PushResult result = jpushClient.sendPush(payload);
           if(result!=null){
        	   appMessageSend.setSendDate(time);
        	   appMessageSend.setSendState("已推送");
        	   LOG.info("Got result - " + result);
        	   LOG.info(result+"...........成功推送............");
               System.out.println("推送成功！！！！！！");
            }else{
            	LOG.info(result+"...........推送失败............");
               System.out.println("result is null..........");
            }
       } catch (APIConnectionException e) {
           LOG.error("Connection error. Should retry later. ", e);
       } catch (APIRequestException e) {
           LOG.error("Error response from JPush server. Should review and fix it. ", e);
           LOG.info("HTTP Status: " + e.getStatus());
           LOG.info("Error Code: " + e.getErrorCode());
           LOG.info("Error Message: " + e.getErrorMessage());
           LOG.info("Msg ID: " + e.getMsgId());
       }
	}
	
	
	
	//所有平台所有用户
	public  PushPayload sendToAllAlert(JpushUntilVo jpushUntilVo) {
        return PushPayload.newBuilder()
                .setPlatform(Platform.all())//设置接受的平台
                .setAudience(Audience.all())//Audience设置为all，说明采用广播方式推送，所有用户都可以接收到
                .setNotification(Notification.alert(jpushUntilVo.getMessageVo().getMessageContent()))
                .build();
    }
	
	//分组推送
	public  PushPayload sendAllByTag(JpushUntilVo jpushUntilVo) {
		AppMessageSend appMessageSend =jpushUntilVo.getMessageVo();
		return PushPayload.newBuilder()
		.setPlatform(Platform.android_ios())//推送平台
		.setAudience(Audience.tag(jpushUntilVo.getTag()))//推送目标
		.setNotification(Notification.newBuilder() //通知
		.setAlert(appMessageSend.getMessageTitle())
		.addPlatformNotification(AndroidNotification.newBuilder()
		.setTitle(appMessageSend.getMessageTitle()).build())
		.addPlatformNotification(IosNotification.newBuilder()
		.incrBadge(1)
		.addExtra(appMessageSend.getMessageTitle(), appMessageSend.getMessageContent()).build())
		.build()).setOptions(Options.newBuilder().setApnsProduction(false).build())
		.build();
		}
	
	//单个用户推送
	public  PushPayload sendAllByAlias(JpushUntilVo jpushUntilVo) {
		AppMessageSend appMessageSend =jpushUntilVo.getMessageVo();
		return PushPayload.newBuilder()
		.setPlatform(Platform.android_ios())
		.setAudience(Audience.alias(jpushUntilVo.getAlias()))
		.setNotification(Notification.newBuilder()
		.setAlert(appMessageSend.getMessageTitle())
		.addPlatformNotification(AndroidNotification.newBuilder()
		.setTitle(appMessageSend.getMessageTitle()).build())
		.addPlatformNotification(IosNotification.newBuilder()
		.incrBadge(1)
		.addExtra(appMessageSend.getMessageTitle(), appMessageSend.getMessageContent()).build())
		.build()).setOptions(Options.newBuilder().setApnsProduction(false).build())
		.build();
		}
}


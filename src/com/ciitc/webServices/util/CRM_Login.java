package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
public class CRM_Login {
	
	// 连接耀莱CRM数据库
//	Connection conn = DBUtil.makeConnection();
//	CallableStatement call = null;
	
	public Map<String, Object> queryByStatus(String userName,String Pwd) {
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String isLogin="";
		Map<String, Object> map=new HashMap<String, Object>();
		//理财师ID
		String brokeId="";
		String brokeName="";
		try {
			if (userName != null&&!"".equals(userName)) {
				//查用户名和密码
				call = conn.prepareCall("select h.C_PASSWORD,t.C_USERCODE,t.C_BROKERACCOUNT,t.C_BROKERNAME from hsi_user h,TFUNDBROKER t "+ "where h.C_USERCODE=t.C_USERCODE and h.C_PASSWORD='"+Pwd+"' and t.C_USERCODE='"+userName+"'");
			}
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				if (rs!=null) {
					isLogin="00";
					brokeId=(rs.getString("C_BROKERACCOUNT"));// 理财师ID
					brokeName=(rs.getString("C_BROKERNAME"));// 理财师姓名
				}else {
					isLogin="01";
				}
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
			//登陆成功后
			//获取用户详细信息及权限、职位、等级关系（组织架构）信息
			//查询登陆的理财师是否有上级/下级																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																										
		/*	if ("00".equals(isLogin)) {
				//根据理财师登陆名查询上下级关系
				call = conn.prepareCall("SELECT LOGINID,UP_LOGINID FROM  VIEW_EMPL_RELATION where LOGINID='"+userName+"'");
				// 执行
				ResultSet rss = call.executeQuery();
				while (rss.next()) {
					if (rss!=null) {
						System.out.println(rs.getString("UP_LOGINID"));
					}
				}
			}*/
		} catch (Exception e) {
			e.printStackTrace();
		}
			map.put("isLogin",isLogin);
			map.put("brokeId", brokeId);
			map.put("brokeName", brokeName);
		return map;
	}
	
	public String queryByDeptNo(String brokeId) {
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String c_deptNodeCode = null;
		try {
			call = conn.prepareCall(" SELECT C_DEPTNODECODE FROM TFUNDBROKER WHERE C_BROKERACCOUNT = '" + brokeId+"'");
			// 执行
  			ResultSet rs = call.executeQuery();
  			while (rs.next()) {
  				c_deptNodeCode = rs.getString("C_DEPTNODECODE");
  			}
  			System.out.println(c_deptNodeCode);
  			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return c_deptNodeCode;
	}
	//检查用户状态
	public String queryByUserState(String userName) {
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		String C_STATUS = null;
		try {
			call = conn.prepareCall(" select C_STATUS from hsi_user@CRMLINK  where c_usercode = '"+userName+"'");
			// 执行
  			ResultSet rs = call.executeQuery();
  			while (rs.next()) {
  				C_STATUS = rs.getString("C_STATUS");
  			}
  			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return C_STATUS;
	}
	
	
}

package com.ciitc.webServices.util;

import cn.com.jit.assp.dsign.DSign;

public class SignToPdfUtil {
	
	private static SignToPdfUtil instance = new SignToPdfUtil();

	private SignToPdfUtil() {

	}

	public static synchronized SignToPdfUtil getInstance() {
		if (null == instance) {
			instance = new SignToPdfUtil();
		}
		return instance;
	}
	/**
	 * 给生成的PDF电子保单签名
	 * 
	 * @param sign
	 *            配置参数
	 * @throws Exception
	 */
	public String signtoPdf(String certId, String url, String qzhDzbdTemporaryPath, String qmhDzbdTemporaryPath) {
		Log.getLogger().info("签名配置id为:" + certId);
		Log.getLogger().info("签名配置文件路径为:" + url);
		DSign ds = new DSign();
	    boolean falg = ds.initConfig(url);
	    Log.getLogger().info("调用签名服务器加载配置文件是否成功:" + falg);
//		ds.setTimeout("10000"); //配置超时 单位（毫秒） 默认10秒  
		String signReturn = null;
		/*********签名**********/
		Log.getLogger().info("签名前文件路径为:" + qzhDzbdTemporaryPath);
		Log.getLogger().info("签名后文件路径为:" + qmhDzbdTemporaryPath);
		signReturn = ds.detachSignPDF("test", qzhDzbdTemporaryPath, qmhDzbdTemporaryPath);
		
		if (null == signReturn) {
			Log.getLogger().error("调用签名服务器返回错误号为:" + ds.getErrorCode());
			Log.getLogger().error("调用签名服务器返回错误描述:" + ds.getErrorMessage());
			return ds.getErrorMessage();
		} else {
			Log.getLogger().info("PDF文件签名成功,签名值已写入PDF文件！");
			return null;
	    }
	}
	
}

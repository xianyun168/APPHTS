package com.ciitc.webServices.util;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;


import oracle.jdbc.internal.OracleTypes;


import com.ciitc.webServices.dao.FileUploadDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.FileUp;
import com.ciitc.webServices.model.FileUploads;
import com.ciitc.webServices.model.ProfessionalInfo;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import jcifs.smb.SmbFile;
import net.sf.json.JSONObject;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年5月24日 下午5:36:24 
 * 修 改 人: 
 ******************************************************************************/
public class CRM_ProfessionalUntil {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	
	private FileUploadDao fileUploadDao = (FileUploadDao) SpringContextUtil.getBean("fileUploadDao");
	
		// 连接耀莱CRM数据库
//		Connection conn = DBUtil.makeConnection();
//		CallableStatement call = null;
		String state = null;
		String msg = null;
		//实名认证后----专业投资者认证申请
		public String professionalSave(ProfessionalInfo professionalInfo,JSONObject json){
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
			try {
				call=conn.prepareCall("{call PKG_YL_INTERFACE.sp_addinvestorreq"+ "(?,?,?,?,?,?,?)}");
				call.setString(1, professionalInfo.getP_custno());
				call.setString(2, "1");
				call.setString(3, professionalInfo.getP_provetype());
				call.setDouble(4, Double.parseDouble(professionalInfo.getP_provebalance()));
				call.setString(5, "5");
				call.registerOutParameter(6, OracleTypes.VARCHAR);
				call.registerOutParameter(7, OracleTypes.VARCHAR);
				// 执行
				call.execute();
				
				String p_reasoncode = call.getObject(6).toString();//是否成功标识,0失败 1成功
				String p_reasoninfo = call.getObject(7).toString();//错误原因
				//System.out.println(p_reasoncode + p_reasoninfo);
				if(null != p_reasoncode && !"".equals(p_reasoncode)) {
					if("1".equals(p_reasoncode) && (null != p_reasoninfo && !"".equals(p_reasoninfo))) {
						Log.getLogger().info("认证申请已提交成功");
						String l_serialno = p_reasoninfo;//pkId
						Log.getLogger().info("认证申请已提交成功，返回编号l_serialno");
						String tempUrl = null;
						String url1 = null;
						List<EsbmmCode> listCode = esbmmCodeService.getCrmUploadFile();
						for (EsbmmCode esbmmCode : listCode) {
							if(esbmmCode.getCodeName().equals("tempPath")) {
								tempUrl = esbmmCode.getCodeValue();//临时上传目录
								Log.getLogger().info("获取服务器临时存取目录"+tempUrl);
							}
							if(esbmmCode.getCodeName().equals("crmUploadPath")) {
								url1 =esbmmCode.getCodeValue();//CRM上传目录
								Log.getLogger().info("获取CRM文件上传目录"+tempUrl);
							}
						}
						List<File> formItems = new CRM_InvestorAuthenticationUntil().fileUpLoad(tempUrl,json);
						Log.getLogger().info("获取临时目录下的所有文件");
						//判断是否上传图片
						if(null != l_serialno && !"".equals(l_serialno) &&  formItems.size()>0) {
							Log.getLogger().info("App端已上传图片");
							//CRM文件存储对象
							FileUp fileUp = new FileUp();
							fileUp.setC_catalog("TINVESTOR_REQ");//专业投资者表名
							fileUp.setV_creator(professionalInfo.getFinancialId());//理财师ID
							//根据理财师的id查询理财师的部门
							String nodecode = reservationUtil.getNodecode(professionalInfo.getFinancialId());
							fileUp.setV_nodecode(nodecode);//部门
							fileUp.setV_serialno(Integer.parseInt(l_serialno));//返回的编号
							
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							String date = sdf.format(new Date());
							String url = url1+date;//当天日期的目录名
							SmbFile file = null;
							try {
								file = new SmbFile(url);
								Log.getLogger().info("判断是否存在当天目录");
								if(!file.exists() && !file.isDirectory()){       
								    System.out.println("当天目录不存在，正在创建");  
								    file.mkdir(); 
								}
								Log.getLogger().info("当天目录已存在"); 
								SmbFile file1 = new SmbFile(url+"/Tblobstorage");
							    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
							    if(!file1.exists() && !file1.isDirectory()) {
							    	Log.getLogger().info("Tblobstorage目录不存在，正在创建"); 
							   	 file1.mkdir(); 
							    }
							} catch (Exception e) {
								Log.getLogger().info("判断共享文件地址是否正确连接");
								// TODO Auto-generated catch block
								state = "01";
								msg = "服务器连接失败！";
								e.printStackTrace();
								return state+"-"+msg;
							}
							//遍历所有文件
							for (int i =0;i<formItems.size();i++) {
								Log.getLogger().info("循环遍历所有文件");
								//本地数据库文件对象
								FileUploads fileUploads = new FileUploads();
								fileUploads.setPkId(UUID.randomUUID().toString());//PkId
								//附件名称 
								String p_filename =  formItems.get(i).getName();;
								fileUploads.setOldFileName(p_filename);//附件原来的名字
								//文件大小
								long p_filesize = formItems.get(i).length();;//附件大小
								fileUp.setP_filename(p_filename);
								fileUp.setP_filesize(p_filesize);
								Log.getLogger().info("开始调用CRM保存文件信息存储过程");
								String l_storageid = reservationUtil.saveFile(fileUp);//调用CRM保存文件信息存储过程
								Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
								String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
								Log.getLogger().info("Tblobstorage目录存在");
							    String uploadUrl = url+"\\Tblobstorage\\"+fileNo+"\\";//4位取3位
							    //文件上传目录
							    SmbFile file2 = null;
								try {
									file2 = new SmbFile(uploadUrl);
									Log.getLogger().info("判断是否存在文件上传目录");
								     if(!file2.exists() && !file2.isDirectory()) {
								    	 Log.getLogger().info(fileNo+"目录不存在，正在创建");
								    	 file2.mkdir();
								    }
								} catch (Exception e) {
									Log.getLogger().info("判断共享文件地址是否正确连接");
									// TODO Auto-generated catch block
									state = "00";
									msg = "申请已提交，但图片服务器连接失败！";
									e.printStackTrace();
									return state+"-"+msg;
								}
							     Log.getLogger().info("开始调用文件上传加密方法");
						    	 String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
						    	 if(null != state2 && !"".equals(state2)) {
						    		 Log.getLogger().info("判断文件上传加密是否成功");
						    		 String fileUrl = state2.split(",")[0];
						    		 String newFileName = state2.split(",")[1];
						    		 fileUploads.setUploadUrl(fileUrl);
						    		 fileUploads.setNewFileName(newFileName);
						    		//根据文件名查询文件PkId---p_filename
						    		 String pkId = fileUploadDao.queryById(p_filename).getPkId();
						    		 if(null != pkId && !"".equals(pkId)) {
						    			 fileUploadDao.updateByPrimaryKey(fileUploads);
						    		 }
						    	 }else {
						    		Log.getLogger().info("判断共享文件地址是否正确连接");
									// TODO Auto-generated catch block
						    		state = "00";
									msg = "申请已提交，但图片服务器连接失败！";
									return state+"-"+msg;
						    	 }
							}
							state = "00";
							msg = "上传图片，已提交认证申请，等待系统审核";
							return state +"-"+ msg;
						}else {
							state = "00";
							msg = "未上传图片，已提交认证申请，等待系统审核";
							return state +"-"+ msg;
						}
					}else {
						if(null != p_reasoninfo && !"".equals(p_reasoninfo)) {
							state = "01";
							msg = p_reasoninfo;
							return state +"-"+ msg;
						}else {
							state = "01";
							msg = "系统异常，请联系管理员";
							return state +"-"+ msg;
						}
					}
					
				}
				if(call !=null){
					call.close();
				}
				if(conn !=null){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
	//是否可以进行专业投资者认证申请
	public Integer queryZyShCount(String p_custno) {
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		//客户存在待审核申请的数量
  		Integer count = 0;
  		try {
  			call = conn.prepareCall(" SELECT COUNT(*) FROM TINVESTOR_REQ WHERE C_STATUS = '1' AND C_CUSTNO = '" + p_custno+"'");
  			// 执行
  			ResultSet rs = call.executeQuery();
  			while (rs.next()) {
  				count = rs.getInt("COUNT(*)");
  			}
  			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
  		} catch (SQLException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		return count;
	}
}

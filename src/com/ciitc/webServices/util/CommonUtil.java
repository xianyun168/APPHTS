package com.ciitc.webServices.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

/*******************************************************************************
 * 模块名称:  工具类
 * 模块功能描述:  请求接口
 * 创 建 人: renqingqing
 * 日 期: 2015年9月27日 下午3:51:30
 * 修 改 人: 
 ******************************************************************************/
public class CommonUtil {
	/*************************************************************************
	 * 函数名: getUrlSession 
	 * 功能描述: 请求接口  
	 * 输入参数: 
	 * 返 回 值: url      请求url
	 * 		    xmlStr   发送的xml报文
	 * 			coding   编码格式
	 * 异 常： Exception
	 * 创 建 人: 任庆庆  
	 * 日 期: 2015年9月27日 下午14:38:04 
	 * 修 改 人: 
	 * 日 期:
	 *************************************************************************/
	public static String getUrlSession(String url, String xmlStr, String coding)
			throws Exception {
		StringBuffer buffer = new StringBuffer();
		InputStream inputStream = null;
		try {
			URL cxptURL = new URL(url);

			// 打开连接
			HttpURLConnection qsConnection = (HttpURLConnection) cxptURL
					.openConnection();

			// 设置请求属性
			qsConnection.setRequestMethod("POST");
			qsConnection.setDoOutput(true);
			qsConnection.setDoInput(true);
			qsConnection.setAllowUserInteraction(true);
			// 设置超时
			qsConnection.setConnectTimeout(60000);
			qsConnection.setReadTimeout(60000);
			OutputStream output = qsConnection.getOutputStream();
			output.write(xmlStr.getBytes(coding));
			// 发送请求数据
			output.flush();
			output.close();
			BufferedReader reader = null;
			String strMessage = "";
			// 接收返回数据
			inputStream = qsConnection.getInputStream();
			reader = new BufferedReader(new InputStreamReader(inputStream));
			while ((strMessage = reader.readLine()) != null) {
				buffer.append(strMessage);
			}
		} catch (MalformedURLException e) {
			System.out.println("url 参数错误");
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			System.out.println("coding 参数错误");
			e.printStackTrace();
		} catch (IOException e) {
			throw e;
		} 
		// 返回报文
		return buffer.toString();
	}
}

package com.ciitc.webServices.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author 杨研
 * @date 2016年10月21日
 */
public class StringUtils {

	public static boolean isNullOrEmpty(String value) {
		if (null != value && !"".equals(value)) {
			return true;
		} else {
			return false;
		}
	}
	public static boolean isNullOrEmptyOrBlank(String value) {
		if (null != value && !"".equals(value) && !"".equals(value.trim())) {
			return true;
		} else {
			return false;
		}
	}
	//判断字符串中是否包含中文
	public static boolean isContainChinese(String str){
		boolean flag=true;
		int count = 0;
		String regEx = "[\\u4e00-\\u9fa5]";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		while (m.find()){
			for (int i = 0; i <= m.groupCount(); i++) {
				count++;
			}
		}
		if(count==0){
			flag = false;
		}
		return flag;
	}
}

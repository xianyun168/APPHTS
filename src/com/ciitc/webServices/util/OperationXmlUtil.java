package com.ciitc.webServices.util;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import javax.xml.stream.XMLOutputFactory;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.InvalidXPathException;
import org.dom4j.Namespace;
import org.dom4j.Node;
import org.dom4j.QName;
import org.dom4j.Visitor;
import org.dom4j.XPath;

/*******************************************************************************
 * 模块名称: XML工具类 模块功能描述: XML拆解、组织报文 创 建 人: 任庆庆 日 期: 2015年9月27日 下午3:40:15 修 改 人:
 ******************************************************************************/
public class OperationXmlUtil {
	public static String MainBillToXML() {
		String reponseMessage = "";
		Document document = DocumentHelper.createDocument();

		// 添加文档根
		Element root = document.addElement("Packet");
		root.addAttribute("type", "REQUEST");
		root.addAttribute("version", "1.0");
		// 添加Head节点
		Element head = root.addElement("Head");
		// 添加Head的子节点
		head.addElement("RequestType").addText("111");
		head.addElement("User").addText("任庆庆");
		head.addElement("Password").addText("11223");
		head.addElement("AreaCode").addText("430000");
		// 添加Body节点
		Element body = root.addElement("Body");
		// 添加PacketInfo节点
		Element packetInfo = body.addElement("PacketInfo");
		// 添加PacketInfo的子节点
		packetInfo.addElement("PacketNo").addText("1");

		// 添加PacketInfo节点
		Element basePart = body.addElement("BasePart");
		basePart.addElement("QueryTime").addText("sss");
		basePart.addElement("AreaCodes").addText("1");

		reponseMessage = root.asXML();
		return reponseMessage;
	}

	public static void main(String[] args) {
		Document document = DocumentHelper.createDocument();
		// 添加文档根
		Element root = document.addElement("Packet");
		root.addAttribute("type", "REQUEST");
		root.addAttribute("version", "1.0");
		Element servicesRoot = root.addElement("Services");
		String xml = MainBillToXML();
		Document doc;
		try {
			doc = DocumentHelper.parseText(xml);
			// 获取根节点
			Element rootElt = doc.getRootElement();
			rootElt.setName("Service");
			Attribute type = rootElt.attribute("type");
			type.detach();
			Attribute version = rootElt.attribute("version");
			version.detach();
			servicesRoot.add(rootElt);
			System.out.println("servicesRoot   "+ servicesRoot.asXML());
			System.out.println("rootElt   "+rootElt.asXML());
			System.out.println("root   "+ root.asXML());
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}

package com.ciitc.webServices.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;

import com.ciitc.webServices.model.MenuInfo;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.MenuService;
import com.ciitc.webServices.service.RoleDetailService;
import com.ciitc.webServices.service.UserInfoService;

	 
public class FilefilterUtil implements  Filter {


	@Autowired(required = false)
    private RoleDetailService<RoleDetailInfo> roleDetailService;
	@Autowired(required = false)
	private MenuService<MenuInfo> menuService;
	

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	 @Override
	 public void doFilter(ServletRequest req, ServletResponse res,
	            FilterChain chain) throws IOException, ServletException {
			
	        HttpServletRequest request = (HttpServletRequest)req;
	        String url =request.getRequestURL().toString();
	        String param =request.getQueryString();
	        boolean menuPathFlag = false;
	        boolean autoFlag = false;
	       
	        UserInfo user = ResourceUtil.getSessionUserName();
	        if(user != null){
	        	if(param != null){
	        		if(url.contains("verification.do")){
		        		chain.doFilter(request, res);
		        	}else if(param.split("=").length>=2
			        		&&param.split("=")[1].contains("&")&&!"key".equals(param.split("=")[1].split("&")[0])){
	    					PrintWriter out = res.getWriter();
							StringBuilder builder = new StringBuilder();
							builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
							builder.append("window.top.location.href=\"");
							builder.append(request.getContextPath());
							builder.append("/\";</script>");
							out.print(builder.toString());
							out.flush(); 
							out.close();
							return;
		        	}else if(param.split("=").length>=2
		        		&&param.split("=")[1].contains("&")&&!"key".equals(param.split("=")[1].split("&")[0])){
    					PrintWriter out = res.getWriter();
						StringBuilder builder = new StringBuilder();
						builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
						builder.append("window.top.location.href=\"");
						builder.append(request.getContextPath());
						builder.append("/\";</script>");
						out.print(builder.toString());
						out.flush(); 
						out.close();
						return;
	        		}else if(param.split("=").length>=2
	        			&&!param.split("=")[1].contains("&")&&!"key".equals(param.split("=")[1])){
        				PrintWriter out = res.getWriter();
						StringBuilder builder = new StringBuilder();
						builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
						builder.append("window.top.location.href=\"");
						builder.append(request.getContextPath());
						builder.append("/\";</script>");
						out.print(builder.toString());
						out.flush(); 
						out.close();
						return;
	        		}else if(url.contains("loginController.do")){
	        			String authStr = param.split("=")[0];
	        			String strArray = "checkuser,login,main,logout,home,noAuth,modifyPassword,updatePassword,";
	        			String[] str = strArray.split(",");
						for(int i = 0;i < str.length;i++){
							 if(authStr.equals(str[i])){
								 chain.doFilter(request, res);
								 return;
							 }
						 }
						PrintWriter out = res.getWriter();
						StringBuilder builder = new StringBuilder();
						builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
						builder.append("window.top.location.href=\"");
						builder.append(request.getContextPath());
						builder.append("/\";</script>");
						out.print(builder.toString());
						out.flush(); 
						out.close();
						return;
		        	}else{
		        		if(param.split("=").length>=2){
		        			String authStr = param.split("=")[0];
			        		 List<RoleDetailInfo> roleDetailList = new ArrayList<RoleDetailInfo>();
			        		 List<MenuInfo> menuInfoList = new ArrayList<MenuInfo>();
			        		 try {
								//根据用户名查询菜单地址
								menuInfoList = menuService.getMenuName(user.getUserCode());
							flag:for(MenuInfo menuInfo:menuInfoList){
									String menuPath = menuInfo.getMenuPath().split(".do")[0];
									if(url.contains("operationController")){
										if("menuController".equals(menuPath)) {
											menuPathFlag = true;
						    				//根据菜单名和用户名查询有什么权限
						    				roleDetailList=roleDetailService.getDescription(user.getUserCode(),menuInfo.getMenuPath());
						    				
						    				//权限中是否开通此权限
						    				for(RoleDetailInfo roleDetailInfo:roleDetailList){
												if(roleDetailInfo!=null&&roleDetailInfo.getDescription()!=null&&!"".equals(roleDetailInfo.getDescription())){
													 String[] str = roleDetailInfo.getDescription().split(",");
													 String oper = authStr+"relate";
													 for(int i = 0;i < str.length;i++){
														 if(oper.equals(str[i])){
															 autoFlag = true;
															 break flag;
														 }
													 }
												 }
												 if(roleDetailInfo!=null&&roleDetailInfo.getRemark()!=null&&!"".equals(roleDetailInfo.getRemark())){
													 String[] str = roleDetailInfo.getRemark().split(",");
													 String oper = authStr+"relate";
													 for(int i = 0;i < str.length;i++){
														 if(oper.equals(str[i])){
															 autoFlag = true;
															 break flag;
														 }
													 }
												 }
											}
						    				break flag;
										}
									}else if(url.contains("esbmmCodeController")){
										if("esbmmCodeTypeController".equals(menuPath)) {
											menuPathFlag = true;
						    				//根据菜单名和用户名查询有什么权限
						    				roleDetailList=roleDetailService.getDescription(user.getUserCode(),menuInfo.getMenuPath());
						    				//权限中是否开通此权限
						    				for(RoleDetailInfo roleDetailInfo:roleDetailList){
												if(roleDetailInfo!=null&&roleDetailInfo.getDescription()!=null&&!"".equals(roleDetailInfo.getDescription())){
													 String[] str = roleDetailInfo.getDescription().split(",");
													 String oper = authStr+"relate";
													 for(int i = 0;i < str.length;i++){
														 if(oper.equals(str[i])){
															 autoFlag = true;
															 break flag;
														 }
													 }
												 }
												 if(roleDetailInfo!=null&&roleDetailInfo.getRemark()!=null&&!"".equals(roleDetailInfo.getRemark())){
													 String[] str = roleDetailInfo.getRemark().split(",");
													 String oper = authStr+"relate";
													 for(int i = 0;i < str.length;i++){
														 if(oper.equals(str[i])){
															 autoFlag = true;
															 break flag;
														 }
													 }
												 }
											}
						    				break flag;
										}
									}else if(url.contains("mailTemplateInfoController")){
										if("mailInfoController".equals(menuPath)) {
											menuPathFlag = true;
											//根据菜单名和用户名查询有什么权限
						    				roleDetailList=roleDetailService.getDescription(user.getUserCode(),menuInfo.getMenuPath());
						    				//权限中是否开通此权限
						    				for(RoleDetailInfo roleDetailInfo:roleDetailList){
												if(roleDetailInfo!=null&&roleDetailInfo.getDescription()!=null&&!"".equals(roleDetailInfo.getDescription())){
													 String[] str = roleDetailInfo.getDescription().split(",");
													 String oper = authStr+"relate";
													 for(int i = 0;i < str.length;i++){
														 if(oper.equals(str[i])){
															 autoFlag = true;
															 break flag;
														 }
													 }
												 }
												 if(roleDetailInfo!=null&&roleDetailInfo.getRemark()!=null&&!"".equals(roleDetailInfo.getRemark())){
													 String[] str = roleDetailInfo.getRemark().split(",");
													 String oper = authStr+"relate";
													 for(int i = 0;i < str.length;i++){
														 if(oper.equals(str[i])){
															 autoFlag = true;
															 break flag;
														 }
													 }
												 }
											}
						    				break flag;
										}
									}else if(url.contains(menuPath)){
										 menuPathFlag = true;
					    				 //根据菜单名和用户名查询有什么权限
					    				 roleDetailList=roleDetailService.getDescription(user.getUserCode(),menuInfo.getMenuPath());
					    				 
					    				//权限中是否开通此权限
										 for(RoleDetailInfo roleDetailInfo:roleDetailList){
											 if(roleDetailInfo!=null&&roleDetailInfo.getDescription()!=null&&!"".equals(roleDetailInfo.getDescription())){
												 String[] str = roleDetailInfo.getDescription().split(",");
												 for(int i = 0;i < str.length;i++){
													 if(authStr.equals(str[i])){
														 autoFlag = true;
														 break flag;
													 }
												 }
											 }
											 if(roleDetailInfo!=null&&roleDetailInfo.getRemark()!=null&&!"".equals(roleDetailInfo.getRemark())){
												 String[] str = roleDetailInfo.getRemark().split(",");
												 for(int i = 0;i < str.length;i++){
													 if(authStr.equals(str[i])){
														 autoFlag = true;
														 break flag;
													 }
												 }
											 }
										 }
										
										 break flag;
									 }
									
								}
								if(autoFlag){
									chain.doFilter(request, res);
								}else{
									 PrintWriter out = res.getWriter();
									 StringBuilder builder = new StringBuilder();
									 builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
									 builder.append("window.top.location.href=\"");
									 builder.append(request.getContextPath());
									 builder.append("/webpage/common/noAuth.jsp\";</script>");
									 out.print(builder.toString());
									 out.flush(); 
									 out.close();
									 return;
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
		        		}else{
		        			PrintWriter out = res.getWriter();
							StringBuilder builder = new StringBuilder();
							builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
							builder.append("window.top.location.href=\"");
							builder.append(request.getContextPath());
							builder.append("/\";</script>");
							out.print(builder.toString());
							out.flush(); 
							out.close();
							return;
		        		}
		        		 
		        	}
	        	}else{
	        		if(!url.contains("/webpage/pages/login")&&!url.contains("/webpage/pages/main")&&!url.contains("/webpage/pages/ERROR")){
		        		PrintWriter out = res.getWriter();
						StringBuilder builder = new StringBuilder();
						builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
						builder.append("window.top.location.href=\"");
						builder.append(request.getContextPath());
						builder.append("/\";</script>");
						out.print(builder.toString());
						out.flush(); 
						out.close();
						return;
	        		}else{
	        			chain.doFilter(request, res);
	        		}
	        	}
	        }else{
	        	if(url.contains("verification.do")){
	        		chain.doFilter(request, res);
	        	}else if(url.contains("loginController.do")){
	        		chain.doFilter(request, res);
	        	}else{
	        		if(!url.contains("/webpage/pages/login")&&!url.contains("/webpage/pages/main")&&!url.contains("/webpage/pages/ERROR")){
		        		 
		        		PrintWriter out = res.getWriter();
						StringBuilder builder = new StringBuilder();
						builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
						builder.append("window.top.location.href=\"");
						builder.append(request.getContextPath());
						builder.append("/\";</script>");
						out.print(builder.toString());
						out.flush(); 
						out.close();
						return;
	        		}else{
	        			chain.doFilter(request, res);
	        		}
	        	}
	        }
	   }

	@SuppressWarnings("unchecked")
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		ServletContext context = arg0.getServletContext();  
	    ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);  
	    roleDetailService = (RoleDetailService<RoleDetailInfo>) ctx.getBean("roleDetailService"); 
	    menuService = (MenuService<MenuInfo>) ctx.getBean("menuService"); 
	}
	
}

package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CustomerInfoDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CustomerInfo;

public class CrmCustomerUtil {
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil
			.getBean("appUserInforDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private CustomerInfoDao customerInfoDao = (CustomerInfoDao) SpringContextUtil
			.getBean("customerInfoDao");
	public String queryCustomerInfo(String FinancialManagerID) {
		//清除本地库里所有数据
		customerInfoDao.deleteAll();
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		CallableStatement call1 = null;
		CallableStatement call2 = null;
		ResultSet rs =null;
		ResultSet rs1 =null;
		ResultSet rs2 =null;
		try {
			if (FinancialManagerID != null&&!"".equals(FinancialManagerID)) {
				call = conn.prepareCall("select * from vcustomerinfo");
				call1 =conn.prepareCall("select a.*, b.C_STATUS from vcustomerinfo a,TCUSTREGISTERREQ b where a.C_CUSTNO=b.C_CUSTNO");
				call2 =conn.prepareCall("select b.C_CUSTNO,SUM(b.F_BALANCE) as F_BALANCE from vcustomerinfo a,TRESERVE b where b.C_CAPITALSTATUS IN ('3','4') and a.C_CUSTNO =b.C_CUSTNO  GROUP BY b.C_CUSTNO");
			}
			// 执行
			rs = call.executeQuery();
			rs1 = call1.executeQuery();
			rs2 = call2.executeQuery();
			CustomerInfo customer =new CustomerInfo();
			while (rs.next()) {
				customer.setPkId(rs.getString("C_CUSTNO"));
				customer.setFinancialManagerId(rs.getString("C_CREATOR"));
				customer.setCustomerName(rs.getString("C_CUSTNAME"));
				customer.setCustomerSex(rs.getString("C_SEX"));
				//客户年龄
				customer.setCustomerAge(rs.getString(""));
				customer.setCustomerTelephone(rs.getString("C_MOBILENO"));
				customer.setCustomerBirthday(rs.getDate("C_BIRTHDAY"));
				customer.setCustomerAddress(rs.getString("C_ADDRESS"));
				//客户投资体量
				customer.setCustomerMoney(rs2.getString("F_BALANCE"));
				//从用户信息表根据客户编号获取客户等级
				AppUserInfor aa = appUserInforDao.selectByCustomerId(customer.getPkId());
				customer.setCustomerLevel(rs.getString(aa.getCustomerLevel()));
				//客户认证状态
				customer.setCustomerAs(rs1.getString("C_STATUS"));
				customer.setCustomerGredentials(rs.getString("C_IDENTITYTYPE"));
				customer.setCustomerGredentialsType(rs.getString("C_IDENTITYNO"));
				//证件照
				customer.setCustomerPhoto(rs.getString(""));
				//客户标签
				customer.setCustomerLabel(rs.getString(""));
				customerInfoDao.insert(customer);
				return "00";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if( null != rs1) {
					rs1.close();
					}
				if( null != rs2) {
					rs2.close();
					}
				if(null != call) {
					call.close();
				}
				if(null != call1) {
					call1.close();
				}
				if(null != call2) {
					call2.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return "01";
	}
}

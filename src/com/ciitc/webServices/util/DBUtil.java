package com.ciitc.webServices.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

/**
 * 数据库连接工具类
 */
public class DBUtil {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
    
	/**
     *  连接工厂
     */
    public static Connection makeConnection(){
    	@SuppressWarnings("unused")
		String crmdb = null;
    	@SuppressWarnings("unused")
		String username =null;
    	@SuppressWarnings("unused")
		String pwd = null;
        Connection conn = null;
        try {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace(); 
            }
            List<EsbmmCode> listCode = esbmmCodeService.connCRMscDb();
			for (EsbmmCode esbmmCode : listCode) {
				if(esbmmCode.getCodeName().equals("crmdb")) {
					crmdb = esbmmCode.getCodeValue();
				}
				if(esbmmCode.getCodeName().equals("username")) {
					username =esbmmCode.getCodeValue();
				}
				if(esbmmCode.getCodeName().equals("pwd")) {
					pwd =esbmmCode.getCodeValue();
				}
			}
            conn = DriverManager.getConnection("jdbc:oracle:thin:"+crmdb, username, pwd);
        } catch (SQLException e) {
            System.out.println("通过JDBC获取Oralce数据库链接对象出错!");
            e.printStackTrace();
        }
        return conn;
    }
   
    //收益表
    public static Connection makeOds(){
    	@SuppressWarnings("unused")
		String crmdb = null;
    	@SuppressWarnings("unused")
		String username =null;
    	@SuppressWarnings("unused")
		String pwd = null;
        Connection conn = null;
        try {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace(); 
            }
            List<EsbmmCode> listCode = esbmmCodeService.connCRMsyDb();
			for (EsbmmCode esbmmCode : listCode) {
				if(esbmmCode.getCodeName().equals("crmdb")) {
					crmdb = esbmmCode.getCodeValue();
				}
				if(esbmmCode.getCodeName().equals("username")) {
					username =esbmmCode.getCodeValue();
				}
				if(esbmmCode.getCodeName().equals("pwd")) {
					pwd =esbmmCode.getCodeValue();
				}
			}
			conn = DriverManager.getConnection("jdbc:oracle:thin:"+crmdb, username, pwd);
        } catch (SQLException e) {
            System.out.println("通过JDBC获取Oralce数据库链接对象出错!");
            e.printStackTrace();
        }
        return conn;
    }
    //本地表
    public static Connection makeKfCrm(){
    	@SuppressWarnings("unused")
		String crmdb = null;
    	@SuppressWarnings("unused")
		String username =null;
    	@SuppressWarnings("unused")
		String pwd = null;
        Connection conn = null;
        try {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace(); 
            }
            List<EsbmmCode> listCode = esbmmCodeService.connKfCrm();
			for (EsbmmCode esbmmCode : listCode) {
				if(esbmmCode.getCodeName().equals("crmdb")) {
					crmdb = esbmmCode.getCodeValue();
				}
				if(esbmmCode.getCodeName().equals("username")) {
					username =esbmmCode.getCodeValue();
				}
				if(esbmmCode.getCodeName().equals("pwd")) {
					pwd =esbmmCode.getCodeValue();
				}
			}
			conn = DriverManager.getConnection("jdbc:oracle:thin:"+crmdb, username, pwd);
        } catch (SQLException e) {
            System.out.println("通过JDBC获取Oralce数据库链接对象出错!");
            e.printStackTrace();
        }
        return conn;
    }
    
    
    
    /**
     *  连接测试
     */
    /*public static void  main(String args[]){
    	ApplicationContext context = new ClassPathXmlApplicationContext("spring-common.xml");
    	EsbmmCodeService<EsbmmCode> esbmmCodeService = (EsbmmCodeService) context.getBean("esbmmCodeService");
    	
    	System.out.println(esbmmCodeService);
    	DBUtil BUtil =new DBUtil();
    	BUtil.makeKfCrm();
        if(BUtil.makeConnection()==null){
            System.out.println("获取数据库链接失败！");
        }
        System.out.println("JDBC获取Oracle数据库链接成功！");
    }*/
}

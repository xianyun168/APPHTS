package com.ciitc.webServices.util;

import java.io.File;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.FileUploadDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTtrustcontractReq;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.FileUp;
import com.ciitc.webServices.model.FileUploads;
import com.ciitc.webServices.model.Integration;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import jcifs.smb.SmbFile;
import net.sf.json.JSONObject;
import oracle.jdbc.internal.OracleTypes;

public class CRM_CustomInfoUntil {
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao =  (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	private FileUploadDao fileUploadDao = (FileUploadDao) SpringContextUtil.getBean("fileUploadDao");
	
	
	//新增客户方法
	public String addCustomInfo(JSONObject json) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		try {
			String birth = json.getString("customerBirthday").replace("-", "");
			call=conn.prepareCall("{call PKG_YL_INTERFACE.sp_addtunregister"+
					"(?,?,?,?,?,?,?,?)}");
			call.setString(1, json.getString("p_custname"));//客户姓名
			call.setString(2, json.getString("financialId"));//理财师ID
			call.setString(3,json.getString("p_sex"));//性别
			call.setString(4,birth);//生日
			call.setString(5,json.getString("p_mobileno"));//手机号
			call.setString(6,json.getString("p_address"));//地址
			call.registerOutParameter(7, OracleTypes.VARCHAR);
			call.registerOutParameter(8, OracleTypes.VARCHAR);
			// 执行
			call.execute();
			String returncode = call.getObject(7).toString();//是否成功标识,0失败 1成功
			String returninfo = call.getObject(8).toString();//错误原因
			if((null != returncode && "1".equals(returncode)) && (null != returninfo && !"".equals(returninfo))) {
				return returninfo;
			}else {
				return returncode;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	//根据客户编号查询客户信息
	public AppUserInfor getCustByNo(String c_custno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		AppUserInfor appUserInfor = new AppUserInfor();
		ResultSet rs = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		try {
  			call = conn.prepareCall(" SELECT * FROM tunregister WHERE C_CUSTNO = '" + c_custno+"'");
  			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				appUserInfor.setCreateDate(rs.getDate("D_CREATEDATE"));//创建日期
  				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));//客户编号
  				appUserInfor.setCustomerSex(rs.getString("C_SEX"));//客户性别
  				appUserInfor.setUserName(rs.getString("C_CUSTNAME"));//客户名称
  				appUserInfor.setCustomerAddress(rs.getString("C_ADDRESS"));//客户地址
  				appUserInfor.setPhoneNumber(rs.getString("C_MOBILENO"));//客户手机号
  				appUserInfor.setIdcardType(rs.getString("C_IDENTITYTYPE"));//证件类型
  				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO"));//证件号码
  				appUserInfor.setcIsDelete(rs.getString("C_ISDELETE"));
  				String birthday = rs.getString("C_BIRTHDAY");
  				try {
  					if(null != birthday && !"".equals(birthday)) {
  						appUserInfor.setCustomerBirthday(sdf.parse(birthday));
  					}
					
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}//证件号码
  				//appUserInfor.setRegion(rs.getString("C_MARKETREGION"));//服务分支机构
  			}
  		} catch (SQLException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return appUserInfor;
	}
	
	//根据客户编号查询客户信息
	public AppUserInfor getCustByNoa(String c_custno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		AppUserInfor appUserInfor = new AppUserInfor();
		ResultSet rs = null;
		try {
  			call = conn.prepareCall(" SELECT * FROM tunregister WHERE C_CUSTNO = '" + c_custno+"'");
  			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				appUserInfor.setCreateDate(rs.getDate("D_CREATEDATE"));//创建日期
  				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));//客户编号
  				appUserInfor.setCustomerSex(rs.getString("C_SEX"));//客户性别
  				appUserInfor.setUserName(rs.getString("C_CUSTNAME"));//客户名称
  				appUserInfor.setCustomerAddress(rs.getString("C_ADDRESS"));//客户地址
  				appUserInfor.setPhoneNumber(rs.getString("C_MOBILENO"));//客户手机号
  				appUserInfor.setcIsDelete(rs.getString("C_ISDELETE"));//客户是否被删除
  				//appUserInfor.setRegion(rs.getString("C_MARKETREGION"));//服务分支机构
  				//客户证件类型
  				appUserInfor.setIdcardType(rs.getString("C_IDENTITYTYPE"));
  				//证件号码
  				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO"));
  				//客户生日
  				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
  				String c_birthday = rs.getString("C_BIRTHDAY");
  				if(null != c_birthday && !"".equals(c_birthday)) {
  					appUserInfor.setCustomerBirthday(sdf.parse(c_birthday));
  				}
  				
  			}
  		} catch (SQLException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return appUserInfor;
	}
	
	//修改客户信息前查询客户是否有待审核申请
  	public Integer updateCust(String c_custno) {
  	    // 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
  		//客户存在待审核申请的数量
  		Integer p_cnt = 0;
  		ResultSet rs = null;
  		try {
  			call = conn.prepareCall(" SELECT COUNT(*) FROM TCUSTMODIFYAUDIT WHERE C_STATUS = '0' AND C_CUSTNO = '" + c_custno+"'");
  			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				p_cnt = rs.getInt("COUNT(*)");
  			}
  		} catch (SQLException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}finally {
  			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
  			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
  		return p_cnt;
  	}
	
	//客户信息修改方法
	public String updateCustomInfo(JSONObject json) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String returncode = null;
		String returninfo = null;
		try {
			call=conn.prepareCall("{call PKG_YL_INTERFACE.sp_updatecustomerinfo"+
					"(?,?,?,?,?,?,?)}");
			call.setString(1, json.getString("p_custno"));//客户编号
			call.setString(2, json.getString("p_custname"));//客户名称
			call.setString(3, json.getString("p_sex"));//性别
			call.setString(4, json.getString("p_mobileno"));//手机号
			call.setString(5, json.getString("p_address"));//地址
			call.registerOutParameter(6, OracleTypes.VARCHAR);
			call.registerOutParameter(7, OracleTypes.VARCHAR);			
			// 执行
			call.execute();
			returncode = call.getObject(6).toString();//是否成功标识,0失败 1成功
			returninfo = call.getObject(7).toString();//结果描述
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return returncode;
	}
	
	//根据审核状态和客户编号提取客户审核唯一标识（审核编号）
	/*public String updateShNo(String c_custno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String c_requestno = null;
		ResultSet rs = null;
		try {
			call = conn.prepareCall(" SELECT C_REQUESTNO FROM TCUSTMODIFYAUDIT WHERE C_STATUS = '0' AND C_CUSTNO = '" + c_custno+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				c_requestno = rs.getString("C_REQUESTNO");
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return c_requestno;
	}*/
	
	//根据审核编号查询客户的审核状态
	/*public String updateQueryState(String shNo) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String shState = null;
		ResultSet rs = null;
		try {
			call = conn.prepareCall(" SELECT C_STATUS FROM TCUSTMODIFYAUDIT WHERE  C_REQUESTNO = '" + shNo+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				shState = rs.getString("C_STATUS");
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return shState;
	}*/

	//根据理财师ID查询所有用户的编号
	/*public List<String> queryCustomInfoList(String financialId) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		List<String> list = new ArrayList<String>();
		ResultSet rs = null;
		try {
			call = conn.prepareCall(" SELECT C_CUSTNO FROM TFUNDBROKERRELATION WHERE  C_BROKERACCOUNT = '" + financialId+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				String custNo = rs.getString("C_CUSTNO");
  				list.add(custNo);
  			}
  			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// TODO Auto-generated method stub
		return list;
	}*/
	
	//根据用户编号查询用户信息
	public AppUserInfor queryCustomInfo(String custNo,String financialId) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		AppUserInfor appUserInfor =  new AppUserInfor();
		KfcrmCustor kfcrmCustor = new KfcrmCustor();
		ResultSet rs = null;
		try {
			call = conn.prepareCall(" SELECT * FROM TUNREGISTER WHERE  C_CUSTNO = '" + custNo+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));//客户编号
  				appUserInfor.setPhoneNumber(rs.getString("C_MOBILENO"));//手机号
  				appUserInfor.setIdcardType(rs.getString("C_IDENTITYTYPE")); //证件类型
  				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO")); //证件号码
  				appUserInfor.setCreateDate(rs.getDate("D_CREATEDATE")); ;//创建时间
  				appUserInfor.setUserName(rs.getString("C_CUSTNAME")); //客户名称
  				appUserInfor.setCustomerSex(rs.getString("C_SEX")); //性别
  				appUserInfor.setcIsDelete(rs.getString("C_ISDELETE"));//客户是否被删除
  				//appUserInfor.setRegion(rs.getString("C_MARKETREGION")); //服务分支机构---
  				appUserInfor.setFinancialId(financialId);//理财师ID
  				String nameStatus = kfcrmCustor.getStateBycustNo(custNo);
  				if(null != nameStatus && "已认证".equals(nameStatus)) {
  					appUserInfor.setNameStatus("1");
  				}else if(null != nameStatus && "待审核".equals(nameStatus)){
  					appUserInfor.setNameStatus("2");
  				}else if(null != nameStatus && "已驳回".equals(nameStatus)){
  					appUserInfor.setNameStatus("3");
  				}else {
  					appUserInfor.setNameStatus("0");
  				}
  				String  birthday = rs.getString("C_BIRTHDAY");//生日
  				if(null != birthday && !"".equals(birthday)) {
  					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
  					Date customerBirthday = null;
  					try {
  						customerBirthday = sdf.parse(birthday);
  					} catch (ParseException e) {
  						// TODO Auto-generated catch block
  						e.printStackTrace();
  					}
  	  				appUserInfor.setCustomerBirthday(customerBirthday);
  				}
  				appUserInfor.setCustomerAddress(rs.getString("C_ADDRESS")); //地址
  				return appUserInfor;
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// TODO Auto-generated method stub
		return null;
	}
	
	//查询理财师
	public String brokeraccount1(String custNo) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String financialId = null;
		ResultSet rs = null;
		try {
			call = conn.prepareCall("select C_BROKERACCOUNT from tfundbrokerrelation where C_CUSTNO= '" + custNo+"'");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				financialId = rs.getString("C_BROKERACCOUNT");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return financialId;
	}

	//根据客户的编号查询客户的合同
	public List<CPTtrustcontractReq> queryByCustNo (String custNo){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		List<CPTtrustcontractReq> trustcontractList = new ArrayList<CPTtrustcontractReq>();
		try {
			call = conn.prepareCall("select * from ttrustcontract_req where C_CUSTNO = '" + custNo+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				CPTtrustcontractReq CPTtrustcontractReq = new CPTtrustcontractReq();
  				CPTtrustcontractReq.setcFundcode(rs.getString("C_FUNDCODE"));
  				CPTtrustcontractReq.setfContractsignbalance(rs.getDouble("F_CONTRACTSIGNBALANCE"));
  				trustcontractList.add(CPTtrustcontractReq);
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return trustcontractList;
	}
	
	//根据客户的编号查询客户的合同
	public List<Integration> queryByCustNo1 (String custNo){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		List<Integration> trustcontractList = new ArrayList<Integration>();
		try {
			call = conn.prepareCall(" select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE " + 
					" from ttrustcontract_req q ,tfundinfo o where  q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS='2' and q.D_FUNDOPENDATE is not null and  C_CUSTNO = '" + custNo+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				Integration integration = new Integration();
				integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
				integration.setCustno(rs.getString("C_CUSTNO"));
				integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
				integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
				integration.setProtuctName("C_FUNDNAME");
				if(null !=rs.getString("D_FUNDOPENDATE") ) {
					Date aa = rs.getDate("D_CONTRACTENDDATE");
					long a =  aa.getTime();
					Date bb = rs.getDate("D_FUNDOPENDATE");
					long b =  bb.getTime();
					long day = ((a-b)/(24*60*60*1000));
					integration.setPositionDate((int)day);
				}
  				trustcontractList.add(integration);
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return trustcontractList;
	}
	
	//根据客户的编号查询客户的合同
	public List<Integration> queryByCustNo2 (String custNo,Date yesterDay){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		List<Integration> trustcontractList = new ArrayList<Integration>();
		try {
			call = conn.prepareCall(" select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE " + 
					" from ttrustcontract_req q ,tfundinfo o where  q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS='2' and q.D_FUNDOPENDATE is not null and  C_CUSTNO = '" + custNo+"'"
							+ " and '"+yesterDay+"' > q.D_CREATEDATE  ");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				Integration integration = new Integration();
				integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
				integration.setCustno(rs.getString("C_CUSTNO"));
				integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
				integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
				integration.setProtuctName("C_FUNDNAME");
				if(null !=rs.getString("D_FUNDOPENDATE") ) {
					Date aa = rs.getDate("D_CONTRACTENDDATE");
					long a =  aa.getTime();
					Date bb = rs.getDate("D_FUNDOPENDATE");
					long b =  bb.getTime();
					long day = (a-b)/(24*60*60*1000);
					integration.setPositionDate((int)day);
				}
  				trustcontractList.add(integration);
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return trustcontractList;
	}
	//查询一段时间内的有效合同
	public List<Integration> queryByCustNo3 (String custNo,Date yesterDay,Date date){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		List<Integration> trustcontractList = new ArrayList<Integration>();
		try {
			call = conn.prepareCall(" select  o.C_FUNDNAME ,o.D_CONTRACTENDDATE,q.C_CUSTNO,q.C_TRUSTCONTRACTID,q.F_CONTRACTSIGNBALANCE,q.D_FUNDOPENDATE " + 
					" from ttrustcontract_req q ,tfundinfo o where  q.C_FUNDCODE = o.C_FUNDCODE and q.C_STATUS='2' and q.D_FUNDOPENDATE is not null and  C_CUSTNO = '" + custNo+"'"
							+ " and "+yesterDay+" >= q.D_AUDITDATE and  q.D_AUDITDATE >= "+date );
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				Integration integration = new Integration();
				integration.setContractsignDate(rs.getDate("D_FUNDOPENDATE"));
				integration.setCustno(rs.getString("C_CUSTNO"));
				integration.setMoney(rs.getInt("F_CONTRACTSIGNBALANCE"));
				integration.setReserveno(rs.getString("C_TRUSTCONTRACTID"));
				integration.setProtuctName("C_FUNDNAME");
				if(null !=rs.getString("D_FUNDOPENDATE") ) {
					Date aa = rs.getDate("D_CONTRACTENDDATE");
					long a =  aa.getTime();
					Date bb = rs.getDate("D_FUNDOPENDATE");
					long b =  bb.getTime();
					long day = (a-b)/(24*60*60*1000);
					integration.setPositionDate((int)day);
				}
  				trustcontractList.add(integration);
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return trustcontractList;
	}
	
	//根据产品的编号查询产品的到期日期，判断产品是否已到期
	public String queryByFundcode (String fundcode) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		String state = null;
		ResultSet rs = null;
		try {
			call = conn.prepareCall("select C_FUNDCODE, D_CONTRACTENDDATE from tfundinfo where C_FUNDCODE = '" + fundcode+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				Date endDate = rs.getDate("D_CONTRACTENDDATE");
  				Date day = new Date();
  				if(day.getTime() < endDate.getTime()) {
  					state = "0";
  				}else {
  					state = "1";
  				}
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return state;
	}
	
	//根据客户的编号查询持仓峰值
	public Double getPeakByCustNo(String custNo) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		Double balance  = 0.0;
		try {
			call = conn.prepareCall("select cvp.c_custno,max(cvp.f_contractsignbalance) from cust_view_position cvp where  cvp.c_custno ='"+custNo+"'   group by cvp.c_custno");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				balance = rs.getDouble("MAX(CVP.F_CONTRACTSIGNBALANCE)");
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return balance;
	}
	
	//客户实名认证接口
	public String customInfoReister(JSONObject json,HttpServletRequest request) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		String state = null;
		String msg = null;
		String p_reasoncode = null;
		try {
			call=conn.prepareCall("{call PKG_YL_INTERFACE.sp_tcustregisterreq"+
					"(?,?,?,?,?,?,?,?,?,?,?)}");
			call.setString(1, json.getString("p_custno"));//客户编号
			call.setString(2, "5");//系统来源--App
			call.setString(3, "156");//国家--默认156
			call.setString(4, json.getString("p_identitytype"));//证件类型
			call.setString(5, json.getString("p_identityno"));//证件号码
			call.setString(6, json.getString("p_email"));//邮箱
			call.setString(7, json.getString("p_MOBILENO"));//手机号
			call.setString(8, json.getString("p_address"));//地址
			
			call.registerOutParameter(9, OracleTypes.VARCHAR);
			call.registerOutParameter(10, OracleTypes.VARCHAR);	
			call.registerOutParameter(11, OracleTypes.VARCHAR);	
			// 执行
			call.execute();
			String p_reasoninfo1 =null;
			p_reasoncode = call.getObject(9).toString();//是否成功标识,0失败 1成功
			String p_reasoninfo = call.getObject(10).toString();//结果描述//文件加密的序列值
			if(!"0".equals(p_reasoncode)) {
				p_reasoninfo1 = call.getObject(11).toString();//结果描述1
			}
			//查询数据字典
			String idtype = null;
			String pkid = esbmmCodeTypeDao.queryByTypeName("IdcardType").get(0).getPkId();
            List<EsbmmCode>  codeList = esbmmCodeDao.queryByTypeIds(pkid);
            for (EsbmmCode esbmmCode : codeList) {
				if("IdcardName".equals(esbmmCode.getCodeName())) {
					idtype = esbmmCode.getCodeValue();
				}
			}
			if(null != p_reasoncode && !"".equals(p_reasoncode)) {
				if("1".equals(p_reasoncode) && (null != p_reasoninfo && !"".equals(p_reasoninfo))) {
					Log.getLogger().info("认证申请已提交成功");
					String l_serialno = p_reasoninfo;//文件加密的序列值
					Log.getLogger().info("认证申请已提交成功，返回编号l_serialno");
					String tempUrl = null;
					String url1 = null;
					List<EsbmmCode> listCode = esbmmCodeService.getCrmUploadFile();
					for (EsbmmCode esbmmCode : listCode) {
						if(esbmmCode.getCodeName().equals("tempPath")) {
							tempUrl = esbmmCode.getCodeValue();//临时上传目录
							Log.getLogger().info("获取服务器临时存取目录"+tempUrl);
						}
						if(esbmmCode.getCodeName().equals("crmUploadPath")) {
							url1 =esbmmCode.getCodeValue();//CRM上传目录
							Log.getLogger().info("获取CRM文件上传目录"+tempUrl);
						}
					}
					List<File> formItems = new CRM_InvestorAuthenticationUntil().fileUpLoad(tempUrl,json);
					Log.getLogger().info("获取临时目录下的所有文件");
					//判断证件类型
					if(idtype.equals(json.getString("p_identitytype"))) {
						//判断是否上传图片
						if(null != l_serialno && !"".equals(l_serialno) && formItems.size()>0) {
							Log.getLogger().info("App端已上传图片");
							//CRM文件存储对象
							FileUp fileUp = new FileUp();
							fileUp.setV_creator(json.getString("financialId"));//理财师ID
							//根据理财师的id查询理财师的部门
							String nodecode = reservationUtil.getNodecode(json.getString("financialId"));
							fileUp.setV_nodecode(nodecode);//部门
							fileUp.setV_serialno(Integer.parseInt(p_reasoninfo1));//返回的编号
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							String date = sdf.format(new Date());
							String url = url1+date;//当天日期的目录名
							SmbFile file = null;
							try {
								file = new SmbFile(url);
								Log.getLogger().info("判断是否存在当天目录");
								if(!file.exists() && !file.isDirectory()){       
									Log.getLogger().info("当天目录不存在，正在创建");  
								    file.mkdir(); 
								}
								Log.getLogger().info("当天目录已存在"); 
								SmbFile file1 = new SmbFile(url+"/Tblobstorage");
							    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
							    if(!file1.exists() && !file1.isDirectory()) {
							    	Log.getLogger().info("Tblobstorage目录不存在，正在创建"); 
							   	 	file1.mkdir(); 
							    }
							} catch (Exception e) {
								Log.getLogger().info("判断共享文件地址是否正确连接");
								// TODO Auto-generated catch block
								deleteSmShCount(json.getString("p_custno"));
								state = "01";
								msg = "图片服务器连接失败！";
								e.printStackTrace();
								return state+"-"+msg; 
							}
							String[] arr = json.getString("fileName").split(",");
							//遍历所有文件
							for (int i =0;i<formItems.size();i++) {
								Log.getLogger().info("循环遍历所有文件");
								//本地数据库文件对象
								FileUploads fileUploads = new FileUploads();
								fileUploads.setPkId(UUID.randomUUID().toString());//PkId
								//附件名称 
								String p_filename = formItems.get(i).getName();
								fileUploads.setOldFileName(p_filename);//附件原来的名字
								//文件大小
								long p_filesize = formItems.get(i).length();//附件大小
								fileUp.setP_filename(p_filename);
								fileUp.setP_filesize(p_filesize);
								Log.getLogger().info("开始调用CRM保存文件信息存储过程");
								String l_storageid = null;
								if(arr[0].equals(formItems.get(i).getName())) {
									fileUp.setL_storageid(l_serialno);
									fileUp.setC_catalog("TCUSTREGISTERREQ_MAIN");
									l_storageid = reservationUtil.saveFile1(fileUp);//调用CRM保存文件信息存储过程
									
								}else {
									fileUp.setC_catalog("TCUSTREGISTERREQ");//实名认证表名
									l_storageid = reservationUtil.saveFile2(fileUp);//调用CRM保存文件信息存储过程
									
								}
								Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
								String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
								Log.getLogger().info("Tblobstorage目录存在");
							    String uploadUrl = url+"\\Tblobstorage\\"+fileNo+"\\";//4位取3位
							   //文件上传目录
							    SmbFile file2 = null;
								try {
									file2 = new SmbFile(uploadUrl);
									Log.getLogger().info("判断是否存在文件上传目录");
								     if(!file2.exists() && !file2.isDirectory()) {
								    	 Log.getLogger().info(fileNo+"目录不存在，正在创建");
								    	 file2.mkdir();
								    }
								} catch (Exception e) {
									Log.getLogger().info("判断共享文件地址是否正确连接");
									// TODO Auto-generated catch block
									deleteSmShCount(json.getString("p_custno"));
									state = "01";
									msg = "图片服务器连接失败！";
									e.printStackTrace();
									return state+"-"+msg; 
								}
							    Log.getLogger().info("开始调用文件上传加密方法");
						    	String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
						    	if(null != state2 && !"".equals(state2)) {
						    		Log.getLogger().info("判断文件上传加密是否成功");
						    		String fileUrl = state2.split(",")[0];
						    		String newFileName = state2.split(",")[1];
						    		fileUploads.setUploadUrl(fileUrl);
						    		fileUploads.setNewFileName(newFileName);
						    		//根据文件名查询文件PkId---p_filename
						    		String pkId = fileUploadDao.queryById(p_filename).getPkId();
						    		if(null != pkId && !"".equals(pkId)) {
						    			fileUploadDao.updateByPrimaryKey(fileUploads);
						    		}
						    	}else {
						    		state = "01";
									msg = "图片服务器连接失败！";
									return state+"-"+msg; 
						    	}
						    	
							}
							//判断证明类型是否是身份证
							String returnStatusChina = DoSmrz.smrz(json.getString("custnoName"),json.getString("p_identityno"),p_reasoninfo1);
							if("实名认证通过".equals(returnStatusChina)) {
								AppUserInfor AppUserInfor = appUserInforDao.selectByCustomerId(json.getString("p_custno"));
								if(null != AppUserInfor.getCustomerId() && !"".equals(AppUserInfor.getCustomerId())) {
									AppUserInfor.setNameStatus("1");
									/*AppUserInfor.setIdcardType(json.getString("p_identitytype"));
									AppUserInfor.setIdcardNumber(json.getString("p_identityno"));*/
									appUserInforDao.updateSmState(AppUserInfor);
									state = "00";
									msg = "认证成功";
								}else {
									state = "01";
									msg = "当前客户不存在";
								}
							}else {
								state = "01";
								msg = "认证已提交";
							}
							return state +"-"+ msg;
						}else {
							//判断证明类型是否是身份证
							String returnStatusChina = DoSmrz.smrz(json.getString("custnoName"),json.getString("p_identityno"),p_reasoninfo);
							if("实名认证通过".equals(returnStatusChina)) {
								AppUserInfor AppUserInfor = appUserInforDao.selectByCustomerId(json.getString("p_custno"));
								if(null != AppUserInfor.getCustomerId() && !"".equals(AppUserInfor.getCustomerId())) {
									AppUserInfor.setNameStatus("1");
									/*AppUserInfor.setIdcardType(json.getString("p_identitytype"));
									AppUserInfor.setIdcardNumber(json.getString("p_identityno"));*/
									appUserInforDao.updateSmState(AppUserInfor);
									state = "00";
									msg = "认证成功";
								}else {
									state = "01";
									msg = "当前客户不存在";
								}
							}else {
								state = "01";
								msg = "认证已提交";
							}
							return state +"-"+ msg;
						}
					}else {
						//判断是否上传图片
						if(null != l_serialno && !"".equals(l_serialno) && formItems.size()>0) {
							Log.getLogger().info("App端已上传图片");
							//CRM文件存储对象
							FileUp fileUp = new FileUp();
							fileUp.setV_creator(json.getString("financialId"));//理财师ID
							//根据理财师的id查询理财师的部门
							String nodecode = reservationUtil.getNodecode(json.getString("financialId"));
							fileUp.setV_nodecode(nodecode);//部门
							fileUp.setV_serialno(Integer.parseInt(p_reasoninfo1));//返回的编号
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							String date = sdf.format(new Date());
							String url = url1+date;//当天日期的目录名
							SmbFile file = null;
							try {
								file = new SmbFile(url);
								Log.getLogger().info("判断是否存在当天目录");
								if(!file.exists() && !file.isDirectory()){       
									Log.getLogger().info("当天目录不存在，正在创建");  
								    file.mkdir(); 
								}
								Log.getLogger().info("当天目录已存在"); 
								SmbFile file1 = new SmbFile(url+"/Tblobstorage");
							    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
							    if(!file1.exists() && !file1.isDirectory()) {
							    	Log.getLogger().info("Tblobstorage目录不存在，正在创建"); 
							   	 	file1.mkdir(); 
							    }
							} catch (Exception e) {
								Log.getLogger().info("判断共享文件地址是否正确连接");
								// TODO Auto-generated catch block
								deleteSmShCount(json.getString("p_custno"));
								state = "01";
								msg = "图片服务器连接失败！";
								e.printStackTrace();
								return state+"-"+msg; 
							}
							String[] arr = json.getString("fileName").split(",");
							//遍历所有文件
							for (int i =0;i<formItems.size();i++) {
								Log.getLogger().info("循环遍历所有文件");
								//本地数据库文件对象
								FileUploads fileUploads = new FileUploads();
								fileUploads.setPkId(UUID.randomUUID().toString());//PkId
								//附件名称 
								String p_filename = formItems.get(i).getName();
								fileUploads.setOldFileName(p_filename);//附件原来的名字
								//文件大小
								long p_filesize = formItems.get(i).length();//附件大小
								fileUp.setP_filename(p_filename);
								fileUp.setP_filesize(p_filesize);
								Log.getLogger().info("开始调用CRM保存文件信息存储过程");
								String l_storageid = null;
								if(arr[0].equals(formItems.get(i).getName())) {
									fileUp.setL_storageid(l_serialno);
									fileUp.setC_catalog("TCUSTREGISTERREQ_MAIN");
									l_storageid = reservationUtil.saveFile1(fileUp);//调用CRM保存文件信息存储过程
									
								}else {
									fileUp.setC_catalog("TCUSTREGISTERREQ");//实名认证表名
									l_storageid = reservationUtil.saveFile2(fileUp);//调用CRM保存文件信息存储过程
									
								}
								Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
								String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
								Log.getLogger().info("Tblobstorage目录存在");
							    String uploadUrl = url+"\\Tblobstorage\\"+fileNo+"\\";//4位取3位
							   //文件上传目录
							    SmbFile file2 = null;
								try {
									file2 = new SmbFile(uploadUrl);
									Log.getLogger().info("判断是否存在文件上传目录");
								     if(!file2.exists() && !file2.isDirectory()) {
								    	 Log.getLogger().info(fileNo+"目录不存在，正在创建");
								    	 file2.mkdir();
								    }
								} catch (Exception e) {
									Log.getLogger().info("判断共享文件地址是否正确连接");
									// TODO Auto-generated catch block
									deleteSmShCount(json.getString("p_custno"));
									state = "01";
									msg = "图片服务器连接失败！";
									e.printStackTrace();
									return state+"-"+msg; 
								}
							    Log.getLogger().info("开始调用文件上传加密方法");
						    	String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
						    	if(null != state2 && !"".equals(state2)) {
						    		Log.getLogger().info("判断文件上传加密是否成功");
						    		String fileUrl = state2.split(",")[0];
						    		String newFileName = state2.split(",")[1];
						    		fileUploads.setUploadUrl(fileUrl);
						    		fileUploads.setNewFileName(newFileName);
						    		//根据文件名查询文件PkId---p_filename
						    		String pkId = fileUploadDao.queryById(p_filename).getPkId();
						    		if(null != pkId && !"".equals(pkId)) {
						    			fileUploadDao.updateByPrimaryKey(fileUploads);
						    		}
						    	}else {
						    		state = "01";
									msg = "图片服务器连接失败！";
									return state+"-"+msg; 
						    	}
						    	
							}
							//更新本地表的实名认证状态
							AppUserInfor AppUserInfor = appUserInforDao.selectByCustomerId(json.getString("p_custno"));
							if(null != AppUserInfor.getCustomerId() && !"".equals(AppUserInfor.getCustomerId())) {
								AppUserInfor.setNameStatus("2");
								appUserInforDao.updateSmState(AppUserInfor);
								state = "00";
								msg = "上传图片，已提交认证申请，等待系统审核";
							}else {
								state = "01";
								msg = "当前客户不存在";
							}
							return state +"-"+ msg;
						}else {
							state = "01";
							msg = "未上传图片";
							return state +"-"+ msg;
						}
					}
				}else {
					if((null != p_reasoninfo && !"".equals(p_reasoninfo))) {
						state = "01";
						msg = p_reasoninfo;
						return state +"-"+ msg;
					}else {
						state = "01";
						msg = "系统异常，请联系管理员";
						return state +"-"+ msg;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	//是否可以进行实名认证申请
	public Integer querySmShCount(String p_custno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		//客户存在待审核申请的数量
  		Integer count = 0;
  		try {
  			call = conn.prepareCall(" SELECT COUNT(*) FROM TCUSTREGISTERREQ WHERE C_STATUS = '1' AND C_CUSTNO = '" + p_custno+"'");
  			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				count = rs.getInt("COUNT(*)");
  			}
  		} catch (SQLException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}finally {
  			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
  			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
  		return count;
	}
	
	//图片上传失败删除实名认证申请
	public void deleteSmShCount(String p_custno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
  		try {
  			call = conn.prepareCall(" delete from TCUSTREGISTERREQ WHERE C_STATUS = '1' AND C_CUSTNO = '" + p_custno+"'");
  			// 执行
  			call.executeUpdate();
  		} catch (SQLException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}finally {
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	
	//根据用户编号查询用户信息
	public String queryCustomInfo1(String custNo) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String userName = "";
		try {
			call = conn.prepareCall(" SELECT C_CUSTNAME FROM TUNREGISTER WHERE  C_CUSTNO = '" + custNo+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				userName = rs.getString("C_CUSTNAME"); //客户名称
  			}
  			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
  			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
  			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return userName;
	}
		
	//根据手机号查询客户
	public int queryByMobileno(String mobileno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		int num = 0;
		try {
			call = conn.prepareCall("  select count(*) from tunregister where C_MOBILENO  = '" + mobileno+"' and C_ISDELETE='0' ");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				num = rs.getInt("count(*)"); //客户名称
  			}
  			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
  			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
  			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return num;
	}
	//查询被驳回的实名认证信息
	public AppUserInfor queryByCustomId(String custonId) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		AppUserInfor appUserInfor = new AppUserInfor();
		try {
			call = conn.prepareCall(" select tc.L_SERIALNO,vc.* from TCUSTREGISTERREQ tc,tunregister vc where tc.C_CUSTNO = vc.C_CUSTNO and tc.C_STATUS = '3' and tc.C_CUSTNO = '" + custonId+"'");
			// 执行
  			rs = call.executeQuery();
  			while (rs.next()) {
  				appUserInfor.setCustomerId(rs.getString("C_CUSTNO")); //客户编号
  				appUserInfor.setUserName(rs.getString("C_CUSTNAME"));//客户名称
  				String identitytype = rs.getString("C_IDENTITYTYPE");
  				String identitytypes = null;
  				String pkid = esbmmCodeTypeDao.queryByTypeName("IdcardType").get(0).getPkId();
  	            List<EsbmmCode>  codeList = esbmmCodeDao.queryByTypeIds(pkid);
  	            for (EsbmmCode esbmmCode : codeList) {
  					if(identitytype.equals(esbmmCode.getCodeValue())) {
  						identitytypes = esbmmCode.getChineseName();
  					}
  				}
  				appUserInfor.setIdcardType(identitytype);//证件类型
  				appUserInfor.setRemark4(identitytypes);//证件类型中文
  				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO"));//证件号码
  				appUserInfor.setCustomerAddress(rs.getString("C_ADDRESS"));//所在地区
  				appUserInfor.setRemark2(rs.getString("C_EMAIL"));//邮箱
  				appUserInfor.setRemark3(rs.getString("L_SERIALNO"));//实名认证表主键
  				appUserInfor.setPhoneNumber(rs.getString("C_MOBILENO"));//客户电话
  				
  			}
  			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
  			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
  			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return appUserInfor;
	}
	//CRM再次实名认证
	public String customInfoReisterOne(JSONObject json,HttpServletRequest request) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		String state = null;
		String msg = null;
		String p_reasoncode = null;
		try {
			call=conn.prepareCall("{call PKG_YL_INTERFACE.sp_tcustregisterreq_update "+
					"(?,?,?,?,?,?,?,?,?,?,?,?)}");
			call.setString(1, json.getString("lserialno"));//客户序列号
			call.setString(2, json.getString("p_custno"));//客户编号
			call.setString(3, "5");//系统来源--App
			call.setString(4, "156");//国家--默认156
			call.setString(5, json.getString("p_identitytype"));//证件类型
			call.setString(6, json.getString("p_identityno"));//证件号码
			call.setString(7, json.getString("p_email"));//邮箱
			call.setString(8, json.getString("p_MOBILENO"));//手机号
			call.setString(9, json.getString("p_address"));//地址
			
			call.registerOutParameter(10, OracleTypes.VARCHAR);
			call.registerOutParameter(11, OracleTypes.VARCHAR);	
			call.registerOutParameter(12, OracleTypes.VARCHAR);	
			// 执行
			call.execute();
			String p_reasoninfo1 =null;
			p_reasoncode = call.getObject(10).toString();//是否成功标识,0失败 1成功
			String p_reasoninfo = call.getObject(11).toString();//结果描述
			if(!"0".equals(p_reasoncode)) {
				p_reasoninfo1 = call.getObject(12).toString();//结果描述1返回实名认证接口数据
			}
			//查询数据字典
			String idtype = null;
			String pkid = esbmmCodeTypeDao.queryByTypeName("IdcardType").get(0).getPkId();
            List<EsbmmCode>  codeList = esbmmCodeDao.queryByTypeIds(pkid);
            for (EsbmmCode esbmmCode : codeList) {
				if("IdcardName".equals(esbmmCode.getCodeName())) {
					idtype = esbmmCode.getCodeValue();
				}
			}
			if(null != p_reasoncode && !"".equals(p_reasoncode)) {
				if("1".equals(p_reasoncode) && (null != p_reasoninfo && !"".equals(p_reasoninfo))) {
					Log.getLogger().info("认证申请已提交成功");
					String l_serialno = p_reasoninfo;//文件加密的序列值
					Log.getLogger().info("认证申请已提交成功，返回编号l_serialno");
					String tempUrl = null;
					String url1 = null;
					List<EsbmmCode> listCode = esbmmCodeService.getCrmUploadFile();
					for (EsbmmCode esbmmCode : listCode) {
						if(esbmmCode.getCodeName().equals("tempPath")) {
							tempUrl = esbmmCode.getCodeValue();//临时上传目录
							Log.getLogger().info("获取服务器临时存取目录"+tempUrl);
						}
						if(esbmmCode.getCodeName().equals("crmUploadPath")) {
							url1 =esbmmCode.getCodeValue();//CRM上传目录
							Log.getLogger().info("获取CRM文件上传目录"+tempUrl);
						}
					}
					List<File> formItems = new CRM_InvestorAuthenticationUntil().fileUpLoad(tempUrl,json);
					Log.getLogger().info("获取临时目录下的所有文件");
					//判断证件类型
					if(idtype.equals(json.getString("p_identitytype"))) {
						//判断是否上传图片
						if(null != l_serialno && !"".equals(l_serialno) && formItems.size()>0) {
							Log.getLogger().info("App端已上传图片");
							//上传图片，先删除以前的图片
							deleteFJ(p_reasoninfo1);
							//CRM文件存储对象
							FileUp fileUp = new FileUp();
							fileUp.setV_creator(json.getString("financialId"));//理财师ID
							//根据理财师的id查询理财师的部门
							String nodecode = reservationUtil.getNodecode(json.getString("financialId"));
							fileUp.setV_nodecode(nodecode);//部门
							fileUp.setV_serialno(Integer.parseInt(p_reasoninfo1));//返回的编号
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							String date = sdf.format(new Date());
							String url = url1+date;//当天日期的目录名
							SmbFile file = null;
							try {
								file = new SmbFile(url);
								Log.getLogger().info("判断是否存在当天目录");
								if(!file.exists() && !file.isDirectory()){       
									Log.getLogger().info("当天目录不存在，正在创建");  
								    file.mkdir(); 
								}
								Log.getLogger().info("当天目录已存在"); 
								SmbFile file1 = new SmbFile(url+"/Tblobstorage");
							    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
							    if(!file1.exists() && !file1.isDirectory()) {
							    	Log.getLogger().info("Tblobstorage目录不存在，正在创建"); 
							   	 	file1.mkdir(); 
							    }
							} catch (Exception e) {
								Log.getLogger().info("判断共享文件地址是否正确连接");
								// TODO Auto-generated catch block
								deleteSmShCount(json.getString("p_custno"));
								state = "01";
								msg = "图片服务器连接失败！";
								e.printStackTrace();
								return state+"-"+msg; 
							}
							String[] arr = json.getString("fileName").split(",");
							//遍历所有文件
							for (int i =0;i<formItems.size();i++) {
								Log.getLogger().info("循环遍历所有文件");
								//本地数据库文件对象
								FileUploads fileUploads = new FileUploads();
								fileUploads.setPkId(UUID.randomUUID().toString());//PkId
								//附件名称 
								String p_filename = formItems.get(i).getName();
								fileUploads.setOldFileName(p_filename);//附件原来的名字
								//文件大小
								long p_filesize = formItems.get(i).length();//附件大小
								fileUp.setP_filename(p_filename);
								fileUp.setP_filesize(p_filesize);
								Log.getLogger().info("开始调用CRM保存文件信息存储过程");
								String l_storageid = null;
								/*if(arr[0].equals(formItems.get(i).getName())) {
									fileUp.setL_storageid(l_serialno);
									fileUp.setC_catalog("TCUSTREGISTERREQ_MAIN");
									l_storageid = reservationUtil.saveFile1(fileUp);//调用CRM保存文件信息存储过程
									
								}else {*/
								fileUp.setC_catalog("TCUSTREGISTERREQ");//实名认证表名
								l_storageid = reservationUtil.saveFile2(fileUp);//调用CRM保存文件信息存储过程
									
								/*}*/
								Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
								String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
								Log.getLogger().info("Tblobstorage目录存在");
							    String uploadUrl = url+"\\Tblobstorage\\"+fileNo+"\\";//4位取3位
							   //文件上传目录
							    SmbFile file2 = null;
								try {
									file2 = new SmbFile(uploadUrl);
									Log.getLogger().info("判断是否存在文件上传目录");
								     if(!file2.exists() && !file2.isDirectory()) {
								    	 Log.getLogger().info(fileNo+"目录不存在，正在创建");
								    	 file2.mkdir();
								    }
								} catch (Exception e) {
									Log.getLogger().info("判断共享文件地址是否正确连接");
									// TODO Auto-generated catch block
									deleteSmShCount(json.getString("p_custno"));
									state = "01";
									msg = "图片服务器连接失败！";
									e.printStackTrace();
									return state+"-"+msg; 
								}
							    Log.getLogger().info("开始调用文件上传加密方法");
						    	String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
						    	if(null != state2 && !"".equals(state2)) {
						    		Log.getLogger().info("判断文件上传加密是否成功");
						    		String fileUrl = state2.split(",")[0];
						    		String newFileName = state2.split(",")[1];
						    		fileUploads.setUploadUrl(fileUrl);
						    		fileUploads.setNewFileName(newFileName);
						    		//根据文件名查询文件PkId---p_filename
						    		String pkId = fileUploadDao.queryById(p_filename).getPkId();
						    		if(null != pkId && !"".equals(pkId)) {
						    			fileUploadDao.updateByPrimaryKey(fileUploads);
						    		}
						    	}else {
						    		state = "01";
									msg = "图片服务器连接失败！";
									return state+"-"+msg; 
						    	}
						    	
							}
							//判断证明类型是否是身份证
							String returnStatusChina = DoSmrz.smrz(json.getString("custnoName"),json.getString("p_identityno"),p_reasoninfo1);
							if("实名认证通过".equals(returnStatusChina)) {
								AppUserInfor AppUserInfor = appUserInforDao.selectByCustomerId(json.getString("p_custno"));
								if(null != AppUserInfor.getCustomerId() && !"".equals(AppUserInfor.getCustomerId())) {
									AppUserInfor.setNameStatus("1");
									appUserInforDao.updateSmState(AppUserInfor);
									state = "00";
									msg = "认证成功";
								}else {
									state = "01";
									msg = "当前客户不存在";
								}
							}else {
								state = "01";
								msg = "认证已提交";
							}
							return state +"-"+ msg;
						}else {
							//判断证明类型是否是身份证
							String returnStatusChina = DoSmrz.smrz(json.getString("custnoName"),json.getString("p_identityno"),p_reasoninfo);
							if("实名认证通过".equals(returnStatusChina)) {
								AppUserInfor AppUserInfor = appUserInforDao.selectByCustomerId(json.getString("p_custno"));
								if(null != AppUserInfor.getCustomerId() && !"".equals(AppUserInfor.getCustomerId())) {
									AppUserInfor.setNameStatus("1");
									appUserInforDao.updateSmState(AppUserInfor);
									state = "00";
									msg = "认证成功";
								}else {
									state = "01";
									msg = "当前客户不存在";
								}
							}else {
								state = "01";
								msg = "认证已提交";
							}
							return state +"-"+ msg;
						}
					}else {
						//判断是否上传图片
						if(null != l_serialno && !"".equals(l_serialno) && formItems.size()>0) {
							Log.getLogger().info("App端已上传图片");
							//上传图片，先删除以前的图片
							deleteFJ(p_reasoninfo1);
							//CRM文件存储对象
							FileUp fileUp = new FileUp();
							fileUp.setV_creator(json.getString("financialId"));//理财师ID
							//根据理财师的id查询理财师的部门
							String nodecode = reservationUtil.getNodecode(json.getString("financialId"));
							fileUp.setV_nodecode(nodecode);//部门
							fileUp.setV_serialno(Integer.parseInt(p_reasoninfo1));//返回的编号
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							String date = sdf.format(new Date());
							String url = url1+date;//当天日期的目录名
							SmbFile file = null;
							try {
								file = new SmbFile(url);
								Log.getLogger().info("判断是否存在当天目录");
								if(!file.exists() && !file.isDirectory()){       
									Log.getLogger().info("当天目录不存在，正在创建");  
								    file.mkdir(); 
								}
								Log.getLogger().info("当天目录已存在"); 
								SmbFile file1 = new SmbFile(url+"/Tblobstorage");
							    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
							    if(!file1.exists() && !file1.isDirectory()) {
							    	Log.getLogger().info("Tblobstorage目录不存在，正在创建"); 
							   	 	file1.mkdir(); 
							    }
							} catch (Exception e) {
								Log.getLogger().info("判断共享文件地址是否正确连接");
								// TODO Auto-generated catch block
								deleteSmShCount(json.getString("p_custno"));
								state = "01";
								msg = "图片服务器连接失败！";
								e.printStackTrace();
								return state+"-"+msg; 
							}
							String[] arr = json.getString("fileName").split(",");
							//遍历所有文件
							for (int i =0;i<formItems.size();i++) {
								Log.getLogger().info("循环遍历所有文件");
								//本地数据库文件对象
								FileUploads fileUploads = new FileUploads();
								fileUploads.setPkId(UUID.randomUUID().toString());//PkId
								//附件名称 
								String p_filename = formItems.get(i).getName();
								fileUploads.setOldFileName(p_filename);//附件原来的名字
								//文件大小
								long p_filesize = formItems.get(i).length();//附件大小
								fileUp.setP_filename(p_filename);
								fileUp.setP_filesize(p_filesize);
								Log.getLogger().info("开始调用CRM保存文件信息存储过程");
								String l_storageid = null;
								/*if(arr[0].equals(formItems.get(i).getName())) {
									fileUp.setL_storageid(l_serialno);
									fileUp.setC_catalog("TCUSTREGISTERREQ_MAIN");
									l_storageid = reservationUtil.saveFile1(fileUp);//调用CRM保存文件信息存储过程
									
								}else {*/
								fileUp.setC_catalog("TCUSTREGISTERREQ");//实名认证表名
								l_storageid = reservationUtil.saveFile2(fileUp);//调用CRM保存文件信息存储过程
									
								/*}*/
								Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
								String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
								Log.getLogger().info("Tblobstorage目录存在");
							    String uploadUrl = url+"\\Tblobstorage\\"+fileNo+"\\";//4位取3位
							   //文件上传目录
							    SmbFile file2 = null;
								try {
									file2 = new SmbFile(uploadUrl);
									Log.getLogger().info("判断是否存在文件上传目录");
								     if(!file2.exists() && !file2.isDirectory()) {
								    	 Log.getLogger().info(fileNo+"目录不存在，正在创建");
								    	 file2.mkdir();
								    }
								} catch (Exception e) {
									Log.getLogger().info("判断共享文件地址是否正确连接");
									// TODO Auto-generated catch block
									deleteSmShCount(json.getString("p_custno"));
									state = "01";
									msg = "图片服务器连接失败！";
									e.printStackTrace();
									return state+"-"+msg; 
								}
							    Log.getLogger().info("开始调用文件上传加密方法");
						    	String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
						    	if(null != state2 && !"".equals(state2)) {
						    		Log.getLogger().info("判断文件上传加密是否成功");
						    		String fileUrl = state2.split(",")[0];
						    		String newFileName = state2.split(",")[1];
						    		fileUploads.setUploadUrl(fileUrl);
						    		fileUploads.setNewFileName(newFileName);
						    		//根据文件名查询文件PkId---p_filename
						    		String pkId = fileUploadDao.queryById(p_filename).getPkId();
						    		if(null != pkId && !"".equals(pkId)) {
						    			fileUploadDao.updateByPrimaryKey(fileUploads);
						    		}
						    	}else {
						    		state = "01";
									msg = "图片服务器连接失败！";
									return state+"-"+msg; 
						    	}
						    	
							}
							//更新本地表的实名认证状态
							AppUserInfor AppUserInfor = appUserInforDao.selectByCustomerId(json.getString("p_custno"));
							if(null != AppUserInfor.getCustomerId() && !"".equals(AppUserInfor.getCustomerId())) {
								AppUserInfor.setNameStatus("2");
								appUserInforDao.updateSmState(AppUserInfor);
								state = "00";
								msg = "上传图片，已提交认证申请，等待系统审核";
							}else {
								state = "01";
								msg = "当前客户不存在";
							}
							return state +"-"+ msg;
						}else {
							state = "01";
							msg = "未上传图片";
							return state +"-"+ msg;
						}
					}
				}else {
					if((null != p_reasoninfo && !"".equals(p_reasoninfo))) {
						state = "01";
						msg = p_reasoninfo;
						return state +"-"+ msg;
					}else {
						state = "01";
						msg = "系统异常，请联系管理员";
						return state +"-"+ msg;
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	//再次实名认证删除附件信息
	public void deleteFJ(String p_reasoninfo1) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		try {
			call = conn.prepareCall(" DELETE FROM tblobstorage WHERE C_SUBKEYID = '" + p_reasoninfo1+"'");
			// 执行
			call.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if(call != null) {
					call.close();
				}
				if(conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}

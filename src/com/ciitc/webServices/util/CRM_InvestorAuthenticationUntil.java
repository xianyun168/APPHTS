package com.ciitc.webServices.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


import com.ciitc.webServices.dao.FileUploadDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.FileUp;
import com.ciitc.webServices.model.FileUploads;
import com.ciitc.webServices.model.InvestorAuthentication;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileOutputStream;
import net.sf.json.JSONObject;
import oracle.jdbc.internal.OracleTypes;


public class CRM_InvestorAuthenticationUntil {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	
	private FileUploadDao fileUploadDao = (FileUploadDao) SpringContextUtil.getBean("fileUploadDao");
	
		// 连接耀莱CRM数据库
//		Connection conn = DBUtil.makeConnection();
//		CallableStatement call = null;
		String state = null;
		String msg = null;
		//实名认证后----合格投资者认证方法
		public String investorAuthentication(InvestorAuthentication investorAuthentication,JSONObject json){
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
			try {
				call=conn.prepareCall("{call PKG_YL_INTERFACE.sp_addqualifyinvestorreq"+
						"(?,?,?,?,?,?,?,?,?,?,?)}");
				call.setString(1, investorAuthentication.getP_custno());//客户编号
				call.setString(2, investorAuthentication.getP_provetype());//证明类型(1资产证明 2收入证明)
				String provebalance = investorAuthentication.getP_provebalance();
				Double p_provebalance = Double.valueOf(provebalance);
				call.setDouble(3, p_provebalance);//证明金额
				call.setInt(4, 0);//附件组编号(外部系统)
				call.setInt(5, 0);//附件ID(外部系统)
				call.setString(6,null);//附件名称
				call.setString(7,null);//附件路径(绝对地址)
				call.setString(8,null);//文件大小
				call.setString(9,"5");//来源系统(0:多金 1:商城 5:APP)
				call.registerOutParameter(10, OracleTypes.VARCHAR);
				call.registerOutParameter(11, OracleTypes.VARCHAR);
				
				// 执行
				call.execute();
				String returncode = call.getObject(10).toString();//处理结果 0:失败 1:成功
				String returninfo = call.getObject(11).toString();//结果描述:前台提示内容，错误信息
				System.out.println(returncode + returninfo);
				//合格投资者是否有返回信息
				if(null != returncode && !"".equals(returncode)) {
					//如果是1，返回编号l_serialno
					if("1".equals(returncode) && (null != returninfo && !"".equals(returninfo))) {
						Log.getLogger().info("认证申请已提交成功");
						String l_serialno = returninfo;//pkId
						Log.getLogger().info("认证申请已提交成功，返回编号l_serialno");
						String tempUrl = null;
						String url1 = null;
						List<EsbmmCode> listCode = esbmmCodeService.getCrmUploadFile();
						for (EsbmmCode esbmmCode : listCode) {
							if(esbmmCode.getCodeName().equals("tempPath")) {
								tempUrl = esbmmCode.getCodeValue();//临时上传目录
								Log.getLogger().info("获取服务器临时存取目录"+tempUrl);
							}
							if(esbmmCode.getCodeName().equals("crmUploadPath")) {
								url1 =esbmmCode.getCodeValue();//CRM上传目录
								Log.getLogger().info("获取CRM文件上传目录"+url1);
							}
						}
						List<File> formItems = new CRM_InvestorAuthenticationUntil().fileUpLoad(tempUrl,json);
						Log.getLogger().info("获取临时目录下的所有文件");
						//判断是否上传图片
						if(null != l_serialno && !"".equals(l_serialno) && formItems.size()>0) {
							Log.getLogger().info("App端已上传图片");
							//CRM文件存储对象
							FileUp fileUp = new FileUp();
							fileUp.setC_catalog("TQUALIFYINVESTOR_REQ");//合格投资者表名
							fileUp.setV_creator(investorAuthentication.getFinancialId());//理财师ID
							//根据理财师的id查询理财师的部门
							String nodecode = reservationUtil.getNodecode(investorAuthentication.getFinancialId());
							fileUp.setV_nodecode(nodecode);//部门
							fileUp.setV_serialno(Integer.parseInt(l_serialno));//返回的编号
							
							SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
							String date = sdf.format(new Date());
							String url = url1+date;//当天日期的目录名
							SmbFile file = null;
							try {
								file = new SmbFile(url);
								Log.getLogger().info("判断是否存在当天目录");
								if(!file.exists() && !file.isDirectory()){       
									Log.getLogger().info("当天目录不存在，正在创建");  
								    file.mkdir(); 
								}
								Log.getLogger().info("当天目录已存在"); 
								SmbFile file1 = new SmbFile(url+"/Tblobstorage");
							    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
							    if(!file1.exists() && !file1.isDirectory()) {
							     Log.getLogger().info("Tblobstorage目录不存在，正在创建"); 
							   	 file1.mkdir(); 
							    }
							} catch (Exception e) {
								// TODO Auto-generated catch block
								state = "00";
								msg = "申请已提交，但图片服务器连接失败！";
								e.printStackTrace();
								return state+"-"+msg; 
							}
							//遍历所有文件
							for (int i =0;i<formItems.size();i++) {
								Log.getLogger().info("循环遍历所有文件");
								//本地数据库文件对象
								FileUploads fileUploads = new FileUploads();
								//附件名称 
								String p_filename = formItems.get(i).getName();
								//文件大小
								long p_filesize = formItems.get(i).length();//附件大小
								fileUp.setP_filename(p_filename);
								fileUp.setP_filesize(p_filesize);
								Log.getLogger().info("开始调用CRM保存文件信息存储过程");
								String l_storageid = reservationUtil.saveFile(fileUp);//调用CRM保存文件信息存储过程
								Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
								String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
								Log.getLogger().info("Tblobstorage目录存在");
							    String uploadUrl = url+"/Tblobstorage/"+fileNo+"/";//4位取3位
							    //文件上传目录
							    SmbFile file2 = null;
								try {
									file2 = new SmbFile(uploadUrl);
									Log.getLogger().info("判断是否存在文件上传目录");
								     if(!file2.exists() && !file2.isDirectory()) {
								    	 Log.getLogger().info(fileNo+"目录不存在，正在创建");
								    	 file2.mkdir();
								    }
								} catch (Exception e) {
									state = "00";
									msg = "申请已提交，但图片服务器连接失败！";
									e.printStackTrace();
									return state+"-"+msg; 
								}
							     Log.getLogger().info("开始调用文件上传加密方法");
						    	 String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
						    	 Log.getLogger().info("判断文件上传加密是否成功");
						    	 if(null != state2 && !"".equals(state2)) {
						    		 Log.getLogger().info("文件上传加密成功");
						    		 String fileUrl = state2.split(",")[0];
						    		 String newFileName = state2.split(",")[1];
						    		 fileUploads.setUploadUrl(fileUrl);
						    		 fileUploads.setNewFileName(newFileName);
						    		 //根据文件名查询文件PkId---p_filename
						    		 String pkId = fileUploadDao.queryById(p_filename).getPkId();
						    		 if(null != pkId && !"".equals(pkId)) {
						    			 fileUploadDao.updateByPrimaryKey(fileUploads);
						    		 }
						    	 }else {
						    		Log.getLogger().info("判断共享文件地址是否正确连接");
						    		state = "00";
									msg = "申请已提交，但图片服务器连接失败！";
									return state+"-"+msg;
						    	 }
							}
							state = "00";
							msg = "上传图片，已提交认证申请，等待系统审核";
							return state +"-"+ msg;
						}else {
							state = "00";
							msg = "未上传图片，已提交认证申请，等待系统审核";
							return state +"-"+ msg;
						}
					}else {
						if(null != returninfo && !"".equals(returninfo)) {
							state = "01";
							msg = returninfo;
							return state +"-"+ msg;
						}else {
							state = "01";
							msg = "系统异常，请联系管理员";
							return state +"-"+ msg;
						}
					}
				}
				if(call !=null){
					call.close();
				}
				if(conn !=null){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		//附件上传
		public List<File> fileUpLoad(String tempUrl,JSONObject json) {
			List<File> formItems = new ArrayList<File>();
			if(json.has("fileName")) {
				String upFiles = json.getString("fileName");
				String[] fileName = upFiles.split(",");
				for(int i = 0;i < fileName.length;i++) {
					String name = fileName[i];
					File isFile = new File(tempUrl+name);
					if(isFile.exists()) {
						formItems.add(isFile);
					}
				}
				return formItems;
			}else {
				return formItems;
			}
		}
		
		//附件加密
		public String fileJm(String uploadUrl,File item,String l_storageid,String tempUrl) {
			String state1 = null;
	    	//修改后的文件名字（l_storageid----调用CRM系统文件保存接口返回的编号）
		    String fileName = l_storageid+"baccessory";
	    	 // 保存文件到硬盘
	    	 InputStream in = null;
	         OutputStream out = null;
	 		try {
	 			in = new BufferedInputStream(new FileInputStream(tempUrl+"/"+item.getName()));
	 			out = new BufferedOutputStream(new SmbFileOutputStream(uploadUrl+fileName));
	 		} catch (Exception e1) {
	 			Log.getLogger().info("判断共享文件地址是否正确连接");
	 			// TODO Auto-generated catch block
	 			e1.printStackTrace();
	 			return state1;
	 		}
	 		try {
	 			byte[] buffer = new byte[4096]; 
	 			  int len = 0; //读取长度 
	            try {
	            	while ((len = in.read(buffer, 0, buffer.length)) != -1) { 
	        		    out.write(buffer, 0, len); 
	        		} 
	            	out.flush(); //刷新缓冲的输出流 
	            	Log.getLogger().info("-----------");
				} catch (IOException e) {
					Log.getLogger().info("判断共享文件地址是否正确连接");
					e.printStackTrace();
					return state1;
				}
	        } finally {
	            try {
	                out.close();
	                in.close();
	            } catch (Exception e) {
	            	Log.getLogger().info("判断共享文件地址是否正确连接");
	                e.printStackTrace();
	                return state1;
	            }
	        }
	    	System.out.println("文件上传并加密成功......");
	    	String newUrl = uploadUrl+fileName;
	    	state1 = newUrl +","+fileName;
   			return state1;//文件上传并加密成功
		}

	//是否可以进行合格投资者认证申请
	public Integer queryHgShCount(String p_custno) {
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		//客户存在待审核申请的数量
  		Integer count = 0;
  		try {
  			call = conn.prepareCall(" SELECT COUNT(*) FROM 	TQUALIFYINVESTOR_REQ WHERE C_STATUS = '1' AND C_CUSTNO = '" + p_custno+"'");
  			// 执行
  			ResultSet rs = call.executeQuery();
  			while (rs.next()) {
  				count = rs.getInt("COUNT(*)");
  			}
  			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
  		} catch (SQLException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		return count;
	}
}

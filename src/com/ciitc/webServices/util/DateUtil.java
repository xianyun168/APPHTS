package com.ciitc.webServices.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {
	
	//layout为日期格式"yyyy-MM-dd HH:mm:ss"
	public static String nowStingDate(String layout){
		SimpleDateFormat df = new SimpleDateFormat(layout);
		//System.out.println(df.format(new Date()));
		return df.format(new Date());
	}
}

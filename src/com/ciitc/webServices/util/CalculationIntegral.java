package com.ciitc.webServices.util;

import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.com.ins.sinosoft.framework.base.util.DateUtil;
import com.ciitc.webServices.dao.BatchAddDao;
import com.ciitc.webServices.dao.appIntegralDetailsDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.Integration;
import com.ciitc.webServices.model.appIntegralDetails;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

public class CalculationIntegral {

	@SuppressWarnings("rawtypes")
	private static BatchAddDao satchAddDao = (BatchAddDao) SpringContextUtil.getBean("batchAddDao");
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static appIntegralDetailsDao<appIntegralDetails> IntegraDetailsDao = (appIntegralDetailsDao) SpringContextUtil.getBean("appIntegralDetailsDao");
	
	@SuppressWarnings("rawtypes")
	private static EsbmmCodeService esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	
	//2018-01-01之前的合同，等级系数全部为1计算
	public void tradingIntegral1(List<Integration> list) {
		if(list.size()>0){
			for(Integration inte:list){
				AppUserInfor appUser = (AppUserInfor) satchAddDao.queryBycustomer(inte.getCustno());
				if(appUser !=null){
					@SuppressWarnings("unused")
					Double integration =0.0;
					//持仓天数数
					double position = inte.getPositionDate();
					double positions = position/365;
					BigDecimal b = new BigDecimal(positions);
					//保留两位小数
					double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
					BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1);
					integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("userId", appUser.getPkId());
					appIntegralDetails appIntegral = new appIntegralDetails();
					String IntegralId = UUID.randomUUID().toString();
					appIntegral.setPkId(IntegralId);
					appIntegral.setUserId(appUser.getPkId());
					appIntegral.setRecordDate(inte.getContractsignDate());
					appIntegral.setIntegralNum(integration+"");
					appIntegral.setValidity(365);
					appIntegral.setIntegralType("00");
					appIntegral.setName(inte.getProtuctName());
					appIntegral.setIntegralState("00");
					appIntegral.setContractNum(inte.getReserveno());
					appIntegral.setConsumptionIntegral(0);
					appIntegral.setExpireIntegral(0);
					appIntegral.setValidIntegral(integration);
					String time = getYears(inte.getContractsignDate());
					SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
					try {
						Date da = sf.parse(time);
						appIntegral.setExpireDate(da);
						IntegraDetailsDao.insertSelective(appIntegral);
						AppUserInfor newAppUser = new AppUserInfor();
						newAppUser.setPkId(appUser.getPkId());
						//计算积分明细总和
						//根据客户编号查询客户有效积分
						Map<String,Object> map1 = new HashMap<String, Object>();
						map1.put("userId", appUser.getPkId());
						//统计此用户有效积分总和
						long sum = IntegraDetailsDao.selectUserIdSum(map1);
						newAppUser.setIntegral(sum);
						satchAddDao.updatebyuserId(newAppUser);
						System.out.println(inte.getCustno()+"--积分--"+newAppUser.getIntegral());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
						
				}
				
			}
			System.out.println("*************************结束*****************************");
		}
	}
	public static void tradingIntegral2(List<Integration> list,String level) {
		if(list.size()>0){
			for(Integration inte:list){
				AppUserInfor appUser = (AppUserInfor) satchAddDao.queryBycustomer(inte.getCustno());
				if(appUser !=null){
					@SuppressWarnings("unused")
					double integration =0.0;
					if(level.equals("1") || level.equals("2")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else if(level.equals("3")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.1);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else if(level.equals("4")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.2);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else if(level.equals("5")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.3);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("userId", appUser.getPkId());
					//查询积分明细
					//List<appIntegralDetails> details = IntegraDetailsDao.selectByuserId(map);
					//判断是否存在这个用户
					//if(details.size()==0){
						appIntegralDetails appIntegral = new appIntegralDetails();
						String IntegralId = UUID.randomUUID().toString();
						appIntegral.setPkId(IntegralId);
						appIntegral.setUserId(appUser.getPkId());
						appIntegral.setRecordDate(inte.getContractsignDate());
						appIntegral.setIntegralNum(integration+"");
						appIntegral.setValidity(365);
						appIntegral.setIntegralType("00");
						appIntegral.setName(inte.getProtuctName());
						appIntegral.setIntegralState("00");
						appIntegral.setContractNum(inte.getReserveno());
						appIntegral.setConsumptionIntegral(0);
						appIntegral.setExpireIntegral(0);
						appIntegral.setValidIntegral(integration);
						String time = CalculationIntegral.getYears(inte.getContractsignDate());
						SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
						try {
							Date da = sf.parse(time);
							appIntegral.setExpireDate(da);
							IntegraDetailsDao.insertSelective(appIntegral);
							AppUserInfor newAppUser = new AppUserInfor();
							newAppUser.setPkId(appUser.getPkId());
							//根据客户编号查询客户有效积分
							Map<String,Object> map1 = new HashMap<String, Object>();
							map1.put("userId", appUser.getPkId());
							//统计此用户有效积分总和
							long sum = IntegraDetailsDao.selectUserIdSum(map1);
							newAppUser.setIntegral(sum);
							satchAddDao.updatebyuserId(newAppUser);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					//}
				}
			}
		}
	}
	
	/**
	 * @param integralNum 合同金额
	 * @param numDays 持仓天数
	 * @param GradeNub 等级系数
	 * @return 积分总数
	 */
	@SuppressWarnings("unchecked")
	public static void tradingIntegral(List<Integration> list) {
		//合同积分
//		Integer integration=(int) ((integralNum/10000)*(numDays/365)*(Double.parseDouble(GradeNub)));
//		return integration;
		if(list.size()>0){
			for(Integration inte:list){
				AppUserInfor appUser = (AppUserInfor) satchAddDao.queryBycustomer(inte.getCustno());
				if(appUser !=null){
					@SuppressWarnings("unused")
					double integration =0.0;
					if(appUser.getCustomerLevel().equals("1") || appUser.getCustomerLevel().equals("2")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else if(appUser.getCustomerLevel().equals("3")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.1);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else if(appUser.getCustomerLevel().equals("4")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.2);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
					}else if(appUser.getCustomerLevel().equals("5")){
						//持仓天数数
						double position = inte.getPositionDate();
						double positions = position/365;
						BigDecimal b = new BigDecimal(positions);
						//保留两位小数
						double position1 = b.setScale(10, BigDecimal.ROUND_HALF_UP).doubleValue();
						BigDecimal b1 = new BigDecimal((inte.getMoney()/10000)*position1*1.3);
						integration =b1.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
						
					}
					//2018-12-25 2019-01-24
					SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
					String formatTimeStr=simpleDateFormat.format(inte.getContractsignDate());
					boolean isBelongDate= DateUtil.belongCalendar(formatTimeStr,"2018-12-25","2019-01-24");
					if(isBelongDate){
						integration=integration*2;
					}
					Map<String,Object> map = new HashMap<String, Object>();
					map.put("userId", appUser.getPkId());
					//查询积分明细
					//List<appIntegralDetails> details = IntegraDetailsDao.selectByuserId(map);
					//判断是否存在这个用户
					//if(details.size()==0){
						appIntegralDetails appIntegral = new appIntegralDetails();
						String IntegralId = UUID.randomUUID().toString();
						appIntegral.setPkId(IntegralId);
						appIntegral.setUserId(appUser.getPkId());
						appIntegral.setRecordDate(inte.getContractsignDate());
						appIntegral.setIntegralNum(integration+"");
						appIntegral.setValidity(365);
						appIntegral.setIntegralType("00");
						appIntegral.setName(inte.getProtuctName());
						appIntegral.setIntegralState("00");
						appIntegral.setContractNum(inte.getReserveno());
						appIntegral.setConsumptionIntegral(0);
						appIntegral.setExpireIntegral(0);


						appIntegral.setValidIntegral(integration);
						String time = CalculationIntegral.getYears(inte.getContractsignDate());
						SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
						try {
							Date da = sf.parse(time);
							appIntegral.setExpireDate(da);
							IntegraDetailsDao.insertSelective(appIntegral);
							AppUserInfor newAppUser = new AppUserInfor();
							newAppUser.setPkId(appUser.getPkId());
							//根据客户编号查询客户有效积分
							Map<String,Object> map1 = new HashMap<String, Object>();
							map1.put("userId", appUser.getPkId());
							//统计此用户有效积分总和
							long sum = IntegraDetailsDao.selectUserIdSum(map1);
							newAppUser.setIntegral(sum);
							satchAddDao.updatebyuserId(newAppUser);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					//}
				}
			}
		}
	}
	/**
	 * @param integration 总积分
	 * @return 总积分+生日积分10 有效期30天
	 */
	public static void BirIntegral(String userId,Integer integration,String name,String type) {
		//Integer birintegralNum=integration+10;
		if(type.equals("01")){
			AppUserInfor appUser= (AppUserInfor) satchAddDao.queryByPkId(userId);
			if(appUser !=null){
				long history = appUser.getIntegral();
				appUser.setIntegral(history+integration);
				satchAddDao.updatebyuserId(appUser);
				String IntegralId = UUID.randomUUID().toString();
				appIntegralDetails appIntegral = new appIntegralDetails();
				appIntegral.setPkId(IntegralId);
				appIntegral.setUserId(userId);
				appIntegral.setRecordDate(new Date());
				appIntegral.setIntegralNum(integration+"");
				appIntegral.setValidity(30);
				appIntegral.setIntegralType("01");
				appIntegral.setName(name);
				appIntegral.setIntegralState("00");
				//appIntegral.setContractNum(inte.getReserveno());
				appIntegral.setConsumptionIntegral(0);
				appIntegral.setExpireIntegral(0);
				appIntegral.setValidIntegral(integration);
				String time = CalculationIntegral.getQuarters(new Date());
				SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date da = sf.parse(time);
					appIntegral.setExpireDate(da);
					IntegraDetailsDao.insertSelective(appIntegral);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}else if(type.equals("02") || type.equals("03") || type.equals("04")){
			AppUserInfor appUser= (AppUserInfor) satchAddDao.queryByPkId(userId);
			if(appUser !=null){
				long history = appUser.getIntegral();
				appUser.setIntegral(history+integration);
				satchAddDao.updatebyuserId(appUser);
				String IntegralId = UUID.randomUUID().toString();
				appIntegralDetails appIntegral = new appIntegralDetails();
				appIntegral.setPkId(IntegralId);
				appIntegral.setUserId(userId);
				appIntegral.setRecordDate(new Date());
				appIntegral.setIntegralNum(integration+"");
				appIntegral.setValidity(365);
				appIntegral.setIntegralType(type);
				appIntegral.setName(name);
				appIntegral.setIntegralState("00");
				appIntegral.setConsumptionIntegral(0);
				appIntegral.setExpireIntegral(0);
				appIntegral.setValidIntegral(integration);
				String time = CalculationIntegral.getYears(new Date());
				SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
				try {
					Date da = sf.parse(time);
					appIntegral.setExpireDate(da);
					IntegraDetailsDao.insertSelective(appIntegral);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	//添加积分明细公共方法
	@SuppressWarnings("unchecked")
	public static void IntegralDel(String userId,double Integral,String protuctName){
		long initial = Math.round(Integral);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("userId", userId);
		//查询积分明细
		List<appIntegralDetails> details = IntegraDetailsDao.selectUserId(map);
		//判断积分明细表中是否存在此用户信息
		if(details.size()==0){
			//没有此用户信息
			appIntegralDetails appIntegral = new appIntegralDetails();
			String IntegralId = UUID.randomUUID().toString();
			appIntegral.setPkId(IntegralId);
			appIntegral.setUserId(userId);
			appIntegral.setRecordDate(new Date());
			appIntegral.setIntegralNum(Integral+"");
			appIntegral.setValidity(365);
			appIntegral.setName(protuctName);
			appIntegral.setIntegralState("01");
			IntegraDetailsDao.insertSelective(appIntegral);
			//根据用户id查询用户
			AppUserInfor appUser = (AppUserInfor) satchAddDao.queryByuserId(userId);
			//判断用户总积分是否大于订单积分
			if(appUser.getIntegral() > Integral){
				double difference = appUser.getIntegral() - Integral;
				AppUserInfor newAppUser = new AppUserInfor();
				newAppUser.setPkId(appUser.getPkId());
				newAppUser.setIntegral(Math.round(difference));
				satchAddDao.updatebyuserId(newAppUser);
			}
		}else{
			//存在此用户记录
			appIntegralDetails appIntegral = new appIntegralDetails();
			String IntegralId = UUID.randomUUID().toString();
			appIntegral.setPkId(IntegralId);
			appIntegral.setUserId(userId);
			appIntegral.setRecordDate(new Date());
			appIntegral.setIntegralNum(Integral+"");
			appIntegral.setValidity(365);
			appIntegral.setName(protuctName);
			appIntegral.setIntegralState("01");
			//流水添加到积分明细表
			IntegraDetailsDao.insertSelective(appIntegral);
			//记录上次循环积分
			//long initial = 0;
			for(int i=0;i<details.size();i++){
				//商品积分值大于有效积分值
				if(details.get(i).getValidIntegral() < initial){
					initial = Math.round(initial - details.get(i).getValidIntegral());
					appIntegralDetails newDetails = new appIntegralDetails();
					newDetails.setConsumptionIntegral(details.get(i).getValidIntegral());
				//	long validIntegral = Math.round(details.get(i).getValidIntegral() - Integral);
					newDetails.setValidIntegral(0);
					newDetails.setPkId(details.get(i).getPkId());
					IntegraDetailsDao.updateByPrimaryKeySelective(newDetails);
				//商品积分值等于有效积分值
				}else if(details.get(i).getValidIntegral() == initial){
					appIntegralDetails newDetails = new appIntegralDetails();
					newDetails.setConsumptionIntegral(initial);
					long validIntegral = Math.round(details.get(i).getValidIntegral() - initial);
					newDetails.setValidIntegral(validIntegral);
					newDetails.setPkId(details.get(i).getPkId());
					IntegraDetailsDao.updateByPrimaryKeySelective(newDetails);
				//商品积分值小于有效积分值	
				}else{
					appIntegralDetails newDetails = new appIntegralDetails();
					newDetails.setConsumptionIntegral(initial);
					long validIntegral = Math.round(details.get(i).getValidIntegral() - initial);
					newDetails.setValidIntegral(validIntegral);
					newDetails.setPkId(details.get(i).getPkId());
					IntegraDetailsDao.updateByPrimaryKeySelective(newDetails);
				}
			}
			long sum = IntegraDetailsDao.selectUserIdSum(map);
			
			//AppUserInfor appUserInfor =  (AppUserInfor) satchAddDao.queryByPkId(userId);
			AppUserInfor appUserInfor = new AppUserInfor();
			appUserInfor.setPkId(userId);
			appUserInfor.setIntegral(sum);
			satchAddDao.updatebyuserId(appUserInfor);
		}
		
	}
	
	
	//初始某个日期时点客户历史的峰值
	/*@SuppressWarnings("unchecked")
	public static void firstCustomerLevel(String date) {
		System.out.println("初始等级计算开始："+new Date());
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		//String sql = "select cvo.c_custno,nvl(cuc.f_contractsignbalance, 0) f_contractsignbalance from ((select distinct c_custno from cust_view_position) cvo left join (select cvp.c_custno, cvp.f_contractsignbalance from cust_view_position cvp  where cvp.d_caldate = (select max(cv.d_caldate) from cust_view_position cv where cv.d_caldate <= to_date('20181231', 'yyyymmdd') and cv.c_custno = cvp.c_custno) and (select max(cp.d_caldate) from cust_view_position cp where cp.c_custno = cvp.c_custno) >= to_date('20181231', 'yyyymmdd')) cuc  on cvo.c_custno = cuc.c_custno)";
		String sql = "select cvp.c_custno,max(cvp.f_contractsignbalance) from cust_view_position cvp where cvp.d_caldate<=to_date('"+date+"','yyyymmdd') group by cvp.c_custno ";
		
		try {
			// 组织架构表
			call = conn.prepareCall(sql);
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				AppUserInfor appUserInfor = new AppUserInfor();
				
				String val = rs.getString("MAX(CVP.F_CONTRACTSIGNBALANCE)");
				if(0 <= Integer.valueOf(val) && Integer.valueOf(val)< 1000000){
					appUserInfor.setCustomerLevel("1");
				}else if(1000000 <= Integer.valueOf(val) && Integer.valueOf(val) <3000000){
					appUserInfor.setCustomerLevel("2");
				}else if(3000000 <= Integer.valueOf(val) && Integer.valueOf(val) <10000000){
					appUserInfor.setCustomerLevel("3");
				}else if(10000000 <= Integer.valueOf(val) && Integer.valueOf(val) <50000000){
					appUserInfor.setCustomerLevel("4");
				}else if(50000000 <= Integer.valueOf(val)){
					appUserInfor.setCustomerLevel("5");
				}

				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));
				satchAddDao.updateBycustomer(appUserInfor);
			}
			System.out.println("初始等级计算结束："+new Date());
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	//查询某个日期时点的当前持仓值
	//定时算等级
	@SuppressWarnings("unchecked")
	public static void CustomerLevel(){
		System.out.println("初始等级计算开始："+new Date());
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs =null;
		SimpleDateFormat time=new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		Integer nowYear = calendar.get(Calendar.YEAR);
		Integer month = calendar.get(Calendar.MONTH)+1;
		/*String startDate = String.valueOf(nowYear)+"0101";
		String endDate = String.valueOf(nowYear)+"1231";*/
		String startDate = null;
		String endDate = null;
		if(month == 1 || month == 2 || month == 3) {
			startDate = String.valueOf(nowYear-1)+"1001";
			endDate = String.valueOf(nowYear-1)+"1231";
		}else if(month == 4 || month == 5 || month == 6) {
			startDate = String.valueOf(nowYear)+"0101";
			endDate = String.valueOf(nowYear)+"0331";
		}else if(month == 7 || month == 8 || month == 9) {
			startDate = String.valueOf(nowYear)+"0401";
			endDate = String.valueOf(nowYear)+"0630";
		}else if(month == 10 || month == 11 || month == 12){
			startDate = String.valueOf(nowYear)+"0701";
			endDate = String.valueOf(nowYear)+"0930";
		}
		//String sql = "select cvo.c_custno,nvl(cuc.f_contractsignbalance, 0) f_contractsignbalance from ((select distinct c_custno from cust_view_position) cvo left join (select cvp.c_custno, cvp.f_contractsignbalance from cust_view_position cvp  where cvp.d_caldate = (select max(cv.d_caldate) from cust_view_position cv where cv.d_caldate <= to_date('"+currentTime+"', 'yyyymmdd') and cv.c_custno = cvp.c_custno) and (select max(cp.d_caldate) from cust_view_position cp where cp.c_custno = cvp.c_custno) >= to_date('"+currentTime+"', 'yyyymmdd')) cuc  on cvo.c_custno = cuc.c_custno)";
		//String sql = "select cvp.c_custno,max(cvp.f_contractsignbalance) from cust_view_position cvp where cvp.d_caldate<=to_date('"+date+"','yyyymmdd') group by cvp.c_custno ";
		//String sql = "select c_custno,max(F_CONTRACTSIGNBALANCE) from cust_view_position where d_caldate between to_date('"+startDate+"', 'yyyymmdd') and to_date('"+endDate+"', 'yyyymmdd') group by c_custno";
		List<EsbmmCode> list = esbmmCodeService.getLevel();
		String one = list.get(0).getCodeValue();
		String[] intervalOne = one.split(",");
		String two = list.get(1).getCodeValue();
		String[] intervalTwo = two.split(",");
		String three = list.get(2).getCodeValue();
		String[] intervalThree = three.split(",");
		String four = list.get(3).getCodeValue();
		String[] intervalFour =four.split(",");
		String five = list.get(4).getCodeValue();
		String[] intervalFive = five.split(",");
		//查询用户表已实名的客户信息
		List<AppUserInfor> appUserInforList = satchAddDao.queryByAll();
		for (AppUserInfor appUserInfor1 : appUserInforList) {
			String customerId = appUserInfor1.getCustomerId();
			String sql =" with a1 as ( select t1.c_custno,t1.d_capitaldate d_capitaldate,t1.l_proserialno,t1.f_balance,t2.d_expirationdate   " + 
					"  from tcapitalcurrents t1 left join tsaleproject t2 on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' " + 
					"   AND t1.C_FLAG = '1'  union all  select t1.c_custno,t1.d_capitaldate d_capitaldate,  t1.l_proserialno,  t1.f_balance," + 
					"   to_date(to_char(t1.d_setupdate,'yyyymmdd'),'yyyymmdd') d_expirationdate  from tcapitalcurrents t1  left join tsaleproject t2 " + 
					"  on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' AND t1.C_FLAG = '9'  AND to_date(to_char(t2.d_setupdate,'yyyymmdd'),'yyyymmdd') <> t1.d_capitaldate ), " + 
					"  A2 AS ( SELECT c_custno, add_date,  SUM(f_balance) f_balance FROM( SELECT  c_custno, d_capitaldate add_date, f_balance  " + 
					"    FROM A1  UNION ALL SELECT c_custno, d_expirationdate+1,  (-1)*f_balance f_balance    FROM A1)  GROUP BY c_custno,  add_date )  " + 
					"  select c_custno, max(f_balance) from( SELECT   t1.c_custno,  t1.add_date startdate, " + 
					"  lead(t1.add_date-1,1,to_date('29991231','yyyymmdd')) over (partition by t1.c_custno order by t1.add_date asc ) enddate,  " + 
					"  sum(t2.f_balance) f_balance FROM  A2  t1 LEFT JOIN  A2 t2 ON t1.add_date >= t2.add_date " + 
					"  and t1.c_custno = t2.c_custno group by  t1.c_custno,t1.add_date ) " + 
					"  where c_custno ='"+customerId+"' and  (startdate between to_date('"+startDate+"','yyyymmdd') and to_date('"+endDate+"','yyyymmdd')  " + 
					"   or (startdate < to_date('"+startDate+"','yyyymmdd') and enddate >=to_date('"+startDate+"','yyyymmdd'))) " + 
					"  group by c_custno ";
			try {
				// 组织架构表
				call = conn.prepareCall(sql);
				// 执行
				rs = call.executeQuery();
				
				AppUserInfor appUserInfor = new AppUserInfor();
				appUserInfor.setCustomerLevel("1");
				while (rs.next()) {
					String val = rs.getString("MAX(F_BALANCE)");
					if(Integer.valueOf(intervalOne[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalOne[1])){
						appUserInfor.setCustomerLevel("1");
					}else if(Integer.valueOf(intervalTwo[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalTwo[1])){
						appUserInfor.setCustomerLevel("2");
					}else if(Integer.valueOf(intervalThree[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalThree[1])){
						appUserInfor.setCustomerLevel("3");
					}else if(Integer.valueOf(intervalFour[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalFour[1])){
						appUserInfor.setCustomerLevel("4");
					}else if(Integer.valueOf(intervalFive[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalFive[1])){
						appUserInfor.setCustomerLevel("5");
					}
					appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));
					//appUserInfor.setCustomerLevel(customerLevel);
				}
				satchAddDao.updateBycustomer(appUserInfor);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			if(rs !=null){
				rs.close();
			}
			if(conn !=null){
				conn.close();
			}
			if(call !=null){
			call.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	// 查询某个日期时点的当前持仓值
	// 定时算等级
	@SuppressWarnings("unchecked")
	public static void CustomerLevel1() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
		String nowDate = sdf.format(new Date());
		if("2018".equals(nowDate)) {
			CustomerLevel();
		}
	}
	//根据客户编号计算客户等级
	/*public static void CustomerLevelBycustoNo(String custoNo){
		 Connection conn = DBUtil.makeConnection();
		 CallableStatement call = null;
		String sql = " select cvp.c_custno,max(cvp.f_contractsignbalance) from cust_view_position cvp where  cvp.c_custno ='"+custoNo+"'   group by cvp.c_custno ";
		try {
			// 组织架构表
			call = conn.prepareCall(sql);
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				AppUserInfor appUserInfor = new AppUserInfor();
				
				String val = rs.getString("MAX(CVP.F_CONTRACTSIGNBALANCE)");
				if(0 <= Integer.valueOf(val) && Integer.valueOf(val)< 1000000){
					appUserInfor.setCustomerLevel("1");
				}else if(1000000 <= Integer.valueOf(val) && Integer.valueOf(val) <3000000){
					appUserInfor.setCustomerLevel("2");
				}else if(3000000 <= Integer.valueOf(val) && Integer.valueOf(val) <10000000){
					appUserInfor.setCustomerLevel("3");
				}else if(10000000 <= Integer.valueOf(val) && Integer.valueOf(val) <50000000){
					appUserInfor.setCustomerLevel("4");
				}else if(50000000 <= Integer.valueOf(val)){
					appUserInfor.setCustomerLevel("5");
				}

				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));
				satchAddDao.updateBycustomer(appUserInfor);
			}
			//System.out.println("初始等级计算结束："+new Date());
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	//根据客户编号计算客户等级
		/*public static String CustomerLevelBycustoNo1(String custoNo){
		 Connection conn = DBUtil.makeConnection();
		 CallableStatement call = null;
			String level = "1";
			String sql = " select cvp.c_custno,max(cvp.f_contractsignbalance) from cust_view_position cvp where  cvp.c_custno ='"+custoNo+"'   group by cvp.c_custno ";
			try {
				// 组织架构表
				call = conn.prepareCall(sql);
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					String val = rs.getString("MAX(CVP.F_CONTRACTSIGNBALANCE)");
					if(0 <= Integer.valueOf(val) && Integer.valueOf(val)< 1000000){
						level = "1";
					}else if(1000000 <= Integer.valueOf(val) && Integer.valueOf(val) <3000000){
						level = "2";
					}else if(3000000 <= Integer.valueOf(val) && Integer.valueOf(val) <10000000){
						level = "3";
					}else if(10000000 <= Integer.valueOf(val) && Integer.valueOf(val) <50000000){
						level = "4";
					}else if(50000000 <= Integer.valueOf(val)){
						level = "5";
					}
				}
				if(rs !=null){
					rs.close();
				}
				if(call !=null){
					call.close();
				}
				if(conn !=null){
					conn.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return level;
		}*/
	
	
	//根据客户编号计算客户等级
		/*public static void CustomerLevelBycustoNo(String custoNo,String date,String datey){
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			//String sql = "select cvo.c_custno,nvl(cuc.f_contractsignbalance, 0) f_contractsignbalance from ((select distinct c_custno from cust_view_position) cvo left join (select cvp.c_custno, cvp.f_contractsignbalance from cust_view_position cvp  where cvp.d_caldate = (select max(cv.d_caldate) from cust_view_position cv where cv.d_caldate <= to_date('"+date+"', 'yyyymmdd') and cv.c_custno = cvp.c_custno) and (select max(cp.d_caldate) from cust_view_position cp where cp.c_custno = cvp.c_custno) >= to_date('"+date+"', 'yyyymmdd')) cuc  on cvo.c_custno = cuc.c_custno)  where cvo.c_custno='"+custoNo+"' ";
			String sql = "select c_custno,max(F_CONTRACTSIGNBALANCE) from cust_view_position where d_caldate between to_date('"+datey+"', 'yyyymmdd') and to_date('"+date+"', 'yyyymmdd') and c_custno ='"+custoNo+"'";
			try {
				// 组织架构表
				call = conn.prepareCall(sql);
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					AppUserInfor appUserInfor = new AppUserInfor();
					
					String val = rs.getString("F_CONTRACTSIGNBALANCE");
					if(0 <= Integer.valueOf(val) && Integer.valueOf(val)< 1000000){
						appUserInfor.setCustomerLevel("1");
					}else if(1000000 <= Integer.valueOf(val) && Integer.valueOf(val) <3000000){
						appUserInfor.setCustomerLevel("2");
					}else if(3000000 <= Integer.valueOf(val) && Integer.valueOf(val) <10000000){
						appUserInfor.setCustomerLevel("3");
					}else if(10000000 <= Integer.valueOf(val) && Integer.valueOf(val) <50000000){
						appUserInfor.setCustomerLevel("4");
					}else if(50000000 <= Integer.valueOf(val)){
						appUserInfor.setCustomerLevel("5");
					}

					appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));
					satchAddDao.updateBycustomer(appUserInfor);
				}
				//System.out.println("初始等级计算结束："+new Date());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}*/
	//2018-11-16号修改，原始的方法是CustomerLevelBycustoNo(String custoNo,String date,String datey);
	public static void CustomerLevelBycustoNo(String custoNo){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs =null;
		SimpleDateFormat time=new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		Integer nowYear = calendar.get(Calendar.YEAR);
		Integer month = calendar.get(Calendar.MONTH)+1;
		String startDate = null;
		String endDate = null;
		if(month == 1 || month == 2 || month == 3) {
			startDate = String.valueOf(nowYear-1)+"1001";
			endDate = String.valueOf(nowYear-1)+"1231";
		}else if(month == 4 || month == 5 || month == 6) {
			startDate = String.valueOf(nowYear)+"0101";
			endDate = String.valueOf(nowYear)+"0331";
		}else if(month == 7 || month == 8 || month == 9) {
			startDate = String.valueOf(nowYear)+"0401";
			endDate = String.valueOf(nowYear)+"0630";
		}else if(month == 10 || month == 11 || month == 12){
			startDate = String.valueOf(nowYear)+"0701";
			endDate = String.valueOf(nowYear)+"0930";
		}
		//查询用户表已实名的客户信息
		String sql =" with a1 as ( select t1.c_custno,t1.d_capitaldate d_capitaldate,t1.l_proserialno,t1.f_balance,t2.d_expirationdate   " + 
				"  from tcapitalcurrents t1 left join tsaleproject t2 on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' " + 
				"   AND t1.C_FLAG = '1'  union all  select t1.c_custno,t1.d_capitaldate d_capitaldate,  t1.l_proserialno,  t1.f_balance," + 
				"   to_date(to_char(t1.d_setupdate,'yyyymmdd'),'yyyymmdd') d_expirationdate  from tcapitalcurrents t1  left join tsaleproject t2 " + 
				"  on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' AND t1.C_FLAG = '9'  AND to_date(to_char(t2.d_setupdate,'yyyymmdd'),'yyyymmdd') <> t1.d_capitaldate ), " + 
				"  A2 AS ( SELECT c_custno, add_date,  SUM(f_balance) f_balance FROM( SELECT  c_custno, d_capitaldate add_date, f_balance  " + 
				"    FROM A1  UNION ALL SELECT c_custno, d_expirationdate+1,  (-1)*f_balance f_balance    FROM A1)  GROUP BY c_custno,  add_date )  " + 
				"  select c_custno, max(f_balance) from( SELECT   t1.c_custno,  t1.add_date startdate, " + 
				"  lead(t1.add_date-1,1,to_date('29991231','yyyymmdd')) over (partition by t1.c_custno order by t1.add_date asc ) enddate,  " + 
				"  sum(t2.f_balance) f_balance FROM  A2  t1 LEFT JOIN  A2 t2 ON t1.add_date >= t2.add_date " + 
				"  and t1.c_custno = t2.c_custno group by  t1.c_custno,t1.add_date ) " + 
				"  where c_custno ='"+custoNo+"' and  (startdate between to_date('"+startDate+"','yyyymmdd') and to_date('"+endDate+"','yyyymmdd')  " + 
				"   or (startdate < to_date('"+startDate+"','yyyymmdd') and enddate >=to_date('"+startDate+"','yyyymmdd'))) " + 
				"  group by c_custno ";
		try {
			// 组织架构表
			call = conn.prepareCall(sql);
			// 执行
			rs = call.executeQuery();
			List<EsbmmCode> list = esbmmCodeService.getLevel();
			String one = list.get(0).getCodeValue();
			String[] intervalOne = one.split(",");
			String two = list.get(1).getCodeValue();
			String[] intervalTwo = two.split(",");
			String three = list.get(2).getCodeValue();
			String[] intervalThree = three.split(",");
			String four = list.get(3).getCodeValue();
			String[] intervalFour =four.split(",");
			String five = list.get(4).getCodeValue();
			String[] intervalFive = five.split(",");
			AppUserInfor appUserInfor = new AppUserInfor();
			appUserInfor.setCustomerLevel("1");
			while (rs.next()) {
				String val = rs.getString("MAX(F_BALANCE)");
				if(Integer.valueOf(intervalOne[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalOne[1])){
					appUserInfor.setCustomerLevel("1");
				}else if(Integer.valueOf(intervalTwo[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalTwo[1])){
					appUserInfor.setCustomerLevel("2");
				}else if(Integer.valueOf(intervalThree[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalThree[1])){
					appUserInfor.setCustomerLevel("3");
				}else if(Integer.valueOf(intervalFour[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalFour[1])){
					appUserInfor.setCustomerLevel("4");
				}else if(Integer.valueOf(intervalFive[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalFive[1])){
					appUserInfor.setCustomerLevel("5");
				}
				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));
			}
			satchAddDao.updateBycustomer(appUserInfor);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if(rs !=null){
				rs.close();
			}
			if(conn !=null){
				conn.close();
			}
			if(call !=null){
			call.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
		//根据客户编号计算客户等级
		/*public static String CustomerLevelBycustoNo1(String custoNo,String date,String datey){
			// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			String level = "1";
			//String sql = "select cvo.c_custno,nvl(cuc.f_contractsignbalance, 0) f_contractsignbalance from ((select distinct c_custno from cust_view_position) cvo left join (select cvp.c_custno, cvp.f_contractsignbalance from cust_view_position cvp  where cvp.d_caldate = (select max(cv.d_caldate) from cust_view_position cv where cv.d_caldate <= to_date('"+date+"', 'yyyymmdd') and cv.c_custno = cvp.c_custno) and (select max(cp.d_caldate) from cust_view_position cp where cp.c_custno = cvp.c_custno) >= to_date('"+date+"', 'yyyymmdd')) cuc  on cvo.c_custno = cuc.c_custno)  where cvo.c_custno='"+custoNo+"' ";
			String sql = "select c_custno,max(F_CONTRACTSIGNBALANCE) from cust_view_position where d_caldate between to_date('"+datey+"', 'yyyymmdd') and to_date('"+date+"', 'yyyymmdd') and c_custno ='"+custoNo+"'";
			try {
				// 组织架构表
				call = conn.prepareCall(sql);
				// 执行
				ResultSet rs = call.executeQuery();
				while (rs.next()) {
					String val = rs.getString("F_CONTRACTSIGNBALANCE");
					if(0 <= Integer.valueOf(val) && Integer.valueOf(val)< 1000000){
						 level = "1";
					}else if(1000000 <= Integer.valueOf(val) && Integer.valueOf(val) <3000000){
						 level = "2";
					}else if(3000000 <= Integer.valueOf(val) && Integer.valueOf(val) <10000000){
						 level = "3";
					}else if(10000000 <= Integer.valueOf(val) && Integer.valueOf(val) <50000000){
						 level = "4";
					}else if(50000000 <= Integer.valueOf(val)){
						 level = "5";
					}
				}
				//System.out.println("初始等级计算结束："+new Date());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return level;
		}*/
	//2018-11-16修改，原方法是CustomerLevelBycustoNo1(String custoNo,String date,String datey);
	//根据客户编号计算客户等级
	public static String CustomerLevelBycustoNo1(String custoNo){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs =null;
		SimpleDateFormat time=new SimpleDateFormat("yyyyMMdd");
		Calendar calendar = Calendar.getInstance();
		Integer nowYear = calendar.get(Calendar.YEAR);
		Integer month = calendar.get(Calendar.MONTH)+1;
		String startDate = null;
		String endDate = null;
		String level = "1";
		if(month == 1 || month == 2 || month == 3) {
			startDate = String.valueOf(nowYear-1)+"1001";
			endDate = String.valueOf(nowYear-1)+"1231";
		}else if(month == 4 || month == 5 || month == 6) {
			startDate = String.valueOf(nowYear)+"0101";
			endDate = String.valueOf(nowYear)+"0331";
		}else if(month == 7 || month == 8 || month == 9) {
			startDate = String.valueOf(nowYear)+"0401";
			endDate = String.valueOf(nowYear)+"0630";
		}else if(month == 10 || month == 11 || month == 12){
			startDate = String.valueOf(nowYear)+"0701";
			endDate = String.valueOf(nowYear)+"0930";
		}
		//查询用户表已实名的客户信息
		String sql =" with a1 as ( select t1.c_custno,t1.d_capitaldate d_capitaldate,t1.l_proserialno,t1.f_balance,t2.d_expirationdate   " + 
				"  from tcapitalcurrents t1 left join tsaleproject t2 on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' " + 
				"   AND t1.C_FLAG = '1'  union all  select t1.c_custno,t1.d_capitaldate d_capitaldate,  t1.l_proserialno,  t1.f_balance," + 
				"   to_date(to_char(t1.d_setupdate,'yyyymmdd'),'yyyymmdd') d_expirationdate  from tcapitalcurrents t1  left join tsaleproject t2 " + 
				"  on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' AND t1.C_FLAG = '9'  AND to_date(to_char(t2.d_setupdate,'yyyymmdd'),'yyyymmdd') <> t1.d_capitaldate ), " + 
				"  A2 AS ( SELECT c_custno, add_date,  SUM(f_balance) f_balance FROM( SELECT  c_custno, d_capitaldate add_date, f_balance  " + 
				"    FROM A1  UNION ALL SELECT c_custno, d_expirationdate+1,  (-1)*f_balance f_balance    FROM A1)  GROUP BY c_custno,  add_date )  " + 
				"  select c_custno, max(f_balance) from( SELECT   t1.c_custno,  t1.add_date startdate, " + 
				"  lead(t1.add_date-1,1,to_date('29991231','yyyymmdd')) over (partition by t1.c_custno order by t1.add_date asc ) enddate,  " + 
				"  sum(t2.f_balance) f_balance FROM  A2  t1 LEFT JOIN  A2 t2 ON t1.add_date >= t2.add_date " + 
				"  and t1.c_custno = t2.c_custno group by  t1.c_custno,t1.add_date ) " + 
				"  where c_custno ='"+custoNo+"' and  (startdate between to_date('"+startDate+"','yyyymmdd') and to_date('"+endDate+"','yyyymmdd')  " + 
				"   or (startdate < to_date('"+startDate+"','yyyymmdd') and enddate >=to_date('"+startDate+"','yyyymmdd'))) " + 
				"  group by c_custno ";
		try {
			// 组织架构表
			call = conn.prepareCall(sql);
			// 执行
			rs = call.executeQuery();
			List<EsbmmCode> list = esbmmCodeService.getLevel();
			String one = list.get(0).getCodeValue();
			String[] intervalOne = one.split(",");
			String two = list.get(1).getCodeValue();
			String[] intervalTwo = two.split(",");
			String three = list.get(2).getCodeValue();
			String[] intervalThree = three.split(",");
			String four = list.get(3).getCodeValue();
			String[] intervalFour =four.split(",");
			String five = list.get(4).getCodeValue();
			String[] intervalFive = five.split(",");
			while (rs.next()) {
				String val = rs.getString("MAX(F_BALANCE)");
				if(Integer.valueOf(intervalOne[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalOne[1])){
					level = "1";
				}else if(Integer.valueOf(intervalTwo[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalTwo[1])){
					level = "2";
				}else if(Integer.valueOf(intervalThree[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalThree[1])){
					level = "3";
				}else if(Integer.valueOf(intervalFour[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalFour[1])){
					level = "4";
				}else if(Integer.valueOf(intervalFive[0]) <= Integer.valueOf(val) && Integer.valueOf(val)<Integer.valueOf(intervalFive[1])){
					level = "5";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		try {
			if(rs !=null){
				rs.close();
			}
			if(conn !=null){
				conn.close();
			}
			if(call !=null){
			call.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return level;
	}
	
	//积分过期定时
	public static void integralOverdue(){
		System.out.println("定时开始："+new Date());
		//根据有效期查询所有用户积分明细
		List<appIntegralDetails> details = IntegraDetailsDao.selectOverdue();
		Map<String,Object> map = new HashMap<String, Object>();
		if(details.size()>0){
			for(int i=0;i<details.size();i++){
				SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				//当前日期
				String current = df.format(new Date());
				//有效日期
				String valid = df.format(details.get(i).getExpireDate());
				//调运日期比较方法
				int a= compare_date(current, valid);
				//判断返回-1或0 积分到期
				if(a == 1){
					if(details.get(i).getValidIntegral()>0){
						//修改到期积分有效值
						appIntegralDetails newDetails = new appIntegralDetails();
						newDetails.setExpireIntegral(details.get(i).getValidIntegral());
						newDetails.setValidIntegral(0);
						newDetails.setPkId(details.get(i).getPkId());
						IntegraDetailsDao.updateByPrimaryKeySelective(newDetails);
						map.put("userId", details.get(i).getUserId());
						//统计此用户有效积分总和
						long sum = IntegraDetailsDao.selectUserIdSum(map);
						//修改用户积分值
						AppUserInfor appUserInfor = new AppUserInfor();
						appUserInfor.setPkId(details.get(i).getUserId());
						appUserInfor.setIntegral(sum);
						satchAddDao.updatebyuserId(appUserInfor);
						
					}
				}
			}
		}
		System.out.println("定时结束："+new Date());
	}
	
	
	//年份
	public static String getYears(Date d)
	{
		//Date d=new Date();
		GregorianCalendar gc =new GregorianCalendar();
		SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
		gc.setTime(d);
		gc.add(1,+1);
		gc.set(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH),gc.get(Calendar.DATE));

	return sf.format(gc.getTime());
	}
	
	//月份
	public static String getQuarters(Date d)
	{
	GregorianCalendar gc =new GregorianCalendar();
	SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
	gc.setTime(d);
	gc.add(2,+1);
	gc.set(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH),gc.get(Calendar.DATE));

	return sf.format(gc.getTime());
	}
	//比较两日期大小
	 public static int compare_date(String DATE1, String DATE2) {
	       
	       
	        DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm");
	        try {
	            Date dt1 = df.parse(DATE1);
	            Date dt2 = df.parse(DATE2);
	            if (dt1.getTime() > dt2.getTime()) {
	                System.out.println("dt1 在dt2前");
	                return 1;
	            } else if (dt1.getTime() < dt2.getTime()) {
	                System.out.println("dt1在dt2后");
	                return -1;
	            } else {
	                return 0;
	            }
	        } catch (Exception exception) {
	            exception.printStackTrace();
	        }
	        return 0;
	    }
	
}

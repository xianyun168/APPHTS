package com.ciitc.webServices.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 * 文件操作工具类
 * 
 * @author 崔骥才
 * @日期 2014年8月14日-上午9:33:26
 */
public class FileUtil {

	/**
	 * 查看文件是否存在
	 * 
	 * @param filePath
	 *            文件地址
	 * @return true or false
	 */
	public static boolean checkFileExist(String filePath) {
		File file = new File(filePath);
		return file.exists() ? true : false;
	}

	/**
	 * 检查文件夹是否存在
	 * 
	 * @param directoryPath
	 * @return
	 */
	public static boolean checkDirectoryExist(String directoryPath) {
		File file = new File(directoryPath);
		if (!file.exists() && !file.isDirectory()) {
			return false;
		}
		return true;

	}

	/**
	 * 创建目录结果
	 * 
	 * @param path
	 * @return
	 */
	public static boolean createDirectory(String path) {
		boolean flag = false;
		File file = new File(path);
		if (!file.exists()) {
			flag = file.mkdirs();
		} else {
			// Log.getLogger().info("要创建的文件地址[" + path + "]存在！不需要创建！");
		}
		return flag;
	}

	/**
	 * 复制文件
	 * 
	 * @param sourceFilePath
	 *            文件源地址
	 * @param targetFilePath
	 *            新文件地址
	 */
	public static void copyFileToWriteFile(String sourceFilePath, String targetFilePath) {
		Writer writer = null;
		InputStream ins = null;
		try {
			writer = new FileWriter(targetFilePath);
			ins = new FileInputStream(new File(sourceFilePath));
			IOUtils.copy(ins, writer);
		} catch (IOException e) {
			// Log.getLogger().error("复制文件时出错，请检查！", e);
		} finally {
			IOUtils.closeQuietly(writer);
			IOUtils.closeQuietly(ins);
		}
	}

	/**
	 * 复制文件
	 * 
	 * @param sourceFile
	 * @param targetFile
	 */
	public static boolean copyFile(String sourceFile, String targetFile) {
		File srcfile = new File(sourceFile);
		File destfile = new File(targetFile);
		try {
			FileUtils.copyFile(srcfile, destfile);
		} catch (IOException e) {
			 Log.getLogger().error("复制文件["+sourceFile+"]到指定位置["+targetFile+"]时出错，请检查！", e);
			 return false;
		} finally {
			if(null != srcfile){
				srcfile = null;
			}
			if(null != destfile){
				destfile = null;
			}
		}
		Log.getLogger().info("复制文件["+sourceFile+"]到指定位置["+targetFile+"]成功！");
		return true;
	}

	/**
	 * 把文件复到一个文件夹
	 * 
	 * @param file
	 * @param directory
	 */
	public static void copyFileToDirectory(String file, String directory) {
		File srcfile = new File(file);
		File destDir = new File(directory);
		try {
			org.apache.commons.io.FileUtils.copyFileToDirectory(srcfile, destDir);
		} catch (IOException e) {
			// Log.getLogger().error("复制文件时出错，请检查！", e);
		} finally {
			srcfile = null;
			destDir = null;
		}

	}

	/**
	 * 将字符串生成文件
	 * 
	 * @param str
	 *            字符
	 * @param targetFilePath
	 *            生成文件地址
	 * @param encode
	 *            文件编码
	 */
	public static void stringToFile(String str, String targetFilePath, String encode) {
		String strWrite = str;
		try {
			IOUtils.write(strWrite, new FileOutputStream(targetFilePath), encode);
		} catch (IOException e) {
			// Log.getLogger().error("字符串生成文件时出错，请检查！", e);
		} finally {
			strWrite = null;
		}
	}

	/**
	 * 默认文件编码为utf-8的字符串转文件
	 * 
	 * @param str
	 * @param targetFilePath
	 */
	public static void stringToFile(String str, String targetFilePath) {
		stringToFile(str, targetFilePath, "UTF-8");
	}

	/**
	 * 将url地址变为字符串，
	 * 
	 * @param urlPath
	 *            　ＵＲＬ地址
	 * @param encode
	 *            　编码
	 * @return
	 */
	public static String urlToString(String urlPath, String encode) {
		String str = "";
		InputStream ins = null;
		try {
			URL url = new URL(urlPath);
			ins = url.openStream();
			str = IOUtils.toString(ins, encode);
		} catch (MalformedURLException e) {
			// Log.getLogger().error("URL地址格式错误，请检查！", e);
		} catch (IOException e) {
			// Log.getLogger().error("文件流出错，请检查！", e);
		} finally {
			IOUtils.closeQuietly(ins);
		}
		return str;
	}

	/**
	 * 默认编码为utf-8的url转字符串
	 * 
	 * @param urlPath
	 * @return
	 */
	public static String urlToString(String urlPath) {
		return urlToString(urlPath, "UTF-8");
	}

	/**
	 * url生成文件
	 * 
	 * @param urlPath
	 *            　url地址
	 * @param targetFilePath
	 *            　生成文件地址
	 * @param encode
	 *            　编码
	 */
	public static void urlToFile(String urlPath, String targetFilePath, String encode) {
		stringToFile(urlToString(urlPath, encode), targetFilePath, encode);

	}

	/**
	 * 默认utf-8编码的url转文件
	 * 
	 * @param urlPath
	 * @param targetFilePath
	 */
	public static void urlToFile(String urlPath, String targetFilePath) {
		stringToFile(urlToString(urlPath), targetFilePath);
	}

	/**
	 * 删除目录下文件
	 * 
	 * @param directory
	 */
	public static void delDirectoryFiles(String directory) {
		File dir = new File(directory);
		try {
			org.apache.commons.io.FileUtils.cleanDirectory(dir);// 清空目录下的文件
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			dir = null;
		}
	}

	/**
	 * 删除目录和目录下的文件
	 * 
	 * @param directory
	 */
	public static void delDirectoryAndFiles(String directory) {
		File dir = new File(directory);

		try {
			org.apache.commons.io.FileUtils.deleteDirectory(dir);// 删除目录和目录下的文件
		} catch (IOException e) {
			// Log.getLogger().error("删除目录与目录下的文件夹出错，请检查！", e);
		} finally {
			dir = null;
		}
	}

	/**
	 * 目录大小
	 * 
	 * @param directory
	 * @return
	 */
	public static Long getDirectorySize(String directory) {
		File file = new File(directory);
		long size = FileUtils.sizeOfDirectory(file);
		return size;
	}

	/**
	 * 创建文件或修改文件时间,如果不存在新建，如果存在，修改文件修改时间
	 * 
	 * @param path
	 */
	public static void createDirectoryOrUpdateModifyDateTime(String path) {
		File file = new File(path);
		try {
			FileUtils.touch(file);
		} catch (IOException e) {
			// Log.getLogger().error("文件流出错，请检查！", e);
		} finally {
			file = null;
		}

	}

	/**
	 * 文件变为字符串
	 * 
	 * @param file
	 *            　文件全地址
	 * @param encode
	 *            　编码
	 * @return
	 */
	public static String fileToString(String file, String encode) {
		String str = "";
		InputStream ins = null;
		try {
			ins = new FileInputStream(file);
			str = IOUtils.toString(ins, encode);
		} catch (FileNotFoundException e) {
			// Log.getLogger().error("文件没有找到请检查文件地址！", e);
		} catch (IOException e) {
			// Log.getLogger().error("文件流出错，请检查！", e);
		} finally {
			IOUtils.closeQuietly(ins);
		}
		return str;
	}

	/**
	 * 默认编码utf-8的文件变为字符串
	 * 
	 * @param file
	 * @return
	 */
	public static String fileToString(String file) {
		return fileToString(file, "UTF-8");
	}

	/**
	 * 没用使用IOUtils工具类的文件转字符串
	 * 
	 * @param fielPath
	 * @return
	 */
	public static String fileToStringWithOutIOUtils(String fielPath, String encode) {

		StringBuffer sb = null;
		FileInputStream fis = null;
		InputStreamReader isr = null;
		BufferedReader in = null;
		try {
			fis = new FileInputStream(fielPath);
			isr = new InputStreamReader(fis, encode);
			in = new BufferedReader(isr);

			String s = new String();
			sb = new StringBuffer();
			while ((s = in.readLine()) != null) {
				sb.append(s + "\n");
			}
			in.close();
			isr.close();
			fis.close();
		} catch (FileNotFoundException e) {
			// Log.getLogger().error("文件不存在请检查!", e);
		} catch (UnsupportedEncodingException e) {
			// Log.getLogger().error("不被支持的编码,请检查!", e);
		} catch (IOException e) {
			// Log.getLogger().error("IO文件操作错误,请检查", e);
		} finally {
			in = null;
			isr = null;
			fis = null;
		}
		return sb.toString();
	}

	/**
	 * 使用默认编码为utf-8的文件变字符串
	 * 
	 * @param fielPath
	 * @return
	 */
	public static String fileToStringWithOutIOUtils(String fielPath) {
		return fileToStringWithOutIOUtils(fielPath, "UTF-8");
	}

	/**
	 * 传入流生成文件 Mr_Arthur 2013-4-12 上午12:54:50
	 * 
	 * @param is
	 *            传入的流
	 * @param filePath
	 *            生成文件地址
	 * @return
	 */
	public static Integer inputStreamToFile(InputStream is, String filePath) {
		Integer result = 0;
		FileOutputStream out = null;
		try {
			out = new FileOutputStream(filePath);
			byte[] buf = new byte[1024];
			int len;
			while ((len = is.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			result = 1;
		} catch (FileNotFoundException e) {
			result = 0;
			// Log.getLogger().error("文件没有生成,请检查!", e);
		} catch (IOException e) {
			result = 0;
			// Log.getLogger().error("IO操作错误,无法生成文件!", e);
		} finally {
			try {
				is.close();
				out.close();
			} catch (IOException e) {
				result = 0;
				// Log.getLogger().error("IO关闭错误!", e);
			}
		}
		return result;
	}

	/**
	 * reader转文件
	 * 
	 * Mr_Arthur 2013-4-13 上午3:57:33
	 * 
	 * @param reader
	 * @param filePath
	 * @param encode
	 * @return
	 */
	@SuppressWarnings("deprecation")
	public static Integer readerToFile(Reader reader, String filePath, String encode) {
		Integer result = 0;

		try {
			stringToFile(IOUtils.toString(IOUtils.toByteArray(reader, encode)), filePath);
			result = 1;
		} catch (IOException e) {
			result = 0;
			// Log.getLogger().error("reader转文件出错!请检查!", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// Log.getLogger().error("流关闭出错!", e);
				}
			}
		}
		return result;
	}

	public static byte[] getBytesFromFile(File f) throws Exception {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(f);
			byte[] b = new byte[fileInputStream.available()];
			fileInputStream.read(b);
			return b;
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (fileInputStream != null) {
					fileInputStream.close();
				}
			} catch (IOException e) {
			}
		}

	}

	public static void wirteDataToFile(String savePath, byte[] data) throws Exception {
		FileOutputStream fileOutputStream = null;
		try {
			fileOutputStream = new FileOutputStream(savePath);
			fileOutputStream.write(data);
			fileOutputStream.flush();
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				if (fileOutputStream != null) {
					fileOutputStream.close();
				}
			} catch (IOException e) {
			}
		}
	}
	
	/** 
	* 删除单个文件 
	* @param fileName 要删除的文件的文件名 
	* @return 单个文件删除成功返回true，否则返回false 
	*/ 
	public static boolean deleteFile(String fileName) { 
	   File file = new File(fileName); 
	   // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除 
	   if(file.exists() && file.isFile()) { 
	    if(file.delete()) { 
	     System.out.println("删除单个文件" + fileName + "成功！"); 
	       return true; 
	    } else { 
	     System.out.println("删除单个文件" + fileName + "失败！"); 
	       return false; 
	    } 
	   } else { 
	    System.out.println("删除单个文件失败：" + fileName + "不存在！"); 
	      return false; 
	   } 
	} 

	public static void main(String[] args) {
		System.out.println(FileUtil.createDirectory("d:/c/ea/"));
		System.out.println(FileUtil.checkDirectoryExist("d:/c/ea/"));
		// System.out.println(FileUtil.checkDirectoryExist("d:/c"));
	}
}

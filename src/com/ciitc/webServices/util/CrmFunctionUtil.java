package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ciitc.webServices.dao.CPTbrokertreeDao;
import com.ciitc.webServices.dao.CPTfundborkerrelationDao;
import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.dao.CPTtrustcontractReqDao;
import com.ciitc.webServices.dao.CPcfResultDao;
import com.ciitc.webServices.dao.ProductInfoDao;
import com.ciitc.webServices.dao.TsaleProjectInfoDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTbrokertree;
import com.ciitc.webServices.model.CPTfundborkerrelation;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.CPTtrustcontractReq;
import com.ciitc.webServices.model.CPcfResult;
import com.ciitc.webServices.model.ProductInfo;
import com.ciitc.webServices.model.TsaleProjectInfo;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import oracle.jdbc.OracleTypes;

public class CrmFunctionUtil {

	// 理财师和用户关联表
	@SuppressWarnings("rawtypes")
	private CPTfundborkerrelationDao cPTfundborkerrelationDao = (CPTfundborkerrelationDao) SpringContextUtil
			.getBean("CPTfundborkerrelationDao");

	// 组织机构
	@SuppressWarnings("rawtypes")
	private CPTbrokertreeDao cPTbrokertreeDao = (CPTbrokertreeDao) SpringContextUtil.getBean("CPTbrokertreeDao");

	// 理财师
	@SuppressWarnings("rawtypes")
	private CPTfundbrokerDao cpTfundbrokerDao = (CPTfundbrokerDao) SpringContextUtil.getBean("CPTfundbrokerDao");

	// 合同申请
	@SuppressWarnings("rawtypes")
	private CPTtrustcontractReqDao cPTtrustcontractReqDao = (CPTtrustcontractReqDao) SpringContextUtil
			.getBean("CPTtrustcontractReqDao");
	
	// 营销项目表（产品）
	@SuppressWarnings("rawtypes")
	private TsaleProjectInfoDao tsaleProjectInfoDao = (TsaleProjectInfoDao) SpringContextUtil
			.getBean("tsaleProjectInfoDao");
	
	// 产品信息
	@SuppressWarnings("rawtypes")
	private ProductInfoDao productInfoDao = (ProductInfoDao) SpringContextUtil.getBean("productInfoDao");

	// 收益信息
	@SuppressWarnings("rawtypes")
	private CPcfResultDao cPcfResultDao = (CPcfResultDao) SpringContextUtil.getBean("CPcfResultDao");
	
	// 理财师查询
	@SuppressWarnings("unchecked")
	public void getFinancial() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		// 备份，异常恢复使用
		List<CPTfundbroker> queryALL = cpTfundbrokerDao.queryALL();
		
		try {
			cpTfundbrokerDao.deleteAll();
			// 连接耀莱CRM数据库
//			Connection conn = DBUtil.makeConnection();
//			CallableStatement call = null;
			// 理财师信息表
			call = conn.prepareCall(
					"{call PKG_CRM_INTERFACE.SP_QUERYTFUNDBROKERINFO" + "(?,?,?,?,?,?,?,?,?,?,?)}");
			call.setString(1, "");
			call.setString(2, "");
			call.setString(3, "");
			call.setString(4, "");
			call.setString(5, "");
			call.setString(7, "");
			call.setString(8, "1");// 参数下标为1，输入参数类型为varchar（java对应string）
			call.setString(9, "");
			call.setString(11, "");
			call.registerOutParameter(6, OracleTypes.CURSOR);
			call.registerOutParameter(10, OracleTypes.NUMBER);
			// 执行
			call.execute();
			rs = (ResultSet) call.getObject(6);
			//System.out.println(call.getObject(10));
			while (rs.next()) {
				CPTfundbroker cpTfundbroker = new CPTfundbroker();
				cpTfundbroker.setcBrokeraccount(rs.getString("c_brokeraccount"));
				cpTfundbroker.setcUsercode(rs.getString("c_usercode"));
				cpTfundbroker.setcBrokername(rs.getString("c_brokername"));
				cpTfundbroker.setcDuty(rs.getString("c_duty"));
				cpTfundbroker.setcPostcode(rs.getString("c_postcode"));
				cpTfundbroker.setcAddress(rs.getString("c_address"));
				cpTfundbroker.setcPhonecode(rs.getString("c_phonecode"));
				cpTfundbroker.setcMobile(rs.getString("c_mobile"));
				cpTfundbroker.setcEmail(rs.getString("c_email"));
				cpTfundbroker.setcSex(rs.getString("c_sex"));
				cpTfundbroker.setcDeptnodecode(rs.getString("c_deptnodecode"));
				cpTfundbroker.setcDeptnodename(rs.getString("c_deptnodename"));
				/*String c_fatherbroker = getBrokertreeById(rs.getString("c_brokeraccount"));
				cpTfundbroker.setcFartherbroker(c_fatherbroker);*/
				cpTfundbrokerDao.insert(cpTfundbroker);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			cpTfundbrokerDao.deleteAll();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					cpTfundbrokerDao.insert(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		
	}

	// 组织机构
	@SuppressWarnings({ "unused", "unchecked" })
	public void getBrokertree() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		// 备份，异常恢复使用
		List<CPTbrokertree> queryALL = cPTbrokertreeDao.queryALL();
		try {
			cPTbrokertreeDao.deleteAll();
			
			// 组织架构表
			call = conn.prepareCall("select * from TBROKERTREE");
			// 执行
			rs = call.executeQuery();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			while (rs.next()) {
				CPTbrokertree cPTbrokertree = new CPTbrokertree();
				cPTbrokertree.setcNodecode(rs.getString("c_nodecode"));
				cPTbrokertree.setcParentcode(rs.getString("c_parentcode"));
				cPTbrokertree.setcNodetype(rs.getString("c_nodetype"));
				cPTbrokertree.setcNodevalue(rs.getString("c_nodevalue"));
				cPTbrokertree.setcDesc(rs.getString("c_desc"));
				cPTbrokertree.setcBranch(rs.getString("c_branch"));
				cPTbrokertree.setcAreacode(rs.getString("c_areacode"));
				cPTbrokertree.setcAreaexcept(rs.getString("c_areaexcept"));
				cPTbrokertree.setcDepttype(rs.getString("c_depttype"));
				cPTbrokertree.setcDefaultbroker(rs.getString("c_defaultbroker"));
				cPTbrokertree.setcLimitagencyno(rs.getString("c_limitagencyno"));
				cPTbrokertree.setcLeaf(rs.getString("c_leaf"));
				cPTbrokertree.setcIsreserve(rs.getString("c_isreserve"));
				cPTbrokertree.setcAddress(rs.getString("c_address"));
				if (null != rs.getString("d_lastmodify") && !"".equals(rs.getString("d_lastmodify"))) {
					cPTbrokertree.setdLastmodify(sdf.parse(rs.getString("d_lastmodify")));
				}
				cPTbrokertree.setcIsuse(rs.getString("c_isuse"));
				cPTbrokertree.setcNetnotemp(rs.getString("c_netnotemp"));
				;
				cPTbrokertree.setcUfaccosysnetno(rs.getString("c_ufaccosysnetno"));
				cPTbrokertree.setcDeptplacetype(rs.getString("c_deptplacetype"));
				cPTbrokertree.setcBranchid(rs.getString("c_branchid"));
				cPTbrokertree.setcParentbranchid(rs.getString("c_parentbranchid"));
				cPTbrokertree.setcIssalary(rs.getString("c_issalary"));
				cPTbrokertree.setcIsreservecount(rs.getString("c_isreservecount"));
				if (null != rs.getString("d_startdate") && !"".equals(rs.getString("d_startdate"))) {
					cPTbrokertree.setdStartdate(sdf.parse(rs.getString("d_startdate")));
				}
				cPTbrokertree.setcDsnodecode(rs.getString("c_dsnodecode"));
				cPTbrokertree.setcOrder(rs.getString("c_order"));
				cPTbrokertree.setcTenantid(rs.getString("c_tenantid"));
				cPTbrokertree.setcIsrealdepartment(rs.getString("c_isrealdepartment"));
				cPTbrokertree.setfRatio(rs.getBigDecimal("f_ratio"));
				cPTbrokertreeDao.insert(cPTbrokertree);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			cPTbrokertreeDao.deleteAll();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					cPTbrokertreeDao.insert(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
			
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	
	//根据理财师的查询理财师的上级id
	public String getBrokertreeById(String brokerAccount) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String fBrokertree = null;
		
		try {
			call = conn.prepareCall("select * from TFUNDBROKER where C_BROKERACCOUNT = '" + brokerAccount+"' ");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				fBrokertree = rs.getString("C_FATHERBROKER");//上级理财师
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return fBrokertree;
	}
	//根据理财师的用户名查询理财师的id
	public String getBrokertreeByCode(String brokerAccount) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String fBrokertree = null;
		
		try {
			call = conn.prepareCall("select * from TFUNDBROKER where C_USERCODE = '" + brokerAccount+"' ");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				fBrokertree = rs.getString("C_BROKERACCOUNT");//上级理财师
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return fBrokertree;
	}

	// 理财师与用户关联
	public void getRelation() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		// 备份，异常恢复使用
		@SuppressWarnings("unchecked")
		List<CPTfundborkerrelation> queryALL = cPTfundborkerrelationDao.queryALL();
		try {
			cPTfundborkerrelationDao.deleteAll();
			
			// 用户和理财师关联表
			call = conn.prepareCall("{call PKG_CRM_INTERFACE.sp_querytfundbrokerrelation" + "(?,?,?)}");
			call.setString(1, "");
			call.setString(2, "");// 参数下标为1，输入参数类型为varchar（java对应string）

			call.registerOutParameter(3, OracleTypes.CURSOR);
			// call.registerOutParameter(10, OracleTypes.NUMBER);
			// 执行
			call.execute();
			rs = (ResultSet) call.getObject(3);
			// System.out.println(call.getObject(10));
			while (rs.next()) {
				CPTfundborkerrelation cpTfundborkerrelation = new CPTfundborkerrelation();
				cpTfundborkerrelation.setcBrokeraccount(rs.getString("c_brokeraccount"));
				cpTfundborkerrelation.setcCustno(rs.getString("c_custno"));
				cpTfundborkerrelation.setcFundacco(rs.getString("c_fundacco"));
				cpTfundborkerrelation.setcRelationType(rs.getString("c_relationtype"));
				cpTfundborkerrelation.setcIsmain(rs.getString("c_ismain"));
				cPTfundborkerrelationDao.insert(cpTfundborkerrelation);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			cPTfundborkerrelationDao.deleteAll();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					cPTfundborkerrelationDao.insert(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}


	/**
	 * 获取客户ID
	 */
	public AppUserInfor queryByCustno(AppUserInfor appUserInfor) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String PhoneNumber = appUserInfor.getPhoneNumber();
		try {
			if (PhoneNumber != null) {
				call = conn.prepareCall("select C_CUSTNO from TUNREGISTER where C_MOBILENO = '" + PhoneNumber + "'");
			}
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));// 客户ID
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return appUserInfor;
	}

	/**
	 * 根据手机号和证件号查CRM系统
	 */
	public AppUserInfor queryByAll(AppUserInfor appUserInfor) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String IdcardNumber = appUserInfor.getIdcardNumber();
		String PhoneNumber = appUserInfor.getPhoneNumber();
		try {
			if (IdcardNumber != null || PhoneNumber != null) {
				call = conn.prepareCall("select * from TUNREGISTER where C_MOBILENO = '" + PhoneNumber
						+ "'  and C_IDENTITYNO = '" + IdcardNumber + "' ");
			}
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				appUserInfor.setRemark2(rs.getString("C_IDENTITYNO"));// 证件号码
				appUserInfor.setRemark3(rs.getString("C_MOBILENO"));// 手机号
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return appUserInfor;
	}

	/**
	 * 根据手机号查CRM客户ID
	 */
	public AppUserInfor queryFicinalId(AppUserInfor appUserInfor) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;		
		String responsecustno = appUserInfor.getCustomerId();
		try {
			call = conn.prepareCall("{call PKG_CRM_INTERFACE.sp_querytfundbrokerrelation(?,?,?)}");
			call.setString(1, responsecustno);
			call.setString(2, "");
			call.registerOutParameter(3, OracleTypes.CURSOR);// 返回游标
			// 执行
			call.execute();
			ResultSet resultSet = (ResultSet) call.getObject(3);
			String ficinalId = null;
			while (resultSet.next()) {
				ficinalId = resultSet.getString("c_brokeraccount");
			}
			//System.out.println("ficinalId: " + ficinalId);
			appUserInfor.setFinancialId(ficinalId);
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return appUserInfor;
	}

	/**
	 * 获取客户ID
	 */
	public AppUserInfor queryByStatus(AppUserInfor appUserInfor) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		CallableStatement call1 = null;
		ResultSet rs = null;
		ResultSet rss = null;
		// 获取客户ID
		String custNo = "";
		// CRM库实名状态
		String status = "";
		String fincalId="";
		String PhoneNumber = appUserInfor.getPhoneNumber();
		try {
			if (PhoneNumber != null) {
				call = conn.prepareCall("select * from TUNREGISTER where C_MOBILENO = '" + PhoneNumber + "' and C_ISDELETE='0'");
			}
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				custNo = (rs.getString("C_CUSTNO"));// 客户ID
				appUserInfor.setIdcardType(rs.getString("C_IDENTITYTYPE")); //证件类型
				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO")); //证件号码
				appUserInfor.setCreateDate(rs.getDate("D_CREATEDATE")); ;//创建时间
				appUserInfor.setUserName(rs.getString("C_CUSTNAME")); //客户名称
				appUserInfor.setCustomerSex(rs.getString("C_SEX")); //性别
				appUserInfor.setcIsDelete(rs.getString("C_ISDELETE"));
				String  birthday = rs.getString("C_BIRTHDAY");//生日
  				if(null != birthday && !"".equals(birthday)) {
  					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
  					Date customerBirthday = null;
  					try {
  						customerBirthday = sdf.parse(birthday);
  					} catch (ParseException e) {
  						// TODO Auto-generated catch block
  						e.printStackTrace();
  					}
  	  				appUserInfor.setCustomerBirthday(customerBirthday);
  				}
			}
			if(null !=custNo && !"".equals(custNo) ) {
				call1 = conn.prepareCall("select *  from TCUSTREGISTERREQ where C_CUSTNO = '" + custNo + "'");
				// 执行
				rss = call1.executeQuery();
				while (rss.next()) {
					status = (rss.getString("C_STATUS"));// 状态
					fincalId=(rss.getString("C_BROKERACCOUNT"));//理财师ID
				}
			}
			appUserInfor.setFinancialId(fincalId);
			appUserInfor.setCustomerId(custNo);
			if ("2".equals(status)) {
				appUserInfor.setNameStatus("1");
			}else if("1".equals(status)){
				appUserInfor.setNameStatus("2");
			}else if("3".equals(status)){
				appUserInfor.setNameStatus("3");
			}else {
				appUserInfor.setNameStatus("0");
			}
	
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if( null != rss) {
					rss.close();
					}
					if(null != call1) {
						call1.close();
					}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return appUserInfor;
	}
	
	// 调用产品接口时---更新合同申请表
	public void getHTInfo1() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		// 备份，异常恢复使用
		@SuppressWarnings("unchecked")
		List<CPTtrustcontractReq> queryALL = cPTtrustcontractReqDao.queryALL();
		try {
			cPTtrustcontractReqDao.deleteAll();

			// 合同申请表
			call = conn.prepareCall("select * from TTRUSTCONTRACT_REQ");
			// 执行
			rs = call.executeQuery();
			// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
			// HH:mm:ss");
			while (rs.next()) {

				CPTtrustcontractReq cPTtrustcontractReq = new CPTtrustcontractReq();
				// id
				cPTtrustcontractReq.setlSerialno(rs.getLong("L_SERIALNO"));
				// 序列号
				cPTtrustcontractReq.setlProserialno(rs.getLong("L_PROSERIALNO"));
				// 合同号
				cPTtrustcontractReq.setcTrustcontractid(rs.getString("C_TRUSTCONTRACTID"));
				// 产品编号
				cPTtrustcontractReq.setcFundcode(rs.getString("C_FUNDCODE"));
				// 客户编号
				cPTtrustcontractReq.setcCustno(rs.getString("C_CUSTNO"));
				// 合同签订金额
				cPTtrustcontractReq.setfContractsignbalance(rs.getDouble("F_CONTRACTSIGNBALANCE"));
				// 手机号
				cPTtrustcontractReq.setcMobileno(rs.getString("C_MOBILENO"));
				// 合同签署日期
				cPTtrustcontractReq.setdContractsigndate(rs.getDate("D_CONTRACTSIGNDATE"));
				// 合同结束日期
				cPTtrustcontractReq.setdContractenddate(rs.getDate("D_CONTRACTENDDATE"));
				// 如果合同结束时间不为空，将值赋给客户实际赎回时间与客户默认赎回时间
				if (rs.getDate("D_CONTRACTENDDATE") != null && !"".equals(rs.getDate("D_CONTRACTENDDATE"))) {
					cPTtrustcontractReq.setActualDate(rs.getDate("D_CONTRACTENDDATE"));
					cPTtrustcontractReq.setDefaultDate(rs.getDate("D_CONTRACTENDDATE"));
				} else {
					// 如果合同结束时间为空，将产品的默认结束时间赋给客户实际赎回时间与客户默认赎回时间
					ProductInfo productInfo = (ProductInfo)productInfoDao.queryById(rs.getString("L_PROSERIALNO"));
					if(productInfo != null) {
						cPTtrustcontractReq.setActualDate(productInfo.getDefaultDate());
						cPTtrustcontractReq.setDefaultDate(productInfo.getDefaultDate());
					}
				}
				// 审核状态
				cPTtrustcontractReq.setcStatus(rs.getString("C_STATUS"));
				cPTtrustcontractReqDao.insertSelective(cPTtrustcontractReq);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			cPTtrustcontractReqDao.deleteAll();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					cPTtrustcontractReqDao.insert(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	
	// 调用收益接口时---更新合同申请表
	public void getHTInfo() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		// 备份，异常恢复使用
		@SuppressWarnings("unchecked")
		List<CPTtrustcontractReq> queryALL = cPTtrustcontractReqDao.queryALL();
		try {
			cPTtrustcontractReqDao.deleteAll();
			
			// 合同申请表
			call = conn.prepareCall("select * from TTRUSTCONTRACT_REQ");
			// 执行
			rs = call.executeQuery();
			// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
			// HH:mm:ss");
			while (rs.next()) {

				CPTtrustcontractReq cPTtrustcontractReq = new CPTtrustcontractReq();
				// id
				cPTtrustcontractReq.setlSerialno(rs.getLong("L_SERIALNO"));
				// 序列号
				cPTtrustcontractReq.setlProserialno(rs.getLong("L_PROSERIALNO"));
				// 合同号
				cPTtrustcontractReq.setcTrustcontractid(rs.getString("C_TRUSTCONTRACTID"));
				// 产品编号
				cPTtrustcontractReq.setcFundcode(rs.getString("C_FUNDCODE"));
				// 客户编号
				cPTtrustcontractReq.setcCustno(rs.getString("C_CUSTNO"));
				// 合同签订金额
				cPTtrustcontractReq.setfContractsignbalance(rs.getDouble("F_CONTRACTSIGNBALANCE"));
				// 手机号
				cPTtrustcontractReq.setcMobileno(rs.getString("C_MOBILENO"));
				// 合同签署日期
				cPTtrustcontractReq.setdContractsigndate(rs.getDate("D_CONTRACTSIGNDATE"));
				// 合同结束日期
				cPTtrustcontractReq.setdContractenddate(rs.getDate("D_CONTRACTENDDATE"));
				// 如果合同结束时间不为空，将值赋给客户实际赎回时间与客户默认赎回时间
				if (rs.getDate("D_CONTRACTENDDATE") != null && !"".equals(rs.getDate("D_CONTRACTENDDATE"))) {
					cPTtrustcontractReq.setActualDate(rs.getDate("D_CONTRACTENDDATE"));
					cPTtrustcontractReq.setDefaultDate(rs.getDate("D_CONTRACTENDDATE"));
				} else {
					// 如果合同结束时间为空，将产品的默认结束时间赋给客户实际赎回时间与客户默认赎回时间
					TsaleProjectInfo productInfo = (TsaleProjectInfo)tsaleProjectInfoDao.selectByPrimaryKey(rs.getString("L_PROSERIALNO"));
					if(productInfo != null) {
						cPTtrustcontractReq.setActualDate(productInfo.getDefaultDate());
						cPTtrustcontractReq.setDefaultDate(productInfo.getDefaultDate());
					}
				}
				// 审核状态
				cPTtrustcontractReq.setcStatus(rs.getString("C_STATUS"));
				cPTtrustcontractReqDao.insertSelective(cPTtrustcontractReq);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			cPTtrustcontractReqDao.deleteAll();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					cPTtrustcontractReqDao.insert(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	
	
	/**
	 * 1查询耀莱数据库；2更新本地数据库
	 * 
	 * @author Bridge
	 * @date 2018年6月5日 下午4:09:25
	 */
	public void updateTsaleProjectInfo() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		// 备份，异常恢复使用
		List<TsaleProjectInfo> queryALL = tsaleProjectInfoDao.queryALL();
		// 连接耀莱CRM数据库
		try {
			// 清空数据表
			tsaleProjectInfoDao.deleteALL();
			//Connection conn = DBUtil.makeConnection();
			// 营销项目表C_SALERIGHTFLAG ='1'销售状态“在售” C_STATUS = '2'项目状态“发行中”
			call = conn.prepareCall("SELECT  T1.L_PROSERIALNO,T1.C_NAME as C_NAME,  L1.C_CAPTION||REPLACE(L2.C_CAPTION,'私募产品','私募基金') as C_PROJECTTYPE,  T4.C_NAME as L_MANDATORID,  T3.C_TRUSTEENAME as C_TRUSTEENAME,  T5.C_OUTSOURCEDEALER as C_OUTSOURCEDEALER,   T6.F_MAXPRODSHARES as F_SCALE,  T3.L_TIMELIMIT||'个月' as C_DEADLINE,  T1.F_PERMINBALANCE as F_PERMINBALANCE,  T7.C_CONTENT as C_CONTENT,  T8.c_incdisdate as C_INCOMEDISTRIBUTION,  T1.C_RAISEBANK as C_RAISEBANK,  T1.C_RAISENAME as C_RAISENAME,   T1.C_RAISEACCO as C_RAISEACCO,  CASE WHEN L3.C_CAPTION IS NOT NULL   THEN '本基金属于（'||L3.C_CAPTION||'）'||(case when L3.C_CAPTION = 'R1' then '低风险' when L3.C_CAPTION = 'R2' then '中低风险' when L3.C_CAPTION = 'R3' then '中风险' when L3.C_CAPTION = 'R4' then '中高风险' when L3.C_CAPTION = 'R5' then '高风险' end) ||'投资品种，适合风险识别、评估、承受能力为C'||REPLACE(L3.C_CAPTION,'R','')||'及以上的合格投资者。'    ELSE NULL END as C_RISKLEVEL,  t1.C_WAYFUND as C_WAYFUND,	  t1.D_EXPIRATIONDATE,	  t1.C_RISKCONTROL FROM TSALEPROJECT T1 LEFT JOIN tsaleprofundrelation T2  ON T1.L_PROSERIALNO = T2.L_PROSERIALNO LEFT JOIN TFUNDINFO T3 ON T2.C_FUNDCODE = T3.C_FUNDCODE LEFT JOIN (select * from tdictionary where l_keyno = '1404') L1  ON T3.C_OPERATEWAY = L1.C_KEYVALUE LEFT JOIN (select * from tdictionary where l_keyno = '3189') L2  ON T3.C_PDTTYPE = L2.C_KEYVALUE LEFT JOIN  TPDTMANDATOR T4 ON T3.l_mandatorid = T4.l_mandatorid LEFT JOIN TPDTFUTUASSMGR T5 ON T3.C_FUNDCODE = T5.C_FUNDCODE LEFT JOIN TPDTARLIMIT T6 ON T3.C_FUNDCODE = T6.C_FUNDCODE LEFT JOIN (SELECT * FROM TFUNDINFOVERTICAL WHERE C_PROCODE = 'C_BENCHMARK')T7 ON T3.C_FUNDCODE = T7.C_FUNDCODE left join  (SELECT  c_fundcode,        nvl(t1.c_incdisdate,t2.c_caption) c_incdisdate FROM TPDTTRUST t1 left join (select * from tdictionary where l_keyno = '1814') t2 on t1.c_syfppd = t2.c_keyvalue)T8 ON T3.C_FUNDCODE = t8.C_FUNDCODE LEFT JOIN (select * from tdictionary where l_keyno = '1408') L3  ON trim(T3.C_RISKLEVEL) = L3.C_KEYVALUE ");
			// 执行
			rs = call.executeQuery();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			while (rs.next()) {
				TsaleProjectInfo tsaleProjectInfo = new TsaleProjectInfo();
				// ID
				tsaleProjectInfo.setPkId(rs.getString("L_PROSERIALNO"));
				// 项目名称
				tsaleProjectInfo.setProductname(rs.getString("C_NAME"));
				// 产品类型
				tsaleProjectInfo.setPruducttype(rs.getString("C_PROJECTTYPE"));
				// 管理人
				tsaleProjectInfo.setProductmanager(rs.getString("L_MANDATORID"));
				// 托管人
				tsaleProjectInfo.setProductcustodian(rs.getString("C_TRUSTEENAME"));
				// 外包服务商
				tsaleProjectInfo.setOutsourcingserviceprovider(rs.getString("C_OUTSOURCEDEALER"));
				// 募集规模,发行规模
				tsaleProjectInfo.setScale(rs.getString("F_SCALE"));
				// 存续期限,项目期限
				tsaleProjectInfo.setDeadline(rs.getString("C_DEADLINE"));
				tsaleProjectInfo.setRemark1(rs.getString("C_DEADLINE").replaceAll("个月", ""));
				// 认购起点,个人投资起点
				tsaleProjectInfo.setPerminbalance(rs.getString("F_PERMINBALANCE"));
				// 业绩比较基准,预期年化收益率
				tsaleProjectInfo.setYield(rs.getString("C_CONTENT"));
				// 分红方式,收益分配方式
				tsaleProjectInfo.setBonustype(rs.getString("C_INCOMEDISTRIBUTION"));
				// 募集户名
				tsaleProjectInfo.setRaisename(rs.getString("C_RAISENAME"));
				// 募集银行
				tsaleProjectInfo.setRaisebank(rs.getString("C_RAISEBANK"));
				// 募集账号
				tsaleProjectInfo.setRaiseacco(rs.getString("C_RAISEACCO"));
				// 产品投向
				tsaleProjectInfo.setProductthrowup(rs.getString("C_WAYFUND"));
				// 风险收益特征
				tsaleProjectInfo.setRiskincome(rs.getString("C_RISKLEVEL"));
				// 产品费率,此字段在CRM系统中没有
				// productinfo.setRate(rate);
				// 产品实际到期时间
				if (rs.getString("D_EXPIRATIONDATE") != null) {
					tsaleProjectInfo.setActualDate(simpleDateFormat.parse(rs.getString("D_EXPIRATIONDATE")));
				}
				// 产品默认到期时间
				if (rs.getString("D_EXPIRATIONDATE") != null) {
					tsaleProjectInfo.setDefaultDate(simpleDateFormat.parse(rs.getString("D_EXPIRATIONDATE")));
				}
				tsaleProjectInfoDao.insertSelective(tsaleProjectInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			tsaleProjectInfoDao.deleteALL();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					tsaleProjectInfoDao.insertSelective(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}

	
	
	
	// 收益表
	public void getProfit() {
		Connection conn1 = DBUtil.makeOds();
		CallableStatement call = null;
		ResultSet rs = null;
		// 备份，异常恢复使用
		@SuppressWarnings("unchecked")
		List<CPcfResult> queryALL = cPcfResultDao.queryALL();
		try {
			cPcfResultDao.deleteAll();
			
			// 合同申请表
			call = conn1.prepareCall("select * from CF_RESULT");
			// 执行
			rs = call.executeQuery();
			// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd
			// HH:mm:ss");
			while (rs.next()) {
				CPcfResult cPcfResult = new CPcfResult();
				// id
				cPcfResult.setCF_PK(rs.getInt("CF_PK"));
				// 返息日期
				cPcfResult.setData_date(rs.getDate("DATA_DATE"));
				// 客户编号
				cPcfResult.setC_CUSTNO(rs.getString("C_CUSTNO"));
				// 产品编号
				cPcfResult.setC_FUNDINFO(rs.getString("C_FUNDINFO"));
				// 营销项目id
				cPcfResult.setL_PROSERIALNO(rs.getInt("L_PROSERIALNO"));
				// 现金流类型
				cPcfResult.setFin_element_ID(rs.getString("FIN_ELEMENT_ID"));
				// 资产负债类型
				cPcfResult.setID_Type(rs.getString("ID_TYPE"));
				// 现金流金额
				cPcfResult.setAmount(rs.getDouble("AMOUNT"));
				// 确认标识（付息确认）
				cPcfResult.setConfirm_Flag(rs.getString("CONFIRM_FLAG"));
				// 本期开始计息日期
				cPcfResult.setStart_Date(rs.getDate("START_DATE"));
				// 合同ID
				cPcfResult.setContract_Id(rs.getString("CONTRACT_ID"));
				// 收益确认标识....与付息相同
				// cPcfResult.setPROFIT_STATUS(rs.getString("CONFIRM_FLAG"));
				//客户名
				cPcfResult.setC_custnm(rs.getString("C_CUSTNM"));
				//客户手机号
				cPcfResult.setC_mobile(rs.getString("C_MOBILE"));
				//产品名称
				cPcfResult.setC_fundnm(rs.getString("C_FUNDNM"));
				//营销项目名称
				cPcfResult.setL_proserialnm(rs.getString("L_PROSERIALNM"));
				
				cPcfResultDao.insertSelective(cPcfResult);
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			cPcfResultDao.deleteAll();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					cPcfResultDao.insertSelective(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn1){
					conn1.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}

	// 修改收益表状态位
	public void getUpdateProfit(String flag, String id) {
		Connection conn1 = DBUtil.makeOds();
		CallableStatement call = null;
		ResultSet rs = null;
		try {
			Statement state = conn1.createStatement();// 容器
			// 根据收益ID修改确认状态
			String sql = "UPDATE CF_RESULT SET CONFIRM_FLAG = '" + flag + "' WHERE CF_PK = '" + id + "' "; // SQL语句
			state.executeUpdate(sql);// 将sql语句上传至数据库执行
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn1){
					conn1.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}

	// 修改收益表收益
	public void getUpdate(double money, int id) {
		Connection conn1 = DBUtil.makeOds();
		CallableStatement call = null;
		ResultSet rs = null;

		try {
			Statement state = conn1.createStatement();// 容器
			// 根据收益ID修改确认状态
			String sql = "UPDATE CF_RESULT SET AMOUNT = '" + money + "' WHERE CF_PK = '" + id + "' "; // SQL语句
			state.executeUpdate(sql);// 将sql语句上传至数据库执行
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn1){
					conn1.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	//查询理财师
	public List<String> brokeraccount() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		List<String> list = new ArrayList<String>();
		try {
			call = conn.prepareCall("select C_BROKERACCOUNT from tfundbroker");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				list.add(rs.getString("C_BROKERACCOUNT"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return list;
	}
	//根据项目编号查询项目名称
	public String getProserialname(String proserialno) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		String proserialname = "";
		try {
			call = conn.prepareCall("select C_NAME,C_SHORTNAME from tsaleproject where L_PROSERIALNO= '"+proserialno+"'");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				proserialname = rs.getString("C_SHORTNAME");
				if(null ==proserialname &&"".equals(proserialname)) {
					proserialname = rs.getString("C_NAME");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return proserialname;
	}
	//根据理财师编号查询理财师用户名
	public String getUserCode(String financialId) {
		// 连接耀莱CRM数据库
			Connection conn = DBUtil.makeConnection();
			CallableStatement call = null;
			ResultSet rs = null;
			String userCode = "";
			try {
				call = conn.prepareCall("select C_USERCODE from tfundbroker where C_BROKERACCOUNT = '"+financialId+"'");
				// 执行
				rs = call.executeQuery();
				while (rs.next()) {
					userCode = rs.getString("C_USERCODE");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}finally {
				try {
					if( null != rs) {
					rs.close();
					}
					if(null != call) {
						call.close();
					}
					if(null != conn){
						conn.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
		}
	return userCode;
	}
}

package com.ciitc.webServices.util;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

/**
 * XML解析工具类
 * 
 * @author Mr_Arthur 2013-2-21 下午3:13:11
 * 
 */
public class Dom4jUtil {
	/**
	 * 创建XML文档设置编码
	 * 
	 * @param encoding
	 * @return
	 */
	public static Document createDocument(String encoding) {
		Document doc = DocumentHelper.createDocument();
		doc.setXMLEncoding(encoding);
		return doc;
	}

	/**
	 * 创建XML文档
	 * 
	 * @return
	 */
	public static Document createDocument() {
		Document doc = DocumentHelper.createDocument();
		doc.setXMLEncoding("UTF-8");
		return doc;
	}

	/**
	 * 通过文件名加载XML文档
	 * 
	 * @param filePath
	 * @return
	 */
	public static Document getDocument(String filePath) {
		SAXReader reader = new SAXReader();
		Document document = null;
		if (FileUtil.checkFileExist(filePath)) {
			try {
				document = reader.read(new File(filePath));
			} catch (DocumentException e) {
				//Log.getLogger().error("读取XML文档失败,请检查XML文档格式!", e);
			}
		} else {
			//Log.getLogger().error("你要加载的XML文件{" + filePath + "}不存在!请检查后在试!");
		}
		return document;
	}

	/**
	 * 得到xml文档的字符串
	 * 
	 * @param filePath
	 * @return
	 */
	public static String getDocumentString(String filePath) {
		return getDocument(filePath).asXML();
	}

	/**
	 * 取得root节点
	 * 
	 * @param document
	 * @return
	 */
	public static Element getRootElement(Document document) {
		return document.getRootElement();
	}

	/**
	 * 取得root节点的所有属性
	 * 
	 * @param rootElement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Attribute> getRootAttrs(Element rootElement) {
		if (!rootElement.isRootElement()) {
			//Log.getLogger().equals("此方法只取得root元素的所有属性,你要取得属性的元素节点不是root节点,请用其它方法!");
			return null;
		}
		List<Attribute> list = new ArrayList<Attribute>();
		for (Iterator<Attribute> i = rootElement.attributeIterator(); i.hasNext();) {
			Attribute attr = i.next();
			list.add(attr);
		}
		if (list.size() <= 0) {
			list = null;
		}
		return list;
	}

	/**
	 * 取得根元素的Map值
	 * 
	 * @param rootElement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, String> getRootAttrsMap(Element rootElement) {
		if (!rootElement.isRootElement()) {
			//Log.getLogger().equals("此方法只取得root元素的所有属性,你要取得属性的元素节点不是root节点,请用其它方法!");
			return null;
		}
		Map<String, String> map = new HashMap<String, String>();
		for (Iterator<Attribute> i = rootElement.attributeIterator(); i.hasNext();) {
			Attribute attr = i.next();
			map.put(attr.getName(), attr.getValue());
		}
		if (map.size() <= 0) {
			map = null;
		}
		return map;
	}

	/**
	 * 取得子元素集合
	 * 
	 * @param parentElement
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Element> getChildElement(Element parentElement) {
		return parentElement.elements();
	}

	/**
	 * 返回指定名字的元素集合
	 * 
	 * @param parentElement
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Element> getChildElement(Element parentElement, String name) {
		return parentElement.elements(name);
	}

	/**
	 * 根据文件地址与节点名字取得节点
	 * 
	 * @param xmlFilePath
	 * @param nodeName
	 * @return
	 */
	public static Node getNode(String xmlFilePath, String nodeName) {
		Node n = getRootElement(getDocument(xmlFilePath)).selectSingleNode("//" + nodeName);
		if (null != n) {
			return n;
		} else {
			return null;
		}
	}

	/**
	 * 根据文件地址与节点名字取得节点的Text值
	 * 
	 * @param xmlFilePath
	 * @param nodeName
	 * @return
	 */
	public static String getNodeText(String xmlFilePath, String nodeName) {
		Node n = getNode(xmlFilePath, nodeName);
		if (null != n) {
			return n.getText();
		} else {
			return null;
		}
	}

	/**
	 * 根据文件地址与节点名字取得节点的指定名字属性的值
	 * 
	 * @param xmlFilePath
	 * @param nodeName
	 * @param attrName
	 * @return
	 */
	public static String getNodeAttribute(String xmlFilePath, String nodeName, String attrName) {
		Node n = getNode(xmlFilePath, nodeName);
		if (null != n) {
			String name = "@" + attrName;
			return n.valueOf(name);
		} else {
			return null;
		}
	}

	/**
	 * 取得指定名称的元素集合
	 * 
	 * @param xmlFilePath
	 * @param nodeName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<Node> getNodeList(String xmlFilePath, String nodeName) {
		List<Node> list = new ArrayList<Node>();
		list = getRootElement(getDocument(xmlFilePath)).selectNodes("//" + nodeName);
		if (list.size() > 0) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * 根据文件地址,节点名称,属性名称,取得有此属性名称的所有节点的此属性值
	 * 
	 * @param xmlFilePath
	 * @param nodeName
	 * @param attrName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<String> getNodesAttribute(String xmlFilePath, String nodeName, String attrName) {
		List<String> list = new ArrayList<String>();
		List<Node> listNode = new ArrayList<Node>();
		listNode = getRootElement(getDocument(xmlFilePath)).selectNodes("//" + nodeName + "[@" + attrName + "]");
		if (listNode.size() > 0) {
			for (Node n : listNode) {
				list.add(n.valueOf("@" + attrName));
			}
		}
		if (listNode.size() > 0 && list.size() > 0) {
			return list;
		} else {
			return null;
		}
	}
	/**
	 * 根据xml报文,节点名称,属性名称,取得有此属性名称的所有节点的此属性值
	 * 
	 * @param xml
	 * @param nodeName
	 * @param attrName
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<String> getNodesAttributes(String xml, String nodeName, String attrName) {
		List<String> list = new ArrayList<String>();
		List<Node> listNode = new ArrayList<Node>();
		
		try {
			listNode = getRootElement(DocumentHelper.parseText(xml)).selectNodes("//" + nodeName + "[@" + attrName + "]");
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (listNode.size() > 0) {
			for (Node n : listNode) {
				list.add(n.valueOf("@" + attrName));
			}
		}
		if (listNode.size() > 0 && list.size() > 0) {
			return list;
		} else {
			return null;
		}
	}

	/**
	 * 根据文件地址,节点名称,属性名称,取得指定位置有此属性名称的节点的此属性值
	 * 
	 * @param xmlFilePath
	 * @param nodeName
	 * @param attrName
	 * @param index
	 *            从0开始
	 * @return
	 */
	public static String getNodeAttribute(String xmlFilePath, String nodeName, String attrName, int index) {
		List<String> list = getNodesAttribute(xmlFilePath, nodeName, attrName);
		if (null != list) {
			if ((list.size() - 1) >= index) {
				return list.get(index);
			} else {
			//	Log.getLogger().error("您传入的索引值{" + index + "}大于所查询出的结果集合,请检查后在试!");
			}
		}
		return null;
	}

	/**
	 * 根据文件名,节点名key与value条件值,查询指定节点指定属性名的值
	 * 
	 * @param xmlFilePath
	 * @param nodeName
	 * @param attrName
	 * @param key
	 * @param value
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getNodeAttribute(String xmlFilePath, String nodeName, String attrName, String key, String value) {
		List<Node> listNode = new ArrayList<Node>();
		listNode = getRootElement(getDocument(xmlFilePath)).selectNodes("//" + nodeName + "[@" + key + "=\"" + value + "\"]");
		if (listNode.size() > 0)
			return listNode.get(0).valueOf("@" + attrName);
		else
			return null;
	}

	/**
	 * 生成漂亮的xml文件通过document
	 * 
	 * @param outFilePath
	 * @param document
	 */
	public static void createPrettyXmlFile(String outFilePath, Document document) {
		XMLWriter writer = null;
		try {
			writer = new XMLWriter(new FileWriter(outFilePath), OutputFormat.createPrettyPrint());
			writer.write(document);
			writer.close();
		} catch (IOException e) {
		//	Log.getLogger().error("生成xml文件{" + outFilePath + "}出错,请检查!", e);
		}
	}

	/**
	 * 生成xml文件通过document
	 * 
	 * @param outFilePath
	 * @param document
	 */
	public static void createXmlFile(String outFilePath, Document document) {
		XMLWriter writer = null;
		try {
			writer = new XMLWriter(new FileWriter(outFilePath));
			writer.write(document);
			writer.close();
		} catch (IOException e) {
			//Log.getLogger().error("生成xml文件{" + outFilePath + "}出错,请检查!", e);
		}
	}

	/**
	 * 根据xml字符串生成xml文档
	 * 
	 * @param outFilePath
	 * @param xmlContent
	 */
	public static void createPrettyXmlFile(String outFilePath, String xmlContent) {
		try {
			Document document = DocumentHelper.parseText(xmlContent);
			createPrettyXmlFile(outFilePath, document);
		} catch (DocumentException e) {
			//Log.getLogger().error("xmlContent构造Document出错,请检查xmlContent内容是否是xml规范内容!", e);
		}
	}

	/**
	 * 根据xml字符串生成xml文档
	 * 
	 * @param outFilePath
	 * @param xmlContent
	 */
	public static void createXmlFile(String outFilePath, String xmlContent) {
		try {
			Document document = DocumentHelper.parseText(xmlContent);
			createXmlFile(outFilePath, document);
		} catch (DocumentException e) {
		//	Log.getLogger().error("xmlContent构造Document出错,请检查xmlContent内容是否是xml规范内容!", e);
		}
	}

	public static void main(String[] args) {
		List<String> list = Dom4jUtil.getNodesAttribute("d:\\tables_mysql_evolution.xml", "class", "tablename");
		for (String s : list) {
			System.out.println(s);
		}
		System.out.println(Dom4jUtil.getNodeAttribute("D:\\Project\\BrainCore\\braincore\\src\\config\\tables.xml", "class", "tablename", "sys_button", "tablepkname"));
	}
}

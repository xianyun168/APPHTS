package com.ciitc.webServices.util;

import java.io.FileInputStream;
import java.util.Date;

import cn.com.jit.assp.dsign.DSign;
import cfca.paperless.bean.PdfBean;
import cfca.paperless.bean.SignLocation;
import cfca.paperless.servlet.PaperlessClient;
import cfca.paperless.util.IoUtil;
import cfca.paperless.util.PaperlessUtil;
import cfca.paperless.util.StrategyUtil;
import cfca.paperless.util.StringUtil;
import cfca.sadk.util.Base64;

public class SealAndSignPdfUtil {
	
	private static SealAndSignPdfUtil instance = new SealAndSignPdfUtil();

	private SealAndSignPdfUtil() {

	}

	public static synchronized SealAndSignPdfUtil getInstance() {
		if (null == instance) {
			instance = new SealAndSignPdfUtil();
		}
		return instance;
	}
	
	public static String operatorCode = "gy01";
	public static String channelCode = "cn01";
	
	/**
	 * 给生成的PDF电子保单签章签名
	 */
	public String sealAndSignPdf(String extUrl, String wqzPdfPath, String qzPdfPath, String yztaPath, String zsPath, String qmPath, String type) {
		try {
			Log.getLogger().info("签章签名开始");
			Log.getLogger().info("签章服务器地址:" + extUrl);
			Log.getLogger().info("印章图案地址:" + yztaPath);
			Log.getLogger().info("证书地址:" + zsPath);
			Log.getLogger().info("签名证书地址:" + zsPath);
			long timeStart = new Date().getTime();// 开始时间
			//初始化客户端对象
			PaperlessClient clientBean = new PaperlessClient(extUrl);// 无纸化辅助接口的访问URL
/**/     	byte[] pdf = IoUtil.read(new FileInputStream(wqzPdfPath));
         	//公钥证书
         	byte[] cert = IoUtil.read(new FileInputStream(zsPath));
         	//印模图片
/**/     	byte[] sealImage = IoUtil.read(new FileInputStream(yztaPath));
         	//签章位置
/**/     	SignLocation signLocation = new SignLocation("保险人签章", "C", 0, 0);
         	// 策略文件
/**/     	String strategyXml = StrategyUtil.createClientSealStrategyXml("业务", "china", sealImage, signLocation, "SM3", "");
         	//计算hash x509Cert参数可以为空，为空时外部合成签名只能是pkcs7
/***/    	String returnMessage = clientBean.autoCalculatePdfHash(pdf, cert,strategyXml, operatorCode, channelCode);

         	String code = StringUtil.getNodeText(returnMessage, "Code");
         	if ("200".equals(code)) {
         		String pdfSealSource = StringUtil.getNodeText(returnMessage, "PdfSealSource");//pdf签名原文的base64值
         		String id = StringUtil.getNodeText(returnMessage, "Id");//标识,用来对应后台的缓存数据
         		Log.getLogger().info("Id标识,用来对应后台的缓存数据" + id);
         		DSign signDs = new DSign();
         		signDs.initConfig(qmPath);
         		String signReturn = "";
         		signReturn = signDs.detachSign(type, Base64.decode(pdfSealSource));
         		if(signReturn != null){
                	//合成签名
		         	PdfBean pdfBean = new PdfBean("", id, "B001", "auto", "");
		        	pdfBean.setPdfId(id);
		        	byte[] sealedPdfData = clientBean.synthesizeOuterSignatureAndSealPdf(Base64.decode(signReturn), PaperlessUtil.buildPdfBeanXml(pdfBean), "", operatorCode, channelCode);
	             	String errorRsString = PaperlessUtil.isError(sealedPdfData);
	             	if ("".equals(errorRsString)) {
	             		System.out.println(sealedPdfData.length);
	             		IoUtil.write(qzPdfPath, sealedPdfData);
	             		long timeEnd = new Date().getTime();// 结束时间
	             		Log.getLogger().info("签章签名所用时间为:" + (timeEnd - timeStart));
	             		return null;
	             	} else {
	             		// 处理结果为异常，打印出错误码和错误信息
	             		return "false";
	             	}
         		}else{
         			Log.getLogger().error("调用签名服务器返回错误号为" + signDs.getErrorCode());
         			Log.getLogger().error("调用签名服务器返回错误描述" + signDs.getErrorMessage());
         			return signDs.getErrorMessage();
         		}
         	} else {
         		Log.getLogger().error("获得PDF文档HASH值失败！" + returnMessage);
         		return returnMessage;
         	}
		} catch (Exception e) {
			e.printStackTrace();
			return "false";
		}
	}
}

package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GK_AppUtil {
	// 根据客户编号查询客户上一季度是否有持仓
	public String getContractStateByCustomerId(String custmerId, String startDate, String endDate) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		//01:不存在有效合同，00：存在有效合同
		String htState = "01";
		try {
			if (custmerId != null) {

				/*String sql = "select COALESCE(max(F_CONTRACTSIGNBALANCE),0) from cust_view_position where d_caldate between to_date('"
						+ startDate + "','yyyymmdd') and to_date('" + endDate + "','yyyymmdd') and c_custno='"+custmerId+"'";*/
				String sql =" with a1 as ( select t1.c_custno,t1.d_capitaldate d_capitaldate,t1.l_proserialno,t1.f_balance,t2.d_expirationdate   " + 
						"  from tcapitalcurrents t1 left join tsaleproject t2 on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' " + 
						"   AND t1.C_FLAG = '1'  union all  select t1.c_custno,t1.d_capitaldate d_capitaldate,  t1.l_proserialno,  t1.f_balance," + 
						"   to_date(to_char(t1.d_setupdate,'yyyymmdd'),'yyyymmdd') d_expirationdate  from tcapitalcurrents t1  left join tsaleproject t2 " + 
						"  on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' AND t1.C_FLAG = '9'  AND to_date(to_char(t2.d_setupdate,'yyyymmdd'),'yyyymmdd') <> t1.d_capitaldate ), " + 
						"  A2 AS ( SELECT c_custno, add_date,  SUM(f_balance) f_balance FROM( SELECT  c_custno, d_capitaldate add_date, f_balance  " + 
						"    FROM A1  UNION ALL SELECT c_custno, d_expirationdate+1,  (-1)*f_balance f_balance    FROM A1)  GROUP BY c_custno,  add_date )  " + 
						"  select c_custno, max(f_balance) from( SELECT   t1.c_custno,  t1.add_date startdate, " + 
						"  lead(t1.add_date-1,1,to_date('29991231','yyyymmdd')) over (partition by t1.c_custno order by t1.add_date asc ) enddate,  " + 
						"  sum(t2.f_balance) f_balance FROM  A2  t1 LEFT JOIN  A2 t2 ON t1.add_date >= t2.add_date " + 
						"  and t1.c_custno = t2.c_custno group by  t1.c_custno,t1.add_date ) " + 
						"  where c_custno ='"+custmerId+"' and  (startdate between to_date('"+startDate+"','yyyymmdd') and to_date('"+endDate+"','yyyymmdd')  " + 
						"   or (startdate < to_date('"+startDate+"','yyyymmdd') and enddate >=to_date('"+startDate+"','yyyymmdd'))) " + 
						"  group by c_custno ";
				call = conn.prepareCall(sql);
			}

			rs = call.executeQuery();

			while (rs.next()) {
				if (rs.getInt("max(f_balance)") != 0) {
					htState = "00";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != rs) {
					rs.close();
				}
				if (null != call) {
					call.close();
				}
				if (null != conn) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return htState;
	}
	// 根据客户编号查询客户上一季度是否有持仓
	public int getContractStateByCustomerId1(String custmerId, String startDate, String endDate) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		int htState = 0;
		try {
			if (custmerId != null) {
				String sql =" with a1 as ( select t1.c_custno,t1.d_capitaldate d_capitaldate,t1.l_proserialno,t1.f_balance,t2.d_expirationdate   " + 
						"  from tcapitalcurrents t1 left join tsaleproject t2 on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' " + 
						"   AND t1.C_FLAG = '1'  union all  select t1.c_custno,t1.d_capitaldate d_capitaldate,  t1.l_proserialno,  t1.f_balance," + 
						"   to_date(to_char(t1.d_setupdate,'yyyymmdd'),'yyyymmdd') d_expirationdate  from tcapitalcurrents t1  left join tsaleproject t2 " + 
						"  on t1.l_proserialno = t2.l_proserialno  where t1.C_ACCOUNTCHECK = '0' AND t1.C_FLAG = '9'  AND to_date(to_char(t2.d_setupdate,'yyyymmdd'),'yyyymmdd') <> t1.d_capitaldate ), " + 
						"  A2 AS ( SELECT c_custno, add_date,  SUM(f_balance) f_balance FROM( SELECT  c_custno, d_capitaldate add_date, f_balance  " + 
						"    FROM A1  UNION ALL SELECT c_custno, d_expirationdate+1,  (-1)*f_balance f_balance    FROM A1)  GROUP BY c_custno,  add_date )  " + 
						"  select c_custno, max(f_balance) from( SELECT   t1.c_custno,  t1.add_date startdate, " + 
						"  lead(t1.add_date-1,1,to_date('29991231','yyyymmdd')) over (partition by t1.c_custno order by t1.add_date asc ) enddate,  " + 
						"  sum(t2.f_balance) f_balance FROM  A2  t1 LEFT JOIN  A2 t2 ON t1.add_date >= t2.add_date " + 
						"  and t1.c_custno = t2.c_custno group by  t1.c_custno,t1.add_date ) " + 
						"  where c_custno ='"+custmerId+"' and  (startdate between to_date('"+startDate+"','yyyymmdd') and to_date('"+endDate+"','yyyymmdd')  " + 
						"   or (startdate < to_date('"+startDate+"','yyyymmdd') and enddate >=to_date('"+startDate+"','yyyymmdd'))) " + 
						"  group by c_custno ";
				call = conn.prepareCall(sql);
			}
			rs = call.executeQuery();
			while (rs.next()) {
				htState = rs.getInt("MAX(F_BALANCE)");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (null != rs) {
					rs.close();
				}
				if (null != call) {
					call.close();
				}
				if (null != conn) {
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return htState;
	}
}

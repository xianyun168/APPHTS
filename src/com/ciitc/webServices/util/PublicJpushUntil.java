package com.ciitc.webServices.util;



import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import cn.jiguang.common.ClientConfig;
import cn.jiguang.common.ServiceHelper;
import cn.jiguang.common.connection.NativeHttpClient;
import cn.jiguang.common.resp.APIConnectionException;
import cn.jiguang.common.resp.APIRequestException;
import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;
import cn.jpush.api.schedule.ScheduleResult;

public class PublicJpushUntil {
	
	public   Logger LOG = LoggerFactory.getLogger(PublicJpushUntil.class);
	
   	public  JPushClient jpushClient;
   	
   	//全部推送
  	@SuppressWarnings("deprecation")
  	public  void SendPushAll(String appKey, String masterSecret,String content) {
  		ClientConfig clientConfig = ClientConfig.getInstance();
          JPushClient jpushClient = new JPushClient(masterSecret,appKey,null,clientConfig);
          String authCode = ServiceHelper.getBasicAuthorization(appKey,masterSecret);
          NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
          jpushClient.getPushClient().setHttpClient(httpClient);
          PushPayload payload=sendToAllAlert(content);
         try {
         	   //System.out.println(payload.toString());
             PushResult result = jpushClient.sendPush(payload);
             if(result!=null){
          	   LOG.info("Got result - " + result);
                 System.out.println("推送成功！！！！！！");
              }else{
                 System.out.println("result is null..........");
              }
         } catch (APIConnectionException e) {
             LOG.error("Connection error. Should retry later. ", e);
         } catch (APIRequestException e) {
             LOG.error("Error response from JPush server. Should review and fix it. ", e);
             LOG.info("HTTP Status: " + e.getStatus());
             LOG.info("Error Code: " + e.getErrorCode());
             LOG.info("Error Message: " + e.getErrorMessage());
             LOG.info("Msg ID: " + e.getMsgId());
         }
  	}
   	
  	
  //单个用户推送
  	@SuppressWarnings("deprecation")
  	public  void SendPushAlias(String appKey, String masterSecret,String title,String content,Set<String> alias) {
  		ClientConfig clientConfig = ClientConfig.getInstance();
          JPushClient jpushClient = new JPushClient(masterSecret,appKey,null,clientConfig);
          String authCode = ServiceHelper.getBasicAuthorization(appKey,masterSecret);
          NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
          jpushClient.getPushClient().setHttpClient(httpClient);
          PushPayload payload=sendAllByAlias(title,content,alias);
         try {
         	 //System.out.println(payload.toString());
             PushResult result = jpushClient.sendPush(payload);
             if(result!=null){
          	   LOG.info("Got result - " + result);
                 System.out.println("推送成功！！！！！！");
              }else{
                 System.out.println("result is null..........");
              }
         } catch (APIConnectionException e) {
             LOG.error("Connection error. Should retry later. ", e);
         } catch (APIRequestException e) {
             LOG.error("Error response from JPush server. Should review and fix it. ", e);
             LOG.info("HTTP Status: " + e.getStatus());
             LOG.info("Error Code: " + e.getErrorCode());
             LOG.info("Error Message: " + e.getErrorMessage());
             LOG.info("Msg ID: " + e.getMsgId());
         }
  	}
   	
   	
   	//分组推送
	@SuppressWarnings("deprecation")
	public  String SendPushTag(String appKey, String masterSecret,String title,String content,Set<String> tag) {
		String state = "";
		ClientConfig clientConfig = ClientConfig.getInstance();
        JPushClient jpushClient = new JPushClient(masterSecret,appKey,null,clientConfig);
        String authCode = ServiceHelper.getBasicAuthorization(appKey,masterSecret);
        NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
        jpushClient.getPushClient().setHttpClient(httpClient);
        PushPayload payload=sendAllByTag(title,content,tag);
       try {
       	   //System.out.println(payload.toString());
           PushResult result = jpushClient.sendPush(payload);
           if(result!=null){
        	   state = "00";//成功
        	   LOG.info("Got result - " + result);
               System.out.println("推送成功！！！！！！");
            }else{
               System.out.println("result is null..........");
            }
       } catch (APIConnectionException e) {
           LOG.error("Connection error. Should retry later. ", e);
       } catch (APIRequestException e) {
           LOG.error("Error response from JPush server. Should review and fix it. ", e);
           LOG.info("HTTP Status: " + e.getStatus());
           LOG.info("Error Code: " + e.getErrorCode());
           LOG.info("Error Message: " + e.getErrorMessage());
           LOG.info("Msg ID: " + e.getMsgId());
       }
       return state;
	}
	
	//定时分组推送（后台）
	public  void timSingleSchedule(String appKey, String masterSecret,String time,String title,String content,Set<String> tag) {
		ClientConfig clientConfig = ClientConfig.getInstance();
        JPushClient jpushClient = new JPushClient(masterSecret,appKey,null,clientConfig);
        String authCode = ServiceHelper.getBasicAuthorization(appKey,masterSecret);
        NativeHttpClient httpClient = new NativeHttpClient(authCode, null, clientConfig);
        jpushClient.getPushClient().setHttpClient(httpClient);
        String name = "定时推送";
        PushPayload push = sendAllByTag(title,content,tag);
        try {
            ScheduleResult result = jpushClient.createSingleSchedule(name, time, push);
            LOG.info("schedule result is " + result);
            System.out.println(result+"................................");
        } catch (APIConnectionException e) {
            LOG.error("Connection error. Should retry later. ", e);
            LOG.info("无法连接到Jpush服务器 ");
        } catch (APIRequestException e) {
            LOG.error("Error response from JPush server. Should review and fix it. ", e);
            LOG.info("HTTP Status: " + e.getStatus());
            LOG.info("Error Code: " + e.getErrorCode());
            LOG.info("Error Message: " + e.getErrorMessage());
        }
    }
	

	
	//所有平台所有用户推送消息
	public  PushPayload sendToAllAlert(String content) {
        return PushPayload.newBuilder()
                .setPlatform(Platform.all())//设置接受的平台
                .setAudience(Audience.all())//Audience设置为all，说明采用广播方式推送，所有用户都可以接收到
                .setNotification(Notification.alert(content))
                .build();
    }
	
	//分组推送
	public  PushPayload sendAllByTag(String title,String content,Set<String> tag) {
		return PushPayload.newBuilder()
		.setPlatform(Platform.android_ios())
		.setAudience(Audience.tag(tag))
		.setNotification(Notification.newBuilder() 
		.setAlert(title)
		.addPlatformNotification(AndroidNotification.newBuilder()
		.setTitle(title).build())
		.addPlatformNotification(IosNotification.newBuilder()
		.incrBadge(1)
		.addExtra(title, content).build())
		.build()).setOptions(Options.newBuilder().setApnsProduction(false).build())
		.build();
		}
	
	//单个用户推送
	public  PushPayload sendAllByAlias(String title,String content,Set<String> alias) {
		return PushPayload.newBuilder()
		.setPlatform(Platform.android_ios())
		.setAudience(Audience.alias(alias))
		.setNotification(Notification.newBuilder()
		.setAlert(title)
		.addPlatformNotification(AndroidNotification.newBuilder()
		.setTitle(title).build())
		.addPlatformNotification(IosNotification.newBuilder()
		.incrBadge(1)
		.addExtra(title, content).build())
		.build()).setOptions(Options.newBuilder().setApnsProduction(false).build())
		.build();
		}
}


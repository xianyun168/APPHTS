
package com.ciitc.webServices.util;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import com.ciitc.webServices.model.AppPerformanceInterface;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
/**
 * 定时任务，更新计算业绩数
 * @author Administrator
 *
 */

public class CRM_AchievementUntilTimeing extends BaseController{
     public void reservat(){
	 List<String>  usernamecall     = new ArrayList<String>();//每次更新有理财师姓名
	 List<String>  departmentid1Arr = new ArrayList<String>();//所属小团队成员
	 List<String>  departmentid2Arr = new ArrayList<String>();//所属分公司成员
	 List<String>  subcompanyidArr  = new ArrayList<String>(); //所属大区成员
	 Double xiaonum;               //用户合同业绩总和
	 Double departmentid1sum;     //所属小团队合同业绩总和
	 Double departmentid2sum;     //所属分公司合同业绩总和;
	 Double subcompanyidArrsum;   //所属大区合同业绩总和;
	 Double Targetxiao;            //用户 当前目标业绩
	 Double Targetdepartmentid1;  //所属当前目标业绩总和
	 Double Targetdepartmentid2;  //所属分公司当前目标业绩总和;
	 Double TargetsubcompanyidArr;//所属大区当前目标业绩总和;	
	 String startTime;//合同开始时-精确到秒
	 String endTime;  //合同结束时间-精确到秒
	 String date;     //目标业绩-精确到天
	 String time;     //插入时间
	    AppPerformanceInterface a =null;
	    SimpleDateFormat simdate=new SimpleDateFormat("yyyy-MM-dd");
		//每日更新查询时间
		Calendar now = Calendar.getInstance();
		int yes = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1;
		int day=now.get(Calendar.DAY_OF_MONTH);
		//每次查询时间都为当天所在日
		startTime=yes+"-"+month+"-"+day+" 00:00:00";
		endTime=yes+"-"+month+"-"+day+" 23:59:59";
		//目标业绩查询时间精确到天
		 date= yes+"-"+month+"-"+day;
		//每次插入时间
		 time=yes+"-"+month+"-"+day;		 
		try{ 
			//每日更新所有用户
			usernamecall = usernamecall();
			
			for(String username:usernamecall){
				Log.getLogger().info("本次循环用户名为:"+username);
				a = new AppPerformanceInterface();
				String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
				//将当日时间字符串转换为Date作为插入数据的时间
				 Date date1=simdate.parse(time);	
				
			
			xiaonum = indexsum(username,startTime,endTime);
			Log.getLogger().info("个人合同业绩"+xiaonum);
			
			departmentid1Arr = member(username);
			
			Double d =null;
			departmentid1sum=0.0;
			for(String name1:departmentid1Arr){
				if(name1!=null){
					d = indexsum(name1,startTime,endTime);
					departmentid1sum=d+departmentid1sum;
				}	
			}	
			Log.getLogger().info("小团队合同业绩"+departmentid1sum);
			
			departmentid2Arr = departmember(username);
			
			Double d1 = null;
			departmentid2sum=0.0;
			for(String name2:departmentid2Arr){
				if(name2!=null){
				d1 = indexsum(name2,startTime,endTime);
				departmentid2sum+=d1;
				}
			}
			Log.getLogger().info("分公司合同业绩"+departmentid2sum);
			
			subcompanyidArr = subcommember(username);
			
			Double d2 =null;
			subcompanyidArrsum=0.0;
			for(String name3:subcompanyidArr){
				if(name3!=null){
				d2 = indexsum(name3,startTime,endTime);
				subcompanyidArrsum+=d2;
				}
			}					
			Log.getLogger().info("大区合同业绩"+subcompanyidArrsum);
			
			Targetxiao = targetperformance(username,date);
			Log.getLogger().info("个人目标业绩"+Targetxiao);
			
			Double d3;
			Targetdepartmentid1=0.0;
			for( String name4:departmentid1Arr){
				if(name4!=null){
			 d3= targetperformance(name4,date);
			Targetdepartmentid1+=d3;
				}
			}	
			Log.getLogger().info("小团队目标业绩"+Targetdepartmentid1);
			
			Double d4;
			Targetdepartmentid2=0.0;
			for( String name6:departmentid2Arr){
				if(name6!=null){
			d4= targetperformance(name6,date);
			Targetdepartmentid2+=d4;
				}
			}			
			Log.getLogger().info("分公司目标业绩"+Targetdepartmentid2);
			
			Double d5;
			TargetsubcompanyidArr=0.0;
			for( String name7:subcompanyidArr){
				if(name7!=null){
			d5= targetperformance(name7,date);
			TargetsubcompanyidArr+=d5;
				}
			}
			Log.getLogger().info("大区目标业绩"+TargetsubcompanyidArr);
	        a.setCodeid(uuid);
			a.setCodename(username);
			a.setTime(date1);
			a.setIndcontract(xiaonum);
			a.setTeamcontract(departmentid1sum);
			a.setCompanycontract(departmentid2sum);
			a.setAreacontact(subcompanyidArrsum);
			a.setIndaims(Targetxiao);
			a.setTeamaims(Targetdepartmentid1);
			a.setCompanyaims(Targetdepartmentid2);
			a.setAreatims(TargetsubcompanyidArr);
			//开始插入数据
	        insertcall(a);//执行插入动作
	        //删除集合数据
	        removearray(departmentid1Arr);
	        removearray(departmentid2Arr);
	        removearray(subcompanyidArr);
		}//到此循环完成所有用户更新操作	
			//更新存储用户集合
			removearray(usernamecall);	
		} catch (Exception e) {
				e.printStackTrace();
			}
		

  }

	/**
	 * 计算个人合同业绩
	 * @param username
	 * @param startTime
	 * @param endTime
	 * @param date
	 * @return
	 * @throws SQLExceptions
	 * @throws ParseException
	 */
	public  double indexsum(String username,String startTime,String endTime){ 
		List<Double>  xiaoarray = new ArrayList<Double>();//个人单品合同业绩
		List<List<String>> arrlist = new ArrayList<List<String>>();
		Double sum = 0.0;
		// 连接34莱CRM数据(34库) 
		Connection conn = DBUtil.makeConnection();
		CallableStatement call =null;
		ResultSet rs = null;
	    try{
		//查出项目编号和合同金额 ，编号作为kay,金额作为value存到map1中（         为项目编号待确定	
		 call = conn.prepareCall("select  f_Contractsignbalance AS n, l_proserialno ,d_Createdate"
				+ " FROM ttrustcontract_req  where c_brokeraccount = (select c_brokeraccount from tfundbroker where c_Usercode='"+username+"') "
				+ " and d_Createdate between to_date('"+startTime+"','yyyy-mm-dd hh24:mi:ss') and to_date('"+endTime+"','yyyy-mm-dd hh24:mi:ss') and C_STATUS='2' ");
		       rs = call.executeQuery();    
		     
		 List<String> list = null;
		while (rs.next()){
				list = new ArrayList<String>();
				//转换时间格式
				Date da = rs.getDate("d_Createdate");
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			     String time=sdf.format(da);
				list.add(time);//存放时间
				list.add(rs.getString("l_proserialno"));//存放编号
				list.add(rs.getString("n"));//存放金额
				//存放每个商品的信息
				arrlist.add(list);
		}		
			if(arrlist.size()>0){
             //遍历集合，每个元素为一个商品的信息
			for(List<String> mapp: arrlist){
			     String time=mapp.get(0);
			     String number= mapp.get(1);
			  Connection con = dbutil();
			  CallableStatement cal = null;
			  ResultSet rs1 =null;
			  boolean fals = true;
			  try{			   
			    cal = con.prepareCall(" SELECT coefficient from CRM_SCALING_COEFFICIENT "
			    		+ "WHERE PRODUCT_ID='"+number+"' and scalin_date=to_date('"+time+"','yyyy-mm-dd')");
				 rs1 = cal.executeQuery();
								
				while (rs1.next()) {
					fals=false;	
					Double value =Double.parseDouble(mapp.get(2));//商品的金额
					Double i =(rs1.getDouble("coefficient"));//该商品对应的折标系数
					xiaoarray.add(value*i);//该商品的总单价				
				
					}
			  }catch(SQLException e){
				  e.printStackTrace();
			  }finally {
				if(rs1 != null){
					try {
						rs1.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			if(cal !=null){
				try {
					cal.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				}
				if(con !=null){
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace();
					}				
				}
			}
			 
			  	//若没有折标系数
				if(fals){
				 xiaoarray.add(Double.parseDouble(mapp.get(2)));
				 
				
				
			 }
				}
		      for(Double doub:xiaoarray){
			        sum+=doub;
		  }
      }else{	
    	 sum=0.0;
      }
	    
	    }catch(SQLException e){
	    	e.printStackTrace();
	    	System.out.println("数据库链接错误");
	    }finally {
			if(rs!=null){
			try {
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}	
			}
			if(call != null){
				try {
					call.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
			if(conn != null){
				try {
					conn.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
			}
		}
	    arrlist.clear();
	    xiaoarray.clear();
	   
		
		return sum;
			
	}
	/**
	 * 计算小团对成员
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public  List<String> member(String username) throws SQLException{
		List<String> arr =new ArrayList<String>();
		Connection con = DBUtil.makeOds();     	
		CallableStatement call = null;
		ResultSet rs1 = null;
		try{
		call = con.prepareCall(" SELECT LOGINID FROM view_empl_app_sale_relation"
				+ " WHERE "
				+ " departmentid1 = "
				+ " (SELECT departmentid1 FROM view_empl_app_sale_relation WHERE loginid='"+username+"') ");
		 rs1= call.executeQuery();
	
		while (rs1.next()){
			
				arr.add(rs1.getString("LOGINID"));	
			
		}
		
		
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
		
			if(rs1!=null){
				try {
					rs1.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				rs1.close();
			}
			if(call!=null){
				try {
					call.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			if(con != null){
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return arr;
	}
	/**
	 * 计算分公司成员
	 * @param username
	 * @return
	 * @throws SQLException 
	 */	
	public  List<String> departmember(String username) throws SQLException{
		List<String> arr = new ArrayList<String>();
		Connection con = DBUtil.makeOds();     	
		CallableStatement call = null;
		ResultSet rs1 = null;
		
		try{
		call = con.prepareCall(" SELECT LOGINID FROM view_empl_app_sale_relation"
				+ " WHERE "
				+ " departmentid2 = "
				+ " (SELECT departmentid2 FROM view_empl_app_sale_relation WHERE loginid='"+username+"') ");
		  rs1 = call.executeQuery();
		while (rs1.next()){	
				arr.add(rs1.getString("LOGINID"));	
		}
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			if(rs1!=null){
				try {
					rs1.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			if(call!=null){
				try {
					call.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			if(con!=null){
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return arr;
	}
	/**
	 * 计算大区成员
	 * @param username
	 * @return
	 * @throws SQLException
	 */
	public  List<String> subcommember(String username) throws SQLException{
		Connection con = DBUtil.makeOds();     	
		CallableStatement call = null;
		List<String> arr = new ArrayList<String>();
		ResultSet rs1 = null;
		try{
		call = con.prepareCall(" SELECT LOGINID FROM view_empl_app_sale_relation"
				+ " WHERE "
				+ " subcompanyid = "
				+ " (SELECT subcompanyid FROM view_empl_app_sale_relation WHERE loginid='"+username+"') ");
		 rs1 = call.executeQuery();
		while (rs1.next()){
			if(rs1.getString("LOGINID") != null){
				arr.add(rs1.getString("LOGINID"));
		}		 
			
	}
		}catch(SQLException e){
			e.printStackTrace();
		}finally {
			if(rs1!=null){
				try {
					rs1.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			if(call!=null){
				try {
					call.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			if(con!=null){
				try {
					con.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return arr;
	}
	/**
	 * 计算个人当天目标业绩
	 * @return
	 * @throws SQLException 
	 */
	public  double targetperformance(String username, String date ) throws SQLException{
		 	Double d = 0.0;
		 	Connection con = DBUtil.makeOds();     	
			CallableStatement call = null;
			ResultSet rs1=null;
		try {
			call = con.prepareCall(" SELECT jobtitle FROM view_empl_app_sale_relation WHERE loginid='"+username+"' ");		
			rs1 = call.executeQuery();
			String jobtitle = null;
			while (rs1.next()){
				if(rs1.getString("jobtitle")!=null){
					jobtitle = rs1.getString("jobtitle");
				}
			}			
			if(jobtitle!=null){			
			Connection co = dbutil();
			CallableStatement cal = null;
			ResultSet rs =null;
			try{
		        cal = co.prepareCall(" select TARGET from  CRM_PERFORMANCE_GOALS "
		         		      + " where ASSESSMENT_START_TIME<= to_date('"+date+"','yyyy-mm-dd') "
                               +" AND  to_date('"+date+"','yyyy-mm-dd')<=ASSESSMENT_END_TIME  and rank='"+jobtitle+"' ");
		          rs= cal.executeQuery();	
		         while (rs.next()){
						d= rs.getDouble("TARGET");
						
					}		         
			}catch(SQLException e){
				e.printStackTrace();
			}finally {
				if(rs!=null){
					try {
						rs.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				if(cal!=null){
					try {
						cal.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				if(co!=null){
					try {
						co.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
			}
		        }catch (SQLException e) {
		           System.out.println("数据库链接对象出错!");
		           e.printStackTrace();
		    }finally {
			if(rs1!=null){
				try {
					rs1.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
					
				}
				if(call!=null){
					try {
						call.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				if(con!=null){
					try {
						con.close();
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
				
			}
			
		return d;
	}
	/**
	 * 每日定时更新所有理财师用户名
	 * @return
	 * @throws SQLException 
	 */
	public  List<String> usernamecall() throws SQLException{
		List<String> list = new ArrayList<String>();
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		try {
				
			 call = conn.prepareCall(" select C_USERCODE from tfundbroker ");
			 rs = call.executeQuery();      
			while (rs.next()){
				
			String str =rs.getString("C_USERCODE");
				list.add(str);
			}
		} catch (SQLException e) {
				e.printStackTrace();
				System.out.println("数据库链接失败");
		}finally {
			
			if(rs!=null){
				try {
					rs.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			if(call!=null){
				try {
					call.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			if(conn!=null){
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
		return list;
	}
	/**
	 * 删除集合数据
	 * @param departmentid1Arr2
	 */
	public  void removearray(List<String> arr){
		Object obj = null;
	    for(int i = 0;i < arr.size(); i++){  
            if (obj == arr.get(i))  
            {  
            	arr.remove(obj);  
            }  
        }  
    }  
	/**
	 * 通过JDBC进行本地数据库链�?
	 * @return
	 */
	public  Connection dbutil(){//192.168.137.1:1521/orcl
        Connection conn = null;
        try {
            try {
                Class.forName("oracle.jdbc.driver.OracleDriver");//10.0.100.34:1521/crmdb192.168.137.1:1521/orcl
            } catch (ClassNotFoundException e) {
                e.printStackTrace(); 
            }
            conn = DriverManager.getConnection("jdbc:oracle:thin:@192.168.137.1:1521:orcl", "kfcrm", "kfcrm");
        } catch (SQLException e) {
            System.out.println("通过JDBC获取Oralce数据库链接对象出错！");
            e.printStackTrace();
        }
        return conn;
    }
	/**
	 * 向数据库插入数据
	 */
	public  void insertcall(AppPerformanceInterface a){
		Connection conn = dbutil();
		PreparedStatement ps=null;
		
		try {	
			
			    conn.setAutoCommit(false);
				String sql = "INSERT INTO APP_PERFORMANCE_INTERFACE "
						   + "(codeid,codename,indcontract,teamcontract,companycontract,areacontact,indaims,teamaims,companyaims,areatims,time) "
						   + "VALUES "
						   + "(?,?,?,?,?,?,?,?,?,?,?)";
				ps = conn.prepareStatement(sql);
					ps.setString(1,a.getCodeid() );
					ps.setString(2,a.getCodename());
					ps.setDouble(3, a.getIndcontract());
					ps.setDouble(4, a.getTeamcontract());	
					ps.setDouble(5, a.getCompanycontract());	
					ps.setDouble(6, a.getAreacontact());	
					ps.setDouble(7, a.getIndaims());	
					ps.setDouble(8, a.getTeamaims());	
					ps.setDouble(9, a.getCompanyaims());	
					ps.setDouble(10, a.getAreatims());	
					ps.setDate(11,  new java.sql.Date(a.getTime().getTime()));	
					ps.executeUpdate();
					conn.commit();
				
	
		}catch(SQLException e){
			e.printStackTrace();
		}finally {

				if(ps!=null){
					try {
						ps.close();
					} catch (SQLException e) {
						
						e.printStackTrace();
					}

				}
				if(conn!=null){
					try {
						conn.close();
					} catch (SQLException e) {
						
						e.printStackTrace();
					}

				}
		}
	}
		
}

package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import com.ciitc.webServices.dao.AppMessageSendDao;
import com.ciitc.webServices.dao.MessageRemindRuleDao;
import com.ciitc.webServices.model.AppMessageSend;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.MessageRemindRule;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

public class CRM_Expiration {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	@SuppressWarnings("rawtypes")
	private MessageRemindRuleDao messageRemindRuleDao = (MessageRemindRuleDao) SpringContextUtil.getBean("messageRemindRuleDao");
	@SuppressWarnings("rawtypes")
	private AppMessageSendDao appMessageSendDao = (AppMessageSendDao) SpringContextUtil.getBean("appMessageSendDao");
	
	
	//产品认购结束期
	public void productRgEnd() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		// 连接耀莱CRM数据库
		PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		try {
			call = conn.prepareCall("select * from tfundinfo");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				Date nowDay = sdf.parse(sdf.format(new Date()));
				long b = nowDay.getTime();
				long issueEnds = 0;
				//认购结束期
				Date issueEnd = rs.getDate("D_ISSUEENDDATE");
				if(null != issueEnd) {
					long c = issueEnd.getTime();
					issueEnds = (c-b)/(24*60*60*1000);
				}
				//查看配置的认购结束期，
				MessageRemindRule messageRemindRule = getMessageRemindRule("产品认/申购期结束提醒");
				if(issueEnds == messageRemindRule.getDays()) {
					//产品名称
					String productName = rs.getString("C_FUNDNAME");
					//推送内容content
					String content = messageRemindRule.getMessageMould();
					content = content.replace("${productName}",productName);
					content = content.replace("${days}",String.valueOf(messageRemindRule.getDays()));
					//推送标签tag
					Set<String> tag = new HashSet<String>();
					tag.add("0001");
					
					AppMessageSend appMessageSend = new AppMessageSend();
					appMessageSend.setPkId(UUID.randomUUID().toString());//主键
					appMessageSend.setFlag("06");
					appMessageSend.setMessageTitle("产品认购期结束");
					appMessageSend.setMessageContent(content);
					appMessageSend.setSendObject("0001");
					appMessageSend.setSendState("未推送");
					appMessageSend.setSendDate("-");
					appMessageSend.setOperateName("-");
					appMessageSend.setMessageType("常规消息");
					appMessageSend.setSendType("系统");
					Date now = new Date();
					appMessageSend.setTimingSendDate(new Date(now.getTime()+60000));
					appMessageSendDao.insertSelective(appMessageSend);
					
					//调用app推送的方法，推送给公司所有员工
					String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "产品认购期结束", content, tag);
					if("00".equals(states)) {
						SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						AppMessageSend appMessageSend1 = new AppMessageSend();
						appMessageSend1.setSendState("已推送");
						appMessageSend1.setSendDate(sf.format(new Date()));
						appMessageSend1.setPkId(appMessageSend.getPkId());
						appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	//产品到期
	public void productEnd() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		try {
			call = conn.prepareCall("select * from tfundinfo");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				//产品到期日期
				Date endDay = rs.getDate("D_CONTRACTENDDATE");
				if(null !=endDay) {
					Date nowDay = sdf.parse(sdf.format(new Date()));
					long a = endDay.getTime();
					long b = nowDay.getTime();
					long day = (a-b)/(24*60*60*1000);
					//产品到期日
					MessageRemindRule messageRemindRule1 = getMessageRemindRule("产品到期提醒");
					//判断时间是否是60天
					if(day==messageRemindRule1.getDays()) {
						//产品名称
						String productName = rs.getString("C_FUNDNAME");
						//推送内容content
						String content = messageRemindRule1.getMessageMould();
						content = content.replace("${productName}",productName);
						content = content.replace("${days}",String.valueOf(messageRemindRule1.getDays()));
						//推送标签tag
						Set<String> tag = new HashSet<String>();
						tag.add("0001");
						
						AppMessageSend appMessageSend = new AppMessageSend();
						appMessageSend.setPkId(UUID.randomUUID().toString());//主键
						appMessageSend.setFlag("08");
						appMessageSend.setMessageTitle("产品到期");
						appMessageSend.setMessageContent(content);
						appMessageSend.setSendObject("0001");
						appMessageSend.setSendState("未推送");
						appMessageSend.setSendDate("-");
						appMessageSend.setOperateName("-");
						appMessageSend.setMessageType("常规消息");
						appMessageSend.setSendType("系统");
						Date now = new Date();
						appMessageSend.setTimingSendDate(new Date(now.getTime()+60000));
						appMessageSendDao.insertSelective(appMessageSend);
						//调用app推送的方法，推送给公司所有员工
						String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "产品到期", content, tag);
						if("00".equals(states)) {
							SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
							AppMessageSend appMessageSend1 = new AppMessageSend();
							appMessageSend1.setSendState("已推送");
							appMessageSend1.setSendDate(sf.format(new Date()));
							appMessageSend1.setPkId(appMessageSend.getPkId());
							appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	//客户产品到期
	public void productKhEnd() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		try {
			call = conn.prepareCall("select * from tfundinfo");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				//产品到期日期
				Date endDay = rs.getDate("D_CONTRACTENDDATE");
				if(null !=endDay) {
					Date nowDay = sdf.parse(sdf.format(new Date()));
					long a = endDay.getTime();
					long b = nowDay.getTime();
					long day = (a-b)/(24*60*60*1000);
					//客户产品到期日
					MessageRemindRule messageRemindRule2 = getMessageRemindRule("客户产品到期");
					if(day==messageRemindRule2.getDays()){
						//如果等于15天则查询那个客户购买了款产品，然后提醒客户的理财师
						CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
						//产品名称
						String productName = rs.getString("C_FUNDNAME");
						//产品编号
						String productNo = rs.getString("C_FUNDCODE");
						//根据产品的编号去合同表查询已生成的合同
						List<AppUserInfor> listApp = getHTByFundcode(productNo);
						if(listApp.size()>0) {
							for (AppUserInfor appUserInfor : listApp) {
								//根据客户的编号查询客户的名称
								String custname = reservationUtil.getCustName(appUserInfor.getCustomerId());
								Set<String> tag = new HashSet<String>();
								tag.add(appUserInfor.getFinancialId());
								String content = messageRemindRule2.getMessageMould();
								content = content.replace("${userName}", custname);
								content = content.replace("${productName}", productName);
								content = content.replace("${days}", messageRemindRule2.getDays().toString());
								
								AppMessageSend appMessageSend = new AppMessageSend();
								appMessageSend.setPkId(UUID.randomUUID().toString());//主键
								appMessageSend.setFlag("05");
								appMessageSend.setMessageTitle("客户产品到期");
								appMessageSend.setMessageContent(content);
								appMessageSend.setSendObject(appUserInfor.getFinancialId());
								appMessageSend.setSendState("未推送");
								appMessageSend.setSendDate("-");
								appMessageSend.setOperateName("-");
								appMessageSend.setMessageType("常规消息");
								appMessageSend.setSendType("系统");
								Date now = new Date();
								appMessageSend.setTimingSendDate(new Date(now.getTime()+60000));
								appMessageSendDao.insertSelective(appMessageSend);
								//推送给客户理财师
								String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "客户产品到期", content, tag);
								if("00".equals(states)) {
									SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
									AppMessageSend appMessageSend1 = new AppMessageSend();
									appMessageSend1.setSendState("已推送");
									appMessageSend1.setSendDate(sf.format(new Date()));
									appMessageSend1.setPkId(appMessageSend.getPkId());
									appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
								}
							}
							
						}
					}
				}
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	
	//根据名称查询推送模板
	public MessageRemindRule getMessageRemindRule(String messageRemindRuleName) {
		MessageRemindRule messageRemindRule = new MessageRemindRule();
		//根据规则名称查询规则信息
		messageRemindRule = messageRemindRuleDao.queryByName(messageRemindRuleName);
		return messageRemindRule;
	}
	//根据产品编号查询已生成的合同
	public List<AppUserInfor> getHTByFundcode(String productNo){
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		List<AppUserInfor> appUserList = new ArrayList<AppUserInfor>();
		// 连接耀莱CRM数据库
		try {
			call = conn.prepareCall("select * from ttrustcontract_req where C_STATUS = '2' and C_FUNDCODE = '"+productNo+"'");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				AppUserInfor appUserInfor = new AppUserInfor();
				//如果结果集不为空返回客户编号和理财师编号
				String c_custno = rs.getString("C_CUSTNO");
				//根据客户的编号查询客户的名称
				String financialId = rs.getString("C_BROKERACCOUNT");
				appUserInfor.setCustomerId(c_custno);
				appUserInfor.setFinancialId(financialId);
				appUserList.add(appUserInfor);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return appUserList;
	}
	//产品额度提醒
	/*public void productEndDate(String productNo) {
		// 连接耀莱CRM数据库
		CallableStatement call1 = null;
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		//根据产品的编号查询项目编号
		String l_proserialno = reservationUtil.getProserialno(productNo);
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		try {
			//某产品预约金额限制
			call = conn.prepareCall("select F_SCALE from tsaleproject where L_PROSERIALNO =  '"+l_proserialno+"'");
			// 执行
			ResultSet rs = call.executeQuery();
			while (rs.next()) {
				//产品预约限制金额
				Integer f_maxprodshares = rs.getInt("F_SCALE");
				//某产品当前已预约金额
				call1 = conn.prepareCall("select sum(f_balance) from treserve  where C_FUNDCODE='"+productNo+"' AND C_STATUS NOT IN ('1','2','5','6','9')");
				ResultSet rs1 = call1.executeQuery();
				Integer f_balance = 0;
				while (rs1.next()) {
					//已经预约的金额
					f_balance = Integer.parseInt(rs1.getString("sum(f_balance)"));
				}
				//产品剩余额度
				Integer money = f_maxprodshares-f_balance;
				PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
				//查看配置的认购结束期，
				MessageRemindRule messageRemindRule = getMessageRemindRule("产品额度提醒");
				if((money<=messageRemindRule.getRemainMoney()*10000)&&(money>0)) {
					//根据产品编号查询产品名称
					//产品名称
					String productName = reservationUtil.getProductName(productNo);
					//推送内容
					String content = messageRemindRule.getMessageMould();
					content = content.replace("${remainMoney}", money.toString());
					content = content.replace("${productName}", productName);
					//推送标签tag
					Set<String> tag = new HashSet<String>();
					tag.add("0001");
					AppMessageSend appMessageSend = new AppMessageSend();
					appMessageSend.setPkId(UUID.randomUUID().toString());//主键
					appMessageSend.setFlag("03");
					appMessageSend.setMessageTitle("产品额度");
					appMessageSend.setMessageContent(content);
					appMessageSend.setSendObject("0001");
					appMessageSend.setSendState("未推送");
					appMessageSend.setSendDate("-");
					appMessageSend.setOperateName("-");
					appMessageSend.setTimingSendDate(new Date());
					appMessageSend.setMessageType("常规消息");
					appMessageSend.setSendType("系统");
					appMessageSend.setTimingSendDate(new Date());
					appMessageSendDao.insertSelective(appMessageSend);
					//调用app推送的方法，推送给公司所有员工
					String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "产品额度", content, tag);
					if("00".equals(states)) {
						SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						AppMessageSend appMessageSend1 = new AppMessageSend();
						appMessageSend1.setSendState("已推送");
						appMessageSend1.setTimingSendDate(new Date());
						appMessageSend1.setSendDate(sf.format(new Date()));
						appMessageSend1.setPkId(appMessageSend.getPkId());
						appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	//产品分红时间
	public void fenHong(String date,String content) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeOds();
		CallableStatement call = null;
		ResultSet rs =  null;
		PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		try {
			call = conn.prepareCall("select DATA_DATE,C_FUNDINFO from CF_RESULT where FIN_ELEMENT_ID = 'A102' and to_char(DATA_DATE,'yyyy-MM-dd') like '"+date+"'  group by  DATA_DATE,C_FUNDINFO ");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				String fundinfo = rs.getString("C_FUNDINFO");
				//根据产品编号查询产品名称
				CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
				String productName = reservationUtil.getProductName(fundinfo);
				content = content.replace("${productName}", productName);
				//推送标签tag
				Set<String> tag = new HashSet<String>();
				tag.add("0001");
				
				AppMessageSend appMessageSend = new AppMessageSend();
				appMessageSend.setPkId(UUID.randomUUID().toString());//主键
				appMessageSend.setFlag("02");
				appMessageSend.setMessageTitle("产品分红");
				appMessageSend.setMessageContent(content);
				appMessageSend.setSendObject("0001");
				appMessageSend.setSendState("未推送");
				appMessageSend.setSendDate("-");
				appMessageSend.setOperateName("-");
				appMessageSend.setMessageType("常规消息");
				appMessageSend.setSendType("系统");
				Date now = new Date();
				appMessageSend.setTimingSendDate(new Date(now.getTime()+60000));
				appMessageSendDao.insertSelective(appMessageSend);
				//调用app推送的方法，推送给公司所有员工
				String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "产品分红", content, tag);
				if("00".equals(states)) {
					SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					AppMessageSend appMessageSend1 = new AppMessageSend();
					appMessageSend1.setSendState("已推送");
					appMessageSend1.setSendDate(sf.format(new Date()));
					appMessageSend1.setPkId(appMessageSend.getPkId());
					appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	//定时更组织架构表
	public void getBrokertree() {
		CrmFunctionUtil crmFunctionUtil = new CrmFunctionUtil();
		crmFunctionUtil.getBrokertree();
	}

}

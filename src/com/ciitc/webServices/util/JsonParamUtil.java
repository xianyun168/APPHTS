package com.ciitc.webServices.util;

import cn.com.ins.sinosoft.framework.base.util.StringUtil;
import net.sf.json.JSONObject;

/**
 * @program: APPHTS
 * @description:
 * @author: zhengyunfei
 * @create: 2019-07-26 16:00
 **/
public class JsonParamUtil implements java.io.Serializable{
    public static  JSONObject getJsonPram(String params) {
        JSONObject json = new JSONObject();
        if(params.contains("&")&&params.contains("=")){
            String paramArray[]=params.split("&");
            for(int i=0;i<paramArray.length;i++){
                String value=paramArray[i];
                if(value.contains("=")){
                    String valueArray[]=value.split("=");
                    if(StringUtil.isNotEmpty(valueArray)&&valueArray.length==2){
                        String key=valueArray[0];
                        String keyValue=valueArray[1];
                        json.put(key,keyValue);
                    }
                }
            }
        }else{
            json = JSONObject.fromObject(params);
        }
        return json;
    }

}

package com.ciitc.webServices.util;

import java.sql.*;

public class DbUtils {
    static Connection conn=null;
    public static Connection getConnection(){
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");//找到oracle驱动器所在的类
            String url="jdbc:oracle:thin:@10.0.100.34:1521:crmdb"; //URL地址
            String username="crmtest";
            String password="crmtest";
            conn= DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            System.out.println("数据库连接错误....");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("数据库连接错误....");
            e.printStackTrace();
        }
        return conn;
    }
    public static void close(PreparedStatement pstmt){
        if(pstmt !=null){
            try {
                pstmt.close();
            } catch (SQLException e) {
                System.out.println("数据库关闭错误....");
                e.printStackTrace();
            }
        }
    }

    public static void close(ResultSet rs){
        if(rs !=null){
            try {
                rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                System.out.println("结果集关闭错误....");
                e.printStackTrace();
            }
        }
    }
    public static void closeTheConn(){
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException e) {
                System.out.println("数据库关闭错误....");
                e.printStackTrace();
            }
        }
    }
}

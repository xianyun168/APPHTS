package com.ciitc.webServices.util;

import java.util.HashMap;
import java.util.Map;

public class TokenUtil {
	
	static Map<String, String> map = new HashMap<String, String>();
	
	public static void add(String policyNo){
		map.put(policyNo, policyNo);
	}
	
	public static String get(String policyNo){
		if(null != map.get(policyNo)){
			return map.get(policyNo);
		}
		return null;
	}
	
	public static void del(String policyNo){
		map.remove(policyNo);
	}

}

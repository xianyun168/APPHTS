package com.ciitc.webServices.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

import jcifs.smb.SmbFile;
import jcifs.smb.SmbFileInputStream;
import jcifs.smb.SmbFileOutputStream;

public class FileUpload {
	
	public void uploadFile(String srcPath,String shareUrl) {
		File parentDir = new File(shareUrl);
        File targetPath = new File(parentDir,UUID.randomUUID().toString().replaceAll("-", "")+".jpg");
        InputStream in = null;
        OutputStream out = null;
		try {
			in = new FileInputStream(srcPath);
			out = new FileOutputStream(targetPath);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
        
        try {
            byte[] bs = new byte[1024];
            int len = -1;
            try {
				while((len = in.read(bs)) != -1) {
				    out.write(bs, 0, len);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
        } finally {
            try {
                out.close();
                in.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("上传成功！！");
	}
	
	public File downFile(String shareUrl,String localUrl) {
		File localfile=null;     
        InputStream bis=null;     
        OutputStream bos=null;     
        try{     
            SmbFile rmifile = new SmbFile(shareUrl);     
            String filename=rmifile.getName();     
            bis=new BufferedInputStream(new SmbFileInputStream(rmifile));     
            localfile=new File(localUrl+File.separator+filename);    
            System.out.println("localfile=="+localfile);  
            bos=new BufferedOutputStream(new FileOutputStream(localfile));     
            int length=rmifile.getContentLength();    
            System.out.println("length=="+length);  
            byte[] buffer=new byte[length];     
            bis.read(buffer);    
            bos.write(buffer);  
            
        } catch (Exception e){      
            System.out.println(e.getMessage());     
        }finally{     
            try {     
                bos.close();     
                bis.close();     
            } catch (IOException e) {     
                e.printStackTrace();     
            }                 
        }     
        return localfile;     
    }     
	
	
	public static void main(String[] args) throws IOException {
		//String srcPath = "D:/opt/test.png";
		//String shareUrl = "\\\\192.168.137.197\\cxn\\";
		//new FileUpload().uploadFile(srcPath, shareUrl);
		//new FileUpload().downFile(srcPath, shareUrl);
		InputStream in = null; 
		OutputStream out = null; 
		try { 
		  //获取图片 
		  File localFile = new File("D:/opt/test1.bmp");
		 // String remotePhotoUrl = "\\\\192.168.137.197\\cxn\\";
		 // String remotePhotoUrl = "smb://ylcaifu.com;appupload:YLcaifu888@10.0.100.39/accessory/"; //存放图片的共享目录
		 // String remotePhotoUrl = "smb://macaijie:199312@192.168.137.197/cxn/"; //存放图片的共享目录 
		 String remotePhotoUrl = "smb://cxn:cxn@192.168.137.216/accesary/"; //存放图片的共享目录 
		  SmbFile remoteFile = new SmbFile(remotePhotoUrl+"/"+localFile.getName()); 
		  in = new BufferedInputStream(new FileInputStream(localFile)); 
		  out = new BufferedOutputStream(new SmbFileOutputStream(remoteFile)); 
		  
		  byte[] buffer = new byte[4096]; 
		  int len = 0; //读取长度 
		  while ((len = in.read(buffer, 0, buffer.length)) != -1) { 
		    out.write(buffer, 0, len); 
		  } 
		  out.flush(); //刷新缓冲的输出流 
		  System.out.println("-----------");
		}catch (Exception e) { 
		  String msg = "发生错误：" + e.getLocalizedMessage(); 
		  System.out.println(msg); 
		}finally { 
		  try { 
		    if(out != null) { 
		      out.close(); 
		    } 
		    if(in != null) { 
		      in.close(); 
		    } 
		  }catch (Exception e) {} 
		} 
	}
    
    
}

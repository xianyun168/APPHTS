package com.ciitc.webServices.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;

import com.ciitc.webServices.dao.AppMessageSendDao;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.model.AppMessageSend;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.MessageRemindRule;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

public class KfcrmCustor {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	@SuppressWarnings("rawtypes")
	private AppMessageSendDao appMessageSendDao = (AppMessageSendDao) SpringContextUtil.getBean("appMessageSendDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao =  (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	
	//查询客户列表
	public List<AppUserInfor> queryCustomList(String brokerCount,String name,String phone) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		List<AppUserInfor> appUserInforList = new ArrayList<AppUserInfor>();
		try {
			call=conn.prepareCall("select c_custno,     " + 
					"       c_custname,    " + 
					"       case when c_sex = '0' then '0' else '1' end sexs ,  " + 
					"       c_birthday, " + 
					"       c_mobileno, " + 
					"       c_address,  " + 
					"       CASE WHEN (FN_TCUSTREGISTERREQ(c_custno)) = '已认证' then '1' else '0' end sm,  " + 
					"       CASE WHEN (fn_tinvestor(c_custno)) = '已认证' then '2' else '0' end zy,   " + 
					"       CASE WHEN (fn_tqualifyinvestor(c_custno)) = '再次认证' then '2' else '0' end  hg " + 
					" from vcustomerinfo@crmlink t1 " + 
					" where exists (select 1 from tfundbrokerrelation@crmlink t2 where t1.c_custno = t2.c_custno " + 
					" and t2.c_brokeraccount = '"+brokerCount+"') and (C_CUSTNAME like '%"+name+"%' or C_MOBILENO like '%"+phone+"%') ");
			rs = call.executeQuery();
  			while (rs.next()) {
  				AppUserInfor appUserInfor = new AppUserInfor();
  				appUserInfor.setCustomerId(rs.getString("c_custno"));//编号
  				appUserInfor.setCustomerAddress(rs.getString("c_address"));//地址
  				appUserInfor.setUserName(rs.getString("c_custname"));//姓名
  				appUserInfor.setPhoneNumber(rs.getString("c_mobileno"));//手机号
  				appUserInfor.setCustomerSex(rs.getString("sexs"));//性别
  				appUserInfor.setNameStatus(rs.getString("sm"));//实名
  				appUserInfor.setHgState(rs.getString("hg"));
  				appUserInfor.setZyState(rs.getString("zy"));
  				//根据客户编号查询客户等级（本地有）
  				AppUserInfor appUserInfor1 =appUserInforDao.queryByCustomerId(rs.getString("c_custno"));
  				if(null != appUserInfor1) {
  					appUserInfor.setCustomerLevel(appUserInfor1.getCustomerLevel());
  				}else {
  					//
  					/*SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					String nowDate = sdf.format(new Date());
					Integer a = Integer.parseInt(nowDate.substring(0, 4))-1;
					String nowDateYear =String.valueOf(a);
					String dates = nowDateYear+"1231";
					String datey = nowDateYear+"0101";*/
  					String level = CalculationIntegral.CustomerLevelBycustoNo1(rs.getString("c_custno"));
  					appUserInfor.setCustomerLevel(level);
  				}
  				appUserInforList.add(appUserInfor);
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
				try {
					if( null != rs) {
					rs.close();
					}
					if(null != call) {
						call.close();
					}
					if(null != conn){
						conn.close();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
			}
		}
		return appUserInforList;
	}
	//已经实名的客户
	public List<AppUserInfor> querySMCustomList(String brokerCount) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		List<AppUserInfor> appUserInforList = new ArrayList<AppUserInfor>();
		try {
			call=conn.prepareCall(" select * from (select c_custno,        c_custname,  " + 
					"     CASE WHEN (FN_TCUSTREGISTERREQ(c_custno)) = '已认证' then '1' else '0' end aa  " + 
					"	 from vcustomerinfo@crmlink t1 where exists (select 1 from tfundbrokerrelation@crmlink t2 where t1.c_custno = t2.c_custno  " + 
					" and t2.c_brokeraccount = '"+brokerCount+"') " + 
					"	 ) where aa = '1'");
			rs = call.executeQuery();
  			while (rs.next()) {
  				AppUserInfor appUserInfor = new AppUserInfor();
  				appUserInfor.setCustomerId(rs.getString("c_custno"));//编号
  				appUserInfor.setUserName(rs.getString("c_custname"));//姓名
  				appUserInforList.add(appUserInfor);
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return appUserInforList;
	}
	//查询实名认证状态
	public String getStateBycustNo(String customerId) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		String state = "";
		try {
			call=conn.prepareCall(" select FN_TCUSTREGISTERREQ('"+customerId+"') states from dual");
			rs = call.executeQuery();
  			while (rs.next()) {
  				state = rs.getString("states");
  				
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return state;
	}
	//查询合格投资者认证状态
	public String queryInvestorAuthenticationState(String customerId) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		String state = "";
		try {
			call=conn.prepareCall("select FN_TQUALIFYINVESTOR('"+customerId+"') states from dual");
			rs = call.executeQuery();
  			while (rs.next()) {
  				state = rs.getString("states");
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return state;
	}
	//查询专业投资者认证状态
	public String queryProfessionalAuthState(String customerId) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		String state = "";
		try {
			call=conn.prepareCall("select FN_TINVESTOR('"+customerId+"') states from dual");
			rs = call.executeQuery();
  			while (rs.next()) {
  				state = rs.getString("states");
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return state;
	}
	//产品额度提醒
	public void productEndDate(String productNo) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		CRM_Expiration expiration =new CRM_Expiration();
		//根据产品的编号查询项目编号
		String l_proserialno = reservationUtil.getProserialno(productNo);
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		try {
			//某产品预约金额限制
			call = conn.prepareCall("select fn_amtleft('"+l_proserialno+"') money from dual");
			// 执行
			rs = call.executeQuery();
			while (rs.next()) {
				//产品剩余额度
				Integer money = rs.getInt("money");
				PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
				//查看配置的认购结束期，
				MessageRemindRule messageRemindRule = expiration.getMessageRemindRule("产品额度提醒");
				if((money<=messageRemindRule.getRemainMoney()*10000)&&(money>0)) {
					//根据产品编号查询产品名称
					//产品名称
					String productName = reservationUtil.getProductName(productNo);
					//推送内容
					String content = messageRemindRule.getMessageMould();
					content = content.replace("${remainMoney}", money.toString());
					content = content.replace("${productName}", productName);
					//推送标签tag
					Set<String> tag = new HashSet<String>();
					tag.add("0001");
					AppMessageSend appMessageSend = new AppMessageSend();
					appMessageSend.setPkId(UUID.randomUUID().toString());//主键
					appMessageSend.setFlag("03");
					appMessageSend.setMessageTitle("产品额度");
					appMessageSend.setMessageContent(content);
					appMessageSend.setSendObject("0001");
					appMessageSend.setSendState("未推送");
					appMessageSend.setSendDate("-");
					appMessageSend.setOperateName("-");
					appMessageSend.setTimingSendDate(new Date());
					appMessageSend.setMessageType("常规消息");
					appMessageSend.setSendType("系统");
					Date now = new Date();
					appMessageSend.setTimingSendDate(new Date(now.getTime()+60000));
					appMessageSendDao.insertSelective(appMessageSend);
					//调用app推送的方法，推送给公司所有员工
					String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "产品额度", content, tag);
					if("00".equals(states)) {
						SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						AppMessageSend appMessageSend1 = new AppMessageSend();
						appMessageSend1.setSendState("已推送");
						appMessageSend1.setSendDate(sf.format(new Date()));
						appMessageSend1.setPkId(appMessageSend.getPkId());
						appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
	}
	//根据客户的电话查询所属公司
	public String queryCompanyByPhone(String phone) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeKfCrm();
		CallableStatement call = null;
		ResultSet rs = null;
		String state = "";
		try {
			call=conn.prepareCall(" select get_company_nm("+phone+") companyName from dual");
			rs = call.executeQuery();
  			while (rs.next()) {
  				state = rs.getString("companyName");
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return state;
	}
	//根据手机号查询crm客户表是否存在两条手机号相同的数据
	public int phoneNumber(String phoneNo) {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		ResultSet rs = null;
		int state = 0 ;
		try {
			call=conn.prepareCall(" select count(*) num from TUNREGISTER where C_MOBILENO = '"+phoneNo+"'");
			rs = call.executeQuery();
  			while (rs.next()) {
  				state = rs.getInt("num");
  			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			try {
				if( null != rs) {
				rs.close();
				}
				if(null != call) {
					call.close();
				}
				if(null != conn){
					conn.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
		}
	}
		return state;
	}
}

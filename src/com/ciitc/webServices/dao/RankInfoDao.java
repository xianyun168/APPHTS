package com.ciitc.webServices.dao;



import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface RankInfoDao<T> extends BaseDao<T> {
    String queryById(String pkId);

}

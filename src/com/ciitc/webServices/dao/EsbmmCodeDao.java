package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import com.ciitc.webServices.model.EsbmmCode;

public interface EsbmmCodeDao<T> extends BaseDao<T> {
	public List<EsbmmCode>  queryByTypeId(Object codeTypePkId);
	public List<EsbmmCode>  queryByTypeIds(Object codeTypePkId);
	public List<EsbmmCode> queryByIdcard(Object codeTypePkId);
	public void deleteByTypeId(Object id);
	public EsbmmCode getCodeName(Map<String, Object> paramMap);
	public EsbmmCode queryByCodeName(Map<String, Object> paramMap);
	public List<EsbmmCode> checkCode(Map<String, Object> paramMap);
	public List<EsbmmCode> queryByChineseName(Map<String, Object> paramMap);
	public EsbmmCode queryByCodeCode(Map<String, Object> paramMap);
	public Integer queryByTypeName(Map<String, Object> paramMap);
	public EsbmmCode findByCodeName(Map<String, Object>paramMap);
	public List<EsbmmCode> queryTypeName(Map<String, Object>paramMap);
	/**
	 * 
	 * @param paramMap
	 * @return
	 */
	List<EsbmmCode> selectByContion(Map<String, Object> paramMap);

	public List<EsbmmCode> queryByCodecodeName(Map<String, Object> paramMap);
	
	//add by renqingqing 20150115
	public List<EsbmmCode> getServiceBySys(Map<String, Object> paramMap);
	
	public String getCodeByName(String codeName);
	public void update1(EsbmmCode esbmmCode1);
}

package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.Sign;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CPTfundbrokerDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String cBrokeraccount);

    int insert(CPTfundbroker record);

    int insertSelective(CPTfundbroker record);

    CPTfundbroker selectByPrimaryKey(String cBrokeraccount);

    int updateByPrimaryKeySelective(CPTfundbroker record);

    int updateByPrimaryKey(CPTfundbroker record);

    //删除全部数据
	void deleteAll();
	//查询是否已经签到
	Sign queryByNow(Map<String, Object> map1);
	
	//备份数据
	List<CPTfundbroker> queryALL();

	CPTfundbroker queryByFid(String cBrokeraccount);

	String selectNameByKey(String financialId);
}
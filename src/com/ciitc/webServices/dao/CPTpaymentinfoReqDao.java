package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.CPTpaymentinfoReq;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CPTpaymentinfoReqDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(Long lSerialno);

    int insert(CPTpaymentinfoReq record);

    int insertSelective(CPTpaymentinfoReq record);

    CPTpaymentinfoReq selectByPrimaryKey(Long lSerialno);

    int updateByPrimaryKeySelective(CPTpaymentinfoReq record);

    int updateByPrimaryKey(CPTpaymentinfoReq record);
}
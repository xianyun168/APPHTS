package com.ciitc.webServices.dao;

import java.util.ArrayList;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

import com.ciitc.webServices.model.ProductAppendixInfo;

public interface ProductAppendixInfoDao<T> extends BaseDao<T> {
	int deleteByPrimaryKey(String pkId);

	int insert(ProductAppendixInfo record);

	int insertSelective(ProductAppendixInfo record);

	ArrayList<ProductAppendixInfo> selectByPrimaryKey(String pkId);
	
	ArrayList<ProductAppendixInfo> selectByProductid(String Id);
	ArrayList<ProductAppendixInfo> selectByProductidForCrm(String Id);

	int updateByPrimaryKeySelective(ProductAppendixInfo record);

	int updateByPrimaryKey(ProductAppendixInfo record);
	
	public void deleteFile(String pkId);
}
package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.ContractApplication;

public interface ContractApplicationDao {
    int deleteByPrimaryKey(String pkId);

    int insert(ContractApplication record);

    int insertSelective(ContractApplication record);

    ContractApplication selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(ContractApplication record);

    int updateByPrimaryKey(ContractApplication record);
}
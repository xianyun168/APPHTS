package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.HelpDocument;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface HelpDocumentDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(HelpDocument record);

    int insertSelective(HelpDocument helpDocument);

    HelpDocument selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(HelpDocument helpDocument);

    int updateByPrimaryKey(HelpDocument record);

   //检查标题是否已经存在
	List<HelpDocument> queryByTitle(Map<String, Object> paramMap);

	//帮助文档列表查询接口
	List<HelpDocument> queryByListAll(String title);
	//修改统计访问次数(每次访问次数+1)
	int updateByKey(HelpDocument helpDocument);
}
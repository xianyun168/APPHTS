package com.ciitc.webServices.dao;

import java.util.List;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface ViewEmplRelationDao <T> extends BaseDao<T> {
	 
	List<T> selectByLoginId(String userCode);
}
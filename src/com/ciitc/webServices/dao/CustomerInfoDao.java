package com.ciitc.webServices.dao;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

import com.ciitc.webServices.model.CustomerInfo;

public interface CustomerInfoDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(CustomerInfo record);

    int insertSelective(CustomerInfo record);

    CustomerInfo selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(CustomerInfo record);

    int updateByPrimaryKey(CustomerInfo record);
    //删除全部
  	void deleteAll();
}
package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.FileUploads;

public interface FileUploadDao {
    int deleteByPrimaryKey(String pkId);

    int insert(FileUploads record);

    int insertSelective(FileUploads record);

    FileUploads selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(FileUploads record);

    int updateByPrimaryKey(FileUploads record);
    
    FileUploads queryById(String oldName);
    
    
}
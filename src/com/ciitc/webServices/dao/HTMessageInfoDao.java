package com.ciitc.webServices.dao;


import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.HTMessageInfo;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface HTMessageInfoDao<T> extends BaseDao<T>{

	int insertSelective(HTMessageInfo hTMessageInfo);

	List<HTMessageInfo> queryByBirthday(Map<String, Object> map);

}
package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface MenuInfoDao<T> extends BaseDao<T> {
	public List<T> queryByParentId(Map<String, Object> paramMap);
	
	public List<T> query();
	
	public int queryMenuAuthCount(Map<String, Object> paramMap);
	
	public List<T> queryMenuByPath(Map<String, Object> paramMap);
	
	public List<T> getMenuName(Map<String, Object> paramMap);
	
	public String getMenuPath(String pkid) throws Exception;
}

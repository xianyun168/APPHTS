package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.MessageRemindRule;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface MessageRemindRuleDao<T> extends BaseDao<T> {
	
	public MessageRemindRule queryByBirth(String flag);

	public MessageRemindRule queryByName(String messageRemindRuleName);
}
package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface DepartDao<T> extends BaseDao<T> {
	public List<T> queryByParentId(Map<String, Object> paramMap);
	
	public List<T> query();
	
	public String queryJK(String pkId);
}

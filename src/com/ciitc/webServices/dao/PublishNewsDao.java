package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;
import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import com.ciitc.webServices.model.PublishNewsInfo;


public interface PublishNewsDao<T>  extends BaseDao<T>{
	public abstract List<T> queryByList1(Map<String, Object> paramMap);
	public abstract int queryByCount1(Map<String, Object> paramMap);
	public abstract List<T> queryByList2(Map<String, Object> paramMap);
	public abstract int queryByCount2(Map<String, Object> paramMap);
	//修改统计访问次数(每次访问次数+1)
    int updateByKey(PublishNewsInfo publishNewsInfo);
    public abstract List<T> queryByListj();
    public abstract List<T> queryByListj1();
    public abstract List<T> queryByListj2();
    public abstract List<T> queryByListjlb();
    PublishNewsInfo selectByPrimaryKey(String pkId);
    PublishNewsInfo selectByPrimaryKeylb(String pkId);
    public abstract List<T> queryQuartzList();
    public abstract List<T> queryQuartzListlb();
    public List<T> queryByNews();
    public abstract List<T> queryByList3(Map<String, Object> paramMap);
    public abstract int queryByCount3(Map<String, Object> paramMap);
    public abstract List<T> queryByLists(Map<String, Object> paramMap);
    public abstract int queryByCounts(Map<String, Object> paramMap);
}
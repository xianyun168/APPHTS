package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;

import com.ciitc.webServices.model.AppGoodsInfor;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年3月8日 下午3:40:11
 * 修 改 人: 
 ******************************************************************************/
public interface AppGoodsInforDao<T> extends BaseDao<T>{

	public abstract List<T> selectList(Map<String,Object> map);
	
	public abstract Integer selectgoodsCount(Map<String,Object> map);

	public abstract AppGoodsInfor queryByGoodsId(String productId);
	
	public abstract void updateBygoodsId(AppGoodsInfor appGoods);
}

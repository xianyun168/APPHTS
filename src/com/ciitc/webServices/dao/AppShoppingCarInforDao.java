package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.ciitc.webServices.model.AppShoppingCarInfor;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月10日 上午11:13:26
 * 修 改 人: 
 ******************************************************************************/
public interface AppShoppingCarInforDao<T> extends BaseDao<T>{
	public List<T> queryByinfo(Map<String,Object> map);
	
	public Integer queryCarCount(String userId);
	
	public Integer queryBynum(Map<String,Object> map);
	
	public void deleteAll(Map<String, Object> map);
	//查询要提交的商品列表
	public List<AppShoppingCarInfor> queryByIds(Map<String, Object> paramMap);
	//根据商品的id和用户的id查询购物车里的商品
	public List<AppShoppingCarInfor> queryByGoodsId(Map<String, Object> map);
	//根据购物车商品的id删除这条信息
	public void deleteById(String pkId);
}

package com.ciitc.webServices.dao;

import java.util.List;
import com.ciitc.webServices.model.PerformanceGoals;
import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
public interface PerformanceGoalsDao<T> extends BaseDao<T> {

	List<PerformanceGoals> queryList(String rank);

   List<PerformanceGoals> selectById(String ids);
   
   List<PerformanceGoals> selectByRank(String rank);
   
   void insert(PerformanceGoals performanceGoals);
   
   int delete(String pkId);
   }

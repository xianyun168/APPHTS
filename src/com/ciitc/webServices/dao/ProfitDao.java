package com.ciitc.webServices.dao;

import java.util.List;
import com.ciitc.webServices.model.Profit;
import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface ProfitDao<T> extends BaseDao<T> {

	 List<Profit> queryByPhoneList(String phoneNumber);
	 
	 List<Profit> queryPositionList(String phoneNumber);

	 List<Profit> queryByHoldingIncomeList(String phoneNumber);
	 
	 List<Profit> queryByTotalAmountList(String phoneNumber);
	 
	 List<Profit> queryTransactionList(String phoneNumber);
	 
	 List<Profit> queryHistoryProfit(String phoneNumber);
	 
	 List<Profit> queryProfitDetailed(String productId);

    }

package com.ciitc.webServices.dao;


import java.util.List;

import com.ciitc.webServices.model.ActivityBasicInfor;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface ActivityBasicInforDao<T> extends BaseDao<T> {
	
	public List<ActivityBasicInfor> queryByList1();
	
	ActivityBasicInfor selectByPrimaryKey(String pkId);
	//根据状态查询活动列表
	public List<ActivityBasicInfor> queryByState(String state);
	//更新活动状态
	public void updateByState(ActivityBasicInfor activityBasicInfor);

}
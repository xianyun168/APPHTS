package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.Notice;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface NoticeDao<T> extends BaseDao<T>  {
    int deleteByPrimaryKey(String pkId);

    int insert(Notice record);

    int insertSelective(Notice record);

    Notice selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(Notice record);

    int updateByPrimaryKey(Notice record);

	Notice selectAll();
}
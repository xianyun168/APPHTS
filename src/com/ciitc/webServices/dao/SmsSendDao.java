package com.ciitc.webServices.dao;


import java.util.Map;

import com.ciitc.webServices.model.SmsSend;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface SmsSendDao<T> extends BaseDao<T> {
	 public SmsSend queryByPhone(Map<String, Object> map);

	//发送成功之后删除未发送
	public void deleteByPhone(Map<String, Object> map);
}
package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.City;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CityDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(Long cid);

    int insert(City record);

    int insertSelective(City record);

    List<City> selectByPrimaryKey(String father);
    
    City selectById(String cityid);

    int updateByPrimaryKeySelective(City record);

    int updateByPrimaryKey(City record);
}
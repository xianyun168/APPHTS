package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.AppOrderInfor;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface AppOrderInforDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(AppOrderInfor record);

    int insertSelective(AppOrderInfor record);

    AppOrderInfor selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(AppOrderInfor record);

    void updateByPrimaryKey(AppOrderInfor record);

	List<AppOrderInfor> queryByuser(String purchaseId);

	List<AppOrderInfor> queryByOrderId(String orderId);
	
	AppOrderInfor queryOrderId(String orderId);
}
package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.CPReservation;

public interface CPReservationDao {
    int deleteByPrimaryKey(String pkId);

    int insert(CPReservation record);

    int insertSelective(CPReservation record);

    CPReservation selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(CPReservation record);

    int updateByPrimaryKey(CPReservation record);
    //删除这个理财师和这个产品下的所有预约
	void deleteByAll(CPReservation reservation);

	List<CPReservation> selectByBF(CPReservation reservation);
}
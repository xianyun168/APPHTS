package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.TsaleProjectInfo;

public interface TsaleProjectInfoDao {
    int deleteByPrimaryKey(String pkId);

    int insert(TsaleProjectInfo record);

    int insertSelective(TsaleProjectInfo record);

    TsaleProjectInfo selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(TsaleProjectInfo record);

    int updateByPrimaryKey(TsaleProjectInfo record);
    
    //备份
	List<TsaleProjectInfo> queryALL();
	
	//删除所有数据
	void deleteALL();

}
package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;

import com.ciitc.webServices.model.EquitiesDetail;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface EquitiesDetailDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(EquitiesDetail record);

    int insertSelective(EquitiesDetail record);

    EquitiesDetail selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(EquitiesDetail record);

    int updateByPrimaryKey(EquitiesDetail record);

	List<EquitiesDetail> queryByLevel(Map<String, Object> paramMap);

	// 查询PK_ID
	EquitiesDetail getPkId(Map<String, Object> param);
	//根据父产品编号查询父产品名称
	String queryByPid(String gkProductPId);
	//根据子产品编号查询子产品名称
	EquitiesDetail queryById1(Map<String, Object> paramMap);

}
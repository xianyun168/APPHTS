package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface OperationDao<T> extends BaseDao<T> {
	
	public List<T> queryByMenuId(Map<String, Object> paramMap);
	
}

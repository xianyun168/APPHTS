package com.ciitc.webServices.dao;



import com.ciitc.webServices.model.FinancialInfo;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface FinancialInfoDao<T> extends BaseDao<T> {
	FinancialInfo selectByPrimaryKey(String pkId);
}
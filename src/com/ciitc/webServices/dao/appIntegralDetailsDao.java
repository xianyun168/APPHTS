package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import org.apache.poi.ss.formula.functions.T;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

import com.ciitc.webServices.model.appIntegralDetails;

public interface appIntegralDetailsDao<T> extends BaseDao<T>{
    int deleteByPrimaryKey(String pkId);

    int insert(appIntegralDetails record);

    int insertSelective(appIntegralDetails record);

    appIntegralDetails selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(appIntegralDetails record);

    int updateByPrimaryKey(appIntegralDetails record);
    
    public List<T> selectByuserId(Map<String,Object> map);
    
    public List<T> selectUserId(Map<String,Object> map);
    
    public long selectUserIdSum(Map<String,Object> map);
    
    public List<T> selectOverdue();
    
    List<T> queryByList1(Map<String, Object> paramMap);
    
    List<T> queryByReserveno(String reserveno);

	List<appIntegralDetails> queryByYX(String pkId);

	appIntegralDetails queryByYXMin(String pkId);

}
package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.Collection;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CollectionDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(Collection record);

    int insertSelective(Collection record);

    Collection selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(Collection record);

    int updateByPrimaryKey(Collection record);
    //查询收藏列表接口
	List<Collection> selectList(Collection collection);
	//查询单个收藏接口
	Collection selectOne(Collection collection);
	//根据对象取消收藏
	void deleteBy(Collection collection);
	//查看这条知识库有没有被收藏过
	Collection selectByTo(Map<String, Object> scap);
	//删除收藏列表里的这条数据
	void deleteByCid(String pkId);
}
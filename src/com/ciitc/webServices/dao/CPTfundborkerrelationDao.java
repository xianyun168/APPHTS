package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.CPTfundborkerrelation;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CPTfundborkerrelationDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String cCustno);

    int insert(CPTfundborkerrelation record);

    int insertSelective(CPTfundborkerrelation record);

    CPTfundborkerrelation selectByPrimaryKey(String cCustno);

    int updateByPrimaryKeySelective(CPTfundborkerrelation record);

    int updateByPrimaryKey(CPTfundborkerrelation record);

    //删除全部
	void deleteAll();
	
	//备份
	List<CPTfundborkerrelation> queryALL();
}
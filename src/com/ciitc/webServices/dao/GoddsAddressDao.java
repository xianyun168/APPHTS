package com.ciitc.webServices.dao;

import java.util.Map;

import com.ciitc.webServices.model.GoddsAddress;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface GoddsAddressDao<T> extends BaseDao<T>{
    int deleteByPrimaryKey(String pkId);

    int insert(GoddsAddress record);

    int insertSelective(GoddsAddress record);

    GoddsAddress selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(GoddsAddress record);

    int updateByPrimaryKey(GoddsAddress record);

	GoddsAddress selectByState(Map<String, Object> map);
}
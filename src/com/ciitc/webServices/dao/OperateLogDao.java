package com.ciitc.webServices.dao;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

import com.ciitc.webServices.model.OperateLog;

public interface OperateLogDao<T> extends BaseDao<T> {
	
	public void add(OperateLog  operateLog);
}

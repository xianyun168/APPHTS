package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.CPTreserve;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CPTreserveDao<T> extends BaseDao<T> {
    int insert(CPTreserve record);

    int insertSelective(CPTreserve record);
}
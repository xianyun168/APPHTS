package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.Sign;

public interface SignDao {
    int deleteByPrimaryKey(String pkId);

    int insert(Sign record);

    int insertSelective(Sign record);

    Sign selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(Sign record);

    int updateByPrimaryKey(Sign record);
    //查询直系下级的签到信息
    List<Sign> queryByLeaderId(Map<String, Object> signMap);
    //查询自己的签到信息
    List<Sign> queryByFinancialId(Map<String, Object> signMap);
    //查询理财师是否已签到
	Sign queryByNow(Map<String, Object> map1);
    
}
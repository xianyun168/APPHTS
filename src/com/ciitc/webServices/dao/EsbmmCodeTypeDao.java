package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.vo.CodeFactoryVo;

public interface EsbmmCodeTypeDao<T> extends BaseDao<T> {
	
	public List<CodeFactoryVo> getCodeFactory(Map<String, Object> paramMap);
	public CodeFactoryVo queryByCodeName(Map<String, Object> paramMap);
	public List<EsbmmCodeType> queryAll();
	public List<EsbmmCodeType> checkCodeType(Map<String, Object> paramMap);
	/**
	 * yutao 2014-12-16
	 * 根据TypeName查询EsbmmCodeType表
	 * @param typeName
	 * @return
	 */
	public List<EsbmmCodeType> queryByTypeName(String typeName);
	
	public List<EsbmmCodeType> queryById(String id);


}

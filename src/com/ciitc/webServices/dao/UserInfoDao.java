package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

import com.ciitc.webServices.model.UserInfo;

public interface UserInfoDao<T> extends BaseDao<T> {
	
	public List<T>  getUserExits(Map<String, Object> paramMap);
	
	public List<T>  getUserForRoute(Map<String, Object> paramMap);
	
	public List<T> queryByCode(Map<String, Object> paramMap);
	
	public List<T> queryAll();
	
	public List<T> queryByRoutePkId(Map<String, Object> paramMap);
	
	public List<T> queryUsers();
	
	public List<T> queryUserCode(Map<String, Object> paramMap);
	
	public List<T> queryExistUserList(Map<String, Object> paramMap);
	
	String getTokenByCode(Map<String, Object> paramMap);
	
	public UserInfo getTokenByCodeJK(String userCode);
	
	public List<T> queryByName(Map<String, Object> paramMap);
	
	public List<T> queryByDepartPkId(Map<String, Object> paramMap);
}

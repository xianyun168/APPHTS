package com.ciitc.webServices.dao;


import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.AppMessageSend;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface AppMessageSendDao<T> extends BaseDao<T> {
	public List<T> queryByParentId(Map<String, Object> paramMap);

	public void insertSelective(AppMessageSend appMessageSend);

	public void updateByPrimaryKeySelective(AppMessageSend appMessageSend1);

	public List<AppMessageSend> queryByAll(Map<String, Object> maps);
}
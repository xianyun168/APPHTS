package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.PaymentApplication;

public interface PaymentApplicationDao {
    int deleteByPrimaryKey(String pkId);

    int insert(PaymentApplication record);

    int insertSelective(PaymentApplication record);

    PaymentApplication selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(PaymentApplication record);

    int updateByPrimaryKey(PaymentApplication record);
}
package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.AppRelation;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface AppRelationDao<T> extends BaseDao<T> {
    int insert(AppRelation record);

    int insertSelective(AppRelation record);

	String selectByLoginid(String financialcode);

	List<AppRelation> selectByUpLoginid(String userCode);
}
package com.ciitc.webServices.dao;
import java.util.Map;

public interface AppPerformanceInterfaceDao {
	Map<String, Object> queryByList(String username);

}

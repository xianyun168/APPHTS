package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.CPcfResult;
import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CPcfResultDao<T> extends BaseDao<T> {
	//收益列表
  	List<CPcfResult> querymingxiList(String Contract_Id);

    //返息时间列表
  	List<CPcfResult> queryDateList(Map<String, Object> paramMap);
  	
	//返息明细列表
  	List<CPcfResult> queryfanxiList(Map<String, Object> paramMap);
  	
    //导出
  	List<CPcfResult> queryfanxiList1(Map<String, Object> paramMap);
  	
  	public abstract int queryByCount1(Map<String, Object> paramMap);
  	
    public CPcfResult  queryById(String pkd);
  	
    void updat(CPcfResult cPcfResult);
    
    int insertSelective(CPcfResult cPcfResult);
    
    //删除全部
  	void deleteAll();
  	
  	//备份
	List<CPcfResult> queryALL();
    }

package com.ciitc.webServices.dao;


import java.util.Date;
import java.util.List;
import java.util.Map;
import com.ciitc.webServices.model.Coefficient;
import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
public interface CoefficientDao<T> extends BaseDao<T> {
    String queryById(String pkId);

    List<Coefficient> queryByDate(String productId);
    
    List<Coefficient> selectByDate(Map<String, Object> paramMap);

	public void insert(Coefficient coefficient);

	public void updateCoefficient(Date scalinDate);
}

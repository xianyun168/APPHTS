package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.RoleDetailInfo;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface RoleDetailInfoDao<T> extends BaseDao<T> {
	
	public List<T> queryByRoleId(Map<String, Object> paramMap);
	
	public List<T> queryByMenuId(Map<String, Object> paramMap);
	
	public List<T> queryByRoleIdAndMenuId(Map<String, Object> paramMap);
	
	public List<T> queryOperationList(Map<String, Object> paramMap);
	
	public List<T> getDescription(Map<String, Object> paramMap);
	
	public List<RoleDetailInfo> getMenuIds(String roIds) throws Exception;
	
	public List<RoleDetailInfo> getDescriptionByMenu(Map<String, Object> paramMap);
	
}

package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.CPTtrustcontractReq;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import com.ciitc.webServices.model.CPTtrustcontractReqV2;

public interface CPTtrustcontractReqDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(Long lSerialno);

    int insert(CPTtrustcontractReq record);

    int insertSelective(CPTtrustcontractReq record);
    int insertBatch(List<CPTtrustcontractReq> record);

    CPTtrustcontractReq selectByPrimaryKey(Long lSerialno);

    int updateByPrimaryKeySelective(CPTtrustcontractReq record);

    int updateByPrimaryKey(CPTtrustcontractReq record);
    
    List<CPTtrustcontractReq> selectByCustomerId1(Map<String, Object> para);
    //根据用户id查询客户合同信息
    List<CPTtrustcontractReq> selectByCustomerId(String pkId);
    
    //获取持仓产品列表
    List<CPTtrustcontractReq> queryChiCangList(String cMobileno);
    List<CPTtrustcontractReq> queryDaoQiShuHui(String cMobileno);
    List<CPTtrustcontractReq> queryTiQianShuHui(String cMobileno);

    //获取总收益
  	List<CPTtrustcontractReq> queryChiCangMoney(String cMobileno);
  	
    //收益列表
  	List<CPTtrustcontractReq> queryshoyiList(String cMobileno);
  	
    //删除全部
  	void deleteAll();
  	
  	//备份
	List<CPTtrustcontractReq> queryALL();
	
	//查询所有合同
	List<CPTtrustcontractReq> queryChiCangList1(String cMobileno);

    double getLeiJiShouYi(String customerId);

    double getChiCangShouYi(String customerId);

    List<CPTtrustcontractReqV2> getMyDeShouYiList(String customerId);
}
package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.AppOrderSubmit;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface AppOrderSubmitDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(AppOrderSubmit record);

    int insertSelective(AppOrderSubmit record);

    AppOrderSubmit selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(AppOrderSubmit record);

    int updateByPrimaryKey(AppOrderSubmit record);
}
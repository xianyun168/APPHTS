package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.EquitiesUseDetails;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface EquitiesUseDetailsDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(EquitiesUseDetails record);

	int insertSelective(EquitiesUseDetails eq);

    EquitiesUseDetails selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(EquitiesUseDetails record);

    int updateByPrimaryKey(EquitiesUseDetails record);

	int queryByCustomerId(Map<String, Object> mapGK);

	int updateState(Map<String, Object> paramMap);

	// 根据国康服务id查询
	EquitiesUseDetails selectByRecordId(String gkRecordId);

	EquitiesUseDetails selectByRecordId1(Map<String, Object> paramMap);

	// 根据国康服务id删除
	int deleteByRecordId(String gkRecordId);
	//根据国康服务id查询服务信息
	EquitiesUseDetails selectBySID(String gkRecordId);

	List<EquitiesUseDetails> queryByCustomerIds(String customerId);
}
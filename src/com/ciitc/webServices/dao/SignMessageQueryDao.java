package com.ciitc.webServices.dao;


import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.SignMessageQuery;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface SignMessageQueryDao<T> extends BaseDao<T>{
	
	void insert(SignMessageQuery SignMessageQuery);
	
	SignMessageQuery selectByPrimaryKey(String pkId);

	List<T> queryByList1(Map<String, Object> paramMap);

	int queryByCount1(Map<String, Object> paramMap);

	List<SignMessageQuery> queryByList2(Map<String, Object> paramMap);
	
	List<SignMessageQuery> queryByUserInfo(Map<String, Object> paramMap);

	void updateByPkId(Map<String, Object> map);
}
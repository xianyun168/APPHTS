package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.CrmMarkTitle;

public interface CrmMarkTitleDao {
    int deleteByPrimaryKey(String pkId);

    int insert(CrmMarkTitle record);

    int insertSelective(CrmMarkTitle record);

    CrmMarkTitle selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(CrmMarkTitle record);

    int updateByPrimaryKey(CrmMarkTitle record);
    
    int queryMarks(String customNo);
    
    List<CrmMarkTitle> queryMarkTitles(String customNo);
    //模糊查询
	List<CrmMarkTitle> selectByMarkTitle(CrmMarkTitle crmMarkTitle);
	//查询此标签是否已存在
	CrmMarkTitle queryByCustomNo(Map<String, Object> parmap);
    
}
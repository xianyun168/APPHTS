package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.AppPerformamcePrivInfo;

public interface AppPerformamcePrivInfoDao {
	 List<AppPerformamcePrivInfo> queryBy(String username);
	  Map<String,Object> queryCarCount(AppPerformamcePrivInfo privinfo);
}

package com.ciitc.webServices.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.junit.runners.Parameterized.Parameter;

import com.ciitc.webServices.model.AppUserInfor;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月26日 下午7:41:28
 * 修 改 人: 
 ******************************************************************************/
public interface BatchAddDao<T> extends BaseDao<T>{

	public T queryByPkId(String userId);
	
	public void updatebyuserId(AppUserInfor appUserInfor);
	
	public T queryByuserId(String pkId);
	//根据pkId增加单个用户的积分
	public void addUserInfoOne(AppUserInfor appUserInfor);

	public List<AppUserInfor> queryByAll();
	
	public void updateBycustomer(AppUserInfor appUserInfor);
	
	public T queryBycustomer(String customerId);
}

package com.ciitc.webServices.dao;

import java.util.List;

import org.apache.poi.ss.formula.functions.T;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月26日 上午11:00:25
 * 修 改 人: 
 ******************************************************************************/
public interface AppOrderDetailsDao<T> extends BaseDao<T>{

	public List<T> queryByorderId (String orderId);
}

package com.ciitc.webServices.dao;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.LoginLog;

public interface AppUserInforDao <T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insertSelective(AppUserInfor record);

    AppUserInfor selectByPrimaryKey(String pkId);
    
    AppUserInfor selectByCustomerId(String CustomerId);

    int updateByPrimaryKey(AppUserInfor record);
    
    AppUserInfor queryByPhone(String phoneNumber);    
    
    void  updateRegistration(AppUserInfor record);
    
    void insertInfor(AppUserInfor appUserInfor);
    
    public AppUserInfor queryByPhoneNumber(String phoneNumber);
    
    List<T> queryByList1(Map<String, Object> paramMap);
    
    AppUserInfor querykhId(String pkId);
    
    AppUserInfor queryCrmId(String pkId);  
    
    List<AppUserInfor> queryByPkId(String pkId);
    
    AppUserInfor queryByPhoneId(String pkId);

	AppUserInfor queryByPkIdOne(String pkId);
	
	AppUserInfor queryByCustomerId(String customerId);
	//根据理财师id查询本地用户表里的数据
	List<AppUserInfor> queryByFinancialId(String financialId);
	//模糊查询
	List<AppUserInfor> selectByUser(AppUserInfor appUserInfor);
	//查询生日
	List<AppUserInfor> queryByBirthday(String birthday);

	List<AppUserInfor> queryByFinancialId1(String financialId);

	List<AppUserInfor> queryALL();

	void deleteAll();

	void updateSmState(AppUserInfor appUserInfor);

	void updateBy(AppUserInfor appUserInfor);

	void updateByRegion(AppUserInfor appUserInfor);

	void updateByPKey(AppUserInfor appUserInfor);
	//删除客户编号为空的数据
	void deleteByCustomId();
	//根据客户编号查询是否有多个
	Integer queryByCounts(String customId);

	List<AppUserInfor> queryTest();

	AppUserInfor queryByPhone1(String phoneNumber);

	void update1(AppUserInfor appUserInfor);

	int queryByPhoneNum(String phoneNumber);

    void addLoginLog(LoginLog loginLog);
}
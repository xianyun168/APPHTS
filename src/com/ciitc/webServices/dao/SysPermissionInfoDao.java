package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import com.ciitc.webServices.model.SysPermissionInfo;

public interface SysPermissionInfoDao<T> extends BaseDao<T> {
	public List<SysPermissionInfo> queryByUserPkId(String userPkId);
	public void deleteByUserPkId(String userPkId);
	
	public List<T> queryRoleUserByRoleId(Map<String, Object> paramMap);
	
	public String getRoleNameByUserPkId(String userPkId);
	
	public List<SysPermissionInfo> getRoleIdByUserPkId(String userPkId);
}

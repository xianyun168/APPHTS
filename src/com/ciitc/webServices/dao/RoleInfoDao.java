package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import com.ciitc.webServices.model.RoleInfo;

public interface RoleInfoDao<T> extends BaseDao<T> {
	
	public List<T> queryByName(Map<String, Object> paramMap);

	public List<RoleInfo> queryAll();
}

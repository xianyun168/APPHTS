package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface RoleDepartDao<T> extends BaseDao<T>  {
	
	public List<T> queryByRoleId(Map<String, Object> paramMap);
	
	public List<T> queryByDepartId(Map<String, Object> paramMap);
	
	public List<T> queryByRoleIdAndDepartId(Map<String, Object> paramMap);
	
}

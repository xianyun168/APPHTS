package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.MemoInfo;

public interface MemoInfoDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(MemoInfo record);

    int insertSelective(MemoInfo record);

    MemoInfo selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(MemoInfo record);

    int updateByPrimaryKey(MemoInfo record);
    //备忘信息查询接口
	List<MemoInfo> selectMonth(MemoInfo memoInfo);
}
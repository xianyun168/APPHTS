package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.Order;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface OrderDao<T> extends BaseDao<T>{
    int deleteByPrimaryKey(String pkId);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
}
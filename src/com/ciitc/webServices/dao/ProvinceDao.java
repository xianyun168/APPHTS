package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.Province;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface ProvinceDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(Long sid);

    int insert(Province record);

    int insertSelective(Province record);

    Province selectByPrimaryKey(Long sid);
    
    List<Province> selectAll();

    int updateByPrimaryKeySelective(Province record);

    int updateByPrimaryKey(Province record);

	Province selectById(String provinceid);
}
package com.ciitc.webServices.dao;

import java.util.Map;

import com.ciitc.webServices.model.ProductFk;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface ProductFkDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(ProductFk record);

    int insertSelective(ProductFk record);

    ProductFk selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(ProductFk record);

    int updateByPrimaryKeyWithBLOBs(ProductFk record);

    int updateByPrimaryKey(ProductFk record);

	ProductFk selectBypid(String pkId);
}
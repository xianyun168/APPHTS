package com.ciitc.webServices.dao;

import java.util.List;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface OrganizaInfoDao<T> extends BaseDao<T> {
	public List<T> queryByParentId(Object id);
}
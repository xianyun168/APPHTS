package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.Area;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface AreaDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(Long rid);

    int insert(Area record);

    int insertSelective(Area record);

    List<Area> selectByPrimaryKey(String father);
    
    Area selectById(String areaid);

    int updateByPrimaryKeySelective(Area record);

    int updateByPrimaryKey(Area record);
}
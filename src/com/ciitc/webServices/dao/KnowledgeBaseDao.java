package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.KnowledgeBase;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface KnowledgeBaseDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(KnowledgeBase record);

    int insertSelective(KnowledgeBase knowledgeBase);

    KnowledgeBase selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(KnowledgeBase knowledgeBase);

    int updateByPrimaryKey(KnowledgeBase record);
    //检查标题是否已经存在
	List<KnowledgeBase> queryByTitle(Map<String, Object> paramMap);
	//查询知识库列表
	List<KnowledgeBase> queryByListAll(String keyWord);
}
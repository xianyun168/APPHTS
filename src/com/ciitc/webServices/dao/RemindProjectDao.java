package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.RemindProject;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface RemindProjectDao<T> extends BaseDao<T>{
    int insert(RemindProject record);

    int insertSelective(RemindProject record);

	List<String> selectAll();
}
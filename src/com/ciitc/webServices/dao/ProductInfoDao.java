package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import com.ciitc.webServices.model.ProductInfo;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface ProductInfoDao<T> extends BaseDao<T> {
	int deleteByPrimaryKey(String pkId);

	int insert(ProductInfo record);

	int insertSelective(ProductInfo record);

	ProductInfo selectByPrimaryKey(String pkId);

	int updateByPrimaryKeySelective(ProductInfo record);

	int updateByPrimaryKeyWithBLOBs(ProductInfo record);

	int updateByPrimaryKey(ProductInfo record);

	public List<T> queryList(Map<String, Object> paramMap);

	public abstract List<T> queryByListj(Map<String, Object> paramMap);	
	public abstract List<T>queryByProfit(Map<String, Object> paramMap);
	
	public abstract List<T>queryByPermin(Map<String, Object> paramMap);
	
	public abstract List<T>queryByDeadline(Map<String, Object> paramMap);

	public void deleteALL();

	public List<T> queryALL();

	List<ProductInfo> queryByProfit1(Map<String, Object> map1);

	List<ProductInfo> queryByDeadline1(Map<String, Object> map1);

	List<ProductInfo> queryByPermin1(Map<String, Object> map1);
	//根据产品编号查询产品名称
	String queryByPid(String pkId);
	//更新crm折标系数
	int updateCrmCoefficient(ProductInfo record);
	//查询产品状态
		String queryByState(String pkId);
}
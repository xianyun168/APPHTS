package com.ciitc.webServices.dao;

import java.util.List;

import com.ciitc.webServices.model.CPTbrokertree;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CPTbrokertreeDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String cNodecode);

    int insert(CPTbrokertree record);

    int insertSelective(CPTbrokertree record);

    int updateByPrimaryKeySelective(CPTbrokertree record);

    int updateByPrimaryKey(CPTbrokertree record);

    //删除全部
	void deleteAll();
	
	public List<T> queryByParentId(Object id);
	
	//备份
	List<CPTbrokertree> queryALL();
	//根据部门编号查询上级部门
	CPTbrokertree queryParentById(String id);
}
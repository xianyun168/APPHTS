package com.ciitc.webServices.dao;

import java.util.List;
import java.util.Map;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface AppCustomerInforDao<T> extends BaseDao<T> {

	  public abstract int queryByCount2(Map<String, Object> paramMap);
	  
	  public abstract List<T> queryByList2(Map<String, Object> paramMap);
}
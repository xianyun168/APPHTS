package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.CompanyInfo;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface CompanyInfoDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(CompanyInfo record);

    int insertSelective(CompanyInfo record);

    CompanyInfo selectByPrimaryKey();

    int updateByPrimaryKeySelective(CompanyInfo record);

    int updateByPrimaryKey(CompanyInfo record);
}
package com.ciitc.webServices.dao;

import com.ciitc.webServices.model.Opinion;

import cn.com.ins.sinosoft.framework.base.dao.BaseDao;

public interface OpinionDao<T> extends BaseDao<T> {
    int deleteByPrimaryKey(String pkId);

    int insert(Opinion record);

    int insertSelective(Opinion record);

    Opinion selectByPrimaryKey(String pkId);

    int updateByPrimaryKeySelective(Opinion record);

    int updateByPrimaryKey(Opinion record);
}
package com.ciitc.webServices.interfacesCrm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.MemoInfoDao;
import com.ciitc.webServices.model.MemoInfo;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_MemoInfoController extends BaseController{
	private MemoInfoDao memoInfoDao = (MemoInfoDao) SpringContextUtil.getBean("memoInfoDao");
	/*
	 * 保存备忘信息接口
	 */
	@RequestMapping(value = "saveMemoInfo",method=RequestMethod.GET)
	@ResponseBody
	public String saveMemoInfo(HttpServletRequest request,JSONObject json) {
		// 
		String state = null;
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("financialId") && json.has("infoMomeDate") && json.has("infoMome")) {
			String financialId = json.getString("financialId");
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd");
			String date = json.getString("infoMomeDate");
			Date infoMomeDate = null;
			try {
				infoMomeDate = sdf.parse(date);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			String infoMome = json.getString("infoMome");
			String uuid = UUID.randomUUID().toString();
			MemoInfo memoInfo = new MemoInfo();
			if((null != financialId && !"".equals(financialId))&& (null != infoMomeDate)
					&&(null != infoMome && !"".equals(infoMome))) {
				memoInfo.setFinancialId(financialId);
				memoInfo.setInfoMome(infoMome);
				memoInfo.setInfoMomeDate(infoMomeDate);
				memoInfo.setCreatDate(new Date());
				memoInfo.setPkId(uuid);
				try {
					memoInfoDao.insertSelective(memoInfo);
					//保存成功
					state = "00";
					msg = "保存成功";
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//保存失败
					state = "01";
					msg = "保存失败";
				}
			}else {
				//必传字段不能为空
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			//必传字段不能为空
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

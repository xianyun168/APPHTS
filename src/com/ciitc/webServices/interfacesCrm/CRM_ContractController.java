package com.ciitc.webServices.interfacesCrm;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.FileUploadDao;
import com.ciitc.webServices.model.Contract;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.FileUp;
import com.ciitc.webServices.model.FileUploads;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.CRM_InvestorAuthenticationUntil;
import com.ciitc.webServices.util.CRM_ReservationUtil;
import com.ciitc.webServices.util.Log;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import jcifs.smb.SmbFile;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_ContractController extends BaseController{
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	
	private FileUploadDao fileUploadDao = (FileUploadDao) SpringContextUtil.getBean("fileUploadDao");
	
	/*
	 * 合同申请接口
	 */
	@RequestMapping(value = "saveContract",method=RequestMethod.GET)
	@ResponseBody
	public String saveContract(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Contract contract = new Contract();
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(json.has("c_fundcode")&&json.has("p_contractbalance")&&json.has("p_bankname")&&json.has("p_fundopendate")&&
				json.has("p_contractsigndate")&&json.has("p_bankacco")&&json.has("p_reserveno")&&json.has("p_nameinbank")&&
				json.has("c_trustcontractid")&&json.has("financialId")&&json.has("bankBrunnm")) {
			String p_subbranchname = json.getString("bankBrunnm");//开户支行
			String c_trustcontractid = json.getString("c_trustcontractid");
			String c_fundcode = json.getString("c_fundcode");//产品编号
			String p_contractbalance = json.getString("p_contractbalance");//合同金额
			String p_bankname = json.getString("p_bankname");//开户行号
			String p_fundopendate = json.getString("p_fundopendate");//合同起息日期
			String p_contractsigndate = json.getString("p_contractsigndate");//合同签署日期
			String p_bankacco = json.getString("p_bankacco");//开户账号
			String p_reserveno = json.getString("p_reserveno");//预约编号
			String p_nameinbank = json.getString("p_nameinbank");//开户姓名
			String financialId = json.getString("financialId");//理财师编号
			if((null != c_fundcode && !"".equals(c_fundcode))&&(null != p_contractbalance && !"".equals(p_contractbalance))&&
					(null != p_bankname && !"".equals(p_bankname))&&(null != p_fundopendate && !"".equals(p_fundopendate))&&
					(null != p_contractsigndate && !"".equals(p_contractsigndate))&&(null != p_bankacco && !"".equals(p_bankacco))&&
					(null != p_reserveno && !"".equals(p_reserveno))&&(null != p_nameinbank && !"".equals(p_nameinbank))&&
					(null != c_trustcontractid && !"".equals(c_trustcontractid))&&(null != financialId && !"".equals(financialId))){
				contract.setP_bankacco(p_bankacco);
				String p_bankno = reservationUtil.getBankNo(p_bankname);
				contract.setP_bankname(p_bankno);
				contract.setC_trustcontractid(c_trustcontractid);
				contract.setP_contractbalance(Integer.parseInt(p_contractbalance));
				try {
					contract.setP_contractsigndate(sdf1.parse(sdf1.format(sdf.parse(p_contractsigndate))));
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					contract.setP_fundopendate(sdf1.parse(sdf1.format(sdf.parse(p_fundopendate))));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				contract.setP_nameinbank(p_nameinbank);
				//根据产品编号查询项目编号
				String p_proserialno = reservationUtil.getProserialno(c_fundcode);
				contract.setP_proserialno(Integer.parseInt(p_proserialno));
				contract.setP_reserveno(p_reserveno);
				contract.setP_subbranchname(p_subbranchname);
				String code = reservationUtil.appcreatecontract(contract);
				String[] arr = code.split(",");
				if("0000".equals(arr[0])&&Integer.parseInt(arr[1])>0) {
					Log.getLogger().info("认证申请已提交成功");
					String l_serialno = arr[1];//文件加密的序列值
					Log.getLogger().info("认证申请已提交成功，返回编号l_serialno");
					String tempUrl = null;
					String url1 = null;
					List<EsbmmCode> listCode = esbmmCodeService.getCrmUploadFile();
					for (EsbmmCode esbmmCode : listCode) {
						if(esbmmCode.getCodeName().equals("tempPath")) {
							tempUrl = esbmmCode.getCodeValue();//临时上传目录
						}
						if(esbmmCode.getCodeName().equals("crmUploadPath")) {
							url1 =esbmmCode.getCodeValue();//CRM上传目录
						}
					}
					//String tempUrl = "D:/opt/crm";
					Log.getLogger().info("获取临时存取目录");
					List<File> formItems = new CRM_InvestorAuthenticationUntil().fileUpLoad(tempUrl,json);
					Log.getLogger().info("获取临时目录下的所有文件");
					//判断是否上传图片
					if(null != l_serialno && !"".equals(l_serialno) && formItems.size()>0) {
						Log.getLogger().info("App端已上传图片");
			 			//CRM文件存储对象
						FileUp fileUp = new FileUp();
						fileUp.setC_catalog("TCAPITALCURRENTS");
						fileUp.setV_creator(financialId);
						//根据理财师的id查询理财师的部门
						String nodecode = reservationUtil.getNodecode(financialId);
						fileUp.setV_nodecode(nodecode);
						fileUp.setV_serialno(Integer.parseInt(arr[0]));
						String date = sdf.format(new Date());
						String url = url1+date;//当天日期的目录名
						SmbFile file = null;
						try {
							file = new SmbFile(url);
							Log.getLogger().info("判断是否存在当天目录");
							if(!file.exists() && !file.isDirectory()){       
							    System.out.println("当天目录不存在");  
							    file.mkdir(); 
							}
							System.out.println("当天目录存在"); 
							SmbFile file1 = new SmbFile(url+"/Tblobstorage");
						    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
						    if(!file1.exists() && !file1.isDirectory()) {
						   	 System.out.println("Tblobstorage目录不存在"); 
						   	 file1.mkdir(); 
						    }
						} catch (Exception e) {
							Log.getLogger().info("判断共享文件地址是否正确连接");
							// TODO Auto-generated catch block
							state = "01";
							msg = "服务器连接失败！";
							e.printStackTrace();
							return state+"-"+msg;
						}
					  //遍历所有文件
						for (int i =0;i<formItems.size();i++) {
							Log.getLogger().info("循环遍历所有文件");
							//本地数据库文件对象
							FileUploads fileUploads = new FileUploads();
							fileUploads.setPkId(UUID.randomUUID().toString());//PkId
							//附件名称 
							String p_filename = formItems.get(i).getName();
							fileUploads.setOldFileName(p_filename);//附件原来的名字
							//文件大小
							long p_filesize = formItems.get(i).length();//附件大小
							fileUp.setP_filename(p_filename);
							fileUp.setP_filesize(p_filesize);
							Log.getLogger().info("开始调用CRM保存文件信息存储过程");
							String l_storageid = reservationUtil.saveFile(fileUp);//调用CRM保存文件信息存储过程
							Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
							String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
						    System.out.println("Tblobstorage目录存在");
						    String uploadUrl = url+"\\Tblobstorage\\"+fileNo+"\\";//4位取3位
						    //文件上传目录
						    SmbFile file2 = null;
							try {
								file2 = new SmbFile(uploadUrl);
								Log.getLogger().info("判断是否存在文件上传目录");
							     if(!file2.exists() && !file2.isDirectory()) {
							    	 System.out.println(fileNo+"目录不存在");
							    	 file2.mkdir();
							    }
							} catch (Exception e) {
								Log.getLogger().info("判断共享文件地址是否正确连接");
								// TODO Auto-generated catch block
								state = "01";
								msg = "服务器连接失败！";
								e.printStackTrace();
								return state+"-"+msg;
							}
						    Log.getLogger().info("开始调用文件上传加密方法");
					    	String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
					    	if(null != state2 && !"".equals(state2)) {
					    		Log.getLogger().info("判断文件上传加密是否成功");
					    		String fileUrl = state2.split(",")[0];
					    		String newFileName = state2.split(",")[1];
					    		fileUploads.setUploadUrl(fileUrl);
					    		fileUploads.setNewFileName(newFileName);
					    		//根据文件名查询文件PkId---p_filename
					    		String pkId = fileUploadDao.queryById(p_filename).getPkId();
					    		if(null != pkId && !"".equals(pkId)) {
					    			fileUploadDao.updateByPrimaryKey(fileUploads);
					    		}
					    	}else {
					    		Log.getLogger().info("判断共享文件地址是否正确连接");
								// TODO Auto-generated catch block
								state = "01";
								msg = "服务器连接失败！";
								return state+"-"+msg;
					    	}
						}
						state = "00";
						msg = "上传图片，合同申请已提交";
					}else {
						state = "00";
						msg = "未上传图片，合同申请已提交";
					}
				}else {
					state = "01";
					msg = arr[1];
				}
				
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

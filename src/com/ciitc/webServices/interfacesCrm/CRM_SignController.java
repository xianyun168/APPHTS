package com.ciitc.webServices.interfacesCrm;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppRelationDao;
import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.dao.SignDao;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.Sign;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_SignController extends BaseController{
	private SignDao signDao = (SignDao) SpringContextUtil.getBean("signDao");
	@SuppressWarnings("rawtypes")
	private CPTfundbrokerDao cpTfundbrokerDao = (CPTfundbrokerDao) SpringContextUtil.getBean("CPTfundbrokerDao");
	@SuppressWarnings("rawtypes")
	private AppRelationDao appRelationDao = (AppRelationDao) SpringContextUtil.getBean("appRelationDao");
	/*
	 * 签到接口
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "saveSign",method=RequestMethod.GET)
	@ResponseBody
	public String saveSign(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if(json.has("financialId")&& json.has("gpsX") && json.has("gpsY")&&json.has("plance")) {
			//理财师id 
			String financialId = json.getString("financialId");
			 //签到时间
			Date signDate = new Date();
			 //GPS_Y坐标
			 String gpsY = json.getString("gpsY");
			 //GPS_X坐标
			 String gpsX = json.getString("gpsX");
			 //地名
			 String plance = json.getString("plance");
			 if((null != financialId && !"".equals(financialId)) && (null != gpsY && !"".equals(gpsY)) &&
					 (null != gpsX && !"".equals(gpsX))&&(null != signDate)&&(null != plance && !"".equals(plance))) {
				 Sign sign = new Sign();
				
				 //根据理财师的id和当前时间判断是否已经签到
				 /*Map<String, Object> map1 = new HashMap<String, Object>();
				 map1.put("financialId", financialId);
				 map1.put("signDate", sdf.format(new Date()));
				 Sign sign1 = signDao.queryByNow(map1);
				 if(null != sign1) {
					 state = "01";
					 msg = "请勿重复签到";
				 }else {*/
					 //根据理财师id查询理财师的姓名和上级id
					 CPTfundbroker financialInfo  = cpTfundbrokerDao.selectByPrimaryKey(financialId);
					 if(null != financialInfo) {
						//理财师姓名
						String financialName = financialInfo.getcBrokername();
						//理财师的用户名
						String financialcode = financialInfo.getcUsercode();
						//根据financialcode查询理财师的直接上级
						String cFartherbroker = appRelationDao.selectByLoginid(financialcode);
						sign.setPkId(UUID.randomUUID().toString());
						sign.setFinancialName(financialName);
						sign.setGpsX(gpsX);
						sign.setGpsY(gpsY);
						sign.setPlance(plance);
						sign.setFinancialId(financialId);
						sign.setSignDate(signDate);
						sign.setLeaderId(cFartherbroker);
						signDao.insertSelective(sign);
						//签到成功
						state = "00";
						msg = "签到成功";
					 }else {
						//没有查到对应的理财师
						state = "01";
						msg = "没有查到对应的理财师";
					 }
				 /*}*/
			 }else {
				//必传字段不能为空
				state = "01";
				msg = "必传字段不能为空";
			 }
		}else {
			//必传字段不能为空
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

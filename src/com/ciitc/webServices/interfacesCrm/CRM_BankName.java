package com.ciitc.webServices.interfacesCrm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.util.BankName;

import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_BankName extends BaseController{
	/*
	 * 根据卡号查询所属银行接口
	 */
	@RequestMapping(value = "getBankName",method=RequestMethod.GET)
	@ResponseBody
	public String getBankName(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		BankName bankName = new BankName();
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("p_bankacco")) {
			String p_bankacco = json.getString("p_bankacco");
			if(null != p_bankacco && !"".equals(p_bankacco)) {
				String p_bankname = bankName.getNameOfBank(p_bankacco);
				if(null != p_bankname && !"".equals(p_bankname)) {
					state = "00";
					msg = "查询成功";
					map.put("p_bankname", p_bankname);
				}else {
					state = "01";
					msg = "您输入的银行卡号有误";
				}
				
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

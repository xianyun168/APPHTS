package com.ciitc.webServices.interfacesCrm;
/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年5月24日 下午4:03:58
 * 修 改 人: 
 ******************************************************************************/
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.model.InvestorAuthentication;
import com.ciitc.webServices.util.CRM_InvestorAuthenticationUntil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_InvestorAuthenticationController {
	
	//实名认证后----合格投资者认证
	@RequestMapping(value = "investorAuthentication",method=RequestMethod.GET)
	@ResponseBody
	public String investorAuthentication(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		InvestorAuthentication investorAuthentication = new InvestorAuthentication();
		CRM_InvestorAuthenticationUntil until = new CRM_InvestorAuthenticationUntil();
		if(json.has("p_custno") && json.has("p_provebalance") && json.has("financialId")
			&& json.has("p_provetype")) {
			//客户编号
			String p_custno = json.getString("p_custno");
			investorAuthentication.setP_custno(p_custno);
			//证明类型
			String p_provetype = json.getString("p_provetype");
			investorAuthentication.setP_provetype(p_provetype);
			//证明金额
			String p_provebalance = json.getString("p_provebalance");
			investorAuthentication.setP_provebalance(p_provebalance);
			//理财师id
			String financialId = json.getString("financialId");
			investorAuthentication.setFinancialId(financialId);
			Integer count = until.queryHgShCount(json.getString("p_custno"));
			if(count<=0) {
				String code = until.investorAuthentication(investorAuthentication,json);
				if(null != code && !"".equals(code)) {
					state = code.split("-")[0];
					msg = code.split("-")[1];
				}
			}else {
				state = "01";
				msg = "此客户存在待审核申请，无法进行再次提交申请";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

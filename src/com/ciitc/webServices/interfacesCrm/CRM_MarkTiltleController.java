package com.ciitc.webServices.interfacesCrm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CrmMarkTitleDao;
import com.ciitc.webServices.model.CrmMarkTitle;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_MarkTiltleController extends BaseController {
	
	@Autowired(required = false)
	private CrmMarkTitleDao crmMarkTitleDao =  (CrmMarkTitleDao) SpringContextUtil.getBean("crmMarkTitleDao");
	
	//客户标签新增接口
	@RequestMapping(value = "addCustomInfoMark",method=RequestMethod.GET)
	@ResponseBody
	public String addCustomInfoMark(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CrmMarkTitle crmMarkTitle = new CrmMarkTitle();
		if(json.has("customNo") && json.has("markTitle") && json.has("financialId")) {
			Map<String, Object> parmap = new HashMap<String, Object>();
			parmap.put("customNo", json.getString("customNo"));
			parmap.put("markTitle", json.getString("markTitle"));
			//查询此标签是否已存在
			CrmMarkTitle crmMarktitle = crmMarkTitleDao.queryByCustomNo(parmap);
			if(null != crmMarktitle) {
				state = "01";
				msg = "此标签已存在";
			}else {
				//查询已有标签个数
				Integer count = crmMarkTitleDao.queryMarks(json.getString("customNo"));
				if(count>3) {
					state = "01";
					msg = "一个客户最多只能添加3个标签";
				}else {
					crmMarkTitle.setPkId(UUID.randomUUID().toString());
					crmMarkTitle.setCustomNo(json.getString("customNo"));
					crmMarkTitle.setFinancialId(json.getString("financialId"));
					crmMarkTitle.setMarkTitle(json.getString("markTitle"));
					crmMarkTitleDao.insert(crmMarkTitle);
					map.put("crmMarkTitle", crmMarkTitle);
					state = "00";
					msg = "标签添加成功";
				}
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	//客户标签查询接口
	@RequestMapping(value = "queryCustomInfoMark",method=RequestMethod.GET)
	@ResponseBody
	public String queryCustomInfoMark(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("customNo")) {
			List<CrmMarkTitle> list = crmMarkTitleDao.queryMarkTitles(json.getString("customNo"));
			map.put("list", list);
			state = "00";
			msg = "标签查询成功";
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	//客户标签删除接口
	@RequestMapping(value = "updateCustomInfoMark",method=RequestMethod.GET)
	@ResponseBody
	public String updateCustomInfoMark(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("pkId")) {
			crmMarkTitleDao.deleteByPrimaryKey(json.getString("pkId"));
			state = "00";
			msg = "标签删除成功";
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	
}

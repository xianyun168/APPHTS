package com.ciitc.webServices.interfacesCrm;

import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import com.ciitc.webServices.dao.CustomerInfoDao;

@Controller
@RequestMapping("/crmCustomerController.do")
public class CRM_CustomerController {	
@SuppressWarnings("rawtypes")
@Autowired(required = false)
private CustomerInfoDao customerInfoDao = (CustomerInfoDao) SpringContextUtil.getBean("customerInfoDao");

	/*
	 * 客户资料修改
	 */
	@RequestMapping(value = "CustomerInfoModify", method = RequestMethod.GET)
	public @ResponseBody String CustomerInfoModify(HttpServletRequest request,
			JSONObject json) {
		//理财师ID
		String brokeId=json.getString("brokeId");
		//客户ID
		String customerId=json.getString("customerId");
		
		customerInfoDao.selectByPrimaryKey(customerId);
		String state = "";
		
		return state;
	}
}

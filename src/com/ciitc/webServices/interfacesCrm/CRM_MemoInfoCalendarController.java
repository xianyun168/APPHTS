package com.ciitc.webServices.interfacesCrm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.MemoInfoDao;
import com.ciitc.webServices.model.MemoInfo;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_MemoInfoCalendarController extends BaseController{
	private MemoInfoDao memoInfoDao = (MemoInfoDao) SpringContextUtil.getBean("memoInfoDao");
	/*
	 * 备忘信息查询接口
	 */
	@RequestMapping(value = "getMemoInfoCalendarList",method=RequestMethod.GET)
	@ResponseBody
	public String getMemoInfoCalendarList(HttpServletRequest request,JSONObject json) {
		String state = null;
		String msg = "";
		MemoInfo memoInfo = new MemoInfo();
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("financialId")) {
			String selectDate = null;
			if(json.has("selectDate")) {
				selectDate = json.getString("selectDate");
			}
			String financialId = json.getString("financialId"); 
			if(null != financialId && !"".equals(financialId)) {
				memoInfo.setFinancialId(financialId);
				int year;
				int month;
				int num;
				if(null != selectDate && !"".equals(selectDate)){
					if("/".equals(selectDate.substring(4, 5))) {
						year = Integer.parseInt(selectDate.substring(0, 4));
						month = Integer.parseInt(selectDate.substring(5));	
						
					}else {
						//日期格式不正确
						state = "01";
						msg = "日期格式不正确";
						map.put("state", state);
						map.put("msg", msg);
						String jsons = JSONArray.fromObject(map).toString();
						return jsons;
					}
				}else {
					Calendar calendar = Calendar.getInstance();
					year = calendar.get(Calendar.YEAR);
					month = calendar.get(Calendar.MONTH)+1;
				}
				if(month == 1 || month == 3 || month == 5 || 
						month == 7 || month == 8 || month == 10 || month == 12 ) {
					num = 31;
				}else if(month == 4 || month == 6 || month == 9 || month == 11) {
					num = 30;
				}else {
					 if(year%400==0||(year%4==0 && year%100!=0)){  
				            num = 29;  
				        }else{  
				            num = 28;  
				        }  
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String start = year +"-"+ month +"-1" +" 00:00:00";
				String end = year +"-"+ month +"-"+num +" 23:59:59";
				try {
					Date startDate=sdf.parse(start);
					Date endDate=sdf.parse(end);
					memoInfo.setStartDate(startDate);
					memoInfo.setEndDate(endDate);
					SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
					List<MemoInfo> memoInfoList  = memoInfoDao.selectMonth(memoInfo);
					if(memoInfoList.size() > 0) {
						for (MemoInfo memoInfo2 : memoInfoList) {
							memoInfo2.setRemark1(sdff.format(memoInfo2.getInfoMomeDate()));
						}
						map.put("memoInfoList", memoInfoList);
						//查询成功
						state = "00";
						msg = "查询成功";
					}else {
						//查询无结果
						state = "01";
						msg = "查询无结果";
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					//查询异常
					state = "01";
					msg = "查询异常";
				}
			}else {
				//理财师不能为空
				state = "01";
				msg = "理财师不能为空";
			}
			
		}else {
			//必传字段不能为空
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

package com.ciitc.webServices.interfacesCrm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppMessageSendDao;
import com.ciitc.webServices.dao.CPTbrokertreeDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.model.AppMessageSend;
import com.ciitc.webServices.model.CPTbrokertree;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.util.CRM_ReservationUtil;
import com.ciitc.webServices.util.CrmFunctionUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_Message extends BaseController{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");
	@SuppressWarnings("rawtypes")
	private AppMessageSendDao appMessageSendDao = (AppMessageSendDao) SpringContextUtil.getBean("appMessageSendDao");
	@SuppressWarnings("rawtypes")
	private CPTbrokertreeDao CPTbrokertreeDao = (CPTbrokertreeDao) SpringContextUtil.getBean("CPTbrokertreeDao");
	/*
	 * 消息列表接口
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "getMessage",method=RequestMethod.GET)
	@ResponseBody
	public String getMessage(HttpServletRequest request,JSONObject json) {
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		String state = null;
		String msg = "";
		CrmFunctionUtil crmFunctionUtil = new CrmFunctionUtil();
		//查询CRM表更新本地表
		crmFunctionUtil.getBrokertree();
		Map<String, Object> map = new HashMap<String, Object>();
		//参数，理财师的id financialId 理财师部门，消息类型
		if(json.has("financialId")) {
			//理财师编号
			String financialId = json.getString("financialId");
			//理财师部门
			//String deptCode = json.getString("deptCode");
			//消息类型
			String flag = null;
			if(json.has("flag")) {
				flag = json.getString("flag");
			}
			List<String> lists = new ArrayList<String>();
			List<AppMessageSend> appMessageSendList = new ArrayList<AppMessageSend>();
			Map<String, Object> maps = new HashMap<String, Object>();
			if(null != financialId && !"".equals(financialId)) {
				//根据理财师的id查询理财师的部门
				String deptCode1 =reservationUtil.getNodecode(financialId);
				
				while(null != deptCode1) {
					lists.add(deptCode1);
					String code = getParentcode(deptCode1);
					deptCode1 = code;
				}
				lists.add(financialId);
				//当前时间
				Date date = new Date();
				long time = date.getTime()-(2*24*60*60*1000);
				Date day = new Date(time);
				/*AppMessageSend appMessageSend = new AppMessageSend();
				appMessageSend.setFlag(flag);
				appMessageSend.setSendDate(day);*/
				maps.put("flag", flag);
				maps.put("day", day);
				maps.put("lists", lists);
				appMessageSendList = appMessageSendDao.queryByAll(maps);
				//查询数据字典
				String pkid = esbmmCodeTypeDao.queryByTypeName("host").get(0).getPkId();
	            String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
				if(appMessageSendList.size()>0) {
					for (AppMessageSend appMessageSend : appMessageSendList) {
						if(null != appMessageSend.getPictureURL() && !"".equals(appMessageSend.getPictureURL())) {
							appMessageSend.setPictureURL(fwqdkh + appMessageSend.getPictureURL());
						}
					}
					map.put("appMessageSendList", appMessageSendList);
					state = "00";
					msg = "查询成功";
				}else {
					state = "01";
					msg = "查询无结果";
				}
				
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
			
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	//根据部门编号查询上级部门
	public String getParentcode(String deptCode) {
		String code = null;
		//根据部门编号查询上级部门编号
		CPTbrokertree tbrokertree = CPTbrokertreeDao.queryParentById(deptCode);
		if(null != tbrokertree && !"0".equals(tbrokertree.getcParentcode())) {
			code = tbrokertree.getcParentcode();
		}
		return code;
	}

}

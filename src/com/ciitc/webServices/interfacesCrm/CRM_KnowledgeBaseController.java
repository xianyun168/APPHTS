package com.ciitc.webServices.interfacesCrm;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CollectionDao;
import com.ciitc.webServices.dao.KnowledgeBaseDao;
import com.ciitc.webServices.model.Collection;
import com.ciitc.webServices.model.KnowledgeBase;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_KnowledgeBaseController extends BaseController{
@SuppressWarnings("rawtypes")
private KnowledgeBaseDao knowledgeBaseDao = (KnowledgeBaseDao) SpringContextUtil.getBean("knowledgeBaseDao");
@SuppressWarnings("rawtypes")
private CollectionDao collectionDao = (CollectionDao) SpringContextUtil.getBean("collectionDao");	
	/*
	 * 知识库查询接口
	 */
	@RequestMapping(value = "getKnowledgeBase",method=RequestMethod.GET)
	@ResponseBody
	public String getKnowledgeBase(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String state = "";
		String msg = "";
		String keyWord ="";
		String pkId ="";
		String financialId = "";
		//判断json字符串里是否包含title和pkId
		if(json.has("keyWord")) {
			keyWord = json.getString("keyWord");
		}
		if(json.has("pkId")) {
			pkId = json.getString("pkId");
		}
		if(json.has("financialId")) {
			financialId = json.getString("financialId");
		}
		//判断pkId是否为空，如果不为空则查询知识库详情，如果为空则查询知识库列表
		if(null != pkId && !"".equals(pkId)) {
			//根据pkId查询知识库详细信息
			KnowledgeBase knowledgeBase = knowledgeBaseDao.selectByPrimaryKey(pkId);
			
			if(null != knowledgeBase) {
				//查看这条知识库有没有被收藏过
				Map<String, Object> scap = new HashMap<String, Object>();
				scap.put("pkId", pkId);
				scap.put("financialId", financialId);
				Collection collection = collectionDao.selectByTo(scap);
				if(null != collection) {
					//已收藏
					knowledgeBase.setRemark1("01");
				}else {
					//未收藏
					knowledgeBase.setRemark1("00");
				}
				
				if(null != knowledgeBase.getReleaseDate()) {
					knowledgeBase.setRemark2(sdf.format(knowledgeBase.getReleaseDate()));
				}
				if(null != knowledgeBase.getUpdatedDate() ) {
					knowledgeBase.setRemark3(sdf.format(knowledgeBase.getUpdatedDate()));
				}
				map.put("knowledgeBase", knowledgeBase);
				state = "00";
				msg = "查询成功";
			}else {
				state = "01";
				msg = "无查询结果";
			}
			
		}else {
			try {
				//查询知识库列表(如果keyWord不为空则按照keyWord模糊查询)
				List<KnowledgeBase> knowledgeBaseList =  knowledgeBaseDao.queryByListAll(keyWord);
				if(knowledgeBaseList.size() > 0) {
					for (KnowledgeBase knowledgeBase : knowledgeBaseList) {
						if(null != knowledgeBase.getReleaseDate()) {
							knowledgeBase.setRemark2(sdf.format(knowledgeBase.getReleaseDate()));
						}
						if(null != knowledgeBase.getUpdatedDate() ) {
							knowledgeBase.setRemark3(sdf.format(knowledgeBase.getUpdatedDate()));
						}
					}
					state = "00";
					msg = "查询成功";
					map.put("knowledgeBaseList", knowledgeBaseList);
				}else {
					state = "01";
					msg = "无查询结果";
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01";
				msg = "查询异常";
			}
		}
		map.put("msg", msg);
		map.put("state", state);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

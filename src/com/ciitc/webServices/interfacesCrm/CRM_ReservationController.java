package com.ciitc.webServices.interfacesCrm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CPReservationDao;
import com.ciitc.webServices.dao.ProductInfoDao;
import com.ciitc.webServices.model.CPReservation;
import com.ciitc.webServices.model.ProductInfo;
import com.ciitc.webServices.model.Reservation;
import com.ciitc.webServices.model.ReservationDtl;
import com.ciitc.webServices.util.CRM_ReservationUtil;
import com.ciitc.webServices.util.KfcrmCustor;
import com.ciitc.webServices.util.ProductLimtUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_ReservationController extends BaseController {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Autowired(required = false)
	private ProductInfoDao<ProductInfo> productInfoDao = (ProductInfoDao) SpringContextUtil.getBean("productInfoDao");
	private CPReservationDao cPReservationDao = (CPReservationDao) SpringContextUtil.getBean("CPReservationDao");
	/*
	 * 认购预约接口
	 */
	@RequestMapping(value = "saveReservation",method=RequestMethod.GET)
	@ResponseBody
	public String saveReservation(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		KfcrmCustor expiration = new KfcrmCustor();
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("p_custno") &&json.has("p_proserialno")  &&
				json.has("p_remitdate") &&json.has("p_expirationdate") &&json.has("p_brokeraccount")) {
			//客户编号
			String p_custno =json.getString("p_custno");
			//项目编号
			String p_proserialno = json.getString("p_proserialno");
			//产品编号
			String p_fundcode = null;
			if(null != p_proserialno && !"".equals(p_proserialno)) {
				p_fundcode = reservationUtil.getFundcode(p_proserialno);
			}
			//预计缴款日期
			String p_remitdate = json.getString("p_remitdate").replaceAll("-", "");
			//预约结束日期
			String p_expirationdate = json.getString("p_expirationdate").replaceAll("-", "");
			//理财师的Id
			String p_brokeraccount = json.getString("p_brokeraccount");
			if((null != p_custno &&!"".equals(p_custno))&&(null != p_proserialno &&!"".equals(p_proserialno))&&
					(null != p_fundcode &&!"".equals(p_fundcode))&&(null != p_remitdate &&!"".equals(p_remitdate))&&
					(null != p_expirationdate &&!"".equals(p_expirationdate))&&
					(null != p_brokeraccount &&!"".equals(p_brokeraccount))) {
				CPReservation reservation = new CPReservation();
				//理财师编号
				reservation.setpBrokeraccount(p_brokeraccount);
				//产品编号
				reservation.setpFundcode(p_fundcode);
				//项目编号
				reservation.setpProserialno(Integer.parseInt(p_proserialno));
				//预计缴款日期
				reservation.setpRemitdate(p_remitdate);
				//预约结束日期
				reservation.setpExpirationdate(p_expirationdate);
				String[] brr = p_custno.split(";");
				String code =null;
				for(int i=0;i<brr.length;i++) {
					String[] crr = brr[i].split(",");
					String custnoName = crr[0];
					String custno = crr[1];
					String balance = crr[2];
					//客户名称
					reservation.setRemark1(custnoName);
					//客户编号
					reservation.setpCustno(custno);
					//预约金额
					reservation.setpBalance(Double.parseDouble(balance));
					//调用认购预约接口
					code = reservationUtil.reservation(reservation);
				}
				
				String[] arr = code.split(",");
				String states = arr[0];
				if("1".equals(states)) {
					//调用产品额度提醒
					expiration.productEndDate(p_fundcode);
					state = "00";
					msg = arr[1];//成功返回预约编号
				}else {
					state = "01";
					msg = arr[1];
				}
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
			
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	/*
	 * 暂时保存后台的认购预约接口
	 */
	@RequestMapping(value = "temporaryReservation",method=RequestMethod.GET)
	@ResponseBody
	public String temporaryReservation(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		if(json.has("deleteAll")&& "1".equals(json.getString("deleteAll"))&&json.has("p_proserialno")&&json.has("p_brokeraccount")) {
			//项目编号
			String p_proserialno = json.getString("p_proserialno");
			//产品编号
			String p_fundcode = null;
			if(null != p_proserialno && !"".equals(p_proserialno)) {
				p_fundcode = reservationUtil.getFundcode(p_proserialno);
			}
			CPReservation reservation = new CPReservation();
			//理财师编号
			reservation.setpBrokeraccount(json.getString("p_brokeraccount"));
			//产品编号
			reservation.setpFundcode(p_fundcode);
			//删除这个理财师和这个产品下的所有预约
			cPReservationDao.deleteByAll(reservation);
			state = "00";
			msg = "保存成功";
		}else {
			if(json.has("p_custno") &&json.has("p_proserialno")  && json.has("p_remitdate")
					 &&json.has("p_expirationdate") &&json.has("p_brokeraccount")) {
				//客户编号
				String p_custno =json.getString("p_custno");
				//项目编号
				String p_proserialno = json.getString("p_proserialno");
				//产品编号
				String p_fundcode = null;
				if(null != p_proserialno && !"".equals(p_proserialno)) {
					p_fundcode = reservationUtil.getFundcode(p_proserialno);
				}
				//预计缴款日期
				String p_remitdate = json.getString("p_remitdate");
				//预约结束日期
				String p_expirationdate = json.getString("p_expirationdate");
				//理财师编号
				String p_brokeraccount = json.getString("p_brokeraccount");
				if((null != p_custno &&!"".equals(p_custno))&&(null != p_proserialno &&!"".equals(p_proserialno))&&
						(null != p_remitdate &&!"".equals(p_remitdate))&&(null != p_expirationdate &&!"".equals(p_expirationdate))&&
						(null != p_brokeraccount &&!"".equals(p_brokeraccount))) {
					
					CPReservation reservation = new CPReservation();
					
					//理财师编号
					reservation.setpBrokeraccount(p_brokeraccount);
					//产品编号
					reservation.setpFundcode(p_fundcode);
					//删除这个理财师和这个产品下的所有预约
					cPReservationDao.deleteByAll(reservation);
					//项目编号
					reservation.setpProserialno(Integer.parseInt(p_proserialno));
					//预计缴款日期
					reservation.setpRemitdate(p_remitdate);
					//预约结束日期
					reservation.setpExpirationdate(p_expirationdate);
					String[] brr = p_custno.split(";");
					for(int i=0;i<brr.length;i++) {
						reservation.setPkId(UUID.randomUUID().toString());
						String[] crr = brr[i].split(",");
						String custName = crr[0];
						String custno = crr[1];
						String balance = crr[2];
						//客户名称
						reservation.setRemark1(custName);
						//客户编号
						reservation.setpCustno(custno);
						//预约金额
						reservation.setpBalance(Double.parseDouble(balance));
						//保存预约
						cPReservationDao.insertSelective(reservation);
					}
					state = "00";
					msg = "保存成功";
					
				}else {
					state = "01";
					msg = "必传字段不能为空";
				}
				
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	/*
	 * 查询暂时保存到后台的数据
	 * 根据理财师的id和项目编号查询
	 */
	@RequestMapping(value = "getReservation",method=RequestMethod.GET)
	@ResponseBody
	public String getReservation(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("p_brokeraccount") && json.has("p_proserialno")) {
			String p_brokeraccount = json.getString("p_brokeraccount");
			String p_proserialno = json.getString("p_proserialno");
			if((null != p_proserialno && !"".equals(p_proserialno))&&(null != p_brokeraccount && !"".equals(p_brokeraccount))) {
				CPReservation reservation = new CPReservation();
				reservation.setpBrokeraccount(p_brokeraccount);
				reservation.setpProserialno(Integer.parseInt(p_proserialno));
				List<CPReservation> reservationList = cPReservationDao.selectByBF(reservation);
				if(reservationList.size()>0) {
					map.put("reservationList", reservationList);
					state = "00";
					msg = "查询成功";
				}else {
					state = "01";
					msg = "查询无结果";
				}
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
			
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	/*
	 * 撤销认购预约接口
	 */
	@RequestMapping(value = "cellReservation",method=RequestMethod.GET)
	@ResponseBody
	public String cellReservation(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("reserveno")) {
			String reserveno = json.getString("reserveno");
			if(null != reserveno && !"".equals(reserveno)) {
				CRM_ReservationUtil crmReservationUtil = new CRM_ReservationUtil();
				String code = crmReservationUtil.cellReservation(reserveno);
				String[] arr = code.split(",");
				if("0".equals(arr[0])) {
					state = "01";
					msg = arr[1];
				}else if("1".equals(arr[0])) {
					state = "00";
					msg = arr[1];
				}
			}else {
				//必传字段不能为空
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			//必传字段不能为空
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	/*
	 * 认购预约列表接口
	 */
	@RequestMapping(value = "getReservationList",method=RequestMethod.GET)
	@ResponseBody
	public String getReservationList(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		ProductLimtUtil prduct = new ProductLimtUtil();
		List<Reservation> reservationList = reservationUtil.getReservationList();
		if(reservationList.size()>0) {
			for (Reservation reservation : reservationList) {
				//根据产品的编号查询项目编号
				String l_proserialno = reservationUtil.getProserialno(reservation.getC_fundcode());
				//根据项目编号查询产品的剩余额度
				int remainingSum = prduct.productEndDate(l_proserialno);
				reservation.setRemainingSum(remainingSum);
				//根据项目编号查询产品的状态
				String status = productInfoDao.queryByState(l_proserialno);
				reservation.setStatus(status);
			}
			map.put("reservationList", reservationList);
			state = "00";
			msg = "查询成功";
		}else {
			state = "01";
			msg = "查询结果为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	/*
	 * 认购预约详情列表接口
	 */
	@RequestMapping(value = "getReservationDtlList",method=RequestMethod.GET)
	@ResponseBody
	public String getReservationDtlList(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		ProductLimtUtil prduct = new ProductLimtUtil();
		if(json.has("financialId") && json.has("funCode")) {
			String financialId = json.getString("financialId");
			String funCode = json.getString("funCode");
			if((null != financialId && !"".equals(financialId))&&(null != funCode && !"".equals(funCode))) {
				List<ReservationDtl> reservationDtlList = reservationUtil.getReservationDtlList(financialId,funCode);
				if(reservationDtlList.size()>0) {
					for (ReservationDtl reservationDtl : reservationDtlList) {
						String status = prduct.getRelation(reservationDtl.getC_reserveno());
						reservationDtl.setC_status(status);
					}
					map.put("reservationDtlList", reservationDtlList);
					state = "00";
					msg = "查询成功";
				}else {
					state = "01";
					msg = "查询结果为空";
				}
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

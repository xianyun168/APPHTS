package com.ciitc.webServices.interfacesCrm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CollectionDao;
import com.ciitc.webServices.model.Collection;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_CollectionController extends BaseController{
	@SuppressWarnings("rawtypes")
	private CollectionDao collectionDao = (CollectionDao) SpringContextUtil.getBean("collectionDao");
	/*
	 * 查询收藏接口
	 */
	@RequestMapping(value = "getCollection",method=RequestMethod.GET)
	@ResponseBody
	public String getCollection(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		// 
		if(json.has("financialId") && json.has("plate")) {
			Collection collection = new Collection();
			String keyWord = null;
			if(json.has("keyWord")) {
				keyWord = json.getString("keyWord");
			}
			String plate = json.getString("plate");
			String financialId = json.getString("financialId");
			if(json.has("keyWord")&& null != json.getString("keyWord") && !"".equals(json.getString("keyWord")) ) {
				keyWord = json.getString("keyWord");
			}
			if(json.has("plate")) {
				plate = json.getString("plate");
			}else {
				plate = "00";
			}
			if((null != financialId && !"".equals(financialId))) {
				if(null != keyWord && !"".equals(keyWord)) {
					collection.setKeyWord(keyWord);
				}
				collection.setPlate(plate);
				collection.setFinancialId(financialId);
				List<Collection> collectionList = collectionDao.selectList(collection);
				if(collectionList.size() > 0) {
					map.put("collectionList", collectionList);
					state = "00";
					msg = "查询成功";
				}else {
					state = "01";
					msg = "查询无结果";
				}
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString(); 
		return jsons;
	}
}

package com.ciitc.webServices.interfacesCrm;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import com.ciitc.webServices.dao.CustomerInfoDao;
import com.ciitc.webServices.model.CustomerInfo;
import com.ciitc.webServices.util.Log;

@Controller
@RequestMapping("/crmCustomerLabelController.do")
public class CRM_CustomerLabelController extends BaseController{
	
@SuppressWarnings({ "rawtypes", "unchecked" })
@Autowired(required = false)
private CustomerInfoDao<CustomerInfo> customerInfoDao = (CustomerInfoDao) SpringContextUtil.getBean("customerInfoDao");
	
	/*
	 * 客户标签新增接口
	 */
	@RequestMapping(value = "customerLabelAdd",method=RequestMethod.GET)
	@ResponseBody
	public String getCustomerLabelAdd(HttpServletRequest request,JSONObject json) {
		//生成一个uuid字符串
		//String uuid = UUID.randomUUID().toString();
		Map<String, Object> map = new HashMap<String, Object>();
		//SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		//转json用
		//JSONObject json1 = JSONObject.fromObject(json);
		//String labelFlag ="";
		String state = "";
		String message ="";
		String pkId = "";
		String financialAdvisorId = "";
		String customerLabel = "";
		/*String pkId = request.getParameter("pkId");
		String financialAdvisorId = request.getParameter("financialAdvisorId");
		String customerLabel = request.getParameter("customerLabel");*/
		if(json.has("pkId")&&json.has("financialAdvisorId")&&json.has("customerLabel")) {
			pkId = json.getString("pkId");
			financialAdvisorId = json.getString("financialAdvisorId");
			customerLabel=json.getString("customerLabel");
			try {
				CustomerInfo customerInfo = customerInfoDao.selectByPrimaryKey(pkId); 
				if(!"".equals(customerInfo)){
			    customerInfo.setCustomerLabel(customerLabel);
			    customerInfoDao.update(customerInfo);
			    }
				else {
					state = "01";
					message="客户信息不存在";
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01";
				message ="标签信息添加失败";
			}
		}else if("".equals(json.get(pkId))){
			state="01";
			message ="错误,传入客户ID为空";
			Log.getLogger().info("错误,传入客户ID为空");	
		}else if("".equals(json.get(financialAdvisorId))){
			state="01";
			message ="错误,传入理财师ID为空";
			Log.getLogger().info("错误,传入理财师ID为空");
		}else{
			state="01";
			message ="错误,客户标签内容为空";
			Log.getLogger().info("错误,客户标签内容为空");
		}
		
		map.put("state", state);
		map.put("message", message);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

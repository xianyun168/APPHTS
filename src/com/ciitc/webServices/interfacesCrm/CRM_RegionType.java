package com.ciitc.webServices.interfacesCrm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_RegionType extends BaseController{
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private EsbmmCodeTypeDao esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private EsbmmCodeDao esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
	
	@RequestMapping(value = "addCustomInfo",method=RequestMethod.GET)
	@ResponseBody
	public String addCustomInfo(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		List<EsbmmCode> IdcardTypecodeList = new ArrayList<EsbmmCode>();
		List<EsbmmCode> RegioncodeList = new ArrayList<EsbmmCode>();
		try {
			//根据数据字典查询证件类型****//
			List<EsbmmCodeType> IdcardTypeList = esbmmCodeTypeDao.queryByTypeName("IdcardType");
			EsbmmCodeType IdcardTypePkId = IdcardTypeList.get(0);
			EsbmmCode codePkId2=new EsbmmCode();
			codePkId2.setCodeTypePkId(IdcardTypePkId.getPkId());
			//根据codeTypePkId查找对应的类型
			IdcardTypecodeList = esbmmCodeDao.queryByIdcard(codePkId2);
			//-----------------------------
			//根据数据字典查询服务分支机构****//
			List<EsbmmCodeType> RegionList = esbmmCodeTypeDao.queryByTypeName("AddressAll");
			EsbmmCodeType RegionListPkId = RegionList.get(0);
			EsbmmCode codePkId3=new EsbmmCode();
			codePkId3.setCodeTypePkId(RegionListPkId.getPkId());
			//根据codeTypePkId查找对应的类型
			RegioncodeList = esbmmCodeDao.queryByIdcard(codePkId3);
			if(IdcardTypecodeList.size()>0 && RegioncodeList.size()>0) {
				state = "00";
				msg = "查询成功";
			}else {
				state = "01";
				msg = "查询失败";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		map.put("state",state );
		map.put("msg",msg );
		map.put("IdcardType", IdcardTypecodeList);
		map.put("regionmap", RegioncodeList);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

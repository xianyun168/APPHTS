package com.ciitc.webServices.interfacesCrm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.model.ProfessionalInfo;
import com.ciitc.webServices.util.CRM_ProfessionalUntil;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人:
 * 日 期: 2018年5月24日 下午4:03:58
 * 修 改 人: 
 ******************************************************************************/
public class CRM_ProfessionalApproveController {
	
	//实名认证后----专业投资者认证申请
	@RequestMapping(value = "approve",method=RequestMethod.GET)
	@ResponseBody
	public String approve(HttpServletRequest request, JSONObject json){
		String state = "" ;
		String msg ="";
		ProfessionalInfo pro = new ProfessionalInfo();
		CRM_ProfessionalUntil professionalUntil = new CRM_ProfessionalUntil();
		Map<String,Object> map = new HashMap<String, Object>();
		//判断字段是否为空
		if(json.has("p_custno") && json.has("p_provetype") && json.has("p_provebalance") && json.has("financialId")){
			pro.setP_custno(json.getString("p_custno"));
			pro.setP_provetype(json.getString("p_provetype"));
			pro.setP_provebalance(json.getString("p_provebalance"));
			pro.setFinancialId(json.getString("financialId"));
			Integer count = professionalUntil.queryZyShCount(json.getString("p_custno"));
			if(count<=0) {
				String code = professionalUntil.professionalSave(pro, json);
				if(null != code && !"".equals(code)) {
					state = code.split("-")[0];
					msg = code.split("-")[1];
				}
			}else {
				state = "01";
				msg = "此客户存在待审核申请，无法进行再次提交申请";
			}
			
		}else{
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String j = JSONArray.fromObject(map).toString();
		return j;
	}
}

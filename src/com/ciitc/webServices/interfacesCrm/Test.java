package com.ciitc.webServices.interfacesCrm;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.CRM_InvestorAuthenticationUntil;
import com.ciitc.webServices.util.Log;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import jcifs.smb.SmbFile;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class Test  extends BaseController{
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	@RequestMapping(value = "application1",method=RequestMethod.GET)
	@ResponseBody
	public String application1(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		
			String tempUrl = null;
			String url1 = null;
			List<EsbmmCode> listCode = esbmmCodeService.getCrmUploadFile();
			for (EsbmmCode esbmmCode : listCode) {
				if(esbmmCode.getCodeName().equals("tempPath")) {
					tempUrl = esbmmCode.getCodeValue();//临时上传目录
				}
				if(esbmmCode.getCodeName().equals("crmUploadPath")) {
					url1 =esbmmCode.getCodeValue();//CRM上传目录
				}
			}
			//String tempUrl = "D:/opt/crm";
			Log.getLogger().info("获取临时存取目录");
			List<File> formItems = new CRM_InvestorAuthenticationUntil().fileUpLoad(tempUrl,json);
			Log.getLogger().info("获取临时目录下的所有文件");
			//判断是否上传图片
			if(formItems.size()>0) {
				Log.getLogger().info("App端已上传图片");
	 			//CRM文件存储对象
				//根据理财师的id查询理财师的部门
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				String date = sdf.format(new Date());
				//String crmFilePath = "D:\\fundcrm\\accessory\\"+date+"\\";
				//String url = "smb://macaijie:199312@192.168.137.197/cxn/"+date;//当天日期的目录名
				String url = url1+date;//当天日期的目录名
				SmbFile file = null;
				try {
					file = new SmbFile(url);
					Log.getLogger().info("判断是否存在当天目录");
					if(!file.exists() && !file.isDirectory()){       
					    System.out.println("当天目录不存在");  
					    file.mkdir(); 
					}
					System.out.println("当天目录存在"); 
					SmbFile file1 = new SmbFile(url+"/Tblobstorage");
				    Log.getLogger().info("判断是否存在"+"/Tblobstorage目录");
				    if(!file1.exists() && !file1.isDirectory()) {
				   	 System.out.println("Tblobstorage目录不存在"); 
				   	 file1.mkdir(); 
				    }
				    for (int i =0;i<formItems.size();i++) {
						Log.getLogger().info("循环遍历所有文件");
						Log.getLogger().info("开始调用CRM保存文件信息存储过程");
						String l_storageid = "8356";//调用CRM保存文件信息存储过程
						Log.getLogger().info("调用CRM保存文件信息存储过程结束，返回序列号");
						String fileNo = l_storageid.substring(0, l_storageid.length()-1);//目录名字
					    System.out.println("Tblobstorage目录存在");
					    String uploadUrl = url+"\\Tblobstorage\\"+fileNo+"\\";//4位取3位
					    //文件上传目录
					    SmbFile file2 = null;
						try {
							file2 = new SmbFile(uploadUrl);
							Log.getLogger().info("判断是否存在文件上传目录");
						     if(!file2.exists() && !file2.isDirectory()) {
						    	 System.out.println(fileNo+"目录不存在");
						    	 file2.mkdir();
						    }
						} catch (Exception e) {
							Log.getLogger().info("判断共享文件地址是否正确连接");
							// TODO Auto-generated catch block
							state = "01";
							msg = "服务器连接失败！";
							e.printStackTrace();
							return state+"-"+msg;
						}
					    Log.getLogger().info("开始调用文件上传加密方法");
				    	String state2 = new CRM_InvestorAuthenticationUntil().fileJm(uploadUrl, formItems.get(i), l_storageid,tempUrl);
				    	if(null != state2 && !"".equals(state2)) {
				    		Log.getLogger().info("判断文件上传加密是否成功");
				    	}else {
				    		Log.getLogger().info("判断共享文件地址是否正确连接");
							// TODO Auto-generated catch block
							state = "01";
							msg = "服务器连接失败！";
							return state+"-"+msg;
				    	}
					}
				} catch (Exception e) {
					Log.getLogger().info("判断共享文件地址是否正确连接");
					// TODO Auto-generated catch block
					state = "01";
					msg = "服务器连接失败！";
					e.printStackTrace();
					return state+"-"+msg;
				}
			  //遍历所有文件
				state = "00";
				msg = "上传图片，缴款申请已提交";
			}else {
				state = "00";
				msg = "未上传图片，缴款申请已提交";
			}
		
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}



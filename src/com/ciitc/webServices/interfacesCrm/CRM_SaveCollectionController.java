package com.ciitc.webServices.interfacesCrm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CollectionDao;
import com.ciitc.webServices.model.Collection;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_SaveCollectionController extends BaseController{
	private CollectionDao collectionDao = (CollectionDao) SpringContextUtil.getBean("collectionDao");
	/*
	 * 收藏、取消收藏接口
	 */
	@RequestMapping(value = "saveCollection",method=RequestMethod.GET)
	@ResponseBody
	public String saveCollection(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		if(json.has("collectionId") && json.has("title") && json.has("keyWord")&&
				json.has("plate") && json.has("financialId") &&json.has("flag")) {
			String collectionId = json.getString("collectionId");
			String title = json.getString("title");
			String plate = json.getString("plate");
			String financialId = json.getString("financialId");
			String flag = json.getString("flag");
			String keyWord = json.getString("keyWord");
			Collection collection = new Collection();
			if((null != collectionId && !"".equals(collectionId)) && (null != title && !"".equals(title)) &&
					(null != plate && !"".equals(plate)) && (null != flag && !"".equals(flag)) &&
					(null != financialId && !"".equals(financialId))&&(null != keyWord && !"".equals(keyWord))) {
				collection.setCollectionId(collectionId);
				collection.setFinancialId(financialId);
				if("00".equals(flag)) {
					Collection collectionOne = collectionDao.selectOne(collection);
					if(null != collectionOne) {
						//已收藏
						state = "01";
						msg = "已收藏,请勿重复收藏";
					}else {
						String uuid = UUID.randomUUID().toString();
						collection.setPkId(uuid);
						collection.setTitle(title);
						collection.setPlate(plate);
						collection.setFlag(flag);
						collection.setKeyWord(keyWord);
						collection.setCollectionTime(new Date());
						collectionDao.insertSelective(collection);
						//收藏成功
						state = "00";
						msg = "收藏成功";
						
					}
					
				}else {
					collectionDao.deleteBy(collection);
					//取消收藏
					state = "00";
					msg = "取消收藏";
				}
			}else {
				//必传字段不能为空
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			//必传字段不能为空
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

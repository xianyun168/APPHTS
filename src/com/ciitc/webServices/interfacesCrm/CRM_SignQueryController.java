package com.ciitc.webServices.interfacesCrm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppRelationDao;
import com.ciitc.webServices.dao.SignDao;
import com.ciitc.webServices.model.AppRelation;
import com.ciitc.webServices.model.Sign;
import com.ciitc.webServices.util.CrmFunctionUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_SignQueryController extends BaseController{
	private SignDao signDao = (SignDao) SpringContextUtil.getBean("signDao");
	@SuppressWarnings("rawtypes")
	private AppRelationDao appRelationDao = (AppRelationDao) SpringContextUtil.getBean("appRelationDao");
	/*
	 * 签到查询接口
	 */
	@RequestMapping(value = "getSign",method=RequestMethod.GET)
	@ResponseBody
	public String getSign(HttpServletRequest request,JSONObject json) {
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		Sign sign = new Sign();
		if(json.has("financialId") && json.has("type") && json.has("financialCode")) {
			//查询时间
			Date signDate =null;
			if(json.has("signDate")) {
				String date = json.getString("signDate");
				if(null != date) {
					try {
						signDate = sdf.parse(date);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else {
					signDate = new Date();
				}
			}else {
				signDate = new Date();
			}
			//理财师的id
			String financialId = json.getString("financialId");
			//签到信息查询类型
			String type = json.getString("type");
			sign.setFinancialId(financialId);
			sign.setRemark1(sdf.format(signDate));
			Map<String, Object> signMap = new HashMap<String, Object>();
			//查询自己的签到信息
			if("00".equals(type)) {
				signMap.put("sign", sign);
				List<Sign> signList = signDao.queryByFinancialId(signMap);
				if(signList.size()>0) {
					for (Sign sign2 : signList) {
						sign2.setRemark1(sdf1.format(sign2.getSignDate()));
					}
					map.put("signList", signList);
					//查询个人签到信息成功
					state = "00";
					msg = "查询个人签到信息成功";
				}else {
					//查询无结果
					state = "01";
					msg = "查询无结果";
				}
				
				//查询直系下属的签到信息
			}else {
				//Map<String, Object> maps = new HashMap<String, Object>();
				List<Sign> signLists = new ArrayList<Sign>();
				CrmFunctionUtil crmFunctionUtil = new CrmFunctionUtil();
				//根据理财师编号查询理财师用户名
				String userCode = crmFunctionUtil.getUserCode(financialId);
				//根据理财师的用户名查询直属下级理财师
				List<AppRelation> viewList = appRelationDao.selectByUpLoginid(userCode);
				//根据这个上级理财师id查询下级理财师的签到信息
				sign.setLeaderId(userCode);
				signMap.put("sign", sign);
				signLists = signDao.queryByLeaderId(signMap);
				if(viewList.size()>0) {
					if(signLists.size()>0) {
						for (Sign signa : signLists) {
							signa.setRemark1(sdf1.format(signa.getSignDate()));
						}
						map.put("signList", signLists);
						//查询下属签到信息成功
						state = "00";
						msg = "查询下属签到信息成功";
					}else {
						//查询无结果
						state = "01";
						msg = "查询无结果";
					}
				}else {
					state = "01";
					msg = "对不起，您没有查看权限";
				}
			}
		}else {
			//必传字段不能为空
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
		
	}

}

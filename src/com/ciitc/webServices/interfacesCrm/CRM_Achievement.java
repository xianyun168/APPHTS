package com.ciitc.webServices.interfacesCrm;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ciitc.webServices.util.CRM_AchievementUntil;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/**
 * 业绩查询接口
 * @author Administrator
 *
 */
public class CRM_Achievement{
	@RequestMapping(value = "propernace",method=RequestMethod.GET)
	@ResponseBody
	public String getAchievement(HttpServletRequest request,JSONObject json){
		Map<String, Object> map = new HashMap<String,Object>();
		CRM_AchievementUntil achuntoil1 = new CRM_AchievementUntil(); 
		String state = "";
		String msg = "";
		if(json.has("UserID")){
			//理财师ID
			String userid = json.getString("UserID");
			map =achuntoil1.reservat(userid);
		}else{
			state = "01";
			msg = "必选参数为空";
			map.put("state",state);
			map.put("msg",msg);
		
		}
		//返回信息
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}

}

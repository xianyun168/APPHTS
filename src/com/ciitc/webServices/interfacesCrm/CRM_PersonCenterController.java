package com.ciitc.webServices.interfacesCrm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CollectionDao;
import com.ciitc.webServices.dao.MemoInfoDao;
import com.ciitc.webServices.dao.SignDao;
import com.ciitc.webServices.model.Collection;
import com.ciitc.webServices.model.MemoInfo;
import com.ciitc.webServices.model.Sign;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_PersonCenterController extends BaseController{
	private SignDao signDao = (SignDao) SpringContextUtil.getBean("signDao");
	@SuppressWarnings("rawtypes")
	private MemoInfoDao memoInfoDao = (MemoInfoDao) SpringContextUtil.getBean("memoInfoDao");
	@SuppressWarnings("rawtypes")
	private CollectionDao collectionDao = (CollectionDao) SpringContextUtil.getBean("collectionDao");
	/*
	 * 个人中心接口
	 */
	@RequestMapping(value = "getPersonCenter",method=RequestMethod.GET)
	@ResponseBody
	public String getPersonCenter(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		SimpleDateFormat sdf1=new SimpleDateFormat("yyyy-MM-dd");
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> signmap = new HashMap<String, Object>();
		if(json.has("financialId")) {
			String financialId = json.getString("financialId");
			if(null != financialId && !"".equals(financialId)) {
				//根据理财师id，查询当天是否已签到
				Sign sign = new Sign();
				Date signDate = new Date();
				sign.setFinancialId(financialId);
				sign.setRemark1(sdf1.format(signDate));
				//sign.setSignDate(signDate);
				signmap.put("sign", sign);
				List<Sign> signList = signDao.queryByFinancialId(signmap);
				if(signList.size()>0) {
					//已签到
					map.put("sign", "0");
				}else {
					//未签到
					map.put("sign", "1");
				}
				//根据理财师id，查询当月的备忘信息
				Calendar calendar = Calendar.getInstance();
				MemoInfo memoInfo = new MemoInfo();
				memoInfo.setFinancialId(financialId);
				int num;
				int year = calendar.get(Calendar.YEAR);
				int month = calendar.get(Calendar.MONTH)+1;
				if(month == 1 || month == 3 || month == 5 || 
						month == 7 || month == 8 || month == 10 || month == 12 ) {
					num = 31;
				}else if(month == 4 || month == 6 || month == 9 || month == 11) {
					num = 30;
				}else {
					 if(year%400==0||(year%4==0 && year%100!=0)){  
				            num = 29;  
				        }else{  
				            num = 28;  
				        }  
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String start = year +"-"+ month +"-1" +" 00:00:00";
				String end = year +"-"+ month +"-"+num +" 23:59:59";
				try {
					Date startDate=sdf.parse(start);
					Date endDate=sdf.parse(end);
					memoInfo.setStartDate(startDate);
					memoInfo.setEndDate(endDate);
					SimpleDateFormat sdff = new SimpleDateFormat("yyyy-MM-dd");
					List<MemoInfo> memoInfoList  = new ArrayList<MemoInfo>();
					memoInfoList  = memoInfoDao.selectMonth(memoInfo);
					if(memoInfoList.size() > 0) {
						for (MemoInfo memoInfo2 : memoInfoList) {
							memoInfo2.setRemark1(sdff.format(memoInfo2.getInfoMomeDate()));
						}
						map.put("memoInfoList", memoInfoList);
						//查询成功
					}else {
						//查询无结果
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//根据理财师id，查询理财师的收藏信息（挨个板块查询）
				Collection collection = new Collection();
				collection.setFinancialId(financialId);
				//客户板块  00客户，
				List<Collection> khCollection = new ArrayList<Collection>();
				collection.setPlate("00");
				khCollection = collectionDao.selectList(collection);
				if(khCollection.size()>0) {
					map.put("khCollection", khCollection);
				}
				//产品板块  01产品，
				List<Collection> cpCollection = new ArrayList<Collection>();
				collection.setPlate("01");
				cpCollection = collectionDao.selectList(collection);
				if(cpCollection.size()>0) {
					map.put("cpCollection", cpCollection);
				}
				//知识库板块  02知识库。
				List<Collection> zskCollection = new ArrayList<Collection>();
				collection.setPlate("02");
				zskCollection = collectionDao.selectList(collection);
				if(cpCollection.size()>0) {
					map.put("zskCollection", zskCollection);
				}
				state = "00";
				msg = "查询成功";
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString(); 
		return jsons;
	}

}

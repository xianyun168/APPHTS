package com.ciitc.webServices.interfacesCrm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CollectionDao;
import com.ciitc.webServices.dao.CrmMarkTitleDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTtrustcontractReq;
import com.ciitc.webServices.model.Collection;
import com.ciitc.webServices.model.CrmMarkTitle;
import com.ciitc.webServices.model.Integration;
import com.ciitc.webServices.util.CRM_CustomInfoUntil;
import com.ciitc.webServices.util.CalculationIntegral;
import com.ciitc.webServices.util.KfcrmCustor;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_CustomInfoController extends BaseController {
	
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao =  (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	private CrmMarkTitleDao crmMarkTitleDao = (CrmMarkTitleDao) SpringContextUtil.getBean("crmMarkTitleDao");
	@SuppressWarnings("rawtypes")
	private CollectionDao collectionDao = (CollectionDao) SpringContextUtil.getBean("collectionDao");
	//客户信息新增接口
	@RequestMapping(value = "addCustomInfo",method=RequestMethod.GET)
	@ResponseBody
	public String addCustomInfo(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		AppUserInfor appUserInfor = null;
		if(json.has("p_custname") && json.has("p_sex") && json.has("p_mobileno") && json.has("p_address") && json.has("financialId") 
			&& 	json.has("customerBirthday")) {
			//根据手机号判断客户是否存在
			String mobileno = json.getString("p_mobileno");
			int num = customInfoUntil.queryByMobileno(mobileno);
			if(num>0) {
				state = "01";
				msg = "客户已存在";
			}else {
				String responsecustno = customInfoUntil.addCustomInfo(json);
				
				if(null != responsecustno && !"".equals(responsecustno)) {
					if("0".equals(responsecustno)) {
						state = "01";
						msg = "新增客户信息数据保存失败";
					}else {
						appUserInfor = customInfoUntil.getCustByNo(responsecustno);
						appUserInfor.setPkId(UUID.randomUUID().toString());//pkId
						appUserInfor.setNameStatus("0");//实名认证状态
						appUserInfor.setCustomerLevel("1");//客户级别
						appUserInfor.setIntegral(0);//客户积分
						appUserInfor.setFinancialId(json.getString("financialId"));//理财师id
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date customerBirthday = null;
						try {
							customerBirthday = sdf.parse(json.getString("customerBirthday"));
							appUserInfor.setCustomerBirthday(customerBirthday);//客户生日
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						appUserInforDao.insertInfor(appUserInfor);
						state = "00";
						msg = "客户信息保存成功";
					}
				}else {
					state = "01";
					msg = "新增客户信息数据保存失败";
				}
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	//客户信息修改接口
	@RequestMapping(value = "updateCustomInfo",method=RequestMethod.GET)
	@ResponseBody
	public String updateCustomInfo(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		AppUserInfor appUserInfor = new AppUserInfor();
		if(json.has("p_custno") && json.has("p_sex") && json.has("p_mobileno") && json.has("p_address") && 	json.has("p_custname")) {
			Integer count = customInfoUntil.updateCust(json.getString("p_custno"));
			if(count<=0) {
				String returncode = customInfoUntil.updateCustomInfo(json);
				if(null !=returncode && !"".equals(returncode)) {
					if("1".equals(returncode)) {
						//String shNo = customInfoUntil.updateShNo(json.getString("p_custno"));
						/*if(null != shNo && !"".equals(shNo)) {
							String shState = customInfoUntil.updateQueryState(shNo);
							if(null != shState && "1".equals(shState)) {
								AppUserInfor appUserInfor1 = appUserInforDao.queryByCustomerId(json.getString("p_custno"));
								appUserInfor.setPkId(appUserInfor1.getPkId());
								appUserInfor.setCustomerId(json.getString("p_custno"));
								appUserInfor.setCustomerSex(json.getString("p_sex"));
								appUserInfor.setUserName(json.getString("p_custname"));
								appUserInfor.setPhoneNumber(json.getString("p_mobileno"));
								appUserInfor.setCustomerAddress(json.getString("p_address"));
								appUserInforDao.updateByPrimaryKey(appUserInfor);
								state = "00";
								msg = "客户信息已成功修改";
							}else {
								state = "01";
								msg = "系统审核未通过或待审核";
							}
						}else {
							state = "01";
							msg = "需审核的客户信息不存在";
						}*/
						state = "00";
						msg = "客户信息修改申请已提交";
					}else {
						state = "01";
						msg = "客户信息修改申请提交失败";
					}
				}
			}else {
				state = "01";
				msg = "此客户存在待审核申请，无法进行修改";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	//客户信息修改接口(点编辑的时候调的接口)
	@RequestMapping(value = "updateCustomInfoBefore",method=RequestMethod.GET)
	@ResponseBody
	public String updateCustomInfoBefore(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		if(json.has("p_custno")) {
			Integer count = customInfoUntil.updateCust(json.getString("p_custno"));
			if(count<=0) {
				state = "00";
				msg = "可以修改";
			}else {
				state = "01";
				msg = "此客户存在待审核申请，无法进行修改";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	//客户信息查询接口（列表）
	@SuppressWarnings("static-access")
	@RequestMapping(value = "queryCustomInfo",method=RequestMethod.GET)
	@ResponseBody
	public String queryCustomInfo(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		KfcrmCustor kfcrmCustor = new KfcrmCustor();
		if(json.has("financialId")) {
			String financialId = json.getString("financialId");
			if(null != financialId && !"".equals(financialId)) {
				String parameter =null;
				if(json.has("parameter")) {
					parameter = json.getString("parameter");
				}
				String name = parameter;
				String phone = parameter;
				List<AppUserInfor> appUserInforsList = kfcrmCustor.queryCustomList(financialId,name,phone);
				if(appUserInforsList.size()>0) {
					state = "00";
					msg = "查询成功";
					map.put("listUser", appUserInforsList);
				}else {
					state = "01";
					msg = "查询无结果";
				}
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	/*
	 * 客户详情接口
	 */
	@RequestMapping(value = "queryCustomInfoById",method=RequestMethod.GET)
	@ResponseBody
	public String queryCustomInfoById(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		String collectionState ="";
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		CalculationIntegral CalculationIntegral = new CalculationIntegral();
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		//CRM_QueryStateUntil queryStateUntil = new CRM_QueryStateUntil();
		KfcrmCustor kfcrmCustor = new KfcrmCustor();
		Double balance = 0.0;
		if(json.has("customerId")&&json.has("financialId")) {
			String customerId = json.getString("customerId");
			String financialId = json.getString("financialId");
			if(null != customerId && !"".equals(customerId)) {
				//根据客户编号查询客户信息
				AppUserInfor appUserInfor = appUserInforDao.queryByCustomerId(customerId);
				if(null != appUserInfor) {
					//根据客户id查询客户信息
	  				AppUserInfor appuser = customInfoUntil.getCustByNo(customerId);
	  				if(null != appuser) {
	  					AppUserInfor app = new AppUserInfor();
	  					app.setUserName(appuser.getUserName());
	  					app.setPhoneNumber(appuser.getPhoneNumber());
	  					app.setCustomerAddress(appuser.getCustomerAddress());
	  					app.setCustomerSex(appuser.getCustomerSex());
	  					app.setCustomerId(appuser.getCustomerId());
	  					app.setIdcardNumber(appuser.getIdcardNumber());
	  					app.setIdcardType(appuser.getIdcardType());
	  					app.setcIsDelete(appuser.getcIsDelete());//客户是否被删除
	  					String nameStatus = kfcrmCustor.getStateBycustNo(customerId);
		  				if(null != nameStatus && "已认证".equals(nameStatus)) {
		  					app.setNameStatus("1");
		  					appUserInfor.setNameStatus("1");
		  				}else if(null != nameStatus && "待审核".equals(nameStatus)) {
		  					app.setNameStatus("2");
		  					appUserInfor.setNameStatus("2");
		  				}else if(null != nameStatus && "已驳回".equals(nameStatus)){
		  					app.setNameStatus("3");
		  					appUserInfor.setNameStatus("3");
		  				}else {
		  					app.setNameStatus("0");
		  					appUserInfor.setNameStatus("0");
		  				}
		  				app.setCustomerBirthday(appuser.getCustomerBirthday());
  						appUserInforDao.updateBy(app);
  						appUserInfor.setCustomerBirthday(appuser.getCustomerBirthday());
  						appUserInfor.setUserName(appuser.getUserName());
  						appUserInfor.setPhoneNumber(appuser.getPhoneNumber());
  						appUserInfor.setCustomerAddress(appuser.getCustomerAddress());
  						appUserInfor.setCustomerSex(appuser.getCustomerSex());
  						appUserInfor.setIdcardNumber(appuser.getIdcardNumber());
  						appUserInfor.setIdcardType(appuser.getIdcardType());
  						appUserInfor.setcIsDelete(appuser.getcIsDelete());//客户是否被删除
  					}
					//根据客户的编号查询客户的标签
					List<CrmMarkTitle> crmMarkTitleList = crmMarkTitleDao.queryMarkTitles(customerId);
					map.put("crmMarkTitleList", crmMarkTitleList);
					//根据客户的编号查询当前持仓金额
					//根据客户的编号查询客户的合同
					List<CPTtrustcontractReq> trustcontractList = customInfoUntil.queryByCustNo(customerId);
					if(trustcontractList.size()>0) {
						for (CPTtrustcontractReq cpTtrustcontractReq : trustcontractList) {
							//获取合同中产品的产品编号
							String fundCode = cpTtrustcontractReq.getcFundcode();
							//根据产品的编号查询产品是否到期
							String code = customInfoUntil.queryByFundcode(fundCode);
							//如果未到期则金额相加
							if("0".equals(code)) {
								balance += cpTtrustcontractReq.getfContractsignbalance();
							}
						}
					}else {
						//客户当前持仓金额
						balance = 0.0;
					}
					map.put("balance", balance);
					//根据客户的编号查询峰值持仓金额
					Double balancePaek = customInfoUntil.getPeakByCustNo(customerId);
					map.put("balancePaek", balancePaek);
					//根据客户编号查询是否被收藏
					Collection collection1 = new Collection();
					collection1.setFinancialId(financialId);
					collection1.setCollectionId(customerId);
					Collection collection = collectionDao.selectOne(collection1);
					if(null != collection) {
						//已收藏
						collectionState = "01";
					}else {
						//未收藏
						collectionState = "00";
					}
					//查询是否是专业或者合格
					//"2"专业/合格，"1"待审核 "0"未认证
					//根据客户编号查询客户是否是合格投资者
					String hg = kfcrmCustor.queryInvestorAuthenticationState(appUserInfor.getCustomerId());
					if(null != hg && "再次认证".equals(hg)) {
						appUserInfor.setHgState("2");
					}else if(null != hg && "待审核".equals(hg)) {
						appUserInfor.setHgState("1");
					}else {
						appUserInfor.setHgState("0");
					}
					
					//根据客户编号查询客户是否是专业投资者
					String zy = kfcrmCustor.queryProfessionalAuthState(appUserInfor.getCustomerId());
					if(null != zy && "已认证".equals(zy)) {
						appUserInfor.setZyState("2");
					}else if(null != zy && "待审核".equals(zy)) {
						appUserInfor.setZyState("1");
					}else {
						appUserInfor.setZyState("0");
					}
					if(null != appUserInfor.getCustomerBirthday()) {
						//客户生日转为String
						appUserInfor.setRemark2(sdf.format(appUserInfor.getCustomerBirthday()));
					}
					map.put("appUserInfor", appUserInfor);
					state = "00";
					msg = "查询成功";
					map.put("collectionState", collectionState);
				}else {
					//查询crm库，添加到本地然后显示到页面
					//根据客户的编号查询客户的信息
					AppUserInfor appUserInfor2 = customInfoUntil.queryCustomInfo(customerId,financialId);
					//积分
					appUserInfor2.setIntegral(0);
					//pkId
					appUserInfor2.setPkId(UUID.randomUUID().toString());
					//客户等级的计算方法
					String nowDate = sdf.format(new Date());
					/*Integer a = Integer.parseInt(nowDate.substring(0, 4))-1;
					String nowDateYear =String.valueOf(a);
					String dates = nowDateYear+"1231";
					String datey = nowDateYear+"0101";*/
					String level = CalculationIntegral.CustomerLevelBycustoNo1(appUserInfor2.getCustomerId());
					appUserInfor2.setCustomerLevel(level);
					appUserInforDao.insertSelective(appUserInfor2);
					//调用客户积分
					//1.先算出当前时间减上365之后的时间
					Date date = new Date();//当前时间
					Calendar ca = Calendar.getInstance();
					ca.add(Calendar.DATE, -365);
					date = ca.getTime();//减后时间
					//2.再判断当前时间和减后时间年份是否一样
					if(sdf.format(new Date()).substring(0, 4).equals(sdf.format(date).substring(0, 4))) {
						//3.如果年份一样则查询这一段时间的合同，按照客户表里的等级计算积分。
						Date yesterDay = new Date(new Date().getTime()-86400000L);
						List<Integration> integrationList  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay,date);
						CalculationIntegral.tradingIntegral(integrationList);
					}else {
						try {
							//4.如果年份不一样，则查询减后时间到年底的合同，等级按照加后时间的前一年年底的时间算，
							Date yesterDay = sdf.parse(sdf.format(date).substring(0, 4)+"-12-31");
							List<Integration> integrationList  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay,date);
							String yesterYear = String.valueOf((Integer.parseInt(sdf.format(date).substring(0, 4))-1))+"1231";
							String yesterYears = String.valueOf((Integer.parseInt(sdf.format(date).substring(0, 4))-1))+"0101";
							//String level1 = CalculationIntegral.CustomerLevelBycustoNo1(appUserInfor.getCustomerId(),yesterYear,yesterYears);
							String level1 = CalculationIntegral.CustomerLevelBycustoNo1(appUserInfor.getCustomerId());
							CalculationIntegral.tradingIntegral2(integrationList,level1);
							//当前时间到此年份之初的合同按照客户表里的等级计算
							Date yesterDay2 = sdf.parse(sdf.format(new Date()).substring(0, 4)+"-01-01");
							Date yesterDay1 = new Date(new Date().getTime()-86400000L);
							List<Integration> integrationList1  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay1,yesterDay2);
							CalculationIntegral.tradingIntegral(integrationList1);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					/*List<Integration> integrationList  = customInfoUntil.queryByCustNo1(appUserInfor2.getCustomerId());
					--CalculationIntegral.tradingIntegral(integrationList);*/
					//再次根据客户编号查询客户信息
					AppUserInfor appUserInfor3 = appUserInforDao.queryByCustomerId(customerId);
					//根据客户的编号查询客户的标签
					List<CrmMarkTitle> crmMarkTitleList = crmMarkTitleDao.queryMarkTitles(customerId);
					map.put("crmMarkTitleList", crmMarkTitleList);
					//根据客户的编号查询当前持仓金额
					//根据客户的编号查询客户的合同
					List<CPTtrustcontractReq> trustcontractList = customInfoUntil.queryByCustNo(customerId);
					if(trustcontractList.size()>0) {
						for (CPTtrustcontractReq cpTtrustcontractReq : trustcontractList) {
							//获取合同中产品的产品编号
							String fundCode = cpTtrustcontractReq.getcFundcode();
							//根据产品的编号查询产品是否到期
							String code = customInfoUntil.queryByFundcode(fundCode);
							//如果未到期则金额相加
							if("0".equals(code)) {
								balance += cpTtrustcontractReq.getfContractsignbalance();
							}
						}
					}else {
						//客户当前持仓金额
						balance = 0.0;
					}
					map.put("balance", balance);
					//根据客户的编号查询峰值持仓金额
					Double balancePaek = customInfoUntil.getPeakByCustNo(customerId);
					map.put("balancePaek", balancePaek);
					//根据客户编号查询是否被收藏
					Collection collection1 = new Collection();
					collection1.setFinancialId(financialId);
					collection1.setCollectionId(customerId);
					Collection collection = collectionDao.selectOne(collection1);
					if(null != collection) {
						//已收藏
						collectionState = "01";
					}else {
						//未收藏
						collectionState = "00";
					}
					//查询是否是专业或者合格
					//"2"专业/合格，"1"待审核 "0"未认证
					//根据客户编号查询客户是否是合格投资者
					String hg = kfcrmCustor.queryInvestorAuthenticationState(appUserInfor3.getCustomerId());
					if(null != hg && "再次认证".equals(hg)) {
						appUserInfor3.setHgState("2");
					}else if(null != hg && "待审核".equals(hg)) {
						appUserInfor3.setHgState("1");
					}else {
						appUserInfor3.setHgState("0");
					}
					
					//根据客户编号查询客户是否是专业投资者
					String zy = kfcrmCustor.queryProfessionalAuthState(appUserInfor3.getCustomerId());
					if(null != zy && "已认证".equals(zy)) {
						appUserInfor3.setZyState("2");
					}else if(null != zy && "待审核".equals(zy)) {
						appUserInfor3.setZyState("1");
					}else {
						appUserInfor3.setZyState("0");
					}
					if(null != appUserInfor3.getCustomerBirthday()) {
						//客户生日转为String
						appUserInfor3.setRemark2(sdf.format(appUserInfor3.getCustomerBirthday()));
					}
					map.put("appUserInfor", appUserInfor3);
					map.put("collectionState", collectionState);
					state = "00";
					msg = "查询成功";
				}
				
			}else {
				state = "01";
				msg = "必传字段不能为空";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	//客户实名认证接口
	@RequestMapping(value = "customInfoReister",method=RequestMethod.GET)
	@ResponseBody
	public String customInfoReister(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		if(json.has("p_custno") && json.has("p_identitytype") && json.has("p_identityno")
			&& json.has("p_email") && json.has("p_MOBILENO") && json.has("p_address") &&
			json.has("financialId") && json.has("custnoName")) {
			Integer count = customInfoUntil.querySmShCount(json.getString("p_custno"));
			if(count<=0) {
				//根据客户编号存客户所属分支机构
				/*AppUserInfor appUserInfor = new AppUserInfor();
				appUserInfor.setCustomerId(json.getString("p_custno"));
				appUserInfor.setRegion(json.getString("servicePath"));
				appUserInforDao.updateByRegion(appUserInfor);
*/				String code = customInfoUntil.customInfoReister(json, request);
				if(null != code && !"".equals(code)) {
					state = code.split("-")[0];
					msg = code.split("-")[1];
				}
			}else {
				state = "01";
				msg = "此客户存在待审核申请，无法进行再次提交申请";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	//已实名认证的客户列表
	@SuppressWarnings("static-access")
	@RequestMapping(value = "queryCustomInfo1",method=RequestMethod.GET)
	@ResponseBody
	public String queryCustomInfo1(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		KfcrmCustor kfcrmCustor = new KfcrmCustor();
		if(json.has("financialId")) {
			String financialId = json.getString("financialId");
			if(null != financialId && !"".equals(financialId)) {
				List<AppUserInfor> listUser = kfcrmCustor.querySMCustomList(financialId);
				if(listUser.size()>0) {
					map.put("listUser", listUser);
					state = "00";
					msg = "查询成功";
				}else {
					state = "01";
					msg = "无查询结果";
				}
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	//再次实名认证回显接口
	@RequestMapping(value = "customInfoReisterHx",method=RequestMethod.GET)
	@ResponseBody
	public String customInfoReisterHx(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		if(json.has("p_custno")) {
			AppUserInfor appUserInfor = customInfoUntil.queryByCustomId(json.getString("p_custno"));
			if(null != appUserInfor) {
				state = "00";
				msg = "查询成功";
				map.put("appUserInfor", appUserInfor);
			}else {
				state = "01";
				msg = "查询无结果";
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	//再次实名认证接口
	@RequestMapping(value = "customInfoReisterOne",method=RequestMethod.GET)
	@ResponseBody
	public String customInfoReisterOne(HttpServletRequest request,JSONObject json) {
		String state = "";
		String msg = "";
		Map<String, Object> map = new HashMap<String, Object>();
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		if(json.has("p_custno") && json.has("p_identitytype") && json.has("p_identityno")
			&& json.has("p_email") && json.has("p_MOBILENO") && json.has("p_address") &&
			json.has("financialId") && json.has("custnoName")&& json.has("lserialno")) {
			String code = customInfoUntil.customInfoReisterOne(json, request);
			if(null != code && !"".equals(code)) {
				state = code.split("-")[0];
				msg = code.split("-")[1];
			}
		}else {
			state = "01";
			msg = "必传字段不能为空";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

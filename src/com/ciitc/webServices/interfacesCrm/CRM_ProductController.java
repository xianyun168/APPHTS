package com.ciitc.webServices.interfacesCrm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.CPTtrustcontractReqDao;
import com.ciitc.webServices.dao.CollectionDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.dao.ProductAppendixInfoDao;
import com.ciitc.webServices.dao.ProductFkDao;
import com.ciitc.webServices.dao.ProductInfoDao;
import com.ciitc.webServices.model.CPTtrustcontractReq;
import com.ciitc.webServices.model.Collection;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.ProductAppendixInfo;
import com.ciitc.webServices.model.ProductFk;
import com.ciitc.webServices.model.ProductInfo;
import com.ciitc.webServices.util.Log;
import com.ciitc.webServices.util.ProductLimtUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class CRM_ProductController extends BaseController {
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private ProductFkDao productFkDao = (ProductFkDao) SpringContextUtil.getBean("productFkDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private EsbmmCodeTypeDao esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil.getBean("esbmmCodeTypeDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private EsbmmCodeDao esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");	
	@SuppressWarnings("rawtypes")
	private CollectionDao collectionDao = (CollectionDao) SpringContextUtil.getBean("collectionDao");	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Autowired(required = false)
	private ProductInfoDao<ProductInfo> productInfoDao = (ProductInfoDao) SpringContextUtil.getBean("productInfoDao");
	@SuppressWarnings({ "rawtypes" })
	@Autowired(required = false)
	private ProductAppendixInfoDao productAppendixInfoDao = (ProductAppendixInfoDao) SpringContextUtil.getBean("productAppendixInfoDao");
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Autowired(required = false)
	private CPTtrustcontractReqDao<CPTtrustcontractReq> CPTtrustcontractReqDao = (CPTtrustcontractReqDao) SpringContextUtil.getBean("CPTtrustcontractReqDao");
	/*
	 * 产品查询列表、详情接口
	 */
	@RequestMapping(value = "getProductList",method=RequestMethod.GET)
	@ResponseBody
	public String getProductList(HttpServletRequest request,JSONObject json) {
		HashMap<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		String filestate="";
		String statefj="";
		//产品编号，把产品编号放到pkId里面
		String pkId = "";
		//产品名称
		String productname ="";
		//产品类型
		String pruducttype ="";
		//存续期限
		String deadline="";
		//理财师id
		String financialId = "";
		if(json1.has("financialId")){
			financialId = json1.getString("financialId");
		}
		if(json1.has("pkId")){
			pkId = json1.getString("pkId");
		}
		if(json1.has("productname")) {
			productname = json1.getString("productname");
					
		}	
		if(json1.has("pruducttype")) {
			pruducttype = json1.getString("pruducttype");
					
		}	
		if(json1.has("deadline")) {
			deadline = json1.getString("deadline");
					
		}	
			
		if((null != pkId && !"".equals(pkId))&&(null != financialId && !"".equals(financialId))) {
			//根据产品编号查询详情
			ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
			ProductFk productFk = productFkDao.selectBypid(pkId);
			if(null != productInfo) {
				if(null != productFk) {
					productInfo.setBettingAgreement(productFk.getBettingAgreement());
					productInfo.setCompensationClause(productFk.getCompensationClause());
					productInfo.setRiskManagement(productFk.getRiskManagement());
					productInfo.setSharePledge(productFk.getSharePledge());
				}
				@SuppressWarnings("unchecked")
				List<ProductAppendixInfo> productAppendInfoList =  productAppendixInfoDao.selectByProductidForCrm(productInfo.getPkId());
				if(productAppendInfoList.size() > 0) {
					//取出数据字典里产品附件的地址
					List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao.queryByTypeName("fileURL");
					Map<String, Object> paramMap = new HashMap<String, Object>();
					paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
					List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
					String URL = listCode.get(0).getCodeValue();
					for (ProductAppendixInfo productAppendixInfo : productAppendInfoList) {
						productAppendixInfo.setRemark3(sdf.format(productAppendixInfo.getCreateDate()));
						productAppendixInfo.setUploadAddress(URL+productAppendixInfo.getUploadAddress());
					}
					statefj = "00,查询产品附件成功";
					Log.getLogger().info("查询产品附件列表");
				}else {
					statefj = "01,产品附件无查询结果";
					Log.getLogger().info("产品附件无查询结果");
				}
				//根据产品id和理财师id查询产品是否被收藏
				//查看这条知识库有没有被收藏过
				Map<String, Object> scap = new HashMap<String, Object>();
				scap.put("pkId", pkId);
				scap.put("financialId", financialId);
				Collection collection = collectionDao.selectByTo(scap);
				if(null != collection) {
					//已收藏
					productInfo.setRemark2("01");
				}else {
					//未收藏
					productInfo.setRemark2("00");
				}
				map.put("productAppendInfoList", productAppendInfoList);
				map.put("productInfo", productInfo);
				state = "00,查询成功";
				Log.getLogger().info("查看产品详情");				
			}else {
				state = "01,无查询结果";
				Log.getLogger().info("无查询结果");
			}
			
		}else {
			try {
				//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
				Map<String, Object> map1 = new HashMap<String, Object>();
				map1.put("productname",productname );
				map1.put("pruducttype", pruducttype);
				map1.put("deadline", deadline);
				List<ProductInfo> productInfoList =  productInfoDao.queryByListj(map1);				
				if(productInfoList.size() > 0) {
					for (ProductInfo productInfo2 : productInfoList) {
						ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
						if(null != productFk) {
							productInfo2.setBettingAgreement(productFk.getBettingAgreement());
							productInfo2.setCompensationClause(productFk.getCompensationClause());
							productInfo2.setRiskManagement(productFk.getRiskManagement());
							productInfo2.setSharePledge(productFk.getSharePledge());
						}
						// 计算产品剩余额度
						//int limit = ProductLimtUtil.productEndDate(productInfo2.getPkId());
						//productInfo2.setRemark3(Integer.toString(limit));
					}
					state = "00,查询成功";
					Log.getLogger().info("查询产品信息列表");
					map.put("productInfoList", new ArrayList<>());
				}else {
					state = "01,产品无查询结果";
					Log.getLogger().info("产品无查询结果");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,产品信息查询异常";
			}
		}
		map.put("state", state);
		map.put("filestate", filestate);
		map.put("statefj", statefj);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	
	//根据收益查询产品表所有字段
	 
	@RequestMapping(value = "getProductProfit",method=RequestMethod.GET)
	@ResponseBody
	public String getProductProfit(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		
		//产品编号，把产品编号放到pkId里面
		String pkId = "";
		//理财师id
		String financialId = "";
		if(json1.has("financialId")){
			financialId = json1.getString("financialId");
		}
		//产品名称
		String productname ="";
		if(json1.has("productname")){
			productname = json1.getString("productname");
		}
		if(json1.has("pkId")){
			pkId = json1.getString("pkId");
		}
		if((null != pkId && !"".equals(pkId))&&(null != financialId && !"".equals(financialId))) {
			//根据产品编号查询详情
			ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
			ProductFk productFk = productFkDao.selectBypid(pkId);
			if(null != productInfo) {
				if(null != productFk) {
					productInfo.setBettingAgreement(productFk.getBettingAgreement());
					productInfo.setCompensationClause(productFk.getCompensationClause());
					productInfo.setRiskManagement(productFk.getRiskManagement());
					productInfo.setSharePledge(productFk.getSharePledge());
				}
				//查看这条知识库有没有被收藏过
				Map<String, Object> scap = new HashMap<String, Object>();
				scap.put("pkId", pkId);
				scap.put("financialId", financialId);
				Collection collection = collectionDao.selectByTo(scap);
				if(null != collection) {
					//已收藏
					productInfo.setRemark2("01");
				}else {
					//未收藏
					productInfo.setRemark2("00");
				}
				map.put("productInfo", productInfo);
				state = "00,查询成功";
				Log.getLogger().info("查看产品详情");				
			}else {
				state = "01,无查询结果";
				Log.getLogger().info("无查询结果");
			}
			
		}else {
			try {
				Map<String, Object> map1 = new HashMap<String, Object>();
				map1.put("productname",productname );
				//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
				List<ProductInfo> productInfoList =  productInfoDao.queryByProfit(map1);
				if(productInfoList.size() > 0) {
					for (ProductInfo productInfo2 : productInfoList) {
						ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
						if(null != productFk) {
							productInfo2.setBettingAgreement(productFk.getBettingAgreement());
							productInfo2.setCompensationClause(productFk.getCompensationClause());
							productInfo2.setRiskManagement(productFk.getRiskManagement());
							productInfo2.setSharePledge(productFk.getSharePledge());
						}
						// 计算产品剩余额度
						//int limit = ProductLimtUtil.productEndDate(productInfo2.getPkId());
						//productInfo2.setRemark3(Integer.toString(limit));
					}
					state = "00,查询成功";
					Log.getLogger().info("查询产品信息列表");
					map.put("productInfoList",  new ArrayList<>());
				}else {
					state = "01,产品无查询结果";
					Log.getLogger().info("产品无查询结果");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,产品信息查询异常";
			}
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
	
	
	 //根据投资金额查询产品表所有字段
	 
	@RequestMapping(value = "getProductDeadline",method=RequestMethod.GET)
	@ResponseBody
	public String getProductDeadline(HttpServletRequest request,JSONObject json) {
			Map<String, Object> map = new HashMap<String, Object>();
			JSONObject json1 = JSONObject.fromObject(json);
			String state = "";
			//产品编号，把产品编号放到pkId里面
			String pkId = "";
			//理财师编号
			String financialId = "";
			//产品名称
			String productname ="";
			if(json1.has("productname")){
				productname = json1.getString("productname");
			}
			if(json1.has("financialId")){
				financialId = json1.getString("financialId");
			}
			if(json1.has("pkId")){
				pkId = json1.getString("pkId");
			}
			if((null != pkId && !"".equals(pkId))&&(null != financialId && !"".equals(financialId))) {
				//根据产品编号查询详情
				ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
				ProductFk productFk = productFkDao.selectBypid(pkId);
				if(null != productInfo) {
					if(null != productFk) {
						productInfo.setBettingAgreement(productFk.getBettingAgreement());
						productInfo.setCompensationClause(productFk.getCompensationClause());
						productInfo.setRiskManagement(productFk.getRiskManagement());
						productInfo.setSharePledge(productFk.getSharePledge());
					}
					//查看这条知识库有没有被收藏过
					Map<String, Object> scap = new HashMap<String, Object>();
					scap.put("pkId", pkId);
					scap.put("financialId", financialId);
					Collection collection = collectionDao.selectByTo(scap);
					if(null != collection) {
						//已收藏
						productInfo.setRemark2("01");
					}else {
						//未收藏
						productInfo.setRemark2("00");
					}
					map.put("productInfo", productInfo);
					state = "00,查询成功";
					Log.getLogger().info("查看产品详情");				
				}else {
					state = "01,无查询结果";
					Log.getLogger().info("无查询结果");
				}
				
			}else {
				try {
					Map<String, Object> map1 = new HashMap<String, Object>();
					map1.put("productname",productname );
					//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
					List<ProductInfo> productInfoList =  productInfoDao.queryByDeadline(map1);
					if(productInfoList.size() > 0) {
						for (ProductInfo productInfo2 : productInfoList) {
							ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
							if(null != productFk) {
								productInfo2.setBettingAgreement(productFk.getBettingAgreement());
								productInfo2.setCompensationClause(productFk.getCompensationClause());
								productInfo2.setRiskManagement(productFk.getRiskManagement());
								productInfo2.setSharePledge(productFk.getSharePledge());
							}
							// 计算产品剩余额度
							//int limit = ProductLimtUtil.productEndDate(productInfo2.getPkId());
							//productInfo2.setRemark3(Integer.toString(limit));
						}
						state = "00,查询成功";
						Log.getLogger().info("查询产品信息列表");
						map.put("productInfoList",  new ArrayList<>());
					}else {
						state = "01,产品无查询结果";
						Log.getLogger().info("产品无查询结果");
					}
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					state = "01,产品信息查询异常";
				}
			}
			map.put("state", state);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		
	
	 //根据投资期限查询产品表所有字段
	 
	@RequestMapping(value = "getProductPermin",method=RequestMethod.GET)
	@ResponseBody
	public String getProductPermin(HttpServletRequest request,JSONObject json) {
		Map<String, Object> map = new HashMap<String, Object>();
		JSONObject json1 = JSONObject.fromObject(json);
		String state = "";
		//产品编号，把产品编号放到pkId里面
		String pkId = "";
		//理财师编号
		String financialId = "";
		//产品名称
		String productname ="";
		if(json1.has("productname")){
			productname = json1.getString("productname");
		}
		if(json1.has("financialId")){
			financialId = json1.getString("financialId");
		}
		if(json1.has("pkId")){
			pkId = json1.getString("pkId");
		}
		if((null != pkId && !"".equals(pkId))&&(null != financialId && !"".equals(financialId))) {
			//根据产品编号查询详情
			ProductInfo productInfo = productInfoDao.selectByPrimaryKey(pkId);
			ProductFk productFk = productFkDao.selectBypid(pkId);
			if(null != productInfo) {
				if(null != productFk) {
					productInfo.setBettingAgreement(productFk.getBettingAgreement());
					productInfo.setCompensationClause(productFk.getCompensationClause());
					productInfo.setRiskManagement(productFk.getRiskManagement());
					productInfo.setSharePledge(productFk.getSharePledge());
				}
				//查看这条知识库有没有被收藏过
				Map<String, Object> scap = new HashMap<String, Object>();
				scap.put("pkId", pkId);
				scap.put("financialId", financialId);
				Collection collection = collectionDao.selectByTo(scap);
				if(null != collection) {
					//已收藏
					productInfo.setRemark2("01");
				}else {
					//未收藏
					productInfo.setRemark2("00");
				}
				map.put("productInfo", productInfo);
				state = "00,查询成功";
				Log.getLogger().info("查看产品详情");				
			}else {
				state = "01,无查询结果";
				Log.getLogger().info("无查询结果");
			}
			
		}else {
			try {
				Map<String, Object> map1 = new HashMap<String, Object>();
				map1.put("productname",productname );
				//查询所有，遍历产品列表将产品存续期限“个月”去掉进行排序 
				List<ProductInfo>list1 =productInfoDao.queryALL();
				for (ProductInfo qq : list1){
					String s =qq.getDeadline();
					String removes ="个月";
					qq.setRemark1(s.replaceAll(removes, ""));
				}map.put("list1", list1);
				//查询产品信息列表,若queryByListj()里面有参数，为模糊查询，否则查询全部
				List<ProductInfo> productInfoList =  productInfoDao.queryByPermin(map1);
				if(productInfoList.size() > 0) {
					for (ProductInfo productInfo2 : productInfoList) {
						ProductFk productFk = productFkDao.selectBypid(productInfo2.getPkId());
						if(null != productFk) {
							productInfo2.setBettingAgreement(productFk.getBettingAgreement());
							productInfo2.setCompensationClause(productFk.getCompensationClause());
							productInfo2.setRiskManagement(productFk.getRiskManagement());
							productInfo2.setSharePledge(productFk.getSharePledge());
						}
					}
					state = "00,查询成功";
					Log.getLogger().info("查询产品信息列表");
					map.put("productInfoList",  new ArrayList<>());
				}else {
					state = "01,产品无查询结果";
					Log.getLogger().info("产品无查询结果");
				}
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				state = "01,产品信息查询异常";
			}
		}
		map.put("state", state);
		String jsons = JSONArray.fromObject(map).toString();
		return jsons;
	}
}

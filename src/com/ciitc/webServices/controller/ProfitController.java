package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.CPcfResult;
import com.ciitc.webServices.model.ProductInfo;
import com.ciitc.webServices.service.CPcfResultService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.ProductInfoService;
import com.ciitc.webServices.util.CRM_CustomInfoUntil;
import com.ciitc.webServices.util.CrmFunctionUtil;
import com.ciitc.webServices.util.ExcelUtil;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

/*******************************************************************************
 * 模块名称: profitController 地址功能描述: 确认及付息 创 建 人: 日 期: 修 改 人:任之阳
 ******************************************************************************/
@Controller
@RequestMapping("/profitController")
public class ProfitController extends BaseController {

	private static final String String = null;
	@Autowired(required = false)
	private CPcfResultService<CPcfResult> cPcfResultService;
	@Autowired(required = false)
	private ProductInfoService<ProductInfo> productInfoService;
	@Autowired(required = false)
	private EsbmmCodeService<?> EsbmmCodeService;
	String cpId = "";
	String fanxiDate = "";
	String productId = "";

	/**
	 * 翻译数据字典
	 */
	/*
	 * public List<Profit> translate(List<Profit> list) { Map<String, Object>
	 * paramMap = new HashMap<String, Object>(); EsbmmCode code = new
	 * EsbmmCode(); for (Profit s : list) { if (s != null && !s.equals("")) {
	 * //是否到期 paramMap.put("typeName", "productStatus");
	 * paramMap.put("codeValue", s.getProductStatus()); code =
	 * EsbmmCodeService.getCodeName(paramMap);
	 * s.setProductStatus(code.getChineseName()); } } return list; }
	 */

	@RequestMapping(params = "profitList=key")
	public ModelAndView profitList(String pkId,String name) {
		cpId = pkId;
		Map<String, Object> context = getRootMap();
		context.put("cpId", cpId);
		context.put("name", name);
		return new ModelAndView("pages/sysManager/profitInfo/profitList", context);
	}

	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			CrmFunctionUtil util = new CrmFunctionUtil();
			util.getHTInfo1();// 与crm数据库同步合同
			//util.getProfit();// 查询返息时先删表再将crm中的数据存入本地
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			// paramMap.put("cPcfResult", cPcfResult);
			paramMap.put("L_PROSERIALNO", cpId);
			List<CPcfResult> list = new ArrayList<CPcfResult>();
			try {
				list = cPcfResultService.queryDateList(paramMap);

				for (CPcfResult cPcfResult : list) {
					SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
					Date date = sd.parse(cPcfResult.getShouyiDate());
					String qianshu = sd.format(date);
					cPcfResult.setRemark1(qianshu);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(params = "detailList=key")
	public ModelAndView detailList(String remark1, String cpId) {
		fanxiDate = remark1 + " 00:00:00";
		productId = cpId;
		Map<String, Object> context = getRootMap();
		context.put("fanxiDate", fanxiDate);
		return new ModelAndView("pages/sysManager/profitInfo/fanxiList", context);
	}

	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrids=key")
	public void datagrids(HttpServletRequest request, HttpServletResponse response, BasePage basePage,
			CPcfResult cPcfResult) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}

			Map<String, Object> paramMap1 = new HashMap<String, Object>();
			paramMap1.put("page", basePage);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = sdf.parse(fanxiDate);
			paramMap1.put("DATA_DATE", date);
			paramMap1.put("L_PROSERIALNO", productId);
			paramMap1.put("px", basePage.getOrder());
			//System.out.println(basePage.getOrder());
			List<CPcfResult> list = new ArrayList<CPcfResult>();
			try {
				list = cPcfResultService.queryfanxiList(paramMap1);
				/*for (CPcfResult cPcfResult2 : list) {
					//根据产品编号查询产品名称
					String pkId = Integer.toString(cPcfResult2.getL_PROSERIALNO());
					String productname =  productInfoService.queryByPid(pkId);
					cPcfResult2.setProductName(productname);
					//根据客户编号查询客户名称
					CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
					String name = customInfoUntil.queryCustomInfo1(cPcfResult2.getC_CUSTNO());
					cPcfResult2.setUserName(name);
				}*/
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到编辑页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(HttpServletRequest req, CPcfResult cPcfResult, String CF_PK) {
		try {
			cPcfResult = cPcfResultService.queryById(CF_PK);
		} catch (Exception e) {
			e.printStackTrace();
		}
		Map<String, Object> context = getRootMap();
		context.put("cPcfResult", cPcfResult);
		return new ModelAndView("pages/sysManager/profitInfo/profitupdate", context);
	}

	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 保存 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, String CF_PK, CPcfResult cPcfResult) throws Exception {
		AjaxJson j = new AjaxJson();
		try {
			// PerformanceGoals add =performanceGoalsService.queryById(pkId);
			cPcfResultService.updat(cPcfResult);
			CrmFunctionUtil util = new CrmFunctionUtil();
			util.getUpdate(cPcfResult.getAmount(), cPcfResult.getCF_PK());// 查询返息时先删表再将crm中的数据存入本地
			message = "修改成功";
		} catch (Exception e) {
			e.printStackTrace();
			message = "修改失败";
		}

		j.setMsg(message);

		return j;
	}

	/*************************************************************************
	 * 函数名: queren 功能描述: 收益确认 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "queren=key")
	@ResponseBody
	public AjaxJson queren(HttpServletRequest request, String CF_PK, CPcfResult cPcfResult) throws Exception {
		AjaxJson j = new AjaxJson();
		try {
			// PerformanceGoals add =performanceGoalsService.queryById(pkId);
			CrmFunctionUtil util = new CrmFunctionUtil();
			// cPcfResultService.deleteAll();
			util.getUpdateProfit("A", CF_PK);
			//util.getProfit();// 与crm同步
			cPcfResult.setConfirm_Flag("A");
			cPcfResultService.updat(cPcfResult);
			// cPcfResult.setPROFIT_STATUS("Y");
			// cPcfResultService.updat(cPcfResult);
			message = "修改成功";
		} catch (Exception e) {
			e.printStackTrace();
			message = "修改失败";
		}

		j.setMsg(message);

		return j;
	}

	
	/*************************************************************************
	 * 函数名: querenList 功能描述: 批量收益确认 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "querenList=key")
	@ResponseBody
	public AjaxJson querenList(String ids) throws Exception {
		AjaxJson j = new AjaxJson();
		try {
			CrmFunctionUtil util = new CrmFunctionUtil();
			String[] result=ids.split(",");
			CPcfResult cPcfResult = new CPcfResult();
			cPcfResult.setConfirm_Flag("A");
			for(int i=0; i<result.length;i++){
				util.getUpdateProfit("A", result[i]);
				cPcfResult.setCF_PK(Integer.valueOf(result[i]));
				cPcfResultService.updat(cPcfResult);
			}
			//util.getProfit();// 与crm同步
			message = "修改成功";
			j.setObj("ok");
		} catch (Exception e) {
			e.printStackTrace();
			message = "修改失败";
		}

		j.setMsg(message);

		return j;
	}

	/*************************************************************************
	 * 函数名: fuxi 功能描述: 付息确认 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "fuxi=key")
	@ResponseBody
	public AjaxJson fuxi(HttpServletRequest request, String CF_PK, CPcfResult cPcfResult) throws Exception {
		AjaxJson j = new AjaxJson();
		try {
			// PerformanceGoals add =performanceGoalsService.queryById(pkId);
			/*
			 * cPcfResult.setConfirm_Flag("Y");
			 * cPcfResultService.updat(cPcfResult);
			 */
			CrmFunctionUtil util = new CrmFunctionUtil();
			// cPcfResultService.deleteAll();
			util.getUpdateProfit("Y", CF_PK);
			cPcfResult.setConfirm_Flag("Y");
			cPcfResultService.updat(cPcfResult);
			//util.getProfit();// 与crm数据库同步
			message = "修改成功";
		} catch (Exception e) {
			e.printStackTrace();
			message = "修改失败";
		}

		j.setMsg(message);

		return j;
	}
	
	
	/*************************************************************************
	 * 函数名: fuxiList 功能描述: 批量付息确认 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "fuxiList=key")
	@ResponseBody
	public AjaxJson fuxiList(String ids) throws Exception {
		AjaxJson j = new AjaxJson();
		try {
			CrmFunctionUtil util = new CrmFunctionUtil();
			String[] result=ids.split(",");
			CPcfResult cPcfResult = new CPcfResult();
			cPcfResult.setConfirm_Flag("Y");
			for(int i=0; i<result.length;i++){
				util.getUpdateProfit("Y", result[i]);
				cPcfResult.setCF_PK(Integer.valueOf(result[i]));
				cPcfResultService.updat(cPcfResult);
			}
			//util.getProfit();// 与crm数据库同步
			message = "修改成功";
			j.setObj("ok");
		} catch (Exception e) {
			e.printStackTrace();
			message = "修改失败";
		}

		j.setMsg(message);

		return j;
	}

	/*************************************************************************
	 * 函数名: exportList 功能描述:导出数据信息 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 * 
	 * @throws Exception
	 *************************************************************************/
	@RequestMapping(params = "export=key")
	public void exportList(HttpServletResponse response, HttpServletRequest request, Date Data_date) throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		List<CPcfResult> list = new ArrayList<CPcfResult>();
		paramMap.put("DATA_DATE", Data_date);
		paramMap.put("L_PROSERIALNO", productId);
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd");
		String fileName = sd.format(Data_date);// 文件名
		list = cPcfResultService.queryfanxiList1(paramMap);
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/x-download");
		ExcelUtil.excel(list, request, response, fileName);
	}

	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

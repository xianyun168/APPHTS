package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.MessageRemindRule;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.TimeRemindRuleService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/timeRemindRuleController")
public class TimeRemindRuleController extends BaseController{
	
	@Autowired(required = false)
	private TimeRemindRuleService<MessageRemindRule> timeRemindRuleService;
	
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	private String message;
	
	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "timeList=key")
	public ModelAndView timeList() {
		return new ModelAndView("pages/remindRule/timeRemindList");
	}
	
	
	@RequestMapping(params="datagrid=key")
	public void datagrid(HttpServletResponse response,MessageRemindRule messageRemindRule, BasePage basePage) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		messageRemindRule.setState("1");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		paramMap.put("messageRemindRule", messageRemindRule);
		List<MessageRemindRule> list = new ArrayList<MessageRemindRule>();
		try {
			list = timeRemindRuleService.queryByList(paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 记录总条数
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
		pageUtil.responseDatagrid(response, jObject);
		
	}
	
	@RequestMapping(params="add=key")
	public ModelAndView repairInforAdd(HttpServletRequest req) {
		return new ModelAndView("pages/remindRule/timeRemindAdd");
	}
	
	@RequestMapping(params="modify=key")
	public ModelAndView repairInforModify(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		MessageRemindRule messageRemindRule=timeRemindRuleService.queryById(pkId);
		context.put("messageRemindRule", messageRemindRule);
		return new ModelAndView("pages/remindRule/timeRemindAdd",context);
	}
	
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(MessageRemindRule messageRemindRule) throws Exception{
		AjaxJson json=new AjaxJson();
		if(messageRemindRule.getPkId()!=null && !"".equals(messageRemindRule.getPkId())){
			try {
				timeRemindRuleService.update(messageRemindRule);
				operateLogService.addLog("时间提醒规则配置", "修改时间提醒规则配置日志", Globals.Log_Type_UPDATE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			message = "编辑成功";
		}else{
			String uuid = UUID.randomUUID().toString();
			messageRemindRule.setPkId(uuid);
			messageRemindRule.setState("1");
			messageRemindRule.setRemainMoney(0.0);
			timeRemindRuleService.add(messageRemindRule);
			operateLogService.addLog("时间提醒规则配置", "新增时间提醒规则配置日志", Globals.Log_Type_INSERT);
			message="保存成功";
		}
		json.setMsg(message);
		return json;
	}
	
	@RequestMapping(params="delete=key")
	@ResponseBody
	public AjaxJson delete(MessageRemindRule messageRemindRule) throws Exception{
		AjaxJson json=new AjaxJson();
		if(messageRemindRule.getPkId()!=null && !"".equals(messageRemindRule.getPkId())){
			timeRemindRuleService.delete(messageRemindRule.getPkId());
			operateLogService.addLog("时间提醒规则配置", "删除时间提醒规则配置日志", Globals.Log_Type_DEL);
		} 
		message="删除成功";
		json.setObj(message);
		return json;
		
	}
	
	@RequestMapping(params="specific=key")
	public ModelAndView repairInforSpecific(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		MessageRemindRule messageRemindRule=timeRemindRuleService.queryById(pkId);
		context.put("messageRemindRule", messageRemindRule);
		return new ModelAndView("pages/remindRule/timeRemindSpecific",context);
	}
	
}

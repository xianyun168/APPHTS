package com.ciitc.webServices.controller;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.CompanyInfo;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.service.CompanyInfoService;
import com.ciitc.webServices.service.OperateLogService;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

@Controller
@RequestMapping("/companyInfoController")
public class CompanyInfoController extends BaseController {
	@Autowired(required = false)
	private CompanyInfoService<CompanyInfo> companyInfoService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	/*
	 * 跳转到公司基本信息显示页面
	 */
	@RequestMapping(params = "company=key")
	public ModelAndView company() {
		Map<String, Object> context = getRootMap();
		try {
			CompanyInfo companyInfo = companyInfoService.selectByPrimaryKey();
			context.put("companyInfo", companyInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/company/company",context);
	}
	/*
	 * 跳转到公司简介页面
	 */
	@RequestMapping(params = "companyInfo=key")
	public ModelAndView companyInfo() {
		Map<String, Object> context = getRootMap();
		try {
			CompanyInfo companyInfo = companyInfoService.selectByPrimaryKey();
			context.put("companyInfo", companyInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/company/companyInfo",context);
	}
	/*
	 * 保存修改的公司信息
	 */
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, HttpServletResponse response,
			CompanyInfo companyInfo){
		AjaxJson j = new AjaxJson();
		if(null == companyInfo.getPkId() || "".equals(companyInfo.getPkId())) {
			//生成一个uuid字符串
			String uuid = UUID.randomUUID().toString();
			companyInfo.setPkId(uuid);
			companyInfoService.insertSelective(companyInfo);
			operateLogService.addLog("关于我们", "新增公司信息日志", Globals.Log_Type_INSERT);
			j.setObj("ok");
			j.setMsg("添加成功");
		}else {
			companyInfoService.updateByPrimaryKeySelective(companyInfo);
			operateLogService.addLog("关于我们", "修改公司信息日志", Globals.Log_Type_UPDATE);
			j.setObj("ok");
			j.setMsg("修改成功");
		}
		
		return j;	
	}

}

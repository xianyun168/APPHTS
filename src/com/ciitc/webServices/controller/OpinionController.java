package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.Opinion;
import com.ciitc.webServices.service.AppUserInforService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.OpinionService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/opinionController")
public class OpinionController extends BaseController{
	@Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;
	@Autowired(required = false)
	private OpinionService<Opinion> opinionService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志

	/*
	 * 跳转到列表显示页面
	 */
	@RequestMapping(params = "opinionList=key")
	public ModelAndView opinionList() {
		return new ModelAndView("pages/opinion/opinionList");
	}
	/*
	 * 意见建议列表查询显示
	 */
	@RequestMapping(params = "getOpinionList=key")
	public void getOpinionList(HttpServletResponse response,HttpServletRequest request,
			BasePage basePage,Opinion opinion) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		paramMap.put("opinion", opinion);
		List<Opinion> list = new ArrayList<Opinion>();
		try {
			list = opinionService.queryByList(paramMap);
			//根据数据字典查询状态
			List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("proposalState");
			EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
			EsbmmCode codePkId = new EsbmmCode();
			codePkId.setCodeTypePkId(codeTypePkId.getPkId());
			//根据codeTypePkId查找对应的类型
			List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
			for (Opinion opinion1 : list) {
				for (EsbmmCode esbmmCode : codeList) {
					if(opinion1.getFlag().equals(esbmmCode.getCodeCode())){
						opinion1.setFlag(esbmmCode.getChineseName());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list,cnt);
		pageUtil.responseDatagrid(response, jObject);
	}
	/*
	 * 跳转到详情页面details
	 */
	@RequestMapping(params = "details=key")
	public ModelAndView details(HttpServletRequest request,HttpServletResponse response,Opinion opinion) {
		Map<String, Object> context = getRootMap();
		try {
			//判断意见状态，如果未读改为已读，如果为已读则不变
			if("00".equals(opinion.getFlag())) {
				Opinion opinion2 = new Opinion();
				opinion2.setPkId(opinion.getPkId());
				opinion2.setFlag("01");
				opinionService.updateByPrimaryKeySelective(opinion2);
				operateLogService.addLog("意见建议", "修改意见建议状态", Globals.Log_Type_UPDATE);
			}
			opinion = opinionService.selectByPrimaryKey(opinion.getPkId());
			//根据数据字典查询状态
			List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("proposalState");
			EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
			EsbmmCode codePkId = new EsbmmCode();
			codePkId.setCodeTypePkId(codeTypePkId.getPkId());
			//根据codeTypePkId查找对应的类型
			List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
			for (EsbmmCode esbmmCode : codeList) {
				if(opinion.getFlag().equals(esbmmCode.getCodeCode())){
					opinion.setFlag(esbmmCode.getChineseName());
				}
			}
			//时间格式转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			context.put("submitTime", sdf.format(opinion.getSubmitTime()));
			context.put("opinion", opinion);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/opinion/opinionDel",context);
	}
	/*
	 * 保存新增知识条目
	 */
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, HttpServletResponse response){
		AjaxJson j = new AjaxJson();
		j.setObj("ok");
		j.setMsg("已查看");
		return j;	
	}
}

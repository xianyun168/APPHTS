package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.SmsSend;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.SmsMessageService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import cn.emay.util.APPSendMessageVo;
import cn.emay.util.CustomSmsIdAndMobile;
import cn.emay.util.SMSMessageUntil;
import cn.emay.util.SmsBatchRequest1;
import cn.emay.util.SmsSingleRequest;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/smsMessageController")
public class SmsMessageController extends BaseController{
	
	@Autowired(required = false)
	private SmsMessageService<SmsSend> smsMessageService;
	
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService;
	
	private String message;
	
	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/	
	@RequestMapping(params = "smsMessageList=key")
	public ModelAndView departList() {
		return new ModelAndView("pages/sms/smsMessageList");
	}
	
	@RequestMapping(params="datagrid=key")
	public void datagrid(HttpServletResponse response,SmsSend smsSend, BasePage basePage) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			if(smsSend.getPhoneNumber() !=null && !"".equals(smsSend.getPhoneNumber())) {
				/*if(smsSend.getPhoneNumber().contains("%")) {
					smsSend.setPhoneNumber(smsSend.getPhoneNumber().replaceAll("%", "."));
				}*/
				String phoneNumber ="%"+smsSend.getPhoneNumber()+"%";
				smsSend.setPhoneNumber(phoneNumber);
			}
			paramMap.put("smsSend", smsSend);
			List<SmsSend> list = new ArrayList<SmsSend>();
			try {
				list = smsMessageService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		
	}
	
	@RequestMapping(params="add=key")
	public ModelAndView repairInforAdd(HttpServletRequest req) {
		return new ModelAndView("pages/sms/smsMessageAdd");
	}
	
	@RequestMapping(params="modify=key")
	public ModelAndView repairInforModify(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		SmsSend smsSend=smsMessageService.queryById(pkId);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timingSendDate=sdf.format(smsSend.getTimingSendDate());
		context.put("timingSendDate", timingSendDate);
		context.put("smsSend", smsSend);
		
		return new ModelAndView("pages/sms/smsMessageAdd",context);
	}
	
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	//新增保存编辑方法
	public AjaxJson saveUpdate(SmsSend smsSend) throws Exception{
		UserInfo user = ResourceUtil.getSessionUserName();
		AjaxJson json=new AjaxJson();
		if(smsSend.getPkId()!=null && !"".equals(smsSend.getPkId())){
			try {
				smsSend.setSmsOperateName(user.getUserName());
				smsSend.setSmsSendType("人工");
				smsSend.setSmsSendState("未发送");
				smsSend.setSendDate("-");
				/*String[] phoneNumbers = smsSend.getPhoneNumber().split(",");
				for(int i=0;i<phoneNumbers.length;i++) {
					String phoneNumber = phoneNumbers[i];
					smsSend.setPhoneNumber(phoneNumber);
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("phoneNumber", phoneNumber);
					map.put("smsSendState", "未发送");
					SmsSend smsSend1= smsMessageService.queryByPhone(map);
					if(smsSend1==null) {
						String uuid = UUID.randomUUID().toString();
						smsSend.setPkId(uuid);
						smsMessageService.add(smsSend);
					}else {
						smsSend.setPkId(smsSend1.getPkId());
						smsMessageService.update(smsSend);
					}
				}*/
				smsMessageService.update(smsSend);
				operateLogService.addLog("短信消息发送管理", "编辑短信消息日志", Globals.Log_Type_UPDATE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			message = "编辑成功";
		}else{
			String uuid = UUID.randomUUID().toString();
			smsSend.setPkId(uuid);
			smsSend.setSmsOperateName(user.getUserName());
			smsSend.setSmsSendType("人工");
			smsSend.setSmsSendState("未发送");
			smsSend.setSendDate("-");
			/*String[] phoneNumbers = smsSend.getPhoneNumber().split(",");
			for(int i=0;i<phoneNumbers.length;i++) {
				String phoneNumber = phoneNumbers[i];
				smsSend.setPhoneNumber(phoneNumber);
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("phoneNumber", phoneNumber);
				map.put("smsSendState", "未发送");
				smsMessageService.add(smsSend);
				message="保存成功";
				SmsSend smsSend1= smsMessageService.queryByPhone(map);
				if(smsSend1 != null) {
					message="手机号已存在";
				}else {
					smsMessageService.add(smsSend);
					message="保存成功";
				}
			}*/
			smsMessageService.add(smsSend);
			message="保存成功";
			operateLogService.addLog("短信消息发送管理", "新增短信消息日志", Globals.Log_Type_INSERT);
		}
		json.setMsg(message);
		return json;
	}
	
	
	@RequestMapping(params="sendMessage=key")
	@ResponseBody
	//发送方法
	public AjaxJson sendMessage(SmsSend smsSend) throws Exception{
		SMSMessageUntil sMSMessageUntil = new SMSMessageUntil();//短信发送方法工具类
		SmsSingleRequest smsSingleRequest = new SmsSingleRequest();//单条短信参数对象
		APPSendMessageVo aPPSendMessageVo = new APPSendMessageVo();//短信平台参数对象
		SmsBatchRequest1 smsBatchRequest1 = new SmsBatchRequest1();//批量短信参数对象
		String SMSSignature = null;
		Calendar nowDate = Calendar.getInstance();
		nowDate.add(Calendar.MINUTE, -5); //当前时间-5分钟
		//--------------------------------------
		List<EsbmmCode> listCode = esbmmCodeService.getSMSAppId();
	//	System.out.println(listCode.size());
		for (EsbmmCode esbmmCode : listCode) {//循环遍历取出短信平台所需的配置参数
			if(esbmmCode.getCodeName().equals("AppId")) {
				aPPSendMessageVo.setAppId(esbmmCode.getCodeValue());//AppId
			}
			if(esbmmCode.getCodeName().equals("secretKey")) {
				aPPSendMessageVo.setSecretKey(esbmmCode.getCodeValue());//密匙
			}
			if(esbmmCode.getCodeName().equals("interfaceHost")) {
				aPPSendMessageVo.setHost(esbmmCode.getCodeValue());//接口地址
			}
			if(esbmmCode.getCodeName().equals("algorithm")) {
				aPPSendMessageVo.setAlgorithm(esbmmCode.getCodeValue());//加密算法
			}
			if(esbmmCode.getCodeName().equals("SMSSignature")) {
				SMSSignature = esbmmCode.getCodeValue();//短信签名
			}
		}
		String  SMSSignatures = SMSSignature + smsSend.getSmsMessageContent();//短信内容=短信签名+消息内容
		smsSend.setSmsMessageContent(SMSSignatures);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		UserInfo user = ResourceUtil.getSessionUserName();
		AjaxJson json=new AjaxJson();
		//编辑
		if(smsSend.getPkId()!=null && !"".equals(smsSend.getPkId())){
			try {
				smsSend.setSmsOperateName(user.getUserName());
				smsSend.setSmsSendType("人工");
				smsSend.setSmsSendState("未发送");
				smsSend.setSendDate("-");
				//-----------------------------------
				//单条发送
				if(!smsSend.getPhoneNumber().contains(",")) {
					smsSingleRequest.setMobile(smsSend.getPhoneNumber());
					smsSingleRequest.setContent(smsSend.getSmsMessageContent());
					smsSingleRequest.setTimerTime(sdf.format(smsSend.getTimingSendDate()));
					smsSingleRequest.setRequestTime(System.currentTimeMillis());
					smsSingleRequest.setRequestValidPeriod(30);
					smsSingleRequest.setCustomSmsId("");
					smsSingleRequest.setExtendCode("");
					if(smsSend.getTimingSendDate().after(nowDate.getTime())) {
						String state = sMSMessageUntil.sendSingleSms(smsSingleRequest, false, aPPSendMessageVo,smsSend);
						operateLogService.addLog("短信消息发送管理", "单条短信消息发送日志", Globals.Log_Type_SEND);
						if(null != state && !"".equals(state)) {
							if(state .equals("发送成功")) {
								smsMessageService.update(smsSend);
								operateLogService.addLog("短信消息发送管理", "单条短信消息编辑日志", Globals.Log_Type_UPDATE);
								message="单条发送成功";
							}else {
								message="单条发送失败";
							}
						}else {
							message="单条发送失败";
						}
					}else{
						message="定时过早，单条发送失败";
					}
				}else {
					//多条发送
					String[] phoneNumbers = smsSend.getPhoneNumber().split(",");
					List<String> list = new ArrayList<String>();
					list.add(phoneNumbers[0]);
					for(int i=1;i<phoneNumbers.length;i++){
						if(list.toString().indexOf(phoneNumbers[i]) == -1){
								list.add(phoneNumbers[i]);
						}
					}
					String[] phoneArrayResult = (String[]) list.toArray(new String[list.size()]);
					CustomSmsIdAndMobile[] smses =new CustomSmsIdAndMobile[phoneArrayResult.length];
					for(int j=0;j<phoneArrayResult.length;j++) {
						smses[j] = new CustomSmsIdAndMobile("",phoneArrayResult[j]);
					}
					//System.out.println(smses);
					smsBatchRequest1.setSmses(smses);
					smsBatchRequest1.setContent(smsSend.getSmsMessageContent());
					smsBatchRequest1.setRequestTime(System.currentTimeMillis());
					smsBatchRequest1.setRequestValidPeriod(30);
					smsBatchRequest1.setExtendCode("");
					smsBatchRequest1.setTimerTime(sdf.format(smsSend.getTimingSendDate()));
					if(smsSend.getTimingSendDate().after(nowDate.getTime())) {
						String state = sMSMessageUntil.sendBatchSms(smsBatchRequest1, false, aPPSendMessageVo, smsSend);
						operateLogService.addLog("短信消息发送管理", "多条短信消息发送日志", Globals.Log_Type_SEND);
						if(null != state && !"".equals(state)) {
							if(state .equals("发送成功")) {
								/*for(int i=0;i<phoneNumbers.length;i++) {
									String phoneNumber = phoneNumbers[i];
									smsSend.setPhoneNumber(phoneNumber);
									String uuid = UUID.randomUUID().toString();
									smsSend.setPkId(uuid);
									smsMessageService.add(smsSend);
									Map<String, Object> map = new HashMap<String, Object>();
									map.put("phoneNumber", phoneNumber);
									map.put("smsSendState", "未发送");
									smsMessageService.deleteByPhone(map);
								}*/
								smsMessageService.update(smsSend);
								operateLogService.addLog("短信消息发送管理", "多条短信消息编辑日志", Globals.Log_Type_UPDATE);
								message="批量发送成功";
							}else {
								message="批量发送失败";
							}
						}else {
							message="批量发送失败";
						}
					}else {
						message="超时,批量发送失败";
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			//单条新增发送
			String uuid = UUID.randomUUID().toString();//短信ID
			smsSend.setPkId(uuid);
			smsSend.setSmsOperateName(user.getUserName());//操作人
			smsSend.setSmsSendType("人工");
			smsSend.setSmsSendState("未发送");
			smsSend.setSendDate("-");
			if(!smsSend.getPhoneNumber().contains(",")) {//判断是单个手机号还是多个
				smsSingleRequest.setMobile(smsSend.getPhoneNumber());//手机号
				smsSingleRequest.setContent(smsSend.getSmsMessageContent());//短信内容
				smsSingleRequest.setTimerTime(sdf.format(smsSend.getTimingSendDate()));//定时时间
				smsSingleRequest.setRequestTime(System.currentTimeMillis());//请求时间
				smsSingleRequest.setRequestValidPeriod(30);//请求有效时间
				smsSingleRequest.setCustomSmsId("");//自定义消息ID
				smsSingleRequest.setExtendCode("");//扩展字段
				if(smsSend.getTimingSendDate().after(nowDate.getTime())) {//定时时间和当前时间作比较
					String state = sMSMessageUntil.sendSingleSms(smsSingleRequest, false, aPPSendMessageVo,smsSend);//调用单条短信发送方法
					operateLogService.addLog("短信消息发送管理", "单条短信消息发送日志", Globals.Log_Type_SEND);
					if(null != state && !"".equals(state)) {//获取短信是否发送成功
						if(state .equals("发送成功")) {//短信发送成功时
							smsMessageService.add(smsSend);
							operateLogService.addLog("短信消息发送管理", "单条短信消息新增日志", Globals.Log_Type_INSERT);
							message="单条发送成功";
						}else {
							message="单条发送失败";
						}
					}else {
						message="单条发送失败";
					}
				}else {
					message="定时过早，单条发送失败";
				}
			}else {
				//多条新增发送
				String[] phoneNumbers = smsSend.getPhoneNumber().split(",");//获取前台所有的手机号以逗号分割
				List<String> list = new ArrayList<String>();
				list.add(phoneNumbers[0]);
				for(int i=1;i<phoneNumbers.length;i++){//循环遍历将手机号放入list集合
					if(list.toString().indexOf(phoneNumbers[i]) == -1){
							list.add(phoneNumbers[i]);
					}
				}
				String[] phoneArrayResult = (String[]) list.toArray(new String[list.size()]);//将list转换为数组
			  //System.out.println(Arrays.toString(phoneArrayResult));
				CustomSmsIdAndMobile[] smses =new CustomSmsIdAndMobile[phoneArrayResult.length];
				for(int j=0;j<phoneArrayResult.length;j++) {
					smses[j] = new CustomSmsIdAndMobile("",phoneArrayResult[j]);//将手机号循环放入CustomSmsIdAndMobile对象
				}
			//	System.out.println(smses);
				smsBatchRequest1.setSmses(smses);//手机号和自定义ID
				smsBatchRequest1.setContent(smsSend.getSmsMessageContent());//短信内容
				smsBatchRequest1.setRequestTime(System.currentTimeMillis());//请求时间
				smsBatchRequest1.setRequestValidPeriod(30);//请求有效时间
				smsBatchRequest1.setExtendCode("");//扩展字段
				smsBatchRequest1.setTimerTime(sdf.format(smsSend.getTimingSendDate()));//短信定时时间
				if(smsSend.getTimingSendDate().after(nowDate.getTime())) {//定时时间和当前时间作比较
					String state = sMSMessageUntil.sendBatchSms(smsBatchRequest1, false, aPPSendMessageVo, smsSend);//调用批量发送信息方法
					operateLogService.addLog("短信消息发送管理", "多条短信消息发送日志", Globals.Log_Type_SEND);
					if(null != state && !"".equals(state)) {//获取短信是否发送成功
						if(state .equals("发送成功")) {//短信发送成功时
							/*for(int i=0;i<phoneNumbers.length;i++) {
								String phoneNumber = phoneNumbers[i];
								smsSend.setPhoneNumber(phoneNumber);
								smsMessageService.add(smsSend);
							}*/
							smsMessageService.add(smsSend);
							operateLogService.addLog("短信消息发送管理", "多条短信消息新增日志", Globals.Log_Type_INSERT);
							message="批量发送成功";
						}else {
							message="批量发送失败";
						}
					}else {
						message="批量发送失败";
					}
				}else {
					message="定时过早，批量发送失败";
				}
			}
		}
		json.setMsg(message);
		return json;
	}
	
	
	@RequestMapping(params="delete=key")
	@ResponseBody
	public AjaxJson delete(SmsSend smsSend) throws Exception{
		AjaxJson json=new AjaxJson();
		if(smsSend.getPkId()!=null && !"".equals(smsSend.getPkId())){
			smsMessageService.delete(smsSend.getPkId());
		} 
		operateLogService.addLog("短信消息发送管理", "短信消息删除日志", Globals.Log_Type_DEL);
		message="删除成功";
		json.setObj(message);
		return json;
		
	}
	
	@RequestMapping(params="specific=key")
	public ModelAndView repairInforSpecific(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		SmsSend smsSend=smsMessageService.queryById(pkId);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timingSendDate=sdf.format(smsSend.getTimingSendDate());
		context.put("timingSendDate", timingSendDate);
		context.put("smsSend", smsSend);
		return new ModelAndView("pages/sms/smsMessageSpecific",context);
	}
	
	
}


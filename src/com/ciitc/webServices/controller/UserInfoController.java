package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.MethodUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;
import com.ciitc.webServices.model.Depart;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.RoleInfo;
import com.ciitc.webServices.model.SysPermissionInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.DepartService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.RoleService;
import com.ciitc.webServices.service.SysPermissionInfoService;
import com.ciitc.webServices.service.UserInfoService;

/*******************************************************************************
 * 模块名称: UserInfoController 模块功能描述: 用户管理模块 创 建 人: 日 期: 修 改 人:
 ******************************************************************************/
@Controller
@RequestMapping("/userInfoController")
public class UserInfoController extends BaseController {


	@Autowired(required = false)
	private UserInfoService<UserInfo> userInfoService;
	@Autowired(required = false)
	private RoleService<RoleInfo> roleService;
	@Autowired(required = false)
	private DepartService<Depart> departService;
	@Autowired(required = false)
	private SysPermissionInfoService<SysPermissionInfo> sysPermissionInfoService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	
	@Autowired
	private  EsbmmCodeService<EsbmmCode> esbmmCodeService;

	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 保存 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, UserInfo userInfo)
			throws Exception {
		
			Map<String, Object> paramMap = new HashMap<String, Object>();
			AjaxJson j = new AjaxJson();
		try {
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(userInfo.getUserCode()) || checkChar.checkChar(userInfo.getUserName()) || checkChar.checkChar(userInfo.getEmail()) ||
			checkChar.checkChar(userInfo.getDepartPkId()) || checkChar.checkChar(userInfo.getAreaCode()) ||
			checkChar.checkChar(userInfo.getTelephone())){
				message = "存在不合法字符";
				j.setMsg(message);
				return j;
			}
			
			
			
			
			
			
			
			
			
			if (userInfo.getPkId() != null && !"".equals(userInfo.getPkId())) {
				
				//userInfoService.updateBySelective(userInfo);
				try {
					sysPermissionInfoService.deleteByUserPkId(userInfo.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String[] roleIds = request.getParameter("roleIds").split(",");
				if (roleIds.length > 0) {
					for (String roleId : roleIds) {
						SysPermissionInfo sysPermissionInfo = new SysPermissionInfo();
						sysPermissionInfo.setUserPkId(userInfo.getPkId());
						sysPermissionInfo.setRolePkId(roleId);
						try {
							sysPermissionInfoService.add(sysPermissionInfo);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							message = "保存失败";
							j.setMsg(message);
							return j;
						}
					}
				}
						try {
							userInfoService.updateBySelective(userInfo);
							operateLogService.addLog("用户管理", "更新用户日志", Globals.Log_Type_UPDATE);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							message = "编辑失败";
							j.setMsg(message);
							return j;
						}
						message = "编辑成功";
				

			} else {
				//Map<String ,Object> paramMap=new HashMap<String, Object>();
					//密码设置md5加密
					userInfo.setPassword(MethodUtil.MD5(userInfo.getPassword()));
					try {
						userInfoService.add(userInfo);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					paramMap.put("userCode", userInfo.getUserCode());
					UserInfo user = new UserInfo();
					try {
						user = userInfoService.queryByCode(paramMap).get(0);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					//设置token
					String token= user.getUserCode()+""+user.getCreatedDate();
					token=MethodUtil.MD5(token);
					user.setToken(token);
					try {
						userInfoService.updateBySelective(user);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String roleId = request.getParameter("roleIds");
					if (!"".equals(roleId)) {
						String[] roleIds = request.getParameter("roleIds").split(
								",");
						if (roleIds.length > 0) {
							for (String id : roleIds) {
								SysPermissionInfo sysPermissionInfo = new SysPermissionInfo();
								sysPermissionInfo.setUserPkId(user.getPkId());
								sysPermissionInfo.setRolePkId(id);
								try {
									sysPermissionInfoService.add(sysPermissionInfo);
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
									message = "保存失败";
									j.setMsg(message);
									return j;
								}
							}
						}
					}
					try {
						operateLogService.addLog("用户管理", "保存用户日志",
								Globals.Log_Type_INSERT);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					message = "保存成功";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			j.setMsg(message);
			return j;
	}
	
	/*************************************************************************
	 * 函数名: checkUserCode 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "checkUserCode=key")
	@ResponseBody
	public AjaxJson checkUserCode(HttpServletRequest request, UserInfo userInfo) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		AjaxJson json = new AjaxJson();
		try {
			paramMap.put("userCode", request.getParameter("userCode"));
			List<UserInfo> list = userInfoService.queryByCode(paramMap);
			if(list.size()==0){
				json.setObj("ok");
			}else{
				json.setObj("no");
				json.setMsg("用户名已存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request,
			HttpServletResponse response, UserInfo userInfo, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("userInfo", userInfo);
			List<UserInfo> list = new ArrayList<UserInfo>();
			try {
				list = userInfoService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*************************************************************************
	 * 函数名: delete 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String id) {

		try {
			userInfoService.delete(id);
			operateLogService.addLog("用户管理", "删除用户日志", Globals.Log_Type_DEL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		AjaxJson json = new AjaxJson();
		json.setObj("ok");
		return json;
	}

	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(HttpServletRequest req) {
		List<Depart> departs=new ArrayList<Depart>();
		try {
			departs = departService.query();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<EsbmmCode> esbmmCodes = new ArrayList<EsbmmCode>();
		try {
			esbmmCodes = esbmmCodeService.getProvinceList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		req.setAttribute("departs", departs);
		req.setAttribute("esbmmCodes", esbmmCodes);
		return new ModelAndView("pages/sysManager/user/userInfoAdd");
	}

	/*************************************************************************
	 * 函数名: modify 功能描述: 跳转到修改页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(UserInfo userInfo, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		List<Depart> departs=new ArrayList<Depart>();
		try {
			departs = departService.query();
			userInfo = userInfoService.queryById(userInfo.getPkId());
			List<SysPermissionInfo> list = sysPermissionInfoService
					.queryByUserPkId(userInfo.getPkId());
			if (list != null) {
				StringBuffer roleNames = new StringBuffer();
				StringBuffer roleIds = new StringBuffer();
				for (SysPermissionInfo s : list) {
					if (s.getRolePkId() != null && !"".equals(s.getRolePkId())) {
						roleNames.append(roleService.queryById(s.getRolePkId())
								.getRoleName());
						roleNames.append(",");
						roleIds.append(roleService.queryById(s.getRolePkId())
								.getPkId());
						roleIds.append(",");
					}
				}
				if (!"".equals(roleNames.toString())) {
					roleNames.deleteCharAt(roleNames.length() - 1);
				}
				context.put("roleIds", roleIds);
				context.put("roleNames", roleNames);
			}
			List<EsbmmCode> esbmmCodes=esbmmCodeService.getProvinceList();
			context.put("esbmmCodes", esbmmCodes);
			context.put("userInfo", userInfo);
			context.put("departs", departs);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/user/userInfoModify", context);
	}

	/*************************************************************************
	 * 函数名: sysPermissionInfoAdd 功能描述: 跳转角色数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "sysPermissionInfoAdd=key")
	public ModelAndView sysPermissionInfoAdd() {
		return new ModelAndView("pages/sysManager/sysPermissionInfo/SysPermissionInfoAdd");
	}

	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "userInfoList=key")
	public ModelAndView userInfoList() {
		return new ModelAndView("pages/sysManager/user/userInfoList");
	}
	/**
	 * 重置密码页面
	 * 
	 * @return
	 */
	@RequestMapping(params = "resetPassword=key")
	public ModelAndView modifyPassword(HttpServletRequest request,String pkId) {
		UserInfo user =new UserInfo();
		Map<String, Object> context = getRootMap();
		try {
			user = userInfoService.queryById(pkId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.put("user", user);
		return new ModelAndView("pages/sysManager/user/passwordReset",context);
	}
	
	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

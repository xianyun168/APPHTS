package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppGoodsInfor;
import com.ciitc.webServices.model.Area;
import com.ciitc.webServices.model.City;
import com.ciitc.webServices.model.GoddsAddress;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.Province;
import com.ciitc.webServices.model.SubmitGoodsList;
import com.ciitc.webServices.service.AreaService;
import com.ciitc.webServices.service.CityService;
import com.ciitc.webServices.service.GoddsAddressService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.ProvinceService;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;

/**
 * 收货地址模块
 * @author tdl
 *
 */
@Controller
@RequestMapping("/goddsAddressController")
public class GoddsAddressController extends BaseController{
	@Autowired(required = false)
	private GoddsAddressService<GoddsAddress> goddsAddressService;
	
	@Autowired(required = false)
	private ProvinceService<Province> provinceService;
	
	@Autowired(required = false)
	private CityService<City> cityService;
	
	@Autowired(required = false)
	private AreaService<Area> areaService;
	
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	/*
	 * 跳转到新增页面
	 */
	@RequestMapping(params = "goddsAddressAdd=key")
	public ModelAndView goddsAddressAdd(String userId) {
		Map<String, Object> context = getRootMap();
		List<Province> list = new ArrayList<Province>();
		try {
			list = provinceService.selectAll();
			context.put("list", list);
			context.put("userId", userId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/goddsAddress/goddsAddressAdd",context);
	}
	@RequestMapping(params = "goddsAddressAdd1=key")
	public ModelAndView goddsAddressAdd1(String goodsIds,String goodsNums,String goodsNames,
			String mainImageUrls,String prices,String userId,String total) {
		Map<String, Object> context = getRootMap();
		String[] goodsId = goodsIds.split(",");
		String[] goodsNum = goodsNums.split(",");
		String[] goodsName = goodsNames.split(",");
		String[] mainImageUrl = mainImageUrls.split(",");
		String[] price = prices.split(",");
		List<AppGoodsInfor> listGoods = new ArrayList<AppGoodsInfor>();
		for(int i=0;i<goodsId.length;i++) {
			AppGoodsInfor appGoodsInfor = new AppGoodsInfor();
			appGoodsInfor.setGoodsId(goodsId[i]);
			appGoodsInfor.setGoodsName(goodsName[i]);
			appGoodsInfor.setMainImageUrl(mainImageUrl[i]);
			appGoodsInfor.setPrice(Double.parseDouble(price[i]));
			appGoodsInfor.setNum(Integer.parseInt(goodsNum[i]));
			listGoods.add(appGoodsInfor);
		}
		context.put("listGoods", listGoods);
		List<Province> list = new ArrayList<Province>();
		try {
			list = provinceService.selectAll();
			context.put("list", list);
			context.put("userId", userId);
			context.put("total", total);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/goddsAddress/countAddressAdd",context);
	}
	//跳转到使用地址列表页面
	@RequestMapping(params = "toGoddsAddressList=key")
	public ModelAndView toGoddsAddressList(String goodsIds,String goodsNums,
			String goodsNames,String mainImageUrls,String prices,String userId,String total) {
		Map<String, Object> context = getRootMap();
		String[] goodsId = goodsIds.split(",");
		String[] goodsNum = goodsNums.split(",");
		String[] goodsName = goodsNames.split(",");
		String[] mainImageUrl = mainImageUrls.split(",");
		String[] price = prices.split(",");
		List<AppGoodsInfor> list = new ArrayList<AppGoodsInfor>();
		for(int i=0;i<goodsId.length;i++) {
			AppGoodsInfor appGoodsInfor = new AppGoodsInfor();
			appGoodsInfor.setGoodsId(goodsId[i]);
			appGoodsInfor.setGoodsName(goodsName[i]);
			appGoodsInfor.setMainImageUrl(mainImageUrl[i]);
			appGoodsInfor.setPrice(Double.parseDouble(price[i]));
			appGoodsInfor.setNum(Integer.parseInt(goodsNum[i]));
			list.add(appGoodsInfor);
		}
		context.put("list", list);
		context.put("userId", userId);
		context.put("total", total);
		return new ModelAndView("pages/order/orderCountAddress",context);
	}
	
	/*
	 * 跳转到列表显示页面
	 */
	@RequestMapping(params = "goddsAddressList=key")
	public ModelAndView goddsAddressList(String userId) {
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("userId", userId);
		return new ModelAndView("pages/goddsAddress/goddsAddress",context);
	}
	/*
	 * 地址列表查询显示
	 */
	@RequestMapping(params = "getGoddsAddressList=key")
	public void getGoddsAddressList(HttpServletResponse response,HttpServletRequest request, BasePage basePage,GoddsAddress goddsAddress) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		paramMap.put("goddsAddress", goddsAddress);
		List<GoddsAddress> list = new ArrayList<GoddsAddress>();
		try {
			list = goddsAddressService.queryByList(paramMap);
			if(list.size() >0) {
				for (GoddsAddress goddsAddress2 : list) {
					String provinceid = goddsAddress2.getProvince();
					Province province = provinceService.selectById(provinceid);
					String cityid = goddsAddress2.getCity();
					City city = cityService.selectById(cityid);
					String areaid = goddsAddress2.getArea();
					Area area = areaService.selectById(areaid);
					if(province !=null && city != null &&  area != null ) {
						goddsAddress2.setProvince(province.getProvince());
						goddsAddress2.setCity(city.getCity());
						goddsAddress2.setArea(area.getArea());
					}
						
				}
			}
			response.getWriter().println(JSONArray.fromObject(list).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*
	 * 保存新增地址
	 */
	@RequestMapping(params="saveUpdate=key")
	public ModelAndView saveUpdate(HttpServletRequest request, HttpServletResponse response,
			GoddsAddress goddsAddress){
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("userId", goddsAddress.getUserId());
		//判断新增还是修改
		//1--默认地址
		/*if(goddsAddress.getAddressState().equals("1")) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("addressState", "1");
			map.put("userId", goddsAddress.getUserId());
			GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
			if(null != goddsAddress1) {
				goddsAddress1.setAddressState("0");
				goddsAddressService.updateByPrimaryKey(goddsAddress1);
			}
		}else {
			goddsAddress.setAddressState("0");
		}*/
		if(null == goddsAddress.getPkId()|| "".equals(goddsAddress.getPkId())) {
			//goddsAddress.setUserId(goddsAddress.getUserId());
			if("1".equals(goddsAddress.getAddressState())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("addressState", "1");
				map.put("userId", goddsAddress.getUserId());
				GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
				if(null != goddsAddress1) {
					goddsAddress1.setAddressState("0");
					goddsAddressService.updateByPrimaryKey(goddsAddress1);
				}
			}else {
				goddsAddress.setAddressState("0");
			}
			//----------------------
			Random ra = new Random();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
			String pkId = sdf.format(new Date());
			for(int i = 0;i<2 ; i++) {
				pkId +=Integer.toString(ra.nextInt(10));
			}
			//----------------------
			goddsAddress.setPkId(pkId);
			goddsAddressService.insert(goddsAddress);
			goddsAddress.setAddTime(new Date());
			//operateLogService.addLog("知识库管理", "新增知识条目", Globals.Log_Type_INSERT);
		}else {
			String pkId = goddsAddress.getPkId();
			GoddsAddress goddsAddress2 = goddsAddressService.selectByPrimaryKey(pkId);
			if("1".equals(goddsAddress2.getAddressState())) {
				goddsAddress.setAddressState("1");
			}else {
				if("1".equals(goddsAddress.getAddressState())) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("addressState", "1");
					map.put("userId", goddsAddress.getUserId());
					GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
					if(null != goddsAddress1) {
						goddsAddress1.setAddressState("0");
						goddsAddressService.updateByPrimaryKey(goddsAddress1);
					}
				}else {
					goddsAddress.setAddressState("0");
				}
			}
			goddsAddressService.updateByPrimaryKey(goddsAddress);
			//operateLogService.addLog("知识库管理", "修改知识条目", Globals.Log_Type_UPDATE);
		}
		return new ModelAndView("pages/goddsAddress/goddsAddress",context);	
	}
	
	/*
	 * 保存新增地址
	 */
	@RequestMapping(params="saveUpdate1=key")
	public ModelAndView saveUpdate1(HttpServletRequest request, HttpServletResponse response,
			GoddsAddress goddsAddress,String userId,String total){
		Map<String, Object> context = new HashMap<String, Object>();
		List<AppGoodsInfor> list = new ArrayList<AppGoodsInfor>();
		for (SubmitGoodsList submitGoods : goddsAddress.getGoddsList()) {
			AppGoodsInfor appGoodsInfor = new AppGoodsInfor();
			appGoodsInfor.setGoodsId(submitGoods.getGoodsId());
			appGoodsInfor.setGoodsName(submitGoods.getGoodsName());
			appGoodsInfor.setMainImageUrl(submitGoods.getPictureURL());
			appGoodsInfor.setNum(Integer.parseInt(submitGoods.getNum()));
			appGoodsInfor.setPrice(submitGoods.getPrice());
			list.add(appGoodsInfor);
		}
		context.put("list", list);
		context.put("userId", userId);
		context.put("total", total);
		//判断新增还是修改
		//1--默认地址
		/*if(goddsAddress.getAddressState().equals("1")) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("addressState", "1");
			map.put("userId", goddsAddress.getUserId());
			GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
			if(null != goddsAddress1) {
				goddsAddress1.setAddressState("0");
				goddsAddressService.updateByPrimaryKey(goddsAddress1);
			}
		}else {
			goddsAddress.setAddressState("0");
		}*/
		if(null == goddsAddress.getPkId()|| "".equals(goddsAddress.getPkId())) {
			goddsAddress.setUserId(userId);
			if("1".equals(goddsAddress.getAddressState())) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("addressState", "1");
				map.put("userId", userId);
				GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
				if(null != goddsAddress1) {
					goddsAddress1.setAddressState("0");
					goddsAddressService.updateByPrimaryKey(goddsAddress1);
				}
			}else {
				goddsAddress.setAddressState("0");
			}
			//----------------------
			Random ra = new Random();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
			String pkId = sdf.format(new Date());
			for(int i = 0;i<2 ; i++) {
				pkId +=Integer.toString(ra.nextInt(10));
			}
			//----------------------
			goddsAddress.setPkId(pkId);
			goddsAddressService.insert(goddsAddress);
			goddsAddress.setAddTime(new Date());
			//operateLogService.addLog("知识库管理", "新增知识条目", Globals.Log_Type_INSERT);
		}else {
			/*String pkId = goddsAddress.getPkId();
			GoddsAddress goddsAddress2 = goddsAddressService.selectByPrimaryKey(pkId);
			if("1".equals(goddsAddress2.getAddressState())) {
				goddsAddress.setAddressState("1");
			}else {*/
				if("1".equals(goddsAddress.getAddressState())) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("addressState", "1");
					map.put("userId", userId);
					GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
					if(null != goddsAddress1) {
						goddsAddress1.setAddressState("0");
						goddsAddressService.updateByPrimaryKey(goddsAddress1);
					}
				}else {
					goddsAddress.setAddressState("0");
				}
			/*}*/
			goddsAddressService.updateByPrimaryKey(goddsAddress);
			//operateLogService.addLog("知识库管理", "修改知识条目", Globals.Log_Type_UPDATE);
		}
		return new ModelAndView("pages/order/orderCountAddress",context);	
	}
	/*
	 * 跳转到修改页面
	 */
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(HttpServletRequest request,HttpServletResponse response,GoddsAddress goddsAddress) {
		Map<String, Object> context = getRootMap();
		List<Province> list = new ArrayList<Province>();
		List<City> list1 = new ArrayList<City>();
		List<Area> list2 = new ArrayList<Area>();
		try {
			list = provinceService.selectAll();
			goddsAddress = goddsAddressService.selectByPrimaryKey(goddsAddress.getPkId());
			String father = goddsAddress.getProvince();
			list1 = cityService.selectByPrimaryKey(father);
			String areaid = goddsAddress.getArea();
			Area area = areaService.selectById(areaid);
			String cityid = goddsAddress.getCity();
			City city = cityService.selectById(cityid);
			list2 = areaService.selectByPrimaryKey(cityid);
			goddsAddress.setRemark1(area.getArea());
			goddsAddress.setRemark2(city.getCity());
			context.put("goddsAddress", goddsAddress);
			context.put("list", list);
			context.put("list1", list1);
			context.put("list2", list2);
			context.put("userId", goddsAddress.getUserId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/goddsAddress/goddsAddressAdd",context);
	}
	/*
	 * 修改默认地址
	 */
	@RequestMapping(params = "details=key")
	@ResponseBody
	public AjaxJson details(HttpServletRequest request,HttpServletResponse response,String pkId) {
		AjaxJson json = new AjaxJson();
		GoddsAddress goddsAddress = goddsAddressService.selectByPrimaryKey(pkId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("addressState", "1");
		map.put("userId", goddsAddress.getUserId());
		GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
		if(null != goddsAddress1) {
			goddsAddress1.setAddressState("0");
			goddsAddressService.updateByPrimaryKey(goddsAddress1);
		}
		goddsAddress.setAddressState("1");
		goddsAddressService.updateByPrimaryKey(goddsAddress);
		json.setObj("ok");
		return json;
	}
	/*
	 * 删除地址
	 */
	@RequestMapping(params="delete=key")
	@ResponseBody
	public AjaxJson delete(HttpServletRequest request,HttpServletResponse response,String pkId){
		AjaxJson json = new AjaxJson();
		try {
			goddsAddressService.deleteByPrimaryKey(pkId);
			//operateLogService.addLog("知识库管理", "删除知识条目", Globals.Log_Type_DEL);
			json.setObj("ok");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	//根据省份id查询所包含的市
	@RequestMapping(params = "getCityList=key")
	public void getCityList(HttpServletResponse response,HttpServletRequest request,Province province){
		List<City> list = new ArrayList<City>();
		String father = province.getProvinceid();
		try {
			list = cityService.selectByPrimaryKey(father);
			response.getWriter().println(JSONArray.fromObject(list).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//根据市级的id查询所包含的县
	@RequestMapping(params = "getAreaList=key")
	public void getAreaList(HttpServletResponse response,HttpServletRequest request,City city){
		List<Area> list = new ArrayList<Area>();
		String father = city.getCityid();
		try {
			list = areaService.selectByPrimaryKey(father);
			response.getWriter().println(JSONArray.fromObject(list).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

package com.ciitc.webServices.controller;

import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.Notice;
import com.ciitc.webServices.service.NoticeService;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

@Controller
@RequestMapping("/noticeController")
public class NoticeController extends BaseController {
	@Autowired(required = false)
	private NoticeService<Notice> noticeService;
	/*
	 * 跳转到公告显示页面
	 */
	@RequestMapping(params = "notice=key")
	public ModelAndView notice() {
		Map<String, Object> context = getRootMap();
		try {
			Notice notice = noticeService.selectAll();
			context.put("notice", notice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/News/noticeModify",context);
	}
	/*
	 * 保存或者修改公告
	 */
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, HttpServletResponse response,
			Notice notice){
		AjaxJson j = new AjaxJson();
		if(null == notice.getPkId() || "".equals(notice.getPkId())) {
			//生成一个uuid字符串
			String uuid = UUID.randomUUID().toString();
			notice.setPkId(uuid);
			noticeService.insertSelective(notice);
			j.setObj("ok");
			j.setMsg("添加成功");
		}else {
			noticeService.updateByPrimaryKeySelective(notice);
			j.setObj("ok");
			j.setMsg("修改成功");
		}
		
		return j;	
	}

}

package com.ciitc.webServices.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.model.Depart;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.RoleDepartInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.DepartService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.RoleDepartService;
import com.ciitc.webServices.service.UserInfoService;
import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;

/*******************************************************************************
 * 模块名称: MenuController 模块功能描述: 用户管理模块 创 建 人: 日 期: 修 改 人:
 ******************************************************************************/
@Controller
@RequestMapping("/departController")
public class DepartController extends BaseController {

	@Autowired(required = false)
	private DepartService<Depart> departService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	@Autowired(required = false)
	private RoleDepartService<RoleDepartInfo> roleDepartService;
	@Autowired(required = false)
	private UserInfoService<UserInfo> userInfoService;

	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "departList=key")
	public ModelAndView departList() {
		return new ModelAndView("pages/sysManager/depart/departList");
	}

	/*************************************************************************
	 * 函数名: menuTree 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "departTree=key")
	public void departTree(HttpServletRequest request,
			HttpServletResponse response, Depart depart) throws Exception {

		try {
			List<Map> list = new ArrayList<Map>();
			list = tree(depart);

			JSONObject jObject = pageUtil.getJsonForPage(list, list.size());
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Map> tree(Depart depart) throws Exception {

		try {
			// menu 列表
			List<Depart> list = new ArrayList<Depart>();

			if (depart.getPkId() != null
					&& !"0".equals(depart.getParentDepartId())) {
				try {
					list = departService.queryByParentId(depart.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					list = departService.query();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			List<Map> treeList = new ArrayList<Map>();

			List<Depart> childList = new ArrayList<Depart>();
			for (Depart dep : list) {
				Map<String, Object> node = new HashMap<String, Object>();
				node.put("pkId", dep.getPkId());
				node.put("departName", dep.getDepartName());
				node.put("departGrade", dep.getDepartGrade());
				if (dep.getParentDepartId() != null
						&& !Globals.Menu_Level_ONE.equals(dep.getParentDepartId())) {
					node.put("_parentId", dep.getParentDepartId());
				} else {
					node.put("_parentId", "");
				}

				// 判断节点是否展开(open,closed)
				try {
					childList = departService.queryByParentId(dep.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (childList != null && childList.size() > 0) {

					node.put("state", "closed");
				} else {
					node.put("state", "open");
				}
				treeList.add(node);
			}
			return treeList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 保存 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, Depart depart)
			throws Exception {
		try {
			AjaxJson j = new AjaxJson();
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(depart.getDepartName())||checkChar.checkChar(depart.getDepartGrade())||checkChar.checkChar(depart.getParentDepartId())){
				message = "存在不合法字符";
				j.setMsg(message);
				return j;
			}		
			Depart dep = new Depart();
			String operateFlag = request.getParameter("operateFlag");
			if ("modify".equals(operateFlag)) {
				//如果编辑时上级机构选择自己把上级id置空不更新
				if(depart.getPkId().equals(depart.getParentDepartId())){
					depart.setParentDepartId("");
					try {
						dep = departService.queryById(depart.getParentDepartId());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (dep != null && dep.getDepartGrade() != "0") {
						int lev = Integer.parseInt(dep.getDepartGrade()) + 1;
						depart.setDepartGrade(String.valueOf(lev));
					}
					getChildren(depart.getPkId(),depart.getDepartGrade());
					try {
						departService.updateBySelective(depart);
						operateLogService.addLog("组织机构管理", "更新组织机构日志", Globals.Log_Type_UPDATE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					try {
						dep = departService.queryById(depart.getParentDepartId());
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (dep != null && dep.getDepartGrade() != "0") {
						int lev = Integer.parseInt(dep.getDepartGrade()) + 1;
						depart.setDepartGrade(String.valueOf(lev));
					}else{
						depart.setParentDepartId(Globals.Menu_Level_ONE);
						depart.setDepartGrade("0");
					}
					getChildren(depart.getPkId(),depart.getDepartGrade());
					try {
						departService.updateBySelective(depart);
						operateLogService.addLog("组织机构管理", "更新组织机构日志", Globals.Log_Type_UPDATE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			} else {
				try {
					dep = departService.queryById(depart.getParentDepartId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				if (dep != null && dep.getDepartGrade() != "0") {
					int lev = Integer.parseInt(dep.getDepartGrade()) + 1;
					depart.setDepartGrade(String.valueOf(lev));
				}else{
					depart.setParentDepartId(Globals.Menu_Level_ONE);
					depart.setDepartGrade("0");
				}
				try {
					departService.add(depart);
					operateLogService.addLog("组织机构管理", "保存组织机构日志", Globals.Log_Type_INSERT);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			message = "保存成功";
			j.setMsg(message);
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/*************************************************************************
	 * 函数名: getChildren 功能描述: 对子菜单级别修改  输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	public List<Depart> getChildren(String id,String level) throws Exception{
		try {
			List<Depart> departs=new ArrayList<Depart>();
			List<Depart> list = new ArrayList<Depart>();;
			try {
				list = departService.queryByParentId(id);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(Depart depart:list){
				depart.setDepartGrade(String.valueOf((Integer.parseInt(level)+1)));
				try {
					departService.updateBySelective(depart);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				departs.addAll(getChildren(depart.getPkId(),depart.getDepartGrade()));
			}
			departs.addAll(list);
			return departs;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/*************************************************************************
	 * 函数名: delete 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String pkId) {

		try {
			AjaxJson json = new AjaxJson();
			//如果用户中有机构这条记录则不被删除
			List<UserInfo> list = new ArrayList<UserInfo>();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("departPkId", pkId);
			try {
				list = userInfoService.queryByDepartPkId(paramMap);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(list.size()>0){
				json.setMsg("该机构已被用户绑定不可删除");
				json.setObj("alert");
			}else {
				// 日志用
				message = "组织机构删除成功";
				
				// 删除节点是否为父节点
				List<Depart> departs = null;

				// 删除权限时先删除权限与角色之间关联表信息
				List<RoleDepartInfo> roleDeparts = null;
				try {
					departs = departService.queryByParentId(pkId);
					roleDeparts = roleDepartService.queryByDepartId(pkId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				
				if(departs != null && departs.size() > 0){
					json.setMsg("含有子节点无法删除");
					json.setObj("alert");
				} else {
					
					if (departs != null && roleDeparts.size() > 0) {
						json.setMsg("组织机构已分配无法删除");
						json.setObj("alert");
					} else {
						try {
							departService.delete(pkId);
							operateLogService.addLog("组织机构管理", "删除组织机构日志", Globals.Log_Type_DEL);
							json.setObj("ok");
						} catch (Exception e) {
							e.printStackTrace();
						}
						// TODO 系统日志追加
					}
				}
			}
			
			
			return json;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	public List<Map> combotree(String parentId) throws Exception {

		try {
			// combotree
			List<Depart> list = new ArrayList<Depart>();

			try {
				list = departService.queryByParentId(parentId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			List<Map> treeList = new ArrayList<Map>();
			for (Depart dep : list) {
				Map<String, Object> node = new HashMap<String, Object>();
				if(dep.getParentDepartId().equals("0")){
					node.put("id", dep.getPkId());
					node.put("text", dep.getDepartName());
					childTree(node);
					treeList.add(node);
				}
			}
			return treeList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private void childTree(Map<String, Object> node) {
		try {
			List<Depart> childList = new ArrayList<Depart>();
			List<Map> tree = new ArrayList<Map>();
			try {
				childList = departService.queryByParentId(node.get("id"));
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (childList.size() > 0) {

				for (Depart dep : childList) {
					Map<String, Object> childNode = new HashMap<String, Object>();
					childNode.put("id", dep.getPkId());
					childNode.put("text", dep.getDepartName());
					tree.add(childNode);
				}
				node.put("children", tree);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "getCombotree=key")
	public void getCombotree(HttpServletRequest request,
			HttpServletResponse response, Depart depart) throws Exception {

		try {
			List<Map> list = new ArrayList<Map>();
			list = combotree(depart.getPkId());

			JSONArray jObject = JSONArray.fromObject(list);
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			try {
				PrintWriter pw = response.getWriter();
				pw.write(jObject.toString());
				pw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(Depart depart, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			context.put("combotree_pkId", depart.getPkId());
			if (depart.getPkId() != null && !"".equals(depart.getPkId())) {
				context.put("departLevel", "1");
			} else {
				context.put("departLevel", "0");
			}
			context.put("operateFlag", "add");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/depart/depart", context);
	}

	/*************************************************************************
	 * 函数名: modify 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(Depart depart, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			depart = departService.queryById(depart.getPkId());
			context.put("depart", depart);
			context.put("combotree_pkId", depart.getParentDepartId());
			context.put("operateFlag", "modify");
			if (depart.getParentDepartId() != null && !"".equals(depart.getParentDepartId())) {
				context.put("departLevel", "1");
			} else {
				context.put("departLevel", "0");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/depart/depart", context);
	}

	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.service.OperateLogSelectService;
import com.ciitc.webServices.util.pageUtil;


@Controller
@RequestMapping("/operateLogSelectController")
public class OperateLogSelectController extends BaseController{
	
	@Autowired(required = false)
	private OperateLogSelectService<OperateLog> operateLogSelectService;
	

	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "operateLogList=key")
	public ModelAndView operateLogList() {
		return new ModelAndView("pages/sysManager/log/logList");
	}
	
	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request,
			HttpServletResponse response, OperateLog operateLog,String startTime,String endTime,BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
//		if(!"".equals(operateLog.getUserName())||!"".equals(startTime)||!"".equals(endTime)||!"".equals(operateLog.getOperateType())||!"".equals(operateLog.getOperateTrack())){
//			basePage.setPage(1);
//		}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("operateLog", operateLog);
			paramMap.put("startTime", startTime);
			paramMap.put("endTime", endTime);
			
			List<OperateLog> list = new ArrayList<OperateLog>();
			try {
				list = operateLogSelectService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject =pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.Collection;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.KnowledgeBase;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.CollectionService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.KnowledgeBaseService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

/**
 * 知识库管理模块
 * @author tdl
 *
 */
@Controller
@RequestMapping("/knowledgeBaseController")
public class KnowledgeBaseController extends BaseController{
	@Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;
	@Autowired(required = false)
	private KnowledgeBaseService<KnowledgeBase> knowledgeBaseService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	
	@Autowired(required = false)
	private CollectionService<Collection> collectionService; //收藏
	/*
	 * 跳转到新增页面
	 */
	@RequestMapping(params = "knowledgeBaseAdd=key")
	public ModelAndView knowledgeBaseAdd() {
		return new ModelAndView("pages/knowledgeBase/knowledgeBaseAdd");
	}
	
	/*
	 * 跳转到列表显示页面
	 */
	@RequestMapping(params = "knowledgeBaseList=key")
	public ModelAndView knowledgeBaseList() {
		return new ModelAndView("pages/knowledgeBase/knowledgeBaseList");
	}
	/*
	 * 知识库列表查询显示
	 */
	@RequestMapping(params = "getKnowledgeBaseList=key")
	public void getKnowledgeBaseList(HttpServletResponse response,HttpServletRequest request,
			BasePage basePage,KnowledgeBase knowledgeBase) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		paramMap.put("knowledgeBase", knowledgeBase);
		List<KnowledgeBase> list = new ArrayList<KnowledgeBase>();
		try {
			list = knowledgeBaseService.queryByList(paramMap);
			//根据数据字典查询状态
			List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("releaseType");
			EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
			EsbmmCode codePkId = new EsbmmCode();
			codePkId.setCodeTypePkId(codeTypePkId.getPkId());
			//根据codeTypePkId查找对应的类型
			List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
			for (KnowledgeBase knowledge : list) {
				for (EsbmmCode esbmmCode : codeList) {
					if(knowledge.getStatus().equals(esbmmCode.getCodeCode())){
						knowledge.setStatus(esbmmCode.getChineseName());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list,cnt);
		pageUtil.responseDatagrid(response, jObject);
	}
	/*
	 * 检查标题是否已经存在
	 */
	@RequestMapping(params="checkCode=key")
	@ResponseBody
	public AjaxJson checkCode(HttpServletRequest request,HttpServletResponse response, KnowledgeBase knowledgeBase,String pkId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		AjaxJson json = new AjaxJson();
		paramMap.put("title", knowledgeBase.getTitle());
		//新增时检查标题是否已经存在
		if(null == pkId || "".equals(pkId)) {
			try {
				List<KnowledgeBase> list = knowledgeBaseService.queryByTitle(paramMap);
				if(list.size()==0){
					json.setObj("ok");
				}else{
					json.setObj("no");
					json.setMsg("标题已存在");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			//修改时检查标题是否已经存在
			List<KnowledgeBase> list = knowledgeBaseService.queryByTitle(paramMap);
			if(list.size()==0) {
				json.setObj("ok");
			}else {
				for (KnowledgeBase knowledgeBase2 : list) {
					if(knowledgeBase2.getPkId().equals(pkId)) {
						json.setObj("ok");
					}else {
						json.setObj("no");
						json.setMsg("标题已存在");
					}
				}
			}
			
		}
		
		return json;
	}
	/*
	 * 保存新增知识条目
	 */
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, HttpServletResponse response,
			String title,String content,String status,String pkId,String keyWord){
		AjaxJson j = new AjaxJson();
		//获取当前登录对象
		UserInfo user = ResourceUtil.getSessionUserName();
		//生成一个uuid字符串
		String uuid = UUID.randomUUID().toString();
		try {
			//检查字符是否合法
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(title) || checkChar.checkChar(content) ||checkChar.checkChar(status)){
				j.setMsg("存在不合法字符");
				return j;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//判断新增还是修改
		if(null == pkId || "".equals(pkId)) {
			KnowledgeBase knowledgeBase = new KnowledgeBase();
			knowledgeBase.setTitle(title);
			knowledgeBase.setContent(content);
			knowledgeBase.setStatus(status);
			knowledgeBase.setKeyWord(keyWord);
			knowledgeBase.setPublisher(user.getUserName());
			knowledgeBase.setPkId(uuid);
			knowledgeBase.setCreatedDate(new Date());
			if("01".equals(status)) {
				knowledgeBase.setReleaseDate(new Date());
			}
			knowledgeBaseService.insertSelective(knowledgeBase);
			operateLogService.addLog("知识库管理", "新增知识条目", Globals.Log_Type_INSERT);
			j.setObj("ok");
			j.setMsg("保存成功");
		}else {
			KnowledgeBase knowledgeBase = new KnowledgeBase();
			knowledgeBase.setPkId(pkId);
			knowledgeBase.setTitle(title);
			knowledgeBase.setContent(content);
			knowledgeBase.setStatus(status);
			knowledgeBase.setKeyWord(keyWord);
			knowledgeBase.setUpdatedDate(new Date());
			if("01".equals(status)) {
				knowledgeBase.setReleaseDate(new Date());
			}
			knowledgeBaseService.updateByPrimaryKeySelective(knowledgeBase);
			operateLogService.addLog("知识库管理", "修改知识条目", Globals.Log_Type_UPDATE);
			j.setObj("ok");
			j.setMsg("修改成功");
		}
		
		return j;	
	}
	/*
	 * 跳转到修改页面
	 */
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(HttpServletRequest request,HttpServletResponse response,KnowledgeBase knowledgeBase) {
		Map<String, Object> context = getRootMap();
		try {
			knowledgeBase = knowledgeBaseService.selectByPrimaryKey(knowledgeBase.getPkId());
			context.put("knowledgeBase", knowledgeBase);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/knowledgeBase/knowledgeBaseAdd",context);
	}
	/*
	 * 跳转到详情页面details
	 */
	@RequestMapping(params = "details=key")
	public ModelAndView details(HttpServletRequest request,HttpServletResponse response,KnowledgeBase knowledgeBase) {
		Map<String, Object> context = getRootMap();
		try {
			knowledgeBase = knowledgeBaseService.selectByPrimaryKey(knowledgeBase.getPkId());
			//根据数据字典查询状态
			List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("releaseType");
			EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
			EsbmmCode codePkId = new EsbmmCode();
			codePkId.setCodeTypePkId(codeTypePkId.getPkId());
			//根据codeTypePkId查找对应的类型
			List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
			for (EsbmmCode esbmmCode : codeList) {
				if(knowledgeBase.getStatus().equals(esbmmCode.getCodeCode())){
					knowledgeBase.setStatus(esbmmCode.getChineseName());
				}
				
			}
			//时间格式转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			context.put("createdDate", sdf.format(knowledgeBase.getCreatedDate()));
			context.put("knowledgeBase", knowledgeBase);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/knowledgeBase/knowledgeBaseDel",context);
	}
	/*
	 * 删除知识条目
	 */
	@RequestMapping(params="delete=key")
	@ResponseBody
	public AjaxJson delete(HttpServletRequest request,HttpServletResponse response,String pkId){
		AjaxJson json = new AjaxJson();
		try {
			knowledgeBaseService.deleteByPrimaryKey(pkId);
			//删除收藏列表里的这条数据
			collectionService.deleteByCid(pkId);
			operateLogService.addLog("知识库管理", "删除知识条目", Globals.Log_Type_DEL);
			json.setObj("ok");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	/*
	 * 取消发布releaseCel
	 */
	@RequestMapping(params="releaseCel=key")
	@ResponseBody
	public AjaxJson releaseCel(HttpServletRequest request,HttpServletResponse response,String pkId){
		AjaxJson json = new AjaxJson();
		try {
			KnowledgeBase knowledgeBase = new KnowledgeBase();
			knowledgeBase.setStatus("00");
			knowledgeBase.setPkId(pkId);
			knowledgeBaseService.updateByPrimaryKeySelective(knowledgeBase);
			operateLogService.addLog("知识库管理", "取消发布知识条目", Globals.Log_Type_UPDATE);
			json.setObj("ok");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
}

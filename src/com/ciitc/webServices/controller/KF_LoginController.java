package com.ciitc.webServices.controller;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.service.AppUserInforService;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

@Controller
@RequestMapping("/kfLoginController.do")
public class KF_LoginController  extends BaseController{
	@Autowired(required = false)
	private AppUserInforService<AppUserInfor> appUserInforService;
	
	/* 
	 * 登陆接口
	 */
	@RequestMapping(value = "login",method=RequestMethod.GET)
	public @ResponseBody String Login(HttpServletRequest request) {
		String state = "";
		AppUserInfor appUserInfor = null;
		String phoneNumber = request.getParameter("phoneNumber");
		String verifiCode = request.getParameter("verifiCode");
		try {
			appUserInfor = appUserInforService.queryByPhone(phoneNumber);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		if(null != appUserInfor&&null != appUserInfor.getRemark1()){
			if(verifiCode.equals(appUserInfor.getRemark1())){
				state = "00";
			}else{
				state = "01";
			}
			appUserInfor.setRemark1("");
			try {
				appUserInforService.update1(appUserInfor);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			state = "01";
		}
		return state;
	}
}

package com.ciitc.webServices.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;






import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppGoodsInfor;
import com.ciitc.webServices.model.AppGoodsInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.AppGoodsInforService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年3月8日 下午3:46:18
 * 修 改 人: 
 ******************************************************************************/
@Controller
@RequestMapping("appGoodsInforController")
public class AppGoodsInforController extends BaseController{

	@Autowired(required = false)
	private AppGoodsInforService<AppGoodsInfor> appGoodsInforService;
	@Autowired(required = false)
	private  EsbmmCodeService<EsbmmCode> esbmmCodeService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	
	
	@RequestMapping(params = "goodsinfoList=key")
	public ModelAndView goodsinfoList() {
		return new ModelAndView("pages/mall/goodsinfoList");
	}
	
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request,
			HttpServletResponse response, AppGoodsInfor appGoodsInfor, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("appGoodsInfor", appGoodsInfor);
			List<AppGoodsInfor> list = new ArrayList<AppGoodsInfor>();
			try {
				list = appGoodsInforService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	@RequestMapping(params="changeSurplus=key")
	public ModelAndView changeSurplus(String pkId){
		Map<String, Object> context = getRootMap();
		AppGoodsInfor appGoodsInfor;
		try {
			appGoodsInfor = appGoodsInforService.queryById(pkId);
			context.put("appGoodsInfor", appGoodsInfor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/mall/changeSurplus",context);
	}
	
	
	@RequestMapping(params="change=key")
	@ResponseBody
	public AjaxJson change(AppGoodsInfor appGoodsInfor) throws Exception{
		AjaxJson json=new AjaxJson();
		if(appGoodsInfor.getPkId()!=null && !"".equals(appGoodsInfor.getPkId())) {
			AppGoodsInfor appGoodsInfor1 = appGoodsInforService.queryById(appGoodsInfor.getPkId());
			appGoodsInfor1.setSurplus(appGoodsInfor.getSurplus());
			appGoodsInforService.update(appGoodsInfor1);
			message="保存成功";
		}
		json.setMsg(message);
		return json;
	}
	
	@RequestMapping(params="add=key")
	public ModelAndView add(HttpServletRequest req){
		return new ModelAndView("pages/mall/goodsinfoAdd");
	}
	
    @RequestMapping(params="uploadPhoto=key")  
    public void uploadImage(HttpServletRequest request,HttpServletResponse response, HttpSession session,  
            @RequestParam(value = "file", required = false) MultipartFile file)  
            throws IllegalStateException, IOException {  
    	try { 
         String fileName = file.getOriginalFilename();//原始文件名
         //System.out.println("file name: " + fileName);
         String name = UUID.randomUUID().toString();
         String extension = FilenameUtils.getExtension(fileName);//图片拓展名
         // 图片上传本地路径  
         List<EsbmmCode> code1 = esbmmCodeService.getUploadAddress();
         String uploadAddress = code1.get(0).getCodeValue();
         String pic_path = uploadAddress+name+"."+extension; 
         List<EsbmmCode> code = esbmmCodeService.getAddress();
		 String addre = code.get(0).getCodeValue();
		 String path1 = "/photo/" + name + "." + extension;
		 File dir = new File(pic_path);  
         if(!dir.exists()) {  
         dir.mkdirs();  
         }  
         // 上传图片  
         file.transferTo(new File(pic_path));
         JSONObject jsonObject = new JSONObject(); 
         // 将虚拟路径保存并返回页面
         jsonObject.put("path", addre+path1);
         jsonObject.put("path1", path1);  
         // 设置响应数据的类型json  
         response.setContentType("application/json; charset=utf-8");  
         response.getWriter().write(jsonObject.toString()); 
    	}catch (Exception e) {  
            throw new RuntimeException("服务器繁忙，上传图片失败");  
        }   
    }
    
    @RequestMapping(params="uploadPhotos=key")  
    @ResponseBody  
    private AjaxJson fildUpload(@RequestParam(value="file1",required=false) MultipartFile[] file1, HttpServletRequest request)throws Exception{  
    	AjaxJson j = new AjaxJson();
    	//基本表单  
        String path="";  
        List<EsbmmCode> code = esbmmCodeService.getAddress();
		String addre = code.get(0).getCodeValue();
        List<String> listImagePath=new ArrayList<String>();  
        String imagesUrl1 = null;
        for (MultipartFile mf : file1) {  
            if(!mf.isEmpty()){  
                //生成uuid作为文件名称 
            	String id = UUID.randomUUID().toString().replaceAll("-","");
                //获得文件类型（可以判断如果不是图片，禁止上传）  
                String contentType=mf.getContentType();  
                //获得文件后缀名称  
                String imageName=contentType.substring(contentType.indexOf("/")+1);  
                String filename = id+"."+imageName;
                // 图片上传本地路径  
                List<EsbmmCode> code1 = esbmmCodeService.getUploadAddress();
                String uploadAddress = code1.get(0).getCodeValue();
                //path="D:\\picture\\"+id+"."+imageName;
                path = uploadAddress+id+"."+imageName;
                String path1 = "/photo/" +filename;
                mf.transferTo(new File(path)); 
                listImagePath.add(addre+path1);
                if(null == imagesUrl1) {
                	imagesUrl1=path1+",";
                }else {
                	imagesUrl1+=path1+",";
                }
            }  
        }  
       // System.out.println(listImagePath);  
        j.setObj(listImagePath);
        j.setMsg(imagesUrl1);
        return j;  
    } 
    
    @RequestMapping(params="modify=key")
	public ModelAndView repairInforModify(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		AppGoodsInfor appGoodsInfor=appGoodsInforService.queryById(pkId);
		int pri =appGoodsInfor.getPrice().intValue();
		Integer price = Integer.valueOf(pri);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String createDate=sdf.format(appGoodsInfor.getCreateDate());
		context.put("createDate", createDate);
		List<EsbmmCode> code = esbmmCodeService.getAddress();
		String addre = code.get(0).getCodeValue();
		String mainURL = addre+appGoodsInfor.getMainImageUrl();
		appGoodsInfor.setMainUrl(mainURL);
		context.put("appGoodsInfor", appGoodsInfor);
		context.put("price", price);
		context.put("addre", addre);
		
		return new ModelAndView("pages/mall/goodsinfoAdd",context);
	}
	
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(AppGoodsInfor appGoodsInfor) throws Exception{
		AjaxJson json=new AjaxJson();
		UserInfo user = ResourceUtil.getSessionUserName();
		if(appGoodsInfor.getPkId()!=null && !"".equals(appGoodsInfor.getPkId())){
			try {
				appGoodsInfor.setShelves("下架");
				appGoodsInfor.setOperatorId(user.getUserName());
				appGoodsInforService.update(appGoodsInfor);
			} catch (Exception e) {
				e.printStackTrace();
			}
			message = "编辑成功";
		}else{
			String uuid = UUID.randomUUID().toString();
			appGoodsInfor.setPkId(uuid);
			appGoodsInfor.setShelves("下架");
			String goodsId = String.valueOf((int)((Math.random()*9+1)*10000));
			appGoodsInfor.setGoodsId(goodsId);
			appGoodsInfor.setOperatorId(user.getUserName());
			appGoodsInforService.add(appGoodsInfor);
			message="保存成功";
		}
		json.setMsg(message);
		return json;
	}
	
	@RequestMapping(params="upGoods=key")
	@ResponseBody
	public AjaxJson upGoods(AppGoodsInfor appGoodsInfor) throws Exception{
		AjaxJson json=new AjaxJson();
		UserInfo user = ResourceUtil.getSessionUserName();
		if(appGoodsInfor.getPkId()!=null && !"".equals(appGoodsInfor.getPkId())){
			appGoodsInfor.setShelves("上架");
			appGoodsInfor.setOperatorId(user.getUserName());
			appGoodsInforService.update(appGoodsInfor);
		}else{
			String uuid = UUID.randomUUID().toString();
			appGoodsInfor.setPkId(uuid);
			appGoodsInfor.setShelves("上架");
			String goodsId = String.valueOf((int)((Math.random()*9+1)*10000));
			appGoodsInfor.setGoodsId(goodsId);
			appGoodsInfor.setOperatorId(user.getUserName());
			appGoodsInforService.add(appGoodsInfor);
			message="上架成功";
		}
		json.setMsg(message);
		return json;
	}
	
	@RequestMapping(params="upDown=key")
	@ResponseBody
	public AjaxJson upDown(String pkId) throws Exception{
		AjaxJson json=new AjaxJson();
		AppGoodsInfor appGoodsInfor = appGoodsInforService.queryById(pkId);
		if(appGoodsInfor.getShelves().equals("上架")){
			appGoodsInfor.setShelves("下架");
			appGoodsInforService.update(appGoodsInfor);
			json.setObj("ok");
			message="下架成功";
		}else if (appGoodsInfor.getShelves().equals("下架")){
			appGoodsInfor.setShelves("上架");
			appGoodsInforService.update(appGoodsInfor);
			json.setObj("ok");
			message="上架成功";
		}
		//json.setObj(message);
		return json;
		
	}
	
	@RequestMapping(params="delete=key")
	@ResponseBody
	public AjaxJson delete(AppGoodsInfor appGoodsInfor) throws Exception{
		AjaxJson json=new AjaxJson();
		if(appGoodsInfor.getPkId()!=null && !"".equals(appGoodsInfor.getPkId())){
			appGoodsInforService.delete(appGoodsInfor.getPkId());
		} 
		message="删除成功";
		json.setObj(message);
		return json;
		
	}
	
	@RequestMapping(params="specific=key")
	public ModelAndView repairInforSpecific(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		AppGoodsInfor appGoodsInfor=appGoodsInforService.queryById(pkId);
		int pri =appGoodsInfor.getPrice().intValue();
		Integer price = Integer.valueOf(pri);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String createDate=sdf.format(appGoodsInfor.getCreateDate());
		context.put("createDate", createDate);
		List<EsbmmCode> code = esbmmCodeService.getAddress();
		String addre = code.get(0).getCodeValue();
		String mainURL = addre+appGoodsInfor.getMainImageUrl();
		context.put("appGoodsInfor", appGoodsInfor);
		appGoodsInfor.setMainUrl(mainURL);
		context.put("price", price);
		context.put("addre", addre);
		return new ModelAndView("pages/mall/goodsinfoSpecific",context);
	}
	
    
 // 向页面返回的信息
 		private String message;

 		public String getMessage() {
 			return message;
 		}

 		public void setMessage(String message) {
 			this.message = message;
 		}
    
}

package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.PerformanceGoals;
import com.ciitc.webServices.model.Profit;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.PerformanceGoalsService;
import com.ciitc.webServices.service.ProfitService;
import com.ciitc.webServices.util.pageUtil;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

/*******************************************************************************
 * 模块名称: performanceGoalsController 地址功能描述: 我的收益模块 创 建 人: 日 期: 修 改 人:任之阳
 ******************************************************************************/
@Controller
@RequestMapping("/performanceGoalsController")
public class PerformanceGoalsController extends BaseController {


	@Autowired(required = false)
	private PerformanceGoalsService<PerformanceGoals> performanceGoalsService;
	@Autowired(required = false)
	private EsbmmCodeService<?> EsbmmCodeService;
 
	/**
	 * 翻译数据字典
	 */

	public List<PerformanceGoals> translate(List<PerformanceGoals> list) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		EsbmmCode code = new EsbmmCode();
		for (PerformanceGoals s : list) {
			if (s != null && !s.equals("")) {
				//是否到期
				paramMap.put("typeName", "rank");
				paramMap.put("codeValue", s.getRank());
				code = EsbmmCodeService.getCodeName(paramMap);
				s.setRank(code.getChineseName());
			}
		}
		return list;
	}
	
	
	
	
	
	@RequestMapping(params ="performanceGoalsList=key")
	public ModelAndView profitList() {
		return new ModelAndView("pages/sysManager/performanceGoals/performanceGoalsList");
	}
	
	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, BasePage basePage,PerformanceGoals performanceGoals) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("performanceGoals", performanceGoals);
			List<PerformanceGoals> list = new ArrayList<PerformanceGoals>();
			try {
				list = performanceGoalsService.queryByList(paramMap);
				list = translate(list);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
}

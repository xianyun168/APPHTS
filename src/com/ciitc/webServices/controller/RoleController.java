package com.ciitc.webServices.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.entity.Tree;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.model.Depart;
import com.ciitc.webServices.model.MenuInfo;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.RoleDepartInfo;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.RoleInfo;
import com.ciitc.webServices.model.SysPermissionInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.model.ZTree;
import com.ciitc.webServices.service.DepartService;
import com.ciitc.webServices.service.MenuService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.RoleDepartService;
import com.ciitc.webServices.service.RoleDetailService;
import com.ciitc.webServices.service.RoleService;
import com.ciitc.webServices.service.SysPermissionInfoService;
import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;

/*******************************************************************************
 * 模块名称: RoleController 模块功能描述: 用户管理模块 创 建 人: 日 期: 修 改 人:
 ******************************************************************************/
@Controller
@RequestMapping("/roleController")
public class RoleController extends BaseController {

	@Autowired(required = false)
	private RoleService<RoleInfo> roleService;
	@Autowired(required = false)
	private RoleDetailService<RoleDetailInfo> roleDetailService;
	@Autowired(required = false)
	private RoleDepartService<RoleDepartInfo> roleDepartService;
	@Autowired(required = false)
	private MenuService<MenuInfo> menuService;
	@Autowired(required = false)
	private DepartService<Depart> departService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	@Autowired(required = false)
	private SysPermissionInfoService<SysPermissionInfo> sysPermissionInfoService;

	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 保存 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, RoleInfo roleInfo)
			throws Exception {
		try {
			AjaxJson j = new AjaxJson();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(roleInfo.getRoleName()) || 
					checkChar.checkChar(roleInfo.getDescription())){
				message = "存在不合法字符";
				j.setMsg(message);
				return j;
			}
			
			if (roleInfo.getPkId() != null && !"".equals(roleInfo.getPkId())) {
				paramMap.put("roleName", roleInfo.getRoleName());
					roleInfo.setDescription(roleInfo.getDescription());
					try {
						roleService.updateBySelective(roleInfo);
						operateLogService.addLog("角色管理", "更新角色日志", Globals.Log_Type_UPDATE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					message = "修改成功";
			} else {
				paramMap.put("roleName", roleInfo.getRoleName());
				List<RoleInfo> list = new ArrayList<RoleInfo>();
				try {
					list = roleService.queryByName(paramMap);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (list.size() == 0) {
					try {
						roleService.add(roleInfo);
						operateLogService.addLog("角色管理", "保存角色日志",Globals.Log_Type_INSERT);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					message = "保存成功";
				} else {
					message = "角色名已存在";
				}
			}
			j.setMsg(message);
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request,
			HttpServletResponse response, RoleInfo roleInfo, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("roleInfo", roleInfo);
			List<RoleInfo> list = new ArrayList<RoleInfo>();
			try {
				list = roleService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*************************************************************************
	 * 函数名: delete 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String pkId) {

		try {
			AjaxJson json = new AjaxJson();
			List<SysPermissionInfo> list = new ArrayList<SysPermissionInfo>();
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("roleId", pkId);
			try {
				list = sysPermissionInfoService.queryRoleUserByRoleId(paramMap);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if(list.size()>0){
				json.setMsg("该角色已被用户绑定不可删除");
				json.setObj("alert");
			}else {
				// 默认数据卡控
				int flag = 0;
				try {
					flag = roleService.checkRole(pkId);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (flag == 1) {
					json.setMsg("该角色为管理员不可删除");
					json.setObj("alert");
				} else {
					// 删除角色之前先删除角色权限关系
					try {
						roleDetailService.deleteByRoleId(pkId);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					try {
						roleService.delete(pkId);
						operateLogService.addLog("角色管理", "删除角色日志", Globals.Log_Type_DEL);
						json.setObj("ok");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			return json;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 更新权限
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "updateAuthority=key")
	@ResponseBody
	public AjaxJson updateAuthority(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			String roleId = request.getParameter("roleId");
			String roleMenu = request.getParameter("rolefunctions");
			List<RoleDetailInfo> menuOfRoleList = roleDetailService
					.queryByRoleId(roleId);

			Map<String, RoleDetailInfo> map = new HashMap<String, RoleDetailInfo>();

			for (RoleDetailInfo menuOfRole : menuOfRoleList) {
				map.put(menuOfRole.getMenuPkId(), menuOfRole);
			}
			String[] roleMenus = roleMenu.split(",");
			Set<String> set = new HashSet<String>();
			for (String s : roleMenus) {
				set.add(s);
			}
			updateCompare(roleId, set, map);
			operateLogService.addLog("角色管理模块", "更新权限日志", Globals.Log_Type_UPDATE);
			j.setMsg("权限更新成功");
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg("权限更新失败");
		}
		return j;
	}

	/**
	 * 更新组织机构权限
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "updateDepartAuthority=key")
	@ResponseBody
	public AjaxJson updateDepartAuthority(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			String roleId = request.getParameter("roleId");
			String roleDepart = request.getParameter("rolefunctions");
			List<RoleDepartInfo> departOfRoleList = roleDepartService
					.queryByRoleId(roleId);

			Map<String, RoleDepartInfo> map = new HashMap<String, RoleDepartInfo>();

			for (RoleDepartInfo departOfRole : departOfRoleList) {
				map.put(departOfRole.getDepartPkId(), departOfRole);
			}
			String[] roleDeparts = roleDepart.split(",");
			Set<String> set = new HashSet<String>();
			for (String s : roleDeparts) {
				set.add(s);
			}
			updateDepartCompare(roleId, set, map);
			operateLogService.addLog("角色管理模块", "更新权限日志", Globals.Log_Type_OTHER);
			j.setMsg("权限更新成功");
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg("权限更新失败");
		}
		return j;
	}
	/**
	 * 权限比较
	 * 
	 * @param set
	 *            最新的权限列表
	 * @param role
	 *            当前角色
	 * @param map
	 *            旧的权限列表
	 * @param entitys
	 *            最后保存的权限列表
	 */
//	private void updateCompare(String roleId, Set<String> set,
//			Map<String, RoleDetailInfo> map) {
//		try {
//			List<RoleDetailInfo> entitys = new ArrayList<RoleDetailInfo>();
//			List<RoleDetailInfo> deleteEntitys = new ArrayList<RoleDetailInfo>();
//			for (String menuId : set) {
//				if (map.containsKey(menuId)) {
//					map.remove(menuId);
//				} else {
//					RoleDetailInfo rd = new RoleDetailInfo();
//					rd.setRolePkId(roleId);
//					rd.setMenuPkId(menuId);
//					UserInfo user = ResourceUtil.getSessionUserName();
//					List<RoleDetailInfo> roleDetailList = new ArrayList<RoleDetailInfo>();
//					try {
//						roleDetailList=roleDetailService.getDescriptionByMenu(user.getUserName(),menuId);
//					} catch (Exception e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
//					if(roleDetailList==null){
//						entitys.add(rd);
//					}else{
//						if(roleDetailList.size()>0){
//							rd.setRemark(roleDetailList.get(0).getRemark());
//							entitys.add(rd);
//						}
//						
//					}
//					
//				}
//			}
//			Collection<RoleDetailInfo> collection = map.values();
//			Iterator<RoleDetailInfo> it = collection.iterator();
//			for (; it.hasNext();) {
//				deleteEntitys.add(it.next());
//			}
//			// / TODO 批量操作表的事务控制
//			for (RoleDetailInfo roleMenu : deleteEntitys) {
//				try {
//					roleDetailService.delete(roleMenu.getPkId());
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//			for (RoleDetailInfo roleMenu : entitys) {
//				try {
//					roleDetailService.add(roleMenu);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
	
	private void updateCompare(String roleId, Set<String> set,
			Map<String, RoleDetailInfo> map) {
		List<RoleDetailInfo> entitys = new ArrayList<RoleDetailInfo>();
		List<RoleDetailInfo> deleteEntitys = new ArrayList<RoleDetailInfo>();
		for (String menuId : set) {
			if (map.containsKey(menuId)) {
				map.remove(menuId);
			} else {
				RoleDetailInfo rd = new RoleDetailInfo();
				rd.setRolePkId(roleId);
				rd.setMenuPkId(menuId);
				entitys.add(rd);
			}
		}
		Collection<RoleDetailInfo> collection = map.values();
		Iterator<RoleDetailInfo> it = collection.iterator();
		for (; it.hasNext();) {
			deleteEntitys.add(it.next());
		}
		// / TODO 批量操作表的事务控制
		for (RoleDetailInfo roleMenu : deleteEntitys) {
			try {
				roleDetailService.delete(roleMenu.getPkId());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		for (RoleDetailInfo roleMenu : entitys) {
			try {
				roleDetailService.add(roleMenu);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 组织机构权限比较
	 * 
	 * @param set
	 *            最新的权限列表
	 * @param role
	 *            当前角色
	 * @param map
	 *            旧的权限列表
	 * @param entitys
	 *            最后保存的权限列表
	 */
	private void updateDepartCompare(String roleId, Set<String> set,
			Map<String, RoleDepartInfo> map) {
		try {
			List<RoleDepartInfo> entitys = new ArrayList<RoleDepartInfo>();
			List<RoleDepartInfo> deleteEntitys = new ArrayList<RoleDepartInfo>();
			for (String departId : set) {
				if (map.containsKey(departId)) {
					map.remove(departId);
				} else {
					RoleDepartInfo rd = new RoleDepartInfo();
					rd.setRolePkId(roleId);
					rd.setDepartPkId(departId);
					entitys.add(rd);
				}
			}
			Collection<RoleDepartInfo> collection = map.values();
			Iterator<RoleDepartInfo> it = collection.iterator();
			for (; it.hasNext();) {
				deleteEntitys.add(it.next());
			}

			// / TODO 批量操作表的事务控制
			for (RoleDepartInfo roleDepart : deleteEntitys) {
				try {
					roleDepartService.delete(roleDepart.getPkId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			for (RoleDepartInfo roleDepart : entitys) {
				try {
					roleDepartService.add(roleDepart);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 设置权限
	 * 
	 * @param role
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 * @return
	 */
	@RequestMapping(params = "setAuthority=key")
	public void setAuthority(HttpServletRequest request,
			HttpServletResponse response, Tree tree) throws Exception {
		// 取得roleId
		try {
			String roleId = request.getParameter("roleId");

			// tree
			List<MenuInfo> menuList = new ArrayList<MenuInfo>();
			try {
				menuList = menuService.queryByParentId(Globals.Menu_Level_ONE);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			List<MenuInfo> loginActionlist = new ArrayList<MenuInfo>();// 已有权限菜单
			RoleInfo role = new RoleInfo();
			try {
				role = roleService.queryById(roleId);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			request.setAttribute("role", role);
			if (role != null) {

				List<RoleDetailInfo> roleMenuList = new ArrayList<RoleDetailInfo>();
				try {
					roleMenuList = roleDetailService.queryByRoleId(roleId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (roleMenuList.size() > 0) {
					for (RoleDetailInfo roleMenu : roleMenuList) {
						MenuInfo menu = new MenuInfo();
						try {
							menu = menuService.queryById(roleMenu
									.getMenuPkId());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						loginActionlist.add(menu);
					}
				}
			}

			List<ZTree> list = new ArrayList<ZTree>();

			list = tree(menuList, loginActionlist);

			JSONArray jObject = JSONArray.fromObject(list);
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			try {
				PrintWriter pw = response.getWriter();
				pw.write(jObject.toString());
				pw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(params = "setDepartAuthority=key")
	public void setDepartAuthority(HttpServletRequest request,
			HttpServletResponse response, Tree tree) throws Exception {
		// 取得roleId
		try {
			String roleId = request.getParameter("roleId");

			// tree
			List<Depart> departList = new ArrayList<Depart>();
			try {
				departList = departService.queryByParentId(Globals.Menu_Level_ONE);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			List<Depart> loginActionlist = new ArrayList<Depart>();// 已有权限菜单
			RoleInfo role = new RoleInfo();
			try {
				role = roleService.queryById(roleId);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if (role != null) {

				List<RoleDepartInfo> roleDepartList = new ArrayList<RoleDepartInfo>();
				try {
					roleDepartList = roleDepartService.queryByRoleId(roleId);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (roleDepartList.size() > 0) {
					for (RoleDepartInfo roleDepartInfo : roleDepartList) {
						try {
							Depart depart = departService.queryById(roleDepartInfo
									.getDepartPkId());
							loginActionlist.add(depart);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}

			List<Tree> list = new ArrayList<Tree>();

			list = treeDepart(departList, loginActionlist);

			JSONArray jObject = JSONArray.fromObject(list);
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			try {
				PrintWriter pw = response.getWriter();
				//System.out.println(jObject.toString());
				pw.write(jObject.toString());
				pw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * 根据模型生成Tree JSON
	 * 
	 * @param all全部对象
	 * @param in已拥有的对象
	 * @param comboTreeModel模型
	 * @return
	 */
	public List<ZTree> tree(List<MenuInfo> all, List<MenuInfo> in) {
		try {
			List<ZTree> trees = new ArrayList<ZTree>();
			for (MenuInfo obj : all) {
				trees.add(trees(obj, in, true));
			}
			return trees;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	/**
	 * 根据模型生成Tree JSON
	 * 
	 * @param all全部对象
	 * @param in已拥有的对象
	 * @param comboTreeModel 模型
	 * @return
	 */
	public List<Tree> treeDepart(List<Depart> all, List<Depart> in) {
		try {
			List<Tree> trees = new ArrayList<Tree>();
			for (Depart obj : all) {
				trees.add(treesDepart(obj, in, true));
			}
			return trees;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	// 构建Tree

	@SuppressWarnings( { "unchecked", "unused" })
	private Tree treesDepart(Depart obj, List<Depart> in, boolean recursive) {
		try {
			Tree tree = new Tree();

			tree.setId(obj.getPkId());
			tree.setText(obj.getDepartName());

			if (in == null) {
			} else {
				if (in.size() > 0) {
					for (Depart inobj : in) {
						if (obj.getPkId().equals(inobj.getPkId())) {
							tree.setChecked(true);
						}
					}
				}
			}
			List<Depart> departs = null;
			try {
				departs = departService.queryByParentId(obj.getPkId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			;
			if (departs != null && departs.size() > 0) {
				tree.setState("closed");
				tree.setChecked(false);

				if (recursive) {
					// 递归查询子节点
					List<Depart> departList = new ArrayList<Depart>(departs);
					// 排序
					List<Tree> children = new ArrayList<Tree>();
					for (Depart depart : departList) {
						Tree t = treesDepart(depart, in, true);
						children.add(t);
					}
					tree.setChildren(children);
				}

			}
			return tree;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	// 构建Tree

	@SuppressWarnings( { "unchecked", "unused" })
	private ZTree trees(MenuInfo obj, List<MenuInfo> in, boolean recursive) {
		try {
			ZTree tree = new ZTree();

			tree.setId(obj.getPkId());
			tree.setName(obj.getMenuName());

			if (in == null) {
			} else {
				if (in.size() > 0) {
					for (MenuInfo inobj : in) {
						if (obj.getPkId().equals(inobj.getPkId())) {
							tree.setChecked(true);
						}
					}
				}
			}
			List<MenuInfo> menus = null;
			try {
				menus = menuService.queryByParentId(obj.getPkId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			;
			
			RoleInfo role=(RoleInfo)SpringContextUtil.getRequest().getAttribute("role");
			if (menus != null && menus.size() > 0) {
				//tree.setOpen("closed");
				tree.setChecked(false);

				if (recursive) {
					// 递归查询子节点
					List<MenuInfo> menuList = new ArrayList<MenuInfo>(menus);
					// 排序
					List<ZTree> children = new ArrayList<ZTree>();
					for (MenuInfo menu : menuList) {
						ZTree t = trees(menu, in, true);
						
						if(role.getRoleName().equalsIgnoreCase("admin")){
//					if(obj.getMenuName().equalsIgnoreCase("系统管理")){
//						tree.setChkDisabled(false);
//						Map<String,Object> map=new HashMap<String,Object>();
//						t.setChkDisabled(true);
//					}
//					if(menu.getMenuName().equalsIgnoreCase("菜单管理")){
////						tree.setChkDisabled(true);
////						Map<String,Object> map=new HashMap<String,Object>();
//						t.setChkDisabled(true);
//					}
						if(menu.getMenuName().equalsIgnoreCase("角色管理")){
//						tree.setChkDisabled(true);
//						Map<String,Object> map=new HashMap<String,Object>();
							t.setChkDisabled(true);
						}
						if(menu.getMenuName().equalsIgnoreCase("用户管理")){
//						tree.setChkDisabled(true);
//						Map<String,Object> map=new HashMap<String,Object>();
							t.setChkDisabled(true);
						}
						if(menu.getMenuName().equalsIgnoreCase("数据字典")){
							tree.setChkDisabled(false);
//						Map<String,Object> map=new HashMap<String,Object>();
							t.setChkDisabled(true);
						}
						}
						children.add(t);
					}
					tree.setChildren(children);
				}

			}
			
			return tree;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

//	// 构建Tree
//
//	@SuppressWarnings( { "unchecked", "unused" })
//	private Tree trees(MenuInfo obj, List<MenuInfo> in, boolean recursive) {
//		Tree tree = new Tree();
//
//		tree.setId(obj.getPkId());
//		tree.setText(obj.getMenuName());
//
//		if (in == null) {
//		} else {
//			if (in.size() > 0) {
//				for (MenuInfo inobj : in) {
//					if (obj.getPkId().equals(inobj.getPkId())) {
//						tree.setChecked(true);
//					}
//				}
//			}
//		}
//		List<MenuInfo> menus = null;
//		try {
//			menus = menuService.queryByParentId(obj.getPkId());
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		;
//		if (menus != null && menus.size() > 0) {
//			tree.setState("closed");
//			tree.setChecked(false);
//
//			if (recursive) {
//				// 递归查询子节点
//				List<MenuInfo> menuList = new ArrayList<MenuInfo>(menus);
//				// 排序
//				List<Tree> children = new ArrayList<Tree>();
//				for (MenuInfo menu : menuList) {
//					Tree t = trees(menu, in, true);
//					RoleInfo role=(RoleInfo)SpringContextUtil.getRequest().getAttribute("role");
//					if(role.getRoleName().equalsIgnoreCase("admin")){
//					if(obj.getMenuName().equalsIgnoreCase("系统管理")){
//						Map<String,Object> map=new HashMap<String,Object>();
//						map.put("disabled","true");
//						map.put("as","as");
//						t.setAttributes(map);
//					}
//					}
//					children.add(t);
//				}
//				tree.setChildren(children);
//			}
//
//		}
//		return tree;
//	}

	/**
	 * 角色列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "fun=key")
	public ModelAndView fun(HttpServletRequest request) {
		String roleId = request.getParameter("roleId");
		request.setAttribute("roleId", roleId);
		return new ModelAndView("pages/sysManager/role/roleSet");
	}
	
	/**
	 * 角色列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "funDepart=key")
	public ModelAndView funDepart(HttpServletRequest request) {
		String roleId = request.getParameter("roleId");
		request.setAttribute("roleId", roleId);
		return new ModelAndView("pages/sysManager/role/roleDepart");
	}
	
	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add() {
		return new ModelAndView("pages/sysManager/role/role");
	}

	/*************************************************************************
	 * 函数名: modify 功能描述: 跳转到修改页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(RoleInfo roleInfo, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			roleInfo = roleService.queryById(roleInfo.getPkId());
			context.put("roleInfo", roleInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/role/roleUpdate", context);
	}

	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "roleList=key")
	public ModelAndView roleList() {
		return new ModelAndView("pages/sysManager/role/roleList");
	}

	/**
	 * 更新按钮权限
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "updateOperation=key")
	@ResponseBody
	public AjaxJson updateOperation(HttpServletRequest request) {
		try {
			AjaxJson j = new AjaxJson();
			String roleId = request.getParameter("roleId");
			String menuId = request.getParameter("menuId");
			String operationcodes = request.getParameter("operationcodes");

			List<RoleDetailInfo> roleMenuList = null;
			try {
				roleMenuList = roleDetailService.queryByRoleIdAndMenuId(roleId,
						menuId);
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (null != roleMenuList && roleMenuList.size() > 0) {
				RoleDetailInfo roleMenu = roleMenuList.get(0);

				roleMenu.setDescription(operationcodes);
				try {
					roleDetailService.updateBySelective(roleMenu);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try {
				operateLogService.addLog("角色管理模块", "更新权限日志", Globals.Log_Type_OTHER);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			j.setMsg("按钮权限更新成功");
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
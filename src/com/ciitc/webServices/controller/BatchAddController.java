package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.appIntegralDetails;
import com.ciitc.webServices.service.BatchAddService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.appIntegralDetailsService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年4月26日 下午7:48:09
 * 修 改 人: 
 ******************************************************************************/
@Controller
@RequestMapping("/batchAddController")
public class BatchAddController extends BaseController{

	@Autowired(required = false)
	private BatchAddService<AppUserInfor> batchAddService;
	@Autowired(required = false)
	private appIntegralDetailsService<appIntegralDetails> appIntegralDetailsService;
	@Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService;
	private String message;
	//跳转到单个用户积分增加页面
	@RequestMapping(params = "toAddOne=key")
	public ModelAndView toAddOne (String pkId) {
		Map<String, Object> context = new HashMap<String, Object>();
		AppUserInfor appUserInfor = batchAddService.queryByPkId(pkId);
		context.put("appUserInfor", appUserInfor);
		//根据数据字典查询状态
		List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("jfType");
		EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
		EsbmmCode codePkId = new EsbmmCode();
		codePkId.setCodeTypePkId(codeTypePkId.getPkId());
		//根据codeTypePkId查找对应的类型
		List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
		context.put("codeList", codeList);
		return new ModelAndView("pages/order/batchAddOne",context);
	}
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, AppUserInfor appUserInfor, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			if(appUserInfor.getCreateDate() !=null){
				SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd"); 
				String createTime = time.format(appUserInfor.getCreateDate());
				paramMap.put("createTime", createTime);
			}
			paramMap.put("page", basePage);
			paramMap.put("appUserInfor", appUserInfor);
			List<AppUserInfor> list = new ArrayList<AppUserInfor>();
			try {
				list = batchAddService.queryByList(paramMap);
				if(list.size()>0) {
					for (AppUserInfor appUserInfor2 : list) {
						//手机号码显示前三后四
						String phone = appUserInfor2.getPhoneNumber();
						if(null != phone && !"".equals(phone)) {
							phone = phone.substring(0, 3) + "****"+phone.substring(phone.length()-4, phone.length());
							appUserInfor2.setPhoneNumber(phone);
						}
						//证件号码显示前四后四
						String idCard = appUserInfor2.getIdcardNumber();
						if(null != idCard && !"".equals(idCard)) {
							idCard = idCard.substring(0, 4) + "****"+idCard.substring(idCard.length()-4, idCard.length());
							appUserInfor2.setIdcardNumber(idCard);
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	//清空个人积分
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String pkId) {
		AjaxJson json = new AjaxJson();
		AppUserInfor appUserInfor = new AppUserInfor();
		appUserInfor.setPkId(pkId);
		appUserInfor.setIntegral(0);
		batchAddService.addUserInfoOne(appUserInfor);
		appIntegralDetails appIntegralDetails = new appIntegralDetails();
		//根据客户pkid查询积分明细表的有效积分，全部改为无效积分
		List<appIntegralDetails> list = appIntegralDetailsService.queryByYX(pkId);
		for (appIntegralDetails appIntegralDetails2 : list) {
			appIntegralDetails2.setExpireIntegral(appIntegralDetails2.getValidIntegral());
			appIntegralDetails2.setValidIntegral(0);
			try {
				appIntegralDetailsService.update(appIntegralDetails2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		appIntegralDetails.setPkId(UUID.randomUUID().toString());
		appIntegralDetails.setUserId(appUserInfor.getPkId());
		appIntegralDetails.setRecordDate(new Date());
		appIntegralDetails.setIntegralNum("0");
		appIntegralDetails.setIntegralState("01");
		appIntegralDetails.setReason("用户积分清零");
		appIntegralDetails.setName("用户积分清零");
		appIntegralDetailsService.insertSelective(appIntegralDetails);
		json.setObj("ok");
		json.setMsg("清零成功");
		return json;
	}
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(String pkId) {
		Map<String, Object> context = new HashMap<String, Object>();
		AppUserInfor appUserInfor = batchAddService.queryByPkId(pkId);
		context.put("appUserInfor", appUserInfor);
		return new ModelAndView("pages/order/batchModify",context);
	}
			
	@RequestMapping(params = "batchAddList=key")
	public ModelAndView userInfoList() {
		return new ModelAndView("pages/order/batchAddList");
	}
	
	@RequestMapping(params = "batchAdd=key")
	public ModelAndView sysPermissionInfoAdd() {
		return new ModelAndView("pages/order/batchAdd");
	}
	
	//增加单个用户积分
	@RequestMapping(params = "saveUpdateOne=key")
	@ResponseBody
	public AjaxJson saveUpdateOne(HttpServletRequest request, HttpServletResponse response, AppUserInfor appUserInfor) throws Exception {
		AjaxJson j = new AjaxJson();
		//保存用户积分明细
		appIntegralDetails appIntegralDetails = new appIntegralDetails();
		appIntegralDetails.setPkId(UUID.randomUUID().toString());
		appIntegralDetails.setUserId(appUserInfor.getPkId());
		appIntegralDetails.setRecordDate(new Date());
		appIntegralDetails.setIntegralNum(appUserInfor.getRemark2());
		appIntegralDetails.setIntegralState("00");
		appIntegralDetails.setIntegralType(appUserInfor.getRemark1());
		appIntegralDetails.setReason(appUserInfor.getRemark3());
		appIntegralDetails.setName(appUserInfor.getRemark4());
		appIntegralDetails.setValidIntegral(Long.parseLong(appUserInfor.getRemark2()));
		appIntegralDetails.setValidity(365);
		String time = getYears(new Date());
		SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
		Date da = sf.parse(time);
		appIntegralDetails.setExpireDate(da);
		appIntegralDetailsService.insertSelective(appIntegralDetails);
		long  num = appUserInfor.getIntegral() + Integer.parseInt(appUserInfor.getRemark2());
		appUserInfor.setIntegral(num);
		//根据pkId增加单个用户的积分
		batchAddService.addUserInfoOne(appUserInfor);
		
		message="保存成功";
		
		j.setMsg(message);
		return j;
	}
	
	//修改单个用户积分
	@RequestMapping(params = "updateOne=key")
	@ResponseBody
	public AjaxJson updateOne(HttpServletRequest request, HttpServletResponse response, AppUserInfor appUserInfor) throws Exception {
		AjaxJson j = new AjaxJson();
		//保存用户积分明细
		appIntegralDetails appIntegralDetails = new appIntegralDetails();
		appIntegralDetails.setPkId(UUID.randomUUID().toString());
		appIntegralDetails.setUserId(appUserInfor.getPkId());
		appIntegralDetails.setRecordDate(new Date());
		appIntegralDetails.setIntegralType("99");
		Integer re2 = Integer.parseInt(appUserInfor.getRemark2());
		if(appUserInfor.getIntegral() > re2) {
			long num = appUserInfor.getIntegral()-re2;
			//根据客户编号查询客户积分明细有效积分
				while(num!=0) {
					appIntegralDetails appList = minIntegra(appUserInfor.getPkId());
					if(appList.getValidIntegral()>=num) {
						appList.setValidIntegral(appList.getValidIntegral()-num);
						appList.setExpireIntegral(num);
						appIntegralDetailsService.updateBySelective(appList);
					}else {
						appList.setValidIntegral(0);
						appList.setExpireIntegral(appList.getValidIntegral());
						num = (long) (num - appList.getValidIntegral());
						appIntegralDetailsService.updateBySelective(appList);
					}
				}
			//
			appIntegralDetails.setIntegralState("01");
			appIntegralDetails.setIntegralNum(String.valueOf(num));
			appIntegralDetails.setConsumptionIntegral(num);
			appIntegralDetails.setReason("用户积分调整：减少"+num+"原因："+appUserInfor.getRemark3());
			appIntegralDetails.setName("修改用户积分");
		}else {
			long num = re2 - appUserInfor.getIntegral();
			appIntegralDetails.setValidity(365);
			appIntegralDetails.setIntegralState("00");
			appIntegralDetails.setIntegralNum(String.valueOf(num));
			appIntegralDetails.setValidIntegral(num);
			String time = getYears(new Date());
			SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
			Date da = sf.parse(time);
			appIntegralDetails.setExpireDate(da);
			appIntegralDetails.setReason("用户积分调整：增加"+num+"原因："+appUserInfor.getRemark3());
			appIntegralDetails.setName("修改用户积分");
		}
		
		appIntegralDetailsService.insertSelective(appIntegralDetails);
		appUserInfor.setIntegral(Integer.parseInt(appUserInfor.getRemark2()));
		//根据pkId增加单个用户的积分
		batchAddService.addUserInfoOne(appUserInfor);
		
		message="修改成功";
		
		j.setMsg(message);
		return j;
	}
	//批量增加用户积分
	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, HttpServletResponse response, AppUserInfor appUserInfor) throws Exception {
		AjaxJson j = new AjaxJson();
		long num = appUserInfor.getIntegral();
		List<AppUserInfor> list = batchAddService.queryByAll();
		if(list != null) {
			for (AppUserInfor appUserInfor2 : list) {
				appUserInfor2.setIntegral(appUserInfor2.getIntegral()+num);
				//保存增加后的积分数量
				batchAddService.addUserInfoOne(appUserInfor2);
				//保存增加的积分明细
				appIntegralDetails appIntegralDetails = new appIntegralDetails();
				appIntegralDetails.setPkId(UUID.randomUUID().toString());
				appIntegralDetails.setUserId(appUserInfor2.getPkId());
				appIntegralDetails.setRecordDate(new Date());
				appIntegralDetails.setIntegralType("99");
				appIntegralDetails.setValidity(365);
				appIntegralDetails.setIntegralState("00");
				appIntegralDetails.setIntegralNum(String.valueOf(num));
				appIntegralDetails.setValidIntegral(num);
				String time = getYears(new Date());
				SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
				Date da = sf.parse(time);
				appIntegralDetails.setExpireDate(da);
				appIntegralDetails.setReason("奖励积分");
				appIntegralDetails.setName("批量增加");
				appIntegralDetailsService.insertSelective(appIntegralDetails);
			}
		}
		//batchAddService.update(appUserInfor);
		message="保存成功";
		
		j.setMsg(message);
		return j;
	}
	//查询最小时间积分明细
	public appIntegralDetails minIntegra(String pkId) {
		appIntegralDetails appInt = appIntegralDetailsService.queryByYXMin(pkId);
		return appInt;
	}
	//年份
	public static String getYears(Date d)
		{
			//Date d=new Date();
			GregorianCalendar gc =new GregorianCalendar();
			SimpleDateFormat sf  =new SimpleDateFormat("yyyy-MM-dd");
			gc.setTime(d);
			gc.add(1,+1);
			gc.set(gc.get(Calendar.YEAR),gc.get(Calendar.MONTH),gc.get(Calendar.DATE));

		return sf.format(gc.getTime());
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

package com.ciitc.webServices.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppMessageSend;
import com.ciitc.webServices.model.CPTbrokertree;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.JpushUntilVo;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.AppMessageService;
import com.ciitc.webServices.service.CPTbrokertreeService;
import com.ciitc.webServices.service.CPTfundbrokerService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.util.JpushUntil;
import com.ciitc.webServices.util.Log;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/appMessageController")
public class AppMessageController extends BaseController{
	
	@Autowired(required = false)
	private AppMessageService<AppMessageSend> appMessageService;
	
	@Autowired(required = false)
	private CPTbrokertreeService<CPTbrokertree>cPTbrokertreeService;
	
	@Autowired(required = false)
	private CPTfundbrokerService<CPTfundbroker>cpTfundbrokerService;
	
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService;
	
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	
	private String message;
	
	
	
	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/	
	@RequestMapping(params = "appmessageList=key")
	public ModelAndView departList() {
		return new ModelAndView("pages/app/appmessageList");
	}
	
	@RequestMapping(params="datagrid=key")
	public void datagrid(HttpServletResponse response,AppMessageSend appMessageSend, BasePage basePage) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		paramMap.put("appMessageSend", appMessageSend);
		List<AppMessageSend> list = new ArrayList<AppMessageSend>();
		try {
			list = appMessageService.queryByList(paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 记录总条数
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
		pageUtil.responseDatagrid(response, jObject);
		
	}
	
	@RequestMapping(params="add=key")
	public ModelAndView repairInforAdd(HttpServletRequest req) {
		return new ModelAndView("pages/app/appmessageAdd");
	}
	
	@RequestMapping(params="modify=key")
	public ModelAndView repairInforModify(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		AppMessageSend appMessageSend=appMessageService.queryById(pkId);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timingSendDate=sdf.format(appMessageSend.getTimingSendDate());
		CPTbrokertree sendObject = cPTbrokertreeService.queryById(appMessageSend.getSendObject());
		if(null != sendObject) {
			appMessageSend.setSendObject(sendObject.getcNodevalue());
		}else {
			CPTfundbroker cpTfundbroker = cpTfundbrokerService.queryByFid(appMessageSend.getSendObject());
			if(null != cpTfundbroker) {
				appMessageSend.setSendObject(cpTfundbroker.getcBrokername());
			}
		}
		context.put("timingSendDate", timingSendDate);
		// 本机地址 
        List<EsbmmCode> code = esbmmCodeService.getAddress();
        String address = code.get(0).getCodeValue();
        String URL = address+appMessageSend.getPictureURL();
        appMessageSend.setURL(URL);
		context.put("appMessageSend", appMessageSend);
		
		return new ModelAndView("pages/app/appmessageAdd",context);
	}
	
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(AppMessageSend appMessageSend) throws Exception{
		UserInfo user = ResourceUtil.getSessionUserName();
		AjaxJson json=new AjaxJson();
		if(appMessageSend.getPkId()!=null && !"".equals(appMessageSend.getPkId())){
			try {
				appMessageSend.setOperateName(user.getUserName());
				appMessageSend.setSendDate("-");
				appMessageSend.setSendType("人工");
				appMessageSend.setSendState("未推送");
				appMessageService.update(appMessageSend);
				operateLogService.addLog("APP消息推送管理", "编辑APP信息日志", Globals.Log_Type_UPDATE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			message = "编辑成功";
		}else{
			String uuid = UUID.randomUUID().toString();
			appMessageSend.setPkId(uuid);
			appMessageSend.setOperateName(user.getUserName());
			appMessageSend.setSendType("人工");
			appMessageSend.setSendState("未推送");
			appMessageSend.setSendDate("-");
			appMessageService.add(appMessageSend);
			operateLogService.addLog("APP消息推送管理", "新增APP信息日志", Globals.Log_Type_INSERT);
			message="保存成功";
		}
		json.setMsg(message);
		return json;
	}
	
	
	
	@RequestMapping(params="sendMessage=key")
	@ResponseBody
	public AjaxJson saveUpdate1(AppMessageSend appMessageSend) throws Exception{
		Calendar nowDate = Calendar.getInstance();
		nowDate.add(Calendar.MINUTE, -5);
		JpushUntil until =new JpushUntil();
		JpushUntilVo untilVo =new JpushUntilVo();
		UserInfo user = ResourceUtil.getSessionUserName();
		String appKey = null;
		String masterSecret = null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();//临时上传目录
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();//CRM上传目录
			}
		}
		AjaxJson json=new AjaxJson();
		if(appMessageSend.getPkId()!=null && !"".equals(appMessageSend.getPkId())){
			try {
				appMessageSend.setSendDate("-");
				appMessageSend.setOperateName(user.getUserName());
				appMessageSend.setSendType("人工");
				appMessageSend.setSendState("未推送");
				//定时给用给用户推送消息（组发--因为每组下面有很多人）
				Set<String> tags = new HashSet<String>();
				tags.add(appMessageSend.getSendObject());
				untilVo.setTag(tags);
				untilVo.setAppKey(appKey);
				untilVo.setMasterSecret(masterSecret);
				untilVo.setMaxtrytimes(3);
				untilVo.setMessageVo(appMessageSend);
				if(appMessageSend.getTimingSendDate().after(nowDate.getTime())) {
					Log.getLogger().info("-------推送开始--------");
					String state = until.timSingleSchedule(untilVo);
					operateLogService.addLog("APP消息推送管理", "APP信息推送日志", Globals.Log_Type_SEND);
					if(null != state && !"".equals(state)) {
						if(state .equals("推送成功")) {
							appMessageService.update(appMessageSend);
							operateLogService.addLog("APP消息推送管理", "编辑APP信息推送日志", Globals.Log_Type_UPDATE);
							message = "推送成功";
							Log.getLogger().info("------推送成功-------");
						}else {
							message = "推送失败";
						}
						 Log.getLogger().info("------状态信息不为空-------");
					}else {
						message = "推送失败";
					}
				}else {
					message="推送时间过早，推送失败";
					Log.getLogger().info("------推送时间过早，推送失败------");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			String uuid = UUID.randomUUID().toString();
			appMessageSend.setPkId(uuid);
			appMessageSend.setOperateName(user.getUserName());
			appMessageSend.setSendType("人工");
			appMessageSend.setSendState("未推送");
			appMessageSend.setSendDate("-");
			//定时给用给用户推送消息（组发--因为每组下面有很多人）
			Set<String> tags = new HashSet<String>();
			tags.add(appMessageSend.getSendObject());
			untilVo.setTag(tags);
			untilVo.setAppKey(appKey);
			untilVo.setMasterSecret(masterSecret);
			untilVo.setMaxtrytimes(3);
			untilVo.setMessageVo(appMessageSend);
			if(appMessageSend.getTimingSendDate().after(nowDate.getTime())) {
				Log.getLogger().info("---------推送开始---------");
				String state = until.timSingleSchedule(untilVo);
				operateLogService.addLog("APP消息推送管理", "APP信息推送日志", Globals.Log_Type_SEND);
				if(null != state && !"".equals(state)) {
					if(state .equals("推送成功")) {
						appMessageService.add(appMessageSend);
						operateLogService.addLog("APP消息推送管理", "新增APP信息推送保存日志", Globals.Log_Type_INSERT);
						message="推送成功";
						Log.getLogger().info("---------推送成功---------");
					}else {
						message="推送失败";
					}
					Log.getLogger().info("------状态信息不为空-------");
				}else {
					message="推送失败";
				}
			}else {
				message="推送时间过早，推送失败";
				Log.getLogger().info("推送时间过早，推送失败");
			}
		}
		json.setMsg(message);
		return json;
	}
	
	
	
	
	
	@RequestMapping(params="delete=key")
	@ResponseBody
	public AjaxJson delete(AppMessageSend appMessageSend) throws Exception{
		AjaxJson json=new AjaxJson();
		if(appMessageSend.getPkId()!=null && !"".equals(appMessageSend.getPkId())){
			appMessageService.delete(appMessageSend.getPkId());
		} 
		operateLogService.addLog("APP消息推送管理", "删除APP信息日志", Globals.Log_Type_DEL);
		message="删除成功";
		json.setObj(message);
		return json;
		
	}
	
	@RequestMapping(params="specific=key")
	public ModelAndView repairInforSpecific(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		AppMessageSend appMessageSend=appMessageService.queryById(pkId);
	//	OrganizaInfo sendObject = organizaInfoService.queryById(appMessageSend.getSendObject());
		CPTbrokertree sendObject = cPTbrokertreeService.queryById(appMessageSend.getSendObject());
		if(null != sendObject) {
			appMessageSend.setSendObject(sendObject.getcNodevalue());
		}else {
			CPTfundbroker cpTfundbroker = cpTfundbrokerService.queryByFid(appMessageSend.getSendObject());
			if(null != cpTfundbroker) {
				appMessageSend.setSendObject(cpTfundbroker.getcBrokername());
			}
		}
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String timingSendDate=sdf.format(appMessageSend.getTimingSendDate());
		context.put("timingSendDate", timingSendDate);
		// 本机地址 
        List<EsbmmCode> code = esbmmCodeService.getAddress();
        String address = code.get(0).getCodeValue();
        String URL = address+appMessageSend.getPictureURL();
        appMessageSend.setURL(URL);
		context.put("appMessageSend", appMessageSend);
		return new ModelAndView("pages/app/appmessageSpecific",context);
	}
	
	
	
	@RequestMapping(params = "uploadPic=key")  
    public void uploadPic( @RequestParam(value = "pic")MultipartFile file, HttpServletRequest request, HttpServletResponse response) throws IllegalStateException, IOException {  
  
        try {  
            // 获取图片原始文件名  
            String originalFilename = file.getOriginalFilename();  
            // 文件名使用ID 
            String name = UUID.randomUUID().toString();  
            // 获取上传图片的扩展名(jpg/png/...)  
            String extension = FilenameUtils.getExtension(originalFilename);  
            // 图片上传本地路径  
            List<EsbmmCode> code1 = esbmmCodeService.getUploadAddress();
            String uploadAddress = code1.get(0).getCodeValue();
            String path = uploadAddress + name + "." + extension;
            //Tomcat虚拟路径
            String path1 = "/photo/" + name + "." + extension;
            File dir = new File(path);  
            if(!dir.exists()) {  
            dir.mkdirs();  
            }  
            // 上传图片  
            file.transferTo(new File(path));  
            // 本机地址 
            JSONObject jsonObject = new JSONObject(); 
            List<EsbmmCode> code = esbmmCodeService.getAddress();
            String address = code.get(0).getCodeValue();
            // 将虚拟路径保存并返回页面
            jsonObject.put("path", address+path1); 
            jsonObject.put("path1", path1);
            // 设置响应数据的类型json  
            response.setContentType("application/json; charset=utf-8");  
            response.getWriter().write(jsonObject.toString());
            operateLogService.addLog("APP消息推送管理", "APP信息图片上传日志", Globals.Log_Type_UPLOAD);
  
        } catch (Exception e) {  
            throw new RuntimeException("服务器繁忙，上传图片失败");  
        }  
    }  
	
	public List<Map> combotree(String cParentcode) throws Exception {

		try {
			// combotree
			//List<OrganizaInfo> list = new ArrayList<OrganizaInfo>();
			List<CPTbrokertree> list = new ArrayList<CPTbrokertree>();
			try {
			//	list = organizaInfoService.queryByParentId(parentId);
				list = cPTbrokertreeService.queryByParentId(cParentcode);
			} catch (Exception e) {
				e.printStackTrace();
			}

			List<Map> treeList = new ArrayList<Map>();
			for (CPTbrokertree ams : list) {
				Map<String, Object> node = new HashMap<String, Object>();
				if(ams.getcParentcode().equals("0")){
					node.put("id", ams.getcNodecode());
					node.put("text", ams.getcNodevalue());
					childTree(node);
					treeList.add(node);
				}
			}
			return treeList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	private void childTree(Map<String, Object> node) {
		try {
			List<CPTbrokertree> childList = new ArrayList<CPTbrokertree>();
			List<Map> tree = new ArrayList<Map>();
			try {
				childList = cPTbrokertreeService.queryByParentId(node.get("id"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (childList.size() > 0) {
				for (CPTbrokertree ams1 : childList) {
					Map<String, Object> childNode = new HashMap<String, Object>();
					childNode.put("id", ams1.getcNodecode());
					childNode.put("text", ams1.getcNodevalue());
					childTree(childNode);
					tree.add(childNode);
				}
				node.put("children", tree);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@RequestMapping(params = "getCombotree=key")
	public void getCombotree(HttpServletRequest request,
			HttpServletResponse response, CPTbrokertree cPTbrokertree) throws Exception {

		try {
			List<Map> list = new ArrayList<Map>();
			list = combotree(cPTbrokertree.getcNodecode());

			JSONArray jObject = JSONArray.fromObject(list);
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			try {
				PrintWriter pw = response.getWriter();
				pw.write(jObject.toString());
				pw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}

package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.dao.AppMessageSendDao;
import com.ciitc.webServices.dao.MessageRemindRuleDao;
import com.ciitc.webServices.model.AppMessageSend;
import com.ciitc.webServices.model.Coefficient;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.MessageRemindRule;
import com.ciitc.webServices.model.ProductInfo;
import com.ciitc.webServices.service.CoefficientService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.ProductInfoService;
import com.ciitc.webServices.util.CRM_ReservationUtil;
import com.ciitc.webServices.util.PublicJpushUntil;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/coefficientController")
public class CoefficientController extends BaseController {
	@SuppressWarnings("rawtypes")
	private AppMessageSendDao appMessageSendDao = (AppMessageSendDao) SpringContextUtil.getBean("appMessageSendDao");
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService;
	@Autowired(required = false)
	private ProductInfoService<ProductInfo> productInfoService;
	@Autowired(required = false)
	private CoefficientService<Coefficient> coefficientService;
	
	@SuppressWarnings("rawtypes")
	private MessageRemindRuleDao messageRemindRuleDao = (MessageRemindRuleDao) SpringContextUtil.getBean("messageRemindRuleDao");
	
	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request,
			HttpServletResponse response, ProductInfo productInfo, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("productInfo", productInfo);
			List<ProductInfo> list = new ArrayList<ProductInfo>();
			try {
				list = productInfoService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*************************************************************************
	 * 功能描述: 跳转到产品信息查询页面
	 *************************************************************************/
	@RequestMapping(params = "ProductInfoList=key")
	public ModelAndView departList() {
		return new ModelAndView(
				"pages/sysManager/coefficient/coefficientList");
	}
	
	/*************************************************************************
	 * 功能描述: 跳转到折标系数配置页面
	 *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(String pkId) {
		Map<String, Object> context = getRootMap();
		List<Coefficient> list = new ArrayList<Coefficient>();
		list=coefficientService.queryByDate(pkId);
		StringBuffer dateVal = new StringBuffer();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		
		if(list.size()!=0){
		for(Coefficient coefficient:list){
			String time = format.format(coefficient.getScalinDate());
			//dateVal.append("{date:'"+time+"',data:"+coefficient.getCoefficient()+"}&");
			dateVal.append(time+":"+coefficient.getCoefficient()+":");
			
		    }
		}else{
			String time = format.format(new Date());
			dateVal.append(time+":"+""+":");	
		}
		context.put("dateVal",dateVal.toString());
		context.put("productId",pkId);
		return new ModelAndView("pages/sysManager/coefficient/dateList",context);
	}
	//批量配置折标系数
	@RequestMapping(params = "modify1=key")
	public ModelAndView modify1(String pkId) {
		Map<String, Object> context = getRootMap();
		context.put("productId",pkId);
		context.put("money",1);
		return new ModelAndView("pages/sysManager/coefficient/dateList1",context);
	}
	
	/*************************************************************************
	 * 功能描述: 跳转折标系数新增变更页面
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(String date,String money,String productId) {
		
		Map<String, Object> context = getRootMap();
		
		if(money.equals("undefined") || "".equals(money)){
			context.put("money", 1);
		}else{
			context.put("money", money);	
		}				
		context.put("date", date);
		context.put("productId", productId);
		return new ModelAndView("pages/sysManager/coefficient/coefficientupdate",context);
	}
	
	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 保存 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
   //配置折标系数
	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request,String date,String money,String productId)throws Exception {
		AjaxJson j = new AjaxJson();
		//根据时间与商品id进行查询，如果包含当前日期则进行修改，不包含进行新增。
			try {
			Map<String, Object> paramMap = new HashMap<String, Object>();
			Coefficient coefficient = new Coefficient();
		    List<Coefficient> list = new ArrayList<Coefficient>();
            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
            Date scalinDate = sf.parse(date);
			paramMap.put("scalinDate", scalinDate);
			paramMap.put("productId", productId);
			list = coefficientService.selectByDate(paramMap);
		
			
			double coefficients = Double.parseDouble(money);
			if(list.size()!=0){
				list.get(0).setCoefficient(coefficients);
				coefficientService.updateBySelective(list.get(0));
				send(productId);
				message = "保存成功";
				//折标系数配置成功之后，执行孙总的存储过程
				ProductInfo updateParam=new ProductInfo();
				updateParam.setPkId(productId);
				updateParam.setRate(money);
				productInfoService.updateCrmCoefficient(updateParam);
			}else{		
				String pkId = UUID.randomUUID().toString();	
				coefficient.setPkId(pkId);
				coefficient.setProductId(productId);	
				coefficient.setCoefficient(coefficients);
				coefficient.setScalinDate(scalinDate);
				coefficientService.insert(coefficient);
				send(productId);
				message = "保存成功";
			   
			}
				
			} catch (Exception e) {
				message = "保存失败";
				e.printStackTrace();
				//j.setMsg(message);
				return j;
			
		}
				j.setMsg("保存成功");

			return j;
		}
	//批量配置折标系数保存
	@RequestMapping(params = "saveUpdate1=key")
	@ResponseBody
	public AjaxJson saveUpdate1(HttpServletRequest request,String startDate,String endDate,String money,String productId)throws Exception {
		AjaxJson j = new AjaxJson();
		//根据时间与商品id进行查询，如果包含当前日期则进行修改，不包含进行新增。
			try {
				Map<String, Object> paramMap = new HashMap<String, Object>();
				Coefficient coefficient = new Coefficient();
			    List<Coefficient> list = new ArrayList<Coefficient>();
	            SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
	            Date sDate = sf.parse(startDate);
	            Date eDate = sf.parse(endDate);
	            while(eDate.after(sDate)||eDate.equals(sDate)) {
	                paramMap.put("scalinDate", sDate);
	    			paramMap.put("productId", productId);
	    			list = coefficientService.selectByDate(paramMap);
	    			double coefficients = Double.parseDouble(money);
	    			if(list.size()!=0){
	    				list.get(0).setCoefficient(coefficients);
	    				coefficientService.updateBySelective(list.get(0));
	    			}else{		
	    				String pkId = UUID.randomUUID().toString();	
	    				coefficient.setPkId(pkId);
	    				coefficient.setProductId(productId);	
	    				coefficient.setCoefficient(coefficients);
	    				coefficient.setScalinDate(sDate);
	    				coefficientService.insert(coefficient);
	    			}
	    			Calendar cal = Calendar.getInstance();
	                cal.setTime(sDate);
	                cal.add(Calendar.DATE, 1);
	                sDate = cal.getTime();
	            }
	            send(productId);
				message = "保存成功";
				//折标系数配置成功之后，执行孙总的存储过程
				ProductInfo updateParam=new ProductInfo();
				updateParam.setPkId(productId);
				updateParam.setRate(money);
				productInfoService.updateCrmCoefficient(updateParam);
			} catch (Exception e) {
				message = "保存失败";
				e.printStackTrace();
				//j.setMsg(message);
				return j;
			}
			j.setMsg("保存成功");
			return j;
		}

	// 向页面返回的信息
		private String message;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
		//推送
		public void send(String productId) {
			CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
			//------------------------------------------------
			PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
			//查看配置的认购结束期，
			MessageRemindRule messageRemindRule = new MessageRemindRule();
			//根据规则名称查询规则信息
			messageRemindRule = messageRemindRuleDao.queryByName("系数变更");
			//根据项目编号查询项目名称
			String productName = reservationUtil.getProductName1(productId);
			//推送内容content
			String content = messageRemindRule.getMessageMould();
			content = content.replace("${productName}",productName);
			//推送标签tag
			Set<String> tag = new HashSet<String>();
			tag.add("0001");
			String appKey=null;
			String masterSecret=null;
			List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
			for (EsbmmCode esbmmCode : jpushConfig) {
				if(esbmmCode.getCodeName().equals("AppKey")) {
					appKey = esbmmCode.getCodeValue();
				}
				if(esbmmCode.getCodeName().equals("MasterSecret")) {
					masterSecret =esbmmCode.getCodeValue();
				}
			}
			AppMessageSend appMessageSend = new AppMessageSend();
			appMessageSend.setPkId(UUID.randomUUID().toString());//主键
			appMessageSend.setFlag("07");
			appMessageSend.setMessageTitle("系数变更");
			appMessageSend.setMessageContent(content);
			appMessageSend.setSendObject("0001");
			appMessageSend.setSendState("未推送");
			appMessageSend.setSendDate("-");
			appMessageSend.setOperateName("-");
			appMessageSend.setMessageType("常规消息");
			appMessageSend.setSendType("系统");
			Date now = new Date();
			appMessageSend.setTimingSendDate(new Date(now.getTime()+6000));
			appMessageSendDao.insertSelective(appMessageSend);
			//调用app推送的方法，推送给公司所有员工
			String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "系数变更", content, tag);
			if("00".equals(states)) {
				SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				AppMessageSend appMessageSend1 = new AppMessageSend();
				appMessageSend1.setSendState("已推送");
				appMessageSend1.setSendDate(sf.format(new Date()));
				appMessageSend1.setPkId(appMessageSend.getPkId());
				appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
			}
			//-----------------------------------------------------------------------
		}
}

package com.ciitc.webServices.controller;

import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.ProductFk;
import com.ciitc.webServices.service.ProductFkService;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

@Controller
@RequestMapping("/productFkController")
public class ProductFkController extends BaseController {
	@Autowired(required = false)
	private ProductFkService<ProductFk> productFkService;
	@RequestMapping(params = "updateProductInfo=key")
	public ModelAndView updateProductInfo(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		//根据产品的id查询风控表
		Map<String, Object> map = getRootMap();
		map.put("productId", pkId);
		ProductFk productFk = productFkService.selectBypid(pkId);
		context.put("productFk", productFk);
		context.put("pkId", pkId);
		return new ModelAndView("pages/sysManager/ProductInfo/productInfoUpdate", context);
	}
	
	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(String pkId,String productId,ProductFk productFk) throws Exception {
		AjaxJson j = new AjaxJson();
		//生成一个uuid字符串
		String uuid = UUID.randomUUID().toString();
		//判断新增还是修改
		if(null == pkId || "".equals(pkId)) {
			productFk.setPkId(uuid);
			productFk.setProductId(productId);
			productFk.setRiskManagement(productFk.getCompensationClause()+productFk.getBettingAgreement()+productFk.getSharePledge());
			productFkService.insertSelective(productFk);
			j.setObj("ok");
			j.setMsg("保存成功");
		}else {
			productFk.setPkId(pkId);
			productFk.setProductId(productId);
			productFk.setRiskManagement(productFk.getCompensationClause()+productFk.getBettingAgreement()+productFk.getSharePledge());
			productFkService.updateByPrimaryKeySelective(productFk);
			j.setObj("ok");
			j.setMsg("修改成功");
		}
		
		return j;	
	}		

}

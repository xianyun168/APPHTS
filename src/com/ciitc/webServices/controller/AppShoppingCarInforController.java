package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppGoodsInfor;
import com.ciitc.webServices.model.AppShoppingCarInfor;
import com.ciitc.webServices.model.Area;
import com.ciitc.webServices.model.City;
import com.ciitc.webServices.model.GoddsAddress;
import com.ciitc.webServices.model.Province;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.AppGoodsInforService;
import com.ciitc.webServices.service.AppShoppingCarInforService;
import com.ciitc.webServices.service.AreaService;
import com.ciitc.webServices.service.CityService;
import com.ciitc.webServices.service.GoddsAddressService;
import com.ciitc.webServices.service.ProvinceService;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

@Controller
@RequestMapping("/appShoppingCarInforController")
public class AppShoppingCarInforController extends BaseController{
	@Autowired(required = false)
	private GoddsAddressService<GoddsAddress> goddsAddressService;
	@Autowired(required = false)
	private AppShoppingCarInforService<AppShoppingCarInfor> appShoppingCarInforService;
	@Autowired(required = false)
	private ProvinceService<Province> provinceService;
	@Autowired(required = false)
	private AppGoodsInforService<AppGoodsInfor> appGoodsInforService;
	@Autowired(required = false)
	private CityService<City> cityService;
	@Autowired(required = false)
	private AreaService<Area> areaService;
	
	//根据收货地址的id查询收货地址的信息
	@RequestMapping(params = "getGoddsAddress=key")
	public ModelAndView getGoddsAddress(HttpServletResponse response,HttpServletRequest request,
			String pkId,String goodsIds,String goodsNums,String goodsNames,
			String mainImageUrls,String prices,String total) {
		Map<String, Object> context = new HashMap<String, Object>();
		try {
			GoddsAddress goddsAddress = goddsAddressService.selectByPrimaryKey(pkId);
			String provinceid = goddsAddress.getProvince();
			Province province = provinceService.selectById(provinceid);
			String cityid = goddsAddress.getCity();
			City city = cityService.selectById(cityid);
			String areaid = goddsAddress.getArea();
			Area area = areaService.selectById(areaid);
			goddsAddress.setProvince(province.getProvince());
			goddsAddress.setCity(city.getCity());
			goddsAddress.setArea(area.getArea());
			//-------------------------
			goddsAddress.setRemark1(goddsAddress.getProvince()+","+goddsAddress.getCity()+","+
				    goddsAddress.getArea()+","+ goddsAddress.getShopAddress());
			context.put("goddsAddress", goddsAddress);
			context.put("userId", goddsAddress.getUserId());
			//---------------------------
			//=============================
			String[] goodsId = goodsIds.split(",");
			String[] goodsNum = goodsNums.split(",");
			String[] goodsName = goodsNames.split(",");
			String[] mainImageUrl = mainImageUrls.split(",");
			String[] price = prices.split(",");
			List<AppGoodsInfor> list = new ArrayList<AppGoodsInfor>();
			for(int i=0;i<goodsId.length;i++) {
				AppGoodsInfor appGoodsInfor = new AppGoodsInfor();
				appGoodsInfor.setGoodsId(goodsId[i]);
				appGoodsInfor.setGoodsName(goodsName[i]);
				appGoodsInfor.setMainImageUrl(mainImageUrl[i]);
				appGoodsInfor.setPrice(Double.parseDouble(price[i]));
				appGoodsInfor.setNum(Integer.parseInt(goodsNum[i]));
				list.add(appGoodsInfor);
			}
			context.put("list", list);
			context.put("total", total);
			//=============================
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/order/orderCount",context);
	}
	//修改页面
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(HttpServletRequest request,HttpServletResponse response,String pkId) {
		Map<String, Object> context = getRootMap();
		List<Province> list = new ArrayList<Province>();
		List<City> list1 = new ArrayList<City>();
		List<Area> list2 = new ArrayList<Area>();
		
		try {
			list = provinceService.selectAll();
			GoddsAddress goddsAddress = goddsAddressService.selectByPrimaryKey(pkId);
			String father = goddsAddress.getProvince();
			list1 = cityService.selectByPrimaryKey(father);
			String areaid = goddsAddress.getArea();
			Area area = areaService.selectById(areaid);
			String cityid = goddsAddress.getCity();
			City city = cityService.selectById(cityid);
			list2 = areaService.selectByPrimaryKey(cityid);
			goddsAddress.setRemark1(area.getArea());
			goddsAddress.setRemark2(city.getCity());
			context.put("goddsAddress", goddsAddress);
			//省
			context.put("list", list);
			//县
			context.put("list1", list1);
			//城市
			context.put("list2", list2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/goddsAddress/goddsAddressAdd",context);
	}
	
	//修改页面
	@RequestMapping(params = "modify1=key")
	public ModelAndView modify1(HttpServletRequest request,HttpServletResponse response,String pkId,
			String goodsIds,String goodsNums,String goodsNames,String mainImageUrls,
			String prices,String userId,String total) {
		Map<String, Object> context = getRootMap();
		List<Province> list = new ArrayList<Province>();
		List<City> list1 = new ArrayList<City>();
		List<Area> list2 = new ArrayList<Area>();
		
		try {
			list = provinceService.selectAll();
			GoddsAddress goddsAddress = goddsAddressService.selectByPrimaryKey(pkId);
			String father = goddsAddress.getProvince();
			list1 = cityService.selectByPrimaryKey(father);
			String areaid = goddsAddress.getArea();
			Area area = areaService.selectById(areaid);
			String cityid = goddsAddress.getCity();
			City city = cityService.selectById(cityid);
			list2 = areaService.selectByPrimaryKey(cityid);
			goddsAddress.setRemark1(area.getArea());
			goddsAddress.setRemark2(city.getCity());
			context.put("goddsAddress", goddsAddress);
			//省
			context.put("list", list);
			//县
			context.put("list1", list1);
			//城市
			context.put("list2", list2);
			//===========================
			String[] goodsId = goodsIds.split(",");
			String[] goodsNum = goodsNums.split(",");
			String[] goodsName = goodsNames.split(",");
			String[] mainImageUrl = mainImageUrls.split(",");
			String[] price = prices.split(",");
			List<AppGoodsInfor> listGoods = new ArrayList<AppGoodsInfor>();
			for(int i=0;i<goodsId.length;i++) {
				AppGoodsInfor appGoodsInfor = new AppGoodsInfor();
				appGoodsInfor.setGoodsId(goodsId[i]);
				appGoodsInfor.setGoodsName(goodsName[i]);
				appGoodsInfor.setMainImageUrl(mainImageUrl[i]);
				appGoodsInfor.setPrice(Double.parseDouble(price[i]));
				appGoodsInfor.setNum(Integer.parseInt(goodsNum[i]));
				listGoods.add(appGoodsInfor);
			}
			context.put("listGoods", listGoods);
			context.put("userId", userId);
			context.put("total", total);
			//===========================
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/goddsAddress/countAddressAdd",context);
	}
	//修改购物车中商品的数量
	@RequestMapping(params = "updateNum=key")
	@ResponseBody
	public AjaxJson updateNum(String productId,String sku,String userId) {
		AjaxJson json = new AjaxJson();
		//UserInfo u = ResourceUtil.getSessionUserName();
		//如果sku=1则代表购物差里商品数量+1，否则-1
		if("1".equals(sku)) {
			try {
				//先根据商品的id去商品表里查询商品的信息，然后插入到购物车表里
				AppGoodsInfor appGoodsInfor = appGoodsInforService.queryByGoodsId(productId);
				AppShoppingCarInfor appShoppingCarInfor = new AppShoppingCarInfor();
				appShoppingCarInfor.setPkId(UUID.randomUUID().toString());
				appShoppingCarInfor.setGoodsId(appGoodsInfor.getGoodsId());
				appShoppingCarInfor.setGoodsName(appGoodsInfor.getGoodsName());
				appShoppingCarInfor.setMainImageUrl(appGoodsInfor.getMainImageUrl());
				appShoppingCarInfor.setPrice(appGoodsInfor.getPrice());
				appShoppingCarInfor.setDescribe(appGoodsInfor.getDescribe());
				appShoppingCarInfor.setCreateDate(new Date());
				//--------------------------------------------
				appShoppingCarInfor.setOperatorId(userId);
				
				appShoppingCarInforService.add(appShoppingCarInfor);
				json.setObj("ok");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else{
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("goodsId", productId);
			map.put("operatorId",userId);
			//先查出来这个商品购物车里有几条信息，然后取一条删除
			List<AppShoppingCarInfor> AppShoppingCarList = appShoppingCarInforService.queryByGoodsId(map);
			if(AppShoppingCarList.size() > 0) {
				AppShoppingCarInfor appShoppingCarInfor = AppShoppingCarList.get(0);
				try {
					appShoppingCarInforService.deleteById(appShoppingCarInfor.getPkId());
					json.setObj("ok");
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			
		}
		return json;
	}


}

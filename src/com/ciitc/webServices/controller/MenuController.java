package com.ciitc.webServices.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.model.MenuInfo;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.service.MenuService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.RoleDetailService;
import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;

/*******************************************************************************
 * 模块名称: MenuController 模块功能描述: 用户管理模块 创 建 人: 日 期: 修 改 人:
 ******************************************************************************/
@Controller
@RequestMapping("/menuController")
public class MenuController extends BaseController {

	@Autowired(required = false)
	private MenuService<MenuInfo> menuService;
	@Autowired(required = false)
    private RoleDetailService<RoleDetailInfo> roleDetailService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志

	/*************************************************************************
	 * 函数名: List 
	 * 功能描述: 跳转数据列表页面 
	 * 输入参数: 
	 * 返 回 值: 
	 * 异 常： 
	 * 创 建 人: 
	 * 日 期: 
	 * 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "menuList=key")
	public ModelAndView menuList() {
		return new ModelAndView("pages/sysManager/menu/menuList");
	}
	
	
	/*************************************************************************
	 * 函数名: menuTree 
	 * 功能描述: 查询数据列表 
	 * 输入参数:
	 * 返 回 值:
	 * 异 常：
	 * 创 建 人:
	 * 日 期: 
	 * 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "menuTree=key")
	public void menuTree(HttpServletRequest request,
			HttpServletResponse response, MenuInfo menu) throws Exception {

		try {
			List<Map> list = new ArrayList<Map>();
			list = tree(menu);

			JSONObject jObject = pageUtil.getJsonForPage(list, list.size());
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public List<Map> tree(MenuInfo menu) throws Exception {

		try {
			// menu 列表
			List<MenuInfo> list = new ArrayList<MenuInfo>();
			
			if (menu.getPkId() != null) {
				try {
					list = menuService.queryByParentId(menu.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					list = menuService.query();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			List<Map> treeList = new ArrayList<Map>();

			List<MenuInfo> childList = new ArrayList<MenuInfo>();
			for (MenuInfo menuInfo : list) {
				Map<String, Object> node = new HashMap<String, Object>();
				node.put("pkId", menuInfo.getPkId());
				node.put("menuName", menuInfo.getMenuName());
				node.put("menuPath", menuInfo.getMenuPath());
				node.put("menuOrder", menuInfo.getMenuOrder());
				node.put("menuGrade", menuInfo.getMenuGrade());
				if (menuInfo.getParentMenuId() != null
						&& !Globals.Menu_Level_ONE.equals(menuInfo.getParentMenuId())) {
					node.put("_parentId", menuInfo.getParentMenuId());
				} else {
					node.put("_parentId", "");
				}

				// 判断节点是否展开(open,closed)
				try {
					childList = menuService.queryByParentId(menuInfo.getPkId());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (childList != null && childList.size() > 0) {

					node.put("state", "closed");
				} else {
					node.put("state", "open");
				}
				treeList.add(node);
			}
			return treeList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	/*************************************************************************
	 * 函数名: saveUpdate 
	 * 功能描述: 保存 
	 * 输入参数: 
	 * 返 回 值: 
	 * 异 常： 
	 * 创 建 人: 
	 * 日 期: 
	 * 修 改 人:
	 *  日 期:
	 *************************************************************************/

	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, MenuInfo menuInfo)
			throws Exception {
		try {
			AjaxJson j = new AjaxJson();
			MenuInfo menu=null;
			
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(menuInfo.getMenuName()) || 
					checkChar.checkChar(menuInfo.getMenuGrade()) ||
					checkChar.checkChar(menuInfo.getParentMenuId()) ||
					checkChar.checkChar(menuInfo.getMenuPath()) ||
					checkChar.checkChar(menuInfo.getMenuOrder())
					
					){
				message = "存在不合法字符";
				j.setMsg(message);
				return j;
			}
			
			
			
			try {
				menu = menuService.queryById(menuInfo.getParentMenuId());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if(menu != null && menu.getMenuGrade() != "0"){
				int lev=Integer.parseInt(menu.getMenuGrade())+1;
				menuInfo.setMenuGrade(String.valueOf(lev));
			}else{
				menuInfo.setParentMenuId(Globals.Menu_Level_ONE);
			}
			String operateFlag = request.getParameter("operateFlag");
			if ("modify".equals(operateFlag)) {
				//如果上级菜单选择自己时，不更新
				if(menuInfo.getPkId().equals(menuInfo.getParentMenuId())){
					menuInfo.setParentMenuId("");
				}
				try {
					menuService.updateBySelective(menuInfo);
					operateLogService.addLog("菜单管理", "更新菜单日志", Globals.Log_Type_UPDATE);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					menuService.add(menuInfo);
					operateLogService.addLog("菜单管理", "保存菜单日志", Globals.Log_Type_INSERT);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			message = "保存成功";
			j.setMsg(message);
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*************************************************************************
	 * 函数名: delete 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String pkId) {
		
		try {
			AjaxJson json = new AjaxJson();
			
			// 日志用
			message = "菜单删除成功";
			
			// 删除节点是否为父节点
			List<MenuInfo> menus = null;

			// 删除权限时先删除权限与角色之间关联表信息
			List<RoleDetailInfo> roleMenus = null;
			try {
				menus = menuService.queryByParentId(pkId);
				roleMenus = roleDetailService.queryByMenuId(pkId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			
			if(menus != null && menus.size() > 0){
				json.setMsg("含有子节点无法删除");
				json.setObj("alert");
			} else {
				
				if (menus != null && roleMenus.size() > 0) {
					json.setMsg("菜单已分配无法删除");
					json.setObj("alert");
				} else {
					try {
						menuService.delete(pkId);
						operateLogService.addLog("菜单管理", "删除菜单日志", Globals.Log_Type_DEL);
						json.setObj("ok");
					} catch (Exception e) {
						e.printStackTrace();
					}
					/// TODO 系统日志追加
				}
			}
			return json;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	public List<Map> combotree(String parentId) throws Exception {
		
		try {
			// combotree
			List<MenuInfo> list = new ArrayList<MenuInfo>();
			
			try {
				list = menuService.queryByParentId(parentId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			List<Map> treeList = new ArrayList<Map>();
			for (MenuInfo menuInfo : list) {
				Map<String, Object> node = new HashMap<String, Object>();
				if(menuInfo.getParentMenuId().equals("0")){
					node.put("id", menuInfo.getPkId());
					node.put("text", menuInfo.getMenuName());
					childTree(node);
					treeList.add(node);
				}
			}
			return treeList;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	private void childTree(Map<String, Object> node){
		try {
			List<MenuInfo> childList = new ArrayList<MenuInfo>();
			List<Map> tree = new ArrayList<Map>();
			try {
				childList = menuService.queryByParentId(node.get("id"));
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			if (childList.size() > 0) {
				
				for(MenuInfo menu : childList){
					Map<String, Object> childNode = new HashMap<String, Object>();
					childNode.put("id", menu.getPkId());
					childNode.put("text", menu.getMenuName());
					tree.add(childNode);
				}
				node.put("children", tree);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "getCombotree=key")
	public void getCombotree(HttpServletRequest request,
			HttpServletResponse response,MenuInfo menu) throws Exception {

		try {
			List<Map> list = new ArrayList<Map>();
			list = combotree(menu.getPkId());

			JSONArray jObject = JSONArray.fromObject(list);
			response.setContentType("application/json");
			response.setHeader("Cache-Control", "no-store");
			try {
				PrintWriter pw = response.getWriter();
				pw.write(jObject.toString());
				pw.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(MenuInfo menuInfo, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			context.put("combotree_pkId", menuInfo.getPkId());
			if(menuInfo.getPkId() != null && !Globals.Menu_Level_ONE.equals(menuInfo.getPkId())){
				context.put("menuLevel", "1");
			}else{
				context.put("menuLevel", "0");
			}
			context.put("operateFlag", "add");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/menu/menu", context);
	}
	
	/*************************************************************************
	 * 函数名: modify 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(MenuInfo menuInfo, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			menuInfo = menuService.queryById(menuInfo.getPkId());
			context.put("menuInfo", menuInfo);
			context.put("combotree_pkId", menuInfo.getParentMenuId());
			context.put("operateFlag", "modify");
			if(menuInfo.getParentMenuId() != null && !Globals.Menu_Level_ONE.equals(menuInfo.getParentMenuId())){
				context.put("menuLevel", "1");
			}else{
				context.put("menuLevel", "0");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/menu/menu", context);
	}
	
	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

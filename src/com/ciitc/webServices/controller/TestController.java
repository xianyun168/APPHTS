package com.ciitc.webServices.controller;

import cn.com.ins.sinosoft.framework.base.util.StringUtil;
import com.ciitc.webServices.model.CPTtrustcontractReqV2;
import com.ciitc.webServices.util.MyDateUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTtrustcontractReqDao;
import com.ciitc.webServices.dao.CPcfResultDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTtrustcontractReq;
import com.ciitc.webServices.model.CPcfResult;
import com.ciitc.webServices.util.CrmFunctionUtil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
/**
 * @program: APPHTS
 * @description:
 * @author: zhengyunfei
 * @create: 2018-11-28 16:56
 **/
@Controller
@RequestMapping("/test")
public class TestController {
    Map<String, Object> map = new HashMap<String, Object>();
    @SuppressWarnings("rawtypes")
    private CPTtrustcontractReqDao CPTtrustcontractReqDao = (CPTtrustcontractReqDao) SpringContextUtil
            .getBean("CPTtrustcontractReqDao");
    @SuppressWarnings("rawtypes")
    private CPcfResultDao CPcfResultDao = (CPcfResultDao) SpringContextUtil.getBean("CPcfResultDao");
    @SuppressWarnings("rawtypes")
    private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");

    @RequestMapping(params = "a=key",method = RequestMethod.GET)
    @ResponseBody
    public Object showView() {
        String state = "";
        double positionMoney = 0.00;// 持仓总金额
        double chizongMoney = 0.00;// 持仓总收益
        double leijiMoney = 0.00;// 累计收益

        List<CPTtrustcontractReqV2> cPTtrustcontractReq = new ArrayList<CPTtrustcontractReqV2>();// 根据手机号查询产品信息
        List<CPTtrustcontractReqV2> chicanglist = new ArrayList<CPTtrustcontractReqV2>();// 查询持仓产品信息
        List<CPTtrustcontractReq> suoyoulist = new ArrayList<CPTtrustcontractReq>();// 查询持仓收益信息
        List<CPTtrustcontractReq> leijilist = new ArrayList<CPTtrustcontractReq>();// 查询累计收益信息
        // 请求Id
        String pkId = "72cef2d6-c762-44c8-8b35-72114b6dc24c";
        // 根据id查询客户Id
        AppUserInfor khId = appUserInforDao.querykhId(pkId);

        try {
            String customerId=khId.getCustomerId();
            cPTtrustcontractReq=CPTtrustcontractReqDao.getMyDeShouYiList(customerId);
            // 计算累计收益
            leijiMoney=CPTtrustcontractReqDao.getLeiJiShouYi(customerId);
            // 计算持仓总金额
            for (CPTtrustcontractReqV2 cpTtrustcontractReq2 : cPTtrustcontractReq) {
                if(!"已到期".equals(cpTtrustcontractReq2.getcStatus())) {
                    positionMoney += cpTtrustcontractReq2.getfContractsignbalance();
                }
            }
            // 计算持仓总收益
            chizongMoney=CPTtrustcontractReqDao.getChiCangShouYi(customerId);
            for (CPTtrustcontractReqV2 cpt : cPTtrustcontractReq) {
                if("已到期".equals(cpt.getcStatus())){
                    cpt.setDaoqiTian(cpt.getcStatus());
                }
                cpt.setZhuangtai(cpt.getcStatus());
                cpt.setMoney(cpt.getfContractsignbalance()+"");
                cpt.setProserialno(cpt.getlProserialno()+"");
                cpt.setFene(cpt.getMoney());
                cpt.setdCreatedate(cpt.getDefaultDate());
                String defaultDate=cpt.getDefaultDate();
                if(StringUtil.isNotEmpty(defaultDate)){
                    defaultDate=defaultDate.replace(".0","");
                }
				if(StringUtil.isNotEmpty(defaultDate)&&defaultDate.contains("-")){
					cpt.setDefaultDate(MyDateUtils.stringToLong(defaultDate,"yyyy-MM-dd HH:mm:ss")+"");
					cpt.setdAuditdate(MyDateUtils.stringToLong(defaultDate,"yyyy-MM-dd HH:mm:ss")+"");
				}else{
					cpt.setDefaultDate(defaultDate);
					cpt.setdAuditdate(defaultDate);
				}
				String dContractsigndate=cpt.getdContractsigndate();
				if(StringUtil.isNotEmpty(dContractsigndate)){
                    dContractsigndate=dContractsigndate.replace(".0","");
                }
				if(StringUtil.isNotEmpty(dContractsigndate)&&dContractsigndate.contains("-")){
					cpt.setdContractsigndate(MyDateUtils.stringToLong(dContractsigndate,"yyyy-MM-dd HH:mm:ss")+"");
				}else{
					cpt.setdContractsigndate(dContractsigndate);
				}

                if(cpt.getdContractsigndate().length()>10){
                    cpt.setQuerenDate(cpt.getdContractsigndate().substring(0,10));
                }else{
                    cpt.setQuerenDate(cpt.getdContractsigndate());
                }

                chicanglist.add(cpt);
            }

            state = "00";
        } catch (Exception e1) {
            e1.printStackTrace();
            state = "01";
        }

        DecimalFormat df = new DecimalFormat("0.00");
        map.put("state", state);
        map.put("chicanglist", chicanglist);
        map.put("leijiMoney", df.format(leijiMoney));
        map.put("chizongMoney", df.format(chizongMoney));
        map.put("positionMoney", df.format(positionMoney));
        return map;
    }
    @RequestMapping(params = "b=key",method = RequestMethod.GET)
    @ResponseBody
    public Object old() {
        String state = "";
        double positionMoney = 0.00;// 持仓总金额
        double chizongMoney = 0.00;// 持仓总收益
        double leijiMoney = 0.00;// 累计收益
        List<CPTtrustcontractReq> cPTtrustcontractReq = new ArrayList<CPTtrustcontractReq>();// 根据手机号查询产品信息
        List<CPTtrustcontractReq> chicanglist = new ArrayList<CPTtrustcontractReq>();// 查询持仓产品信息
        List<CPTtrustcontractReq> suoyoulist = new ArrayList<CPTtrustcontractReq>();// 查询持仓收益信息
        List<CPTtrustcontractReq> leijilist = new ArrayList<CPTtrustcontractReq>();// 查询累计收益信息
        // 请求Id
        String pkId = "72cef2d6-c762-44c8-8b35-72114b6dc24c";
        // 根据id查询客户Id
        AppUserInfor khId = appUserInforDao.querykhId(pkId);

        try {
            // 根据用户id查询购买产品合同信息
            cPTtrustcontractReq = CPTtrustcontractReqDao.queryChiCangList1(khId.getCustomerId());
            // 计算累计收益
            for (CPTtrustcontractReq cPTtrustcontractReq2 : cPTtrustcontractReq) {
                suoyoulist = CPTtrustcontractReqDao.queryChiCangMoney(cPTtrustcontractReq2.getcTrustcontractid());
                for (CPTtrustcontractReq cpTtrustcontractReq2 : suoyoulist) {
                    leijiMoney += cpTtrustcontractReq2.getAmount();//现金流
                }
            }
            for (CPTtrustcontractReq cpt : cPTtrustcontractReq) {
                /*
                 * if (cpt.getActualDate() == null) { // 如果到期时间为空，到期天数为-
                 * cpt.setDaoqiTian("-"); chicanglist.add(cpt); }
                 */
                if (cpt.getActualDate() != null && !"".equals(cpt.getActualDate())) {

                    // 根据当前时间与默认到期时间相减得到到期天数
                    Date time = new Date();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    String a = dateFormat.format(time);
                    Date newDate = dateFormat.parse(a);

                    Date oldTime = cpt.getActualDate();
                    String b = dateFormat.format(oldTime);
                    Date oldDate = dateFormat.parse(b);
                    if (newDate.getTime() <= oldDate.getTime()) {

                        Calendar cal = Calendar.getInstance();
                        cal.setTime(newDate);
                        long time1 = cal.getTimeInMillis();
                        //System.out.println(time1);
                        cal.setTime(oldDate);
                        long time2 = cal.getTimeInMillis();
                        //System.out.println(time2);
                        long between_days = (time2 - time1) / (1000 * 3600 * 24);
                        String days = String.valueOf(between_days);
                        cpt.setDaoqiTian(days);
                        cpt.setProserialno(cpt.getlProserialno().toString());
                        chicanglist.add(cpt);
                    }else {
                        cpt.setDaoqiTian("已到期");
                        cpt.setProserialno(cpt.getlProserialno().toString());
                        chicanglist.add(cpt);
                    }
                }
            }
            // 计算持仓总金额
            for (CPTtrustcontractReq cpTtrustcontractReq2 : chicanglist) {
                if(!"已到期".equals(cpTtrustcontractReq2.getDaoqiTian())) {
                    DecimalFormat df = new DecimalFormat("0.00");
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                    String qianshu = format.format(cpTtrustcontractReq2.getdContractsigndate());
                    // 签署日期与份额确认日期相同
                    cpTtrustcontractReq2.setQuerenDate(qianshu);
                    cpTtrustcontractReq2.setQianshu(qianshu);
                    cpTtrustcontractReq2.setZhuangtai("未到期");
                    String money = df.format(cpTtrustcontractReq2.getfContractsignbalance());
                    cpTtrustcontractReq2.setMoney(money);
                    // 确认份额与购买金额相同
                    int fene = (int) cpTtrustcontractReq2.getfContractsignbalance();
                    cpTtrustcontractReq2.setFene(fene);
                    positionMoney += cpTtrustcontractReq2.getfContractsignbalance();
                }
            }
            // 计算持仓总收益
            for (CPTtrustcontractReq cPTtrustcontractReq2 : chicanglist) {
                if(!"已到期".equals(cPTtrustcontractReq2.getDaoqiTian())) {
                    leijilist = CPTtrustcontractReqDao.queryChiCangMoney(cPTtrustcontractReq2.getcTrustcontractid());
                    for (CPTtrustcontractReq cpTtrustcontractReq3 : leijilist) {
                        chizongMoney += cpTtrustcontractReq3.getAmount();
                    }
                }
            }
            state = "00";
        } catch (Exception e1) {
            e1.printStackTrace();
            state = "01";
        }

        DecimalFormat df = new DecimalFormat("0.00");
        map.put("state", state);
        map.put("chicanglist", chicanglist);
        map.put("leijiMoney", df.format(leijiMoney));
        map.put("chizongMoney", df.format(chizongMoney));
        map.put("positionMoney", df.format(positionMoney));
        String jsons = JSONArray.fromObject(map).toString();
        return map;
    }
    @RequestMapping(params = "c=key",method = RequestMethod.GET)
    @ResponseBody
    public Object c() {
        String state = "";
        double positionMoney = 0.00;// 持仓总金额
        double chizongMoney = 0.00;// 持仓总收益
        double leijiMoney = 0.00;// 累计收益

        List<CPTtrustcontractReqV2> cPTtrustcontractReq = new ArrayList<CPTtrustcontractReqV2>();// 根据手机号查询产品信息
        List<CPTtrustcontractReqV2> chicanglist = new ArrayList<CPTtrustcontractReqV2>();// 查询持仓产品信息
        List<CPTtrustcontractReq> suoyoulist = new ArrayList<CPTtrustcontractReq>();// 查询持仓收益信息
        List<CPTtrustcontractReq> leijilist = new ArrayList<CPTtrustcontractReq>();// 查询累计收益信息
        // 请求Id
        String pkId = "72cef2d6-c762-44c8-8b35-72114b6dc24c";
        // 根据id查询客户Id
        AppUserInfor khId = appUserInforDao.querykhId(pkId);

        try {
            String customerId=khId.getCustomerId();
            cPTtrustcontractReq=CPTtrustcontractReqDao.getMyDeShouYiList(customerId);
            // 计算累计收益
            leijiMoney=CPTtrustcontractReqDao.getLeiJiShouYi(customerId);
            // 计算持仓总金额
            for (CPTtrustcontractReqV2 cpTtrustcontractReq2 : cPTtrustcontractReq) {
                if(!"已到期".equals(cpTtrustcontractReq2.getcStatus())) {
                    positionMoney += cpTtrustcontractReq2.getfContractsignbalance();
                }
            }
            // 计算持仓总收益
            chizongMoney=CPTtrustcontractReqDao.getChiCangShouYi(customerId);
            for (CPTtrustcontractReqV2 cpt : cPTtrustcontractReq) {
                if("已到期".equals(cpt.getcStatus())){
                    cpt.setDaoqiTian(cpt.getcStatus());
                }
                cpt.setZhuangtai(cpt.getcStatus());
                cpt.setProserialno(cpt.getlProserialno()+"");
                cpt.setdCreatedate(cpt.getDefaultDate());
                if(cpt.getdContractsigndate().length()>10){
                    cpt.setQuerenDate(cpt.getdContractsigndate().substring(0,10));
                }else{
                    cpt.setQuerenDate(cpt.getdContractsigndate());
                }

                chicanglist.add(cpt);
            }

            state = "00";
        } catch (Exception e1) {
            e1.printStackTrace();
            state = "01";
        }

        DecimalFormat df = new DecimalFormat("0.00");
        map.put("state", state);
        map.put("chicanglist", chicanglist);
        map.put("leijiMoney", df.format(leijiMoney));
        map.put("chizongMoney", df.format(chizongMoney));
        map.put("positionMoney", df.format(positionMoney));
        return map;
    }
    /**
     * 把long 转换成 日期 再转换成String类型
     */
    public static String transferLongToDate(String dateFormat, Long millSec) {
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        Date date = new Date(millSec);
        return sdf.format(date);
    }
    public static void main(String args[]){

    }
}

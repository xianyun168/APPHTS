package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;


/*******************************************************************************
 * 模块名称: EsbmmCodeTypeController 
 * 模块功能描述: 数据字典管理模块
 * 创 建 人: 
 * 日 期: 
 * 修 改 人: 
 ******************************************************************************/
@Controller
@RequestMapping("/esbmmCodeTypeController")
public class EsbmmCodeTypeController extends BaseController {

    @Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType>   esbmmCodeTypeService;
    @Autowired(required = false)
    private EsbmmCodeService<EsbmmCode>   esbmmCodeService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	  
	 /*************************************************************************
     *  函数名:   saveUpdate      
     *  功能描述: 保存
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	  
    @RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson save(HttpServletRequest request, EsbmmCodeType esbmmCodeType) throws Exception {
		try {
			AjaxJson j = new AjaxJson();
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(esbmmCodeType.getChineseName())||checkChar.checkChar(esbmmCodeType.getTypeName())){
				message = "存在不合法字符";
				j.setMsg(message);
				return j;
			}
			if (esbmmCodeType.getPkId() != null && !"".equals(esbmmCodeType.getPkId())) {
				 try {
					esbmmCodeTypeService.updateBySelective(esbmmCodeType);
					operateLogService.addLog("数据字典", "更新代码类型日志", Globals.Log_Type_UPDATE);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					esbmmCodeTypeService.add(esbmmCodeType);
					operateLogService.addLog("数据字典", "保存代码类型日志",
							Globals.Log_Type_INSERT);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			message = "保存成功";
			j.setMsg(message);
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	 /*************************************************************************
     *  函数名:   datagrid      
     *  功能描述: 查询数据字典树列表
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: myb
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
    @RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, EsbmmCodeType esbmmCodeType,Map<String, Object> paramMap) {
    	try {
			List<Map> treeList = new ArrayList<Map>();
			
			try {
				List<EsbmmCodeType> list=esbmmCodeTypeService.queryAll();
				//一级代码类型
				for(EsbmmCodeType codeType:list){
					Map<String, Object> node = new HashMap<String, Object>();
					String pkId=codeType.getPkId();
					node.put("pkId",codeType.getPkId());
					node.put("_parentId","");
					node.put("cnName",codeType.getChineseName());
					node.put("value", codeType.getTypeName());
					node.put("state", "closed");
					treeList.add(node);
					EsbmmCode code=new EsbmmCode();
					code.setCodeTypePkId(pkId);
					List<EsbmmCode> codes=esbmmCodeService.queryByTypeId(code);
					//子节点查询
					for(EsbmmCode  esbmmCode: codes){
						Map<String, Object> nod = new HashMap<String, Object>();
						nod.put("pkId", esbmmCode.getPkId());
						nod.put("cnName", esbmmCode.getChineseName());
						nod.put("value",esbmmCode.getCodeValue());
						nod.put("codeTypePkId",esbmmCode.getCodeTypePkId());
						nod.put("_parentId",esbmmCode.getCodeTypePkId());
						treeList.add(nod);
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			JSONObject jObject = pageUtil.getJsonForPage(treeList, treeList.size());
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	 /*************************************************************************
     *  函数名:   delete      
     *  功能描述: 删除列表
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String pkId) {
		try {
			/*try {
//			esbmmCodeService.deleteByTypeId(pkId);
//			esbmmCodeTypeService.delete(pkId);
				esbmmCodeTypeService.deletecascade(pkId, pkId);
				operateLogService.addLog("数据字典", "删除代码类型日志", Globals.Log_Type_DEL);
				
				} catch (Exception e) {
				e.printStackTrace();
			}*/
			AjaxJson json = new AjaxJson();
			
			//数据字典子节点
			List<EsbmmCode> codeList = null;
			
			//数据字典
			List<EsbmmCodeType> codeTypeList = null;
			
			try {
				codeList = esbmmCodeTypeService.queryCodeByTypeId(pkId); 
				
				codeTypeList = esbmmCodeTypeService.queryCodeTypeByTypeId(pkId);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			if(codeList != null && codeList.size()>0){
				json.setMsg("含有子节点无法删除");
				json.setObj("alert");
				json.setSuccess(false);
			}else {
				try {
					esbmmCodeTypeService.delete(pkId);
					operateLogService.addLog("数据字典", "删除代码类型日志", Globals.Log_Type_DEL);
					json.setObj("ok");
					json.setSuccess(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				/// TODO 系统日志追加
			}
			
			return json;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	 /*************************************************************************
     *  函数名:   add      
     *  功能描述: 跳转到增加页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add() {
		return new ModelAndView("pages/sysManager/dictionary/EsbmmCodeTypeAdd");
	}
	
	
	 /*************************************************************************
     *  函数名:   modify      
     *  功能描述: 跳转到修改页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(EsbmmCodeType  esbmmCodeType, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			esbmmCodeType = esbmmCodeTypeService.queryById(esbmmCodeType.getPkId());
			context.put("esbmmCodeType", esbmmCodeType);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/dictionary/EsbmmCodeTypeAdd", context);
	}
	
	
	 /*************************************************************************
     *  函数名:   List      
     *  功能描述: 跳转数据列表页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "esbmmCodeTypeList=key")
	public ModelAndView esbmmCodeTypeList() {
		return new ModelAndView("pages/sysManager/dictionary/EsbmmCodeTypeList");
	}

	/*************************************************************************
	 * 函数名: checkUserCode 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "checkExit=key")
	@ResponseBody
	public AjaxJson checkExit(HttpServletRequest request) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		AjaxJson json = new AjaxJson();
		try {
			paramMap.put("typeName", request.getParameter("typeName"));
			paramMap.put("chineseName", request.getParameter("chineseName"));
			
			List<EsbmmCodeType> list = esbmmCodeTypeService.checkCodeType(paramMap);
			if(list.size()==0){
				json.setObj("ok");
			}else{
				json.setObj("no");
				json.setMsg("代码类型或中文名称已存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}	
	
	


	
	
	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}



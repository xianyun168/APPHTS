package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppCustomerInfor;
import com.ciitc.webServices.model.AppGoodsInfor;
import com.ciitc.webServices.model.AppOrderDetails;
import com.ciitc.webServices.model.AppOrderInfor;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.AppCustomerInforService;
import com.ciitc.webServices.service.AppOrderDetailsService;
import com.ciitc.webServices.service.AppOrderInforService;
import com.ciitc.webServices.service.BatchAddService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;


/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年3月22日 下午2:45:00
 * 修 改 人: 
 ******************************************************************************/
@Controller
@RequestMapping("/appCustomerInforController")
public class AppCustomerInforController extends BaseController{

	@Autowired(required = false)
	private AppCustomerInforService<AppCustomerInfor> appCustomerInforService;
	@Autowired(required = false)
	private AppOrderDetailsService<AppOrderDetails> appOrderDetailsService;
	@Autowired(required = false)
	private AppOrderInforService<AppOrderInfor> orderInforService;
	@Autowired(required = false)
	private BatchAddService<AppUserInfor> batchAddService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	
	@RequestMapping(params = "appCustomerInforList=key")
	public ModelAndView appCustomerInforList(){
		return new ModelAndView("pages/shSale/appCustomerInforList");
	}
	
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request,HttpServletResponse response,AppCustomerInfor appCustomerInfor,BasePage basePage){
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			String date ="";
			if(null != appCustomerInfor.getCustomerTime()){
				date = time.format(appCustomerInfor.getCustomerTime());
			}
			paramMap.put("date", date);
			paramMap.put("appCustomerInfor", appCustomerInfor);
			List<AppCustomerInfor> list = new ArrayList<AppCustomerInfor>();
			try {
				list = appCustomerInforService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(params="add=key")
	public ModelAndView add(HttpServletRequest req){
		return new ModelAndView("pages/shSale/appCustomerInforAdd");
	}
	
	@RequestMapping(params = "orderList=key")
	public ModelAndView orderList() {
		
		return new ModelAndView("pages/shSale/orderList");
	}
	
	
	@RequestMapping(params = "goodsDatagrid=key")
	public void oraderDatagrid(HttpServletRequest request,HttpServletResponse response,AppOrderDetails appOrderDetails,BasePage basePage){
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("appOrderDetails", appOrderDetails);
			List<AppOrderDetails> list = new ArrayList<AppOrderDetails>();
			try {
				list = appOrderDetailsService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@RequestMapping(params = "goodsList=key")
	public ModelAndView goodsList(String orderId) {
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("orderId", orderId);
		return new ModelAndView("pages/shSale/goodsList",context);
	}

	
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request,HttpServletResponse response,AppCustomerInfor appCustomerInfor) throws Exception{
		AjaxJson json=new AjaxJson();
		if(appCustomerInfor.getPkId()!=null && !"".equals(appCustomerInfor.getPkId())){
			try {
				appCustomerInforService.update(appCustomerInfor);
			} catch (Exception e) {
				e.printStackTrace();
			}
			message = "编辑成功";
		}else{
			//String uuid = UUID.randomUUID().toString();
			String goodsNames = request.getParameter("goodsNames");
			AppOrderInfor appOrderInfor = orderInforService.queryOrderId(appCustomerInfor.getOrderId());
			AppUserInfor appUserInfor = batchAddService.queryByPkId(appOrderInfor.getPurchaseId());
			Random ra = new Random();
			String customerId ="S";
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
			customerId += sdf.format(new Date());
			for(int i = 0;i<2 ; i++) {
				customerId +=Integer.toString(ra.nextInt(10));
			}
			UserInfo u = ResourceUtil.getSessionUserName();
			appCustomerInfor.setCustomerId(customerId);
			appCustomerInfor.setCustomerTime(new Date());
			appCustomerInfor.setCustomerName(appUserInfor.getUserName());
			appCustomerInfor.setCreateDate(new Date());
			appCustomerInfor.setOrderPrice(appOrderInfor.getOrderprice().intValue());
			appCustomerInfor.setOperatorId(u.getPkId());
			appCustomerInfor.setGoodsIds(goodsNames);
			appCustomerInforService.add(appCustomerInfor);
			message="保存成功";
		}
		json.setMsg(message);
		return json;
	}
	
	@RequestMapping(params = "afterUpdate=key")
	public ModelAndView afterUpdate(String pkId) {
		Map<String, Object> context = new HashMap<String, Object>();
		try {
			AppCustomerInfor appCustomerInfor = appCustomerInforService.queryById(pkId);
			SimpleDateFormat time=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
			String customerDate = time.format(appCustomerInfor.getCustomerTime());
			context.put("appCustomerInfor", appCustomerInfor);
			context.put("customerDate", customerDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView("pages/shSale/appCustomerInforAdd",context);
	}
	
	 // 向页面返回的信息
		private String message;

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}
}

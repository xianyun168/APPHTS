package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.PerformanceGoals;
import com.ciitc.webServices.model.RankInfo;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.PerformanceGoalsService;
import com.ciitc.webServices.service.RankInfoService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

/*******************************************************************************
 * 模块名称: rankInfoController 地址功能描述: 职级管理 创 建 人: 日 期: 修 改 人:任之阳
 ******************************************************************************/
@Controller
@RequestMapping("/rankInfoController")
public class RankInfoController extends BaseController {

	@Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;
	@Autowired(required = false)
	private RankInfoService<RankInfo> rankInfoService;
	@Autowired(required = false)
	private PerformanceGoalsService<PerformanceGoals> performanceGoalsService;
	@Autowired(required = false)
	private EsbmmCodeService<?> EsbmmCodeService;
	/**
	 * 翻译数据字典
	 */

	public List<RankInfo> translate(List<RankInfo> list) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		EsbmmCode code = new EsbmmCode();
		for (RankInfo s : list) {
			if (s != null && !s.equals("")) {
				//是否到期
				paramMap.put("typeName", "rank");
				paramMap.put("codeValue", s.getRank());
				code = EsbmmCodeService.getCodeName(paramMap);
				s.setRank(code.getChineseName());
			}
		}
		return list;
	}
	public List<PerformanceGoals> translate2(List<PerformanceGoals> list) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		EsbmmCode code = new EsbmmCode();
		for (PerformanceGoals s : list) {
			if (s != null && !s.equals("")) {
				//是否到期
				paramMap.put("typeName", "rank");
				paramMap.put("codeValue", s.getRank());
				code = EsbmmCodeService.getCodeName(paramMap);
				s.setRank(code.getChineseName());
			}
		}
		return list;
	}
	@RequestMapping(params ="rankInfoList=key")
	public ModelAndView rankList() {
		Map<String, Object> context =getRootMap();
		//取数据字典里的职级
		List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("rank");
		EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
		EsbmmCode codePkId = new EsbmmCode();
		codePkId.setCodeTypePkId(codeTypePkId.getPkId());
		//根据codeTypePkId查找对应的类型
		List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
		context.put("codeList", codeList);
		return new ModelAndView("pages/sysManager/rankInfo/rankInfoList",context);
	}
	
	/*************************************************************************
	 * 函数名: modify=key 功能描述: 跳转到修改页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(PerformanceGoals performanceGoals, HttpServletRequest req) {
		Map<String, Object> context =getRootMap();
		try {
			performanceGoals=performanceGoalsService.queryById(performanceGoals.getPkId());
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String startTime = format.format(performanceGoals.getAssessmentStartTime());
			String endTime = format.format(performanceGoals.getAssessmentEndTime());
			context.put("performanceGoals", performanceGoals);
			context.put("startTime", startTime);
			context.put("endTime", endTime);
			//取数据字典里的职级
			List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("rank");
			EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
			EsbmmCode codePkId = new EsbmmCode();
			codePkId.setCodeTypePkId(codeTypePkId.getPkId());
			//根据codeTypePkId查找对应的类型
			List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
			for (EsbmmCode esbmmCode : codeList) {
				if(esbmmCode.getCodeValue().equals(performanceGoals.getRank())) {
					context.put("rank", esbmmCode.getChineseName());
				}
			}
			/*if(performanceGoals.getRank().equals("80")){
				context.put("rank", "副总经理6(内部晋升)");
			}else if(performanceGoals.getRank().equals("63")){
				context.put("rank", "副总经理5(内部晋升) ");
			}else if(performanceGoals.getRank().equals("45")){
				context.put("rank", "副总经理4");
			}else if(performanceGoals.getRank().equals("52")){
				context.put("rank", "副总经理3");
			} else if(performanceGoals.getRank().equals("56")){
				context.put("rank", "副总经理2");
			}else if(performanceGoals.getRank().equals("41")){
				context.put("rank", "副总经理1");
			}else if(performanceGoals.getRank().equals("60")){
				context.put("rank", "部门经理5");
			}else if(performanceGoals.getRank().equals("75")){
				context.put("rank", "部门经理4");
			}if(performanceGoals.getRank().equals("58")){
				context.put("rank", "部门经理3");
			}else if(performanceGoals.getRank().equals("43")){
				context.put("rank", "部门经理2");
			}if(performanceGoals.getRank().equals("55")){
				context.put("rank", "部门经理1");
			}if(performanceGoals.getRank().equals("59")){
				context.put("rank", "代理部门经理");
			}if(performanceGoals.getRank().equals("88")){
				context.put("rank", "首席理财管4(内部晋升)");
			}if(performanceGoals.getRank().equals("87")){
				context.put("rank", "首席理财管3(内部晋升)");
			}if(performanceGoals.getRank().equals("86")){
				context.put("rank", "首席理财管2(内部晋升)");
			}if(performanceGoals.getRank().equals("85")){
				context.put("rank", "首席理财管1(内部晋升)");
			}if(performanceGoals.getRank().equals("76")){
				context.put("rank", "资深理财师4");
			}if(performanceGoals.getRank().equals("51")){
				context.put("rank", "资深理财师3");
			}if(performanceGoals.getRank().equals("50")){
				context.put("rank", "资深理财师2");
			}if(performanceGoals.getRank().equals("48")){
				context.put("rank", "资深理财师1");
			}if(performanceGoals.getRank().equals("57")){
				context.put("rank", "理财师5");
			}if(performanceGoals.getRank().equals("46")){
				context.put("rank", "理财师4");
			}if(performanceGoals.getRank().equals("53")){
				context.put("rank", "理财师3");
			}if(performanceGoals.getRank().equals("49")){
				context.put("rank", "理财2");
			}if(performanceGoals.getRank().equals("44")){
				context.put("rank", "理财师1");
			}if(performanceGoals.getRank().equals("47")){
				context.put("rank", "助理理财师");
			}if(performanceGoals.getRank().equals("42")){
				context.put("rank", "助理理财师(考察期)");
			}*/
	
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return new ModelAndView("pages/sysManager/rankInfo/modify", context);
	}
	/*************************************************************************
	 * 函数名: datagrid 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, BasePage basePage,RankInfo rankInfo) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("rankInfo", rankInfo);
			List<RankInfo> list = new ArrayList<RankInfo>();
			try {
				list = rankInfoService.queryByList(paramMap);
				list = translate(list);
				
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(HttpServletRequest req, RankInfo rankInfo,String pkId) {
		String ranks = rankInfoService.queryById(pkId);
		Map<String, Object> context = getRootMap();
		List<PerformanceGoals> list = new ArrayList<PerformanceGoals>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		list = performanceGoalsService.queryList(ranks);

		for (int i = 0; i < list.size(); i++) {
			String startTime = format.format(list.get(i).getAssessmentStartTime());
			String endTime = format.format(list.get(i).getAssessmentEndTime());
           list.get(i).setRemark1(startTime);
           list.get(i).setRemark2(endTime);
		}
		context.put("list", list);
		context.put("ranks", ranks);
		//取数据字典里的职级
		List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("rank");
		EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
		EsbmmCode codePkId = new EsbmmCode();
		codePkId.setCodeTypePkId(codeTypePkId.getPkId());
		//根据codeTypePkId查找对应的类型
		List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
		for (EsbmmCode esbmmCode : codeList) {
			if(esbmmCode.getCodeValue().equals(ranks)) {
				context.put("rank", esbmmCode.getChineseName());
			}
		}
		/*if(ranks.equals("80")){
			context.put("rank", "副总经理6(内部晋升)");
		}else if(ranks.equals("63")){
			context.put("rank", "副总经理5(内部晋升)");
		}else if(ranks.equals("45")){
			context.put("rank", "副总经理4");
		}else if(ranks.equals("52")){
			context.put("rank", "副总经理3");
		} else if(ranks.equals("56")){
			context.put("rank", "副总经理2");
		}else if(ranks.equals("41")){
			context.put("rank", "副总经理1");
		}else if(ranks.equals("60")){
			context.put("rank", "部门经理5");
		}else if(ranks.equals("75")){
			context.put("rank", "部门经理4");
		}if(ranks.equals("58")){
			context.put("rank", "部门经理3");
		}else if(ranks.equals("43")){
			context.put("rank", "部门经理2");
		}else if(ranks.equals("55")){
			context.put("rank", "部门经理1");
		}else if(ranks.equals("59")){
			context.put("rank", "代理部门经理");
		}else if(ranks.equals("88")){
			context.put("rank", "首席理财管4(内部晋升)");
		}else if(ranks.equals("87")){
			context.put("rank", "首席理财管3(内部晋升)");
		}else if(ranks.equals("86")){
			context.put("rank", "首席理财管2(内部晋升)");
		}else if(ranks.equals("85")){
			context.put("rank", "首席理财管1(内部晋升)");
		}else if(ranks.equals("76")){
			context.put("rank", "资深理财师4");
		}else if(ranks.equals("51")){
			context.put("rank", "资深理财师3");
		}else if(ranks.equals("50")){
			context.put("rank", "资深理财师2");
		}else if(ranks.equals("48")){
			context.put("rank", "资深理财师1");
		}else if(ranks.equals("57")){
			context.put("rank", "理财师5");
		}else if(ranks.equals("46")){
			context.put("rank", "理财师4 ");
		}else if(ranks.equals("53")){
			context.put("rank", "理财师3");
		}else if(ranks.equals("49")){
			context.put("rank", "理财2");
		}else if(ranks.equals("44")){
			context.put("rank", "理财师1");
		}else if(ranks.equals("47")){
			context.put("rank", "助理理财师");
		}else if(ranks.equals("42")){
			context.put("rank", "助理理财师(考察期)");
		}*/
		return new ModelAndView("pages/sysManager/rankInfo/rankUpdate",context);
	}//pages/sysManager/rankInfo/rankInfoList
	
	/*************************************************************************
	 * 函数名:Details 功能描述: 跳转到详情页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "details=key")
	public ModelAndView details(HttpServletRequest req, RankInfo rankInfo,String pkId) {
		String ranks = rankInfoService.queryById(pkId);
		Map<String, Object> context = getRootMap();
		List<PerformanceGoals> list = new ArrayList<PerformanceGoals>();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		list = performanceGoalsService.queryList(ranks);

		for (int i = 0; i < list.size(); i++) {
			String startTime = format.format(list.get(i).getAssessmentStartTime());
			String endTime = format.format(list.get(i).getAssessmentEndTime());
           list.get(i).setRemark1(startTime);
           list.get(i).setRemark2(endTime);
           
		}
		list = translate2(list);
		context.put("list", list);
		context.put("ranks", ranks);
		return new ModelAndView("pages/sysManager/rankInfo/rankDetails",context);
	}
	
	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 保存 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, RankInfo rankInfo,double target,Date assessmentStartTime,Date assessmentEndTime,String rank,String pkId,PerformanceGoals performanceGoals)throws Exception {
		AjaxJson j = new AjaxJson();
		//如果PkId不为空,则进行编辑,如果为空,则保存数据
	
			try {
				PerformanceGoals performanceGoals1 = new PerformanceGoals();
				String ids = UUID.randomUUID().toString();
				performanceGoals1.setPkId(ids);
				performanceGoals1.setRank(rank);
				performanceGoals1.setAssessmentStartTime(assessmentStartTime);
				performanceGoals1.setAssessmentEndTime(assessmentEndTime);
				performanceGoals1.setTarget(target);
				performanceGoalsService.insert(performanceGoals1);
				
				List<PerformanceGoals> list = new ArrayList<PerformanceGoals>();
				list = performanceGoalsService.selectById(ids);
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				for (int i = 0; i < list.size(); i++) {
					String startTime = format.format(list.get(i).getAssessmentStartTime());
					String endTime = format.format(list.get(i).getAssessmentEndTime());
		           list.get(i).setRemark1(startTime);
		           list.get(i).setRemark2(endTime);
				}
			j.setObj(list);
			message="保存成功";
			} catch (Exception e) {
				message = "保存失败";
				e.printStackTrace();
				j.setMsg(message);
				return j;
			
		}
				j.setMsg(message);

			return j;
		}
	
	
	
	
	
	
	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 保存 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/

	@RequestMapping(params = "update=key")
	@ResponseBody
	public AjaxJson update(HttpServletRequest request,String pkId,PerformanceGoals performanceGoals)throws Exception {
		AjaxJson j = new AjaxJson();
		try{
		//PerformanceGoals add =performanceGoalsService.queryById(pkId);
		
		performanceGoalsService.updateBySelective(performanceGoals);
		message = "修改成功";
		
		List<PerformanceGoals> list = new ArrayList<PerformanceGoals>();
		list = performanceGoalsService.queryList(performanceGoals.getRank());
		
	     j.setObj(list);
		} catch (Exception e) {
			e.printStackTrace();
			message = "修改失败";
		    }
	
		j.setMsg(message);

			return j;
		}
	
	
	
	
	
	
	
	/*************************************************************************
	* 函数名: delete 功能描述: 跳转到增加页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	*************************************************************************/
	@RequestMapping(params="delete")
	@ResponseBody
	public AjaxJson delete(String pkId,String rank){

			AjaxJson ajaxJson =new AjaxJson();   
			try {
				performanceGoalsService.delete(pkId);
				ajaxJson.setMsg("ok");
				
				List<PerformanceGoals> list = new ArrayList<PerformanceGoals>();
				list = performanceGoalsService.selectByRank(rank);
				
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				for (int i = 0; i < list.size(); i++) {
					String startTime = format.format(list.get(i).getAssessmentStartTime());
					String endTime = format.format(list.get(i).getAssessmentEndTime());
		           list.get(i).setRemark1(startTime);
		           list.get(i).setRemark2(endTime);
				}
				
				ajaxJson.setObj(list);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			return ajaxJson;
		
	} 
	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}

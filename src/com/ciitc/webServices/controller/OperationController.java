package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.Operation;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.vo.CodeFactoryVo;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.OperationService;
import com.ciitc.webServices.service.RoleDetailService;
import com.ciitc.webServices.util.pageUtil;


/*******************************************************************************
 * 模块名称: OperationController 
 * 模块功能描述: 用户管理模块 
 * 创 建 人: 
 * 日 期: 
 * 修 改 人: 
 ******************************************************************************/
@Controller
@RequestMapping("/operationController")
public class OperationController extends BaseController {

    @Autowired(required = false)
	private OperationService<Operation>   operationService;
    @Autowired(required = false)
    private RoleDetailService<RoleDetailInfo> roleDetailService;
    @Autowired(required = false)
    private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	  
	 /*************************************************************************
     *  函数名:   saveUpdate      
     *  功能描述: 保存
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	  
    @RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, Operation operation) throws Exception {
		try {
			AjaxJson j = new AjaxJson();
			if (operation.getPkId() != null && !"".equals(operation.getPkId())) {
				 try {
					operationService.updateBySelective(operation);
					 operateLogService.addLog("菜单管理", "更新按钮设置日志", Globals.Log_Type_UPDATE);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				try {
					operationService.add(operation);
					 operateLogService.addLog("菜单管理", "保存按钮设置日志", Globals.Log_Type_INSERT);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			message = "保存成功";
			j.setMsg(message);
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	 /*************************************************************************
     *  函数名:   datagrid      
     *  功能描述: 查询数据列表
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
    @RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, Operation operation, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("operation", operation);
			List<Operation> list = new ArrayList<Operation>();
			try {
				list = operationService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	 /*************************************************************************
     *  函数名:   delete      
     *  功能描述: 查询数据列表
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String pkId) {

		try {
			operationService.delete(pkId);
			operateLogService.addLog("菜单管理", "删除按钮设置日志", Globals.Log_Type_DEL);
			} catch (Exception e) {
			e.printStackTrace();
		}
		AjaxJson json = new AjaxJson();
		json.setObj("ok");
		return json;
	}
	
	 /*************************************************************************
     *  函数名:   add      
     *  功能描述: 跳转到增加页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(Operation  operation) {
		
		Map<String, Object> context = getRootMap();
		context.put("typeName", "operation");
		List<CodeFactoryVo> list = new ArrayList<CodeFactoryVo>();
		try {
			list = esbmmCodeTypeService.getCodeFactory(context);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		context.put("list", list);
		context.put("operation", operation);
		return new ModelAndView("pages/sysManager/menu/operation", context);
	}
	
	
	 /*************************************************************************
     *  函数名:   modify      
     *  功能描述: 跳转到修改页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(Operation  operation, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			operation = operationService.queryById(operation.getPkId());
			context.put("operation", operation);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/menu/operation", context);
	}
	
	
	 /*************************************************************************
     *  函数名:   List      
     *  功能描述: 跳转数据列表页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "operationList=key")
	public ModelAndView operationList(HttpServletRequest request,String menuId) {
		Map<String, Object> context = getRootMap();
		context.put("menuId", menuId);
		return new ModelAndView("pages/sysManager/menu/operationList",context);
	}
	
	
	/**
	 * 按钮权限展示
	 * @param request
	 * @param functionId
	 * @param roleId
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(params = "operationListForFunction=key")
	public ModelAndView operationListForFunction(HttpServletRequest request,String menuId,String roleId) throws Exception {	

		try {
			Map<String, Object> context = getRootMap();

			List<Operation> operationList = new ArrayList<Operation>();
			try {
				operationList = operationService.queryByMenuId(menuId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<RoleDetailInfo> roleMenuList = new ArrayList<RoleDetailInfo>();
			try {
				roleMenuList = roleDetailService.queryByRoleIdAndMenuId(roleId,menuId);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Set<String> operationCodes = new HashSet<String>();
			
			if (null != roleMenuList && roleMenuList.size() > 0) {
				RoleDetailInfo roleMenu = roleMenuList.get(0);
				if (null != roleMenu.getDescription()) {
					String[] operationArry = roleMenu.getDescription().split(",");
					for (int i = 0; i < operationArry.length; i++) {
						operationCodes.add(operationArry[i]);
					}
				}
			}
			context.put("menuId", menuId);
			context.put("operationList", operationList);
			context.put("operationcodes", operationCodes);
			return new ModelAndView("pages/sysManager/role/operationListForFunction",context);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	

	
	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}



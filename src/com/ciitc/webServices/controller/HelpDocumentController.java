package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.HelpDocument;
import com.ciitc.webServices.model.KnowledgeBase;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.HelpDocumentService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

/**
 * 帮助文档管理模块
 * @author tdl
 *
 */
@Controller
@RequestMapping("/helpDocumentController")
public class HelpDocumentController extends BaseController {
	@Autowired(required = false)
	private HelpDocumentService<KnowledgeBase> helpDocumentService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志

	/*
	 * 跳转到列表显示页面
	 */
	@RequestMapping(params = "knowledgeBaseList=key")
	public ModelAndView knowledgeBaseList() {
		return new ModelAndView("pages/helpDocument/helpDocumentList");
	}
	/*
	 * 帮助文档列表查询显示
	 */
	@RequestMapping(params = "getHelpDocumentList=key")
	public void getHelpDocumentList(HttpServletResponse response,HttpServletRequest request,
			BasePage basePage,HelpDocument helpDocument) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		/*paramMap.put("title", helpDocument.getTitle());
		paramMap.put("startDate", helpDocument.getStartDate());
		paramMap.put("endDate", helpDocument.getEndDate());*/
		//helpDocument.setTitle("%"+helpDocument.getTitle()+"%");
		paramMap.put("helpDocument", helpDocument);
		List<KnowledgeBase> list = new ArrayList<KnowledgeBase>();
		try {
			list = helpDocumentService.queryByList(paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list,cnt);
		pageUtil.responseDatagrid(response, jObject);
	}
	/*
	 * 跳转到新增页面
	 */
	@RequestMapping(params = "helpDocumentAdd=key")
	public ModelAndView knowledgeBaseAdd() {
		return new ModelAndView("pages/helpDocument/helpDocumentAdd");
	}
	/*
	 * 检查标题是否已经存在
	 */
	@RequestMapping(params="checkCode=key")
	@ResponseBody
	public AjaxJson checkCode(HttpServletRequest request,HttpServletResponse response, HelpDocument helpDocument,String pkId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		AjaxJson json = new AjaxJson();
		paramMap.put("title", helpDocument.getTitle());
		//新增时检查标题是否已经存在
		if(null == pkId || "".equals(pkId)) {
			try {
				List<HelpDocument> list = helpDocumentService.queryByTitle(paramMap);
				if(list.size()==0){
					json.setObj("ok");
				}else{
					json.setObj("no");
					json.setMsg("标题已存在");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else {
			//修改时检查标题是否已经存在
			List<HelpDocument> list = helpDocumentService.queryByTitle(paramMap);
			if(list.size()==0) {
				json.setObj("ok");
			}else {
				for (HelpDocument helpDocument1 : list) {
					if(helpDocument1.getPkId().equals(pkId)) {
						json.setObj("ok");
					}else {
						json.setObj("no");
						json.setMsg("标题已存在");
					}
				}
			}
			
		}
		
		return json;
	}
	/*
	 * 保存新增知识条目
	 */
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, HttpServletResponse response,
			HelpDocument helpDocument){
		//String title,String content,String pkId
		AjaxJson j = new AjaxJson();
		//获取当前登录对象
		UserInfo user = ResourceUtil.getSessionUserName();
		//生成一个uuid字符串
		String uuid = UUID.randomUUID().toString();
		try {
			//检查字符是否合法
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(helpDocument.getTitle()) || checkChar.checkChar(helpDocument.getTitle())){
				j.setMsg("存在不合法字符");
				return j;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//判断新增还是修改
		if(null == helpDocument.getPkId() || "".equals(helpDocument.getPkId())) {
			HelpDocument helpDocument1 = new HelpDocument();
			helpDocument1.setTitle(helpDocument.getTitle());
			helpDocument1.setContent(helpDocument.getContent());
			helpDocument1.setPublisher(user.getUserName());
			helpDocument1.setCounts(0);
			helpDocument1.setPkId(uuid);
			helpDocument1.setCreatedDate(new Date());
			helpDocumentService.insertSelective(helpDocument1);
			operateLogService.addLog("帮助文档", "新增帮助文档", Globals.Log_Type_INSERT);
			j.setObj("ok");
			j.setMsg("保存成功");
		}else {
			HelpDocument helpDocument1 = new HelpDocument();
			helpDocument1.setPkId(helpDocument.getPkId());
			helpDocument1.setTitle(helpDocument.getTitle());
			helpDocument1.setContent(helpDocument.getContent());
			helpDocument1.setUpdatedDate(new Date());
			helpDocumentService.updateByPrimaryKeySelective(helpDocument1);
			operateLogService.addLog("帮助文档", "修改帮助文档", Globals.Log_Type_UPDATE);
			j.setObj("ok");
			j.setMsg("修改成功");
		}
		
		return j;	
	}
	/*
	 * 跳转到修改页面
	 */
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(HttpServletRequest request,HttpServletResponse response,HelpDocument helpDocument) {
		Map<String, Object> context = getRootMap();
		try {
			helpDocument = helpDocumentService.selectByPrimaryKey(helpDocument.getPkId());
			context.put("helpDocument", helpDocument);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/helpDocument/helpDocumentAdd",context);
	}
	/*
	 * 删除知识条目
	 */
	@RequestMapping(params="delete=key")
	@ResponseBody
	public AjaxJson delete(HttpServletRequest request,HttpServletResponse response,String pkId){
		AjaxJson json = new AjaxJson();
		try {
			helpDocumentService.deleteByPrimaryKey(pkId);
			operateLogService.addLog("帮助文档", "删除帮助文档", Globals.Log_Type_DEL);
			json.setObj("ok");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}
	/*
	 * 跳转到详情页面details
	 */
	@RequestMapping(params = "details=key")
	public ModelAndView details(HttpServletRequest request,HttpServletResponse response,HelpDocument helpDocument) {
		Map<String, Object> context = getRootMap();
		try {
			helpDocument = helpDocumentService.selectByPrimaryKey(helpDocument.getPkId());
			//时间格式转换
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			context.put("createdDate", sdf.format(helpDocument.getCreatedDate()));
			context.put("helpDocument", helpDocument);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/helpDocument/helpDocumentDel",context);
	}
}

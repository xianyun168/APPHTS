package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.entity.SessionInfo;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.util.MethodUtil;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;

import com.ciitc.webServices.model.Depart;
import com.ciitc.webServices.model.MenuInfo;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.RoleDepartInfo;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.RoleInfo;
import com.ciitc.webServices.model.SysPermissionInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.MenuService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.RoleDepartService;
import com.ciitc.webServices.service.RoleDetailService;
import com.ciitc.webServices.service.RoleService;
import com.ciitc.webServices.service.SysPermissionInfoService;
import com.ciitc.webServices.service.UserInfoService;
import com.ciitc.webServices.util.NumberComparator;


/**
 * 登陆初始化控制器
 */
@Controller
@RequestMapping("/loginController")
public class LoginController {
    @Autowired(required = false)
	private UserInfoService<UserInfo>   userInfoService;
    @Autowired(required = false)
	private RoleService<RoleInfo>   roleService;
    @Autowired(required = false)
    private RoleDetailService<RoleDetailInfo> roleDetailService;
    @Autowired(required = false)
	private MenuService<MenuInfo> menuService;
    @Autowired(required = false)
	private SysPermissionInfoService<SysPermissionInfo>   sysPermissionInfoService;
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
    @Autowired(required = false)
    private RoleDepartService<RoleDepartInfo> roleDepartService;
    
	private String message = null;

	/**
	 * 检查用户名称
	 * 
	 * @param user
	 * @param req
	 * @return
	 */
	@RequestMapping(params = "checkuser=key")
	@ResponseBody
	public AjaxJson checkuser(UserInfo user, HttpServletRequest req,String code) {
		
		try {
			HttpSession session = req.getSession();
			AjaxJson j = new AjaxJson();
			if(user.getUserCode()==null){
				j.setMsg("操作失败");
				return j;
			}
			List<UserInfo> list = null;
			try {
				//密码添加md5加密
				if(user!=null&&!"".equals(user.getPkId())){
					String password =MethodUtil.MD5(user.getPassword());
					list = userInfoService.checkUserExits(user.getUserCode(),password);
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			UserInfo u = null;
			if(list != null && list.size() > 0){
				u = list.get(0);
			}
			
			String vcode = (String) req.getSession().getAttribute("randomStr");
			if(vcode==null){
				session.removeAttribute("randomStr");
				j.setMsg("验证码错误!");
				j.setSuccess(false);
				return j;
			}else if(!vcode.equalsIgnoreCase(code)){
				session.removeAttribute("randomStr");
				j.setMsg("验证码错误!");
				j.setSuccess(false);
				return j;
			}
			if (u != null) {
				message = "用户: " + u.getUserName() +"登录成功";
				SessionInfo sessionInfo = new SessionInfo();
				sessionInfo.setUser(u);
				session.setMaxInactiveInterval(60 * 30);
				session.setAttribute(Globals.USER_SESSION, sessionInfo);
				u.setLastloginDate(new Date());
				try {
					userInfoService.updateBySelective(u);
					operateLogService.addLog("用户登陆", message, Globals.Log_Type_LOGIN);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				session.removeAttribute("randomStr");
				j.setMsg("用户名或密码错误!");
				j.setSuccess(false);
			}
			
			session.removeAttribute("randomStr");	
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 用户登录
	 * 
	 * @param user
	 * @param request
	 * @param session
	 * @return
	 */
	@RequestMapping(params = "login=key")
	public ModelAndView login(HttpServletRequest request) {
		
		try {
			// 定义跳转页面
			String page = "";
			// session获取 user
			UserInfo user = ResourceUtil.getSessionUserName();
//		UserInfo  user= new UserInfo();
//		String userId ="c64fe1a8-1344-4800-b690-e53cba459b82";
//		user.setPkId(userId);
//		try {
//			HttpSession session = request.getSession();
//			user = userInfoService.queryById(user.getPkId());
//			
//			SessionInfo sessionInfo = new SessionInfo();
//			sessionInfo.setUser(user);
//			session.setMaxInactiveInterval(60 * 30);
//			session.setAttribute(Globals.USER_SESSION, sessionInfo);
//		} catch (Exception e) {
//		}
			

			String roles = "";
			if (user != null) {
				
				List<SysPermissionInfo> roleUsers=null;
				try {
					roleUsers = sysPermissionInfoService.queryByUserPkId(user.getPkId());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				for (SysPermissionInfo ru : roleUsers) {
					RoleInfo role = new RoleInfo();
					try {
						role = roleService.queryById(ru.getRolePkId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					roles += role.getRoleName()+"," ;
				}
				if(roles.length()>0){
					roles = roles.substring(0,roles.length()-1);
				}

				request.setAttribute("roleName", roles);
				request.setAttribute("userName", user.getUserName());
				request.setAttribute("userCode", user.getUserCode());
//			String indexStyle = "default";
//			Cookie[] cookies = request.getCookies();
//			for (Cookie cookie : cookies) {
//				if(cookie==null || StringUtils.isEmpty(cookie.getName())){
//					continue;
//				}
//			}
				
				
				HttpSession session = request.getSession();
				
				if (session.getAttributeNames().hasMoreElements()) {
					SessionInfo sessionInfo = (SessionInfo) session.getAttribute(Globals.USER_SESSION);
					if (sessionInfo != null) {
						sessionInfo.setAuthority(roles);
						//sessionInfo.setDeparts(departs());
						session.setAttribute(Globals.USER_SESSION, sessionInfo);
					}

				}
				
				int menuLimit = left(request);
				if(menuLimit == -1){
					return new ModelAndView(
							new RedirectView("loginController.do?login=key"));
				}
				
				page = "pages/main/main";
			} else {
				page = "pages/login/login";
			}
			return new ModelAndView(page);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/*************************************************************************
	*  函数名:         
	*  功能描述: 跳转菜单页面
	*  创 建 人: 王志恒
	*  日    期: 2015年10月28日 下午4:31:01
	*  修 改 人:
	*************************************************************************/
	@RequestMapping(params = "main=key")
	public ModelAndView menu(HttpServletRequest request) {
		return new ModelAndView("pages/login/login");
	}
	/**
	 * 菜单跳转
	 * 
	 * @return
	 */
	public int left(HttpServletRequest request) {
		try {
			HttpSession session = request.getSession();
			// session获取 user
			UserInfo user = ResourceUtil.getSessionUserName();
			// 登录权限
			if (user.getPkId() == null) {
				session.removeAttribute(Globals.USER_SESSION);
				
				// 没有权限
				return -1;
			}
			String menu = "";
			menu =getMenuMap(user);
			if(menu=="" ||menu==null){
				return -1;
			}else{
				request.setAttribute("menuMap", menu);
			}
			request.setAttribute("nav", getNavMap(user));
			return 0;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
		
	}
	

	/**
	 * 获取权限的map
	 * @param user
	 * @return
	 */
	private String getMenuMap(UserInfo user) {
		
		try {
			String menu = "";
			Map<String, List<MenuInfo>> functionMap  = new HashMap<String, List<MenuInfo>>();
			Map<String,MenuInfo> loginActionlist = getUserFunction(user);
			if (loginActionlist!=null && loginActionlist.size() > 0) {
				Collection<MenuInfo> allFunctions = loginActionlist.values();
				for (MenuInfo function : allFunctions) {
					if (!functionMap.containsKey(function.getMenuGrade())) {
						functionMap.put(function.getMenuGrade(),
								new ArrayList<MenuInfo>());
					}

					functionMap.get(function.getMenuGrade()).add(function);
				}
				// 菜单栏排序
				Collection<List<MenuInfo>> c = functionMap.values();
				for (List<MenuInfo> list :c) {
					Collections.sort(list, new NumberComparator());
				}
			}
			
			try {
				menu = menuService.initMenu(functionMap);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			return menu;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 获取权限的map
	 * @param user
	 * @return
	 */
	private String getNavMap(UserInfo user) {
		
		try {
			String menu = "";
			Map<String, List<MenuInfo>> functionMap  = new HashMap<String, List<MenuInfo>>();
			Map<String,MenuInfo> loginActionlist = getUserFunction(user);
			if (loginActionlist!=null && loginActionlist.size() > 0) {
				Collection<MenuInfo> allFunctions = loginActionlist.values();
				for (MenuInfo function : allFunctions) {
					if (!functionMap.containsKey(function.getMenuGrade())) {
						functionMap.put(function.getMenuGrade(),
								new ArrayList<MenuInfo>());
					}

					functionMap.get(function.getMenuGrade()).add(function);
				}
				// 菜单栏排序
				Collection<List<MenuInfo>> c = functionMap.values();
				for (List<MenuInfo> list :c) {
					Collections.sort(list, new NumberComparator());
				}
			}
			
			menu = menuService.initNav(functionMap);
			return menu;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 获取用户菜单列表
	 * @param user
	 * @return 
	 * @throws Exception 
	 */
	@SuppressWarnings("unused")
	private Map<String, MenuInfo> getUserFunction(UserInfo user) {
		
		try {
			// session 取得
			HttpSession session = SpringContextUtil.getSession();
			
			Map<String,MenuInfo> loginActionlist = new HashMap<String, MenuInfo>();
			List<SysPermissionInfo> rUsers = sysPermissionInfoService.queryByUserPkId(user.getPkId());
			for (SysPermissionInfo ru : rUsers) {
				
				RoleInfo role = new RoleInfo();
				try {
					role = roleService.queryById(ru.getRolePkId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				// session 取得roleMenuList
				List<RoleDetailInfo> roleFunctionList = getSessionRoleFunction(role.getPkId());
				

				if (roleFunctionList == null) {
					
					session.setMaxInactiveInterval(60 * 30);
					
					try {
						roleFunctionList = roleDetailService.queryByRoleId(role.getPkId());
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					session.setAttribute(role.getPkId(), roleFunctionList);
				} else {
					if (roleFunctionList.get(0).getPkId() == null || "".equals(roleFunctionList.get(0).getPkId())) {
						try {
							roleFunctionList = roleDetailService.queryByRoleId(role.getPkId());
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
				
				String exceptId = "";
				for (RoleDetailInfo roleFunction : roleFunctionList) {

					MenuInfo function = null;
					try {
						function = menuService.queryById(roleFunction.getMenuPkId());

					} catch (Exception e) {
						e.printStackTrace();
					}
					if(function.getMenuName().equals("跳转页面配置")){
						exceptId = function.getPkId();
					}
				}
				
				for (RoleDetailInfo roleFunction : roleFunctionList) {

					MenuInfo function = null;
					try {
						function = menuService.queryById(roleFunction.getMenuPkId());
						if(exceptId.equals(function.getPkId()) || exceptId.equals(function.getParentMenuId())){

						}else{
							loginActionlist.put(function.getPkId(),function);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}		
			return loginActionlist;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 退出系统
	 * 
	 * @param user
	 * @param req
	 * @return
	 */
	@RequestMapping(params = "logout=key")
	@ResponseBody
	public void logout(HttpServletRequest request) {

		try {
			HttpSession session = request.getSession();
			UserInfo user = ResourceUtil.getSessionUserName();

			List<SysPermissionInfo> rUsers = sysPermissionInfoService.queryByUserPkId(user.getPkId());
			for (SysPermissionInfo ru : rUsers) {
				
				RoleInfo role = new RoleInfo();
				try {
					role = roleService.queryById(ru.getRolePkId());
					session.removeAttribute(role.getPkId());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}

			// 判断用户是否为空不为空则清空session中的用户object
			operateLogService.addLog("退出系统", "退出系统", Globals.Log_Type_EXIT);
			
			session.removeAttribute(Globals.USER_SESSION);// 注销该操作用户
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * 首页跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "home=key")
	public ModelAndView home(HttpServletRequest request) {
		return new ModelAndView("pages/main/welcome");
	}
	
	public List<RoleDetailInfo> getSessionRoleFunction(String roleId) {
		try {
			HttpSession session = SpringContextUtil.getSession();
			session.setMaxInactiveInterval(30*60);
			if (session.getAttributeNames().hasMoreElements()) {
				List<RoleDetailInfo> roleFunctionList = (List<RoleDetailInfo>)session.getAttribute(roleId);
				if (roleFunctionList != null && roleFunctionList.size() > 0) {
					return roleFunctionList;
				} else {
					return null;
				}
			} else {
				return null;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 无权限页面提示跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "noAuth=key")
	public ModelAndView noAuth(HttpServletRequest request) {
		return new ModelAndView("common/noAuth");
	}
	
	
	/**
	 * 修改密码
	 * 
	 * @return
	 */
	@RequestMapping(params = "modifyPassword=key")
	public ModelAndView modifyPassword(HttpServletRequest request) {
		UserInfo userInfo = ResourceUtil.getSessionUserName();
		UserInfo user =new UserInfo();
		try {
			user = userInfoService.queryById(userInfo.getPkId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView("pages/main/passwordModify","user",user);
	}
	
	
	/**
	 * 修改密码
	 * 
	 * @return
	 */
	@RequestMapping(params = "updatePassword=key")
	@ResponseBody
	public AjaxJson updatePassword(HttpServletRequest request, UserInfo userInfo) {
		try {
			AjaxJson j = new AjaxJson();
			if(userInfo!=null&&!"".equals(userInfo.getPkId())){
				String oldPassWord= userInfoService.queryById(userInfo.getPkId()).getPassword();
				String passwordRemark = MethodUtil.MD5(userInfo.getRemark());
				if(!oldPassWord.equals(passwordRemark)){
					message = "修改密码必须录入正确的旧密码。";
					j.setMsg(message);
					return j;
				}
			}
			//密码设置md5加密
			UserInfo userInfoPass = new UserInfo();
			userInfoPass = userInfoService.queryById(userInfo.getPkId());
			userInfoPass.setPassword(MethodUtil.MD5(userInfo.getPassword()));
			userInfoService.updateBySelective(userInfoPass);
			operateLogService.addLog("用户管理模块", "修改密码日志", Globals.Log_Type_UPDATE);
			message = "修改密码成功";
			j.setMsg(message);
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public List<String> departs() {
		try {
			// 存放departID，用于去除重复
			Set<String> set = new HashSet<String>();
			List<String> departs = new ArrayList<String>();
			// 取得当前用户信息
			UserInfo user = ResourceUtil.getSessionUserName();
			
			List<SysPermissionInfo> rUsers = sysPermissionInfoService.queryByUserPkId(user.getPkId());
			for (SysPermissionInfo ru : rUsers) {
				
				RoleInfo role = new RoleInfo();
				try {
					role = roleService.queryById(ru.getRolePkId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				List<RoleDepartInfo> roleDepartList = null;
				
				try {
					roleDepartList = roleDepartService.queryByRoleId(role.getPkId());
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				
				for (RoleDepartInfo roleDepart : roleDepartList) {

					Depart depart = null;
					try {
						set.add(roleDepart.getDepartPkId());
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
			
			for (String departId : set) {  
				departs.add(departId);
			}
			return departs;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}

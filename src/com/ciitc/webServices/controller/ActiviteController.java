package com.ciitc.webServices.controller;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.ActivityBasicInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.SignMessageQuery;
import com.ciitc.webServices.service.ActiviteService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.SignMessageQueryService;
import com.ciitc.webServices.util.CalculationIntegral;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/activiteController")
public class ActiviteController extends BaseController {

	@Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;

	@Autowired(required = false)
	private ActiviteService<ActivityBasicInfor> activiteService;

	@Autowired(required = false)
	private SignMessageQueryService<SignMessageQuery> signMessageQueryService;

	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;

	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志

	private String message;

	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "activiteList=key")
	public ModelAndView departList() {
		return new ModelAndView("pages/activite/activiteInfoList");
	}

	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletResponse response, ActivityBasicInfor activityBasicInfor, BasePage basePage) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		paramMap.put("activityBasicInfor", activityBasicInfor);
		List<ActivityBasicInfor> list = new ArrayList<ActivityBasicInfor>();
		try {
			list = activiteService.queryByList(paramMap);
			// 根据数据字典查询状态
			List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("activityState");
			EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
			EsbmmCode codePkId = new EsbmmCode();
			codePkId.setCodeTypePkId(codeTypePkId.getPkId());
			// 根据codeTypePkId查找对应的类型
			List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
			for (ActivityBasicInfor activityBasicInfor1 : list) {
				String activityStartDate = sdf.format(activityBasicInfor1.getActivityStartDate());
				String activityEndDate = sdf.format(activityBasicInfor1.getActivityEndDate());
				String activitySignStart = sdf.format(activityBasicInfor1.getActivitySignStart());
				String activitySignEnd = sdf.format(activityBasicInfor1.getActivitySignEnd());
				activityBasicInfor1.setRemark1(activityStartDate);
				activityBasicInfor1.setRemark2(activityEndDate);
				activityBasicInfor1.setRemark3(activitySignStart);
				activityBasicInfor1.setRemark4(activitySignEnd);
				for (EsbmmCode esbmmCode : codeList) {
					if (activityBasicInfor1.getActivityState().equals(esbmmCode.getCodeCode())) {
						activityBasicInfor1.setActivityState(esbmmCode.getChineseName());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 记录总条数
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
		pageUtil.responseDatagrid(response, jObject);

	}

	@RequestMapping(params = "add=key")
	public ModelAndView repairInforAdd(HttpServletRequest req) {
		return new ModelAndView("pages/activite/activiteInforAdd");
	}

	@RequestMapping(params = "modify=key")
	public ModelAndView repairInforModify(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		ActivityBasicInfor activityBasicInfor = activiteService.queryById(pkId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String activityStartDate = sdf.format(activityBasicInfor.getActivityStartDate());
		String activityEndDate = sdf.format(activityBasicInfor.getActivityEndDate());
		String activitySignStart = sdf.format(activityBasicInfor.getActivitySignStart());
		String activitySignEnd = sdf.format(activityBasicInfor.getActivitySignEnd());
		context.put("activityStartDate", activityStartDate);
		context.put("activityEndDate", activityEndDate);
		context.put("activitySignStart", activitySignStart);
		context.put("activitySignEnd", activitySignEnd);
		// 本机地址
		List<EsbmmCode> code = esbmmCodeService.getAddress();
		String address = code.get(0).getCodeValue();
		String URL = address+activityBasicInfor.getPictureURL();
		activityBasicInfor.setURL(URL);
		context.put("activityBasicInfor", activityBasicInfor);

		return new ModelAndView("pages/activite/activiteInforAdd", context);
	}

	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(ActivityBasicInfor activityBasicInfor) throws Exception {
		AjaxJson json = new AjaxJson();
		if (activityBasicInfor.getPkId() != null && !"".equals(activityBasicInfor.getPkId())) {
			try {
				activiteService.update(activityBasicInfor);
				operateLogService.addLog("活动信息管理", "修改活动信息日志", Globals.Log_Type_UPDATE);
			} catch (Exception e) {
				e.printStackTrace();
			}
			message = "编辑成功";
		} else {
			String uuid = UUID.randomUUID().toString();
			activityBasicInfor.setPkId(uuid);
			activiteService.add(activityBasicInfor);
			operateLogService.addLog("活动信息管理", "新增活动信息日志", Globals.Log_Type_INSERT);
			message = "保存成功";
		}
		json.setMsg(message);
		return json;
	}

	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(ActivityBasicInfor activityBasicInfor) throws Exception {
		AjaxJson json = new AjaxJson();
		if (activityBasicInfor.getPkId() != null && !"".equals(activityBasicInfor.getPkId())) {
			activiteService.delete(activityBasicInfor.getPkId());
			operateLogService.addLog("活动信息管理", "删除活动信息日志", Globals.Log_Type_DEL);
		}
		message = "删除成功";
		json.setObj(message);
		return json;

	}

	@RequestMapping(params = "specific=key")
	public ModelAndView repairInforSpecific(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		ActivityBasicInfor activityBasicInfor = activiteService.queryById(pkId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String activityStartDate = sdf.format(activityBasicInfor.getActivityStartDate());
		String activityEndDate = sdf.format(activityBasicInfor.getActivityEndDate());
		String activitySignStart = sdf.format(activityBasicInfor.getActivitySignStart());
		String activitySignEnd = sdf.format(activityBasicInfor.getActivitySignEnd());
		context.put("activityStartDate", activityStartDate);
		context.put("activityEndDate", activityEndDate);
		context.put("activitySignStart", activitySignStart);
		context.put("activitySignEnd", activitySignEnd);
		// 本机地址
		List<EsbmmCode> code = esbmmCodeService.getAddress();
		String address = code.get(0).getCodeValue();
		String URL = address+activityBasicInfor.getPictureURL();
		activityBasicInfor.setURL(URL);
		context.put("activityBasicInfor", activityBasicInfor);
		return new ModelAndView("pages/activite/activiteInforSpecific", context);
	}

	@RequestMapping(params = "signQuery=key")
	public ModelAndView getSignQuery(String pkIds, String activityState) {
		Map<String, Object> context = getRootMap();
		context.put("pkIds", pkIds);
		String activityState1 = null;
		try {
			activityState1 = java.net.URLDecoder.decode(activityState, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if ("已结束".equals(activityState1)) {
			return new ModelAndView("pages/activite/signQueryInfo", context);
		} else {
			return new ModelAndView("pages/activite/signQueryInfo1", context);
		}
	}

	@RequestMapping(params = "datagrid1=key")
	public void datagrid1(HttpServletResponse response, SignMessageQuery signMessageQuery, BasePage basePage) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		if (signMessageQuery.getUserPhoneNumber() != null && !"".equals(signMessageQuery.getUserPhoneNumber())) {
			String userPhoneNumber = "%" + signMessageQuery.getUserPhoneNumber() + "%";
			signMessageQuery.setUserPhoneNumber(userPhoneNumber);
		}
		paramMap.put("signMessageQuery", signMessageQuery);
		List<SignMessageQuery> list = new ArrayList<SignMessageQuery>();
		try {
			list = signMessageQueryService.queryByList1(paramMap);
			for (SignMessageQuery signMessageQuery2 : list) {
				List<EsbmmCodeType> IdcardTypeList = esbmmCodeTypeService.queryByTypeName("RegistrationStatus");
				EsbmmCodeType IdcardTypePkId = IdcardTypeList.get(0);
				EsbmmCode codePkId2 = new EsbmmCode();
				codePkId2.setCodeTypePkId(IdcardTypePkId.getPkId());
				// 根据codeTypePkId查找对应的类型
				List<EsbmmCode> IdcardTypecodeList = esbmmCodeService.queryByIdcard(codePkId2);
				for (EsbmmCode esbmmCode : IdcardTypecodeList) {
					if (signMessageQuery2.getRemark1().equals(esbmmCode.getCodeValue())) {
						signMessageQuery2.setRemark1(esbmmCode.getChineseName());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 记录总条数
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
		pageUtil.responseDatagrid(response, jObject);

	}

	@RequestMapping(params = "uploadPic=key")
	public void uploadPic(@RequestParam(value = "pic") MultipartFile file, HttpServletRequest request,
			HttpServletResponse response) throws IllegalStateException, IOException {

		try {
			// 获取图片原始文件名
			String originalFilename = file.getOriginalFilename();
			// 文件名使用ID
			String name = UUID.randomUUID().toString();
			// 获取上传图片的扩展名(jpg/png/...)
			String extension = FilenameUtils.getExtension(originalFilename);
			// 图片上传本地路径
			List<EsbmmCode> code1 = esbmmCodeService.getUploadAddress();
			String uploadAddress = code1.get(0).getCodeValue();
			String path = uploadAddress + name + "." + extension;
			// Tomcat虚拟路径
			String path1 = "/photo/" + name + "." + extension;
			File dir = new File(path);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			// 上传图片
			file.transferTo(new File(path));
			// 本机地址
			JSONObject jsonObject = new JSONObject();
			List<EsbmmCode> code = esbmmCodeService.getAddress();
			String address = code.get(0).getCodeValue();
			// 将虚拟路径保存并返回页面
			jsonObject.put("path", address + path1);
			jsonObject.put("path1", path1);
			// 设置响应数据的类型json
			response.setContentType("application/json; charset=utf-8");
			response.getWriter().write(jsonObject.toString());
			operateLogService.addLog("活动消息管理", "活动内容图片上传日志", Globals.Log_Type_UPLOAD);

		} catch (Exception e) {
			throw new RuntimeException("服务器繁忙，上传图片失败");
		}
	}

	/*************************************************************************
	 * 函数名: exportList 功能描述: 统计导出需要的信息 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 * 
	 * @throws Exception
	 *************************************************************************/
	@RequestMapping(params = "exportList=key")
	public void exportList(HttpServletResponse response, HttpServletRequest request, SignMessageQuery signMessageQuery)
			throws Exception {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("signMessageQuery", signMessageQuery);
		List<SignMessageQuery> list = new ArrayList<SignMessageQuery>();
		list = signMessageQueryService.queryByList2(paramMap);
		if (list.size() > 0) {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/x-download");
			excel1(list, response,request);
		}

	}

	public void excel1(List<SignMessageQuery> list, HttpServletResponse response,HttpServletRequest request) throws Exception {
		String activityId = list.get(0).getActivityId();
		String activityName = activiteService.queryById(activityId).getActivityName();
		String filedisplay = null;
		filedisplay = activityName + ".xls";
		String agent = request.getHeader("USER-AGENT");
		if (null != agent && -1 != agent.indexOf("MSIE")) { // IE
			filedisplay = java.net.URLEncoder.encode(filedisplay, "UTF-8");
		} else if (null != agent && -1 != agent.indexOf("Firefox")) { // Firefox
			filedisplay = new String(filedisplay.getBytes("UTF-8"), "iso-8859-1");
		} else {
			filedisplay = java.net.URLEncoder.encode(filedisplay, "UTF-8");
		}
		//filedisplay = URLEncoder.encode(filedisplay, "UTF-8");
		// 创建一个工作簿
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet(activityName);
		// 创建行
		HSSFRow row = sheet.createRow((int) 0);
		// 设置默认的列宽
		sheet.setDefaultColumnWidth(10);
		// 设置样式
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		org.apache.poi.hssf.usermodel.HSSFFont font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setLocked(true);
		style.setFont(font);
		// 设置表内容格式
		HSSFCellStyle style2 = wb.createCellStyle();
		style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		HSSFCell cell = row.createCell(0);
		cell.setCellValue("用户姓名");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("用户手机号");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("报名时间");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("理财师姓名");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("理财师电话");
		cell.setCellStyle(style);
		// 将数据写入excel
		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow((int) i + 1);
			SignMessageQuery signMessageQuery = list.get(i);
			// 创建单元格，设置值
			row.createCell(0).setCellValue(signMessageQuery.getUserName());
			row.createCell(1).setCellValue(signMessageQuery.getUserPhoneNumber());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String signDate = sdf.format(signMessageQuery.getSignDate());
			row.createCell(2).setCellValue(signDate);
			row.createCell(3).setCellValue(signMessageQuery.getFinancialName());
			row.createCell(4).setCellValue(signMessageQuery.getFinancialPhoneNumber());
			for (int j = 0; j < 5; j++) {
				row.getCell(j).setCellStyle(style2);
			}
		}
		OutputStream out = response.getOutputStream();
		response.addHeader("Content-Disposition", "attachment;filename=" + filedisplay);
		wb.write(out);
		operateLogService.addLog("活动信息管理", "导出报名信息日志", Globals.Log_Type_EXPORT);
		out.close();
	}
	//校验导出的数据
	@RequestMapping(params = "exports=key")
	@ResponseBody
	public AjaxJson exports(HttpServletResponse response, HttpServletRequest request, SignMessageQuery signMessageQuery)
			throws Exception {
		AjaxJson json = new AjaxJson();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("signMessageQuery", signMessageQuery);
		List<SignMessageQuery> list = new ArrayList<SignMessageQuery>();
		list = signMessageQueryService.queryByList2(paramMap);
		if (list.size() > 0) {
			json.setObj("ok");
			json.setMsg("报名人员信息已导出");
		}else {
			json.setObj("no");
			json.setMsg("暂无报名人员信息");
		}
		return json;
	}

	// 活动确认
	@RequestMapping(params="confirm=key")
	@ResponseBody
	public AjaxJson confirm(String pkId) throws Exception{
		AjaxJson json=new AjaxJson();
		CalculationIntegral calculationIntegral = new CalculationIntegral();
		if(pkId!=null && !"".equals(pkId)){
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("pkId", pkId);
			map.put("remark1", "00");
			signMessageQueryService.updateByPkId(map);
			//根据报名信息查询参加的活动信息
			SignMessageQuery signMessageQuery = signMessageQueryService.queryById(pkId);
			String activityPkId = signMessageQuery.getActivityId();
			ActivityBasicInfor activityBasicInfor = activiteService.queryById(activityPkId);
			String name = activityBasicInfor.getActivityName();
			String userId = signMessageQuery.getRemark2();
			String type = "03";
			Integer integration = 10;
			calculationIntegral.BirIntegral(userId, integration, name, type);
			//operateLogService.addLog("活动信息管理", "删除活动信息日志", Globals.Log_Type_DEL);
		} 
		message="确认成功";
		json.setObj("ok");
		json.setMsg(message);
		return json;
		
	}

}

package com.ciitc.webServices.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.AppGoodsInfor;
import com.ciitc.webServices.model.AppOrderDetails;
import com.ciitc.webServices.model.AppOrderInfor;
import com.ciitc.webServices.model.AppShoppingCarInfor;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.Area;
import com.ciitc.webServices.model.City;
import com.ciitc.webServices.model.GoddsAddress;
import com.ciitc.webServices.model.HelpDocument;
import com.ciitc.webServices.model.KnowledgeBase;
import com.ciitc.webServices.model.OrderCount;
import com.ciitc.webServices.model.Province;
import com.ciitc.webServices.model.SubmitGoodsList;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.model.appIntegralDetails;
import com.ciitc.webServices.service.AppGoodsInforService;
import com.ciitc.webServices.service.AppOrderDetailsService;
import com.ciitc.webServices.service.AppOrderInforService;
import com.ciitc.webServices.service.AppShoppingCarInforService;
import com.ciitc.webServices.service.AreaService;
import com.ciitc.webServices.service.BatchAddService;
import com.ciitc.webServices.service.CityService;
import com.ciitc.webServices.service.GoddsAddressService;
import com.ciitc.webServices.service.HelpDocumentService;
import com.ciitc.webServices.service.ProvinceService;
import com.ciitc.webServices.service.appIntegralDetailsService;
import com.ciitc.webServices.util.CalculationIntegral;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.entity.SessionInfo;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/orderInforController")
public class OrderInforController extends BaseController{
	
	@Autowired(required = false)
	private AppOrderInforService<AppOrderInfor> orderInforService;
	@Autowired(required = false)
	private AppGoodsInforService<AppGoodsInfor> appGoodsInforService;
	@Autowired(required = false)
	private AppShoppingCarInforService<AppShoppingCarInfor> appShoppingCarInforService;
	@Autowired(required = false)
	private GoddsAddressService<GoddsAddress> goddsAddressService;
	@Autowired(required = false)
	private ProvinceService<Province> provinceService;
	@Autowired(required = false)
	private BatchAddService<AppUserInfor> batchAddService;
	@Autowired(required = false)
	private appIntegralDetailsService<appIntegralDetails> appIntegralDetailsService;
	@Autowired(required = false)
	private CityService<City> cityService;
	@Autowired(required = false)
	private AreaService<Area> areaService;
	@Autowired(required = false)
	private AppOrderDetailsService<AppOrderDetails> appOrderDetailsService;
	@Autowired(required = false)
	private appIntegralDetailsService<appIntegralDetails> appIntegral;
	@Autowired(required = false)
	private HelpDocumentService<KnowledgeBase> helpDocumentService;
	private String message;
	
	/*************************************************************************
	 * 函数名: List 功能描述: 跳转数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "orderInfoList=key")
	public ModelAndView orderInfoList() {
		return new ModelAndView("pages/order/orderManagerList");
	}
	
	@RequestMapping(params="datagrid=key")
	public void datagrid(HttpServletResponse response,AppOrderInfor orderManagerInfo, BasePage basePage,
													String purchaseTime,String flag) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("page", basePage);
		paramMap.put("purchaseTime", purchaseTime);
		paramMap.put("flag", flag);
		paramMap.put("orderManagerInfo", orderManagerInfo);
		List<AppOrderInfor> list = new ArrayList<AppOrderInfor>();
		try {
			list = orderInforService.queryByList(paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// 记录总条数
		int cnt = basePage.getPager().getRowCount();
		JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
		pageUtil.responseDatagrid(response, jObject);
		
	}
	
	@RequestMapping(params = "add=key")
	public ModelAndView sendGoods(String userId) {
		Map<String, Object> context = new HashMap<String, Object>();
		context.put("userId", userId);
		return new ModelAndView("pages/order/order",context);
	}
	
	@RequestMapping(params = "sendGoodsList=key")
	public ModelAndView sendGoodsList(String userId) {
		Map<String, Object> context = getRootMap();
		context.put("userId",userId);
		return new ModelAndView("pages/order/orderList",context);
	}
	
	@RequestMapping(params = "shoppingCarList=key")
	public ModelAndView shoppingCarList(String userId) {
		Map<String, Object> context = getRootMap();
		context.put("userId", userId);
		return new ModelAndView("pages/order/shoppingCarList",context);
	}
	
	
	//查询所有商品
	@RequestMapping(params = "queryGoodsList=key")
	@ResponseBody
	public AjaxJson queryGoodsList(Integer end,Integer start,String goodsName){
		AjaxJson json=new AjaxJson();
		Map<String,Object> map = new HashMap<String,Object>();
		//map.put("start", start);
		//map.put("end", end);
		map.put("goodsName", goodsName);
		List<AppGoodsInfor> list = new ArrayList<AppGoodsInfor>();
		try {
			list = appGoodsInforService.selectList(map);
		} catch (Exception e) {
			e.printStackTrace();
		}
		json.setObj(list);
		return json;
	}
	
	//根据id查询商品表，添加到购物车表中
	@RequestMapping(params = "queryCarList=key")
	@ResponseBody
	public AjaxJson queryCarList(String productId,String shopInfoId,Integer sku,String userId){
		AjaxJson json=new AjaxJson();
		try {
			//UserInfo u = ResourceUtil.getSessionUserName();
			AppGoodsInfor goods = appGoodsInforService.queryById(productId);
			AppShoppingCarInfor carInfo = new AppShoppingCarInfor();
			carInfo.setGoodsId(goods.getGoodsId());
			carInfo.setGoodsName(goods.getGoodsName());
			carInfo.setMainImageUrl(goods.getMainImageUrl());
			carInfo.setPrice(goods.getPrice());
			carInfo.setDescribe(goods.getDescribe());
			carInfo.setCreateDate(new Date());
			carInfo.setOperatorId(userId);
			appShoppingCarInforService.add(carInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		json.setSuccess(true);
		json.setMsg("");
		return json;
	}
	
	//改变购物车中商品数量
	@RequestMapping(params = "queryByCarInfo=key")
	@ResponseBody
	public AjaxJson queryByCarInfo(String userId){
		AjaxJson json=new AjaxJson();
		try {
			Integer cartNum = appShoppingCarInforService.queryCarCount(userId);
			json.setObj(cartNum);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	//查询购物车中所有的商品
	@RequestMapping(params = "queryByCarInfoList=key")
	@ResponseBody
	public AjaxJson queryByCarInfoList(String userId){
		AjaxJson json=new AjaxJson();
		Map<String,Object> map = new HashMap<String, Object>();
		try {
			map.put("operatorId", userId);
			List<AppShoppingCarInfor> list = appShoppingCarInforService.queryByinfo(map);
			map.clear();
			for(int i=0;i<list.size();i++){
				map.put("goodsId", list.get(i).getGoodsId());
				map.put("operatorId", list.get(i).getOperatorId());
				Integer num = appShoppingCarInforService.queryBynum(map);
				Integer goodsNum = appGoodsInforService.selectgoodsCount(map);
				list.get(i).setRemark1(num.toString());
				list.get(i).setRemark2(goodsNum.toString());
			}
			json.setObj(list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}
	
	//删除购物车中所有商品
	@RequestMapping(params="deleteAll=key")
	@ResponseBody
	public AjaxJson deleteAll(String ids,String userId) throws Exception{
		AjaxJson json=new AjaxJson();
		//UserInfo u = ResourceUtil.getSessionUserName();
		//appShoppingCarInforService.deleteAll(u.getPkId());
		if(ids!=null && !"".equals(ids)){
			String id[] = ids.split(",");
			for(int i=0;i<id.length;i++){
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("goodsId", id[i]);
				map.put("operatorId", userId);
				appShoppingCarInforService.deleteAll(map);
			}
		} 
		json.setSuccess(true);
		return json;
	}
	
	//详情页面
	@RequestMapping(params="goodsDetails=key")
	public ModelAndView goodsDetails(String productId,String userId) throws Exception {
		Map<String, Object> context = getRootMap();
		//AppGoodsInfor goods=appGoodsInforService.queryByGoodsId(productId);
		context.put("productId", productId);
		context.put("userId", userId);
		return new ModelAndView("pages/order/goodsDetails",context);
	}
	
	//显示商品详细信息
	@RequestMapping(params="goodsDetailsData=key")
	@ResponseBody
	public AjaxJson goodsDetailsData(String productId){
		AjaxJson json=new AjaxJson();
		if(productId !=null && !"".equals(productId)){
			AppGoodsInfor goods=appGoodsInforService.queryByGoodsId(productId);
			if(goods == null){
				json.setObj("false");
				return json;
			}
			json.setObj(goods);
		}
		return json;
	}
	
	//根据详情页面添加商品
	@RequestMapping(params = "addCarList=key")
	@ResponseBody
	public AjaxJson addCarList(String productId,String userId,int span){
		AjaxJson json=new AjaxJson();
		try {
			for(int i=0;i<span;i++){
			//UserInfo u = ResourceUtil.getSessionUserName();
			AppGoodsInfor goods = appGoodsInforService.queryByGoodsId(productId);
			AppShoppingCarInfor carInfo = new AppShoppingCarInfor();
			carInfo.setGoodsId(goods.getGoodsId());
			carInfo.setGoodsName(goods.getGoodsName());
			carInfo.setMainImageUrl(goods.getMainImageUrl());
			carInfo.setPrice(goods.getPrice());
			carInfo.setDescribe(goods.getDescribe());
			carInfo.setCreateDate(new Date());
			carInfo.setOperatorId(userId);
			appShoppingCarInforService.add(carInfo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		json.setSuccess(true);
		json.setMsg("");
		return json;
	}
	
	@RequestMapping(params = "test=key")
	public ModelAndView sendGoodss(String pkId) {
		Map<String, Object> context = getRootMap();
		context.put("pkId", pkId);
		return new ModelAndView("pages/order/orderManagerAdd",context);
	}
	
	@RequestMapping(params="saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(AppOrderInfor orderManagerInfo) throws Exception{
		AjaxJson json=new AjaxJson();
		if(orderManagerInfo.getPkId()!=null && !"".equals(orderManagerInfo.getPkId())){
			try {
				orderManagerInfo.setOrderState("已发货");
				orderInforService.updateByPrimaryKey(orderManagerInfo);
			} catch (Exception e) {
				e.printStackTrace();
			}
			message = "编辑成功";
		}
		json.setMsg(message);
		return json;
	}
	
	@RequestMapping(params="specific=key")
	public ModelAndView repairInforSpecific(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		AppOrderInfor orderManagerInfo=orderInforService.queryById(pkId);
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		String purchaseTime=sdf.format(orderManagerInfo.getPurchaseTime());
		context.put("purchaseTime", purchaseTime);
		context.put("orderManagerInfo", orderManagerInfo);
		return new ModelAndView("pages/order/orderManagerSpecific",context);
	}
	
	//提交订单校验
	@RequestMapping(params="checkOrder=key")
	@ResponseBody
	public AjaxJson checkOrder(String userId,String total) throws Exception{
		AjaxJson json=new AjaxJson();
			try {
				AppUserInfor appUser = batchAddService.queryByPkId(userId);
				String[] totals = total.split("\\.");
				Integer  totalNum = Integer.parseInt(totals[0]);
				if(appUser != null){
					if(appUser.getIntegral() < totalNum){
						json.setObj("0");
					}else{
						json.setObj("1");
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		return json;
	}
	
	//提交订单
	@RequestMapping(params="countSubmit=key")
	public ModelAndView countSubmit(OrderCount orderCount,String userId,String total) throws Exception{
		//UserInfo u = ResourceUtil.getSessionUserName();
		//String orderId = UUID.randomUUID().toString();
		//==========================================
		Map<String, Object> context = getRootMap();
		Random ra = new Random();
		String orderId ="d";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
		orderId += sdf.format(new Date());
		for(int i = 0;i<2 ; i++) {
			orderId +=Integer.toString(ra.nextInt(10));
		}
		//==========================================
		String pkId = UUID.randomUUID().toString();
		AppUserInfor appUserInfor = batchAddService.queryByuserId(userId);
		AppOrderInfor appOrderInfor = new AppOrderInfor();
		appOrderInfor.setPkId(pkId);
		appOrderInfor.setAddress(orderCount.getAddress());
		appOrderInfor.setOrderId(orderId);
		appOrderInfor.setPurchaseId(userId);
		appOrderInfor.setReceiptName(orderCount.getShopName());
		appOrderInfor.setReceiptPhone(orderCount.getShopPhone());
		appOrderInfor.setPurchaseTime(new Date());
		appOrderInfor.setOrderState("未发货");
		appOrderInfor.setOrderprice(Double.parseDouble(total));
		appOrderInfor.setGoodsIds(orderCount.getGoddsList().get(0).getGoodsId());
		appOrderInfor.setGoodsName(orderCount.getGoddsList().get(0).getGoodsName());
		appOrderInfor.setMainImageUrl(orderCount.getGoddsList().get(0).getPictureURL());
		appOrderInfor.setRemark1(appUserInfor.getUserName());
		Integer count = 0;
		for (SubmitGoodsList submitGoodsList : orderCount.getGoddsList()) {
			count += Integer.parseInt(submitGoodsList.getNum());
			String id = UUID.randomUUID().toString();
			AppOrderDetails appOrderDetails = new AppOrderDetails();
			appOrderDetails.setPkId(id);
			appOrderDetails.setOrderId(orderId);
			appOrderDetails.setGoodsId(submitGoodsList.getGoodsId());
			appOrderDetails.setGoodsName(submitGoodsList.getGoodsName());
			appOrderDetails.setPrice(submitGoodsList.getPrice());
			appOrderDetails.setGoodNumber(Integer.parseInt(submitGoodsList.getNum()));
			appOrderDetails.setMainImageUrl(submitGoodsList.getPictureURL());
			appOrderDetails.setCountPrice((submitGoodsList.getPrice())*(Integer.parseInt(submitGoodsList.getNum())));
			appOrderDetails.setCreateDate(new Date());
			appOrderDetails.setOperatorId(userId);
			appOrderDetailsService.add(appOrderDetails);
			AppGoodsInfor appGoodsInfo = appGoodsInforService.queryByGoodsId(submitGoodsList.getGoodsId());
			AppGoodsInfor appGoods = new AppGoodsInfor();
			appGoods.setGoodsId(submitGoodsList.getGoodsId());
			appGoods.setSurplus(appGoodsInfo.getSurplus() - Integer.parseInt(submitGoodsList.getNum()));
			appGoodsInforService.updateBygoodsId(appGoods);
//			appIntegralDetails appIntegral = new appIntegralDetails();
//			String IntegralId = UUID.randomUUID().toString();
//			appIntegral.setPkId(IntegralId);
//			appIntegral.setUserId(userId);
//			appIntegral.setRecordDate(new Date());
//			appIntegral.setIntegralNum(submitGoodsList.getPrice()+"");
//			appIntegral.setValidity(365);
//			appIntegral.setIntegralType("04");
//			appIntegral.setName("兑换："+submitGoodsList.getGoodsName());
//			appIntegral.setIntegralState("01");
//			appIntegralDetailsService.insertSelective(appIntegral);
			CalculationIntegral.IntegralDel(userId, submitGoodsList.getPrice()*(Integer.parseInt(submitGoodsList.getNum())), submitGoodsList.getGoodsName());
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("goodsId", submitGoodsList.getGoodsId());
			map.put("operatorId", userId);
			appShoppingCarInforService.deleteAll(map);
			
		}
		appOrderInfor.setGoodsNum(count);
		orderInforService.insertSelective(appOrderInfor);
//		AppUserInfor appUser = batchAddService.queryByPkId(userId);
//		String[] totals = total.split("\\.");
//		Integer  totalNum = Integer.parseInt(totals[0]);
//		if(appUser != null) {
//			long sum = appUser.getIntegral() - totalNum;
//			appUser.setIntegral(sum);
//		}
//		
//		batchAddService.updatebyuserId(appUser);
		context.put("userId", userId);
		return new ModelAndView("pages/order/SuccessfulOrder",context);
	}
	
	//提交订单校验
	@RequestMapping(params="checkGoodsNum=key")
	@ResponseBody
	public AjaxJson checkGoodsNum(String goodsIds,String goodsNums) throws Exception{
		AjaxJson json=new AjaxJson();
			try {
				String[] goodsId = goodsIds.split(",");
				String[] goodsNum = goodsNums.split(",");
				for(int i=0;i<goodsId.length;i++){
					AppGoodsInfor appGoodsInfor = appGoodsInforService.queryByGoodsId(goodsId[i]);
					if(Integer.parseInt(goodsNum[i]) > appGoodsInfor.getSurplus()){
						json.setObj("no");
						json.setMsg(appGoodsInfor.getGoodsName()+"库存不足啦！");
						return json;
					}else{
						json.setObj("ok");
					}
				}
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		return json;
	}
	
	//去结算
	@RequestMapping(params = "orderCountList=key")
	public ModelAndView orderCountList(String goodsIds,String goodsNums,String goodsNames,String mainImageUrls,String prices,String userId) {
		Map<String, Object> context = getRootMap();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("addressState", "1");
		map.put("userId", userId);
		GoddsAddress goddsAddress1 = goddsAddressService.selectByState(map);
		if(null != goddsAddress1) {
			String provinceid = goddsAddress1.getProvince();
			String areaid = goddsAddress1.getArea();
			String cityid = goddsAddress1.getCity();
			Province province = provinceService.selectById(provinceid);
			City city = cityService.selectById(cityid);
			Area area = areaService.selectById(areaid);
			goddsAddress1.setProvince(province.getProvince());
			goddsAddress1.setArea(area.getArea());
			goddsAddress1.setCity(city.getCity());
			goddsAddress1.setRemark1(goddsAddress1.getProvince()+","+goddsAddress1.getCity()+","+
				    goddsAddress1.getArea()+","+ goddsAddress1.getShopAddress());
		}
		String[] goodsId = goodsIds.split(",");
		String[] goodsNum = goodsNums.split(",");
		String[] goodsName = goodsNames.split(",");
		String[] mainImageUrl = mainImageUrls.split(",");
		String[] price = prices.split(",");
		List<AppGoodsInfor> list = new ArrayList<AppGoodsInfor>();
		int total =0;
		for(int i=0;i<goodsId.length;i++) {
			AppGoodsInfor appGoodsInfo = appGoodsInforService.queryByGoodsId(goodsId[i]);
			AppGoodsInfor appGoodsInfor = new AppGoodsInfor();
			appGoodsInfor.setGoodsId(goodsId[i]);
			appGoodsInfor.setGoodsName(goodsName[i]);
			appGoodsInfor.setMainImageUrl(mainImageUrl[i]);
			//appGoodsInfor.setPrice(Double.parseDouble(price[i]));
			appGoodsInfor.setRemark3(price[i]);
			appGoodsInfor.setNum(Integer.parseInt(goodsNum[i]));
			total += (Double.parseDouble(price[i]))*(Integer.parseInt(goodsNum[i]));
			/*AppGoodsInfor appGoods = new AppGoodsInfor();
			appGoods.setGoodsId(goodsId[i]);
			appGoods.setSurplus(appGoodsInfo.getSurplus() - Integer.parseInt(goodsNum[i]));
			appGoodsInforService.updateBygoodsId(appGoods);*/
			list.add(appGoodsInfor);
		}
		
		context.put("goddsAddress", goddsAddress1);
		context.put("list", list);
		context.put("total", total);
		context.put("userId",userId);
		return new ModelAndView("pages/order/orderCount",context);
	}
	//订单列表
	@RequestMapping(params = "getOrderList=key")
	public void getOrderList(HttpServletResponse response,HttpServletRequest request,String userId) {
		//List<AppOrderInfor> list = new ArrayList<AppOrderInfor>();
		List<AppOrderInfor> appOrder = new ArrayList<AppOrderInfor>();
		//List<AppOrderInfor> list1 = new ArrayList<AppOrderInfor>();
		try {
			//list = orderInforService.queryByAll(userId);
			//for (AppOrderInfor appOrderInfor1 : list) {
				//AppOrderInfor appOrderInfor = new AppOrderInfor();
				//Double countPrice = 0.0;
				appOrder = orderInforService.queryByuser(userId);
//				for (AppOrderInfor appOrderInfor2 : appOrder) {
//					countPrice += appOrderInfor2.getOrderprice();
//				}
//				appOrderInfor.setOrderId(appOrderInfor1.getOrderId());
//				appOrderInfor.setOrderState(appOrder.get(0).getOrderState());
//				appOrderInfor.setGoodsName(appOrder.get(0).getGoodsName());
//				appOrderInfor.setMainImageUrl(appOrder.get(0).getMainImageUrl());
//				appOrderInfor.setGoodsNum(appOrder.size());
//				appOrderInfor.setOrderprice(countPrice);
//				list1.add(appOrderInfor);
		//	}
			response.getWriter().println(JSONArray.fromObject(appOrder).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	//根据订单id查询订单详情
	@RequestMapping(params = "orderDel=key")
	public ModelAndView orderDel(String ordersId,String userId) {
		Map<String, Object> context = new HashMap<String, Object>();
		AppOrderInfor appOrderInfor = orderInforService.selectByPrimaryKey(ordersId);
		List<AppOrderDetails> orderList = appOrderDetailsService.queryByorderId(ordersId);
		Double money = 0.0;
		for (AppOrderDetails appOrderDetails : orderList) {
			money += appOrderDetails.getCountPrice();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		context.put("orderList", orderList);
		context.put("appOrderInfor", appOrderInfor);
		context.put("userId", userId);
		context.put("money", money);
		context.put("purchaseTime", sdf.format(orderList.get(0).getCreateDate()));
		return new ModelAndView("pages/order/orderDel",context);
	}
	
	//查询积分明细
	@RequestMapping(params="queryIntegralDetails=key")
	@ResponseBody
	public AjaxJson queryIntegralDetails(String userId,String integralState) throws Exception{
		AjaxJson json=new AjaxJson();
		Map<String, Object> map = new HashMap<String, Object>();
			try {
				map.put("userId", userId);
				if(null != integralState && !"".equals(integralState)){
					map.put("integralState", integralState);
				}
				List<appIntegralDetails> list = appIntegral.selectByuserId(map);
				json.setObj(list);
			} catch (Exception e) {
				e.printStackTrace();
			}
		return json;
	} 
	
	//根据订单id查询订单详情
		@RequestMapping(params = "integralDetails=key")
		public ModelAndView integralDetails(String userId) {
			Map<String, Object> context = new HashMap<String, Object>();
			context.put("userId", userId);
			return new ModelAndView("pages/order/integralDetails",context);
		}
		
		//积分计算规则
	@RequestMapping(params = "IntegralRules=key")
	public ModelAndView IntegralRules(String title) {
		Map<String, Object> context = new HashMap<String, Object>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if(title.equals("01")){
			title = "积分计算规则";
		}
		paramMap.put("title", title);
		List<HelpDocument> list = helpDocumentService.queryByTitle(paramMap);
		context.put("content", list.get(0).getContent());
		context.put("title", title);
		return new ModelAndView("pages/order/IntegralRules",context);
	}	
	//退货规则
	@RequestMapping(params = "returnRules=key")
	public ModelAndView returnRules(String title) {
		Map<String, Object> context = new HashMap<String, Object>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		if(title.equals("02")){
			title = "退换货规则提示";
		}
		paramMap.put("title", title);
		List<HelpDocument> list = helpDocumentService.queryByTitle(paramMap);
		context.put("content", list.get(0).getContent());
		context.put("title", title);
		return new ModelAndView("pages/order/returnRules",context);
	}	
		
	
	//查询用户信息
	@RequestMapping(params="queryUserInfo=key")
	@ResponseBody
	public AjaxJson queryUserInfo(String userId) throws Exception{
		AjaxJson json=new AjaxJson();
		try {
			AppUserInfor appUser = batchAddService.queryByPkId(userId);
			json.setObj(appUser);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return json;
	} 
}

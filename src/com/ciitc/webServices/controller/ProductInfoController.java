package com.ciitc.webServices.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.ProductAppendixInfo;
import com.ciitc.webServices.model.ProductInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.ProductAppendixInfoService;
import com.ciitc.webServices.service.ProductInfoService;
import com.ciitc.webServices.util.DBUtil;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/productInfoController")
public class ProductInfoController extends BaseController {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Autowired(required = false)
	private ProductInfoService<ProductInfo> productInfoService = (ProductInfoService) SpringContextUtil.getBean("productInfoService");

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Autowired(required = false)
	private ProductAppendixInfoService<ProductAppendixInfo> productappendixinfoService = (ProductAppendixInfoService) SpringContextUtil.getBean("productappendixinfoService");

	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode> esbmmCodeService;

	/**
	 * 1查询耀莱数据库；2更新本地数据库；3将数据展示到页面
	 * 
	 * @author Bridge
	 * @date 2018年4月29日 下午7:55:53
	 */
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, ProductInfo productInfo, BasePage basePage) {
		try {
			updateProductInfo();

			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("productInfo", productInfo);
			paramMap.put("px", basePage.getOrder());
			List<ProductInfo> list = new ArrayList<ProductInfo>();
			try {
				list = productInfoService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 1查询耀莱数据库；2更新本地数据库
	 * 
	 * @author Bridge
	 * @date 2018年6月5日 下午4:09:25
	 */
	public void updateProductInfo() {
		// 备份，异常恢复使用
		List<ProductInfo> queryALL = productInfoService.queryALL();
		// 连接耀莱CRM数据库
		try {
			// 清空数据表
			productInfoService.deleteALL();
			Connection conn = DBUtil.makeConnection();
			// 营销项目表C_SALERIGHTFLAG ='1'销售状态“在售” C_STATUS = '2'项目状态“发行中”
			CallableStatement call = conn
					.prepareCall("SELECT  T1.L_PROSERIALNO,T1.C_STATUS,T1.C_NAME as C_NAME,  L1.C_CAPTION||REPLACE(L2.C_CAPTION,'私募产品','私募基金') as C_PROJECTTYPE,  T4.C_NAME as L_MANDATORID,  T3.C_TRUSTEENAME as C_TRUSTEENAME,  T5.C_OUTSOURCEDEALER as C_OUTSOURCEDEALER,   T6.F_MAXPRODSHARES as F_SCALE,   T1.C_DEADLINE as C_DEADLINE,  T1.F_PERMINBALANCE as F_PERMINBALANCE,  T7.C_CONTENT as C_CONTENT,  T8.c_incdisdate as C_INCOMEDISTRIBUTION,  T1.C_RAISEBANK as C_RAISEBANK,  T1.C_RAISENAME as C_RAISENAME,   T1.C_RAISEACCO as C_RAISEACCO,  CASE WHEN L3.C_CAPTION IS NOT NULL    THEN '本基金属于（'||L3.C_CAPTION||'）'||(case when L3.C_CAPTION = 'R1' then '低风险' when L3.C_CAPTION = 'R2' then '中低风险' when L3.C_CAPTION = 'R3' then '中风险' when L3.C_CAPTION = 'R4' then '中高风险' when L3.C_CAPTION = 'R5' then '高风险' end) ||'投资品种，适合风险识别、评估、承受能力为C'||REPLACE(L3.C_CAPTION,'R','')||'及以上的合格投资者。'    ELSE NULL END as C_RISKLEVEL,  t1.C_WAYFUND as C_WAYFUND,	  t1.D_EXPIRATIONDATE,	  t1.C_RISKCONTROL FROM TSALEPROJECT T1 LEFT JOIN tsaleprofundrelation T2  ON T1.L_PROSERIALNO = T2.L_PROSERIALNO LEFT JOIN TFUNDINFO T3 ON T2.C_FUNDCODE = T3.C_FUNDCODE LEFT JOIN (select * from tdictionary where l_keyno = '1404') L1  ON T3.C_OPERATEWAY = L1.C_KEYVALUE LEFT JOIN (select * from tdictionary where l_keyno = '3189') L2  ON T3.C_PDTTYPE = L2.C_KEYVALUE LEFT JOIN  TPDTMANDATOR T4 ON T3.l_mandatorid = T4.l_mandatorid LEFT JOIN TPDTFUTUASSMGR T5 ON T3.C_FUNDCODE = T5.C_FUNDCODE LEFT JOIN TPDTARLIMIT T6 ON T3.C_FUNDCODE = T6.C_FUNDCODE LEFT JOIN (SELECT * FROM TFUNDINFOVERTICAL WHERE C_PROCODE = 'C_BENCHMARK')T7 ON T3.C_FUNDCODE = T7.C_FUNDCODE left join  (SELECT  c_fundcode,        nvl(t1.c_incdisdate,t2.c_caption) c_incdisdate FROM TPDTTRUST t1 left join (select * from tdictionary where l_keyno = '1814') t2 on t1.c_syfppd = t2.c_keyvalue)T8 ON T3.C_FUNDCODE = t8.C_FUNDCODE LEFT JOIN (select * from tdictionary where l_keyno = '1408') L3  ON trim(T3.C_RISKLEVEL) = L3.C_KEYVALUE where T1.c_salerightflag = '1'");
			// 执行
			ResultSet rs = call.executeQuery();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			while (rs.next()) {
				ProductInfo productinfo = new ProductInfo();
				// ID
				productinfo.setPkId(rs.getString("L_PROSERIALNO"));
				// 项目名称
				productinfo.setProductname(rs.getString("C_NAME"));
				// 产品类型
				productinfo.setPruducttype(rs.getString("C_PROJECTTYPE"));
				// 管理人
				productinfo.setProductmanager(rs.getString("L_MANDATORID"));
				// 托管人
				productinfo.setProductcustodian(rs.getString("C_TRUSTEENAME"));
				// 外包服务商
				productinfo.setOutsourcingserviceprovider(rs.getString("C_OUTSOURCEDEALER"));
				// 募集规模,发行规模
				productinfo.setScale(rs.getString("F_SCALE"));
				//产品状态
				productinfo.setStatus(rs.getString("C_STATUS"));
				// 存续期限,项目期限
				productinfo.setDeadline(rs.getString("C_DEADLINE"));
				productinfo.setRemark1(rs.getString("C_DEADLINE").replaceAll("个月", ""));
				// 认购起点,个人投资起点
				productinfo.setPerminbalance(rs.getString("F_PERMINBALANCE"));
				// 业绩比较基准,预期年化收益率
				productinfo.setYield(rs.getString("C_CONTENT"));
				// 分红方式,收益分配方式
				productinfo.setBonustype(rs.getString("C_INCOMEDISTRIBUTION"));
				// 募集户名
				productinfo.setRaisename(rs.getString("C_RAISENAME"));
				// 募集银行
				productinfo.setRaisebank(rs.getString("C_RAISEBANK"));
				// 募集账号
				productinfo.setRaiseacco(rs.getString("C_RAISEACCO"));
				// 产品投向
				productinfo.setProductthrowup(rs.getString("C_WAYFUND"));
				// 风险收益特征
				productinfo.setRiskincome(rs.getString("C_RISKLEVEL"));
				// 产品费率,此字段在CRM系统中没有
				// productinfo.setRate(rate);
				// 产品实际到期时间
				if (rs.getString("D_EXPIRATIONDATE") != null) {
					productinfo.setActualDate(simpleDateFormat.parse(rs.getString("D_EXPIRATIONDATE")));
				}
				// 产品默认到期时间
				if (rs.getString("D_EXPIRATIONDATE") != null) {
					productinfo.setDefaultDate(simpleDateFormat.parse(rs.getString("D_EXPIRATIONDATE")));
				}
				productInfoService.addNoUUID(productinfo);
			}
			if(rs !=null){
				rs.close();
			}
			if(call !=null){
				call.close();
			}
			if(conn !=null){
				conn.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			productInfoService.deleteALL();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					productInfoService.addNoUUID(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	/**
	 * 跳转产品信息详情
	 * 
	 * @author Bridge
	 * @date 2018年4月29日 下午6:32:58
	 */
	@RequestMapping(params = "productInfo=key")
	public ModelAndView productInfo(String pkId) {
		Map<String, Object> context = getRootMap();
		// TODO 这里代码没写！！！！！！！！
		try {
			ProductInfo productinfo = productInfoService.queryById(pkId);
			// 项目名称,产品名称
			context.put("getProductName", productinfo.getProductname());
			// 产品类型
			context.put("getPruductType", productinfo.getPruducttype());
			context.put("productmanager", productinfo.getProductmanager());
			context.put("productcustodian", productinfo.getProductcustodian());
			context.put("outsourcingserviceprovider", productinfo.getOutsourcingserviceprovider());
			context.put("scale", productinfo.getScale());
			context.put("deadline", productinfo.getDeadline());
			context.put("perminbalance", productinfo.getPerminbalance());
			context.put("yield", productinfo.getYield());
			context.put("bonustype", productinfo.getBonustype());
			context.put("raisename", productinfo.getRaisename());
			context.put("raisebank", productinfo.getRaisebank());
			context.put("raiseacco", productinfo.getRaiseacco());
			context.put("productthrowup", productinfo.getProductthrowup());
			context.put("riskincome", productinfo.getRiskincome());
			context.put("rate", productinfo.getRate());
			context.put("pledge", productinfo.getPledge());
			context.put("agreement", productinfo.getAgreement());
			context.put("clause", productinfo.getClass());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			if (productinfo.getActualDate() != null) {
				context.put("actualDate", sdf.format(productinfo.getActualDate()));
			}
			if (productinfo.getDefaultDate() != null) {
				context.put("defaultDate", sdf.format(productinfo.getDefaultDate()));
			}
			// 业绩比较基准
			context.put("getBenchmark", productinfo.getYield());
			// 存续期限,项目期限
			context.put("getTerm", productinfo.getDeadline());
			// 认购起点,个人投资起点

			context.put("getSubscriptionSP", productinfo.getPerminbalance());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/ProductInfo/ProductInfoSpecific", context);
	}

	/*************************************************************************
	 * 功能描述: 跳转到产品信息查询页面
	 *************************************************************************/
	@RequestMapping(params = "ProductInfoList=key")
	public ModelAndView departList() {
		return new ModelAndView("pages/sysManager/ProductInfo/ProductInfoList");
	}

	/*************************************************************************
	 * 函数名: modify 功能描述: 编辑 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "detail=key")
	public ModelAndView detail(ProductInfo productInfo, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			productInfo = productInfoService.queryById(productInfo.getPkId());
			context.put("productInfo", productInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/ProductInfo/ProductInfoDetails", context);
	}

	/**
	 * 进入文件管理页面
	 * 
	 * @author Bridge
	 * @date 2018年5月2日 下午4:12:51
	 */
	@RequestMapping(params = "tofiles=key")
	public ModelAndView tofiles(ProductInfo productInfo, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		context.put("productid", productInfo.getPkId());
		return new ModelAndView("pages/sysManager/ProductInfo/ProductInfoFiles", context);
	}

	/**
	 * 查询文件列表
	 * 
	 * @author Bridge
	 * @date 2018年5月2日 下午5:27:52
	 */
	@RequestMapping(params = "queryFiles=key")
	public void queryFiles(HttpServletRequest request, HttpServletResponse response, ProductAppendixInfo productAppendixInfo, BasePage basePage) {
		Map<String, Object> context = getRootMap();
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("productAppendixInfo", productAppendixInfo);
			List<ProductAppendixInfo> list = new ArrayList<ProductAppendixInfo>();
			try {
				list = productappendixinfoService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 文件上传方法
	 * 
	 * @author Bridge
	 * @date 2018年5月2日 下午8:24:33
	 */
	@RequestMapping(params = "uploadFile=key")
	public void uploadFile(HttpServletRequest request, HttpServletResponse response, HttpSession session, ProductAppendixInfo productAppendixInfo, @RequestParam(value = "file", required = false) MultipartFile file) throws IllegalStateException, IOException {
		try {
			String fileName = file.getOriginalFilename();// 原始文件名
			String id = UUID.randomUUID().toString().replaceAll("-", "");
			String extension = FilenameUtils.getExtension(fileName);// 拓展名
			// 上传本地路径
			List<EsbmmCode> code1 = esbmmCodeService.getUploadAddress();
			String uploadAddress = code1.get(0).getCodeValue();
			// 真实路径，文件实际存储位置
			String pic_path = uploadAddress + id + "." + extension;
			List<EsbmmCode> code = esbmmCodeService.getAddress();
			String addre = code.get(0).getCodeValue();
			// 虚拟路径，tomcat映射
			String path1 = "/photo/" + id + "." + extension;
			File dir = new File(pic_path);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			// 上传
			file.transferTo(new File(pic_path));

			UserInfo user = ResourceUtil.getSessionUserName();

			ProductAppendixInfo pai = new ProductAppendixInfo();
			pai.setPkId(id);
			pai.setFileName(id+"."+extension);
			pai.setProductId(productAppendixInfo.getProductId());
			pai.setTrue_address(pic_path);
			pai.setUploadAddress(path1);
			pai.setOperator(user.getPkId());
			pai.setCreateDate(new Date());
			pai.setVisible("00");
			pai.setOldName(fileName);
			productappendixinfoService.addNoUUID(pai);

			JSONObject jsonObject = new JSONObject();
			// 将虚拟路径保存并返回页面
			jsonObject.put("path", addre + path1);
			// 设置响应数据的类型json
			response.setContentType("application/json; charset=utf-8");
			response.getWriter().write(jsonObject.toString());
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("服务器繁忙，上传图片失败");
		}
	}
	
	/**
	 * ***********************************************************************
	*  函数名:         
	*  功能描述: 
	*  创 建 人: 
	*  日    期: 2018年6月5日 下午7:52:53
	*  修 改 人:
	************************************************************************
	 */
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(String id,String fileName) {
		try {
			// 上传本地路径
			List<EsbmmCode> code1 = esbmmCodeService.getUploadAddress();
			String uploadAddress = code1.get(0).getCodeValue();
			String url = uploadAddress + fileName;
			 File file = new File(url);
		        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
		        if (file.exists() && file.isFile()) {
		            if (file.delete()) {
		                System.out.println("删除单个文件" + fileName + "成功！");
		               
		            } else {
		                System.out.println("删除单个文件" + fileName + "失败！");
		              
		            }
		        } else {
		            System.out.println("删除单个文件失败：" + fileName + "不存在！");
		          
		        }
			productappendixinfoService.deleteFile(id);
		} catch (Exception e) {
			e.printStackTrace();
		}
		AjaxJson json = new AjaxJson();
		json.setObj("ok");
		return json;
	}
	
	  /**
     * 删除单个文件
     *
     * @param fileName
     *            要删除的文件的文件名
     * @return 单个文件删除成功返回true，否则返回false
     */
    public static boolean deleteFile(String fileName) {
        File file = new File(fileName);
        // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
        if (file.exists() && file.isFile()) {
            if (file.delete()) {
                System.out.println("删除单个文件" + fileName + "成功！");
                return true;
            } else {
                System.out.println("删除单个文件" + fileName + "失败！");
                return false;
            }
        } else {
            System.out.println("删除单个文件失败：" + fileName + "不存在！");
            return false;
        }
    }

	/**
	 * 下载文件
	 * 
	 * @author Bridge
	 * @date 2018年5月2日 下午7:23:20
	 */
	@RequestMapping(params = "download=key")
	public void fileDowdnload(String fileid, HttpServletResponse response) {
		try {
			ArrayList<ProductAppendixInfo> queryById = productappendixinfoService.selectByPrimaryKey(fileid);
			ProductAppendixInfo productAppendixInfo = queryById.get(0);

			// 下载本地文件
			String fileName = productAppendixInfo.getFileName(); // 文件的默认保存名
			// 读到流中
			InputStream inStream = new FileInputStream(productAppendixInfo.getTrue_address());// 文件的存放路径
			// 设置输出的格式
			response.reset();
			response.setContentType("bin");
			response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
			// 循环取出流中的数据
			byte[] b = new byte[100];
			int len;
			try {
				while ((len = inStream.read(b)) > 0)
					response.getOutputStream().write(b, 0, len);
				inStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * 用于修改文件是否在APP可见
	 * 
	 * @author Bridge
	 * @date 2018年5月2日 下午8:51:34
	 */
	@RequestMapping(params = "appIsLook=key")
	public void appIsLook(String fileid, HttpServletResponse response) {
		try {
			ArrayList<ProductAppendixInfo> queryById = productappendixinfoService.selectByPrimaryKey(fileid);
			ProductAppendixInfo productAppendixInfo = queryById.get(0);

			if ("00".equals(productAppendixInfo.getVisible())) {
				productAppendixInfo.setVisible("01");
			} else if ("01".equals(productAppendixInfo.getVisible())) {
				productAppendixInfo.setVisible("00");
			}

			productappendixinfoService.update(productAppendixInfo);

			JSONObject jsonObject = new JSONObject();
			// 将虚拟路径保存并返回页面
			jsonObject.put("success", "success");
			// 设置响应数据的类型json
			response.setContentType("application/json; charset=utf-8");
			response.getWriter().write(jsonObject.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/*************************************************************************
	 * 功能描述: 跳转到产品信息查询页面
	 *************************************************************************/
	@RequestMapping(params = "detail=key")
	public ModelAndView detail() {
		return new ModelAndView("pages/sysManager/ProductInfo/ProductInfoList");
	}

}

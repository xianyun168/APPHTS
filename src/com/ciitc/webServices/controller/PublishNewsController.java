package com.ciitc.webServices.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.PublishNewsInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.PublishNewsService;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;

/*******************************************************************************
 * 模块名称: PublishNewsController 模块功能描述: 用户管理模块 创 建 人: 日 期: 修 改 人:
 ******************************************************************************/
@Controller
@RequestMapping("/publishNewsController")
public class PublishNewsController extends BaseController {	
@Autowired(required = false)
private PublishNewsService<PublishNewsInfo> publishNewsService;
@Autowired(required = false)
private EsbmmCodeService<EsbmmCode> esbmmCodeService;
@Autowired(required = false)
private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;
@Autowired(required = false)
private OperateLogService<OperateLog> operateLogService; // 记录系统日志
// 向页面返回的信息
private String message;
	
	
	/*************************************************************************
	 * 函数名: saveUpdate 功能描述: 新闻发布新增保存功能  输入参数: 返 回 值: 异 常： 创 建 人:李天一 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(MultipartHttpServletRequest murequest,HttpServletRequest request,
			PublishNewsInfo publishNews) throws Exception {
		AjaxJson j = new AjaxJson();
		if (publishNews.getPkId() != null
				&& !"".equals(publishNews.getPkId())) {
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            //String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
        	String httpUrl ="/news"+pictureUpload;
        	if(null != pictureUpload && !"".equals(pictureUpload)) {
        		publishNews.setNewsImage(pictureUpload);
    			publishNews.setNewsImageInternet(httpUrl);
        	}
			publishNews.setOperateTime(new Date());
			publishNewsService.update(publishNews);
			operateLogService.addLog("新闻发布管理", "编辑新闻发布信息", Globals.Log_Type_UPDATE);
			message="修改成功";
		} else {
			String uuid = UUID.randomUUID().toString();
			publishNews.setPkId(uuid);
			publishNews.setRemark1("0");
			publishNews.setAuditState("未提交");
			publishNews.setPublishState("未发布");
			//上传照片并保存为本地路径和互联网路径
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
            String httpUrl ="/news"+pictureUpload;
			publishNews.setNewsImage(pictureUpload);
			publishNews.setNewsImageInternet(httpUrl);
			publishNews.setOperateTime(new Date());
			publishNewsService.add(publishNews);
			operateLogService.addLog("新闻发布管理", "新增新闻发布信息", Globals.Log_Type_INSERT);
			message="保存成功";
		}
		j.setMsg(message);
		return j;
	}
	
	/*************************************************************************
	 * 函数名:audit 功能描述:新闻发布增加页面提交审核  删除信息: 返 回 值: 异 常： 创 建 人:李天一 日 期:2018/3/18 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "audit=key")
	@ResponseBody
	//使用多图上传MultipartHttpServletRequest切记form表单提交需要在jsp加上enctype="multipart/form-data" 且提交方式必须为post
	//如果在提交的form表单中使用富文本编辑器ueditor，请修改配置文件ueditor.all.js,ueditor.all.min.js
	public AjaxJson audit(MultipartHttpServletRequest murequest,HttpServletRequest request,
			PublishNewsInfo publishNews) throws Exception {
		AjaxJson j = new AjaxJson();
		if (publishNews.getPkId() != null
				&& !"".equals(publishNews.getPkId())) {
			publishNews.setAuditState("待审核");
			//上传照片并保存为本地路径和互联网路径
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
            String httpUrl ="/news"+pictureUpload;
            if(null != pictureUpload && !"".equals(pictureUpload)) {
            	publishNews.setNewsImage(pictureUpload);
    			publishNews.setNewsImageInternet(httpUrl);
            }
			publishNews.setOperateTime(new Date());
			publishNewsService.update(publishNews);
			operateLogService.addLog("新闻发布管理", "提交审核", Globals.Log_Type_UPDATE);
			message="修改成功";
		} else {
			String uuid = UUID.randomUUID().toString();
			publishNews.setPkId(uuid);
			publishNews.setRemark1("0");
			publishNews.setPublishState("未发布");
			publishNews.setAuditState("待审核");
			//上传照片并保存为本地路径和互联网路径
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
            String httpUrl ="/news"+pictureUpload;
            if(null != pictureUpload && !"".equals(pictureUpload)) {
            	publishNews.setNewsImage(pictureUpload);
    			publishNews.setNewsImageInternet(httpUrl);
            }
			publishNews.setOperateTime(new Date());
			publishNewsService.add(publishNews);
			operateLogService.addLog("新闻发布管理", "新增新闻发布信息并提交审核", Globals.Log_Type_INSERT);
			message="保存成功";
		}
		j.setMsg(message);
		return j;
	}
	
	/*************************************************************************
	 * 函数名:auditw 功能描述:新闻发布列表页面提交审核  删除信息: 返 回 值: 异 常： 创 建 人:李天一 日 期:2018/3/18 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "auditw=key")
	@ResponseBody
	//使用多图上传MultipartHttpServletRequest切记form表单提交需要在jsp加上enctype="multipart/form-data" 且提交方式必须为post
	//如果在提交的form表单中使用富文本编辑器ueditor，请修改配置文件ueditor.all.js,ueditor.all.min.js
	public AjaxJson auditw(@RequestParam(required = false)MultipartHttpServletRequest murequest,HttpServletRequest request,
			PublishNewsInfo publishNews) throws Exception {
		AjaxJson j = new AjaxJson();
		if (publishNews.getPkId() != null
				&& !"".equals(publishNews.getPkId())) {
			publishNews.setAuditState("待审核");
			publishNewsService.update(publishNews);
			publishNews.setOperateTime(new Date());
			operateLogService.addLog("新闻发布管理", "提交审核", Globals.Log_Type_UPDATE);
			message="修改成功";
		}/* else {
			String uuid = UUID.randomUUID().toString();
			publishNews.setPkId(uuid);
			publishNews.setRemark1("0");
			publishNews.setPublishState("未发布");
			publishNews.setAuditState("待审核");
        	String pictureUpload = pictureUpload(murequest);
			publishNews.setNewsImage(pictureUpload);
			publishNews.setOperateTime(new Date());
			publishNewsService.add(publishNews);
			operateLogService.addLog("新闻发布管理", "新增新闻发布信息并提交审核", Globals.Log_Type_INSERT);
			message="保存成功";
		}*/
		j.setMsg(message);
		return j;
	}
	
	/*************************************************************************
	 * 函数名: saveUpdateLB 功能描述: 轮播图保存功能  输入参数: 返 回 值: 异 常： 创 建 人:李天一 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "saveUpdateLB=key")
	@ResponseBody
	public AjaxJson saveUpdateLB(MultipartHttpServletRequest murequest,HttpServletRequest request,
			PublishNewsInfo publishNews,String roleIds) throws Exception {
		AjaxJson j = new AjaxJson();
		if (publishNews.getPkId() != null
				&& !"".equals(publishNews.getPkId())) {
			//publishNews.setNewsPublishConfiglb(new Date());
			//上传照片并保存为本地路径和互联网路径
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
            String httpUrl ="/news"+pictureUpload;
            if(null != pictureUpload && !"".equals(pictureUpload)) {
            	publishNews.setNewsImage(pictureUpload);
    			publishNews.setNewsImageInternet(httpUrl);
            }
			publishNews.setOperateTime(new Date());
			publishNews.setNewsRelationId(roleIds);
			publishNewsService.update(publishNews);
			operateLogService.addLog("新闻轮播图配置管理", "编辑轮播图配置信息", Globals.Log_Type_UPDATE);
			message="修改成功";
		} else {
			String uuid = UUID.randomUUID().toString();
			publishNews.setPkId(uuid);
			publishNews.setRemark1("1");
			publishNews.setAuditStatelb("未提交");
			publishNews.setPublishStatelb("未发布");
			publishNews.setNewsRelationId(roleIds);
			PublishNewsInfo publishNewsInfo =publishNewsService.queryById(publishNews.getNewsRelationId());
			publishNews.setNewsRelated(publishNewsInfo.getNewsTitle());
			//上传照片并保存为本地路径和互联网路径
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
            String httpUrl ="/news"+pictureUpload;
            if(null != pictureUpload && !"".equals(pictureUpload)) {
            	publishNews.setNewsImage(pictureUpload);
    			publishNews.setNewsImageInternet(httpUrl);
            }
			publishNews.setOperateTime(new Date());
			publishNewsService.add(publishNews);
			operateLogService.addLog("新闻轮播图配置管理", "新增轮播图配置信息", Globals.Log_Type_INSERT);
			message="保存成功";
		}
		j.setMsg(message);
		return j;
	}
	/*************************************************************************
	 * 函数名:auditLB 功能描述:轮播图新增页面提交审核  删除信息: 返 回 值: 异 常： 创 建 人:李天一 日 期:2018/3/18 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "auditLB=key")
	@ResponseBody
	//@RequestParam(required = false)要求了文件可传可不传，传了不加也成功，不传报错rg.springframework.web.multipart.MultipartException: The current request is not a multipart request
	//使用多图上传MultipartHttpServletRequest切记form表单提交需要在jsp加上enctype="multipart/form-data" 且提交方式必须为post
	//如果在提交的form表单中使用富文本编辑器ueditor，请修改配置文件ueditor.all.js,ueditor.all.min.js
	public AjaxJson auditLB(MultipartHttpServletRequest murequest,PublishNewsInfo publishNews,HttpServletRequest req,String roleIds) throws Exception {
		AjaxJson json = new AjaxJson();
		if (publishNews!= null&&publishNews.getPkId() != null && !"".equals(publishNews.getPkId())) {
			publishNews.setAuditStatelb("待审核");
			//上传照片并保存为本地路径和互联网路径
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
            String httpUrl ="/news"+pictureUpload;
            if(null != pictureUpload && !"".equals(pictureUpload)) {
            	publishNews.setNewsImage(pictureUpload);
    			publishNews.setNewsImageInternet(httpUrl);
            }
			publishNews.setOperateTime(new Date());
			publishNews.setNewsRelationId(roleIds);
			publishNewsService.update(publishNews);
			operateLogService.addLog("新闻轮播图配置管理", "提交审核", Globals.Log_Type_UPDATE);
			message="保存成功";
		}else {
			//UserInfo user = ResourceUtil.getSessionUserName();
			String uuid = UUID.randomUUID().toString();
			publishNews.setPkId(uuid);
			publishNews.setAuditStatelb("待审核");
			publishNews.setPublishStatelb("未发布");
			publishNews.setRemark1("1");
			publishNews.setNewsRelationId(roleIds);
			PublishNewsInfo publishNewsInfo =publishNewsService.queryById(publishNews.getNewsRelationId());
			publishNews.setNewsRelated(publishNewsInfo.getNewsTitle());
			//上传照片并保存为本地路径和互联网路径
			String pictureUpload = pictureUpload(murequest);
        	List<EsbmmCode> code = esbmmCodeService.getAddress();
            String fwqdkh = code.get(0).getCodeValue();
        	//String httpUrl =fwqdkh+"/news"+pictureUpload;
            String httpUrl ="/news"+pictureUpload;
            if(null != pictureUpload && !"".equals(pictureUpload)) {
            	publishNews.setNewsImage(pictureUpload);
    			publishNews.setNewsImageInternet(httpUrl);
            }
			publishNews.setOperateTime(new Date());
			publishNewsService.add(publishNews);
			operateLogService.addLog("新闻轮播图配置管理", "新增轮播图配置信息并提交审核", Globals.Log_Type_INSERT);
			message="保存成功";
			}
		    json.setMsg(message);
		    return json;
	}
	
	/*************************************************************************
	 * 函数名:auditLBw 功能描述:轮播图列表页面提交审核  删除信息: 返 回 值: 异 常： 创 建 人:李天一 日 期:2018/3/18 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "auditLBw=key")
	@ResponseBody
	//@RequestParam(required = false)要求了文件可传可不传，传了不加也成功，不传报错rg.springframework.web.multipart.MultipartException: The current request is not a multipart request
	//使用多图上传MultipartHttpServletRequest切记form表单提交需要在jsp加上enctype="multipart/form-data" 且提交方式必须为post
	//如果在提交的form表单中使用富文本编辑器ueditor，请修改配置文件ueditor.all.js,ueditor.all.min.js
	public AjaxJson auditLBw(@RequestParam(required = false)MultipartHttpServletRequest murequest,PublishNewsInfo publishNews,HttpServletRequest req) throws Exception {
		AjaxJson json = new AjaxJson();
		if (publishNews!= null&&publishNews.getPkId() != null && !"".equals(publishNews.getPkId())) {
			publishNews.setAuditStatelb("待审核");
			publishNews.setOperateTime(new Date());
			publishNewsService.update(publishNews);
			operateLogService.addLog("新闻轮播图配置管理", "提交审核", Globals.Log_Type_UPDATE);
			message="保存成功";
		}/*else {
			//UserInfo user = ResourceUtil.getSessionUserName();
			String uuid = UUID.randomUUID().toString();
			publishNews.setPkId(uuid);
			publishNews.setAuditStatelb("待审核");
			publishNews.setRemark1("1");
			String pictureUpload = pictureUpload(murequest);
			publishNews.setNewsImage(pictureUpload);
			publishNews.setOperateTime(new Date());
			publishNewsService.add(publishNews);
			operateLogService.addLog("新闻轮播图配置管理", "新增轮播图配置信息", Globals.Log_Type_INSERT);
			message="保存成功";
			}*/
		    json.setMsg(message);
		    return json;
	}
	/*************************************************************************
	 * 函数名: addLB 功能描述: 跳转到轮播图增加页面   输入参数: 返 回 值: 异 常： 创 建 人:李天一  日 期:2018/03/12
	 *************************************************************************/
	@RequestMapping(params = "addLB=key")
	public ModelAndView addLB(HttpServletRequest req,
			PublishNewsInfo publishNews) {
		Map<String, Object> context = getRootMap();
		return new ModelAndView("pages/News/publishNewsAddLB",context);
	}
	/*************************************************************************
	 * 函数名: sysPermissionInfoAddLB 功能描述: 跳转角色数据列表页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "sysPermissionInfoAddLB=key")
	public ModelAndView sysPermissionInfoAddLB() {
		return new ModelAndView("pages/News/SysPermissionInfoAddLB");
	}
	
	/*************************************************************************
	 * 函数名: modify 功能描述: 跳转到新闻修改页面 输入参数: 返 回 值: 异 常： 创 建 人:李天一 日 期: 2018-01-31修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(PublishNewsInfo publishNews, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		if(null != publishNews){
			try {
				publishNews = publishNewsService.queryById(publishNews.getPkId());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String ss = sdf.format(publishNews.getNewsPublishConfig());
				context.put("aaa", ss);
				String pkid = esbmmCodeTypeService.queryByTypeName("host").get(0).getPkId();
	            String fwqdkh = esbmmCodeService.queryByTypeIds(pkid).get(0).getCodeValue();
				String path = fwqdkh + publishNews.getNewsImageInternet();
				publishNews.setNewsImage(path);
				context.put("publishNewsInfo", publishNews);
				context.put("imghead", publishNews.getNewsImage());
				// 回显照片
				/*String hxzp= publishNews.getNewsImage();
				List<EsbmmCode> addrelist = esbmmCodeService.getAddseList();
				EsbmmCode addr = addrelist.get(0);
				String realPath = addr.getCodeValue();
				String oldPath = realPath + hxzp;
				String baisc = req.getSession().getServletContext().getRealPath("/");	
				baisc = baisc.replaceAll("\\\\", "/");
				String newPath = baisc + hxzp;
				try {
					oldPath = oldPath.substring(0, oldPath.lastIndexOf("/"));
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("找不到需要转换的图片路径");
					e.printStackTrace();
				}
				newPath = newPath.substring(0, newPath.lastIndexOf("/"));
				try {
					copyFolder(oldPath, newPath);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("照片文件已被删除或更改，不存在");
					e.printStackTrace();
				}*/
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		return new ModelAndView("pages/News/publishNewsModify",
				context);
	}
	
	/*************************************************************************
	 * 函数名: modifyLB 功能描述: 跳转到轮播图修改页面 输入参数: 返 回 值: 异 常： 创 建 人:李天一 日 期: 2018-01-31修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "modifyLB=key")
	public ModelAndView modifyLB(PublishNewsInfo publishNews, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		if(null != publishNews){
			try {
				publishNews = publishNewsService.queryById(publishNews.getPkId());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String ss = sdf.format(publishNews.getNewsPublishConfiglb());
				context.put("aaa", ss);
				String pkid = esbmmCodeTypeService.queryByTypeName("host").get(0).getPkId();
	            String fwqdkh = esbmmCodeService.queryByTypeIds(pkid).get(0).getCodeValue();
				String path = fwqdkh + publishNews.getNewsImageInternet();
				publishNews.setNewsImage(path);
				context.put("publishNewsInfo", publishNews);
				context.put("imghead", publishNews.getNewsImage());
				// 回显照片
				/*String hxzp= publishNews.getNewsImage();
				List<EsbmmCode> addrelist = esbmmCodeService.getAddseList();
				EsbmmCode addr = addrelist.get(0);
				String realPath = addr.getCodeValue();
				String oldPath = realPath + hxzp;
				String baisc = req.getSession().getServletContext().getRealPath("/");	
				baisc = baisc.replaceAll("\\\\", "/");
				String newPath = baisc + hxzp;
				try {
					oldPath = oldPath.substring(0, oldPath.lastIndexOf("/"));
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("找不到需要转换的图片路径");
				}
				newPath = newPath.substring(0, newPath.lastIndexOf("/"));
				try {
					copyFolder(oldPath, newPath);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("照片文件已被删除或更改，不存在");
					e.printStackTrace();
				}*/
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}	
		return new ModelAndView("pages/News/publishNewsLBModify",
				context);
	}
	
	/*************************************************************************
	 * 函数名: delete 功能描述:删除新闻  删除信息 : 返 回 值: 异 常： 创 建 人:李天一 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "delete=key")
	@ResponseBody
	public AjaxJson delete(PublishNewsInfo publishNews) throws Exception {
		AjaxJson json = new AjaxJson();
		if (publishNews.getPkId() != null
				&& !"".equals(publishNews.getPkId())) {
			publishNewsService.delete(publishNews.getPkId());
		}
		operateLogService.addLog("新闻发布管理", "删除新闻发布信息", Globals.Log_Type_DEL);
		message="删除成功";
		json.setObj(message);
		return json;
	}
	
	
	/*************************************************************************
	 * 函数名:publish 功能描述:取消发布新闻  删除信息: 返 回 值: 异 常： 创 建 人:李天一 日 期:2018/3/18 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "cancelpublish=key")
	@ResponseBody
	public AjaxJson publish(PublishNewsInfo publishNews,HttpServletRequest req) throws Exception {
		AjaxJson json = new AjaxJson();
		if (publishNews!= null&&publishNews.getPkId() != null && !"".equals(publishNews.getPkId())) {
			publishNews.setPublishState("未发布");
			publishNews.setAuditState("未提交");
			publishNews.setNewsAuditor("");
			publishNews.setOperateTime(new Date());
			publishNewsService.update(publishNews);
			operateLogService.addLog("新闻发布管理", "新闻信息撤销发布", Globals.Log_Type_OTHER);
			message="保存成功";
		}
		    json.setMsg(message);
		    return json;
	}
	/*************************************************************************
	 * 函数名:publishLB 功能描述:取消发布轮播图  删除信息: 返 回 值: 异 常： 创 建 人:李天一 日 期:2018/3/18 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "cancelpublishLB=key")
	@ResponseBody
	public AjaxJson publishLB(PublishNewsInfo publishNews,HttpServletRequest req) throws Exception {
		AjaxJson json = new AjaxJson();
		if (publishNews!= null&&publishNews.getPkId() != null && !"".equals(publishNews.getPkId())) {
			publishNews.setPublishStatelb("未发布");
			publishNews.setAuditStatelb("未提交");
			publishNews.setNewsAuditorlb("");
			publishNews.setOperateTime(new Date());
			publishNewsService.update(publishNews);
			operateLogService.addLog("轮播图配置管理", "轮播图撤销发布", Globals.Log_Type_OTHER);
			message="保存成功";
		}
		    json.setMsg(message);
		    return json;
	}
	
	/*******************************************************************************************
	 * 函数名: datagrid 功能描述: 查询新闻列表  输入参数: 返 回 值: 异 常： 创 建 人: 李天一 日 期: 修 改 人: 日 期:
	 *******************************************************************************************/
	@RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request,
			HttpServletResponse response, PublishNewsInfo publishNews, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);	
			//如果不把对象put进去，paraMap无值，报getProperty(null,XXX)的错误
			paramMap.put("publishNewsInfo", publishNews);		
			List<PublishNewsInfo> list = new ArrayList<PublishNewsInfo>();
			try {
				list = publishNewsService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*******************************************************************************************
	 * 函数名: datagrids 功能描述: 查询轮播图关联新闻审核状态为通过并且发布状态为已发布的所有数据  输入参数: 返 回 值: 异 常： 创 建 人: 李天一 日 期: 修 改 人: 日 期:
	 *******************************************************************************************/
	@RequestMapping(params = "datagrids=key")
	public void datagrids(HttpServletRequest request,
			HttpServletResponse response, PublishNewsInfo publishNews, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);	
			//如果不把对象put进去，paraMap无值，报getProperty(null,XXX)的错误
			paramMap.put("publishNewsInfo", publishNews);		
			List<PublishNewsInfo> list = new ArrayList<PublishNewsInfo>();
			try {
				list = publishNewsService.queryByLists(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*******************************************************************************************
	 * 函数名: datagrid1 功能描述: 查询轮播图列表  输入参数: 返 回 值: 异 常： 创 建 人: 李天一 日 期: 修 改 人: 日 期:
	 *******************************************************************************************/
	@RequestMapping(params = "datagridLB=key")
	public void datagrid1(HttpServletRequest request,
			HttpServletResponse response, PublishNewsInfo publishNews, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);	
			//如果不把对象put进去，paraMap无值，报getProperty(null,XXX)的错误
			paramMap.put("publishNewsInfo", publishNews);		
			List<PublishNewsInfo> list = new ArrayList<PublishNewsInfo>();
			try {
				list = publishNewsService.queryByList1(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*******************************************************************************************
	 * 函数名: datagrid2 功能描述: 新闻发布审核查询列表  输入参数: 返 回 值: 异 常： 创 建 人: 李天一 日 期: 修 改 人: 日 期:
	 *******************************************************************************************/
	@RequestMapping(params = "datagrid2=key")
	public void datagrid2(HttpServletRequest request,
			HttpServletResponse response, PublishNewsInfo publishNews, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);	
			//如果不把对象put进去，paraMap无值，报getProperty(null,XXX)的错误
			paramMap.put("publishNewsInfo", publishNews);		
			List<PublishNewsInfo> list = new ArrayList<PublishNewsInfo>();
			try {
				list = publishNewsService.queryByList2(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*******************************************************************************************
	 * 函数名: datagrid3 功能描述: 新闻轮播图审核查询列表  输入参数: 返 回 值: 异 常： 创 建 人: 李天一 日 期: 修 改 人: 日 期:
	 *******************************************************************************************/
	@RequestMapping(params = "datagrid3=key")
	public void datagrid3(HttpServletRequest request,
			HttpServletResponse response, PublishNewsInfo publishNews, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);	
			//如果不把对象put进去，paraMap无值，报getProperty(null,XXX)的错误
			paramMap.put("publishNewsInfo", publishNews);		
			List<PublishNewsInfo> list = new ArrayList<PublishNewsInfo>();
			try {
				list = publishNewsService.queryByList3(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*************************************************************************
	 * 函数名: publishNewsList 功能描述: 跳转到新闻发布页面  输入参数: 返 回 值: 异 常： 创 建 人:李天一  日 期:2018/03/12
	 *************************************************************************/
	@RequestMapping(params = "publishNewsList=key")
	public ModelAndView publishNewsList() {
		return new ModelAndView("pages/News/publishNewsList");
	}
	/*************************************************************************
	 * 函数名: publishNewsListLB 功能描述: 跳转到轮播图发布页面  输入参数: 返 回 值: 异 常： 创 建 人:李天一  日 期:2018/03/12
	 *************************************************************************/
	@RequestMapping(params = "publishNewsListLB=key")
	public ModelAndView publishNewsListLB() {
		return new ModelAndView("pages/News/publishNewsListLB");
	}
	/*************************************************************************
	 * 函数名: newsAuditList 功能描述: 跳转到新闻审核页面 输入参数: 返 回 值: 异 常： 创 建 人:李天一  日 期:2018/03/12
	 *************************************************************************/
	@RequestMapping(params = "newsAuditList=key")
	public ModelAndView newsAuditList() {
		return new ModelAndView("pages/News/newsAuditList");
	}
	/*************************************************************************
	 * 函数名: newsAuditListLB 功能描述: 跳转到新闻审核页面 输入参数: 返 回 值: 异 常： 创 建 人:李天一  日 期:2018/03/12
	 *************************************************************************/
	@RequestMapping(params = "newsAuditListLB=key")
	public ModelAndView newsAuditListLB() {
		return new ModelAndView("pages/News/newsAuditListLB");
	}
	/*************************************************************************
	 * 函数名: add 功能描述: 跳转到新闻增加页面   输入参数: 返 回 值: 异 常： 创 建 人:李天一  日 期:2018/03/12
	 *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(HttpServletRequest req,
			PublishNewsInfo publishNews) {
		Map<String, Object> context = getRootMap();
		return new ModelAndView("pages/News/publishNewsAdd",context);
	}
	
	/*************************************************************************
	 * 函数名: details 功能描述: 新闻发布详情页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "detials=key")
	public ModelAndView detials(PublishNewsInfo publishNews, HttpServletRequest req) {

		Map<String, Object> context = getRootMap();
			try {
			    publishNews =publishNewsService.queryById(publishNews.getPkId());
			    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				String ss = sdf.format(publishNews.getNewsPublishConfig());
				context.put("aaa", ss);
				String pkid = esbmmCodeTypeService.queryByTypeName("host").get(0).getPkId();
	            String fwqdkh = esbmmCodeService.queryByTypeIds(pkid).get(0).getCodeValue();
				String path = fwqdkh + publishNews.getNewsImageInternet();
				publishNews.setNewsImage(path);
				context.put("publishNewsInfo", publishNews);
				context.put("imghead", publishNews.getNewsImage());
				// 回显照片
				/*String hxzp= publishNews.getNewsImage();
				List<EsbmmCode> addrelist = esbmmCodeService.getAddseList();
				EsbmmCode addr = addrelist.get(0);
				String realPath = addr.getCodeValue();
				String oldPath = realPath + hxzp;
				String baisc = req.getSession().getServletContext().getRealPath("/");	
				baisc = baisc.replaceAll("\\\\", "/");
				String newPath = baisc + hxzp;
				try {
					oldPath = oldPath.substring(0, oldPath.lastIndexOf("/"));
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("找不到图片路径");
				}
				newPath = newPath.substring(0, newPath.lastIndexOf("/"));
				try {
					copyFolder(oldPath, newPath);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println("照片文件已被删除或更改，不存在");
					e.printStackTrace();
				}*/
			} catch (Exception e) {
				e.printStackTrace();
			}
		return new ModelAndView("pages/News/publishNewsDetails", context);	
	}
	
	/*************************************************************************
	 * 函数名: details 功能描述: 新闻轮播图详情页面 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "detialsLB=key")
	public ModelAndView detialsLB(PublishNewsInfo publishNews, HttpServletRequest req) {

		Map<String, Object> context = getRootMap();
				try {
				    publishNews =publishNewsService.queryById(publishNews.getPkId());
				    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String ss = sdf.format(publishNews.getNewsPublishConfiglb());
					context.put("aaa", ss);
					String pkid = esbmmCodeTypeService.queryByTypeName("host").get(0).getPkId();
		            String fwqdkh = esbmmCodeService.queryByTypeIds(pkid).get(0).getCodeValue();
					String path = fwqdkh + publishNews.getNewsImageInternet();
					publishNews.setNewsImage(path);
					context.put("publishNewsInfo", publishNews);
					context.put("imghead", publishNews.getNewsImage());
					// 回显照片
					/*String hxzp= publishNews.getNewsImage();
					List<EsbmmCode> addrelist = esbmmCodeService.getAddseList();
					EsbmmCode addr = addrelist.get(0);
					String realPath = addr.getCodeValue();
					String oldPath = realPath + hxzp;
					String baisc = req.getSession().getServletContext().getRealPath("/");	
					baisc = baisc.replaceAll("\\\\", "/");
					String newPath = baisc + hxzp;
					try {
						oldPath = oldPath.substring(0, oldPath.lastIndexOf("/"));
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("找不到图片路径");
					}
					newPath = newPath.substring(0, newPath.lastIndexOf("/"));
					try {
						copyFolder(oldPath, newPath);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("照片文件已被删除或更改，不存在");
						e.printStackTrace();
					}*/
				} catch (Exception e) {
					e.printStackTrace();
				}
		return new ModelAndView("pages/News/publishNewsDetailsLB", context);	
	}
	/*************************************************************************
	 * 函数名: showDetials 功能描述: 跳转到新闻具体审核页面(通过、未通过)  输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "showDetials=key")
	public ModelAndView showDetials(PublishNewsInfo publishNews, HttpServletRequest req) {

		Map<String, Object> context = getRootMap();
				try {
				    publishNews =publishNewsService.queryById(publishNews.getPkId());
				    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String ss = sdf.format(publishNews.getNewsPublishConfig());
					context.put("aaa", ss);
					String pkid = esbmmCodeTypeService.queryByTypeName("host").get(0).getPkId();
		            String fwqdkh = esbmmCodeService.queryByTypeIds(pkid).get(0).getCodeValue();
					String path = fwqdkh + publishNews.getNewsImageInternet();
					publishNews.setNewsImage(path);
					context.put("imghead", publishNews.getNewsImage());
					// 回显照片
					/*String hxzp= publishNews.getNewsImage();
					List<EsbmmCode> addrelist = esbmmCodeService.getAddseList();
					EsbmmCode addr = addrelist.get(0);
					String realPath = addr.getCodeValue();
					String oldPath = realPath + hxzp;
					String baisc = req.getSession().getServletContext().getRealPath("/");	
					baisc = baisc.replaceAll("\\\\", "/");
					String newPath = baisc + hxzp;
					try {
						oldPath = oldPath.substring(0, oldPath.lastIndexOf("/"));
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("找不到图片路径");
					}
					newPath = newPath.substring(0, newPath.lastIndexOf("/"));
					try {
						copyFolder(oldPath, newPath);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("照片文件已被删除或更改，不存在");
						e.printStackTrace();
					}*/
					context.put("publishNewsInfo", publishNews);
				} catch (Exception e) {
					e.printStackTrace();
				}
		return new ModelAndView("pages/News/publishNewsShowDetails", context);	
	}
	/*************************************************************************
	 * 函数名: showDetialsLB 功能描述: 跳转到轮播图具体审核页面 (通过、未通过) 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "showDetialsLB=key")
	public ModelAndView showDetialsLB(PublishNewsInfo publishNews, HttpServletRequest req) {

		Map<String, Object> context = getRootMap();
				try {
				    publishNews =publishNewsService.queryById(publishNews.getPkId());
				    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					String ss = sdf.format(publishNews.getNewsPublishConfiglb());
					context.put("aaa", ss);
					String pkid = esbmmCodeTypeService.queryByTypeName("host").get(0).getPkId();
		            String fwqdkh = esbmmCodeService.queryByTypeIds(pkid).get(0).getCodeValue();
					String path = fwqdkh + publishNews.getNewsImageInternet();
					publishNews.setNewsImage(path);
					context.put("imghead", publishNews.getNewsImage());
					// 回显照片
					/*String hxzp= publishNews.getNewsImage();
					List<EsbmmCode> addrelist = esbmmCodeService.getAddseList();
					EsbmmCode addr = addrelist.get(0);
					String realPath = addr.getCodeValue();
					String oldPath = realPath + hxzp;
					String baisc = req.getSession().getServletContext().getRealPath("/");	
					baisc = baisc.replaceAll("\\\\", "/");
					String newPath = baisc + hxzp;
					try {
						oldPath = oldPath.substring(0, oldPath.lastIndexOf("/"));
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println("找不到图片路径");
					}
					newPath = newPath.substring(0, newPath.lastIndexOf("/"));
					try {
						copyFolder(oldPath, newPath);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						System.out.println("照片文件已被删除或更改，不存在");
						e.printStackTrace();
					}*/
					context.put("publishNewsInfo", publishNews);
				} catch (Exception e) {
					e.printStackTrace();
				}
		return new ModelAndView("pages/News/publishNewsShowDetails1", context);	
	}
	
	/*************************************************************************
	 * 函数名: through 功能描述: 审核通过  输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
		@RequestMapping(params = "through=key")
		@ResponseBody
		public AjaxJson through(MultipartHttpServletRequest murequest,
			 HttpServletRequest request,PublishNewsInfo publishNews) throws Exception {
			AjaxJson j = new AjaxJson();
			UserInfo user = ResourceUtil.getSessionUserName();
			try {
				if (publishNews != null && publishNews.getPkId() != null
						&& !"".equals(publishNews.getPkId())) {
					PublishNewsInfo pp =publishNewsService.queryById(publishNews.getPkId());
					pp.setNewsAuditOpinion(publishNews.getNewsAuditOpinion());
					if ("0".equals(pp.getRemark1())) {
						pp.setNewsAuditor(user.getUserName());
						pp.setAuditState("通过");
						pp.setPublishState("已发布");
					} else {
						pp.setNewsAuditorlb(user.getUserName());
						pp.setAuditStatelb("通过");
						pp.setPublishStatelb("已发布");
					}
					publishNews.setOperateTime(new Date());
					publishNewsService.update(pp);
				}
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println("获取的对象没有数据,请检查!");
			}
			 message = "审核通过";
			j.setMsg(message);
			return j;
		}
		
		/*************************************************************************
		 * 函数名: rejected 功能描述: 审核驳回  输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
		 *************************************************************************/
		@RequestMapping(params = "rejected=key")
		@ResponseBody
		public AjaxJson rejected(MultipartHttpServletRequest murequest,
				HttpServletRequest request, PublishNewsInfo publishNews)
				throws Exception {
			AjaxJson j = new AjaxJson();
			// 审核状态
			UserInfo user = ResourceUtil.getSessionUserName();
			if (publishNews != null && publishNews.getPkId() != null
					&& !"".equals(publishNews.getPkId())){
				PublishNewsInfo pp =publishNewsService.queryById(publishNews.getPkId());	
				if ("0".equals(pp.getRemark1())) {
				pp.setNewsAuditor(user.getUserName());
				pp.setAuditState("未通过");
				}else{
					pp.setNewsAuditorlb(user.getUserName());
					pp.setAuditStatelb("未通过");
				}
				publishNews.setOperateTime(new Date());
				publishNewsService.update(pp);
			}
			message = "审核驳回";
			j.setMsg(message);
			return j;
		}
		
	/*************************************************************************
	 * 函数名: selectnNews 功能描述: 查询前台数据显示列表  输入参数: 返 回 值: 异 常： 创 建 人:李天一 日 期: 修 改 人: 日 期:
	 * @throws Exception 
	 *************************************************************************/
	/*@RequestMapping(params = "selectnNews=key")
	@ResponseBody
	public AjaxJson selectnNews(HttpServletRequest request,PublishNewsInfo publishNews) throws Exception {
		AjaxJson j = new AjaxJson();
		Map<String, Object> context = getRootMap();
		List<PublishNewsInfo> list = new ArrayList<PublishNewsInfo>();
		list = publishNewsService.queryByNews();
		context.put("list", list);
	     j.setObj(list);
		 return j;
	}*/
	
	
	/*****************************************************************************************
	 * 函数名: show 功能描述: 跳转到具体新闻页面  输入参数: 返 回 值: 异 常： 创 建 人:李天一 日 期: 修 改 人: 日 期:
	 * @throws Exception 
	 *****************************************************************************************/
	/*@RequestMapping(params = "show=key")
	public ModelAndView show(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		PublishNewsInfo content = publishNewsService.queryById(pkId);
		context.put("content", content);
		return new ModelAndView("pages/ShowNews/newsDetail",context);
	}*/
	
	/*************************************************************************
	 * 函数名: pictureUpload 功能描述: 上传照片 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	public  String pictureUpload(MultipartHttpServletRequest murequest) {
		MultipartFile file = murequest.getFile("pictureUpload");
		return uploadFileCustomerHeadOne(file);
	}
	
	/*************************************************************************
	 * 函数名: uploadFileCustomerHeadOne 功能描述: 复制上传的照片到文件夹  输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	public  synchronized String uploadFileCustomerHeadOne(MultipartFile muFile ) {
		// 服务器保存图片路径-
		//HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		 /*request.getSession().getServletContext() 获取的是Servlet容器对象，相当于tomcat容器了
		getRealPath("/") 获取实际路径，“/”指代项目根目录，所以代码返回的是项目在容器中的实际发布运行的根路径*/
		List<EsbmmCode> addrelist = esbmmCodeService.getAddseList();
		EsbmmCode address = addrelist.get(0);
		String basic = address.getCodeValue();
		//String realPath = request.getSession().getServletContext().getRealPath("/");
		// 获取上传文件的保存名称、服务器本地保存路径、后缀，拼接成文件地址
		//HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		//String path = request.getContextPath();
		//String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
		if (muFile.getSize() > 0) {
			String fileNamess = getPicNamePathSuffix(basic);
			File file = new File(basic + fileNamess);
			try {
				//复制上传的文件到目录里
				muFile.transferTo(file);
				return fileNamess;
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/*************************************************************************
	 * 函数名: getPicNamePathSuffix 功能描述: 获取上传文件的保存名称（数据字典配置路径+文件+后缀,拼接成文件地址） 
	 *************************************************************************/
	public String getPicNamePathSuffix(String basic) {
		// 根据系统时间生成上传后保存的文件名
		long now = System.currentTimeMillis();
		//取得生成毫秒数
		String prefix = String.valueOf(now);
		SimpleDateFormat sdfDateFormat = new SimpleDateFormat("yyMMdd");
		String picPath = "/IMG/" + sdfDateFormat.format(new Date())
				+ "News" + "/";
		// 创建文件目录
		File picSaveFile = new File(basic + picPath);		
		if (!picSaveFile.exists()) {
			try {
				picSaveFile.mkdirs();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// 文件的后缀
		String suffix = ".jpg";
		PublishNewsInfo publishNewsInfo = new PublishNewsInfo();
		publishNewsInfo.setNewsImage(picPath);
		String ss = picPath + prefix + suffix;
		return ss ;
	}
	
	/**
	 * 复制整个文件夹内容
	 * 
	 * @param oldPath
	 *            String 原文件路径 如：D:/apache-tomcat-7.0.40/IMG/upload/180419News
	 * @param newPath
	 *            String 复制后路径 如：D:/apache-tomcat-7.0.40/wtpwebapps/APPHTS/IMG/upload/180419News
	 * @return boolean
	 */
	/*************************************************************************
	 * 函数名: copyFolder 功能描述: 复制生成的照片文件到项目目录下
	 *************************************************************************/
	public void copyFolder(String oldPath, String newPath) {
		try {
			(new File(newPath)).mkdirs(); // 如果文件夹不存在 则建立新文件夹
			File a = new File(oldPath);
			String[] file = a.list();
			File temp = null;
			try {
				for (int i = 0; i < file.length; i++) {
					if (oldPath.endsWith(File.separator)) {
						temp = new File(oldPath + file[i]);
					} else {

						temp = new File(oldPath + File.separator + file[i]);
					}
					if (temp.isFile()) {
						FileInputStream input = new FileInputStream(temp);
						FileOutputStream output = new FileOutputStream(newPath
								+ "/" + (temp.getName()).toString());
						byte[] b = new byte[1024 * 5];
						int len;
						while ((len = input.read(b)) != -1) {
							output.write(b, 0, len);
						}
						output.flush();
						output.close();
						input.close();
					}
					if (temp.isDirectory()) {// 如果是子文件夹
						copyFolder(oldPath + "/" + file[i], newPath + "/"
								+ file[i]);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println("照片文件已被删除或更改，不存在");
				e.printStackTrace();
			}
		} catch (Exception e) {
			System.out.println("复制整个文件夹内容操作出错");
			e.printStackTrace();

		}

	}
	

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}

package com.ciitc.webServices.controller;

import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.EquitiesDetail;
import com.ciitc.webServices.model.EquitiesUseDetails;
import com.ciitc.webServices.model.SignMessageQuery;
import com.ciitc.webServices.model.UserInforEq;
import com.ciitc.webServices.service.AppUserInforService;
import com.ciitc.webServices.service.CPTfundbrokerService;
import com.ciitc.webServices.service.EquitiesDetailService;
import com.ciitc.webServices.service.EquitiesUseDetailsService;
import com.ciitc.webServices.util.GK_AppUtil;
import com.ciitc.webServices.util.KfcrmCustor;

import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
@Controller
@RequestMapping("/equitiesUseDetailsController")
public class EquitiesUseDetailsController extends BaseController{
	@Autowired(required = false)
	private AppUserInforService<AppUserInfor> appUserInforService;
	@Autowired(required = false)
	private EquitiesUseDetailsService<EquitiesUseDetails> equitiesUseDetailsService;
	@Autowired(required = false)
	private EquitiesDetailService<EquitiesDetail> equitiesDetailService;
	@Autowired(required = false)
	private CPTfundbrokerService<CPTfundbroker> cpTfundbrokerService;
	
	/**
	 * 
	 *导出客户权益
	 */
	@RequestMapping(params = "exportList=key")
	public void exportList(HttpServletResponse response, HttpServletRequest request, SignMessageQuery signMessageQuery)
			throws Exception {
		List<AppUserInfor> list = new ArrayList<AppUserInfor>();
		List<UserInforEq> list1 = new ArrayList<UserInforEq>();
		list = appUserInforService.queryTest();
		if (list.size() > 0) {
			int i=0;
			for (AppUserInfor appUserInfor : list) {
				UserInforEq userInforEq = new UserInforEq();
				//手机号
				userInforEq.setPhoneNumber(appUserInfor.getPhoneNumber());
				//客户编号
				userInforEq.setCusstomerId(appUserInfor.getCustomerId());
				//客户姓名
				userInforEq.setUserName(appUserInfor.getUserName());
				//证件号码
				userInforEq.setIdcardNumber(appUserInfor.getIdcardNumber());
				//证件类型
				userInforEq.setIdcardType(appUserInfor.getIdcardType());
				//客户等级
				userInforEq.setCustomerLevel(appUserInfor.getCustomerLevel());
				//理财师编号
				userInforEq.setFinancialId(appUserInfor.getFinancialId());
				//根据理财师编号查询理财师姓名
				String financialName = cpTfundbrokerService.selectNameByKey(userInforEq.getFinancialId());
				//理财师姓名
				userInforEq.setFinancialName(financialName);
				//根据客户手机号查询所属分公司
				KfcrmCustor kfcrmCustor = new KfcrmCustor();
				String companyName = kfcrmCustor.queryCompanyByPhone(appUserInfor.getPhoneNumber());
				//所属分公司
				userInforEq.setCompanyName(companyName);
				//所属大区
				userInforEq.setRegion("null");
				//
				//累计投资金额
				userInforEq.setCountMoney(100);
				int maxMoney = consultation(appUserInfor.getCustomerId());
				//持仓峰值
				userInforEq.setMaxMoney(maxMoney);
				//电话咨询
				if("5".equals(appUserInfor.getCustomerLevel())) {
					userInforEq.setConsultation("null");
				}else {
					if(maxMoney!=0) {
						userInforEq.setConsultation("无限次");
					}else {
						userInforEq.setConsultation("null");
					}
				}
				//权益次数
				List<EquitiesDetail> equilList = equitiesNum(appUserInfor.getCustomerLevel(),appUserInfor.getCustomerId());
				int famousDoctorNum = 0;
				int prvivateDoctorNum = 0;
				if(equilList.size()>0) {
					for (EquitiesDetail equitiesDetail : equilList) {
						if(equitiesDetail.getEquitiesName().contains("名医看诊")) {
							//名医看诊权益次数
							famousDoctorNum = Integer.valueOf(equitiesDetail.getEquitiesNum());
						}else if(equitiesDetail.getEquitiesName().contains("私人医生")) {
							//私人医生权益次数
							prvivateDoctorNum = Integer.valueOf(equitiesDetail.getEquitiesNum());
						}
					}
				}
				//名医看诊权益次数
				userInforEq.setFamousDoctorNum(famousDoctorNum);
				//私人医生权益次数
				userInforEq.setPrvivateDoctorNum(prvivateDoctorNum);
				
				//根据客户编号查询客户权益使用明细
				List<EquitiesUseDetails> equitiesUseList = new ArrayList<EquitiesUseDetails>();
				equitiesUseList = equitiesUseDetailsService.queryByCustomerIds(appUserInfor.getCustomerId());
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				//使用权益明细
				String equitiesUse = "";
				for (EquitiesUseDetails equitiesUseDetails : equitiesUseList) {
					equitiesUse+="权益预约时间："+sdf.format(equitiesUseDetails.getMedicalTime())+",";
					equitiesUse+="权益名称："+equitiesUseDetails.getEquitiesName()+";"+"\r\n"+"";
				}
				userInforEq.setEquitiesUsedDetail(equitiesUse);
				//剩余权益明细
				String equitiesDetail=queryEquitiesNum(appUserInfor.getCustomerLevel(),appUserInfor.getCustomerId());
				userInforEq.setEquitiesDetail(equitiesDetail);
				list1.add(userInforEq);
				
				System.out.println(i++);
			}
			System.out.println(list1.size());
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/x-download");
			excel1(list1, response,request);
		}

	}
	//根据客户等级查询权益剩余次数
	public List<EquitiesDetail> equitiesNum(String customerLevel,String customerId) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerLevel",customerLevel);
		//根据客户等级和国康产品编号查询客户的权限
		List<EquitiesDetail> equitiesDetailsList = equitiesDetailService.queryByLevel(paramMap);
		return equitiesDetailsList;
	}
	//根据客户等级查询权益剩余次数
	public String queryEquitiesNum(String customerLevel,String customerId) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerLevel",customerLevel);
		//剩余权益次数
		String equitiesNums = "";
		//根据客户等级和国康产品编号查询客户的权限
		List<EquitiesDetail> equitiesDetailsList = equitiesDetailService.queryByLevel(paramMap);
		if(equitiesDetailsList.size() > 0) {
			//获取当前年份
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			String start =String.valueOf(year)+"-01-01";
			//当前时间
			Date now = new Date();
			String end = sdf.format(now);
			String equitiesNum = "";
			for (EquitiesDetail equitiesDetail : equitiesDetailsList) {
				String equitiesId = equitiesDetail.getEquitiesId();
				//权益次数
				String num = equitiesDetail.getEquitiesNum();
				Integer nums = Integer.valueOf(num);
				//使用次数
				Map<String, Object> mapGK = new HashMap<String, Object>();
				mapGK.put("start", start);
				mapGK.put("end", end);
				mapGK.put("customerId", customerId);
				mapGK.put("equitiesId", equitiesId);
				Integer count = equitiesUseDetailsService.queryByCustomerId(mapGK);
				if(nums > count) {
					equitiesNum = String.valueOf(nums-count);
				}else {
					equitiesNum = "0";
				}
				equitiesNums+="权益名称："+equitiesDetail.getEquitiesName()+"权益剩余次数："+equitiesNum+";"+"\r\n"+"";
			}
		} 
		return equitiesNums;
	}
	
	//查询客户是否存在电话咨询
	private int consultation(String customerId) {
		GK_AppUtil gkAppUtil = new GK_AppUtil();
		Calendar calendar = Calendar.getInstance();
		Integer nowYear = calendar.get(Calendar.YEAR);
		Integer month = calendar.get(Calendar.MONTH) + 1;
		String startDate = null;
		String endDate = null;
		if (month == 1 || month == 2 || month == 3) {
			startDate = String.valueOf(nowYear - 1) + "1001";
			endDate = String.valueOf(nowYear - 1) + "1231";
		} else if (month == 4 || month == 5 || month == 6) {
			startDate = String.valueOf(nowYear) + "0101";
			endDate = String.valueOf(nowYear) + "0331";
		} else if (month == 7 || month == 8 || month == 9) {
			startDate = String.valueOf(nowYear) + "0401";
			endDate = String.valueOf(nowYear) + "0630";
		} else if (month == 10 || month == 11 || month == 12) {
			startDate = String.valueOf(nowYear) + "0701";
			endDate = String.valueOf(nowYear) + "0930";
		}
		int htState = gkAppUtil.getContractStateByCustomerId1(customerId, startDate, endDate);
		return htState;
	}

	public void excel1(List<UserInforEq> list, HttpServletResponse response,HttpServletRequest request) throws Exception {
		String activityName ="客户权益表";
		String filedisplay = null;
		filedisplay = activityName + ".xls";
		String agent = request.getHeader("USER-AGENT");
		if (null != agent && -1 != agent.indexOf("MSIE")) { // IE
			filedisplay = java.net.URLEncoder.encode(filedisplay, "UTF-8");
		} else if (null != agent && -1 != agent.indexOf("Firefox")) { // Firefox
			filedisplay = new String(filedisplay.getBytes("UTF-8"), "iso-8859-1");
		} else {
			filedisplay = java.net.URLEncoder.encode(filedisplay, "UTF-8");
		}
		// 创建一个工作簿
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet(activityName);
		// 创建行
		HSSFRow row = sheet.createRow((int) 0);
		// 设置默认的列宽
		sheet.setDefaultColumnWidth(10);
		// 设置样式
		HSSFCellStyle style = wb.createCellStyle();
		style.setWrapText(true);
		//表头水平居中
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		//表头垂直居中
		style.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		org.apache.poi.hssf.usermodel.HSSFFont font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setLocked(true);
		style.setFont(font);
		// 设置表内容格式
		HSSFCellStyle style2 = wb.createCellStyle();
		style2.setWrapText(true);
		style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		//垂直居中
		style2.setVerticalAlignment(HSSFCellStyle.VERTICAL_CENTER);
		HSSFCell cell = row.createCell(0);
		cell.setCellValue("客户姓名");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("身份证号");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("手机号");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("对应理财师");
		cell.setCellStyle(style);

		cell = row.createCell(4);
		cell.setCellValue("所属分公司");
		cell.setCellStyle(style);
		
		cell = row.createCell(5);
		cell.setCellValue("所属大区");
		cell.setCellStyle(style);
		
		cell = row.createCell(6);
		cell.setCellValue("累计投资金额");
		cell.setCellStyle(style);
		
		cell = row.createCell(7);
		cell.setCellValue("持仓峰值");
		cell.setCellStyle(style);
		
		cell = row.createCell(8);
		cell.setCellValue("客户等级");
		cell.setCellStyle(style);

		cell = row.createCell(9);
		cell.setCellValue("名医看诊权益总次数");
		cell.setCellStyle(style);

		cell = row.createCell(10);
		cell.setCellValue("私人医生权益总次数");
		cell.setCellStyle(style);
		
		cell = row.createCell(11);
		cell.setCellValue("电话咨询");
		cell.setCellStyle(style);
		
		cell = row.createCell(12);
		cell.setCellValue("权益使用明细");
		cell.setCellStyle(style);
		
		cell = row.createCell(13);
		cell.setCellValue("权益剩余明细");
		cell.setCellStyle(style);
		// 将数据写入excel
		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow((int) i + 1);
			UserInforEq userInforEq = list.get(i);
			// 创建单元格，设置值
			row.createCell(0).setCellValue(userInforEq.getUserName());
			row.createCell(1).setCellValue(userInforEq.getIdcardNumber());
			row.createCell(2).setCellValue(userInforEq.getPhoneNumber());
			row.createCell(3).setCellValue(userInforEq.getFinancialName());
			row.createCell(4).setCellValue(userInforEq.getCompanyName());
			row.createCell(5).setCellValue(userInforEq.getRegion());
			row.createCell(6).setCellValue(userInforEq.getCountMoney());
			row.createCell(7).setCellValue(userInforEq.getMaxMoney());
			row.createCell(8).setCellValue(userInforEq.getCustomerLevel());
			row.createCell(9).setCellValue(userInforEq.getFamousDoctorNum());
			row.createCell(10).setCellValue(userInforEq.getPrvivateDoctorNum());
			row.createCell(11).setCellValue(userInforEq.getConsultation());
			row.createCell(12).setCellValue(userInforEq.getEquitiesUsedDetail());
			row.createCell(13).setCellValue(userInforEq.getEquitiesDetail());
			for (int j = 0; j < 14; j++) {
				row.getCell(j).setCellStyle(style2);
				sheet.autoSizeColumn(j);
			}
		}
		OutputStream out = response.getOutputStream();
		response.addHeader("Content-Disposition", "attachment;filename=" + filedisplay);
		wb.write(out);
		out.close();
	}

}

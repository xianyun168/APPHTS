package com.ciitc.webServices.controller;


import java.io.OutputStream;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.ciitc.webServices.dao.AppMessageSendDao;
import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.RemindProjectDao;
import com.ciitc.webServices.dao.appIntegralDetailsDao;
import com.ciitc.webServices.model.ActivityBasicInfor;
import com.ciitc.webServices.model.AppMessageSend;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.HTMessageInfo;
import com.ciitc.webServices.model.Integration;
import com.ciitc.webServices.model.MessageRemindRule;
import com.ciitc.webServices.model.RemindProject;
import com.ciitc.webServices.model.appIntegralDetails;
import com.ciitc.webServices.service.ActiviteService;
import com.ciitc.webServices.service.AppUserInforService;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.HTMessageInfoService;
import com.ciitc.webServices.service.TimeRemindRuleService;
import com.ciitc.webServices.service.appIntegralDetailsService;
import com.ciitc.webServices.util.CRM_CustomInfoUntil;
import com.ciitc.webServices.util.CRM_Expiration;
import com.ciitc.webServices.util.CRM_ReservationUtil;
import com.ciitc.webServices.util.CalculationIntegral;
import com.ciitc.webServices.util.CrmFunctionUtil;
import com.ciitc.webServices.util.DBUtil;
import com.ciitc.webServices.util.GK_AppUtil;
import com.ciitc.webServices.util.KfcrmCustor;
import com.ciitc.webServices.util.Log;
import com.ciitc.webServices.util.PublicJpushUntil;
import com.ciitc.webServices.util.pageUtil;

import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONObject;


@Controller
@RequestMapping("/readMessageController")
public class ReadMessageController extends BaseController{
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static appIntegralDetailsDao<appIntegralDetails> IntegraDetailsDao = (appIntegralDetailsDao) SpringContextUtil.getBean("appIntegralDetailsDao");
	@SuppressWarnings("rawtypes")
	private RemindProjectDao remindProjectDao = (RemindProjectDao) SpringContextUtil.getBean("remindProjectDao");
	@SuppressWarnings("rawtypes")
	private AppMessageSendDao appMessageSendDao = (AppMessageSendDao) SpringContextUtil.getBean("appMessageSendDao");
	@SuppressWarnings("rawtypes")
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao =  (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	
	@Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType> esbmmCodeTypeService;
	
	@Autowired(required = false)
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService;
	
	@Autowired(required = false)
	private AppUserInforService <AppUserInfor> appUserInforService;
	
	@Autowired(required = false)
	private TimeRemindRuleService<MessageRemindRule> timeRemindRuleService;
	
	@Autowired(required = false)
	private HTMessageInfoService <HTMessageInfo> hTMessageInfoService;
	
	@Autowired(required = false)
	private appIntegralDetailsService<appIntegralDetails> appIntegralDetailsService;
	
	@Autowired(required = false)
	private ActiviteService<ActivityBasicInfor> activiteService;
	
	private String message;
	
	@RequestMapping(params="readMessage=key")
	public ModelAndView readMessage() {
		return new ModelAndView("pages/HTmessageRemind/HTmessageList");
	}
	
	@RequestMapping(params="datagrid=key")
	public void datagrid(HttpServletResponse response,HTMessageInfo hTMessageInfo, BasePage basePage) {
		if (basePage.getPage() == 0) {
			basePage.setPage(1);
		}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("hTMessageInfo", hTMessageInfo);
			List<HTMessageInfo> list = new ArrayList<HTMessageInfo>();
			try {
				list = hTMessageInfoService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		
	}
	//导出客户生日跳转
	
	@RequestMapping(params = "exports=key")
	public ModelAndView exports(HttpServletResponse response, HttpServletRequest request)
			throws Exception {
		return new ModelAndView("pages/HTmessageRemind/excel");
	}
	//导出客户生日
	@RequestMapping(params = "exportList=key")
	@ResponseBody
	public AjaxJson exportList(HttpServletResponse response, HttpServletRequest request,String startDate,String endDate )
			throws Exception {
		AjaxJson json = new AjaxJson();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<HTMessageInfo> list = new ArrayList<HTMessageInfo>();
		Map<String, Object> map = new HashMap<String, Object>();
		if(null != startDate && !"".equals(startDate)) {
			map.put("startDate", sdf.parse(startDate));
		}
		if(null != endDate && !"".equals(endDate)) {
			map.put("endDate", sdf.parse(endDate));
		}
		list = hTMessageInfoService.queryByBirthday(map);
		if (list.size() > 0) {
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/x-download");
			//excel1(list, response);
			json.setObj("ok");
			json.setMsg("客户生日已导出");
		}else {
			json.setObj("no");
			json.setMsg("暂无数据导出");
		}
		return json;

	}
	@RequestMapping(params = "excel1=key")
	public void excel1(HttpServletResponse response, HttpServletRequest request,String startDate,String endDate) throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<HTMessageInfo> list = new ArrayList<HTMessageInfo>();
		Map<String, Object> map = new HashMap<String, Object>();
		if(null != startDate && !"".equals(startDate)) {
			map.put("startDate", sdf.parse(startDate));
		}
		if(null != endDate && !"".equals(endDate)) {
			map.put("endDate", sdf.parse(endDate));
		}
		list = hTMessageInfoService.queryByBirthday(map);
		String filedisplay = null;
		filedisplay = "客户生日.xls";
		String books  = "客户生日";
		String agent = request.getHeader("USER-AGENT");
		if (null != agent && -1 != agent.indexOf("MSIE")) { // IE
			filedisplay = java.net.URLEncoder.encode(filedisplay, "UTF-8");
		} else if (null != agent && -1 != agent.indexOf("Firefox")) { // Firefox
			filedisplay = new String(filedisplay.getBytes("UTF-8"), "iso-8859-1");
		} else {
			filedisplay = java.net.URLEncoder.encode(filedisplay, "UTF-8");
		}
		//filedisplay = URLEncoder.encode(filedisplay, "UTF-8");
		books = URLEncoder.encode(filedisplay, "UTF-8");
		// 创建一个工作簿
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet(books);
		// 创建行
		HSSFRow row = sheet.createRow((int) 0);
		// 设置默认的列宽
		sheet.setDefaultColumnWidth(10);
		// 设置样式
		HSSFCellStyle style = wb.createCellStyle();
		style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
		org.apache.poi.hssf.usermodel.HSSFFont font = wb.createFont();
		font.setFontHeightInPoints((short) 11);
		font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		style.setLocked(true);
		style.setFont(font);
		// 设置表内容格式
		HSSFCellStyle style2 = wb.createCellStyle();
		style2.setAlignment(HSSFCellStyle.ALIGN_CENTER);

		HSSFCell cell = row.createCell(0);
		cell.setCellValue("序号");
		cell.setCellStyle(style);

		cell = row.createCell(1);
		cell.setCellValue("姓名");
		cell.setCellStyle(style);

		cell = row.createCell(2);
		cell.setCellValue("生日");
		cell.setCellStyle(style);

		cell = row.createCell(3);
		cell.setCellValue("手机号");
		cell.setCellStyle(style);
		// 将数据写入excel
		int k = 0;
		for (int i = 0; i < list.size(); i++) {
			row = sheet.createRow((int) i + 1);
			HTMessageInfo htmessageInfo = list.get(i);
			// 创建单元格，设置值
			if(null != htmessageInfo.getBirth() && !"".equals(htmessageInfo.getBirth())) {
				row.createCell(0).setCellValue(k);
				if(null != htmessageInfo.getName() && !"".equals(htmessageInfo.getName())) {
					row.createCell(1).setCellValue(htmessageInfo.getName());
				}else {
					row.createCell(1).setCellValue("-");
				}
				if(null != htmessageInfo.getBirth() && !"".equals(htmessageInfo.getBirth())) {
					row.createCell(2).setCellValue(htmessageInfo.getBirth());
				}else {
					row.createCell(2).setCellValue("-");
				}
				if(null != htmessageInfo.getPhone() && !"".equals(htmessageInfo.getPhone())) {
					row.createCell(3).setCellValue(htmessageInfo.getPhone());
				}else {
					row.createCell(3).setCellValue("-");
				}
				
				for (int j = 0; j < 4; j++) {
					row.getCell(j).setCellStyle(style2);
				}
				k++;
			}
			
		}
		OutputStream out = response.getOutputStream();
		response.addHeader("Content-Disposition", "attachment;filename=" + filedisplay);
		wb.write(out);
		out.close();
	}
	@RequestMapping(params="countMessage=key")
	@ResponseBody
	public AjaxJson countMessage(HTMessageInfo hTMessageInfo) throws Exception{
		AjaxJson json=new AjaxJson();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		hTMessageInfo.setState("未读");
		paramMap.put("hTMessageInfo",hTMessageInfo);
		int count = hTMessageInfoService.queryByCount(paramMap);
		json.setObj(count);
		return json;
		
	}
	
	@RequestMapping(params="query1=key")
	@ResponseBody
	public AjaxJson query1(HTMessageInfo hTMessageInfo) throws Exception{
		AjaxJson json=new AjaxJson();
		if(hTMessageInfo.getPkId()!=null && !"".equals(hTMessageInfo.getPkId())){
			HTMessageInfo hTMessageInfo1 = hTMessageInfoService.queryById(hTMessageInfo.getPkId());
			if(hTMessageInfo1.getState()!=null && !"".equals(hTMessageInfo1.getState())) {
				hTMessageInfo1.setState("已读");
				hTMessageInfo1.setFlag(1);
				hTMessageInfoService.update(hTMessageInfo1);
			}
		} 
		message="修改成功";
		json.setObj(message);
		return json;
		
	}
	
	
	@RequestMapping(params="specific=key")
	public ModelAndView repairInforSpecific(String pkId) throws Exception {
		Map<String, Object> context = getRootMap();
		HTMessageInfo hTMessageInfo = hTMessageInfoService.queryById(pkId);
		context.put("hTMessageInfo", hTMessageInfo);
		return new ModelAndView("pages/HTmessageRemind/HTmessageSpecific",context);
	}
	
	
	public void save() {
		MessageRemindRule messageRemindRule = timeRemindRuleService.queryByBirth("03");
		Integer days = messageRemindRule.getDays();
		SimpleDateFormat sdf =new SimpleDateFormat("MM-dd");
		SimpleDateFormat sdf1 =new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, days);
		String birthday = sdf.format(now.getTime());
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("birthday", birthday);
		List<AppUserInfor> list = new ArrayList<AppUserInfor>();
		try {
			list = appUserInforService.queryByList1(paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				AppUserInfor appUserInfor = list.get(i);
				HTMessageInfo HTMessageInfo = new HTMessageInfo();
				HTMessageInfo.setPkId(UUID.randomUUID().toString());
				HTMessageInfo.setBirth(sdf.format(appUserInfor.getCustomerBirthday()));
				HTMessageInfo.setName(appUserInfor.getUserName());
				HTMessageInfo.setPhone(appUserInfor.getPhoneNumber());
				HTMessageInfo.setRemark1(sdf1.format(new Date()));
				HTMessageInfo.setMessage("您好，"+birthday+"是"+appUserInfor.getUserName()+"的生日,请记得祝福");
				HTMessageInfo.setState("未读");
				HTMessageInfo.setFlag(0);
				try {
					hTMessageInfoService.add(HTMessageInfo);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}else {
			return;
		}
		
	}
	//定时查询活动
	public void saveActivity() {
		//定时查询未开始的活动，如果时间已到，就改为已开始
		List<ActivityBasicInfor> activityList = activiteService.queryByState("00");
		if(activityList.size()>0) {
			for(int i=0;i<activityList.size();i++) {
				ActivityBasicInfor activityBasicInfor = activityList.get(i);
				long signEnd = activityBasicInfor.getActivityStartDate().getTime();
				long nowTime = new Date().getTime();
				if(nowTime >= signEnd ) {
					activityBasicInfor.setActivityState("01");
					activiteService.updateByState(activityBasicInfor);
				}
			}
		}
		//定时查询已开始的活动，如果时间已到，就改为已结束
		List<ActivityBasicInfor> activityList1 = activiteService.queryByState("01");
		if(activityList1.size()>0) {
			for(int i=0;i<activityList1.size();i++) {
				ActivityBasicInfor activityBasicInfor = activityList1.get(i);
				long endDate = activityBasicInfor.getActivityEndDate().getTime();
				long nowDate = new Date().getTime();
				if(nowDate >= endDate ) {
					activityBasicInfor.setActivityState("02");
					activiteService.updateByState(activityBasicInfor);
				}
			}
		}
	}
	
	public void saveJfNum() {
		//根据数据字典查询状态
		List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("jfType");
		EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
		EsbmmCode codePkId = new EsbmmCode();
		codePkId.setCodeTypePkId(codeTypePkId.getPkId());
		//根据codeTypePkId查找对应的类型
		List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codePkId);
		
		List<EsbmmCodeType> codeTypePkIdList1 = esbmmCodeTypeService.queryByTypeName("jfMakeState");
		EsbmmCodeType codeTypePkId1 = codeTypePkIdList1.get(0);
		EsbmmCode codePkId1 = new EsbmmCode();
		codePkId1.setCodeTypePkId(codeTypePkId1.getPkId());
		//根据codeTypePkId查找对应的类型
		List<EsbmmCode> codeList1 = esbmmCodeService.queryByTypeIds(codePkId1);
		
		SimpleDateFormat sdf =new SimpleDateFormat("MM-dd");
		String birthday = sdf.format(new Date());
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("birthday", birthday);
		List<AppUserInfor> list = new ArrayList<AppUserInfor>();
		try {
			list = appUserInforService.queryByBirthday(birthday);
		} catch (Exception e) {
			e.printStackTrace();
		}
		GK_AppUtil gkAppUtil = new GK_AppUtil();
		SimpleDateFormat sdf1 =new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdf2 =new SimpleDateFormat("MMdd");
		//开始时间
		String nowDate = sdf1.format(new Date());
		Integer a = Integer.parseInt(nowDate.substring(0, 4))-1;
		String nowDateYear =String.valueOf(a);
		String startDate = nowDateYear+sdf2.format(new Date());
		//结束时间
		String endDate = sdf1.format(new Date());
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				AppUserInfor appUserInfor = list.get(i);
				//客户编号 
				String customerId = appUserInfor.getCustomerId();
				//根据客户编号查询客户最近一年是否有持仓
				String status = gkAppUtil.getContractStateByCustomerId(customerId,startDate,endDate);
				if("00".equals(status)) {
					long jfNum = appUserInfor.getIntegral();
					//积分明细实体
					appIntegralDetails IntegralDetails = new appIntegralDetails();
					//积分ID
					IntegralDetails.setPkId(UUID.randomUUID().toString());
					//客户ID
					IntegralDetails.setUserId(appUserInfor.getPkId());
					//积分创建日期
					IntegralDetails.setRecordDate(new Date());
					//积分值
					IntegralDetails.setIntegralNum("100");
					//名称
					IntegralDetails.setName("生日积分");
					if(jfNum == 0) {
						appUserInfor.setIntegral(0 + (Integer.valueOf(IntegralDetails.getIntegralNum())));
					}else {
						appUserInfor.setIntegral(jfNum + (Integer.valueOf(IntegralDetails.getIntegralNum())));
					}
					try {
						appUserInforService.update1(appUserInfor);
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					//积分有效期
					IntegralDetails.setValidity(30);
					//积分原因
					IntegralDetails.setReason("生日");
					//消耗积分
					IntegralDetails.setConsumptionIntegral(0);
					//到期分值
					IntegralDetails.setExpireIntegral(0);
					//有效分值
					IntegralDetails.setValidIntegral(100);
					//到期日期
					Calendar now = Calendar.getInstance();
			        now.add(Calendar.DAY_OF_MONTH, 30);
					IntegralDetails.setExpireDate(now.getTime());
					//积分类型
					for (EsbmmCode esbmmCode : codeList) {
						if("用户生日".equals(esbmmCode.getChineseName())){
							IntegralDetails.setIntegralType(esbmmCode.getCodeCode());
						}
					}
					//积分操作状态
					for (EsbmmCode esbmmCode1 : codeList1) {
						if("增加".equals(esbmmCode1.getChineseName())){
							IntegralDetails.setIntegralState(esbmmCode1.getCodeCode());
						}
					}
					try {
						appIntegralDetailsService.add(IntegralDetails);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}else {
			return;
		}
	}
	public void modifyJfNum() {
		List<EsbmmCodeType> codeTypePkIdList1 = esbmmCodeTypeService.queryByTypeName("jfMakeState");
		EsbmmCodeType codeTypePkId1 = codeTypePkIdList1.get(0);
		EsbmmCode codePkId1 = new EsbmmCode();
		codePkId1.setCodeTypePkId(codeTypePkId1.getPkId());
		//根据codeTypePkId查找对应的类型
		List<EsbmmCode> codeList1 = esbmmCodeService.queryByTypeIds(codePkId1);
		
		SimpleDateFormat sdf =new SimpleDateFormat("MM-dd");
		Calendar now = Calendar.getInstance();
        now.add(Calendar.DAY_OF_MONTH, -30);
		String recordDate1 = sdf.format(now.getTime());
		//System.out.println(recordDate1);
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("recordDate1", recordDate1);
		List<appIntegralDetails> list = new ArrayList<appIntegralDetails>();
		try {
			list = appIntegralDetailsService.queryByList1(paramMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(list.size()>0) {
			for(int i=0;i<list.size();i++) {
				appIntegralDetails appIntegralDetails1 = list.get(i);
				//积分操作状态
				/*for (EsbmmCode esbmmCode1 : codeList1) {
					if("消耗".equals(esbmmCode1.getChineseName())){
						appIntegralDetails1.setIntegralState(esbmmCode1.getCodeCode());
					}
				}*/
				appIntegralDetails1.setExpireIntegral(100);
				appIntegralDetails1.setValidIntegral(0);
				//appIntegralDetails1.setExpireDate(new Date());
				try {
					appIntegralDetailsService.update(appIntegralDetails1);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				AppUserInfor appUserInfor = appUserInforService.selectByPrimaryKey(appIntegralDetails1.getUserId());
				if(appUserInfor != null) {
					if(appUserInfor.getIntegral() != 0) {
						long jfNum = (appUserInfor.getIntegral())-(Integer.valueOf(appIntegralDetails1.getIntegralNum()));
						 appUserInfor.setIntegral(jfNum);
						 try {
							appUserInforService.update1(appUserInfor);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}else {
			return;
		}
	}	
	//定时查询合同,增加积分
	public void addIntegration() {
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		KfcrmCustor kfcrmCustor = new KfcrmCustor();
		CRM_ReservationUtil reservationUtil = new CRM_ReservationUtil();
		CalculationIntegral calculationIntegral = new CalculationIntegral();
		//根据数据字典查询状态
		List<EsbmmCodeType> codeTypePkIdList = esbmmCodeTypeService.queryByTypeName("ContractIntegral");
		EsbmmCodeType codeTypePkId = codeTypePkIdList.get(0);
		//根据codeTypePkId查找对应的类型
		List<EsbmmCode> codeList = esbmmCodeService.queryByTypeIds(codeTypePkId.getPkId());
		EsbmmCode esbmmCode = codeList.get(0);
		String type = esbmmCode.getCodeValue();
		if("1".equals(type)) {
			queryCustomInfo1();
			//初始化查询所有合同，增加积分
			//List<Integration> integrationlist = reservationUtil.getContract();
			//1、先查询2018-01-01之前的合同。等级全部为1
  			List<Integration> integrationlist = reservationUtil.getContract();
			if(integrationlist.size()>0) {
				calculationIntegral.tradingIntegral1(integrationlist);
			}
  			//2、再查询2018-01-01到现在的合同。等级按照客户表里的等级
			List<Integration> integrationlist1 = reservationUtil.getContract1();
			if(integrationlist1.size()>0) {
				calculationIntegral.tradingIntegral(integrationlist1);
			}
			EsbmmCode esbmmCode1 = new EsbmmCode();
			esbmmCode1.setPkId(esbmmCode.getPkId());
			esbmmCode1.setCodeValue("2");
			//数据字典里合同积分的value
			try {
				esbmmCodeService.update1(esbmmCode1);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//esbmmCodeService.updateBySelective(esbmmCode1);
			/*if(integrationlist.size()>0) {
				calculationIntegral.tradingIntegral(integrationlist);
			}*/
		}else {
			//查询每天所增加的合同，增加积分
			Date date = new Date();
			Date yesterDay = new Date(date.getTime()-86400000L);
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
			String nowDay = sdf.format(yesterDay);
			List<Integration> integrationlist = reservationUtil.getContractNow(nowDay);
			if(integrationlist.size()>0) {
				//查询昨天签署合同的客户编号，根据客户编号查询本地表是否有这个客户，如果没有则添加到本地表
				for (Integration integration : integrationlist) {
					String customerId = integration.getCustno();
					AppUserInfor appUserInfor = appUserInforDao.queryByCustomerId(customerId);
					if(null == appUserInfor) {
						//查询CRM客户表
						AppUserInfor appUserInfora = customInfoUntil.getCustByNoa(customerId);
						if(null != appUserInfora) {
							//本地客户表主键
							appUserInfora.setPkId(UUID.randomUUID().toString());
							//客户积分
							appUserInfora.setIntegral(0);
							//客户等级
							appUserInfora.setCustomerLevel("1");
							//理财师编号(根据客户)
			  				String financialId =customInfoUntil.brokeraccount1(customerId);
			  				appUserInfora.setFinancialId(financialId);//理财师ID
			  			    //实名认证状态
			  				String nameStatus = kfcrmCustor.getStateBycustNo(customerId);
			  				if(null != nameStatus && "已认证".equals(nameStatus)) {
			  					appUserInfora.setNameStatus("1");
			  				}else if(null != nameStatus && "待审核".equals(nameStatus)) {
			  					appUserInfora.setNameStatus("2");
			  				}else if(null != nameStatus && "已驳回".equals(nameStatus)){
			  					appUserInfora.setNameStatus("3");
			  				}else {
			  					appUserInfora.setNameStatus("0");
			  				}
			  			//将数据添加到本地客户表
						appUserInforDao.insertInfor(appUserInfora);
						//客户等级的计算方法(找当前时间的前一年年底的时间)
						/*String nowDate = sdf.format(new Date());
						Integer a = Integer.parseInt(nowDate.substring(0, 4))-1;
						String nowDateYear =String.valueOf(a);
						String dates = nowDateYear+"1231";
						String datey = nowDateYear+"0101";*/
						//CalculationIntegral.CustomerLevelBycustoNo(appUserInfora.getCustomerId(),dates,datey);
						CalculationIntegral.CustomerLevelBycustoNo(appUserInfora.getCustomerId());
						}
					}
				}
				calculationIntegral.tradingIntegral(integrationlist);
			}
		}
				
	}
	//分红
	public void fenHong() {
		CRM_Expiration expiration =new CRM_Expiration();
		MessageRemindRule messageRemindRule = new MessageRemindRule();
		//根据规则名称查询规则信息
		messageRemindRule = expiration.getMessageRemindRule("产品分红提醒");
		if(null != messageRemindRule) {
			Integer days = messageRemindRule.getDays();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar now = Calendar.getInstance();
	        now.add(Calendar.DAY_OF_MONTH, days);
			String day = sdf.format(now.getTime());
			String content =  messageRemindRule.getMessageMould();
			content = content.replace("${days}", String.valueOf(messageRemindRule.getDays()));
			expiration.fenHong(day,content);
		}
	}
	//生日提醒
	public void birthdays() {
		CRM_Expiration expiration =new CRM_Expiration();
		MessageRemindRule messageRemindRule = new MessageRemindRule();
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		//根据规则名称查询规则信息
		messageRemindRule = expiration.getMessageRemindRule("客户生日提醒");
		if(null != messageRemindRule) {
			PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
			//查询当前时间
			Calendar now = Calendar.getInstance();
	        now.add(Calendar.DAY_OF_MONTH, messageRemindRule.getDays());
			SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
			String birthday = sdf.format(now.getTime());
			List<AppUserInfor> userInfoList = new ArrayList<AppUserInfor>();
			//查询生日
			userInfoList = appUserInforService.queryByBirthday(birthday);
			if(userInfoList.size()>0) {
				for (AppUserInfor appUserInfor : userInfoList) {
					Set<String> tag = new HashSet<String>();
					tag.add(appUserInfor.getFinancialId());
					String content = messageRemindRule.getMessageMould();
					Date CustomerBirthday = appUserInfor.getCustomerBirthday();
					content = content.replace("${customerBirthday}", sdf.format(CustomerBirthday));
					content = content.replace("${userName}", appUserInfor.getUserName());
					content = content.replace("${days}", messageRemindRule.getDays().toString());
					AppMessageSend appMessageSend = new AppMessageSend();
					appMessageSend.setPkId(UUID.randomUUID().toString());//主键
					appMessageSend.setFlag("01");
					appMessageSend.setMessageTitle("客户生日");
					appMessageSend.setMessageContent(content);
					appMessageSend.setSendObject(appUserInfor.getFinancialId());
					appMessageSend.setSendState("未推送");
					appMessageSend.setSendDate("-");
					appMessageSend.setOperateName("-");
					appMessageSend.setMessageType("常规消息");
					appMessageSend.setSendType("系统");
					Date now1 = new Date();
					appMessageSend.setTimingSendDate(new Date(now1.getTime()+60000));
					appMessageSendDao.insertSelective(appMessageSend);
					//推送给客户理财师
					String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "客户生日", content, tag);
					if("00".equals(states)) {
						SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
						AppMessageSend appMessageSend1 = new AppMessageSend();
						appMessageSend1.setSendState("已推送");
						appMessageSend1.setSendDate(sf.format(new Date()));
						appMessageSend1.setPkId(appMessageSend.getPkId());
						appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
					}
				}
			}
		}
	}
	
	//查询所有用户信息
	public void queryCustomInfo1() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		// 备份，异常恢复使用
		List<AppUserInfor> queryALL = appUserInforDao.queryALL();
		AppUserInfor appUserInfor =  new AppUserInfor();
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		KfcrmCustor kfcrmCustor = new KfcrmCustor();
		ResultSet rs = null;
		try {
			appUserInforDao.deleteAll();
			call = conn.prepareCall(" SELECT * FROM TUNREGISTER");
			// 执行
  			rs = call.executeQuery();
  			System.out.println("初始等级计算开始："+new Date());
  			while (rs.next()) {
  				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));//客户编号
  				appUserInfor.setPhoneNumber(rs.getString("C_MOBILENO"));//手机号
  				appUserInfor.setIdcardType(rs.getString("C_IDENTITYTYPE")); //证件类型
  				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO")); //证件号码
  				appUserInfor.setCreateDate(rs.getDate("D_CREATEDATE")); ;//创建时间
  				appUserInfor.setUserName(rs.getString("C_CUSTNAME")); //客户名称
  				appUserInfor.setCustomerSex(rs.getString("C_SEX")); //性别
  				appUserInfor.setcIsDelete(rs.getString("C_ISDELETE"));//客户是否被删除
  				//appUserInfor.setRegion(rs.getString("C_MARKETREGION")); //服务分支机构---
  				//根据客户编号查询理财师
  				String financialId =customInfoUntil.brokeraccount1(rs.getString("C_CUSTNO"));
  				appUserInfor.setFinancialId(financialId);//理财师ID
  				String nameStatus = kfcrmCustor.getStateBycustNo(rs.getString("C_CUSTNO"));
  				if(null != nameStatus && "已认证".equals(nameStatus)) {
  					appUserInfor.setNameStatus("1");//已认证
  				}else if(null != nameStatus && "待审核".equals(nameStatus)){
  					appUserInfor.setNameStatus("2");//审核中
  				}else if(null != nameStatus && "已驳回".equals(nameStatus)){
  					appUserInfor.setNameStatus("3");
  				}else {
  					appUserInfor.setNameStatus("0");//未认证
  				}
  				
  				String  birthday = rs.getString("C_BIRTHDAY");//生日
  				if(null != birthday && !"".equals(birthday)) {
  					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
  					Date customerBirthday = null;
  					try {
  						customerBirthday = sdf.parse(birthday);
  					} catch (ParseException e) {
  						// TODO Auto-generated catch block
  						e.printStackTrace();
  					}
  	  				
  	  				appUserInfor.setCustomerBirthday(customerBirthday);
  				}
  				appUserInfor.setIntegral(0);//客户积分
  				appUserInfor.setCustomerLevel("1");//客户等级
  				appUserInfor.setPkId(UUID.randomUUID().toString());
  				appUserInfor.setCustomerAddress(rs.getString("C_ADDRESS")); //地址
  				appUserInforDao.insertSelective(appUserInfor);
  				//初始化第一次跑数据，等级按照前一年年底的时间算(2017-12-31)
  				CalculationIntegral.CustomerLevelBycustoNo(rs.getString("C_CUSTNO"));
  			}
  			System.out.println("初始等级计算结束："+new Date());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// 清空数据表(删除已经插入的部分数据)
			appUserInforDao.deleteAll();
			// 恢复数据
			for (int i = 0; i < queryALL.size(); i++) {
				try {
					appUserInforDao.insertInfor(queryALL.get(i));
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
		
	//定时客户列表查询
	public void queryCustomList() {
		// 连接耀莱CRM数据库
		Connection conn = DBUtil.makeConnection();
		CallableStatement call = null;
		CRM_CustomInfoUntil customInfoUntil = new CRM_CustomInfoUntil();
		CalculationIntegral CalculationIntegral = new CalculationIntegral();
		KfcrmCustor kfcrmCustor = new KfcrmCustor();
		//删除客户表客户编号为空的数据
		//appUserInforDao.deleteByCustomId();
		//查询crm客户信息
		ResultSet rs = null;
		try {
			call = conn.prepareCall(" SELECT * FROM TUNREGISTER");
			rs = call.executeQuery();
			while (rs.next()) {
				String customId = rs.getString("C_CUSTNO");
				AppUserInfor appUserInfor = new AppUserInfor();
				appUserInfor.setCustomerId(rs.getString("C_CUSTNO"));//客户编号
				appUserInfor.setPhoneNumber(rs.getString("C_MOBILENO"));//手机号
				appUserInfor.setIdcardType(rs.getString("C_IDENTITYTYPE")); //证件类型
				appUserInfor.setIdcardNumber(rs.getString("C_IDENTITYNO")); //证件号码
				appUserInfor.setUserName(rs.getString("C_CUSTNAME")); //客户名称
				appUserInfor.setCustomerSex(rs.getString("C_SEX")); //性别
				appUserInfor.setcIsDelete(rs.getString("C_ISDELETE"));//客户是否被删除
				String financialId =customInfoUntil.brokeraccount1(rs.getString("C_CUSTNO"));
				appUserInfor.setFinancialId(financialId);//理财师ID
				String  birthday = rs.getString("C_BIRTHDAY");//生日
				if(null != birthday && !"".equals(birthday)) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
					Date customerBirthday = null;
					try {
						customerBirthday = sdf.parse(birthday);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	  				appUserInfor.setCustomerBirthday(customerBirthday);
				}
				String nameStatus = kfcrmCustor.getStateBycustNo(rs.getString("C_CUSTNO"));
				if(null != nameStatus && "已认证".equals(nameStatus)) {
					appUserInfor.setNameStatus("1");//已认证
				}else if(null != nameStatus && "待审核".equals(nameStatus)){
					appUserInfor.setNameStatus("2");//审核中
				}else if(null != nameStatus && "已驳回".equals(nameStatus)){
  					appUserInfor.setNameStatus("3");
  				}else {
					appUserInfor.setNameStatus("0");//未认证
				}
				appUserInfor.setCustomerAddress(rs.getString("C_ADDRESS")); //地址
				AppUserInfor appUserInfor2 = new AppUserInfor();
				try {
					Integer count = appUserInforDao.queryByCounts(customId);
					if(count > 1) {
						throw new Exception(customId);
					}
					appUserInfor2 = appUserInforDao.queryByCustomerId(customId);
				} catch (Exception e) {
					// TODO: handle exception
					Log.getLogger().info("客户编号重复",e);
					continue;
				}
				if(null != appUserInfor2) {
					appUserInfor.setPkId(appUserInfor2.getPkId());
					//根据客户编号查询客户有效积分
					Map<String,Object> map1 = new HashMap<String, Object>();
					map1.put("userId", appUserInfor2.getPkId());
					//统计此用户有效积分总和
					long sum = IntegraDetailsDao.selectUserIdSum(map1);
					appUserInfor.setIntegral(sum);
					appUserInforDao.updateByPKey(appUserInfor);
				}else {
					//根据客户手机号查询本地表是否存在此客户
					AppUserInfor appUserInfor3 = appUserInforDao.queryByPhone1(appUserInfor.getPhoneNumber());
					if(appUserInfor3 !=null) {
						try{
							//查询crm是否存在两条手机号相同的数据
							int num  = kfcrmCustor.phoneNumber(appUserInfor.getPhoneNumber());
							//如果crm有两条此手机号则抛异常
							if(num >1) {
								throw new Exception(appUserInfor.getPhoneNumber());
							}
						}catch (Exception e) {
							// TODO: handle exception
							Log.getLogger().info("手机号重复",e);
							continue;
						}
						appUserInfor.setPkId(appUserInfor3.getPkId());
						//根据客户编号查询客户有效积分
						Map<String,Object> map1 = new HashMap<String, Object>();
						map1.put("userId", appUserInfor3.getPkId());
						//统计此用户有效积分总和
						long sum = IntegraDetailsDao.selectUserIdSum(map1);
						appUserInfor.setIntegral(sum);
						appUserInforDao.updateByPKey(appUserInfor);
					}else {
						appUserInfor.setIntegral(0);//客户积分
						appUserInfor.setCustomerLevel("1");//客户等级
						appUserInfor.setPkId(UUID.randomUUID().toString());
						appUserInfor.setCreateDate(rs.getDate("D_CREATEDATE")); //创建时间
						appUserInforDao.insertSelective(appUserInfor);
						//客户等级的计算方法(找当前时间的前一年年底的时间)
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						/*String nowDate = sdf.format(new Date());
						Integer a = Integer.parseInt(nowDate.substring(0, 4))-1;
						String nowDateYear =String.valueOf(a);
						String dates = nowDateYear+"1231";
						String datey = nowDateYear+"0101";*/
						CalculationIntegral.CustomerLevelBycustoNo(appUserInfor.getCustomerId());
						//调用客户积分
						//1.先算出当前时间减上365之后的时间
						Date date = new Date();//当前时间
						Calendar ca = Calendar.getInstance();
						ca.add(Calendar.DATE, -365);
						date = ca.getTime();//减后时间
						//2.再判断当前时间和减后时间年份是否一样
						if(sdf.format(new Date()).substring(0, 4).equals(sdf.format(date).substring(0, 4))) {
							//3.如果年份一样则查询这一段时间的合同，按照客户表里的等级计算积分。
							Date yesterDay = new Date(new Date().getTime()-172800000L);
							List<Integration> integrationList  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay,date);
							CalculationIntegral.tradingIntegral(integrationList);
						}else {
							//4.如果年份不一样，则查询减后时间到年底的合同，等级按照加后时间的前一年年底的时间算，
							Date yesterDay = sdf.parse(sdf.format(date).substring(0, 4)+"-12-31");
							List<Integration> integrationList  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay,date);
							String yesterYear = String.valueOf((Integer.parseInt(sdf.format(date).substring(0, 4))-1))+"1231";
							String yesterYears = String.valueOf((Integer.parseInt(sdf.format(date).substring(0, 4))-1))+"0101";
							//String level = CalculationIntegral.CustomerLevelBycustoNo1(appUserInfor.getCustomerId(),yesterYear,yesterYears);
							String level = CalculationIntegral.CustomerLevelBycustoNo1(appUserInfor.getCustomerId());
							CalculationIntegral.tradingIntegral2(integrationList,level);
							//当前时间到此年份之初的合同按照客户表里的等级计算
							Date yesterDay2 = sdf.parse(sdf.format(new Date()).substring(0, 4)+"-01-01");
							Date yesterDay1 = new Date(new Date().getTime()-172800000L);
							List<Integration> integrationList1  = customInfoUntil.queryByCustNo3(appUserInfor.getCustomerId(),yesterDay1,yesterDay2);
							CalculationIntegral.tradingIntegral(integrationList1);
						}
					}
				}
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}finally {
			if(rs != null) {
				try {
					rs.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(call != null) {
				try {
					call.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if(conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	//新产品上线提醒
	public void queryProduct() {
		CrmFunctionUtil crmFunctionUtil = new CrmFunctionUtil();
		CRM_Expiration expiration =new CRM_Expiration();
		String appKey=null;
		String masterSecret=null;
		List<EsbmmCode> jpushConfig = esbmmCodeService.getCrmAppJpush();
		for (EsbmmCode esbmmCode : jpushConfig) {
			if(esbmmCode.getCodeName().equals("AppKey")) {
				appKey = esbmmCode.getCodeValue();
			}
			if(esbmmCode.getCodeName().equals("MasterSecret")) {
				masterSecret =esbmmCode.getCodeValue();
			}
		}
		//查询视图
		List<RemindProject> list = new ArrayList<RemindProject>();
		list = remindProjectDao.selectAll();
		PublicJpushUntil publicJpushUntil = new PublicJpushUntil();
		MessageRemindRule messageRemindRule = expiration.getMessageRemindRule("新产品上线");
		
		if(list.size()>0) {
			//根据营销编号查询产品名称
			for (RemindProject remindProject : list) {
				String proserialno = String.valueOf(remindProject.getlProserialno());
				String proserialName = crmFunctionUtil.getProserialname(proserialno);
				//推送
				Set<String> tag = new HashSet<String>();
				tag.add("0001");
				String content = messageRemindRule.getMessageMould();
				content = content.replace("${productName}", proserialName);
				
				AppMessageSend appMessageSend = new AppMessageSend();
				appMessageSend.setPkId(UUID.randomUUID().toString());//主键
				appMessageSend.setFlag("04");
				appMessageSend.setMessageTitle("新产品上线");
				appMessageSend.setMessageContent(content);
				appMessageSend.setSendObject("0001");
				appMessageSend.setSendState("未推送");
				appMessageSend.setSendDate("-");
				appMessageSend.setOperateName("-");
				appMessageSend.setMessageType("常规消息");
				appMessageSend.setSendType("系统");
				Date now = new Date();
				appMessageSend.setTimingSendDate(new Date(now.getTime()+60000));
				appMessageSendDao.insertSelective(appMessageSend);
				//推送给客户理财师
				String states = publicJpushUntil.SendPushTag(appKey, masterSecret, "新产品上线", content, tag);
				if("00".equals(states)) {
					SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					AppMessageSend appMessageSend1 = new AppMessageSend();
					appMessageSend1.setSendState("已推送");
					appMessageSend1.setSendDate(sf.format(new Date()));
					appMessageSend1.setPkId(appMessageSend.getPkId());
					appMessageSendDao.updateByPrimaryKeySelective(appMessageSend1);
				}
			}
		}
	}
	public void souyi() {
		CrmFunctionUtil util = new CrmFunctionUtil();
		util.updateTsaleProjectInfo();
		util.getProfit();// 与crm数据库同步收益
	}
	//定时同步crm产品到本地库
	public void syncChanpinFromCrm(){
		ProductInfoController productInfoController = new ProductInfoController();
		productInfoController.updateProductInfo();
	}
}

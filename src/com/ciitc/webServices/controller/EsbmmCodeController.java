package com.ciitc.webServices.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.page.AjaxJson;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.web.BaseController;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.model.OperateLog;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.service.EsbmmCodeTypeService;
import com.ciitc.webServices.service.OperateLogService;
import com.ciitc.webServices.service.RoleDetailService;
import com.ciitc.webServices.util.checkChar;
import com.ciitc.webServices.util.pageUtil;


/*******************************************************************************
 * 模块名称: EsbmmCodeController 
 * 模块功能描述: 用户管理模块 
 * 创 建 人: 
 * 日 期: 
 * 修 改 人: 
 ******************************************************************************/
@Controller
@RequestMapping("/esbmmCodeController")
public class EsbmmCodeController extends BaseController {

    @Autowired(required = false)
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService;
    @Autowired(required = false)
	private EsbmmCodeTypeService<EsbmmCodeType>   esbmmCodeTypeService;
	@Autowired(required = false)
    private RoleDetailService<RoleDetailInfo> roleDetailService;
    
	@Autowired(required = false)
	private OperateLogService<OperateLog> operateLogService; // 记录系统日志
	  
	 /*************************************************************************
     *  函数名:   saveUpdate      
     *  功能描述: 保存
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	  
    @RequestMapping(params = "saveUpdate=key")
	@ResponseBody
	public AjaxJson saveUpdate(HttpServletRequest request, EsbmmCode esbmmCode) throws Exception {
		try {
			AjaxJson j = new AjaxJson();
			checkChar checkChar = new checkChar();
			if(checkChar.checkChar(esbmmCode.getCodeCode())||checkChar.checkChar(esbmmCode.getCodeName())
					||checkChar.checkChar(esbmmCode.getChineseName())||checkChar.checkChar(esbmmCode.getCodeValue())){
				message = "存在不合法字符";
				j.setMsg(message);
				return j;
			}
			EsbmmCodeType codeTypePkId =null;
			try {
				codeTypePkId = esbmmCodeTypeService.queryById(esbmmCode.getCodeTypePkId());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(codeTypePkId!=null&&!"".equals(codeTypePkId)){
				if (esbmmCode.getPkId() != null && !"".equals(esbmmCode.getPkId())) {
					try {
						esbmmCodeService.updateBySelective(esbmmCode);
						 operateLogService.addLog("数据字典", "更新代码值日志", Globals.Log_Type_UPDATE);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			} else {
				/*if(esbmmCode.getChineseName()!=null&& !"".equals(esbmmCode.getChineseName())){
					
				}*/
				try {
					esbmmCodeService.add(esbmmCode);
					operateLogService.addLog("数据字典", "保存代码值日志",
							Globals.Log_Type_INSERT);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			j.setMsg("保存成功");
			
			}else{
				j.setMsg("请在一级目录添加代码值");
			}
			return j;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	 /*************************************************************************
     *  函数名:   datagrid      
     *  功能描述: 查询数据列表
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
    @RequestMapping(params = "datagrid=key")
	public void datagrid(HttpServletRequest request, HttpServletResponse response, EsbmmCode esbmmCode, BasePage basePage) {
		try {
			if (basePage.getPage() == 0) {
				basePage.setPage(1);
			}
			Map<String, Object> paramMap = new HashMap<String, Object>();
			paramMap.put("page", basePage);
			paramMap.put("esbmmCode", esbmmCode);
			List<EsbmmCode> list = new ArrayList<EsbmmCode>();
			try {
				list = esbmmCodeService.queryByList(paramMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 记录总条数
			int cnt = basePage.getPager().getRowCount();
			JSONObject jObject = pageUtil.getJsonForPage(list, cnt);
			pageUtil.responseDatagrid(response, jObject);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	 /*************************************************************************
     *  函数名:   delete      
     *  功能描述: 查询数据列表
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
		@RequestMapping(params = "delete=key")
		@ResponseBody
		public AjaxJson delete(String pkId) {
			AjaxJson j = new AjaxJson();
	   		BasePage basePage = new BasePage();
	   		basePage.setPage(1);
			
			
		    try {
				esbmmCodeService.delete(pkId);
				operateLogService.addLog("数据字典", "删除代码值日志", Globals.Log_Type_DEL);
			} catch (Exception e) {
			e.printStackTrace();
		}
		AjaxJson json = new AjaxJson();
		json.setObj("ok");
		return json;
		}
    
/*    @RequestMapping(params = "delete=key")
   	@ResponseBody
   	public AjaxJson deleteById(String pkId)throws Exception {
   		AjaxJson j = new AjaxJson();
   		BasePage basePage = new BasePage();
   		basePage.setPage(1);
  		
   		// 删除节点是否为父节点
   		List<EsbmmCode> type = null;
   		List<EsbmmCode> ter = null;
   		//boolean flag=true;
   		type = esbmmCodeService.queryByTypeIds(pkId);
   		Map<String, Object> paramMap = new HashMap<String, Object>();
   		paramMap.put("page", basePage);
   		paramMap.put("catPkId", pkId);
   		ter = esbmmCodeService.queryByList(paramMap);
   		if(type != null && type.size() > 0){
   			message = "含有子节点无法删除！";
   		} else {
   			
   				esbmmCodeService.delete(pkId);
   					operateLogService.addLog("数据字典", "代码值删除", Globals.Log_Type_DEL);
   					message = "删除成功";
   			}
   		j.setMsg(message);
   		j.setObj("alert");
   		return j;
   	}
    */
    

	
	 /*************************************************************************
     *  函数名:   add      
     *  功能描述: 跳转到增加页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "add=key")
	public ModelAndView add(EsbmmCode  esbmmCode, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
			context.put("esbmmCode", esbmmCode);
		return new ModelAndView("pages/sysManager/dictionary/EsbmmCodeAdd", context);
	}
	
	
	 /*************************************************************************
     *  函数名:   modify      
     *  功能描述: 跳转到修改页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "modify=key")
	public ModelAndView modify(EsbmmCode  esbmmCode, HttpServletRequest req) {
		Map<String, Object> context = getRootMap();
		try {
			esbmmCode = esbmmCodeService.queryById(esbmmCode.getPkId());
			context.put("esbmmCode", esbmmCode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new ModelAndView("pages/sysManager/dictionary/EsbmmCodeAdd", context);
	}
	
	
	 /*************************************************************************
     *  函数名:   List      
     *  功能描述: 跳转数据列表页面
     *  输入参数:        
     *  返 回 值:       
     *  异    常：
     *  创 建 人: 
     *  日    期: 
     *  修 改 人:
     *  日    期:
     *************************************************************************/
	@RequestMapping(params = "esbmmCodeList=key")
	public ModelAndView esbmmCodeList() {
		return new ModelAndView("pages/sysManager/dictionary/EsbmmCodeList");
	}

	
	/*************************************************************************
	 * 函数名: checkUserCode 功能描述: 查询数据列表 输入参数: 返 回 值: 异 常： 创 建 人: 日 期: 修 改 人: 日 期:
	 *************************************************************************/
	@RequestMapping(params = "checkExit=key")
	@ResponseBody
	public AjaxJson checkExit(HttpServletRequest request) {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		AjaxJson json = new AjaxJson();
		try {
			paramMap.put("codeTypePkId", request.getParameter("codeTypePkId"));
			paramMap.put("codeName", request.getParameter("codeName"));
			paramMap.put("codeValue", request.getParameter("codeValue"));
			
			List<EsbmmCode> list = esbmmCodeService.checkCode(paramMap);
			if(list.size()==0){
				json.setObj("ok");
			}else{
				json.setObj("no");
				json.setMsg("代码值已存在");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return json;
	}	

	
	// 向页面返回的信息
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}



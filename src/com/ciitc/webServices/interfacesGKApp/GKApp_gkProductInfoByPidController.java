package com.ciitc.webServices.interfacesGKApp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.EquitiesDetailDao;
import com.ciitc.webServices.dao.EquitiesUseDetailsDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EquitiesDetail;
import com.ciitc.webServices.model.EquitiesDetail2;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class GKApp_gkProductInfoByPidController extends BaseController{
	@SuppressWarnings("rawtypes")
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	private EquitiesDetailDao equitiesDetailDao = (EquitiesDetailDao) SpringContextUtil.getBean("equitiesDetailDao");
	@SuppressWarnings("rawtypes")
	private EquitiesUseDetailsDao equitiesUseDetailsDao = (EquitiesUseDetailsDao) SpringContextUtil.getBean("equitiesUseDetailsDao");
	/*
	 * 获取产品权限及剩余次数接口
	 */
	@RequestMapping(value = "productInfoByPid",method=RequestMethod.GET)
	@ResponseBody
	public String productInfoByPid(HttpServletRequest request,JSONObject json){
		/*SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		System.out.println(sdf1.format(new Date()));*/
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//返回状态
		String state = "";
		//状态信息
		String msg = "";
		//姓名、
		String name = null;
		//证件号、
		String idCard = null;
		// 国康父产品编码
		String gkProductPId = null;
		// 国康子产品编码
		String gkProductId = null;
		//客户编号
		String customerId = null;
		if(json.has("name")) {
			name = json.getString("name");
		}
		if(json.has("idCard")) {
			idCard = json.getString("idCard");
		}
		if (json.has("gkProductPId")&&null!=json.getString("gkProductPId")&&!"".equals(json.getString("gkProductPId"))) {
			gkProductPId = json.getString("gkProductPId");
		}else {
			state = "01";
			msg = "国康父产品编码为空";
			map.put("state", state);
			map.put("msg", msg);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if (json.has("gkProductId")&&null!=json.getString("gkProductId")&&!"".equals(json.getString("gkProductId"))) {
			gkProductId = json.getString("gkProductId");
		} else {
			state = "01";
			msg = "国康子产品编码为空";
			map.put("state", state);
			map.put("msg", msg);
			// 把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if(json.has("customerId")&&null!=json.getString("customerId")&&!"".equals(json.getString("customerId"))) {
			customerId = json.getString("customerId");
		}else {
			state = "01";
			msg = "客户编号为空";
			map.put("state", state);
			map.put("msg", msg);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		//根据客户编号，查询客户等级
		AppUserInfor appUserInfor = appUserInforDao.queryByCustomerId(customerId);
		//客户等级
		String level = "1";
		if(null != appUserInfor) {
			level = appUserInfor.getCustomerLevel();
		}
		
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerLevel",level);
		//paramMap.put("equitiesPid", gkProductPId);
		paramMap.put("equitiesId", gkProductId);
		//根据客户等级和国康产品编号查询客户的权限
		List<EquitiesDetail> equitiesDetailsList = equitiesDetailDao.queryByLevel(paramMap);
		if(equitiesDetailsList.size() > 0) {
			//获取当前年份
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			String start =String.valueOf(year)+"-01-01";
		
			//当前时间
			Date now = new Date();
			String end = sdf.format(now);
			List<EquitiesDetail2> equitList = new ArrayList<EquitiesDetail2>();
			//剩余权益次数
			String equitiesNum = null;
			for (EquitiesDetail equitiesDetail : equitiesDetailsList) {
				//子产品编号
				//String[] equitiesIds = equitiesDetail.getEquitiesId().split("/");
				String equitiesId = equitiesDetail.getEquitiesId();
				//权益次数
				String num = equitiesDetail.getEquitiesNum();
				Integer nums = Integer.valueOf(num);
				//使用次数
				/*Integer usedNum = 0;
				for (String equitiesId : equitiesIds) {*/
					//查询本年1月1号到11月18号客户已经使用过的权限
					Map<String, Object> mapGK = new HashMap<String, Object>();
					mapGK.put("start", start);
					mapGK.put("end", end);
					mapGK.put("customerId", customerId);
					mapGK.put("equitiesId", equitiesId);
					Integer count = equitiesUseDetailsDao.queryByCustomerId(mapGK);
					/*usedNum+=count;
				}*/
				if(nums > count) {
					equitiesNum = String.valueOf(nums-count);
				}else {
					equitiesNum = "0";
				}
				state = "00";
				msg = "查询成功";
			}
			map.put("equitiesNum", equitiesNum);
		} else {
			state = "01";
			msg = "客户没有权限";
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		//System.out.println(sdf1.format(new Date()));
		return jsons;
	}

}

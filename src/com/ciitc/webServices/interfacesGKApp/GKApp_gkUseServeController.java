package com.ciitc.webServices.interfacesGKApp;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.EquitiesDetailDao;
import com.ciitc.webServices.dao.EquitiesUseDetailsDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EquitiesDetail;
import com.ciitc.webServices.model.EquitiesUseDetails;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class GKApp_gkUseServeController extends BaseController {
	@SuppressWarnings("rawtypes")
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EquitiesUseDetailsDao<EquitiesUseDetails> equitiesUseDetailsDao = (EquitiesUseDetailsDao) SpringContextUtil
			.getBean("equitiesUseDetailsDao");
	private EquitiesDetailDao equitiesDetailDao = (EquitiesDetailDao) SpringContextUtil.getBean("equitiesDetailDao");
	

	/**
	 * 产品权益次数扣减接口
	 * 
	 */

	@RequestMapping(value = "gkUseServe", method = RequestMethod.GET)
	@ResponseBody
		public String gkUseServe(HttpServletRequest request, JSONObject json) {
		/*SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		System.out.println(sdf1.format(new Date()));*/
		HashMap<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> paramMap = new HashMap<String, Object>();

		String state = "";
		String msg = "";
		String name = "";
		String idCard = "";
		String customerId = "";
		String gkRecordId = "";
		// 姓名
		if (json.has("name")) {
			name = json.getString("name");
		}
		if (json.has("idCard")) {
			idCard = json.getString("idCard");
		}
		/*if (json.has("customerId")&&null!=json.getString("customerId")&&!"".equals(json.getString("customerId"))) {
			customerId = json.getString("customerId");
		}else {
			state = "01";
			msg = "客户编号为空";
			map.put("state", state);
			map.put("msg", msg);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}*/
		if (json.has("gkRecordId")&&null!=json.getString("gkRecordId")&&!"".equals(json.getString("gkRecordId"))) {
			gkRecordId = json.getString("gkRecordId");
		} else {
			state = "01";
			msg = "国康服务记录ID为空";
			map.put("state", state);
			map.put("msg", msg);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		paramMap.put("serviceRecordId", gkRecordId);
		// 00待使用，01待取消，02已预约，03已使用，04已撤销
		paramMap.put("useState", "00");
		//根据国康服务记录ID查询子产品编码和客户编号
		EquitiesUseDetails equitiesUseDetails = equitiesUseDetailsDao.selectBySID(gkRecordId);
		if(null == equitiesUseDetails) {
			map.put("state", "01");
			map.put("msg", "不存在此服务记录id");
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		customerId = equitiesUseDetails.getCustomerId();
		String gkProductId = equitiesUseDetails.getEquitiesId();
		//根据客户编号，查询客户等级
		AppUserInfor appUserInfor = appUserInforDao.queryByCustomerId(customerId);
		//客户等级
		String level = "1";
		if(null != appUserInfor) {
			level = appUserInfor.getCustomerLevel();
		}
		Map<String, Object> paramMap1 = new HashMap<String, Object>();
		paramMap1.put("customerLevel",level);
		//paramMap.put("equitiesPid", gkProductPId);
		paramMap1.put("equitiesId", gkProductId);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//根据客户等级和国康产品编号查询客户的权限
		List<EquitiesDetail> equitiesDetailsList = equitiesDetailDao.queryByLevel(paramMap1);
		//剩余权益次数
		int equitiesNum = 0;
		if(equitiesDetailsList.size() > 0) {
			//获取当前年份
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			String start =String.valueOf(year)+"-01-01";
			//当前时间
			Date now = new Date();
			String end = sdf.format(now);
			for (EquitiesDetail equitiesDetail : equitiesDetailsList) {
				//子产品编号
				String[] equitiesIds = equitiesDetail.getEquitiesId().split("/");
				//权益次数
				String num = equitiesDetail.getEquitiesNum();
				Integer nums = Integer.valueOf(num);
				//使用次数
				Integer usedNum = 0;
				for (String equitiesId : equitiesIds) {
					//查询本年1月1号至今客户已经使用过的权限
					Map<String, Object> mapGK = new HashMap<String, Object>();
					mapGK.put("start", start);
					mapGK.put("end", end);
					mapGK.put("customerId", customerId);
					mapGK.put("equitiesId", equitiesId);
					Integer count = equitiesUseDetailsDao.queryByCustomerId(mapGK);
					usedNum+=count;
				}
				if(nums > usedNum) {
					equitiesNum = nums-usedNum;
				}else {
					equitiesNum = 0;
				}
			}
		}
		if(equitiesNum>0) {
			// 根据国康服务记录ID更新权益状态
			if (equitiesUseDetailsDao.updateState(paramMap) > 0) {
				state = "00";// 成功
				msg = "使用成功";
			} else {
				state = "01";// 失败
				msg = "使用失败";
			}
		}else {
			state = "01";// 失败
			msg = "权益次数为零";
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		//System.out.println(sdf1.format(new Date()));
		return jsons;
	}
}

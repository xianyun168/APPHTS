package com.ciitc.webServices.interfacesGKApp;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.dao.EquitiesDetailDao;
import com.ciitc.webServices.dao.EquitiesUseDetailsDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.EquitiesUseDetails;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.util.KfcrmCustor;
import com.ciitc.webServices.util.SMSPublicMessageUntil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import cn.emay.util.SmsSingleRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;




public class GKApp_gkRecoverServeController extends BaseController {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EquitiesUseDetailsDao<EquitiesUseDetails> equitiesUseDetailsDao = (EquitiesUseDetailsDao) SpringContextUtil
			.getBean("equitiesUseDetailsDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil
			.getBean("esbmmCodeTypeDao");
	private EquitiesDetailDao equitiesDetailDao = (EquitiesDetailDao) SpringContextUtil.getBean("equitiesDetailDao");
	@SuppressWarnings("rawtypes")
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	@SuppressWarnings("rawtypes")
	private CPTfundbrokerDao cpTfundbrokerDao = (CPTfundbrokerDao) SpringContextUtil.getBean("CPTfundbrokerDao");

	/**
	 * 产品权益次数恢复接口
	 */	
	@RequestMapping(value = "gkRecoverServe", method = RequestMethod.GET)
	@ResponseBody
	public String gkRecoverServe(HttpServletRequest request, JSONObject json) {
		/*SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		System.out.println(sdf1.format(new Date()));*/
		HashMap<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> paramMap = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String state = "";
		String msg = "";
		String name = "";
		String idCard = "";
		String gkProductId = "";
		String gkRecordId = "";
		// 姓名
		if (json.has("name")) {
			name = json.getString("name");
		}
		if (json.has("idCard")) {
			idCard = json.getString("idCard");
		}
		if (json.has("gkProductId")) {
			gkProductId = json.getString("gkProductId");
		}
		if (json.has("gkRecordId")&&null!=json.getString("gkRecordId")&&!"".equals(json.getString("gkRecordId"))) {
			gkRecordId = json.getString("gkRecordId");
		} else {
			state = "01";
			msg = "国康服务记录ID为空";
			map.put("state", state);
			map.put("msg", msg);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		paramMap.put("serviceRecordId", gkRecordId);
		// 00待使用，01待取消，02已预约，03已使用，04已撤销
		paramMap.put("useState", "04");

		// 根据国康服务记录ID更新权益状态
		if (equitiesUseDetailsDao.updateState(paramMap) > 0) {
			state = "00";// 成功
			msg = "撤销成功，";
		} else {
			state = "01";// 失败
			msg = "撤销失败，";
		}
		// 发短信
		if (state.equals("00")) {
			paramMap.put("payType", "01");
			// 根据国康服务id和付费类型查询出信息
			EquitiesUseDetails equitiesUseDetails = equitiesUseDetailsDao.selectByRecordId1(paramMap);
			if (equitiesUseDetails != null) {
				String telephone = "";
				String equitiesName = "";
				String hospital = "";
				String department = "";
				Date medicalTime = new Date();

				equitiesName = equitiesUseDetails.getEquitiesName();
				telephone = equitiesUseDetails.getTelephone();
				hospital = equitiesUseDetails.getHospital();
				department = equitiesUseDetails.getDepartment();
				medicalTime = equitiesUseDetails.getMedicalTime();
				name = equitiesUseDetails.getUserName();
				// 当前时间
				Date currentTime = new Date();
				String now = sdf.format(currentTime);
				String medicalTime1 = sdf.format(medicalTime);

				// 客户所属分公司
				String companyName = "";
				// 客户理财师
				String financialName = "";
				// 根据客户编号，查询客户等级
				AppUserInfor appUserInfor = appUserInforDao.queryByCustomerId(equitiesUseDetails.getCustomerId());

				if (!"".equals(appUserInfor.getPhoneNumber())) {
					// 根据客户的电话查询所属公司
					KfcrmCustor kfcrmCustor = new KfcrmCustor();
					companyName = kfcrmCustor.queryCompanyByPhone(appUserInfor.getPhoneNumber());
				}
				String financialId = appUserInfor.getFinancialId();
				// 根据理财师编号查询理财师姓名
				CPTfundbroker financialInfo = cpTfundbrokerDao.selectByPrimaryKey(financialId);
				financialName = financialInfo.getcBrokername();

				String flag = sendMessage(name, telephone, now, equitiesName, hospital, department, medicalTime1,
						financialName, companyName);
				if (null != flag && flag.equals("成功")) {
					// 短信发送成功
					msg += "短信发送成功";
				} else {
					// 短信发送失败
					msg += "短信发送失败";
				}
			}
		}
		map.put("state", state);
		map.put("msg", msg);
		String jsons = JSONArray.fromObject(map).toString();
		//System.out.println(sdf1.format(new Date()));
		return jsons;
	}

	private String sendMessage(String name, String telephone, String now, String equitiesName, String hospital,
			String department, String medicalTime1, String financialName, String companyName) {

		SmsSingleRequest smsSingleRequest = new SmsSingleRequest();
		boolean isGzip = false;
		// AppId
		String appId = "";
		// 密匙
		String secretKey = "";
		// 接口地址
		String host = "";
		// 加密算法
		String algorithm = "";
		// 短信签名
		String sign = "";
		// ------------- -------------------------
		List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao.queryByTypeName("SMSConfiguration");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
		List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
		for (EsbmmCode esbmmCode : listCode) {
			if (esbmmCode.getCodeName().equals("AppId")) {
				appId = esbmmCode.getCodeValue();// AppId
			}
			if (esbmmCode.getCodeName().equals("secretKey")) {
				secretKey = esbmmCode.getCodeValue();// 密匙
			}
			if (esbmmCode.getCodeName().equals("interfaceHost")) {
				host = esbmmCode.getCodeValue();// 接口地址
			}
			if (esbmmCode.getCodeName().equals("algorithm")) {
				algorithm = esbmmCode.getCodeValue();// 加密算法
			}
			if (esbmmCode.getCodeName().equals("SMSSignature")) {
				sign = esbmmCode.getCodeValue();// 短信签名
			}
		}

		// 编辑短信内容
		String signMessage = sign + "您的客户" + name + "(电话:" + telephone + ",理财师:"+financialName+
				",所属分公司:"+companyName+"),在" + now + "取消了"+medicalTime1 + hospital+"的"+department+"的"+equitiesName+
				"服务。预约服务已取消，请知悉。";
		smsSingleRequest.setContent(signMessage);
		// 需要发送的手机号码
		// 查询数据字典
		String pkid = esbmmCodeTypeDao.queryByTypeName("testPhone").get(0).getPkId();
		String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
		smsSingleRequest.setMobile(fwqdkh);
		smsSingleRequest.setCustomSmsId("");
		smsSingleRequest.setExtendCode("");
		smsSingleRequest.setRequestTime(System.currentTimeMillis());
		smsSingleRequest.setRequestValidPeriod(30);
		smsSingleRequest.setTimerTime("");
		// 发送短信
		SMSPublicMessageUntil smsPublicMessageUntil = new SMSPublicMessageUntil();
		String flag = smsPublicMessageUntil.setSingleSms(smsSingleRequest, isGzip, appId, secretKey, host, algorithm);

		return flag;
	}
}

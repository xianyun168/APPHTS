package com.ciitc.webServices.interfacesGKApp;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.EquitiesDetailDao;
import com.ciitc.webServices.dao.EquitiesUseDetailsDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.EquitiesDetail;
import com.ciitc.webServices.model.EquitiesDetail2;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class GKApp_gkEquitiesListController extends BaseController {

	@SuppressWarnings({ "rawtypes" })
	@Autowired(required = false)
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	private EquitiesDetailDao equitiesDetailDao = (EquitiesDetailDao) SpringContextUtil.getBean("equitiesDetailDao");
	@SuppressWarnings({ "rawtypes" })
	@Autowired(required = false)
	private EquitiesUseDetailsDao equitiesUseDetailsDao = (EquitiesUseDetailsDao) SpringContextUtil
			.getBean("equitiesUseDetailsDao");

	/**
	 * 产品权益列表接口 
	 */
	@RequestMapping(value = "gkEquitiesList", method = RequestMethod.GET)
	@ResponseBody
	public String gkEquitiesList(HttpServletRequest request, JSONObject json) {
		/*SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		System.out.println(sdf1.format(new Date()));*/
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// 返回状态
		String state = "";
		// 状态信息
		String msg = "";
		// 姓名、
		String name = null;
		// 证件号、
		String idCard = null;
		// 客户编号
		String customerId = null;
		if (json.has("name")) {
			name = json.getString("name");
		}
		if (json.has("idCard")) {
			idCard = json.getString("idCard");
		}

		if (json.has("customerId")&&null!=json.getString("customerId")&&!"".equals(json.getString("customerId"))) {
			customerId = json.getString("customerId");
		} else {
			state = "01";
			msg = "客户编号为空";
			map.put("state", state);
			map.put("msg", msg);
			// 把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}

		// 根据客户编号查询等级
		AppUserInfor appUserInfor = appUserInforDao.queryByCustomerId(customerId);
		// 客户等级
		String level = "1";
		if (null != appUserInfor) {
			level = appUserInfor.getCustomerLevel();
		}

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerLevel", level);

		// 根据客户等级查询客户的权益
		List<EquitiesDetail> equitiesDetailsList = equitiesDetailDao.queryByLevel(paramMap);

		if (equitiesDetailsList.size() > 0) {
			List<EquitiesDetail2> equitList = new ArrayList<EquitiesDetail2>();
			// 获取当前年份
			Calendar calendar = Calendar.getInstance();
			int year = calendar.get(Calendar.YEAR);
			String start = String.valueOf(year) + "-01-01";
			// 当前时间
			Date now = new Date();
			String end = sdf.format(now);

			Map<String, Object> mapGK = new HashMap<String, Object>();
			mapGK.put("start", start);
			mapGK.put("end", end);
			mapGK.put("customerId", customerId);

			for (EquitiesDetail equitiesDetail : equitiesDetailsList) {
				// 国康父产品编码
				String equitiesPid = equitiesDetail.getEquitiesPid();
				// 国康子产品编码
				//String[] equitiesIds = equitiesDetail.getEquitiesId().split("/");
				/*Integer count = 0;
				if(equitiesIds.length>=2) {
					EquitiesDetail2 equit = new EquitiesDetail2();
					// 权益次数
					String num = equitiesDetail.getEquitiesNum();
					Integer nums = Integer.valueOf(num);
					equit.setGkProductPId(equitiesPid);
					for (int i=0; i<equitiesIds.length;i++) {
						//子编码
						String equitiesId = equitiesIds[i];
						mapGK.put("equitiesId", equitiesId);
						// 查询本年使用过的权益次数
						Integer usedCount = equitiesUseDetailsDao.queryByCustomerId(mapGK);
						//if (count > usedCount) {
						//	continue;
						//}
						//记录次数
						count += usedCount;
					}
					if (nums > count) {
						equit.setEquitiesNum(String.valueOf(nums - count));
					} else {
						equit.setEquitiesNum("0");
					}
					equit.setUsedEquitiesNum(String.valueOf(count));
					equitList.add(equit);
				}else {*/
					EquitiesDetail2 equit = new EquitiesDetail2();
					//子编码
					String equitiesId = equitiesDetail.getEquitiesId();
					// 权益次数
					String num = equitiesDetail.getEquitiesNum();
					mapGK.put("equitiesId", equitiesId);
					// 查询本年使用过的权益次数
					Integer usedCount = equitiesUseDetailsDao.queryByCustomerId(mapGK);
					equit.setUsedEquitiesNum(String.valueOf(usedCount));
					Integer nums = Integer.valueOf(num);
					equit.setGkProductPId(equitiesPid);
					equit.setGkProductId(equitiesId);
					if (nums > usedCount) {
						equit.setEquitiesNum(String.valueOf(nums - usedCount));
					} else {
						equit.setEquitiesNum("0");
					}
					equitList.add(equit);
				}
				state = "00";
				msg = "查询成功";
			//}
			map.put("equitList", equitList);
		} else {
			state = "01";
			msg = "客户没有权益";
		}
		map.put("state", state);
		map.put("msg", msg);
		// 把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		//System.out.println(sdf1.format(new Date()));
		return jsons;
	}
}

package com.ciitc.webServices.interfacesGKApp;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ciitc.webServices.dao.AppUserInforDao;
import com.ciitc.webServices.dao.CPTfundbrokerDao;
import com.ciitc.webServices.dao.EquitiesDetailDao;
import com.ciitc.webServices.dao.EquitiesUseDetailsDao;
import com.ciitc.webServices.dao.EsbmmCodeDao;
import com.ciitc.webServices.dao.EsbmmCodeTypeDao;
import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.model.CPTfundbroker;
import com.ciitc.webServices.model.EquitiesDetail;
import com.ciitc.webServices.model.EquitiesUseDetails;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.EsbmmCodeType;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.KfcrmCustor;
import com.ciitc.webServices.util.SMSPublicMessageUntil;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import cn.com.ins.sinosoft.framework.base.web.BaseController;
import cn.emay.util.SmsSingleRequest;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class GKApp_gkAppointmentController extends BaseController{


	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeDao<EsbmmCode> esbmmCodeDao = (EsbmmCodeDao) SpringContextUtil.getBean("esbmmCodeDao");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Autowired(required = false)
	private EsbmmCodeTypeDao<EsbmmCodeType> esbmmCodeTypeDao = (EsbmmCodeTypeDao) SpringContextUtil
			.getBean("esbmmCodeTypeDao");
	private EquitiesDetailDao equitiesDetailDao = (EquitiesDetailDao) SpringContextUtil.getBean("equitiesDetailDao");
	@SuppressWarnings("rawtypes")
	private EquitiesUseDetailsDao equitiesUseDetailsDao = (EquitiesUseDetailsDao) SpringContextUtil.getBean("equitiesUseDetailsDao");
	@SuppressWarnings("rawtypes")
	private AppUserInforDao appUserInforDao = (AppUserInforDao) SpringContextUtil.getBean("appUserInforDao");
	@SuppressWarnings("rawtypes")
	private CPTfundbrokerDao cpTfundbrokerDao = (CPTfundbrokerDao) SpringContextUtil.getBean("CPTfundbrokerDao");
	/**
	 * 服务信息接收或更新接口
	 */
	@RequestMapping(value = "productAppointment",method=RequestMethod.GET)
	@ResponseBody
	public String productAppointment(HttpServletRequest request,JSONObject json) {
		/*SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
		System.out.println(sdf1.format(new Date())+"开始");*/
		Map<String, Object> map = new HashMap<String, Object>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		//返回状态
		String state = "";
		//状态信息
		String msg = "";
		//姓名、
		String name = null;
		//证件号、
		String idCard = null;
		// 国康父产品编码
		String gkProductPId = null;
		// 国康子产品编码
		String gkProductId = null;
		//客户编号
		String customerId = null;
		//服务类型
		String serviceType = null;
		//预约就医时间
		String medicalTime1 = null;
		// 付费类型
	    String payType = null;
		// 就诊医院
	    String hospital = null;
		// 科室
	    String department = null;
		// 就医原因
	    String reason = null;
		// 国康服务记录ID
		String serviceRecordId = null;
		//客户电话
		String telephone=null;
		if(json.has("name")&&null!=json.getString("name")&&!"".equals(json.getString("name"))) {
			name = json.getString("name");
		}else {
			state = "01";
			msg = "姓名为空";
			map.put("state", state);
			map.put("msg", msg);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if(json.has("idCard")&&null!=json.getString("idCard")&&!"".equals(json.getString("idCard"))) {
			idCard = json.getString("idCard");
		}else {
			state = "01";
			msg = "证件号码为空";
			map.put("state", state);
			map.put("msg", msg);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if (json.has("gkProductPId")&&null!=json.getString("gkProductPId")&&!"".equals(json.getString("gkProductPId"))) {
			gkProductPId = json.getString("gkProductPId");
		}else {
			state = "01";
			msg = "国康父产品编码为空";
			map.put("state", state);
			map.put("msg", msg);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if (json.has("gkProductId")&&null!=json.getString("gkProductId")&&!"".equals(json.getString("gkProductId"))) {
			gkProductId = json.getString("gkProductId");
		} else {
			state = "01";
			msg = "国康子产品编码为空";
			map.put("state", state);
			map.put("msg", msg);
			// 把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if(json.has("customerId")&&null!=json.getString("customerId")&&!"".equals(json.getString("customerId"))) {
			customerId = json.getString("customerId");
		}else {
			state = "01";
			msg = "客户编号为空";
			map.put("state", state);
			map.put("msg", msg);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if (json.has("serviceType")&&null!=json.getString("serviceType")&&!"".equals(json.getString("serviceType"))) {
			serviceType = json.getString("serviceType");
		} else {
			state = "01";
			msg = "服务类型为空";
			map.put("state", state);
			map.put("msg", msg);
			// 把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if (json.has("medicalTime")&&null!=json.getString("medicalTime")&&!"".equals(json.getString("medicalTime"))) {
			medicalTime1 = json.getString("medicalTime");
		} else {
			state = "01";
			msg = "预约时间为空";
			map.put("state", state);
			map.put("msg", msg);
			// 把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if (json.has("gkRecordId")&&null!=json.getString("gkRecordId")&&!"".equals(json.getString("gkRecordId"))) {
			serviceRecordId = json.getString("gkRecordId");
		}else {
			state = "01";
			msg = "国康服务记录ID为空";
			map.put("state", state);
			map.put("msg", msg);
			//把map集合转成json字符串
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}

		if (json.has("payType")&&null!=json.getString("payType")&&!"".equals(json.getString("payType"))) {
			payType = json.getString("payType");
		} else {
			state = "01";
			msg = "付费类型为空";
			map.put("state", state);
			map.put("msg", msg);
			String jsons = JSONArray.fromObject(map).toString();
			return jsons;
		}
		if (json.has("telephone")) {
			telephone = json.getString("telephone");
		}
		if (json.has("reason")) {
			reason = json.getString("reason");
		}
		if (json.has("hospital")) {
			hospital = json.getString("hospital");
		}
		if (json.has("department")) {
			department = json.getString("department");
		}
		//当前时间
		Date currentTime = new Date();
		String now = sdf.format(currentTime);
		//就医时间
		Date medicalTime = new Date();
		try {
			medicalTime = sdf.parse(medicalTime1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//是否是更新
		String flags = "0";
		//根据服务记录id查询是否已存在，如果存在则删除已存在的记录
		EquitiesUseDetails equitiesUseDetails= equitiesUseDetailsDao.selectByRecordId(serviceRecordId);
		if (null != equitiesUseDetails) {
			flags = "1";
			equitiesUseDetailsDao.deleteByRecordId(serviceRecordId);
			//System.out.println(sdf1.format(new Date())+"删除变更之前的数据");
		}else {
			if("03".equals(serviceType)||"04".equals(serviceType)) {
				state = "01";
				msg = "国康服务记录ID为空";
				map.put("state", state);
				map.put("msg", msg);
				String jsons = JSONArray.fromObject(map).toString();
				return jsons;
			}
		}
		//子产品名称
		String equitiesName = null;
		String equitiesPname = null;
		//客户所属分公司
		String companyName="";
		//客户理财师
		String financialName = "";
		// 根据客户编号，查询客户等级
		AppUserInfor appUserInfor = appUserInforDao.queryByCustomerId(customerId);
		//System.out.println(sdf1.format(new Date())+"查询客户表");
		// 客户等级
		String level = "1";
		if (null != appUserInfor) {
			level = appUserInfor.getCustomerLevel();
		}
		if(!"".equals(appUserInfor.getPhoneNumber())) {
			//根据客户的电话查询所属公司
			KfcrmCustor kfcrmCustor = new KfcrmCustor();
			companyName = kfcrmCustor.queryCompanyByPhone(appUserInfor.getPhoneNumber());
			//System.out.println(sdf1.format(new Date())+"查询所属公司");
		}
		String financialId = appUserInfor.getFinancialId();
		//根据理财师编号查询理财师姓名
		financialName = cpTfundbrokerDao.selectNameByKey(financialId);
		//System.out.println(sdf1.format(new Date())+"查询理财师姓名");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("customerLevel", level);
		paramMap.put("EquitiesId", gkProductId);
		//根据子产品编号查询子产品名称
		EquitiesDetail equitiesDetail = equitiesDetailDao.queryById1(paramMap);
		//System.out.println(sdf1.format(new Date())+"查询权益表");
		equitiesName = equitiesDetail.getEquitiesName();
		//根据父产品编号查询父产品名称
		equitiesPname = equitiesDetail.getEquitiesPname();
		EquitiesUseDetails equit = new EquitiesUseDetails();
		equit.setPkId(UUID.randomUUID().toString());
		equit.setCreateTime(currentTime);
		equit.setUserName(name);
		equit.setIdCard(idCard);
		equit.setEquitiesPid(gkProductPId);
		equit.setEquitiesPname(equitiesPname);
		equit.setEquitiesId(gkProductId);
		equit.setEquitiesName(equitiesName);
		equit.setCustomerId(customerId);
		equit.setPayType(payType);
		equit.setHospital(hospital);
		equit.setDepartment(department);
		equit.setReason(reason);
		equit.setUseState(serviceType);
		equit.setMedicalTime(medicalTime);
		equit.setServiceRecordId(serviceRecordId);
		equit.setTelephone(telephone);
		if (equitiesUseDetailsDao.insertSelective(equit) > 0) {
			//System.out.println(sdf1.format(new Date())+"保存预约记录");
			state = "00";
			msg = "成功,";

		} else {
			state = "01";
			msg = "失败,";
		}
		String signMessage=null;
		if("01".equals(payType)&&"0".equals(flags)&&"02".equals(serviceType)) {
			signMessage = "您的客户" + name + "(电话:" + telephone + ",理财师:"+financialName+
					",所属分公司:"+companyName+"),在" + now + "预约了"+medicalTime1 + hospital+department+equitiesName+
					"服务。";
		}else if("00".equals(payType)&&"0".equals(flags)&&"02".equals(serviceType)){
			signMessage = "您的客户" + name + "(电话:" + telephone + ",理财师:"+financialName+
					",所属分公司:"+companyName+"),在" + now + "预约了"+medicalTime1 + hospital+department+equitiesName+
					"服务。此预约为客户自费预约，请知悉。";
		}else if("1".equals(flags)&&"00".equals(payType)&&"04".equals(serviceType)){
			signMessage ="您的客户" + name + "(电话:" + telephone + ",理财师:"+financialName+
					",所属分公司:"+companyName+"),在" + now + "取消了"+medicalTime1 + hospital+department+equitiesName+
					"服务。此客户为自费预约，服务已取消，请知悉。";
		}else if("1".equals(flags)&&"02".equals(serviceType)){
			signMessage =  "您的客户" + equitiesUseDetails.getUserName() + "(电话:" + equitiesUseDetails.getTelephone() + ",理财师:"+financialName+
					",所属分公司:"+companyName+"),在" + sdf.format(equitiesUseDetails.getCreateTime())+ "预约了"+sdf.format(equitiesUseDetails.getMedicalTime())+ equitiesUseDetails.getHospital()+equitiesUseDetails.getDepartment()+equitiesUseDetails.getEquitiesName()+
					"服务。预约信息变更为："+"您的客户" + name + "(电话:" + telephone + ",理财师:"+financialName+
					",所属分公司:"+companyName+"),在" + now + "预约了"+medicalTime1 + hospital+"的"+department+"的"+equitiesName+
					"服务。请知悉。";
		}
		if (state.equals("00")&&!"".equals(signMessage)&&null != signMessage) {
			//System.out.println(sdf1.format(new Date())+"开始发送短信");
			//String flag = sendMessage(name, telephone, now, equitiesName,department,hospital,medicalTime1,financialName,companyName,flags,payType,equitiesUseDetails,serviceType);
			String flag = sendMessage(signMessage);
			if (null != flag && flag.equals("成功")) {
				// 短信发送成功
				msg += "短信发送成功";
			} else {
				// 短信发送失败
				msg += "短信发送失败";
			}
		}
		map.put("state", state);
		map.put("msg", msg);
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		//System.out.println(sdf1.format(new Date()));
		return jsons;
	}
	//String name, String telephone, String now, String equitiesName,String department,String hospital,
	//String medicalTime1,String financialName,String companyName,String flags,String payType,
	//EquitiesUseDetails equitiesUseDetails,String serviceType
	private String sendMessage(String signMessage) {
		SmsSingleRequest smsSingleRequest = new SmsSingleRequest();
		boolean isGzip = false;
		// AppId
		String appId = "";
		// 密匙
		String secretKey = "";
		// 接口地址
		String host = "";
		// 加密算法
		String algorithm = "";
		// 短信签名
		String sign = "";
		// ------------- -------------------------
		List<EsbmmCodeType> esbmmCodeType = esbmmCodeTypeDao.queryByTypeName("SMSConfiguration");
		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("codeTypePkId", esbmmCodeType.get(0).getPkId());
		List<EsbmmCode> listCode = esbmmCodeDao.selectByContion(paramMap);
		for (EsbmmCode esbmmCode : listCode) {
			if (esbmmCode.getCodeName().equals("AppId")) {
				appId = esbmmCode.getCodeValue();// AppId
			}
			if (esbmmCode.getCodeName().equals("secretKey")) {
				secretKey = esbmmCode.getCodeValue();// 密匙
			}
			if (esbmmCode.getCodeName().equals("interfaceHost")) {
				host = esbmmCode.getCodeValue();// 接口地址
			}
			if (esbmmCode.getCodeName().equals("algorithm")) {
				algorithm = esbmmCode.getCodeValue();// 加密算法
			}
			if (esbmmCode.getCodeName().equals("SMSSignature")) {
				sign = esbmmCode.getCodeValue();// 短信签名
			}
		}
		// 编辑短信内容
		String signMessage1 = sign+signMessage;
		smsSingleRequest.setContent(signMessage1);
		// 需要发送的手机号码
		// 查询数据字典
		String pkid = esbmmCodeTypeDao.queryByTypeName("testPhone").get(0).getPkId();
		String fwqdkh = esbmmCodeDao.queryByTypeIds(pkid).get(0).getCodeValue();
		smsSingleRequest.setMobile(fwqdkh);
		smsSingleRequest.setCustomSmsId("");
		smsSingleRequest.setExtendCode("");
		smsSingleRequest.setRequestTime(System.currentTimeMillis());
		smsSingleRequest.setRequestValidPeriod(30);
		smsSingleRequest.setTimerTime("");
		// 发送短信
		SMSPublicMessageUntil smsPublicMessageUntil = new SMSPublicMessageUntil();
		String flag = smsPublicMessageUntil.setSingleSms(smsSingleRequest, isGzip, appId, secretKey, host, algorithm);
		return flag;
	}
}

package com.ciitc.webServices.procedure;

import com.ciitc.webServices.model.AppUserInfor;
import com.ciitc.webServices.test.DBUtil;
import oracle.jdbc.OracleTypes;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 配置折标系数成功后，调用存储过程
 */
public class CoefficientProcedure {
	
	Connection conn = DBUtil.makeConnection();
    CallableStatement call=null;
    /**
     *  测试方法
     */
    public String test(AppUserInfor appUserInfor){
        try {
			call=conn.prepareCall("{call CRMTEST.PKG_CRM_INTERFACE.SP_addUnregister(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
			 	call.setString(1,appUserInfor.getUserName());//客户姓名
		        call.setString(2, "aa");//客户简称
		        call.setString(3, "1");//客户类型
		        call.setString(4, appUserInfor.getIdcardNumber());//证件类型
		        call.setString(5, appUserInfor.getIdcardType());//证件号码
		        call.setString(6, "");//性别
		        call.setString(7, "");//国家
		        call.setString(8, "");//语言
		        call.setString(9, "");//联系电话
		        call.setString(10, appUserInfor.getPhoneNumber());//手机号码
		        call.setString(11, "");//邮件地址
		        call.setString(12, appUserInfor.getCustomerAddress());//地址
		        call.registerOutParameter(13, OracleTypes.VARCHAR);//--返回客户编号
		        call.registerOutParameter(14, OracleTypes.VARCHAR);//是否成功标识,0失败 1成功
		        call.registerOutParameter(15, OracleTypes.VARCHAR);//错误原因
		        call.setString(16, "0");
		        call.setString(17, "");
		        call.setString(18, "");
		        call.setString(19, "");
		        call.setString(20, "");
		        call.setString(21, "0");
		        call.setString(22, "");
		        call.setString(23, "");
		        call.setString(24, "1");
		        call.setString(25, "1");
		        //执行
		        call.execute();
		        String responsecustno = call.getString(13);
		        String responsecode = call.getString(14);
		        String responsereason=call.getString(15);
		        System.out.println("responsecustno: " + responsecustno
		        		                  + ", responsecode: " + responsecode
		        		                  + ", responsereason: " + responsereason
		        		                      );
		        appUserInfor.setCustomerId(responsecustno);
		        return responsecustno;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//P_COMMON为包名，QUERY_PROVINCE_PAGE为存储过程名，？代表参数
		return null;
    }
    
    
    /**
     * @param productId 产品ID
     */
    public String call(String productId){
        try {
			call=conn.prepareCall("{call CRMTEST.PKG_CRM_INTERFACE.sp_querytfundbrokerrelation(?,?,?)}");
			 	call.setString(1,productId);
		        call.setString(2, "");
		        call.registerOutParameter(3, OracleTypes.CURSOR);//返回游标
		        //执行
		        call.execute();
		        ResultSet resultSet= (ResultSet) call.getObject(3);
		        String ficinalId = null;
		        while(resultSet.next()){
		        		ficinalId = resultSet.getString("c_brokeraccount");
		        }
		        System.out.println("ficinalId: " + ficinalId);
		      return ficinalId;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//P_COMMON为包名，QUERY_PROVINCE_PAGE为存储过程名，？代表参数
		return null;
    }
}

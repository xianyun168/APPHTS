package com.ciitc.webServices.servlet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;

import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.RoleDetailService;

public class AuthorityServlet extends HandlerInterceptorAdapter {
	
	@Autowired(required = false)
    private RoleDetailService<RoleDetailInfo> roleDetailService;
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		super.afterCompletion(request, response, handler, ex);
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		String menuName = "交强险保单查询";
		String authorityCode = "update1";
		UserInfo user = ResourceUtil.getSessionUserName();
		boolean authority = false;
		if(user!=null){
			 List<RoleDetailInfo> roleDetailList = new ArrayList<RoleDetailInfo>();
			 try {
				//根据菜单名和用户名查询有什么权限
				roleDetailList=roleDetailService.getDescription(user.getUserName(),menuName);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//权限中是否开通此权限
			 for(RoleDetailInfo roleDetailInfo:roleDetailList){
				 if(roleDetailInfo.getDescription().contains(authorityCode)){
					 authority = true;
					 return true;
				 }
			 }
		}
		//未开通此权限则直接返回
		 if(!authority){
			 return true;
		 }
		
		return true;
	}

}

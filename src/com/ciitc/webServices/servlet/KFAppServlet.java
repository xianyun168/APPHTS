package com.ciitc.webServices.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ciitc.webServices.interfaces.KF_ActivityInfoController;
import com.ciitc.webServices.interfaces.KF_CRM_CompanyNewsContorller;
import com.ciitc.webServices.interfaces.KF_CRM_FirstNewsContorller;
import com.ciitc.webServices.interfaces.KF_CRM_IndustryNewsContorller;
import com.ciitc.webServices.interfaces.KF_CRM_ProductInfoController;
import com.ciitc.webServices.interfaces.KF_CRM_PublishNewsContorller;
import com.ciitc.webServices.interfaces.KF_CompanyInfoController;
import com.ciitc.webServices.interfaces.KF_GoodsInterfaceController;
import com.ciitc.webServices.interfaces.KF_HelpQueryController;
import com.ciitc.webServices.interfaces.KF_IsBuyProductController;
import com.ciitc.webServices.interfaces.KF_LoginController;
import com.ciitc.webServices.interfaces.KF_NoticeController;
import com.ciitc.webServices.interfaces.KF_OpinionController;
import com.ciitc.webServices.interfaces.KF_OrderProductController;
import com.ciitc.webServices.interfaces.KF_PersonCenterController;
import com.ciitc.webServices.interfaces.KF_ProfitController;
import com.ciitc.webServices.interfaces.KF_SignInterfaceController;

import net.sf.json.JSONObject;

public class KFAppServlet extends HttpServlet {

	/**
	 * 客服APP公共接口
	 */
	private static final long serialVersionUID = 1L;
	String login = "login";// 登录
	String regidter = "regidter";// 注册
	String realNameRegistration = "realNameRegistration";// 实名注册
	String getHelpQuery = "getHelpQuery";// 帮助文档
	String getCompanyInfo = "getCompanyInfo";// 公司基本信息
	String getSignQuery = "getSignQuery";// 活动报名接口
	String saveOpinion = "saveOpinion";// 意见建议接口
	String getUserInFo = "getUserInFo";// 个人中心接口
	String queryActivityInfo = "queryActivityInfo";// 活动信息查询
	String getNewsListQuery = "getNewsListQuery";// 轮播新闻列表、详情接口
	String getIndustryNewsQueryList = "getIndustryNewsQueryList";//行业新闻列表、详情接口
	String getCompanyNewsQueryList = "getCompanyNewsQueryList";//公司新闻列表、详情接口
	String getFirstNewsQueryList = "getFirstNewsQueryList";//新闻头条列表、详情接口
	String KF_Transaction = "KF_Transaction";// 交易记录查询
	String KF_HistoryProfit = "KF_HistoryProfit";//历史累计总收益
    String KF_ProfitDetailed = "KF_ProfitDetailed";//产品历史详细收益
    String KF_Profit = "KF_Profit";//持仓产品，持仓在总额，累计收益，持仓收益查询
    String getProductList="getProductList";//产品列表、详情查询
    String getProductProfit="getProductProfit";//根据收益查询产品列表
    String getProductPermin="getProductPermin";//根据产品期限查询产品列表
    String getProductDeadline="getProductDeadline";//根据起投金额查询产品列表
    String orderProductSMS="orderProductSMS";//客户在app点击预约产品，短信通知理财师
    String saveaa = "saveaa";
    String queryGoodsList ="queryGoodsList";
    String getnotice ="getnotice";//公告
    String isBuyProduct = "isBuyProduct";//判断是否购买过产品
	public KFAppServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// StringBuffer type = request.getRequestURL();

		BufferedReader type = new BufferedReader(
				new InputStreamReader((ServletInputStream) request.getInputStream(), "utf-8"));
		StringBuffer sb = new StringBuffer("");
		String temp;
		while ((temp = type.readLine()) != null) {
			sb.append(temp);
		}
		type.close();
		String params = sb.toString();
		System.out.println(params);

		JSONObject json = JSONObject.fromObject(params);
		String interfaceAPI = json.getString("interfaceAPI");
		// 登录
		if (interfaceAPI.equals("login")) {
			KF_LoginController login = new KF_LoginController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(login.Login(request, json));
			return;

			// 实名制注册
		} else if (interfaceAPI.equals("realNameRegistration")) {
			KF_LoginController login = new KF_LoginController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(login.realNameRegistration(request, json));
			return;

			//发送验证码
		}else if (interfaceAPI.equals("sandSMS")) {
			KF_LoginController login = new KF_LoginController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			try {
				response.getWriter().print(login.sandSMS(request, json));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;

			//获取地区 组织机构
		}else if (interfaceAPI.equals("addressShow")) {
			KF_LoginController login = new KF_LoginController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			try {
				response.getWriter().print(login.addressShow(request, json));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;

			
		}
		// 帮助文档
		else if (interfaceAPI.equals("getHelpQuery")) {
			KF_HelpQueryController helpQuery = new KF_HelpQueryController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(helpQuery.getHelpQuery(request, json));
			return;

			// 公司基本信息
		} else if (interfaceAPI.equals("getCompanyInfo")) {
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			KF_CompanyInfoController companyInfo = new KF_CompanyInfoController();
			response.getWriter().print(companyInfo.getCompanyInfo(request, json));
			return;

			// 活动报名接口
		} else if (interfaceAPI.equals("getSignQuery")) {
			KF_SignInterfaceController signIncontroller = new KF_SignInterfaceController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(signIncontroller.getSignQuery(request, json));
			return;

			// 意见建议接口
		} else if (interfaceAPI.equals("saveOpinion")) {
			KF_OpinionController opinion = new KF_OpinionController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(opinion.saveOpinion(request, json));
			return;

			// 个人中心接口
		} else if (interfaceAPI.equals("getUserInFo")) {
			KF_PersonCenterController personCenter = new KF_PersonCenterController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(personCenter.getUserInFo(request, json));
			return;

			// 持仓产品，持仓在总额，累计收益，持仓收益查询
		} else if (interfaceAPI.equals("KF_Profit")) {
			KF_ProfitController profit = new KF_ProfitController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(profit.getPosition(request, json));
			return;

			// 活动信息查询
		} else if (interfaceAPI.equals("queryActivityInfo")) {
			KF_ActivityInfoController kfActivityInfoController = new KF_ActivityInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(kfActivityInfoController.queryActivityInfo(request, json));
			return;

			// 轮播新闻列表、详情查询
		} else if (interfaceAPI.equals("getNewsListQuery")) {
			KF_CRM_PublishNewsContorller publishNewsContorller = new KF_CRM_PublishNewsContorller();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(publishNewsContorller.getNewsListQuery(request, json));
			return;
			//公司新闻列表、详情查询
		}else if (interfaceAPI.equals("getCompanyNewsQueryList")) {
			KF_CRM_CompanyNewsContorller publishNewsContorller = new KF_CRM_CompanyNewsContorller();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(publishNewsContorller.getCompanyNewsQueryList(request, json));
			return;
			//行业新闻列表、详情查询
		}else if (interfaceAPI.equals("getIndustryNewsQueryList")) {
			KF_CRM_IndustryNewsContorller publishNewsContorller = new KF_CRM_IndustryNewsContorller();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(publishNewsContorller.getIndustryNewsQueryList(request, json));
			return;
			//新闻头条列表、详情查询
		}else if (interfaceAPI.equals("getFirstNewsQueryList")) {
			KF_CRM_FirstNewsContorller publishNewsContorller = new KF_CRM_FirstNewsContorller();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(publishNewsContorller.getFirstNewsQueryList(request, json));
			return;
		/*}
			// 交易记录查询
			 else if (interfaceAPI.equals("getProductListQuery")) {
				 KF_CRM_ProductInfoController productInfoController = new KF_CRM_ProductInfoController();
					response.setContentType("utf-8");
					response.setCharacterEncoding("utf-8");
					response.getWriter().print(productInfoController.getProductList(request, json));
					return;
*/
					// 交易记录查询
		} else if (interfaceAPI.equals("KF_Transaction")) {
			
			/*String userid  = json.getString("userid");*/
			
			/*System.out.println(KF_LoginController.userToken.get(userid));*/
			KF_ProfitController profit = new KF_ProfitController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(profit.getTransaction(request, json));
			return;
			
			//历史累计总收益
		}else if (interfaceAPI.equals("KF_HistoryProfit")) {
			KF_ProfitController profit = new KF_ProfitController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(profit.getHistoryProfit(request, json));
			return;
			
			//产品历史详细收益
		}else if (interfaceAPI.equals("KF_ProfitDetailed")) {
			KF_ProfitController profit = new KF_ProfitController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(profit.getProfitDetailed(request, json));
			return;
			//查询产品列表
		}else if (interfaceAPI.equals("getProductList")) {
			KF_CRM_ProductInfoController productInfo=new KF_CRM_ProductInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getProductList(request, json));
			return;
		//根据收益查询产品列表
		}else if (interfaceAPI.equals("getProductProfit")) {
			KF_CRM_ProductInfoController productInfo=new KF_CRM_ProductInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getProductProfit(request, json));
			return;
			//根据投资金额查询产品列表
		}else if (interfaceAPI.equals("getProductPermin")) {
			KF_CRM_ProductInfoController productInfo=new KF_CRM_ProductInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getProductPermin(request, json));
			return;
			//根据投资期限查询产品列表
		}else if (interfaceAPI.equals("getProductDeadline")) {
			KF_CRM_ProductInfoController productInfo=new KF_CRM_ProductInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getProductDeadline(request, json));
			return;
			//客户在app点击预约产品，短信通知理财师
		}else if (interfaceAPI.equals("orderProductSMS")) {
			KF_OrderProductController orderProduct=new KF_OrderProductController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			try {
				response.getWriter().print(orderProduct.orderProductSMS(request, json));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}else if(interfaceAPI.equals("queryGoodsList")){
			KF_GoodsInterfaceController goodsList =new KF_GoodsInterfaceController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			try {
				response.getWriter().print(goodsList.queryGoodsList(null, null, ""));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//公告
		}else if(interfaceAPI.equals("getnotice")){
			KF_NoticeController goodsList =new KF_NoticeController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			try {
				response.getWriter().print(goodsList.getnotice(request, json));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else if(interfaceAPI.equals("isBuyProduct")){
			KF_IsBuyProductController goodsList =new KF_IsBuyProductController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			try {
				response.getWriter().print(goodsList.isBuyProduct(request, json));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
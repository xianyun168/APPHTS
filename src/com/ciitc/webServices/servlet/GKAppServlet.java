package com.ciitc.webServices.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.ins.sinosoft.framework.base.util.StringUtil;
import com.ciitc.webServices.interfaces.KF_IsBuyProductController;
import com.ciitc.webServices.interfacesGKApp.GKApp_gkAppointmentController;
import com.ciitc.webServices.interfacesGKApp.GKApp_gkEquitiesListController;
import com.ciitc.webServices.interfacesGKApp.GKApp_gkProductInfoByPidController;
import com.ciitc.webServices.interfacesGKApp.GKApp_gkRecoverServeController;
import com.ciitc.webServices.interfacesGKApp.GKApp_gkUseServeController;

import com.ciitc.webServices.util.JsonParamUtil;
import com.google.gson.Gson;
import net.sf.json.JSONObject;

public class GKAppServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GKAppServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		BufferedReader type = new BufferedReader(
				new InputStreamReader((ServletInputStream) request.getInputStream(), "utf-8"));
		StringBuffer sb = new StringBuffer("");
		String temp;
		while ((temp = type.readLine()) != null) {
			sb.append(temp);
		}
		type.close();
		String params = sb.toString();
		System.out.println(params);
		JSONObject json = JsonParamUtil.getJsonPram(params);
		String interfaceAPI = json.getString("interfaceAPI");
		//检查客户是否存在持仓产品
		if (interfaceAPI.equals("isBuyProduct")) {
			KF_IsBuyProductController login = new KF_IsBuyProductController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(login.isBuyProduct(request, json));
			return;
			//检查客户的权限以及次数
		} else if (interfaceAPI.equals("productInfoByPid")) {
			GKApp_gkProductInfoByPidController login = new GKApp_gkProductInfoByPidController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(login.productInfoByPid(request, json));
			return;
			// 产品权益次数恢复接口
		} else if (interfaceAPI.equals("gkRecoverServe")) {
			GKApp_gkRecoverServeController gkRecoverServeController = new GKApp_gkRecoverServeController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(gkRecoverServeController.gkRecoverServe(request, json));
			return;
			// 产品权益次数扣减接口
		} else if (interfaceAPI.equals("gkUseServe")) {
			GKApp_gkUseServeController gkUseServeController = new GKApp_gkUseServeController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(gkUseServeController.gkUseServe(request, json));
			return;
			//服务信息接收或更新接口
		} else if (interfaceAPI.equals("productAppointment")) {
			GKApp_gkAppointmentController gkUseServeController = new GKApp_gkAppointmentController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(gkUseServeController.productAppointment(request, json));
			return;
			// 产品权益列表接口
		} else if (interfaceAPI.equals("gkEquitiesList")) {
			GKApp_gkEquitiesListController gkEquitiesListController = new GKApp_gkEquitiesListController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(gkEquitiesListController.gkEquitiesList(request, json));
			return;
		}
	}

}

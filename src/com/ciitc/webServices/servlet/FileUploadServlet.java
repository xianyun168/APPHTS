package com.ciitc.webServices.servlet;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;

import com.ciitc.webServices.dao.FileUploadDao;
import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.model.FileUploads;
import com.ciitc.webServices.service.EsbmmCodeService;
import com.ciitc.webServices.util.Log;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import net.sf.json.JSON;
import net.sf.json.JSONArray;


//@MultipartConfig
public class FileUploadServlet extends HttpServlet{
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
	
	private FileUploadDao fileUploadDao = (FileUploadDao) SpringContextUtil.getBean("fileUploadDao");
	
	private static final long serialVersionUID = 1L;

	public FileUploadServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String tempUrl = null;
		List<EsbmmCode> listCode = esbmmCodeService.getCrmUploadFile();
		for (EsbmmCode esbmmCode : listCode) {
			if(esbmmCode.getCodeName().equals("tempPath")) {
				tempUrl = esbmmCode.getCodeValue();//临时上传目录
			}
		}
		//String tempUrl = "D:/opt/crm/";
		// 配置上传参数
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        // 中文处理
        upload.setHeaderEncoding("UTF-8"); 
        try {
            // 解析请求的内容提取文件数据（文件数量集合）
            List<FileItem> formItems = upload.parseRequest(request);
            if (formItems != null && formItems.size() > 0) {
            	Log.getLogger().info("已接收App传的图片");
            	for (FileItem item : formItems) {
					if (!item.isFormField()) {
						//附件名称 
						String p_filename =  item.getName();
						long p_filesize = item.getSize();//附件大小
						//System.out.println(p_filename + p_filesize);
						File storeFile = new File(tempUrl+"/"+p_filename);
						item.write(storeFile);
						Log.getLogger().info("图片已上传到临时目录");
						FileUploads fileUploads = new FileUploads();
						fileUploads.setPkId(UUID.randomUUID().toString());
						fileUploads.setOldFileName(p_filename);
						fileUploadDao.insert(fileUploads);
						response.setContentType("utf-8");
						response.setCharacterEncoding("utf-8");
					}
            	}
            	Map<String, Object> map = new HashMap<String, Object>();
				map.put("state", "00");
				map.put("msg", "成功");
				//把map集合转成json字符串
				String jsons = JSONArray.fromObject(map).toString();
				response.getWriter().print(jsons);
            }
        } catch (Exception ex) {
           ex.printStackTrace();
        }
	}

}

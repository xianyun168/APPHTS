package com.ciitc.webServices.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ciitc.webServices.model.EsbmmCode;
import com.ciitc.webServices.service.EsbmmCodeService;

import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
import net.sf.json.JSONArray;

/**
 * Servlet implementation class VersionServlet
 */
public class VersionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private EsbmmCodeService<EsbmmCode>   esbmmCodeService = (EsbmmCodeService) SpringContextUtil.getBean("esbmmCodeService");
    public VersionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("utf-8");
		response.setCharacterEncoding("utf-8");
		List<EsbmmCode> versionList = esbmmCodeService.getVersion();
		List<EsbmmCode> addressList = esbmmCodeService.getAppAddress();
		Map<String, Object> map = new HashMap<String, Object>();
		for (EsbmmCode esbmmCode : versionList) {
			map.put(esbmmCode.getCodeName(),esbmmCode.getCodeValue());
		}
		for (EsbmmCode esbmmCode : addressList) {
			map.put(esbmmCode.getCodeName(),esbmmCode.getCodeValue());
		}
		map.put("state", "00");
		map.put("msg", "成功");
		//把map集合转成json字符串
		String jsons = JSONArray.fromObject(map).toString();
		response.getWriter().print(jsons);
	}

}

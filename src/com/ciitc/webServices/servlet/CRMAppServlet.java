package com.ciitc.webServices.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ciitc.webServices.interfaces.CRM_LoginController;
import com.ciitc.webServices.interfacesCrm.CRM_Achievement;
import com.ciitc.webServices.interfacesCrm.CRM_ApplicationController;
import com.ciitc.webServices.interfacesCrm.CRM_BankName;
import com.ciitc.webServices.interfacesCrm.CRM_CollectionController;
import com.ciitc.webServices.interfacesCrm.CRM_ContractController;
import com.ciitc.webServices.interfacesCrm.CRM_CustomInfoController;
import com.ciitc.webServices.interfacesCrm.CRM_InvestorAuthenticationController;
import com.ciitc.webServices.interfacesCrm.CRM_KnowledgeBaseController;
import com.ciitc.webServices.interfacesCrm.CRM_MarkTiltleController;
import com.ciitc.webServices.interfacesCrm.CRM_MemoInfoCalendarController;
import com.ciitc.webServices.interfacesCrm.CRM_MemoInfoController;
import com.ciitc.webServices.interfacesCrm.CRM_Message;
import com.ciitc.webServices.interfacesCrm.CRM_PersonCenterController;
import com.ciitc.webServices.interfacesCrm.CRM_ProductController;
import com.ciitc.webServices.interfacesCrm.CRM_ProfessionalApproveController;
import com.ciitc.webServices.interfacesCrm.CRM_RegionType;
import com.ciitc.webServices.interfacesCrm.CRM_ReservationController;
import com.ciitc.webServices.interfacesCrm.CRM_SaveCollectionController;
import com.ciitc.webServices.interfacesCrm.CRM_SignController;
import com.ciitc.webServices.interfacesCrm.CRM_SignQueryController;

import net.sf.json.JSONObject;
@MultipartConfig
public class CRMAppServlet extends HttpServlet {

	/**
	 * CRM APP公共接口
	 */
	private static final long serialVersionUID = 1L;
	String saveCollection = "saveCollection";//收藏、取消收藏接口
	String getCollection = "getCollection";//查询收藏接口
	String getKnowledgeBase = "getKnowledgeBase";//知识库接口
	String getMemoInfoCalendarList = "getMemoInfoCalendarList";//备忘信息查询接口
	String saveMemoInfo = "saveMemoInfo";////保存备忘信息接口
	String getSign = "getSign";//签到查询接口
	String saveSign = "saveSign";//签到接口
	String login="login";//登陆接口
	String application="application";//缴款申请接口
	String saveReservation="saveReservation";//认购预约提交接口
	String temporaryReservation="temporaryReservation";//认购预约保存接口
	String getReservation="getReservation";//查询保存到后台的认购预约数据接口
	String saveContract="saveContract";//合同申请接口
	String addCustomInfo="addCustomInfo";//添加客户接口
	String queryCustomInfo="queryCustomInfo";//客户列表接口
	String updateCustomInfo="updateCustomInfo";//修改客户接口
	String queryCustomInfoById="queryCustomInfoById";//客户详情接口
	String getReservationDtlList="getReservationDtlList";//预约列表详情接口
	String getPersonCenter="getPersonCenter";//个人中心接口
	String cellReservation="cellReservation";//认购预约撤销接口
	String investorAuthentication="investorAuthentication";//合格投资者认证接口
	String customInfoReister = "customInfoReister";//实名认证接口
	String approve = "approve";//专业投资者认证接口
	String propernace = "propernace";//业绩查询接口
	
	String addCustomInfoMark = "addCustomInfoMark";//新增客户标签
	String updateCustomInfoMark = "updateCustomInfoMark";//删除客户标签
	String queryCustomInfoMark = "queryCustomInfoMark";//查询客户标签
	String getProductList = "getProductList";//查询产品列表
	String updateCustomInfoBefore = "updateCustomInfoBefore";//修改客户信息校验接口
	String getProductProfit = "getProductProfit";//按收益查询接口
	String getProductPermin = "getProductPermin";//按投资期限接口
	String getProductDeadline = "getProductDeadline";//按起投金额查询接口
	String getReservationList = "getReservationList";//认购预约列表接口
	String getBankName = "getBankName";//根据银行卡号查询银行名称
	String getMessage = "getMessage";//消息列表
	String addCustomInfo1 = "addCustomInfo1";//消息列表
	String queryCustomInfo1 = "queryCustomInfo1";//查询实名认证客户列表
	String customInfoReisterOne = "customInfoReisterOne";//再次进行实名认证
	String customInfoReisterHx = "customInfoReisterHx";//回显实名认证信息
	
	

	public CRMAppServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		BufferedReader type = new BufferedReader(
				new InputStreamReader((ServletInputStream) request.getInputStream(), "utf-8"));
		StringBuffer sb = new StringBuffer("");
		String temp;
		while ((temp = type.readLine()) != null) {
			sb.append(temp);
		}
		type.close();
		String params = sb.toString();
		System.out.println(params);

		JSONObject json = JSONObject.fromObject(params);
		String interfaceAPI = json.getString("interfaceAPI");
		//收藏、取消收藏接口
		if (interfaceAPI.equals("saveCollection")) {
			CRM_SaveCollectionController productInfo=new CRM_SaveCollectionController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.saveCollection(request, json));
			return;
			//查询收藏接口
		}else  if(interfaceAPI.equals("getCollection")){
			CRM_CollectionController productInfo=new CRM_CollectionController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getCollection(request, json));
			return;
			//知识库查询接口
		}else  if(interfaceAPI.equals("getKnowledgeBase")){
			CRM_KnowledgeBaseController productInfo=new CRM_KnowledgeBaseController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getKnowledgeBase(request, json));
			return;
			//备忘信息查询接口
		}else  if(interfaceAPI.equals("getMemoInfoCalendarList")){
			CRM_MemoInfoCalendarController productInfo=new CRM_MemoInfoCalendarController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getMemoInfoCalendarList(request, json));
			return;
			//保存备忘信息接口
		}else  if(interfaceAPI.equals("saveMemoInfo")){
			CRM_MemoInfoController productInfo=new CRM_MemoInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.saveMemoInfo(request, json));
			return;
			//签到查询接口
		}else  if(interfaceAPI.equals("getSign")){
			CRM_SignQueryController productInfo=new CRM_SignQueryController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getSign(request, json));
			return;
			//签到接口
		}else  if(interfaceAPI.equals("saveSign")){
			CRM_SignController productInfo=new CRM_SignController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.saveSign(request, json));
			return;
			//登陆接口
		}else if (interfaceAPI.equals("login")) {
			CRM_LoginController login=new CRM_LoginController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			try {
				response.getWriter().print(login.login(request, json));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//缴款申请接口
		}else  if(interfaceAPI.equals("application")){
			CRM_ApplicationController productInfo=new CRM_ApplicationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.application(request, json));
			return;
			//认购预约提交接口
		}else  if(interfaceAPI.equals("saveReservation")){
			CRM_ReservationController productInfo=new CRM_ReservationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.saveReservation(request, json));
			return;
			//认购预约保存接口
		}else  if(interfaceAPI.equals("temporaryReservation")){
			CRM_ReservationController productInfo=new CRM_ReservationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.temporaryReservation(request, json));
			return;
			//查询暂时保存到后台的认购预约数据
		}else  if(interfaceAPI.equals("getReservation")){
			CRM_ReservationController productInfo=new CRM_ReservationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getReservation(request, json));
			return;
			//撤销认购预约接口
		}else  if(interfaceAPI.equals("cellReservation")){
			CRM_ReservationController productInfo=new CRM_ReservationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.cellReservation(request, json));
			return;
			//认购预约列表接口
		}else  if(interfaceAPI.equals("getReservationList")){
			CRM_ReservationController productInfo=new CRM_ReservationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getReservationList(request, json));
			return;
			//认购预约详情接口
		}else  if(interfaceAPI.equals("getReservationDtlList")){
			CRM_ReservationController productInfo=new CRM_ReservationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getReservationDtlList(request, json));
			return;
			//合同申请接口
		}else  if(interfaceAPI.equals("saveContract")){
			CRM_ContractController productInfo=new CRM_ContractController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.saveContract(request, json));
			return;
			//客户信息新增接口
		}else  if(interfaceAPI.equals("addCustomInfo")){
			CRM_CustomInfoController productInfo=new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.addCustomInfo(request, json));
			return;
			//客户列表接口
		}else  if(interfaceAPI.equals("queryCustomInfo")){
			CRM_CustomInfoController productInfo=new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.queryCustomInfo(request, json));
			return;
			//客户信息修改接口
		}else  if(interfaceAPI.equals("updateCustomInfo")){
			CRM_CustomInfoController productInfo=new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.updateCustomInfo(request, json));
			return;
			//客户详情接口
		}else  if(interfaceAPI.equals("queryCustomInfoById")){
			CRM_CustomInfoController productInfo=new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.queryCustomInfoById(request, json));
			return;
			//客户实名认证接口
		}else  if(interfaceAPI.equals("customInfoReister")){
			CRM_CustomInfoController productInfo=new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			//方法调用错误
			response.getWriter().print(productInfo.customInfoReister(request, json));
			return;
			//个人中心接口
		}else  if(interfaceAPI.equals("getPersonCenter")){
			CRM_PersonCenterController productInfo=new CRM_PersonCenterController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getPersonCenter(request, json));
			return;
			//撤销预约接口
		}else  if(interfaceAPI.equals("cellReservation")){
			CRM_ReservationController productInfo=new CRM_ReservationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.cellReservation(request, json));
			return;
			//合格投资者认证接口
		}else  if(interfaceAPI.equals("investorAuthentication")){
			CRM_InvestorAuthenticationController productInfo=new CRM_InvestorAuthenticationController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.investorAuthentication(request, json));
			return;
			//专业投资者认证接口
		}else  if(interfaceAPI.equals("approve")){
			CRM_ProfessionalApproveController approve=new CRM_ProfessionalApproveController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(approve.approve(request, json));
			return;
			//添加客户标签接口
		}else  if(interfaceAPI.equals("addCustomInfoMark")){
			CRM_MarkTiltleController addCustomInfoMark=new CRM_MarkTiltleController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(addCustomInfoMark.addCustomInfoMark(request, json));
			return;
			//客户标签查询接口
		}else  if(interfaceAPI.equals("queryCustomInfoMark")){
			CRM_MarkTiltleController queryCustomInfoMark=new CRM_MarkTiltleController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(queryCustomInfoMark.queryCustomInfoMark(request, json));
			return;
			//客户标签删除接口
		}else  if(interfaceAPI.equals("updateCustomInfoMark")){
			CRM_MarkTiltleController updateCustomInfoMark=new CRM_MarkTiltleController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(updateCustomInfoMark.updateCustomInfoMark(request, json));
			return;
			//产品列表接口
		}else  if(interfaceAPI.equals("getProductList")){
			CRM_ProductController updateCustomInfoMark=new CRM_ProductController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(updateCustomInfoMark.getProductList(request, json));
			return;
			//修改客户信息校验接口
		}else  if(interfaceAPI.equals("updateCustomInfoBefore")){
			CRM_CustomInfoController productInfo=new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.updateCustomInfoBefore(request, json));
			return;
			//按收益查询接口
		}else  if(interfaceAPI.equals("getProductProfit")){
			CRM_ProductController productInfo=new CRM_ProductController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getProductProfit(request, json));
			return;
			//按投资期限接口
		}else  if(interfaceAPI.equals("getProductPermin")){
			CRM_ProductController productInfo=new CRM_ProductController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getProductPermin(request, json));
			return;
			//按起投金额查询接口
		}else  if(interfaceAPI.equals("getProductDeadline")){
			CRM_ProductController productInfo=new CRM_ProductController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getProductDeadline(request, json));
			return;
			//根据银行卡号查询银行名称
		}else  if(interfaceAPI.equals("getBankName")){
			CRM_BankName productInfo=new CRM_BankName();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getBankName(request, json));
			return;
			//消息列表
		}else  if(interfaceAPI.equals("getMessage")){
			CRM_Message productInfo=new CRM_Message();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.getMessage(request, json));
			return;
			//服务分支机构
		}else  if(interfaceAPI.equals("addCustomInfo1")){
			CRM_RegionType productInfo=new CRM_RegionType();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().print(productInfo.addCustomInfo(request, json));
			return;
			//消息列表

			//业绩查询接口
		} else if(interfaceAPI.equals("propernace")){
			CRM_Achievement achievement = new CRM_Achievement();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().println(achievement.getAchievement(request, json));
			return;
			//查询实名认证客户列表
		}else if(interfaceAPI.equals("queryCustomInfo1")){
			CRM_CustomInfoController achievement = new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().println(achievement.queryCustomInfo1(request, json));
			return;
			//再次进行实名认证
		}
		else if(interfaceAPI.equals("customInfoReisterOne")){
			CRM_CustomInfoController achievement = new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().println(achievement.customInfoReisterOne(request, json));
			return;
			//回显实名认证信息
		}
		else if(interfaceAPI.equals("customInfoReisterHx")){
			CRM_CustomInfoController achievement = new CRM_CustomInfoController();
			response.setContentType("utf-8");
			response.setCharacterEncoding("utf-8");
			response.getWriter().println(achievement.customInfoReisterHx(request, json));
			return;
			//再次进行实名认证
		}
	}
}
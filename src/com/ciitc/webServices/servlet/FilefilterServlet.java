package com.ciitc.webServices.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.support.WebApplicationContextUtils;

import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;

import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.RoleDetailService;

@Service
public class FilefilterServlet implements  Filter {
	
	@Autowired(required = false)
    private RoleDetailService<RoleDetailInfo> roleDetailService ;

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	 @Override
	 public void doFilter(ServletRequest req, ServletResponse res,
	            FilterChain chain) throws IOException, ServletException {
			
	        HttpServletRequest request = (HttpServletRequest)req;
	        String url =request.getRequestURL().toString();
	        //System.out.println(url);
	        
	        UserInfo user = ResourceUtil.getSessionUserName();
	        if(user != null){
	        	
	        	//RoleDetailInfoDao<RoleDetailInfo> roleDetailInfoDao = (RoleDetailInfoDao<RoleDetailInfo>) SpringContextUtil.getBean("roleDetailInfoDao");
	        	boolean authority = false;
				List<RoleDetailInfo> roleDetailList = new ArrayList<RoleDetailInfo>();
//				Map<String, Object> paramMap = new HashMap<String, Object>();
//				paramMap.put("USER_NAME", user.getUserName());
//				paramMap.put("MENU_NAME", "交强险保单查询");
				
				 try {
					//根据菜单名和用户名查询有什么权限
					roleDetailList=this.roleDetailService.getDescription(user.getUserName(),"交强险保单查询");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				//权限中是否开通此权限
				 for(RoleDetailInfo roleDetailInfo:roleDetailList){
					 if(roleDetailInfo.getDescription().contains("update1")){
						 authority = true;
					 }
				 }
				//未开通此权限则直接返回
				 if(!authority){
					 PrintWriter out = res.getWriter();
					 StringBuilder builder = new StringBuilder();
						builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
						builder.append("window.top.location.href=\"");
						builder.append(request.getContextPath());
						builder.append("/\";</script>");
						out.print(builder.toString());
						out.flush(); 
						out.close();
				 }else{
					 if(!url.contains("/webpage/pages/login")&&!url.contains("/webpage/pages/main")&&!url.contains("/webpage/pages/ERROR")){
			        		PrintWriter out = res.getWriter();
							StringBuilder builder = new StringBuilder();
							builder.append("<script type=\"text/javascript\" charset=\"UTF-8\">");
							builder.append("window.top.location.href=\"");
							builder.append(request.getContextPath());
							builder.append("/\";</script>");
							out.print(builder.toString());
							out.flush(); 
							out.close();
			        }else{
			        	chain.doFilter(request, res);
			        }
				 }
	        }else{
	        	chain.doFilter(request, res);
	        }
	   }

	@SuppressWarnings("unchecked")
	@Override
	public void init(FilterConfig arg0) throws ServletException {
		ServletContext context = arg0.getServletContext();  
	    ApplicationContext ctx = WebApplicationContextUtils.getWebApplicationContext(context);  
	    roleDetailService = (RoleDetailService<RoleDetailInfo>) ctx.getBean("roleDetailService"); 
		
	}

}

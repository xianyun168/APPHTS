package com.ciitc.webServices.model;

public class SubmitGoodsList {
	//商品编号
	 private String goodsId;
	//商品名称
	 private String goodsName;
	//商品价格
	 private Double price;
	//商品数量
	 private String num;
	 //商品图片地址
	 private String pictureURL;
	public String getGoodsId() {
		return goodsId;
	}
	public void setGoodsId(String goodsId) {
		this.goodsId = goodsId;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getPictureURL() {
		return pictureURL;
	}
	public void setPictureURL(String pictureURL) {
		this.pictureURL = pictureURL;
	}
	 

}

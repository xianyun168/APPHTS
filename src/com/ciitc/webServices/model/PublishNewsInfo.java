package com.ciitc.webServices.model;

import java.util.Date;

public class PublishNewsInfo {
    private String pkId;
    //新闻/轮播图标题
    private String newsTitle;
    //新闻/轮播图图片保存路径
    private String newsImage;
    //新闻类型
    private String newsType;
    //新闻作者
    private String newsAuthor;
    //新闻来源
    private String newsSource;
    //新闻发布时间配置
    private Date newsPublishConfig;
    //是否置顶
    private String newsTop;
    //审核意见
    private String newsAuditOpinion;
    //审核人
    private String newsAuditor;
    //发布状态
    private String publishState;
    //审核状态
    private String auditState;
    //备注1 用于区分是普通新闻还是轮播新闻
    private String remark1;
    //备注2
    private String remark2;
    //备注3
    private String remark3;
    //新闻内容
    private String newsContent;
    //是否轮播
    private String newsCarousel;
    //模糊查询时间段起始时间
    private Date startDate;
    //模糊查询时间段结束时间
    private Date endDate;
    //关联新闻
    private String newsRelated;
    //轮播图发布时间配置
    private Date newsPublishConfiglb;
    //轮播图审核人
    private String newsAuditorlb;
    //轮播图审核状态
    private String auditStatelb;    
    //轮播图发布状态
    private String publishStatelb;
    //关联新闻ID
    private String newsRelationId;
    //系统操作时间
    private Date operateTime;
    //互联网读取图片保存地址
    private String newsImageInternet;
    

	public Date getOperateTime() {
		return operateTime;
	}

	public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	}

	public String getNewsRelationId() {
		return newsRelationId;
	}

	public void setNewsRelationId(String newsRelationId) {
		this.newsRelationId = newsRelationId;
	}

	public Date getNewsPublishConfiglb() {
		return newsPublishConfiglb;
	}

	public void setNewsPublishConfiglb(Date newsPublishConfiglb) {
		this.newsPublishConfiglb = newsPublishConfiglb;
	}

	public String getNewsAuditorlb() {
		return newsAuditorlb;
	}

	public void setNewsAuditorlb(String newsAuditorlb) {
		this.newsAuditorlb = newsAuditorlb;
	}

	public String getAuditStatelb() {
		return auditStatelb;
	}

	public void setAuditStatelb(String auditStatelb) {
		this.auditStatelb = auditStatelb;
	}

	public String getPublishStatelb() {
		return publishStatelb;
	}

	public void setPublishStatelb(String publishStatelb) {
		this.publishStatelb = publishStatelb;
	}  
    public String getNewsRelated() {
		return newsRelated;
	}

	public void setNewsRelated(String newsRelated) {
		this.newsRelated = newsRelated;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getNewsCarousel() {
		return newsCarousel;
	}

	public void setNewsCarousel(String newsCarousel) {
		this.newsCarousel = newsCarousel;
	}

	public int getCounts() {
		return counts;
	}

	public void setCounts(int counts) {
		this.counts = counts;
	}

	private int counts;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getNewsTitle() {
        return newsTitle;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle == null ? null : newsTitle.trim();
    }

    public String getNewsImage() {
        return newsImage;
    }

    public void setNewsImage(String newsImage) {
        this.newsImage = newsImage == null ? null : newsImage.trim();
    }

    public String getNewsType() {
        return newsType;
    }

    public void setNewsType(String newsType) {
        this.newsType = newsType == null ? null : newsType.trim();
    }

    public String getNewsAuthor() {
        return newsAuthor;
    }

    public void setNewsAuthor(String newsAuthor) {
        this.newsAuthor = newsAuthor == null ? null : newsAuthor.trim();
    }

    public String getNewsSource() {
        return newsSource;
    }

    public void setNewsSource(String newsSource) {
        this.newsSource = newsSource == null ? null : newsSource.trim();
    }

    public Date getNewsPublishConfig() {
        return newsPublishConfig;
    }

    public void setNewsPublishConfig(Date newsPublishConfig) {
        this.newsPublishConfig = newsPublishConfig;
    }

    public String getNewsTop() {
        return newsTop;
    }

    public void setNewsTop(String newsTop) {
        this.newsTop = newsTop == null ? null : newsTop.trim();
    }

    public String getNewsAuditOpinion() {
        return newsAuditOpinion;
    }

    public void setNewsAuditOpinion(String newsAuditOpinion) {
        this.newsAuditOpinion = newsAuditOpinion == null ? null : newsAuditOpinion.trim();
    }

    public String getNewsAuditor() {
        return newsAuditor;
    }

    public void setNewsAuditor(String newsAuditor) {
        this.newsAuditor = newsAuditor == null ? null : newsAuditor.trim();
    }

    public String getPublishState() {
        return publishState;
    }

    public void setPublishState(String publishState) {
        this.publishState = publishState == null ? null : publishState.trim();
    }

    public String getAuditState() {
        return auditState;
    }

    public void setAuditState(String auditState) {
        this.auditState = auditState == null ? null : auditState.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    public String getNewsContent() {
        return newsContent;
    }

    public void setNewsContent(String newsContent) {
        this.newsContent = newsContent;
    }

	public String getNewsImageInternet() {
		return newsImageInternet;
	}

	public void setNewsImageInternet(String newsImageInternet) {
		this.newsImageInternet = newsImageInternet;
	}
}
package com.ciitc.webServices.model;

import java.math.BigDecimal;
import java.util.Date;

public class CPTpaymentinfoReq {
    private Long lSerialno;

    private Long lProserialno;

    private Long lBenserialno;

    private String cPaytype;

    private String cMoneytype;

    private BigDecimal fPaybalance;

    private Date dPaydate;

    private String cMemo;

    private String cCreator;

    private Date dCreatedate;

    private String cStatus;

    private BigDecimal fCommissioncharge;

    private String cPayer;

    private String cReserveno;

    private String cPaymentagent;

    private Short lPaidcount;

    private String cFrombankno;

    private String cFrombankname;

    private String cFromnameinbank;

    private String cFrombankacco;

    private String cAccountnumber;

    private String cAccoid;

    private String cPropertytype;

    private String cPropertyno;

    private String cPropertyname;

    private BigDecimal cPropertynum;

    private String cPropertyunit;

    private BigDecimal fPropertynum;

    private String cPropertymemo;

    public Long getlSerialno() {
        return lSerialno;
    }

    public void setlSerialno(Long lSerialno) {
        this.lSerialno = lSerialno;
    }

    public Long getlProserialno() {
        return lProserialno;
    }

    public void setlProserialno(Long lProserialno) {
        this.lProserialno = lProserialno;
    }

    public Long getlBenserialno() {
        return lBenserialno;
    }

    public void setlBenserialno(Long lBenserialno) {
        this.lBenserialno = lBenserialno;
    }

    public String getcPaytype() {
        return cPaytype;
    }

    public void setcPaytype(String cPaytype) {
        this.cPaytype = cPaytype == null ? null : cPaytype.trim();
    }

    public String getcMoneytype() {
        return cMoneytype;
    }

    public void setcMoneytype(String cMoneytype) {
        this.cMoneytype = cMoneytype == null ? null : cMoneytype.trim();
    }

    public BigDecimal getfPaybalance() {
        return fPaybalance;
    }

    public void setfPaybalance(BigDecimal fPaybalance) {
        this.fPaybalance = fPaybalance;
    }

    public Date getdPaydate() {
        return dPaydate;
    }

    public void setdPaydate(Date dPaydate) {
        this.dPaydate = dPaydate;
    }

    public String getcMemo() {
        return cMemo;
    }

    public void setcMemo(String cMemo) {
        this.cMemo = cMemo == null ? null : cMemo.trim();
    }

    public String getcCreator() {
        return cCreator;
    }

    public void setcCreator(String cCreator) {
        this.cCreator = cCreator == null ? null : cCreator.trim();
    }

    public Date getdCreatedate() {
        return dCreatedate;
    }

    public void setdCreatedate(Date dCreatedate) {
        this.dCreatedate = dCreatedate;
    }

    public String getcStatus() {
        return cStatus;
    }

    public void setcStatus(String cStatus) {
        this.cStatus = cStatus == null ? null : cStatus.trim();
    }

    public BigDecimal getfCommissioncharge() {
        return fCommissioncharge;
    }

    public void setfCommissioncharge(BigDecimal fCommissioncharge) {
        this.fCommissioncharge = fCommissioncharge;
    }

    public String getcPayer() {
        return cPayer;
    }

    public void setcPayer(String cPayer) {
        this.cPayer = cPayer == null ? null : cPayer.trim();
    }

    public String getcReserveno() {
        return cReserveno;
    }

    public void setcReserveno(String cReserveno) {
        this.cReserveno = cReserveno == null ? null : cReserveno.trim();
    }

    public String getcPaymentagent() {
        return cPaymentagent;
    }

    public void setcPaymentagent(String cPaymentagent) {
        this.cPaymentagent = cPaymentagent == null ? null : cPaymentagent.trim();
    }

    public Short getlPaidcount() {
        return lPaidcount;
    }

    public void setlPaidcount(Short lPaidcount) {
        this.lPaidcount = lPaidcount;
    }

    public String getcFrombankno() {
        return cFrombankno;
    }

    public void setcFrombankno(String cFrombankno) {
        this.cFrombankno = cFrombankno == null ? null : cFrombankno.trim();
    }

    public String getcFrombankname() {
        return cFrombankname;
    }

    public void setcFrombankname(String cFrombankname) {
        this.cFrombankname = cFrombankname == null ? null : cFrombankname.trim();
    }

    public String getcFromnameinbank() {
        return cFromnameinbank;
    }

    public void setcFromnameinbank(String cFromnameinbank) {
        this.cFromnameinbank = cFromnameinbank == null ? null : cFromnameinbank.trim();
    }

    public String getcFrombankacco() {
        return cFrombankacco;
    }

    public void setcFrombankacco(String cFrombankacco) {
        this.cFrombankacco = cFrombankacco == null ? null : cFrombankacco.trim();
    }

    public String getcAccountnumber() {
        return cAccountnumber;
    }

    public void setcAccountnumber(String cAccountnumber) {
        this.cAccountnumber = cAccountnumber == null ? null : cAccountnumber.trim();
    }

    public String getcAccoid() {
        return cAccoid;
    }

    public void setcAccoid(String cAccoid) {
        this.cAccoid = cAccoid == null ? null : cAccoid.trim();
    }

    public String getcPropertytype() {
        return cPropertytype;
    }

    public void setcPropertytype(String cPropertytype) {
        this.cPropertytype = cPropertytype == null ? null : cPropertytype.trim();
    }

    public String getcPropertyno() {
        return cPropertyno;
    }

    public void setcPropertyno(String cPropertyno) {
        this.cPropertyno = cPropertyno == null ? null : cPropertyno.trim();
    }

    public String getcPropertyname() {
        return cPropertyname;
    }

    public void setcPropertyname(String cPropertyname) {
        this.cPropertyname = cPropertyname == null ? null : cPropertyname.trim();
    }

    public BigDecimal getcPropertynum() {
        return cPropertynum;
    }

    public void setcPropertynum(BigDecimal cPropertynum) {
        this.cPropertynum = cPropertynum;
    }

    public String getcPropertyunit() {
        return cPropertyunit;
    }

    public void setcPropertyunit(String cPropertyunit) {
        this.cPropertyunit = cPropertyunit == null ? null : cPropertyunit.trim();
    }

    public BigDecimal getfPropertynum() {
        return fPropertynum;
    }

    public void setfPropertynum(BigDecimal fPropertynum) {
        this.fPropertynum = fPropertynum;
    }

    public String getcPropertymemo() {
        return cPropertymemo;
    }

    public void setcPropertymemo(String cPropertymemo) {
        this.cPropertymemo = cPropertymemo == null ? null : cPropertymemo.trim();
    }
}
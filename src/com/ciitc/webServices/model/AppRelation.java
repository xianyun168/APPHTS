package com.ciitc.webServices.model;

public class AppRelation {
    private String loginid;

    private String upLoginid;

    private String superFlg;

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid == null ? null : loginid.trim();
    }

    public String getUpLoginid() {
        return upLoginid;
    }

    public void setUpLoginid(String upLoginid) {
        this.upLoginid = upLoginid == null ? null : upLoginid.trim();
    }

    public String getSuperFlg() {
        return superFlg;
    }

    public void setSuperFlg(String superFlg) {
        this.superFlg = superFlg == null ? null : superFlg.trim();
    }
}
package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 收益表
 * @author 任之阳
 *
 */
public class Profit {
     //主键
	private String pkId;
	
	 //产品Id
	private String productId;
	
	 //用户Id
	private String userId;
	
	 //是否到期
	private String productStatus;
	
	 //产品本期收益
	private double productProfit;
	
	 //产品开始时间
	private Date startProduct;
	
	 //产品结束时间
	private Date endProduct;
	
	 //确认份额
	private String confirmShare;
	
	 //份额确认日
	private Date confirmDate;
	
	 //投资金额
	private double investmentMoney;
	
	//交易记录状态
	private String transactionStatus;
	
	//本期收益时间
	private Date profitDate;
	
	//产品名称
    private String productName;
    
    //产品类型
    private String pruductType;

    //业绩比较基准
    private String benchmark;
    
    //确认付息
    private String confirmInterest;
    
    //收益确认
    private String profitConfirm;

	//备用字段1
	private String remark1;
	
	//备用字段2
	private String remark2;
	
	//备用字段3
	private String remark3;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getProductStatus() {
		return productStatus;
	}

	public void setProductStatus(String productStatus) {
		this.productStatus = productStatus;
	}

	public double getProductProfit() {
		return productProfit;
	}

	public void setProductProfit(double productProfit) {
		this.productProfit = productProfit;
	}

	public Date getStartProduct() {
		return startProduct;
	}

	public void setStartProduct(Date startProduct) {
		this.startProduct = startProduct;
	}

	public Date getEndProduct() {
		return endProduct;
	}

	public void setEndProduct(Date endProduct) {
		this.endProduct = endProduct;
	}

	public String getConfirmShare() {
		return confirmShare;
	}

	public void setConfirmShare(String confirmShare) {
		this.confirmShare = confirmShare;
	}

	public Date getConfirmDate() {
		return confirmDate;
	}

	public void setConfirmDate(Date confirmDate) {
		this.confirmDate = confirmDate;
	}

	public double getInvestmentMoney() {
		return investmentMoney;
	}

	public void setInvestmentMoney(double investmentMoney) {
		this.investmentMoney = investmentMoney;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
	public Date getProfitDate() {
		return profitDate;
	}

	public void setProfitDate(Date profitDate) {
		this.profitDate = profitDate;
	}
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getPruductType() {
		return pruductType;
	}

	public void setPruductType(String pruductType) {
		this.pruductType = pruductType;
	}

	public String getBenchmark() {
		return benchmark;
	}

	public void setBenchmark(String benchmark) {
		this.benchmark = benchmark;
	}
	public String getConfirmInterest() {
		return confirmInterest;
	}

	public void setConfirmInterest(String confirmInterest) {
		this.confirmInterest = confirmInterest;
	}

	public String getProfitConfirm() {
		return profitConfirm;
	}

	public void setProfitConfirm(String profitConfirm) {
		this.profitConfirm = profitConfirm;
	}
}

package com.ciitc.webServices.model;

import java.util.Map;
import java.util.Set;

public class JpushUntilVo {
	private  String appKey;
	 
	private  String masterSecret;
	 
    private  Integer maxtrytimes;
    
    private  Set<String> alias;//使用户ID
    
    private Set<String> tag;//部门ID
    
    private AppMessageSend messageVo;
    
	public Set<String> getAlias() {
		return alias;
	}

	public void setAlias(Set<String> alias) {
		this.alias = alias;
	}

	public Set<String> getTag() {
		return tag;
	}

	public void setTag(Set<String> tag) {
		this.tag = tag;
	}

	public AppMessageSend getMessageVo() {
		return messageVo;
	}

	public void setMessageVo(AppMessageSend messageVo) {
		this.messageVo = messageVo;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getMasterSecret() {
		return masterSecret;
	}

	public void setMasterSecret(String masterSecret) {
		this.masterSecret = masterSecret;
	}

	public Integer getMaxtrytimes() {
		return maxtrytimes;
	}

	public void setMaxtrytimes(Integer maxtrytimes) {
		this.maxtrytimes = maxtrytimes;
	}

	
    
    
}

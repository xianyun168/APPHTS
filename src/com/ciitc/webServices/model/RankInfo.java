package com.ciitc.webServices.model;

import java.util.Date;

/**
 * 职级
 * @author 任之阳
 *
 */
public class RankInfo {

	  //主键
	private String pkId;
	
	 //职级
	private String rank;
	
	 //备用字段1
	private String remark1;
		
	 //备用字段2 
	private String remark2;
		
	 //备用字段3
	private String remark3;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
}

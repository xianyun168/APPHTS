package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 收藏表
 * @author tdl
 *
 */
public class Collection {
	//主键
    private String pkId;
    //收藏物PK_ID(客户ID、产品ID、知识库ID)
    private String collectionId;
    //标题(客户收藏的客户名、产品收藏的产品名称、知识库收藏的知识标题)
    private String title;
    //所属板块（00客户收藏、01产品收藏、02知识库收藏）
    private String plate;
    //理财师PK_ID
    private String financialId;
    //收藏时间
    private Date collectionTime;
    //标识符（00收藏、01取消收藏）
    private String flag;
    //关键字
    private String keyWord;
    //备用字段1
    private String remark1;

    private String remark2;

    private String remark3;

    
    public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId == null ? null : collectionId.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate == null ? null : plate.trim();
    }

    public String getFinancialId() {
        return financialId;
    }

    public void setFinancialId(String financialId) {
        this.financialId = financialId == null ? null : financialId.trim();
    }

    public Date getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(Date collectionTime) {
        this.collectionTime = collectionTime;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
package com.ciitc.webServices.model;

import java.util.Date;

public class EquitiesUseDetails {
	private String pkId;
	// 客户编号
    private String customerId;
	// 就医时间
    private Date medicalTime;
	// 国康子产品编码
    private String equitiesId;
	// 付费类型
    private String payType;
	// 就诊医院
    private String hospital;
	// 科室
    private String department;
	// 就医原因
    private String reason;
	// 权益名称
    private String equitiesName;

    private String remark1;

    private String remark2;

    private String remark3;
	// 姓名
    private String userName;
	// 使用状态
    private String useState;
	// 证件号码
    private String idCard;
	// 国康服务记录id
    private String serviceRecordId;
    //创建时间
    private Date createTime;
	// 国康父产品编码
    private String equitiesPid;
	// 国康父产品名字
    private String equitiesPname;
	// 电话
	private String telephone;

    
	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getEquitiesId() {
		return equitiesId;
	}

	public void setEquitiesId(String equitiesId) {
		this.equitiesId = equitiesId;
	}

	public String getEquitiesName() {
		return equitiesName;
	}

	public void setEquitiesName(String equitiesName) {
		this.equitiesName = equitiesName;
	}

	public String getEquitiesPid() {
		return equitiesPid;
	}

	public void setEquitiesPid(String equitiesPid) {
		this.equitiesPid = equitiesPid;
	}

	public String getEquitiesPname() {
		return equitiesPname;
	}

	public void setEquitiesPname(String equitiesPname) {
		this.equitiesPname = equitiesPname;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId == null ? null : customerId.trim();
    }

    public Date getMedicalTime() {
        return medicalTime;
    }

    public void setMedicalTime(Date medicalTime) {
        this.medicalTime = medicalTime;
    }


    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital == null ? null : hospital.trim();
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department == null ? null : department.trim();
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUseState() {
        return useState;
    }

    public void setUseState(String useState) {
        this.useState = useState == null ? null : useState.trim();
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard == null ? null : idCard.trim();
    }

    public String getServiceRecordId() {
        return serviceRecordId;
    }

    public void setServiceRecordId(String serviceRecordId) {
        this.serviceRecordId = serviceRecordId == null ? null : serviceRecordId.trim();
    }
}
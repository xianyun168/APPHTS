package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 合同申请表
 * @author tdl
 *
 */
public class ContractApplication {
    //主键
	private String pkId;
	//预约编号
    private String orderId;
    //缴款编号
    private String paymentId;
    //合同申请时间
    private Date contractApplicationTime;
    //合同申请状态(00 预约、01 待审核、02 通过、03 驳回)
    private String contractApplicationState;
    //标识符(00 申请、01 撤销申请)
    private String flag;
    //备用字段1
    private String remark1;

    private String remark2;

    private String remark3;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId == null ? null : paymentId.trim();
    }

    public Date getContractApplicationTime() {
        return contractApplicationTime;
    }

    public void setContractApplicationTime(Date contractApplicationTime) {
        this.contractApplicationTime = contractApplicationTime;
    }

    public String getContractApplicationState() {
        return contractApplicationState;
    }

    public void setContractApplicationState(String contractApplicationState) {
        this.contractApplicationState = contractApplicationState == null ? null : contractApplicationState.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
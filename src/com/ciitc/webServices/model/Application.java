package com.ciitc.webServices.model;

public class Application {
	//预约编号C_RESERVENO_IN，付款时间PAYDATE，付款金额PAYAMT，
	//银行名称BANKNAME（根据名称查询编码BANKNO），
	//付款账号ACCTNO，付款账号名ACCTNM，支行名称BANKBRUNNM（可填可不填）
	public String reserveno;
	public String paydate;
	public Integer payamt;
	public String acctno;
	public String acctnm;
	public String bankbrunnm;
	public String bankName;
	public String bankNo;
	public String getReserveno() {
		return reserveno;
	}
	public void setReserveno(String reserveno) {
		this.reserveno = reserveno;
	}
	public String getPaydate() {
		return paydate;
	}
	public void setPaydate(String paydate) {
		this.paydate = paydate;
	}
	public Integer getPayamt() {
		return payamt;
	}
	public void setPayamt(Integer payamt) {
		this.payamt = payamt;
	}
	public String getAcctno() {
		return acctno;
	}
	public void setAcctno(String acctno) {
		this.acctno = acctno;
	}
	public String getAcctnm() {
		return acctnm;
	}
	public void setAcctnm(String acctnm) {
		this.acctnm = acctnm;
	}
	public String getBankbrunnm() {
		return bankbrunnm;
	}
	public void setBankbrunnm(String bankbrunnm) {
		this.bankbrunnm = bankbrunnm;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getBankNo() {
		return bankNo;
	}
	public void setBankNo(String bankNo) {
		this.bankNo = bankNo;
	}
	
	

}

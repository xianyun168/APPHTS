package com.ciitc.webServices.model;

import java.io.Serializable;
import java.util.Date;

public class UserInfo implements Serializable  {

	//主键
    private  String pkId;
    //部门主键
    private String departPkId;
    //用户名名
    private  String userName;
    //用户名代码
    private String userCode;
    //密码
    private  String password;
    //省份代码
    private String areaCode;
    //EMAIL地址
    private  String email;
    //电话
    private String telephone;
    //用户口令（userCode+createdDate）的md5
    private String token;
    //登录状态
    private  String status;
    //上次登录时间
    private  Date lastloginDate;
    //失败次数
    private  Integer failCount;
    //LOCKTIME
    private  Date lockTime;
    //创建人
    private  String createdBy;
    //创建时间
    private  Date createdDate;
    //修改人
    private  String updatedBy;
    //修改时间
    private  Date updatedDate;
    //标志位
    private  String flag;
    //备注
    private  String remark;

	 public String getPkId() {
		return this.pkId;
	 }
	
	 public void setPkId(String pkId) {
		this.pkId = pkId;
	 }
	 
	 public String getDepartPkId() {
		return departPkId;
	}

	public void setDepartPkId(String departPkId) {
		this.departPkId = departPkId;
	}

	public String getUserName() {
		return this.userName;
	 }
	
	 public void setUserName(String userName) {
		this.userName = userName;
	 }
	 
	 public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getPassword() {
		return this.password;
	 }
	
	 public void setPassword(String password) {
		this.password = password;
	 }
	 
	 public String getEmail() {
		return this.email;
	 }
	
	 public void setEmail(String email) {
		this.email = email;
	 }
	 
	 public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getStatus() {
		return this.status;
	 }
	
	 public void setStatus(String status) {
		this.status = status;
	 }
	 
	 public Date getLastloginDate() {
		return this.lastloginDate;
	 }
	
	 public void setLastloginDate(Date lastloginDate) {
		this.lastloginDate = lastloginDate;
	 }
	 
	 public Integer getFailCount() {
		return this.failCount;
	 }
	
	 public void setFailCount(Integer failCount) {
		this.failCount = failCount;
	 }
	 
	 public Date getLockTime() {
		return this.lockTime;
	 }
	
	 public void setLockTime(Date lockTime) {
		this.lockTime = lockTime;
	 }
	 
	 public String getCreatedBy() {
		return this.createdBy;
	 }
	
	 public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	 }
	 
	 public Date getCreatedDate() {
		return this.createdDate;
	 }
	
	 public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	 }
	 
	 public String getUpdatedBy() {
		return this.updatedBy;
	 }
	
	 public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	 }
	 
	 public Date getUpdatedDate() {
		return this.updatedDate;
	 }
	
	 public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	 }
	 
	 public String getFlag() {
		return this.flag;
	 }
	
	 public void setFlag(String flag) {
		this.flag = flag;
	 }
	 
	 public String getRemark() {
		return this.remark;
	 }
	
	 public void setRemark(String remark) {
		this.remark = remark;
	 }

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	 
 }
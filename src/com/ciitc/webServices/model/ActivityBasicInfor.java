package com.ciitc.webServices.model;

import java.util.Date;

public class ActivityBasicInfor {
    private String pkId;

    private String activityName;

    private Date activityStartDate;
    
    private Date activityEndDate;

    private String activityAttendCondition;

    private String activityPlace;

    private String activityContent;

    private String activityState;
    
    private String pictureURL;

    private Date activitySignStart;
    
    private Date activitySignEnd;

    private String remark1;

    private String remark2;

    private String remark3;
    
    private String remark4;
    
    private String customerLevel;
    
    private String URL;
    
    public String getURL() {
		return URL;
	}

	public void setURL(String uRL) {
		URL = uRL;
	}

	public String getCustomerLevel() {
		return customerLevel;
	}

	public void setCustomerLevel(String customerLevel) {
		this.customerLevel = customerLevel;
	}

	public String getRemark4() {
		return remark4;
	}

	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getActivityName() {
        return activityName;
    }

    public String getPictureURL() {
		return pictureURL;
	}

	public void setPictureURL(String pictureURL) {
		this.pictureURL = pictureURL;
	}

	public void setActivityName(String activityName) {
        this.activityName = activityName == null ? null : activityName.trim();
    }


    public Date getActivityStartDate() {
		return activityStartDate;
	}

	public void setActivityStartDate(Date activityStartDate) {
		this.activityStartDate = activityStartDate;
	}

	public Date getActivityEndDate() {
		return activityEndDate;
	}

	public void setActivityEndDate(Date activityEndDate) {
		this.activityEndDate = activityEndDate;
	}

	public String getActivityAttendCondition() {
        return activityAttendCondition;
    }

    public void setActivityAttendCondition(String activityAttendCondition) {
        this.activityAttendCondition = activityAttendCondition == null ? null : activityAttendCondition.trim();
    }

    public String getActivityPlace() {
        return activityPlace;
    }

    public void setActivityPlace(String activityPlace) {
        this.activityPlace = activityPlace == null ? null : activityPlace.trim();
    }

    public String getActivityContent() {
        return activityContent;
    }

    public void setActivityContent(String activityContent) {
        this.activityContent = activityContent == null ? null : activityContent.trim();
    }

    public String getActivityState() {
        return activityState;
    }

    public void setActivityState(String activityState) {
        this.activityState = activityState == null ? null : activityState.trim();
    }


    public Date getActivitySignStart() {
		return activitySignStart;
	}

	public void setActivitySignStart(Date activitySignStart) {
		this.activitySignStart = activitySignStart;
	}

	public Date getActivitySignEnd() {
		return activitySignEnd;
	}

	public void setActivitySignEnd(Date activitySignEnd) {
		this.activitySignEnd = activitySignEnd;
	}

	public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
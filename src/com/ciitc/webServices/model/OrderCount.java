package com.ciitc.webServices.model;

import java.util.ArrayList;
import java.util.List;

public class OrderCount {
	//用户id
	private String userId;
	//收货人姓名
    private String shopName;
    //收货人电话
    private String shopPhone;
    //收货人地址
    private String address;
    //兑换的商品列表
    List<SubmitGoodsList> goddsList = new ArrayList<SubmitGoodsList>();
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getShopName() {
		return shopName;
	}
	public void setShopName(String shopName) {
		this.shopName = shopName;
	}
	public String getShopPhone() {
		return shopPhone;
	}
	public void setShopPhone(String shopPhone) {
		this.shopPhone = shopPhone;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public List<SubmitGoodsList> getGoddsList() {
		return goddsList;
	}
	public void setGoddsList(List<SubmitGoodsList> goddsList) {
		this.goddsList = goddsList;
	}
    
    

}

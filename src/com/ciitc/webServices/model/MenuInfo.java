package com.ciitc.webServices.model;

import java.util.Date;

public class MenuInfo  {

    //主键
    private  String pkId;
    //上级菜单ID
    private  String parentMenuId;
    //菜单名称
    private  String menuName;
    //对应菜单级别
    private  String menuGrade;
    //菜单排序
    private  String menuOrder;
    //菜单URL
    private  String menuPath;
    //创建人
    private  String createdBy;
    //创建时间
    private  Date createdDate;
    //修改人
    private  String updatedBy;
    //修改时间
    private  Date updatedDate;
    //标志位
    private  String flag;
    //备注
    private  String remark;

	 public String getPkId() {
		return this.pkId;
	 }
	
	 public void setPkId(String pkId) {
		this.pkId = pkId;
	 }
	 
	 
	 public String getParentMenuId() {
		return parentMenuId;
	}

	public void setParentMenuId(String parentMenuId) {
		this.parentMenuId = parentMenuId;
	}

	public String getMenuName() {
		return this.menuName;
	 }
	
	 public void setMenuName(String menuName) {
		this.menuName = menuName;
	 }
	 
	 public String getMenuGrade() {
		return this.menuGrade;
	 }
	
	 public void setMenuGrade(String menuGrade) {
		this.menuGrade = menuGrade;
	 }
	 
	 public String getMenuOrder() {
		return this.menuOrder;
	 }
	
	 public void setMenuOrder(String menuOrder) {
		this.menuOrder = menuOrder;
	 }
	 
	 public String getMenuPath() {
		return this.menuPath;
	 }
	
	 public void setMenuPath(String menuPath) {
		this.menuPath = menuPath;
	 }
	 
	 public String getCreatedBy() {
		return this.createdBy;
	 }
	
	 public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	 }
	 
	 public Date getCreatedDate() {
		return this.createdDate;
	 }
	
	 public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	 }
	 
	 public String getUpdatedBy() {
		return this.updatedBy;
	 }
	
	 public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	 }
	 
	 public Date getUpdatedDate() {
		return this.updatedDate;
	 }
	
	 public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	 }
	 
	 public String getFlag() {
		return this.flag;
	 }
	
	 public void setFlag(String flag) {
		this.flag = flag;
	 }
	 
	 public String getRemark() {
		return this.remark;
	 }
	
	 public void setRemark(String remark) {
		this.remark = remark;
	 }
	 
 }
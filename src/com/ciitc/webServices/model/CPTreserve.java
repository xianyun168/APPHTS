package com.ciitc.webServices.model;

import java.math.BigDecimal;
import java.util.Date;

public class CPTreserve {
    private String cReserveno;

    private String cFundacco;

    private String cCustno;

    private String cFundcode;

    private Date dReservedate;

    private String cBrokeraccount;

    private String cStatus;

    private String cMemo;

    private String cBusinflag;

    private BigDecimal fBalance;

    private BigDecimal fShares;

    private Long lContractserialno;

    private String cCreator;

    private String cProfitclass;

    private Short lBookingcount;

    private Date dRemitdate;

    private Date dExpirationdate;

    private String cPhone;

    private String cMobileno;

    private String cEmail;

    private String cZipcode;

    private String cAddress;

    private Date dDealdate;

    private Long lProserialno;

    private String cIscomity;

    private String cComityno;

    private String cProcustfrom;

    private String cNameinbank;

    private String cBankname;

    private String cBankacco;

    private String cFaxno;

    private String cContact;

    private String cReservesource;

    private String cDealer;

    private String cCustfrom;

    private String cPaystatus;

    private BigDecimal fContractbalance;

    private BigDecimal fTotalpaybalance;

    private String cBrokerrecommend;

    private String cSalesource;

    private String cChannelno;

    private String cQuotaflag;

    private String cReserveorderno;

    private Integer lInvestserialno;

    private BigDecimal fPersonbalance;

    private BigDecimal fDeptbalance;

    private Long lProfitid;

    private String cDeptnodecode;

    private String cTrustcontractid;

    private String cProductnum;

    private String fContractblance;

    private String cNodecode;

    private Date dExpectedvalueday;

    private String cReceiveassert;

    private Date dCreatedate;

    private Date dModifydate;

    private String cBuytype;

    private BigDecimal fTempTotalbalance;

    private BigDecimal fTempTotalbalance1;

    private String cTempDeptnodecode;

    private Date dReservationsuccessdate;

    private String cCapitalstatus;

    private String cCapitalbals;

    private String cIsproinvestor;

    private BigDecimal fSubscriptionratio;

    private String cIsprintreceipt;

    private String cSetstatus;

    private String cInterviewstatus;

    private Date dOptime;

    private BigDecimal fCommisionconvert;

    private String cTrustagencyno;

    private String cChannelsource;

    private String cIsoldcustomer;

    private String cVisitstatus;

    private String cVisitmemo;

    private Long lVisitcount;

    private String cSalebrokeraccount;

    private String cSubbranchname;

    private Date dContractsigndate;

    private String cDocfillstatus;

    private String cReasoncode;

    private Date dFundopendate;

    private String cWealthsource;

    private Date dDocfilldate;

    private BigDecimal lMorecontactno;

    private String cReasonmsg;

    private Date dDeldate;

    public String getcReserveno() {
        return cReserveno;
    }

    public void setcReserveno(String cReserveno) {
        this.cReserveno = cReserveno == null ? null : cReserveno.trim();
    }

    public String getcFundacco() {
        return cFundacco;
    }

    public void setcFundacco(String cFundacco) {
        this.cFundacco = cFundacco == null ? null : cFundacco.trim();
    }

    public String getcCustno() {
        return cCustno;
    }

    public void setcCustno(String cCustno) {
        this.cCustno = cCustno == null ? null : cCustno.trim();
    }

    public String getcFundcode() {
        return cFundcode;
    }

    public void setcFundcode(String cFundcode) {
        this.cFundcode = cFundcode == null ? null : cFundcode.trim();
    }

    public Date getdReservedate() {
        return dReservedate;
    }

    public void setdReservedate(Date dReservedate) {
        this.dReservedate = dReservedate;
    }

    public String getcBrokeraccount() {
        return cBrokeraccount;
    }

    public void setcBrokeraccount(String cBrokeraccount) {
        this.cBrokeraccount = cBrokeraccount == null ? null : cBrokeraccount.trim();
    }

    public String getcStatus() {
        return cStatus;
    }

    public void setcStatus(String cStatus) {
        this.cStatus = cStatus == null ? null : cStatus.trim();
    }

    public String getcMemo() {
        return cMemo;
    }

    public void setcMemo(String cMemo) {
        this.cMemo = cMemo == null ? null : cMemo.trim();
    }

    public String getcBusinflag() {
        return cBusinflag;
    }

    public void setcBusinflag(String cBusinflag) {
        this.cBusinflag = cBusinflag == null ? null : cBusinflag.trim();
    }

    public BigDecimal getfBalance() {
        return fBalance;
    }

    public void setfBalance(BigDecimal fBalance) {
        this.fBalance = fBalance;
    }

    public BigDecimal getfShares() {
        return fShares;
    }

    public void setfShares(BigDecimal fShares) {
        this.fShares = fShares;
    }

    public Long getlContractserialno() {
        return lContractserialno;
    }

    public void setlContractserialno(Long lContractserialno) {
        this.lContractserialno = lContractserialno;
    }

    public String getcCreator() {
        return cCreator;
    }

    public void setcCreator(String cCreator) {
        this.cCreator = cCreator == null ? null : cCreator.trim();
    }

    public String getcProfitclass() {
        return cProfitclass;
    }

    public void setcProfitclass(String cProfitclass) {
        this.cProfitclass = cProfitclass == null ? null : cProfitclass.trim();
    }

    public Short getlBookingcount() {
        return lBookingcount;
    }

    public void setlBookingcount(Short lBookingcount) {
        this.lBookingcount = lBookingcount;
    }

    public Date getdRemitdate() {
        return dRemitdate;
    }

    public void setdRemitdate(Date dRemitdate) {
        this.dRemitdate = dRemitdate;
    }

    public Date getdExpirationdate() {
        return dExpirationdate;
    }

    public void setdExpirationdate(Date dExpirationdate) {
        this.dExpirationdate = dExpirationdate;
    }

    public String getcPhone() {
        return cPhone;
    }

    public void setcPhone(String cPhone) {
        this.cPhone = cPhone == null ? null : cPhone.trim();
    }

    public String getcMobileno() {
        return cMobileno;
    }

    public void setcMobileno(String cMobileno) {
        this.cMobileno = cMobileno == null ? null : cMobileno.trim();
    }

    public String getcEmail() {
        return cEmail;
    }

    public void setcEmail(String cEmail) {
        this.cEmail = cEmail == null ? null : cEmail.trim();
    }

    public String getcZipcode() {
        return cZipcode;
    }

    public void setcZipcode(String cZipcode) {
        this.cZipcode = cZipcode == null ? null : cZipcode.trim();
    }

    public String getcAddress() {
        return cAddress;
    }

    public void setcAddress(String cAddress) {
        this.cAddress = cAddress == null ? null : cAddress.trim();
    }

    public Date getdDealdate() {
        return dDealdate;
    }

    public void setdDealdate(Date dDealdate) {
        this.dDealdate = dDealdate;
    }

    public Long getlProserialno() {
        return lProserialno;
    }

    public void setlProserialno(Long lProserialno) {
        this.lProserialno = lProserialno;
    }

    public String getcIscomity() {
        return cIscomity;
    }

    public void setcIscomity(String cIscomity) {
        this.cIscomity = cIscomity == null ? null : cIscomity.trim();
    }

    public String getcComityno() {
        return cComityno;
    }

    public void setcComityno(String cComityno) {
        this.cComityno = cComityno == null ? null : cComityno.trim();
    }

    public String getcProcustfrom() {
        return cProcustfrom;
    }

    public void setcProcustfrom(String cProcustfrom) {
        this.cProcustfrom = cProcustfrom == null ? null : cProcustfrom.trim();
    }

    public String getcNameinbank() {
        return cNameinbank;
    }

    public void setcNameinbank(String cNameinbank) {
        this.cNameinbank = cNameinbank == null ? null : cNameinbank.trim();
    }

    public String getcBankname() {
        return cBankname;
    }

    public void setcBankname(String cBankname) {
        this.cBankname = cBankname == null ? null : cBankname.trim();
    }

    public String getcBankacco() {
        return cBankacco;
    }

    public void setcBankacco(String cBankacco) {
        this.cBankacco = cBankacco == null ? null : cBankacco.trim();
    }

    public String getcFaxno() {
        return cFaxno;
    }

    public void setcFaxno(String cFaxno) {
        this.cFaxno = cFaxno == null ? null : cFaxno.trim();
    }

    public String getcContact() {
        return cContact;
    }

    public void setcContact(String cContact) {
        this.cContact = cContact == null ? null : cContact.trim();
    }

    public String getcReservesource() {
        return cReservesource;
    }

    public void setcReservesource(String cReservesource) {
        this.cReservesource = cReservesource == null ? null : cReservesource.trim();
    }

    public String getcDealer() {
        return cDealer;
    }

    public void setcDealer(String cDealer) {
        this.cDealer = cDealer == null ? null : cDealer.trim();
    }

    public String getcCustfrom() {
        return cCustfrom;
    }

    public void setcCustfrom(String cCustfrom) {
        this.cCustfrom = cCustfrom == null ? null : cCustfrom.trim();
    }

    public String getcPaystatus() {
        return cPaystatus;
    }

    public void setcPaystatus(String cPaystatus) {
        this.cPaystatus = cPaystatus == null ? null : cPaystatus.trim();
    }

    public BigDecimal getfContractbalance() {
        return fContractbalance;
    }

    public void setfContractbalance(BigDecimal fContractbalance) {
        this.fContractbalance = fContractbalance;
    }

    public BigDecimal getfTotalpaybalance() {
        return fTotalpaybalance;
    }

    public void setfTotalpaybalance(BigDecimal fTotalpaybalance) {
        this.fTotalpaybalance = fTotalpaybalance;
    }

    public String getcBrokerrecommend() {
        return cBrokerrecommend;
    }

    public void setcBrokerrecommend(String cBrokerrecommend) {
        this.cBrokerrecommend = cBrokerrecommend == null ? null : cBrokerrecommend.trim();
    }

    public String getcSalesource() {
        return cSalesource;
    }

    public void setcSalesource(String cSalesource) {
        this.cSalesource = cSalesource == null ? null : cSalesource.trim();
    }

    public String getcChannelno() {
        return cChannelno;
    }

    public void setcChannelno(String cChannelno) {
        this.cChannelno = cChannelno == null ? null : cChannelno.trim();
    }

    public String getcQuotaflag() {
        return cQuotaflag;
    }

    public void setcQuotaflag(String cQuotaflag) {
        this.cQuotaflag = cQuotaflag == null ? null : cQuotaflag.trim();
    }

    public String getcReserveorderno() {
        return cReserveorderno;
    }

    public void setcReserveorderno(String cReserveorderno) {
        this.cReserveorderno = cReserveorderno == null ? null : cReserveorderno.trim();
    }

    public Integer getlInvestserialno() {
        return lInvestserialno;
    }

    public void setlInvestserialno(Integer lInvestserialno) {
        this.lInvestserialno = lInvestserialno;
    }

    public BigDecimal getfPersonbalance() {
        return fPersonbalance;
    }

    public void setfPersonbalance(BigDecimal fPersonbalance) {
        this.fPersonbalance = fPersonbalance;
    }

    public BigDecimal getfDeptbalance() {
        return fDeptbalance;
    }

    public void setfDeptbalance(BigDecimal fDeptbalance) {
        this.fDeptbalance = fDeptbalance;
    }

    public Long getlProfitid() {
        return lProfitid;
    }

    public void setlProfitid(Long lProfitid) {
        this.lProfitid = lProfitid;
    }

    public String getcDeptnodecode() {
        return cDeptnodecode;
    }

    public void setcDeptnodecode(String cDeptnodecode) {
        this.cDeptnodecode = cDeptnodecode == null ? null : cDeptnodecode.trim();
    }

    public String getcTrustcontractid() {
        return cTrustcontractid;
    }

    public void setcTrustcontractid(String cTrustcontractid) {
        this.cTrustcontractid = cTrustcontractid == null ? null : cTrustcontractid.trim();
    }

    public String getcProductnum() {
        return cProductnum;
    }

    public void setcProductnum(String cProductnum) {
        this.cProductnum = cProductnum == null ? null : cProductnum.trim();
    }

    public String getfContractblance() {
        return fContractblance;
    }

    public void setfContractblance(String fContractblance) {
        this.fContractblance = fContractblance == null ? null : fContractblance.trim();
    }

    public String getcNodecode() {
        return cNodecode;
    }

    public void setcNodecode(String cNodecode) {
        this.cNodecode = cNodecode == null ? null : cNodecode.trim();
    }

    public Date getdExpectedvalueday() {
        return dExpectedvalueday;
    }

    public void setdExpectedvalueday(Date dExpectedvalueday) {
        this.dExpectedvalueday = dExpectedvalueday;
    }

    public String getcReceiveassert() {
        return cReceiveassert;
    }

    public void setcReceiveassert(String cReceiveassert) {
        this.cReceiveassert = cReceiveassert == null ? null : cReceiveassert.trim();
    }

    public Date getdCreatedate() {
        return dCreatedate;
    }

    public void setdCreatedate(Date dCreatedate) {
        this.dCreatedate = dCreatedate;
    }

    public Date getdModifydate() {
        return dModifydate;
    }

    public void setdModifydate(Date dModifydate) {
        this.dModifydate = dModifydate;
    }

    public String getcBuytype() {
        return cBuytype;
    }

    public void setcBuytype(String cBuytype) {
        this.cBuytype = cBuytype == null ? null : cBuytype.trim();
    }

    public BigDecimal getfTempTotalbalance() {
        return fTempTotalbalance;
    }

    public void setfTempTotalbalance(BigDecimal fTempTotalbalance) {
        this.fTempTotalbalance = fTempTotalbalance;
    }

    public BigDecimal getfTempTotalbalance1() {
        return fTempTotalbalance1;
    }

    public void setfTempTotalbalance1(BigDecimal fTempTotalbalance1) {
        this.fTempTotalbalance1 = fTempTotalbalance1;
    }

    public String getcTempDeptnodecode() {
        return cTempDeptnodecode;
    }

    public void setcTempDeptnodecode(String cTempDeptnodecode) {
        this.cTempDeptnodecode = cTempDeptnodecode == null ? null : cTempDeptnodecode.trim();
    }

    public Date getdReservationsuccessdate() {
        return dReservationsuccessdate;
    }

    public void setdReservationsuccessdate(Date dReservationsuccessdate) {
        this.dReservationsuccessdate = dReservationsuccessdate;
    }

    public String getcCapitalstatus() {
        return cCapitalstatus;
    }

    public void setcCapitalstatus(String cCapitalstatus) {
        this.cCapitalstatus = cCapitalstatus == null ? null : cCapitalstatus.trim();
    }

    public String getcCapitalbals() {
        return cCapitalbals;
    }

    public void setcCapitalbals(String cCapitalbals) {
        this.cCapitalbals = cCapitalbals == null ? null : cCapitalbals.trim();
    }

    public String getcIsproinvestor() {
        return cIsproinvestor;
    }

    public void setcIsproinvestor(String cIsproinvestor) {
        this.cIsproinvestor = cIsproinvestor == null ? null : cIsproinvestor.trim();
    }

    public BigDecimal getfSubscriptionratio() {
        return fSubscriptionratio;
    }

    public void setfSubscriptionratio(BigDecimal fSubscriptionratio) {
        this.fSubscriptionratio = fSubscriptionratio;
    }

    public String getcIsprintreceipt() {
        return cIsprintreceipt;
    }

    public void setcIsprintreceipt(String cIsprintreceipt) {
        this.cIsprintreceipt = cIsprintreceipt == null ? null : cIsprintreceipt.trim();
    }

    public String getcSetstatus() {
        return cSetstatus;
    }

    public void setcSetstatus(String cSetstatus) {
        this.cSetstatus = cSetstatus == null ? null : cSetstatus.trim();
    }

    public String getcInterviewstatus() {
        return cInterviewstatus;
    }

    public void setcInterviewstatus(String cInterviewstatus) {
        this.cInterviewstatus = cInterviewstatus == null ? null : cInterviewstatus.trim();
    }

    public Date getdOptime() {
        return dOptime;
    }

    public void setdOptime(Date dOptime) {
        this.dOptime = dOptime;
    }

    public BigDecimal getfCommisionconvert() {
        return fCommisionconvert;
    }

    public void setfCommisionconvert(BigDecimal fCommisionconvert) {
        this.fCommisionconvert = fCommisionconvert;
    }

    public String getcTrustagencyno() {
        return cTrustagencyno;
    }

    public void setcTrustagencyno(String cTrustagencyno) {
        this.cTrustagencyno = cTrustagencyno == null ? null : cTrustagencyno.trim();
    }

    public String getcChannelsource() {
        return cChannelsource;
    }

    public void setcChannelsource(String cChannelsource) {
        this.cChannelsource = cChannelsource == null ? null : cChannelsource.trim();
    }

    public String getcIsoldcustomer() {
        return cIsoldcustomer;
    }

    public void setcIsoldcustomer(String cIsoldcustomer) {
        this.cIsoldcustomer = cIsoldcustomer == null ? null : cIsoldcustomer.trim();
    }

    public String getcVisitstatus() {
        return cVisitstatus;
    }

    public void setcVisitstatus(String cVisitstatus) {
        this.cVisitstatus = cVisitstatus == null ? null : cVisitstatus.trim();
    }

    public String getcVisitmemo() {
        return cVisitmemo;
    }

    public void setcVisitmemo(String cVisitmemo) {
        this.cVisitmemo = cVisitmemo == null ? null : cVisitmemo.trim();
    }

    public Long getlVisitcount() {
        return lVisitcount;
    }

    public void setlVisitcount(Long lVisitcount) {
        this.lVisitcount = lVisitcount;
    }

    public String getcSalebrokeraccount() {
        return cSalebrokeraccount;
    }

    public void setcSalebrokeraccount(String cSalebrokeraccount) {
        this.cSalebrokeraccount = cSalebrokeraccount == null ? null : cSalebrokeraccount.trim();
    }

    public String getcSubbranchname() {
        return cSubbranchname;
    }

    public void setcSubbranchname(String cSubbranchname) {
        this.cSubbranchname = cSubbranchname == null ? null : cSubbranchname.trim();
    }

    public Date getdContractsigndate() {
        return dContractsigndate;
    }

    public void setdContractsigndate(Date dContractsigndate) {
        this.dContractsigndate = dContractsigndate;
    }

    public String getcDocfillstatus() {
        return cDocfillstatus;
    }

    public void setcDocfillstatus(String cDocfillstatus) {
        this.cDocfillstatus = cDocfillstatus == null ? null : cDocfillstatus.trim();
    }

    public String getcReasoncode() {
        return cReasoncode;
    }

    public void setcReasoncode(String cReasoncode) {
        this.cReasoncode = cReasoncode == null ? null : cReasoncode.trim();
    }

    public Date getdFundopendate() {
        return dFundopendate;
    }

    public void setdFundopendate(Date dFundopendate) {
        this.dFundopendate = dFundopendate;
    }

    public String getcWealthsource() {
        return cWealthsource;
    }

    public void setcWealthsource(String cWealthsource) {
        this.cWealthsource = cWealthsource == null ? null : cWealthsource.trim();
    }

    public Date getdDocfilldate() {
        return dDocfilldate;
    }

    public void setdDocfilldate(Date dDocfilldate) {
        this.dDocfilldate = dDocfilldate;
    }

    public BigDecimal getlMorecontactno() {
        return lMorecontactno;
    }

    public void setlMorecontactno(BigDecimal lMorecontactno) {
        this.lMorecontactno = lMorecontactno;
    }

    public String getcReasonmsg() {
        return cReasonmsg;
    }

    public void setcReasonmsg(String cReasonmsg) {
        this.cReasonmsg = cReasonmsg == null ? null : cReasonmsg.trim();
    }

    public Date getdDeldate() {
        return dDeldate;
    }

    public void setdDeldate(Date dDeldate) {
        this.dDeldate = dDeldate;
    }
}
package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 备忘信息表
 * @author tdl
 *
 */
public class MemoInfo {
    //主键
	private String pkId;
	//理财师id
    private String financialId;
    //备忘日期
    private Date infoMomeDate;
    //查询年月
    private String selectDate;
    //备忘信息
    private String infoMome;
    //创建时间
    private Date creatDate;
    //开始时间
    private Date startDate;
    //结束时间
    private Date endDate;
    //备用字段1
    private String remark1;

    private String remark2;

    private String remark3;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFinancialId() {
        return financialId;
    }

    public void setFinancialId(String financialId) {
        this.financialId = financialId == null ? null : financialId.trim();
    }
    
	public String getSelectDate() {
		return selectDate;
	}

	public void setSelectDate(String selectDate) {
		this.selectDate = selectDate;
	}

	public Date getInfoMomeDate() {
		return infoMomeDate;
	}

	public void setInfoMomeDate(Date infoMomeDate) {
		this.infoMomeDate = infoMomeDate;
	}

	public String getInfoMome() {
        return infoMome;
    }

    public void setInfoMome(String infoMome) {
        this.infoMome = infoMome == null ? null : infoMome.trim();
    }

    public Date getCreatDate() {
        return creatDate;
    }

    public void setCreatDate(Date creatDate) {
        this.creatDate = creatDate;
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
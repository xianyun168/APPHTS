package com.ciitc.webServices.model;

import java.util.Date;

public class ProductInfo {
	// 主键
	private String pkId;
	// 项目名称,产品名称
	private String productname;
	// 产品类型
	private String pruducttype;
	// 管理人
	private String productmanager;
	// 托管人
	private String productcustodian;
	// 外包服务商
	private String outsourcingserviceprovider;
	// 募集规模,发行规模
	private String scale;
	// 存续期限,项目期限
	private String deadline;
	// 认购起点,个人投资起点
	private String perminbalance;
	// 业绩比较基准,预期年化收益率
	private String yield;
	// 分红方式,收益分配方式
	private String bonustype;
	// 募集户名
	private String raisename;
	// 募集银行
	private String raisebank;
	// 募集账号
	private String raiseacco;
	// 产品投向
	private String productthrowup;
	// 风险收益特征
	private String riskincome;
	// 产品费率
	private String rate;
	// 股权质押（暂时无用）
	private String pledge;
	// 对赌协议（暂时无用）
	private String agreement;
	// 补偿条款（暂时无用）
	private String clause;
	// 产品实际到期时间
	private Date actualDate;
	// 产品默认到期时间
	private Date defaultDate;
	// 备用字段1
	private String remark1;
	// 备用字段2
	private String remark2;
	// 备用字段3
	private String remark3;
	
	//产品状态--（1-待发行、2-热销中（热销）、3-发行结束）
	private String status;
	private String compensationClause;//股权质押

    private String bettingAgreement;//对赌协议

    private String sharePledge;//补偿条款
    
    private String riskManagement;//风控措施
    private int spciTye;//开放式产品标志 1-开放式产品 2-非开放式产品

	public int getSpciTye() {
		return spciTye;
	}

	public void setSpciTye(int spciTye) {
		this.spciTye = spciTye;
	}

	public String getCompensationClause() {
		return compensationClause;
	}

	public void setCompensationClause(String compensationClause) {
		this.compensationClause = compensationClause;
	}

	public String getBettingAgreement() {
		return bettingAgreement;
	}

	public void setBettingAgreement(String bettingAgreement) {
		this.bettingAgreement = bettingAgreement;
	}

	public String getSharePledge() {
		return sharePledge;
	}

	public void setSharePledge(String sharePledge) {
		this.sharePledge = sharePledge;
	}

	public String getRiskManagement() {
		return riskManagement;
	}

	public void setRiskManagement(String riskManagement) {
		this.riskManagement = riskManagement;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getProductname() {
		return productname;
	}

	public void setProductname(String productname) {
		this.productname = productname;
	}

	public String getPruducttype() {
		return pruducttype;
	}

	public void setPruducttype(String pruducttype) {
		this.pruducttype = pruducttype;
	}

	public String getProductmanager() {
		return productmanager;
	}

	public void setProductmanager(String productmanager) {
		this.productmanager = productmanager;
	}

	public String getProductcustodian() {
		return productcustodian;
	}

	public void setProductcustodian(String productcustodian) {
		this.productcustodian = productcustodian;
	}

	public String getOutsourcingserviceprovider() {
		return outsourcingserviceprovider;
	}

	public void setOutsourcingserviceprovider(String outsourcingserviceprovider) {
		this.outsourcingserviceprovider = outsourcingserviceprovider;
	}

	public String getScale() {
		return scale;
	}

	public void setScale(String scale) {
		this.scale = scale;
	}

	public String getDeadline() {
		return deadline;
	}

	public void setDeadline(String deadline) {
		this.deadline = deadline;
	}

	public String getPerminbalance() {
		return perminbalance;
	}

	public void setPerminbalance(String perminbalance) {
		this.perminbalance = perminbalance;
	}

	public String getYield() {
		return yield;
	}

	public void setYield(String yield) {
		this.yield = yield;
	}

	public String getBonustype() {
		return bonustype;
	}

	public void setBonustype(String bonustype) {
		this.bonustype = bonustype;
	}

	public String getRaisename() {
		return raisename;
	}

	public void setRaisename(String raisename) {
		this.raisename = raisename;
	}

	public String getRaisebank() {
		return raisebank;
	}

	public void setRaisebank(String raisebank) {
		this.raisebank = raisebank;
	}

	public String getRaiseacco() {
		return raiseacco;
	}

	public void setRaiseacco(String raiseacco) {
		this.raiseacco = raiseacco;
	}

	public Date getActualDate() {
		return actualDate;
	}

	public void setActualDate(Date actualDate) {
		this.actualDate = actualDate;
	}

	public Date getDefaultDate() {
		return defaultDate;
	}

	public void setDefaultDate(Date defaultDate) {
		this.defaultDate = defaultDate;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

	public String getProductthrowup() {
		return productthrowup;
	}

	public void setProductthrowup(String productthrowup) {
		this.productthrowup = productthrowup;
	}

	public String getRiskincome() {
		return riskincome;
	}

	public void setRiskincome(String riskincome) {
		this.riskincome = riskincome;
	}

	public String getRate() {
		return rate;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public String getPledge() {
		return pledge;
	}

	public void setPledge(String pledge) {
		this.pledge = pledge;
	}

	public String getAgreement() {
		return agreement;
	}

	public void setAgreement(String agreement) {
		this.agreement = agreement;
	}

	public String getClause() {
		return clause;
	}

	public void setClause(String clause) {
		this.clause = clause;
	}

}
package com.ciitc.webServices.model;


public class ReservationDtl {
	//客户编号
	private String c_custno;
	//客户名称
	private String c_custname;
	//预约金额
	private Integer f_balance;
	//预约时间
	private String d_reservedate;
	//预约状态
	private String c_status;
	//预约编号
	private String c_reserveno;
	//受益级别
	private String c_profitclass;
	//产品编号
	private String c_fundcode;
	public String getC_fundcode() {
		return c_fundcode;
	}
	public void setC_fundcode(String c_fundcode) {
		this.c_fundcode = c_fundcode;
	}
	public String getC_reserveno() {
		return c_reserveno;
	}
	public void setC_reserveno(String c_reserveno) {
		this.c_reserveno = c_reserveno;
	}
	public String getC_profitclass() {
		return c_profitclass;
	}
	public void setC_profitclass(String c_profitclass) {
		this.c_profitclass = c_profitclass;
	}
	public String getC_custno() {
		return c_custno;
	}
	public void setC_custno(String c_custno) {
		this.c_custno = c_custno;
	}
	public String getC_custname() {
		return c_custname;
	}
	public void setC_custname(String c_custname) {
		this.c_custname = c_custname;
	}
	public Integer getF_balance() {
		return f_balance;
	}
	public void setF_balance(Integer f_balance) {
		this.f_balance = f_balance;
	}
	public String getD_reservedate() {
		return d_reservedate;
	}
	public void setD_reservedate(String d_reservedate) {
		this.d_reservedate = d_reservedate;
	}
	public String getC_status() {
		return c_status;
	}
	public void setC_status(String c_status) {
		this.c_status = c_status;
	}
	
	

}

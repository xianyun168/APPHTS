package com.ciitc.webServices.model;

/**
 * 业绩计算表
 * @author 任之阳
 *
 */
public class PerformanceCalcuation {

	 //主键
	private String pkId;
	
	 //理财师Id
	private String financiaId;
	
     //认购金额
	private double subscriptionMoney;
	
	 //折标系数
	private double coefficient;
	 
	 //每笔金额
	private double eachMoney;
	
	//备用字段1
	private String remark1;
		
	//备用字段2
	private String remark2;
		
	//备用字段3
	private String remark3;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getFinanciaId() {
		return financiaId;
	}

	public void setFinanciaId(String financiaId) {
		this.financiaId = financiaId;
	}

	public double getSubscriptionMoney() {
		return subscriptionMoney;
	}

	public void setSubscriptionMoney(double subscriptionMoney) {
		this.subscriptionMoney = subscriptionMoney;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}

	public double getEachMoney() {
		return eachMoney;
	}

	public void setEachMoney(double eachMoney) {
		this.eachMoney = eachMoney;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

}

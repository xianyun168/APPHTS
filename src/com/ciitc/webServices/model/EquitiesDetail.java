package com.ciitc.webServices.model;

public class EquitiesDetail {
	private String pkId;
	// 国康父产品编码
    private String equitiesPid;
	// 国康父产品名字
    private String equitiesPname;
    //国康子产品编码
    private String equitiesId;
	// 国康子产品名字
    private String equitiesName;
	// 权益次数
    private String equitiesNum;
	// 客户等级
    private String customerLevel;

    private String remark1;

    private String remark2;

    private String remark3;
    

    public String getEquitiesId() {
		return equitiesId;
	}

	public void setEquitiesId(String equitiesId) {
		this.equitiesId = equitiesId;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getEquitiesPid() {
        return equitiesPid;
    }

    public void setEquitiesPid(String equitiesPid) {
        this.equitiesPid = equitiesPid == null ? null : equitiesPid.trim();
    }

    public String getEquitiesPname() {
        return equitiesPname;
    }

    public void setEquitiesPname(String equitiesPname) {
        this.equitiesPname = equitiesPname == null ? null : equitiesPname.trim();
    }

    public String getEquitiesName() {
        return equitiesName;
    }

    public void setEquitiesName(String equitiesName) {
        this.equitiesName = equitiesName == null ? null : equitiesName.trim();
    }

    public String getEquitiesNum() {
        return equitiesNum;
    }

    public void setEquitiesNum(String equitiesNum) {
        this.equitiesNum = equitiesNum == null ? null : equitiesNum.trim();
    }

    public String getCustomerLevel() {
        return customerLevel;
    }

    public void setCustomerLevel(String customerLevel) {
        this.customerLevel = customerLevel == null ? null : customerLevel.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
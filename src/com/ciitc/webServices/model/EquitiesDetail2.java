package com.ciitc.webServices.model;

public class EquitiesDetail2 {
	
	// 国康父产品编码
	private String gkProductPId;
	// 国康子产品编码
	private String gkProductId;
	// 权益次数
    private String equitiesNum;
	// 权益已使用次数
	private String usedEquitiesNum;


	public String getGkProductPId() {
		return gkProductPId;
	}

	public void setGkProductPId(String gkProductPId) {
		this.gkProductPId = gkProductPId;
	}

	public String getGkProductId() {
		return gkProductId;
	}

	public void setGkProductId(String gkProductId) {
		this.gkProductId = gkProductId;
	}

	public String getUsedEquitiesNum() {
		return usedEquitiesNum;
	}

	public void setUsedEquitiesNum(String usedEquitiesNum) {
		this.usedEquitiesNum = usedEquitiesNum;
	}

	public String getEquitiesNum() {
		return equitiesNum;
	}
	public void setEquitiesNum(String equitiesNum) {
		this.equitiesNum = equitiesNum;
	}
	
}
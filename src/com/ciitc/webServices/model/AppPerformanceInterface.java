package com.ciitc.webServices.model;

import java.util.Date;

/**
 * 业绩接口核算表
 * @author Administrator
 *
 */
public class AppPerformanceInterface {
	private  String      codeid;//理财师ID
	private  String   codename;//理财师编号
	private  Double   indcontract;//个人合同业绩和
	private  Double   teamcontract;//小团队合同业绩和
	private  Double   companycontract;//分公司合同业绩和
	private  Double   areacontact;//大区合同业绩和
	private  Double   indaims;//个人当前目标业绩
	private  Double   teamaims;//小团队目标业绩和
	private  Double   companyaims;//分公司目标业绩和
	private  Double   areatims;//大区目标业绩和
	private  String  indtage;//个人目标完成率
	private  String  teamtage;//小团队目标完成率
	private  String  companytage;//分公司目标完成率
	private  String  areatage;//大区目标完成率
	private  Date    time;//时间
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getCodeid() {
		return codeid;
	}
	public void setCodeid(String codeid) {
		this.codeid = codeid;
	}
	public String getCodename() {
		return codename;
	}
	public void setCodename(String codename) {
		this.codename = codename;
	}
	public Double getIndcontract() {
		return indcontract;
	}
	public void setIndcontract(Double indcontract) {
		this.indcontract = indcontract;
	}
	public Double getTeamcontract() {
		return teamcontract;
	}
	public void setTeamcontract(Double teamcontract) {
		this.teamcontract = teamcontract;
	}
	public Double getCompanycontract() {
		return companycontract;
	}
	public void setCompanycontract(Double companycontract) {
		this.companycontract = companycontract;
	}
	public Double getAreacontact() {
		return areacontact;
	}
	public void setAreacontact(Double areacontact) {
		this.areacontact = areacontact;
	}
	public Double getIndaims() {
		return indaims;
	}
	public void setIndaims(Double indaims) {
		this.indaims = indaims;
	}
	public Double getTeamaims() {
		return teamaims;
	}
	public void setTeamaims(Double teamaims) {
		this.teamaims = teamaims;
	}
	public Double getCompanyaims() {
		return companyaims;
	}
	public void setCompanyaims(Double companyaims) {
		this.companyaims = companyaims;
	}
	public Double getAreatims() {
		return areatims;
	}
	public void setAreatims(Double areatims) {
		this.areatims = areatims;
	}
	
	public void setTeamtage(String teamtage) {
		this.teamtage = teamtage;
	}
	public void setCompanytage(String companytage) {
		this.companytage = companytage;
	}
	public void setAreatage(String areatage) {
		this.areatage = areatage;
	}
	public void setIndtage(String indtage) {
		this.indtage = indtage;
	}
	
	
	public String getIndtage() {
		return indtage;
	}
	public String getTeamtage() {
		return teamtage;
	}
	public String getCompanytage() {
		return companytage;
	}
	public String getAreatage() {
		return areatage;
	}
	@Override
	public String toString() {
		return "AppPerformanceInterface [codeid=" + codeid + ", codename=" + codename + ", indcontract=" + indcontract
				+ ", teamcontract=" + teamcontract + ", companycontract=" + companycontract + ", areacontact="
				+ areacontact + ", indaims=" + indaims + ", teamaims=" + teamaims + ", companyaims=" + companyaims
				+ ", areatims=" + areatims + ", indtage=" + indtage + ", teamtage=" + teamtage + ", companytage="
				+ companytage + ", areatage=" + areatage + ", time=" + time + "]";
	}
	
   
}

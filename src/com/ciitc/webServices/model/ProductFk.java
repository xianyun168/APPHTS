package com.ciitc.webServices.model;

public class ProductFk {
    private String pkId;

    private String productId;

    private String compensationClause;//股权质押

    private String bettingAgreement;//对赌协议

    private String sharePledge;//补偿条款

    private String flag;

    private String states;

    private String remark1;

    private String remark2;

    private String remark3;

    private String remark4;

    private String remark5;

    private String riskManagement;//风控措施

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getCompensationClause() {
        return compensationClause;
    }

    public void setCompensationClause(String compensationClause) {
        this.compensationClause = compensationClause == null ? null : compensationClause.trim();
    }

    public String getBettingAgreement() {
        return bettingAgreement;
    }

    public void setBettingAgreement(String bettingAgreement) {
        this.bettingAgreement = bettingAgreement == null ? null : bettingAgreement.trim();
    }

    public String getSharePledge() {
        return sharePledge;
    }

    public void setSharePledge(String sharePledge) {
        this.sharePledge = sharePledge == null ? null : sharePledge.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getStates() {
        return states;
    }

    public void setStates(String states) {
        this.states = states == null ? null : states.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    public String getRemark4() {
        return remark4;
    }

    public void setRemark4(String remark4) {
        this.remark4 = remark4 == null ? null : remark4.trim();
    }

    public String getRemark5() {
        return remark5;
    }

    public void setRemark5(String remark5) {
        this.remark5 = remark5 == null ? null : remark5.trim();
    }

    public String getRiskManagement() {
        return riskManagement;
    }

    public void setRiskManagement(String riskManagement) {
        this.riskManagement = riskManagement == null ? null : riskManagement.trim();
    }
}
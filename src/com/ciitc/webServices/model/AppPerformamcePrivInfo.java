package com.ciitc.webServices.model;

public class AppPerformamcePrivInfo {
	private int departmentid;
	private String departmentname;
	private String loginid;
	private String lvltyp;
	public int getDepartmentid() {
		return departmentid;
	}
	public void setDepartmentid(int departmentid) {
		this.departmentid = departmentid;
	}
	public String getDepartmentname() {
		return departmentname;
	}
	public void setDepartmentname(String departmentname) {
		this.departmentname = departmentname;
	}
	public String getLoginid() {
		return loginid;
	}
	public void setLoginid(String loginid) {
		this.loginid = loginid;
	}
	public String getLvltyp() {
		return lvltyp;
	}
	public void setLvltyp(String lvltyp) {
		this.lvltyp = lvltyp;
	}
	@Override
	public String toString() {
		return "AppPerformamcePrivInfo [departmentid=" + departmentid + ", departmentname=" + departmentname
				+ ", loginid=" + loginid + ", lvltyp=" + lvltyp + "]";
	}
	
	

}

package com.ciitc.webServices.model;

public class CPTfundborkerrelation {

    private String cBrokeraccount;

    private String cCustno;

    private String cRelationType;

    private String cIsmain;

    private String cFundacco;


    public String getcBrokeraccount() {
        return cBrokeraccount;
    }

    public void setcBrokeraccount(String cBrokeraccount) {
        this.cBrokeraccount = cBrokeraccount == null ? null : cBrokeraccount.trim();
    }

    public String getcCustno() {
        return cCustno;
    }

    public void setcCustno(String cCustno) {
        this.cCustno = cCustno == null ? null : cCustno.trim();
    }

    public String getcRelationType() {
        return cRelationType;
    }

    public void setcRelationType(String cRelationType) {
        this.cRelationType = cRelationType == null ? null : cRelationType.trim();
    }

    public String getcIsmain() {
        return cIsmain;
    }

    public void setcIsmain(String cIsmain) {
        this.cIsmain = cIsmain == null ? null : cIsmain.trim();
    }

    public String getcFundacco() {
        return cFundacco;
    }

    public void setcFundacco(String cFundacco) {
        this.cFundacco = cFundacco == null ? null : cFundacco.trim();
    }
}
package com.ciitc.webServices.model;

import java.util.Date;

public class EsbmmCode  {

    //主键
    private  String pkId;
    //代码类型主键
    private  String codeTypePkId;
    //
    private  String codeCode;
    //代码英文名称
    private  String codeName;
    //代码中文名称
    private  String chineseName;
    //有效标识位
    private  String status;
    //代码值
    private  String codeValue;
    //创建人
    private  String createdBy;
    //创建时间
    private  Date createdDate;
    //修改人
    private  String updatedBy;
    //修改时间
    private  Date updatedDate;
    //标志位
    private  String flag;
    //备注
    private  String remark;

	 public String getPkId() {
		return this.pkId;
	 }
	
	 public void setPkId(String pkId) {
		this.pkId = pkId;
	 }
	 
	 public String getCodeTypePkId() {
		return this.codeTypePkId;
	 }
	
	 public void setCodeTypePkId(String codeTypePkId) {
		this.codeTypePkId = codeTypePkId;
	 }
	 
	 public String getCodeCode() {
		return this.codeCode;
	 }
	
	 public void setCodeCode(String codeCode) {
		this.codeCode = codeCode;
	 }
	 
	 public String getCodeName() {
		return this.codeName;
	 }
	
	 public void setCodeName(String codeName) {
		this.codeName = codeName;
	 }
	 
	 public String getChineseName() {
		return this.chineseName;
	 }
	
	 public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	 }
	 
	 public String getStatus() {
		return this.status;
	 }
	
	 public void setStatus(String status) {
		this.status = status;
	 }
	 
	 public String getCodeValue() {
		return this.codeValue;
	 }
	
	 public void setCodeValue(String codeValue) {
		this.codeValue = codeValue;
	 }
	 
	 public String getCreatedBy() {
		return this.createdBy;
	 }
	
	 public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	 }
	 
	 public Date getCreatedDate() {
		return this.createdDate;
	 }
	
	 public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	 }
	 
	 public String getUpdatedBy() {
		return this.updatedBy;
	 }
	
	 public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	 }
	 
	 public Date getUpdatedDate() {
		return this.updatedDate;
	 }
	
	 public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	 }
	 
	 public String getFlag() {
		return this.flag;
	 }
	
	 public void setFlag(String flag) {
		this.flag = flag;
	 }
	 
	 public String getRemark() {
		return this.remark;
	 }
	
	 public void setRemark(String remark) {
		this.remark = remark;
	 }
	 
 }
package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 签到表
 * @author tdl
 *
 */
public class Sign {
    //主键
	private String pkId;
	//理财师id
    private String financialId;
    //理财师名字
    private String financialName;
    //签到时间
    private Date signDate;
    //GPS_Y坐标
    private String gpsY;
    //GPS_X坐标
    private String gpsX;
    //上级领导id
    private String leaderId;

    private String remark1;

    private String remark2;

    private String remark3;
    
    //地名
    private String plance;

    public String getPlance() {
		return plance;
	}

	public void setPlance(String plance) {
		this.plance = plance;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getFinancialId() {
        return financialId;
    }

    public void setFinancialId(String financialId) {
        this.financialId = financialId == null ? null : financialId.trim();
    }

    public String getFinancialName() {
        return financialName;
    }

    public void setFinancialName(String financialName) {
        this.financialName = financialName == null ? null : financialName.trim();
    }

    public Date getSignDate() {
        return signDate;
    }

    public void setSignDate(Date signDate) {
        this.signDate = signDate;
    }

    public String getGpsY() {
        return gpsY;
    }

    public void setGpsY(String gpsY) {
        this.gpsY = gpsY == null ? null : gpsY.trim();
    }

    public String getGpsX() {
        return gpsX;
    }

    public void setGpsX(String gpsX) {
        this.gpsX = gpsX == null ? null : gpsX.trim();
    }

    public String getLeaderId() {
        return leaderId;
    }

    public void setLeaderId(String leaderId) {
        this.leaderId = leaderId == null ? null : leaderId.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
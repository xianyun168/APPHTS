package com.ciitc.webServices.model;

import java.util.Date;

public class CustomerInfo {
	//pkId
    private String pkId;
    //理财师ID
    private String financialManagerId;
    //客户姓名
    private String customerName;
    //客户性别
    private String customerSex;
    //客户年龄
    private String customerAge;
    //客户手机号
    private String customerTelephone;
    //出生日期
    private Date customerBirthday;
    //客户住址
    private String customerAddress;
    //客户投资体量
    private String customerMoney;
    //客户等级
    private String customerLevel;
    //客户实名认证状态
    private String customerAs;
    //证件号码
    private String customerGredentials;
    //证件类型
    private String customerGredentialsType;
    //证件照
    private String customerPhoto;
    //客户标签
    private String customerLabel;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getFinancialManagerId() {
        return financialManagerId;
    }

    public void setFinancialManagerId(String financialManagerId) {
        this.financialManagerId = financialManagerId == null ? null : financialManagerId.trim();
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName == null ? null : customerName.trim();
    }

    public String getCustomerSex() {
        return customerSex;
    }

    public void setCustomerSex(String customerSex) {
        this.customerSex = customerSex == null ? null : customerSex.trim();
    }

    public String getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(String customerAge) {
        this.customerAge = customerAge == null ? null : customerAge.trim();
    }

    public String getCustomerTelephone() {
        return customerTelephone;
    }

    public void setCustomerTelephone(String customerTelephone) {
        this.customerTelephone = customerTelephone == null ? null : customerTelephone.trim();
    }

    public Date getCustomerBirthday() {
        return customerBirthday;
    }

    public void setCustomerBirthday(Date customerBirthday) {
        this.customerBirthday = customerBirthday;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress == null ? null : customerAddress.trim();
    }

    public String getCustomerMoney() {
        return customerMoney;
    }

    public void setCustomerMoney(String customerMoney) {
        this.customerMoney = customerMoney == null ? null : customerMoney.trim();
    }

    public String getCustomerLevel() {
        return customerLevel;
    }

    public void setCustomerLevel(String customerLevel) {
        this.customerLevel = customerLevel == null ? null : customerLevel.trim();
    }

    public String getCustomerAs() {
        return customerAs;
    }

    public void setCustomerAs(String customerAs) {
        this.customerAs = customerAs == null ? null : customerAs.trim();
    }

    public String getCustomerGredentials() {
        return customerGredentials;
    }

    public void setCustomerGredentials(String customerGredentials) {
        this.customerGredentials = customerGredentials == null ? null : customerGredentials.trim();
    }

    public String getCustomerGredentialsType() {
        return customerGredentialsType;
    }

    public void setCustomerGredentialsType(String customerGredentialsType) {
        this.customerGredentialsType = customerGredentialsType == null ? null : customerGredentialsType.trim();
    }

    public String getCustomerPhoto() {
        return customerPhoto;
    }

    public void setCustomerPhoto(String customerPhoto) {
        this.customerPhoto = customerPhoto == null ? null : customerPhoto.trim();
    }

    public String getCustomerLabel() {
        return customerLabel;
    }

    public void setCustomerLabel(String customerLabel) {
        this.customerLabel = customerLabel == null ? null : customerLabel.trim();
    }
}
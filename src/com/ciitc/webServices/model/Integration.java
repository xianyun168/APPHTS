package com.ciitc.webServices.model;

import java.sql.Date;

public class Integration {
	//客户编号
	private String custno;
	//产品名称
	private String protuctName;
	//持仓日期
	private Integer positionDate;
	//合同起息日期
	private Date contractsignDate;
	//合同金额
	private Integer money;
	//合同编号
	private String reserveno;
	public String getCustno() {
		return custno;
	}
	public void setCustno(String custno) {
		this.custno = custno;
	}
	public String getProtuctName() {
		return protuctName;
	}
	public void setProtuctName(String protuctName) {
		this.protuctName = protuctName;
	}
	public Integer getPositionDate() {
		return positionDate;
	}
	public void setPositionDate(Integer positionDate) {
		this.positionDate = positionDate;
	}
	public Date getContractsignDate() {
		return contractsignDate;
	}
	public void setContractsignDate(Date contractsignDate) {
		this.contractsignDate = contractsignDate;
	}
	public Integer getMoney() {
		return money;
	}
	public void setMoney(Integer money) {
		this.money = money;
	}
	public String getReserveno() {
		return reserveno;
	}
	public void setReserveno(String reserveno) {
		this.reserveno = reserveno;
	}
	

}

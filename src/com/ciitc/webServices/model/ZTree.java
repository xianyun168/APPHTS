package com.ciitc.webServices.model;


import java.util.List;


public class ZTree{

	private String id = "";
	private String name;// 树节点名称
	
	private Boolean chkDisabled = false;// 是否禁用
	private Boolean checked = false;// 是否勾选状态
	private List<ZTree> children;// 子节点
	private Boolean open = true;// 是否展开(open,closed)
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getChkDisabled() {
		return chkDisabled;
	}
	public void setChkDisabled(Boolean chkDisabled) {
		this.chkDisabled = chkDisabled;
	}
	public Boolean getChecked() {
		return checked;
	}
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}
	public List<ZTree> getChildren() {
		return children;
	}
	public void setChildren(List<ZTree> children) {
		this.children = children;
	}
	public Boolean getOpen() {
		return open;
	}
	public void setOpen(Boolean open) {
		this.open = open;
	}

}

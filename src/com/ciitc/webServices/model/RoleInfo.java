package com.ciitc.webServices.model;

import java.util.Date;

public class RoleInfo  {

    //主键
    private  String pkId;
    //角色名称
    private  String roleName;
    //描述
    private  String description;
    //有效标志位
    private  String status;
    //创建人
    private  String createdBy;
    //创建时间
    private  Date createdDate;
    //修改人
    private  String updatedBy;
    //修改时间
    private  Date updatedDate;
    //标志位
    private  String flag;
    //备注
    private  String remark;

	 public String getPkId() {
		return this.pkId;
	 }
	
	 public void setPkId(String pkId) {
		this.pkId = pkId;
	 }
	 
	 public String getRoleName() {
		return this.roleName;
	 }
	
	 public void setRoleName(String roleName) {
		this.roleName = roleName;
	 }
	 
	 public String getDescription() {
		return this.description;
	 }
	
	 public void setDescription(String description) {
		this.description = description;
	 }
	 
	 public String getStatus() {
		return this.status;
	 }
	
	 public void setStatus(String status) {
		this.status = status;
	 }
	 
	 public String getCreatedBy() {
		return this.createdBy;
	 }
	
	 public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	 }
	 
	 public Date getCreatedDate() {
		return this.createdDate;
	 }
	
	 public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	 }
	 
	 public String getUpdatedBy() {
		return this.updatedBy;
	 }
	
	 public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	 }
	 
	 public Date getUpdatedDate() {
		return this.updatedDate;
	 }
	
	 public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	 }
	 
	 public String getFlag() {
		return this.flag;
	 }
	
	 public void setFlag(String flag) {
		this.flag = flag;
	 }
	 
	 public String getRemark() {
		return this.remark;
	 }
	
	 public void setRemark(String remark) {
		this.remark = remark;
	 }
	 
 }
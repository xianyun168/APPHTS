package com.ciitc.webServices.model;

import java.util.Date;

public class Operation  {

    //主键
    private  String pkId;
    //操作代码
    private  String operationCode;
    //操作名称
    private  String operationName;
    //菜单表主键
    private  String menuPkId;
    //状态
    private  String status;
    //创建人
    private  String createdBy;
    //创建时间
    private  Date createdDate;
    //修改人
    private  String updatedBy;
    //修改时间
    private  Date updatedDate;
    //标志位
    private  String flag;
    //备注
    private  String remark;

	 public String getPkId() {
		return this.pkId;
	 }
	
	 public void setPkId(String pkId) {
		this.pkId = pkId;
	 }
	 
	 public String getOperationCode() {
		return this.operationCode;
	 }
	
	 public void setOperationCode(String operationCode) {
		this.operationCode = operationCode;
	 }
	 
	 public String getOperationName() {
		return this.operationName;
	 }
	
	 public void setOperationName(String operationName) {
		this.operationName = operationName;
	 }
	 
	 public String getMenuPkId() {
		return this.menuPkId;
	 }
	
	 public void setMenuPkId(String menuPkId) {
		this.menuPkId = menuPkId;
	 }
	 
	 public String getStatus() {
		return this.status;
	 }
	
	 public void setStatus(String status) {
		this.status = status;
	 }
	 
	 public String getCreatedBy() {
		return this.createdBy;
	 }
	
	 public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	 }
	 
	 public Date getCreatedDate() {
		return this.createdDate;
	 }
	
	 public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	 }
	 
	 public String getUpdatedBy() {
		return this.updatedBy;
	 }
	
	 public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	 }
	 
	 public Date getUpdatedDate() {
		return this.updatedDate;
	 }
	
	 public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	 }
	 
	 public String getFlag() {
		return this.flag;
	 }
	
	 public void setFlag(String flag) {
		this.flag = flag;
	 }
	 
	 public String getRemark() {
		return this.remark;
	 }
	
	 public void setRemark(String remark) {
		this.remark = remark;
	 }
	 
 }
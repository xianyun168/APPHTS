package com.ciitc.webServices.model;

public class UserInforEq {
	//客户编号
	private String cusstomerId;
	//客户电话
    private String phoneNumber;
    //客户名称
    private String userName;
    //证件类型
    private String idcardType;
    //证件号码
    private String idcardNumber;
    //所属大区
    private String region;
    //理财师名称
    private String financialName;
    //理财师编号
    private String financialId;
    //所属分公司
    private String companyName;
    //累计投资金额
    private int countMoney;
    //持仓峰值
    private int maxMoney;
    //客户等级
    private String customerLevel;
    //名医看诊权益总次数
    private int famousDoctorNum;
    //私人医生权益总次数
    private int prvivateDoctorNum;
    //电话咨询
    private String consultation;
    //权益使用明细
    private String EquitiesUsedDetail;
    //权益剩余明细
    private String EquitiesDetail;
    
	public String getConsultation() {
		return consultation;
	}
	public void setConsultation(String consultation) {
		this.consultation = consultation;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getIdcardType() {
		return idcardType;
	}
	public void setIdcardType(String idcardType) {
		this.idcardType = idcardType;
	}
	public String getIdcardNumber() {
		return idcardNumber;
	}
	public void setIdcardNumber(String idcardNumber) {
		this.idcardNumber = idcardNumber;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getFinancialName() {
		return financialName;
	}
	public void setFinancialName(String financialName) {
		this.financialName = financialName;
	}
	public String getFinancialId() {
		return financialId;
	}
	public void setFinancialId(String financialId) {
		this.financialId = financialId;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public int getCountMoney() {
		return countMoney;
	}
	public void setCountMoney(int countMoney) {
		this.countMoney = countMoney;
	}
	public String getCustomerLevel() {
		return customerLevel;
	}
	public void setCustomerLevel(String customerLevel) {
		this.customerLevel = customerLevel;
	}
	public int getMaxMoney() {
		return maxMoney;
	}
	public void setMaxMoney(int maxMoney) {
		this.maxMoney = maxMoney;
	}
	public int getFamousDoctorNum() {
		return famousDoctorNum;
	}
	public void setFamousDoctorNum(int famousDoctorNum) {
		this.famousDoctorNum = famousDoctorNum;
	}
	public int getPrvivateDoctorNum() {
		return prvivateDoctorNum;
	}
	public void setPrvivateDoctorNum(int prvivateDoctorNum) {
		this.prvivateDoctorNum = prvivateDoctorNum;
	}
	public String getEquitiesUsedDetail() {
		return EquitiesUsedDetail;
	}
	public void setEquitiesUsedDetail(String equitiesUsedDetail) {
		EquitiesUsedDetail = equitiesUsedDetail;
	}
	public String getEquitiesDetail() {
		return EquitiesDetail;
	}
	public void setEquitiesDetail(String equitiesDetail) {
		EquitiesDetail = equitiesDetail;
	}
	public String getCusstomerId() {
		return cusstomerId;
	}
	public void setCusstomerId(String cusstomerId) {
		this.cusstomerId = cusstomerId;
	}
	
}
package com.ciitc.webServices.model;

import java.util.Date;

public class SmsSend {
    private String pkId;

    private String phoneNumber;

    private String smsSendType;

    private String smsSendState;

    private String smsMessageContent;

    private String smsOperateName;

    private Date timingSendDate;
    
    private String sendDate;

    private String remark1;

    private String remark2;

    private String remark3;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
    }

    public String getSmsSendType() {
        return smsSendType;
    }

    public void setSmsSendType(String smsSendType) {
        this.smsSendType = smsSendType == null ? null : smsSendType.trim();
    }

    public String getSmsSendState() {
        return smsSendState;
    }

    public void setSmsSendState(String smsSendState) {
        this.smsSendState = smsSendState == null ? null : smsSendState.trim();
    }

    public String getSmsMessageContent() {
        return smsMessageContent;
    }

    public void setSmsMessageContent(String smsMessageContent) {
        this.smsMessageContent = smsMessageContent == null ? null : smsMessageContent.trim();
    }

    public String getSmsOperateName() {
        return smsOperateName;
    }

    public void setSmsOperateName(String smsOperateName) {
        this.smsOperateName = smsOperateName == null ? null : smsOperateName.trim();
    }

    public Date getTimingSendDate() {
        return timingSendDate;
    }

    public void setTimingSendDate(Date timingSendDate) {
        this.timingSendDate = timingSendDate;
    }
    
    public String getSendDate() {
		return sendDate;
	}

	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}

	public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
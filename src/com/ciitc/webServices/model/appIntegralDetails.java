package com.ciitc.webServices.model;

import java.util.Date;

public class appIntegralDetails {
	//主键
    private String pkId;
    //用户ID
    private String userId;
    //记录日期
    private Date recordDate;
    //积分值
    private String integralNum;
    //有效期
    private Integer validity;
    //积分类型
    private String integralType;
    //
    private String name;
    //积分状态
    private String integralState;
    //事由
    private String reason;

    private String remark1;

    private String remark2;

    private String remark3;
   
    //合同编号
    private String contractNum;
    
    //消耗积分
    private double consumptionIntegral;
    
    //到期分值
    private double expireIntegral;
    
    //有效分值
    private double validIntegral;
    
    //到期日期
    private Date expireDate;
    
    public String getContractNum() {
		return contractNum;
	}

	public void setContractNum(String contractNum) {
		this.contractNum = contractNum;
	}

	public double getConsumptionIntegral() {
		return consumptionIntegral;
	}

	public void setConsumptionIntegral(double consumptionIntegral) {
		this.consumptionIntegral = consumptionIntegral;
	}

	public double getExpireIntegral() {
		return expireIntegral;
	}

	public void setExpireIntegral(double expireIntegral) {
		this.expireIntegral = expireIntegral;
	}

	public double getValidIntegral() {
		return validIntegral;
	}

	public void setValidIntegral(double validIntegral) {
		this.validIntegral = validIntegral;
	}

	public Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(Date expireDate) {
		this.expireDate = expireDate;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Date getRecordDate() {
        return recordDate;
    }

    public void setRecordDate(Date recordDate) {
        this.recordDate = recordDate;
    }

    public String getIntegralNum() {
        return integralNum;
    }

    public void setIntegralNum(String integralNum) {
        this.integralNum = integralNum == null ? null : integralNum.trim();
    }

    public Integer getValidity() {
        return validity;
    }

    public void setValidity(Integer validity) {
        this.validity = validity;
    }

    public String getIntegralType() {
        return integralType;
    }

    public void setIntegralType(String integralType) {
        this.integralType = integralType == null ? null : integralType.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getIntegralState() {
        return integralState;
    }

    public void setIntegralState(String integralState) {
        this.integralState = integralState == null ? null : integralState.trim();
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
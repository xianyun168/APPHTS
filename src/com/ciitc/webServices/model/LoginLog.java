package com.ciitc.webServices.model;

/**
 * @program: APPHTS
 * @description:
 * @author: zhengyunfei
 * @create: 2018-12-13 11:05
 **/
public class LoginLog implements java.io.Serializable{
    private String pkId;
    private String phoneNumber;
    private String logonDate;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLogonDate() {
        return logonDate;
    }

    public void setLogonDate(String logonDate) {
        this.logonDate = logonDate;
    }
}

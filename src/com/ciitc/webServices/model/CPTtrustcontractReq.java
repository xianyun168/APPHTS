package com.ciitc.webServices.model;

import java.math.BigDecimal;
import java.util.Date;

public class CPTtrustcontractReq {
    private Long lSerialno;

    private Long lProserialno;
    
    private String proserialno;

    private String cTrustcontractid;

    private String cFundcode;

    private String cProfitclass;

    private String cSharetype;

    private String cDivflag;

    private String cCustno;

    private double fContractsignbalance;

    private String cEmail;

    private String cMobileno;

    private String cAddress;

    private String cZipcode;

    private Date dAuditdate;

    private Date dContractsigndate;

    private String cStatus;

    private String cCustfrom;

    private String cFundacco;

    private String cCheckor;

    private String cDealer;

    private String cBrokeraccount;

    private String cCreator;

    private Date dCreatedate;

    private String cIscomity;

    private String cPhone;

    private String cAuditoption;

    private String cFaxno;

    private String cAgencyno;

    private String cProcustfrom;

    private Short lRejectcount;

    private String cRecommender;

    private String cChannelno;

    private String cPaystatus;

    private BigDecimal fTotalpaybalance;

    private String cEastcusttype;

    private String cEastcontractcusttype;

    private String cEastcosttype;

    private BigDecimal fEastcostvalue;

    private String cBusinflag;

    private String cEastareatype;

    private String cEastareaname;

    private String cEastproductsalemode;

    private String cEastregistrationgbcode;

    private String cIscompanypay;

    private String cAntimoneyflag;

    private String cDeletememo;

    private String cContact;

    private String cContype;

    private String cContno;

    private Date dConidvaliddate;

    private Date dConidvaliddatebeg;

    private String cChannelsource;

    private String cMemo;

    private String cBuytype;

    private Long lProfitid;

    private String cDeptnodecode;

    private String cBusinflagxc;

    private BigDecimal fTempTotalbalance;

    private String cCustomerarea;

    private String cBenefittype;

    private String cControlperson;

    private String cBenefitperson;

    private String cBranchno;

    private Date dContractenddate;

    private String cIsproinvestor;

    private String cIsdefaultcontact;

    private String cIsupdatecontact;

    private String cVideostatus;

    private String cIssendcontract;

    private String cDealstatus;

    private Date dLastmodifydate;

    private String cPaperauditstatus;

    private String cPapercontractid;

    private Date dFundopendate;

    private String cIssendsystem;

    private String cIsvideoaudio;

    private Date dVideoaudiodata;

    private String cCicleesubscription;

    private String cSigenaddress;

    private String cMaterialauditstauts;

    private String cMaterialauditoption;

    private String cWealthsource;

    private String cIsinvestsale;

    private BigDecimal fRedeemcontractbalance;

    private String cIslcontract;

    private String cHsbpminstanceid;

    private String cCapitallasttypedesc;

    private String cCashtype;

    private Long lContracttime;

    private String cPagefrom;

    private String cDirectpost;

    private String cOutsysid;

    private BigDecimal fYield;
    
    //产品名称
  	private String productName;
  	
  	 //业绩比较基准
  	private String benchmark;
    
  	
  	//距离到期天数
    private String daoqiTian;

    //份额确认日
    private String querenDate;
    
    //确认份额
    private int fene;
    
    //购买产品状态
    private String zhuangtai;
    
    //签署日期
    private String qianshu;
    
    //现金流金额
  	private double Amount;
  	
  	
  	//客户实际赎回时间
  	private Date actualDate;

  	//客户默认结束时间
  	private Date defaultDate;
  
  	//提前赎回时间
  	private String tiqianDate;
  	
  	private String money;
  	private String spciTye;

    public String getSpciTye() {
        return spciTye;
    }

    public void setSpciTye(String spciTye) {
        this.spciTye = spciTye;
    }

    public String getProserialno() {
		return proserialno;
	}

	public void setProserialno(String proserialno) {
		this.proserialno = proserialno;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getTiqianDate() {
		return tiqianDate;
	}

	public void setTiqianDate(String tiqianDate) {
		this.tiqianDate = tiqianDate;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public String getQianshu() {
		return qianshu;
	}

	public void setQianshu(String qianshu) {
		this.qianshu = qianshu;
	}

	public String getDaoqiTian() {
		return daoqiTian;
	}

	public void setDaoqiTian(String daoqiTian) {
		this.daoqiTian = daoqiTian;
	}

	public String getQuerenDate() {
		return querenDate;
	}

	public void setQuerenDate(String querenDate) {
		this.querenDate = querenDate;
	}

	public int getFene() {
		return fene;
	}

	public void setFene(int fene) {
		this.fene = fene;
	}

	public String getZhuangtai() {
		return zhuangtai;
	}

	public void setZhuangtai(String zhuangtai) {
		this.zhuangtai = zhuangtai;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getBenchmark() {
		return benchmark;
	}

	public void setBenchmark(String benchmark) {
		this.benchmark = benchmark;
	}

	public Long getlSerialno() {
        return lSerialno;
    }

    public void setlSerialno(Long lSerialno) {
        this.lSerialno = lSerialno;
    }

    public Long getlProserialno() {
        return lProserialno;
    }

    public void setlProserialno(Long lProserialno) {
        this.lProserialno = lProserialno;
    }

    public String getcTrustcontractid() {
        return cTrustcontractid;
    }

    public void setcTrustcontractid(String cTrustcontractid) {
        this.cTrustcontractid = cTrustcontractid == null ? null : cTrustcontractid.trim();
    }

    public String getcFundcode() {
        return cFundcode;
    }

    public void setcFundcode(String cFundcode) {
        this.cFundcode = cFundcode == null ? null : cFundcode.trim();
    }

    public String getcProfitclass() {
        return cProfitclass;
    }

    public void setcProfitclass(String cProfitclass) {
        this.cProfitclass = cProfitclass == null ? null : cProfitclass.trim();
    }

    public String getcSharetype() {
        return cSharetype;
    }

    public void setcSharetype(String cSharetype) {
        this.cSharetype = cSharetype == null ? null : cSharetype.trim();
    }

    public String getcDivflag() {
        return cDivflag;
    }

    public void setcDivflag(String cDivflag) {
        this.cDivflag = cDivflag == null ? null : cDivflag.trim();
    }

    public String getcCustno() {
        return cCustno;
    }

    public void setcCustno(String cCustno) {
        this.cCustno = cCustno == null ? null : cCustno.trim();
    }

    public double getfContractsignbalance() {
        return fContractsignbalance;
    }

    public void setfContractsignbalance(double fContractsignbalance) {
        this.fContractsignbalance = fContractsignbalance;
    }

    public String getcEmail() {
        return cEmail;
    }

    public void setcEmail(String cEmail) {
        this.cEmail = cEmail == null ? null : cEmail.trim();
    }

    public String getcMobileno() {
        return cMobileno;
    }

    public void setcMobileno(String cMobileno) {
        this.cMobileno = cMobileno == null ? null : cMobileno.trim();
    }

    public String getcAddress() {
        return cAddress;
    }

    public void setcAddress(String cAddress) {
        this.cAddress = cAddress == null ? null : cAddress.trim();
    }

    public String getcZipcode() {
        return cZipcode;
    }

    public void setcZipcode(String cZipcode) {
        this.cZipcode = cZipcode == null ? null : cZipcode.trim();
    }

    public Date getdAuditdate() {
        return dAuditdate;
    }

    public void setdAuditdate(Date dAuditdate) {
        this.dAuditdate = dAuditdate;
    }

    public Date getdContractsigndate() {
        return dContractsigndate;
    }

    public void setdContractsigndate(Date dContractsigndate) {
        this.dContractsigndate = dContractsigndate;
    }

    public String getcStatus() {
        return cStatus;
    }

    public void setcStatus(String cStatus) {
        this.cStatus = cStatus == null ? null : cStatus.trim();
    }

    public String getcCustfrom() {
        return cCustfrom;
    }

    public void setcCustfrom(String cCustfrom) {
        this.cCustfrom = cCustfrom == null ? null : cCustfrom.trim();
    }

    public String getcFundacco() {
        return cFundacco;
    }

    public void setcFundacco(String cFundacco) {
        this.cFundacco = cFundacco == null ? null : cFundacco.trim();
    }

    public String getcCheckor() {
        return cCheckor;
    }

    public void setcCheckor(String cCheckor) {
        this.cCheckor = cCheckor == null ? null : cCheckor.trim();
    }

    public String getcDealer() {
        return cDealer;
    }

    public void setcDealer(String cDealer) {
        this.cDealer = cDealer == null ? null : cDealer.trim();
    }

    public String getcBrokeraccount() {
        return cBrokeraccount;
    }

    public void setcBrokeraccount(String cBrokeraccount) {
        this.cBrokeraccount = cBrokeraccount == null ? null : cBrokeraccount.trim();
    }

    public String getcCreator() {
        return cCreator;
    }

    public void setcCreator(String cCreator) {
        this.cCreator = cCreator == null ? null : cCreator.trim();
    }

    public Date getdCreatedate() {
        return dCreatedate;
    }

    public void setdCreatedate(Date dCreatedate) {
        this.dCreatedate = dCreatedate;
    }

    public String getcIscomity() {
        return cIscomity;
    }

    public void setcIscomity(String cIscomity) {
        this.cIscomity = cIscomity == null ? null : cIscomity.trim();
    }

    public String getcPhone() {
        return cPhone;
    }

    public void setcPhone(String cPhone) {
        this.cPhone = cPhone == null ? null : cPhone.trim();
    }

    public String getcAuditoption() {
        return cAuditoption;
    }

    public void setcAuditoption(String cAuditoption) {
        this.cAuditoption = cAuditoption == null ? null : cAuditoption.trim();
    }

    public String getcFaxno() {
        return cFaxno;
    }

    public void setcFaxno(String cFaxno) {
        this.cFaxno = cFaxno == null ? null : cFaxno.trim();
    }

    public String getcAgencyno() {
        return cAgencyno;
    }

    public void setcAgencyno(String cAgencyno) {
        this.cAgencyno = cAgencyno == null ? null : cAgencyno.trim();
    }

    public String getcProcustfrom() {
        return cProcustfrom;
    }

    public void setcProcustfrom(String cProcustfrom) {
        this.cProcustfrom = cProcustfrom == null ? null : cProcustfrom.trim();
    }

    public Short getlRejectcount() {
        return lRejectcount;
    }

    public void setlRejectcount(Short lRejectcount) {
        this.lRejectcount = lRejectcount;
    }

    public String getcRecommender() {
        return cRecommender;
    }

    public void setcRecommender(String cRecommender) {
        this.cRecommender = cRecommender == null ? null : cRecommender.trim();
    }

    public String getcChannelno() {
        return cChannelno;
    }

    public void setcChannelno(String cChannelno) {
        this.cChannelno = cChannelno == null ? null : cChannelno.trim();
    }

    public String getcPaystatus() {
        return cPaystatus;
    }

    public void setcPaystatus(String cPaystatus) {
        this.cPaystatus = cPaystatus == null ? null : cPaystatus.trim();
    }

    public BigDecimal getfTotalpaybalance() {
        return fTotalpaybalance;
    }

    public void setfTotalpaybalance(BigDecimal fTotalpaybalance) {
        this.fTotalpaybalance = fTotalpaybalance;
    }

    public String getcEastcusttype() {
        return cEastcusttype;
    }

    public void setcEastcusttype(String cEastcusttype) {
        this.cEastcusttype = cEastcusttype == null ? null : cEastcusttype.trim();
    }

    public String getcEastcontractcusttype() {
        return cEastcontractcusttype;
    }

    public void setcEastcontractcusttype(String cEastcontractcusttype) {
        this.cEastcontractcusttype = cEastcontractcusttype == null ? null : cEastcontractcusttype.trim();
    }

    public String getcEastcosttype() {
        return cEastcosttype;
    }

    public void setcEastcosttype(String cEastcosttype) {
        this.cEastcosttype = cEastcosttype == null ? null : cEastcosttype.trim();
    }

    public BigDecimal getfEastcostvalue() {
        return fEastcostvalue;
    }

    public void setfEastcostvalue(BigDecimal fEastcostvalue) {
        this.fEastcostvalue = fEastcostvalue;
    }

    public String getcBusinflag() {
        return cBusinflag;
    }

    public void setcBusinflag(String cBusinflag) {
        this.cBusinflag = cBusinflag == null ? null : cBusinflag.trim();
    }

    public String getcEastareatype() {
        return cEastareatype;
    }

    public void setcEastareatype(String cEastareatype) {
        this.cEastareatype = cEastareatype == null ? null : cEastareatype.trim();
    }

    public String getcEastareaname() {
        return cEastareaname;
    }

    public void setcEastareaname(String cEastareaname) {
        this.cEastareaname = cEastareaname == null ? null : cEastareaname.trim();
    }

    public String getcEastproductsalemode() {
        return cEastproductsalemode;
    }

    public void setcEastproductsalemode(String cEastproductsalemode) {
        this.cEastproductsalemode = cEastproductsalemode == null ? null : cEastproductsalemode.trim();
    }

    public String getcEastregistrationgbcode() {
        return cEastregistrationgbcode;
    }

    public void setcEastregistrationgbcode(String cEastregistrationgbcode) {
        this.cEastregistrationgbcode = cEastregistrationgbcode == null ? null : cEastregistrationgbcode.trim();
    }

    public String getcIscompanypay() {
        return cIscompanypay;
    }

    public void setcIscompanypay(String cIscompanypay) {
        this.cIscompanypay = cIscompanypay == null ? null : cIscompanypay.trim();
    }

    public String getcAntimoneyflag() {
        return cAntimoneyflag;
    }

    public void setcAntimoneyflag(String cAntimoneyflag) {
        this.cAntimoneyflag = cAntimoneyflag == null ? null : cAntimoneyflag.trim();
    }

    public String getcDeletememo() {
        return cDeletememo;
    }

    public void setcDeletememo(String cDeletememo) {
        this.cDeletememo = cDeletememo == null ? null : cDeletememo.trim();
    }

    public String getcContact() {
        return cContact;
    }

    public void setcContact(String cContact) {
        this.cContact = cContact == null ? null : cContact.trim();
    }

    public String getcContype() {
        return cContype;
    }

    public void setcContype(String cContype) {
        this.cContype = cContype == null ? null : cContype.trim();
    }

    public String getcContno() {
        return cContno;
    }

    public void setcContno(String cContno) {
        this.cContno = cContno == null ? null : cContno.trim();
    }

    public Date getdConidvaliddate() {
        return dConidvaliddate;
    }

    public void setdConidvaliddate(Date dConidvaliddate) {
        this.dConidvaliddate = dConidvaliddate;
    }

    public Date getdConidvaliddatebeg() {
        return dConidvaliddatebeg;
    }

    public void setdConidvaliddatebeg(Date dConidvaliddatebeg) {
        this.dConidvaliddatebeg = dConidvaliddatebeg;
    }

    public String getcChannelsource() {
        return cChannelsource;
    }

    public void setcChannelsource(String cChannelsource) {
        this.cChannelsource = cChannelsource == null ? null : cChannelsource.trim();
    }

    public String getcMemo() {
        return cMemo;
    }

    public void setcMemo(String cMemo) {
        this.cMemo = cMemo == null ? null : cMemo.trim();
    }

    public String getcBuytype() {
        return cBuytype;
    }

    public void setcBuytype(String cBuytype) {
        this.cBuytype = cBuytype == null ? null : cBuytype.trim();
    }

    public Long getlProfitid() {
        return lProfitid;
    }

    public void setlProfitid(Long lProfitid) {
        this.lProfitid = lProfitid;
    }

    public String getcDeptnodecode() {
        return cDeptnodecode;
    }

    public void setcDeptnodecode(String cDeptnodecode) {
        this.cDeptnodecode = cDeptnodecode == null ? null : cDeptnodecode.trim();
    }

    public String getcBusinflagxc() {
        return cBusinflagxc;
    }

    public void setcBusinflagxc(String cBusinflagxc) {
        this.cBusinflagxc = cBusinflagxc == null ? null : cBusinflagxc.trim();
    }

    public BigDecimal getfTempTotalbalance() {
        return fTempTotalbalance;
    }

    public void setfTempTotalbalance(BigDecimal fTempTotalbalance) {
        this.fTempTotalbalance = fTempTotalbalance;
    }

    public String getcCustomerarea() {
        return cCustomerarea;
    }

    public void setcCustomerarea(String cCustomerarea) {
        this.cCustomerarea = cCustomerarea == null ? null : cCustomerarea.trim();
    }

    public String getcBenefittype() {
        return cBenefittype;
    }

    public void setcBenefittype(String cBenefittype) {
        this.cBenefittype = cBenefittype == null ? null : cBenefittype.trim();
    }

    public String getcControlperson() {
        return cControlperson;
    }

    public void setcControlperson(String cControlperson) {
        this.cControlperson = cControlperson == null ? null : cControlperson.trim();
    }

    public String getcBenefitperson() {
        return cBenefitperson;
    }

    public void setcBenefitperson(String cBenefitperson) {
        this.cBenefitperson = cBenefitperson == null ? null : cBenefitperson.trim();
    }

    public String getcBranchno() {
        return cBranchno;
    }

    public void setcBranchno(String cBranchno) {
        this.cBranchno = cBranchno == null ? null : cBranchno.trim();
    }

    public Date getdContractenddate() {
        return dContractenddate;
    }

    public void setdContractenddate(Date dContractenddate) {
        this.dContractenddate = dContractenddate;
    }

    public String getcIsproinvestor() {
        return cIsproinvestor;
    }

    public void setcIsproinvestor(String cIsproinvestor) {
        this.cIsproinvestor = cIsproinvestor == null ? null : cIsproinvestor.trim();
    }

    public String getcIsdefaultcontact() {
        return cIsdefaultcontact;
    }

    public void setcIsdefaultcontact(String cIsdefaultcontact) {
        this.cIsdefaultcontact = cIsdefaultcontact == null ? null : cIsdefaultcontact.trim();
    }

    public String getcIsupdatecontact() {
        return cIsupdatecontact;
    }

    public void setcIsupdatecontact(String cIsupdatecontact) {
        this.cIsupdatecontact = cIsupdatecontact == null ? null : cIsupdatecontact.trim();
    }

    public String getcVideostatus() {
        return cVideostatus;
    }

    public void setcVideostatus(String cVideostatus) {
        this.cVideostatus = cVideostatus == null ? null : cVideostatus.trim();
    }

    public String getcIssendcontract() {
        return cIssendcontract;
    }

    public void setcIssendcontract(String cIssendcontract) {
        this.cIssendcontract = cIssendcontract == null ? null : cIssendcontract.trim();
    }

    public String getcDealstatus() {
        return cDealstatus;
    }

    public void setcDealstatus(String cDealstatus) {
        this.cDealstatus = cDealstatus == null ? null : cDealstatus.trim();
    }

    public Date getdLastmodifydate() {
        return dLastmodifydate;
    }

    public void setdLastmodifydate(Date dLastmodifydate) {
        this.dLastmodifydate = dLastmodifydate;
    }

    public String getcPaperauditstatus() {
        return cPaperauditstatus;
    }

    public void setcPaperauditstatus(String cPaperauditstatus) {
        this.cPaperauditstatus = cPaperauditstatus == null ? null : cPaperauditstatus.trim();
    }

    public String getcPapercontractid() {
        return cPapercontractid;
    }

    public void setcPapercontractid(String cPapercontractid) {
        this.cPapercontractid = cPapercontractid == null ? null : cPapercontractid.trim();
    }

    public Date getdFundopendate() {
        return dFundopendate;
    }

    public void setdFundopendate(Date dFundopendate) {
        this.dFundopendate = dFundopendate;
    }

    public String getcIssendsystem() {
        return cIssendsystem;
    }

    public void setcIssendsystem(String cIssendsystem) {
        this.cIssendsystem = cIssendsystem == null ? null : cIssendsystem.trim();
    }

    public String getcIsvideoaudio() {
        return cIsvideoaudio;
    }

    public void setcIsvideoaudio(String cIsvideoaudio) {
        this.cIsvideoaudio = cIsvideoaudio == null ? null : cIsvideoaudio.trim();
    }

    public Date getdVideoaudiodata() {
        return dVideoaudiodata;
    }

    public void setdVideoaudiodata(Date dVideoaudiodata) {
        this.dVideoaudiodata = dVideoaudiodata;
    }

    public String getcCicleesubscription() {
        return cCicleesubscription;
    }

    public void setcCicleesubscription(String cCicleesubscription) {
        this.cCicleesubscription = cCicleesubscription == null ? null : cCicleesubscription.trim();
    }

    public String getcSigenaddress() {
        return cSigenaddress;
    }

    public void setcSigenaddress(String cSigenaddress) {
        this.cSigenaddress = cSigenaddress == null ? null : cSigenaddress.trim();
    }

    public String getcMaterialauditstauts() {
        return cMaterialauditstauts;
    }

    public void setcMaterialauditstauts(String cMaterialauditstauts) {
        this.cMaterialauditstauts = cMaterialauditstauts == null ? null : cMaterialauditstauts.trim();
    }

    public String getcMaterialauditoption() {
        return cMaterialauditoption;
    }

    public void setcMaterialauditoption(String cMaterialauditoption) {
        this.cMaterialauditoption = cMaterialauditoption == null ? null : cMaterialauditoption.trim();
    }

    public String getcWealthsource() {
        return cWealthsource;
    }

    public void setcWealthsource(String cWealthsource) {
        this.cWealthsource = cWealthsource == null ? null : cWealthsource.trim();
    }

    public String getcIsinvestsale() {
        return cIsinvestsale;
    }

    public void setcIsinvestsale(String cIsinvestsale) {
        this.cIsinvestsale = cIsinvestsale == null ? null : cIsinvestsale.trim();
    }

    public BigDecimal getfRedeemcontractbalance() {
        return fRedeemcontractbalance;
    }

    public void setfRedeemcontractbalance(BigDecimal fRedeemcontractbalance) {
        this.fRedeemcontractbalance = fRedeemcontractbalance;
    }

    public String getcIslcontract() {
        return cIslcontract;
    }

    public void setcIslcontract(String cIslcontract) {
        this.cIslcontract = cIslcontract == null ? null : cIslcontract.trim();
    }

    public String getcHsbpminstanceid() {
        return cHsbpminstanceid;
    }

    public void setcHsbpminstanceid(String cHsbpminstanceid) {
        this.cHsbpminstanceid = cHsbpminstanceid == null ? null : cHsbpminstanceid.trim();
    }

    public String getcCapitallasttypedesc() {
        return cCapitallasttypedesc;
    }

    public void setcCapitallasttypedesc(String cCapitallasttypedesc) {
        this.cCapitallasttypedesc = cCapitallasttypedesc == null ? null : cCapitallasttypedesc.trim();
    }

    public String getcCashtype() {
        return cCashtype;
    }

    public void setcCashtype(String cCashtype) {
        this.cCashtype = cCashtype == null ? null : cCashtype.trim();
    }

    public Long getlContracttime() {
        return lContracttime;
    }

    public void setlContracttime(Long lContracttime) {
        this.lContracttime = lContracttime;
    }

    public String getcPagefrom() {
        return cPagefrom;
    }

    public void setcPagefrom(String cPagefrom) {
        this.cPagefrom = cPagefrom == null ? null : cPagefrom.trim();
    }

    public String getcDirectpost() {
        return cDirectpost;
    }

    public void setcDirectpost(String cDirectpost) {
        this.cDirectpost = cDirectpost == null ? null : cDirectpost.trim();
    }

    public String getcOutsysid() {
        return cOutsysid;
    }

    public void setcOutsysid(String cOutsysid) {
        this.cOutsysid = cOutsysid == null ? null : cOutsysid.trim();
    }

    public BigDecimal getfYield() {
        return fYield;
    }

    public void setfYield(BigDecimal fYield) {
        this.fYield = fYield;
    }

	public Date getActualDate() {
		return actualDate;
	}

	public void setActualDate(Date actualDate) {
		this.actualDate = actualDate;
	}

	public Date getDefaultDate() {
		return defaultDate;
	}

	public void setDefaultDate(Date defaultDate) {
		this.defaultDate = defaultDate;
	}
}
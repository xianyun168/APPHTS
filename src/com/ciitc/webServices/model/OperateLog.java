package com.ciitc.webServices.model;

import java.util.Date;

public class OperateLog  {

    //主键
    private  String pkId;
    //操作模块
    private  String operateMode;
    //操作人
    private  String userPkId;
    //操作人名称
    private  String userName;
    //操作类型
    private  String operateType;
    //操作时间
    private  Date operateTime;
    //操作轨迹
    private  String operateTrack;
    //标志位
    private  String flag;
    //备注
    private  String remark;

	 public String getPkId() {
		return this.pkId;
	 }
	
	 public void setPkId(String pkId) {
		this.pkId = pkId;
	 }
	 
	 public String getOperateMode() {
		return this.operateMode;
	 }
	
	 public void setOperateMode(String operateMode) {
		this.operateMode = operateMode;
	 }
	 
	 public String getUserPkId() {
		return this.userPkId;
	 }
	
	 public void setUserPkId(String userPkId) {
		this.userPkId = userPkId;
	 }
	 
	 public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getOperateType() {
		return this.operateType;
	 }
	
	 public void setOperateType(String operateType) {
		this.operateType = operateType;
	 }
	 
	 public Date getOperateTime() {
		return this.operateTime;
	 }
	
	 public void setOperateTime(Date operateTime) {
		this.operateTime = operateTime;
	 }
	 
	 public String getOperateTrack() {
		return this.operateTrack;
	 }
	
	 public void setOperateTrack(String operateTrack) {
		this.operateTrack = operateTrack;
	 }
	 
	 public String getFlag() {
		return this.flag;
	 }
	
	 public void setFlag(String flag) {
		this.flag = flag;
	 }
	 
	 public String getRemark() {
		return this.remark;
	 }
	
	 public void setRemark(String remark) {
		this.remark = remark;
	 }
	 
 }
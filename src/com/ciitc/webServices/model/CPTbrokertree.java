package com.ciitc.webServices.model;

import java.math.BigDecimal;
import java.util.Date;

public class CPTbrokertree {
    private String cNodecode;

    private String cParentcode;

    private String cNodetype;

    private String cNodevalue;

    private String cDesc;

    private String cBranch;

    private String cAreacode;

    private String cAreaexcept;

    private String cDepttype;

    private String cDefaultbroker;

    private String cLimitagencyno;

    private String cLeaf;

    private String cIsreserve;

    private String cAddress;

    private Date dLastmodify;

    private String cIsuse;

    private String cNetnotemp;

    private String cUfaccosysnetno;

    private String cBranchno;

    private String cDeptplacetype;

    private String cBranchid;

    private String cParentbranchid;

    private String cIssalary;

    private String cIsreservecount;

    private Date dStartdate;

    private String cDsnodecode;

    private String cOrder;

    private String cTenantid;

    private String cIsrealdepartment;

    private BigDecimal fRatio;

    public String getcNodecode() {
        return cNodecode;
    }

    public void setcNodecode(String cNodecode) {
        this.cNodecode = cNodecode == null ? null : cNodecode.trim();
    }

    public String getcParentcode() {
        return cParentcode;
    }

    public void setcParentcode(String cParentcode) {
        this.cParentcode = cParentcode == null ? null : cParentcode.trim();
    }

    public String getcNodetype() {
        return cNodetype;
    }

    public void setcNodetype(String cNodetype) {
        this.cNodetype = cNodetype == null ? null : cNodetype.trim();
    }

    public String getcNodevalue() {
        return cNodevalue;
    }

    public void setcNodevalue(String cNodevalue) {
        this.cNodevalue = cNodevalue == null ? null : cNodevalue.trim();
    }

    public String getcDesc() {
        return cDesc;
    }

    public void setcDesc(String cDesc) {
        this.cDesc = cDesc == null ? null : cDesc.trim();
    }

    public String getcBranch() {
        return cBranch;
    }

    public void setcBranch(String cBranch) {
        this.cBranch = cBranch == null ? null : cBranch.trim();
    }

    public String getcAreacode() {
        return cAreacode;
    }

    public void setcAreacode(String cAreacode) {
        this.cAreacode = cAreacode == null ? null : cAreacode.trim();
    }

    public String getcAreaexcept() {
        return cAreaexcept;
    }

    public void setcAreaexcept(String cAreaexcept) {
        this.cAreaexcept = cAreaexcept == null ? null : cAreaexcept.trim();
    }

    public String getcDepttype() {
        return cDepttype;
    }

    public void setcDepttype(String cDepttype) {
        this.cDepttype = cDepttype == null ? null : cDepttype.trim();
    }

    public String getcDefaultbroker() {
        return cDefaultbroker;
    }

    public void setcDefaultbroker(String cDefaultbroker) {
        this.cDefaultbroker = cDefaultbroker == null ? null : cDefaultbroker.trim();
    }

    public String getcLimitagencyno() {
        return cLimitagencyno;
    }

    public void setcLimitagencyno(String cLimitagencyno) {
        this.cLimitagencyno = cLimitagencyno == null ? null : cLimitagencyno.trim();
    }

    public String getcLeaf() {
        return cLeaf;
    }

    public void setcLeaf(String cLeaf) {
        this.cLeaf = cLeaf == null ? null : cLeaf.trim();
    }

    public String getcIsreserve() {
        return cIsreserve;
    }

    public void setcIsreserve(String cIsreserve) {
        this.cIsreserve = cIsreserve == null ? null : cIsreserve.trim();
    }

    public String getcAddress() {
        return cAddress;
    }

    public void setcAddress(String cAddress) {
        this.cAddress = cAddress == null ? null : cAddress.trim();
    }

    public Date getdLastmodify() {
        return dLastmodify;
    }

    public void setdLastmodify(Date dLastmodify) {
        this.dLastmodify = dLastmodify;
    }

    public String getcIsuse() {
        return cIsuse;
    }

    public void setcIsuse(String cIsuse) {
        this.cIsuse = cIsuse == null ? null : cIsuse.trim();
    }

    public String getcNetnotemp() {
        return cNetnotemp;
    }

    public void setcNetnotemp(String cNetnotemp) {
        this.cNetnotemp = cNetnotemp == null ? null : cNetnotemp.trim();
    }

    public String getcUfaccosysnetno() {
        return cUfaccosysnetno;
    }

    public void setcUfaccosysnetno(String cUfaccosysnetno) {
        this.cUfaccosysnetno = cUfaccosysnetno == null ? null : cUfaccosysnetno.trim();
    }

    public String getcBranchno() {
        return cBranchno;
    }

    public void setcBranchno(String cBranchno) {
        this.cBranchno = cBranchno == null ? null : cBranchno.trim();
    }

    public String getcDeptplacetype() {
        return cDeptplacetype;
    }

    public void setcDeptplacetype(String cDeptplacetype) {
        this.cDeptplacetype = cDeptplacetype == null ? null : cDeptplacetype.trim();
    }

    public String getcBranchid() {
        return cBranchid;
    }

    public void setcBranchid(String cBranchid) {
        this.cBranchid = cBranchid == null ? null : cBranchid.trim();
    }

    public String getcParentbranchid() {
        return cParentbranchid;
    }

    public void setcParentbranchid(String cParentbranchid) {
        this.cParentbranchid = cParentbranchid == null ? null : cParentbranchid.trim();
    }

    public String getcIssalary() {
        return cIssalary;
    }

    public void setcIssalary(String cIssalary) {
        this.cIssalary = cIssalary == null ? null : cIssalary.trim();
    }

    public String getcIsreservecount() {
        return cIsreservecount;
    }

    public void setcIsreservecount(String cIsreservecount) {
        this.cIsreservecount = cIsreservecount == null ? null : cIsreservecount.trim();
    }

    public Date getdStartdate() {
        return dStartdate;
    }

    public void setdStartdate(Date dStartdate) {
        this.dStartdate = dStartdate;
    }

    public String getcDsnodecode() {
        return cDsnodecode;
    }

    public void setcDsnodecode(String cDsnodecode) {
        this.cDsnodecode = cDsnodecode == null ? null : cDsnodecode.trim();
    }

    public String getcOrder() {
        return cOrder;
    }

    public void setcOrder(String cOrder) {
        this.cOrder = cOrder == null ? null : cOrder.trim();
    }

    public String getcTenantid() {
        return cTenantid;
    }

    public void setcTenantid(String cTenantid) {
        this.cTenantid = cTenantid == null ? null : cTenantid.trim();
    }

    public String getcIsrealdepartment() {
        return cIsrealdepartment;
    }

    public void setcIsrealdepartment(String cIsrealdepartment) {
        this.cIsrealdepartment = cIsrealdepartment == null ? null : cIsrealdepartment.trim();
    }

    public BigDecimal getfRatio() {
        return fRatio;
    }

    public void setfRatio(BigDecimal fRatio) {
        this.fRatio = fRatio;
    }
}
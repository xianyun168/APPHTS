package com.ciitc.webServices.model;

public class CPReservation {
	//主键
    private String pkId;
    //客户编号
    private String pCustno;
    //营销项目编号
    private Integer pProserialno;
    //产品编号
    private String pFundcode;
    //预约金额
    private Double pBalance;
    //预约结束日期
    private String pExpirationdate;
    //理财师编号
    private String pBrokeraccount;

    private String remark1;

    private String remark2;

    private String remark3;
    //预计缴款日期
    private String pRemitdate;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getpCustno() {
        return pCustno;
    }

    public void setpCustno(String pCustno) {
        this.pCustno = pCustno == null ? null : pCustno.trim();
    }

    public Integer getpProserialno() {
		return pProserialno;
	}

	public void setpProserialno(Integer pProserialno) {
		this.pProserialno = pProserialno;
	}

	public String getpFundcode() {
        return pFundcode;
    }

    public void setpFundcode(String pFundcode) {
        this.pFundcode = pFundcode == null ? null : pFundcode.trim();
    }

    

    public Double getpBalance() {
		return pBalance;
	}

	public void setpBalance(Double pBalance) {
		this.pBalance = pBalance;
	}

	public String getpExpirationdate() {
        return pExpirationdate;
    }

    public void setpExpirationdate(String pExpirationdate) {
        this.pExpirationdate = pExpirationdate == null ? null : pExpirationdate.trim();
    }

    public String getpBrokeraccount() {
        return pBrokeraccount;
    }

    public void setpBrokeraccount(String pBrokeraccount) {
        this.pBrokeraccount = pBrokeraccount == null ? null : pBrokeraccount.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    public String getpRemitdate() {
        return pRemitdate;
    }

    public void setpRemitdate(String pRemitdate) {
        this.pRemitdate = pRemitdate == null ? null : pRemitdate.trim();
    }
}
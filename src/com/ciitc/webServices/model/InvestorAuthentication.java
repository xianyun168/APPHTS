package com.ciitc.webServices.model;

public class InvestorAuthentication {
	
	private String p_custno;
	
	private String p_provetype;
	
	private String p_provebalance;
	
	private String financialId;
	
	public String getFinancialId() {
		return financialId;
	}

	public void setFinancialId(String financialId) {
		this.financialId = financialId;
	}

	public String getP_custno() {
		return p_custno;
	}

	public void setP_custno(String p_custno) {
		this.p_custno = p_custno;
	}

	public String getP_provetype() {
		return p_provetype;
	}

	public void setP_provetype(String p_provetype) {
		this.p_provetype = p_provetype;
	}

	public String getP_provebalance() {
		return p_provebalance;
	}

	public void setP_provebalance(String p_provebalance) {
		this.p_provebalance = p_provebalance;
	}

	
}

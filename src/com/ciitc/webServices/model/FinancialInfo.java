package com.ciitc.webServices.model;

public class FinancialInfo {
    private String pkId;

    private String financialName;

    private String financialPhoneNumber;

    private String rank;

    private String remark1;

    private String remark2;

    private String remark3;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getFinancialName() {
        return financialName;
    }

    public void setFinancialName(String financialName) {
        this.financialName = financialName == null ? null : financialName.trim();
    }

    public String getFinancialPhoneNumber() {
        return financialPhoneNumber;
    }

    public void setFinancialPhoneNumber(String financialPhoneNumber) {
        this.financialPhoneNumber = financialPhoneNumber == null ? null : financialPhoneNumber.trim();
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank == null ? null : rank.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
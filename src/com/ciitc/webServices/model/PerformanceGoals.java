package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 业绩目标管理表
 * @author 任之阳
 *
 */
public class PerformanceGoals {
     //主键
	private String pkId;
	
	 //职级
	private String rank;
	
	 //业绩目标
	private double target;
	
	 //业绩考核开始时间
	private Date assessmentStartTime;
	
	 //业绩考核结束时间
	private Date assessmentEndTime;
	
	//备用字段1
	private String remark1;
	
	//备用字段2
	private String remark2;
	
	//备用字段3
	private String remark3;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public double getTarget() {
		return target;
	}

	public void setTarget(double target) {
		this.target = target;
	}

	public Date getAssessmentStartTime() {
		return assessmentStartTime;
	}

	public void setAssessmentStartTime(Date assessmentStartTime) {
		this.assessmentStartTime = assessmentStartTime;
	}

	public Date getAssessmentEndTime() {
		return assessmentEndTime;
	}

	public void setAssessmentEndTime(Date assessmentEndTime) {
		this.assessmentEndTime = assessmentEndTime;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

}

package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 组织机构model
 */
public class Depart  {

    //主键
    private  String pkId;
    //上级组织机构主键
    private  String parentDepartId;
    //组织机构名称
    private  String departName;
    //对应组织机构级别
    private  String departGrade;
    //创建人
    private  String createdBy;
    //创建时间
    private  Date createdDate;
    //修改人
    private  String updatedBy;
    //修改时间
    private  Date updatedDate;
    //标志位
    private  String flag;
    //备注
    private  String remark;
    
	public String getPkId() {
		return pkId;
	}
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	public String getParentDepartId() {
		return parentDepartId;
	}
	public void setParentDepartId(String parentDepartId) {
		this.parentDepartId = parentDepartId;
	}
	public String getDepartName() {
		return departName;
	}
	public void setDepartName(String departName) {
		this.departName = departName;
	}
	public String getDepartGrade() {
		return departGrade;
	}
	public void setDepartGrade(String departGrade) {
		this.departGrade = departGrade;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

 }
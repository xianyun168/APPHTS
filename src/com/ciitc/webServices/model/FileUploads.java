package com.ciitc.webServices.model;

public class FileUploads {
    private String pkId;

    private String oldFileName;

    private String newFileName;

    private String uploadUrl;

    private String rmeark1;

    private String rmeark2;

    private String rmeark3;

    private String rmeark4;

    private String rmeark5;

    private String rmeark6;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getOldFileName() {
        return oldFileName;
    }

    public void setOldFileName(String oldFileName) {
        this.oldFileName = oldFileName == null ? null : oldFileName.trim();
    }

    public String getNewFileName() {
        return newFileName;
    }

    public void setNewFileName(String newFileName) {
        this.newFileName = newFileName == null ? null : newFileName.trim();
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl == null ? null : uploadUrl.trim();
    }

    public String getRmeark1() {
        return rmeark1;
    }

    public void setRmeark1(String rmeark1) {
        this.rmeark1 = rmeark1 == null ? null : rmeark1.trim();
    }

    public String getRmeark2() {
        return rmeark2;
    }

    public void setRmeark2(String rmeark2) {
        this.rmeark2 = rmeark2 == null ? null : rmeark2.trim();
    }

    public String getRmeark3() {
        return rmeark3;
    }

    public void setRmeark3(String rmeark3) {
        this.rmeark3 = rmeark3 == null ? null : rmeark3.trim();
    }

    public String getRmeark4() {
        return rmeark4;
    }

    public void setRmeark4(String rmeark4) {
        this.rmeark4 = rmeark4 == null ? null : rmeark4.trim();
    }

    public String getRmeark5() {
        return rmeark5;
    }

    public void setRmeark5(String rmeark5) {
        this.rmeark5 = rmeark5 == null ? null : rmeark5.trim();
    }

    public String getRmeark6() {
        return rmeark6;
    }

    public void setRmeark6(String rmeark6) {
        this.rmeark6 = rmeark6 == null ? null : rmeark6.trim();
    }
}
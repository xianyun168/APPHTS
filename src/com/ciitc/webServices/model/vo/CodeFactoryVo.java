package com.ciitc.webServices.model.vo;


/**
 *数据字典VO 
 */
public class CodeFactoryVo {
	
	   //代码中文名称
	   private  String chineseName;
	   //代码值
	    private  String codeValue;
	    //代码
	    private String codecode;
	    
		public String getChineseName() {
			return chineseName;
		}
		public void setChineseName(String chineseName) {
			this.chineseName = chineseName;
		}
		public String getCodeValue() {
			return codeValue;
		}
		public void setCodeValue(String codeValue) {
			this.codeValue = codeValue;
		}
		public String getCodecode() {
			return codecode;
		}
		public void setCodecode(String codecode) {
			this.codecode = codecode;
		}
	

}

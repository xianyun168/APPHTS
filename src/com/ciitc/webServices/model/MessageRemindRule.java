package com.ciitc.webServices.model;


public class MessageRemindRule {
    private String pkId;

    private String messageRemindRuleName;

    private Integer  days;

    private Double remainMoney;

    private String realTimeNotice;

    private String messageMould;
    
    private String state;

    private String remark1;
    
    private String flag;

    private String remark2;

    private String remark3;
    
    private String remindObject;
    

    public String getRemindObject() {
		return remindObject;
	}

	public void setRemindObject(String remindObject) {
		this.remindObject = remindObject;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

	public String getMessageRemindRuleName() {
		return messageRemindRuleName;
	}

	public void setMessageRemindRuleName(String messageRemindRuleName) {
		this.messageRemindRuleName = messageRemindRuleName;
	}
	
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	
	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Integer getDays() {
		return days;
	}

	public void setDays(Integer days) {
		this.days = days;
	}

	public Double getRemainMoney() {
		return remainMoney;
	}

	public void setRemainMoney(Double remainMoney) {
		this.remainMoney = remainMoney;
	}

	public String getRealTimeNotice() {
        return realTimeNotice;
    }

    public void setRealTimeNotice(String realTimeNotice) {
        this.realTimeNotice = realTimeNotice == null ? null : realTimeNotice.trim();
    }

    public String getMessageMould() {
        return messageMould;
    }

    public void setMessageMould(String messageMould) {
        this.messageMould = messageMould == null ? null : messageMould.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
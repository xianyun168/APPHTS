package com.ciitc.webServices.model;

import java.util.Date;

public class Contract {
	 private Integer p_proserialno;  //项目编号，
	 private String p_checkor;  //合同审核人，
	 private Integer p_contractbalance; //合同金额， 
	 private Date p_contractsigndate; //合同签署日期，
	 private Date p_fundopendate; //合同起息日期，
	 private String p_bankname; //开户行号
	 private String p_subbranchname; //开户支行，
	 private String p_bankacco; //开户账号,
	 private String p_reserveno; //预约编号,
	 private String p_nameinbank; //开户姓名
	 private String c_trustcontractid; //合同编号
	 private String c_fundcode;//产品编号
	 
	public String getC_fundcode() {
		return c_fundcode;
	}
	public void setC_fundcode(String c_fundcode) {
		this.c_fundcode = c_fundcode;
	}
	public String getC_trustcontractid() {
		return c_trustcontractid;
	}
	public void setC_trustcontractid(String c_trustcontractid) {
		this.c_trustcontractid = c_trustcontractid;
	}
	public Integer getP_proserialno() {
		return p_proserialno;
	}
	public void setP_proserialno(Integer p_proserialno) {
		this.p_proserialno = p_proserialno;
	}
	public String getP_checkor() {
		return p_checkor;
	}
	public void setP_checkor(String p_checkor) {
		this.p_checkor = p_checkor;
	}
	public Integer getP_contractbalance() {
		return p_contractbalance;
	}
	public void setP_contractbalance(Integer p_contractbalance) {
		this.p_contractbalance = p_contractbalance;
	}
	public Date getP_contractsigndate() {
		return p_contractsigndate;
	}
	public void setP_contractsigndate(Date p_contractsigndate) {
		this.p_contractsigndate = p_contractsigndate;
	}
	public Date getP_fundopendate() {
		return p_fundopendate;
	}
	public void setP_fundopendate(Date p_fundopendate) {
		this.p_fundopendate = p_fundopendate;
	}
	public String getP_bankname() {
		return p_bankname;
	}
	public void setP_bankname(String p_bankname) {
		this.p_bankname = p_bankname;
	}
	public String getP_subbranchname() {
		return p_subbranchname;
	}
	public void setP_subbranchname(String p_subbranchname) {
		this.p_subbranchname = p_subbranchname;
	}
	public String getP_bankacco() {
		return p_bankacco;
	}
	public void setP_bankacco(String p_bankacco) {
		this.p_bankacco = p_bankacco;
	}
	public String getP_reserveno() {
		return p_reserveno;
	}
	public void setP_reserveno(String p_reserveno) {
		this.p_reserveno = p_reserveno;
	}
	public String getP_nameinbank() {
		return p_nameinbank;
	}
	public void setP_nameinbank(String p_nameinbank) {
		this.p_nameinbank = p_nameinbank;
	}
	 

}

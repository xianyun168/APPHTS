package com.ciitc.webServices.model;

import java.util.Date;

/**
 * 折标系数表
 * @author 任之阳
 *
 */
public class Coefficient {

	 //主键
	private String pkId;
	
	 //产品Id
	private String productId;
	
     //折标系数
	private double coefficient;
	
	 //折标日期
	private Date scalinDate;
	
	 //备用字段1
	private String remark1;
		
	 //备用字段2 
	private String remark2;
		
	 //备用字段3
	private String remark3;

	public String getPkId() {
		return pkId;
	}

	public void setPkId(String pkId) {
		this.pkId = pkId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public double getCoefficient() {
		return coefficient;
	}

	public void setCoefficient(double coefficient) {
		this.coefficient = coefficient;
	}

	public Date getScalinDate() {
		return scalinDate;
	}

	public void setScalinDate(Date scalinDate) {
		this.scalinDate = scalinDate;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}
}

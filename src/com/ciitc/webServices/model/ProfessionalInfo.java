package com.ciitc.webServices.model;
/*******************************************************************************
 * 模块名称:  
 * 模块功能描述:  
 * 创 建 人: 
 * 日 期: 2018年5月24日 下午4:14:45
 * 修 改 人: 
 ******************************************************************************/
public class ProfessionalInfo {

	//客户编号
	private String p_custno;
	//证明类型
	private String p_provetype;
	//证明金额
	private String p_provebalance;
	//理财师ID
	private String financialId;
	public String getP_custno() {
		return p_custno;
	}
	public void setP_custno(String p_custno) {
		this.p_custno = p_custno;
	}
	public String getP_provetype() {
		return p_provetype;
	}
	public void setP_provetype(String p_provetype) {
		this.p_provetype = p_provetype;
	}
	public String getP_provebalance() {
		return p_provebalance;
	}
	public void setP_provebalance(String p_provebalance) {
		this.p_provebalance = p_provebalance;
	}
	public String getFinancialId() {
		return financialId;
	}
	public void setFinancialId(String financialId) {
		this.financialId = financialId;
	}
	
}

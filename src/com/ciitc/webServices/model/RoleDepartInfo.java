package com.ciitc.webServices.model;

import java.util.Date;

public class RoleDepartInfo {
	//主键
    private  String pkId;
    //角色表主键
    private  String rolePkId;
    //菜单表主键
    private  String departPkId;
    //描述
    private  String description;
    //创建人
    private  String createdBy;
    //创建时间
    private  Date createdDate;
    //修改人
    private  String updatedBy;
    //修改时间
    private  Date updatedDate;
    //标志位
    private  String flag;
    //备注
    private  String remark;
	public String getPkId() {
		return pkId;
	}
	public void setPkId(String pkId) {
		this.pkId = pkId;
	}
	public String getRolePkId() {
		return rolePkId;
	}
	public void setRolePkId(String rolePkId) {
		this.rolePkId = rolePkId;
	}
	public String getDepartPkId() {
		return departPkId;
	}
	public void setDepartPkId(String departPkId) {
		this.departPkId = departPkId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
    
    
}

package com.ciitc.webServices.model;

import java.util.Date;

public class AppUserInfor {
    private String pkId;

    private String phoneNumber;

    private String userpwd;

    private Date createDate;

    private String userName;

    private String idcardType;

    private String idcardNumber;

    private String region;

    private String imageUrl;

    private String financialId;

    private long integral;

    private String nameStatus;

    private String customerId;

    private String customerSex;

    private String customerAge;

    private Date customerBirthday;

    private String customerAddress;

    private String customerInvestmentVolume;

    private String customerLevel;

    private String customerPhoto;

    private String customerLabel;

    private String remark1;

    private String remark2;

    private String remark3;
    
    private String remark4;
    
    private String financialName;
    
    private String financialPwd;
    //合格投资者
    private String hgState;
    //专业投资者
    private String zyState;
    //2018-11-27 新增是否删除字段
    private String cIsDelete;
    
    public String getcIsDelete() {
		return cIsDelete;
	}

	public void setcIsDelete(String cIsDelete) {
		this.cIsDelete = cIsDelete;
	}

	public String getHgState() {
		return hgState;
	}

	public void setHgState(String hgState) {
		this.hgState = hgState;
	}

	public String getZyState() {
		return zyState;
	}

	public void setZyState(String zyState) {
		this.zyState = zyState;
	}

	public String getRemark4() {
		return remark4;
	}

	public String getFinancialName() {
		return financialName;
	}

	public void setFinancialName(String financialName) {
		this.financialName = financialName;
	}

	public String getFinancialPwd() {
		return financialPwd;
	}

	public void setFinancialPwd(String financialPwd) {
		this.financialPwd = financialPwd;
	}

	public void setRemark4(String remark4) {
		this.remark4 = remark4;
	}

	public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber == null ? null : phoneNumber.trim();
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd == null ? null : userpwd.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getIdcardType() {
        return idcardType;
    }

    public void setIdcardType(String idcardType) {
        this.idcardType = idcardType == null ? null : idcardType.trim();
    }

    public String getIdcardNumber() {
        return idcardNumber;
    }

    public void setIdcardNumber(String idcardNumber) {
        this.idcardNumber = idcardNumber == null ? null : idcardNumber.trim();
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region == null ? null : region.trim();
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    public String getFinancialId() {
        return financialId;
    }

    public void setFinancialId(String financialId) {
        this.financialId = financialId == null ? null : financialId.trim();
    }


    public long getIntegral() {
		return integral;
	}

	public void setIntegral(long integral) {
		this.integral = integral;
	}

	public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus == null ? null : nameStatus.trim();
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId == null ? null : customerId.trim();
    }

    public String getCustomerSex() {
        return customerSex;
    }

    public void setCustomerSex(String customerSex) {
        this.customerSex = customerSex == null ? null : customerSex.trim();
    }

    public String getCustomerAge() {
        return customerAge;
    }

    public void setCustomerAge(String customerAge) {
        this.customerAge = customerAge == null ? null : customerAge.trim();
    }

    public Date getCustomerBirthday() {
        return customerBirthday;
    }

    public void setCustomerBirthday(Date customerBirthday) {
        this.customerBirthday = customerBirthday;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress == null ? null : customerAddress.trim();
    }

    public String getCustomerInvestmentVolume() {
        return customerInvestmentVolume;
    }

    public void setCustomerInvestmentVolume(String customerInvestmentVolume) {
        this.customerInvestmentVolume = customerInvestmentVolume == null ? null : customerInvestmentVolume.trim();
    }

    public String getCustomerLevel() {
        return customerLevel;
    }

    public void setCustomerLevel(String customerLevel) {
        this.customerLevel = customerLevel == null ? null : customerLevel.trim();
    }

    public String getCustomerPhoto() {
        return customerPhoto;
    }

    public void setCustomerPhoto(String customerPhoto) {
        this.customerPhoto = customerPhoto == null ? null : customerPhoto.trim();
    }

    public String getCustomerLabel() {
        return customerLabel;
    }

    public void setCustomerLabel(String customerLabel) {
        this.customerLabel = customerLabel == null ? null : customerLabel.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
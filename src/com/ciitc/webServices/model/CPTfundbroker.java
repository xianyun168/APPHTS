package com.ciitc.webServices.model;

public class CPTfundbroker {
    private String cBrokeraccount;

    private String cUsercode;

    private String cBrokername;

    private String cDuty;

    private String cPostcode;

    private String cAddress;

    private String cPhonecode;

    private String cMobile;

    private String cEmail;

    private String cSex;

    private String cDeptnodecode;

    private String cDeptnodename;
    
    private String cFartherbroker;


    
    public String getcFartherbroker() {
		return cFartherbroker;
	}

	public void setcFartherbroker(String cFartherbroker) {
		this.cFartherbroker = cFartherbroker;
	}

	public String getcBrokeraccount() {
        return cBrokeraccount;
    }

    public void setcBrokeraccount(String cBrokeraccount) {
        this.cBrokeraccount = cBrokeraccount == null ? null : cBrokeraccount.trim();
    }

    public String getcUsercode() {
        return cUsercode;
    }

    public void setcUsercode(String cUsercode) {
        this.cUsercode = cUsercode == null ? null : cUsercode.trim();
    }

    public String getcBrokername() {
        return cBrokername;
    }

    public void setcBrokername(String cBrokername) {
        this.cBrokername = cBrokername == null ? null : cBrokername.trim();
    }

    public String getcDuty() {
        return cDuty;
    }

    public void setcDuty(String cDuty) {
        this.cDuty = cDuty == null ? null : cDuty.trim();
    }

    public String getcPostcode() {
        return cPostcode;
    }

    public void setcPostcode(String cPostcode) {
        this.cPostcode = cPostcode == null ? null : cPostcode.trim();
    }

    public String getcAddress() {
        return cAddress;
    }

    public void setcAddress(String cAddress) {
        this.cAddress = cAddress == null ? null : cAddress.trim();
    }

    public String getcPhonecode() {
        return cPhonecode;
    }

    public void setcPhonecode(String cPhonecode) {
        this.cPhonecode = cPhonecode == null ? null : cPhonecode.trim();
    }

    public String getcMobile() {
        return cMobile;
    }

    public void setcMobile(String cMobile) {
        this.cMobile = cMobile == null ? null : cMobile.trim();
    }

    public String getcEmail() {
        return cEmail;
    }

    public void setcEmail(String cEmail) {
        this.cEmail = cEmail == null ? null : cEmail.trim();
    }

    public String getcSex() {
        return cSex;
    }

    public void setcSex(String cSex) {
        this.cSex = cSex == null ? null : cSex.trim();
    }

    public String getcDeptnodecode() {
        return cDeptnodecode;
    }

    public void setcDeptnodecode(String cDeptnodecode) {
        this.cDeptnodecode = cDeptnodecode == null ? null : cDeptnodecode.trim();
    }

    public String getcDeptnodename() {
        return cDeptnodename;
    }

    public void setcDeptnodename(String cDeptnodename) {
        this.cDeptnodename = cDeptnodename == null ? null : cDeptnodename.trim();
    }
}
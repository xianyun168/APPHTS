package com.ciitc.webServices.model;

import java.util.Date;

public class TsaleProjectInfo {
    private String pkId;

    private String productname;

    private String pruducttype;

    private String productmanager;

    private String productcustodian;

    private String outsourcingserviceprovider;

    private String scale;

    private String deadline;

    private String perminbalance;

    private String yield;

    private String raisename;

    private String raisebank;

    private String raiseacco;

    private String remark1;

    private String remark2;

    private String remark3;

    private Date actualDate;

    private Date defaultDate;

    private String bonustype;

    private String productthrowup;

    private String riskincome;

    private String rate;

    private String pledge;

    private String agreement;

    private String clause;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname == null ? null : productname.trim();
    }

    public String getPruducttype() {
        return pruducttype;
    }

    public void setPruducttype(String pruducttype) {
        this.pruducttype = pruducttype == null ? null : pruducttype.trim();
    }

    public String getProductmanager() {
        return productmanager;
    }

    public void setProductmanager(String productmanager) {
        this.productmanager = productmanager == null ? null : productmanager.trim();
    }

    public String getProductcustodian() {
        return productcustodian;
    }

    public void setProductcustodian(String productcustodian) {
        this.productcustodian = productcustodian == null ? null : productcustodian.trim();
    }

    public String getOutsourcingserviceprovider() {
        return outsourcingserviceprovider;
    }

    public void setOutsourcingserviceprovider(String outsourcingserviceprovider) {
        this.outsourcingserviceprovider = outsourcingserviceprovider == null ? null : outsourcingserviceprovider.trim();
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale == null ? null : scale.trim();
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline == null ? null : deadline.trim();
    }

    public String getPerminbalance() {
        return perminbalance;
    }

    public void setPerminbalance(String perminbalance) {
        this.perminbalance = perminbalance == null ? null : perminbalance.trim();
    }

    public String getYield() {
        return yield;
    }

    public void setYield(String yield) {
        this.yield = yield == null ? null : yield.trim();
    }

    public String getRaisename() {
        return raisename;
    }

    public void setRaisename(String raisename) {
        this.raisename = raisename == null ? null : raisename.trim();
    }

    public String getRaisebank() {
        return raisebank;
    }

    public void setRaisebank(String raisebank) {
        this.raisebank = raisebank == null ? null : raisebank.trim();
    }

    public String getRaiseacco() {
        return raiseacco;
    }

    public void setRaiseacco(String raiseacco) {
        this.raiseacco = raiseacco == null ? null : raiseacco.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }

    public Date getActualDate() {
        return actualDate;
    }

    public void setActualDate(Date actualDate) {
        this.actualDate = actualDate;
    }

    public Date getDefaultDate() {
        return defaultDate;
    }

    public void setDefaultDate(Date defaultDate) {
        this.defaultDate = defaultDate;
    }

    public String getBonustype() {
        return bonustype;
    }

    public void setBonustype(String bonustype) {
        this.bonustype = bonustype == null ? null : bonustype.trim();
    }

    public String getProductthrowup() {
        return productthrowup;
    }

    public void setProductthrowup(String productthrowup) {
        this.productthrowup = productthrowup == null ? null : productthrowup.trim();
    }

    public String getRiskincome() {
        return riskincome;
    }

    public void setRiskincome(String riskincome) {
        this.riskincome = riskincome == null ? null : riskincome.trim();
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate == null ? null : rate.trim();
    }

    public String getPledge() {
        return pledge;
    }

    public void setPledge(String pledge) {
        this.pledge = pledge == null ? null : pledge.trim();
    }

    public String getAgreement() {
        return agreement;
    }

    public void setAgreement(String agreement) {
        this.agreement = agreement == null ? null : agreement.trim();
    }

    public String getClause() {
        return clause;
    }

    public void setClause(String clause) {
        this.clause = clause == null ? null : clause.trim();
    }
}
package com.ciitc.webServices.model;

public class Reservation {
	private String c_fundcode;//产品编号
	private String c_fundname;//产品名称
	private Integer num;//购买人数
	private Integer countMoney;//购买总额
	private String status;//产品状态
	private Integer remainingSum;//产品余额
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getRemainingSum() {
		return remainingSum;
	}
	public void setRemainingSum(Integer remainingSum) {
		this.remainingSum = remainingSum;
	}
	public String getC_fundcode() {
		return c_fundcode;
	}
	public void setC_fundcode(String c_fundcode) {
		this.c_fundcode = c_fundcode;
	}
	public String getC_fundname() {
		return c_fundname;
	}
	public void setC_fundname(String c_fundname) {
		this.c_fundname = c_fundname;
	}
	public Integer getNum() {
		return num;
	}
	public void setNum(Integer num) {
		this.num = num;
	}
	public Integer getCountMoney() {
		return countMoney;
	}
	public void setCountMoney(Integer countMoney) {
		this.countMoney = countMoney;
	}
	
	

}

package com.ciitc.webServices.model;

public class FileUp {
	private String l_storageid;
	private String c_catalog;
	private Integer v_serialno ;//输出的值             
    private String p_filename ;//为文件名 如 客服APP测试指引.pdf
    private String v_creator;// 理财师编号 select C_CREATOR from tunregister WHERE C_CUSTNO = 传入客户号;         
    private long p_filesize;//文件大小 单位：字节  如 38564
    private String v_nodecode;//理财师所属部门select C_DEPTNODECODE  from tfundbroker where C_BROKERACCOUNT = v_creator;
	
	public String getL_storageid() {
		return l_storageid;
	}
	public void setL_storageid(String l_storageid) {
		this.l_storageid = l_storageid;
	}
	public String getC_catalog() {
		return c_catalog;
	}
	public void setC_catalog(String c_catalog) {
		this.c_catalog = c_catalog;
	}
	public Integer getV_serialno() {
		return v_serialno;
	}
	public void setV_serialno(Integer v_serialno) {
		this.v_serialno = v_serialno;
	}
	public String getP_filename() {
		return p_filename;
	}
	public void setP_filename(String p_filename) {
		this.p_filename = p_filename;
	}
	public String getV_creator() {
		return v_creator;
	}
	public void setV_creator(String v_creator) {
		this.v_creator = v_creator;
	}
	public long getP_filesize() {
		return p_filesize;
	}
	public void setP_filesize(long p_filesize) {
		this.p_filesize = p_filesize;
	}
	public String getV_nodecode() {
		return v_nodecode;
	}
	public void setV_nodecode(String v_nodecode) {
		this.v_nodecode = v_nodecode;
	}

    
}

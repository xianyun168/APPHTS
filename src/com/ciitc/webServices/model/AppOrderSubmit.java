package com.ciitc.webServices.model;

import java.util.Date;

public class AppOrderSubmit {
    private String pkId;

    private String goodsId;

    private String userId;

    private Long goodsNumber;

    private Date countDate;

    private String renmark1;

    private String renmark2;

    private String renmark3;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId == null ? null : goodsId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Long getGoodsNumber() {
        return goodsNumber;
    }

    public void setGoodsNumber(Long goodsNumber) {
        this.goodsNumber = goodsNumber;
    }

    public Date getCountDate() {
        return countDate;
    }

    public void setCountDate(Date countDate) {
        this.countDate = countDate;
    }

    public String getRenmark1() {
        return renmark1;
    }

    public void setRenmark1(String renmark1) {
        this.renmark1 = renmark1 == null ? null : renmark1.trim();
    }

    public String getRenmark2() {
        return renmark2;
    }

    public void setRenmark2(String renmark2) {
        this.renmark2 = renmark2 == null ? null : renmark2.trim();
    }

    public String getRenmark3() {
        return renmark3;
    }

    public void setRenmark3(String renmark3) {
        this.renmark3 = renmark3 == null ? null : renmark3.trim();
    }
}
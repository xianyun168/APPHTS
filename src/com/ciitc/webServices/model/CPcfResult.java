package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 产品收益表
 * @author 任之阳
 *
 */
public class CPcfResult {
     //主键
	private int CF_PK;
	
	 //现金流发生日期
	private Date Data_date;
	
	 //客户ID（CRM）
	private String C_CUSTNO;
	 //产品ID（CRM）
	private String C_FUNDINFO;
	
	 //营销项目ID（CRM）
	private int L_PROSERIALNO;
	//营销项目名称
	private String productName;
	
	 //现金流类型
	private String Fin_element_ID;
	
	 //资产负债类型
	private String ID_Type;
	
	 //现金流金额
	private double Amount;
	
	 //付息确认标识
	private String Confirm_Flag;
	//收益确认标识
	private String PROFIT_STATUS;
	
	//本期开始计息日期
	private Date Start_Date;
	
	//合同ID
    private String Contract_Id;

    private String cTrustcontractid;
    //收益时间格式转换
    private String shouyiDate;
    //客户名称
    private String userName;
    private String remark1;
    private String remark2;
    private String money;
    
    private String flag;
    
    //客户名
    private String c_custnm;
    //客户手机号
    private String c_mobile;
    //产品名称
    private String c_fundnm;
    //营销项目名称
    private String l_proserialnm;
    
	public String getC_custnm() {
		return c_custnm;
	}

	public void setC_custnm(String c_custnm) {
		this.c_custnm = c_custnm;
	}

	public String getC_mobile() {
		return c_mobile;
	}

	public void setC_mobile(String c_mobile) {
		this.c_mobile = c_mobile;
	}

	public String getC_fundnm() {
		return c_fundnm;
	}

	public void setC_fundnm(String c_fundnm) {
		this.c_fundnm = c_fundnm;
	}

	public String getL_proserialnm() {
		return l_proserialnm;
	}

	public void setL_proserialnm(String l_proserialnm) {
		this.l_proserialnm = l_proserialnm;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getRemark2() {
		return remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getRemark1() {
		return remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getShouyiDate() {
		return shouyiDate;
	}

	public void setShouyiDate(String shouyiDate) {
		this.shouyiDate = shouyiDate;
	}

	public String getcTrustcontractid() {
		return cTrustcontractid;
	}

	public void setcTrustcontractid(String cTrustcontractid) {
		this.cTrustcontractid = cTrustcontractid;
	}

	public int getCF_PK() {
		return CF_PK;
	}

	public void setCF_PK(int cF_PK) {
		CF_PK = cF_PK;
	}

	public Date getData_date() {
		return Data_date;
	}

	public void setData_date(Date data_date) {
		Data_date = data_date;
	}

	public String getC_CUSTNO() {
		return C_CUSTNO;
	}

	public void setC_CUSTNO(String c_CUSTNO) {
		C_CUSTNO = c_CUSTNO;
	}

	public String getC_FUNDINFO() {
		return C_FUNDINFO;
	}

	public void setC_FUNDINFO(String c_FUNDINFO) {
		C_FUNDINFO = c_FUNDINFO;
	}

	public int getL_PROSERIALNO() {
		return L_PROSERIALNO;
	}

	public void setL_PROSERIALNO(int l_PROSERIALNO) {
		L_PROSERIALNO = l_PROSERIALNO;
	}

	public String getFin_element_ID() {
		return Fin_element_ID;
	}

	public void setFin_element_ID(String fin_element_ID) {
		Fin_element_ID = fin_element_ID;
	}

	public String getID_Type() {
		return ID_Type;
	}

	public void setID_Type(String iD_Type) {
		ID_Type = iD_Type;
	}

	public double getAmount() {
		return Amount;
	}

	public void setAmount(double amount) {
		Amount = amount;
	}

	public String getConfirm_Flag() {
		return Confirm_Flag;
	}

	public void setConfirm_Flag(String confirm_Flag) {
		Confirm_Flag = confirm_Flag;
	}

	public Date getStart_Date() {
		return Start_Date;
	}

	public void setStart_Date(Date start_Date) {
		Start_Date = start_Date;
	}

	public String getContract_Id() {
		return Contract_Id;
	}

	public void setContract_Id(String contract_Id) {
		Contract_Id = contract_Id;
	}

	public String getPROFIT_STATUS() {
		return PROFIT_STATUS;
	}

	public void setPROFIT_STATUS(String pROFIT_STATUS) {
		PROFIT_STATUS = pROFIT_STATUS;
	}
    
}

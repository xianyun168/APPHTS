package com.ciitc.webServices.model;

import java.util.Date;

public class EsbmmCodeType  {

    //
    private  String pkId;
    //
    private  String typeName;
    //
    private  String chineseName;
    //
    private  String status;
    //
    private  String createdBy;
    //
    private  Date createdDate;
    //
    private  String updatedBy;
    //
    private  Date updatedDate;
    //
    private  String flag;
    //
    private  String remark;
    private String userPkId;

	 public String getPkId() {
		return this.pkId;
	 }
	
	 public void setPkId(String pkId) {
		this.pkId = pkId;
	 }
	 
	 public String getTypeName() {
		return this.typeName;
	 }
	
	 public void setTypeName(String typeName) {
		this.typeName = typeName;
	 }
	 
	 public String getChineseName() {
		return this.chineseName;
	 }
	
	 public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	 }
	 
	 public String getStatus() {
		return this.status;
	 }
	
	 public void setStatus(String status) {
		this.status = status;
	 }
	 
	 public String getCreatedBy() {
		return this.createdBy;
	 }
	
	 public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	 }
	 
	 public Date getCreatedDate() {
		return this.createdDate;
	 }
	
	 public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	 }
	 
	 public String getUpdatedBy() {
		return this.updatedBy;
	 }
	
	 public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	 }
	 
	 public Date getUpdatedDate() {
		return this.updatedDate;
	 }
	
	 public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	 }
	 
	 public String getFlag() {
		return this.flag;
	 }
	
	 public void setFlag(String flag) {
		this.flag = flag;
	 }
	 
	 public String getRemark() {
		return this.remark;
	 }
	
	 public void setRemark(String remark) {
		this.remark = remark;
	 }

	public String getUserPkId() {
		return userPkId;
	}

	public void setUserPkId(String userPkId) {
		this.userPkId = userPkId;
	}
	 
	 
	 
 }
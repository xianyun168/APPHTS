package com.ciitc.webServices.model;

import java.util.Date;
/**
 * 缴款申请表
 * @author tdl
 *
 */
public class PaymentApplication {
    //主键
	private String pkId;
	//预约编号
    private String orderId;
    //缴款金额
    private Long appleMoney;
    //银行卡号
    private String bankCardNumber;
    //所属银行
    private String bank;
    //证明图片
    private String proofPictureUrl;
    //缴款时间
    private Date paymentTime;
    //缴款申请状态(00 预约、01 待审核、02 通过、03 驳回)
    private String paymentApplicationState;
    //标志(00 申请、01 撤销申请)
    private String flag;
    //
    private String remark1;
    //
    private String remark2;
    //
    private String remark3;

    public String getPkId() {
        return pkId;
    }

    public void setPkId(String pkId) {
        this.pkId = pkId == null ? null : pkId.trim();
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId == null ? null : orderId.trim();
    }

    public Long getAppleMoney() {
        return appleMoney;
    }

    public void setAppleMoney(Long appleMoney) {
        this.appleMoney = appleMoney;
    }

    public String getBankCardNumber() {
        return bankCardNumber;
    }

    public void setBankCardNumber(String bankCardNumber) {
        this.bankCardNumber = bankCardNumber == null ? null : bankCardNumber.trim();
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank == null ? null : bank.trim();
    }

    public String getProofPictureUrl() {
        return proofPictureUrl;
    }

    public void setProofPictureUrl(String proofPictureUrl) {
        this.proofPictureUrl = proofPictureUrl == null ? null : proofPictureUrl.trim();
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getPaymentApplicationState() {
        return paymentApplicationState;
    }

    public void setPaymentApplicationState(String paymentApplicationState) {
        this.paymentApplicationState = paymentApplicationState == null ? null : paymentApplicationState.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getRemark1() {
        return remark1;
    }

    public void setRemark1(String remark1) {
        this.remark1 = remark1 == null ? null : remark1.trim();
    }

    public String getRemark2() {
        return remark2;
    }

    public void setRemark2(String remark2) {
        this.remark2 = remark2 == null ? null : remark2.trim();
    }

    public String getRemark3() {
        return remark3;
    }

    public void setRemark3(String remark3) {
        this.remark3 = remark3 == null ? null : remark3.trim();
    }
}
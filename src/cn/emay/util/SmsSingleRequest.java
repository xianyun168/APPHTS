package cn.emay.util;


/**
 * 单条短信发送参数
 * @author Frank
 *
 */
public class SmsSingleRequest{
	
	private String mobile;//手机号

	private String content;//消息内容
	
	private String timerTime;//定时发送时间
	
	private Long requestTime;//请求时间
	
	private Integer requestValidPeriod;//请求有效时间
	
	private String customSmsId;//自定义消息ID
	
	private String extendCode;//扩展字段
	
	public String getTimerTime() {
		return timerTime;
	}

	public void setTimerTime(String timerTime) {
		this.timerTime = timerTime;
	}

	public Long getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Long requestTime) {
		this.requestTime = requestTime;
	}

	public Integer getRequestValidPeriod() {
		return requestValidPeriod;
	}

	public void setRequestValidPeriod(Integer requestValidPeriod) {
		this.requestValidPeriod = requestValidPeriod;
	}

	public String getExtendCode() {
		return extendCode;
	}

	public void setExtendCode(String extendCode) {
		this.extendCode = extendCode;
	}


	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCustomSmsId() {
		return customSmsId;
	}

	public void setCustomSmsId(String customSmsId) {
		this.customSmsId = customSmsId;
	}
	
}

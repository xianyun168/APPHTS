package cn.emay.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.ciitc.webServices.model.SmsSend;
import com.ciitc.webServices.util.Log;


public class SMSMessageUntil {
	
	/**
	 * 发送单条短信
	 * @param isGzip 是否压缩
	 */
	public  String sendSingleSms(SmsSingleRequest smsSingleRequest,boolean isGzip,APPSendMessageVo aPPSendMessageVo,SmsSend smsSend) {
		String state = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar afterDate = Calendar.getInstance();
		try {
			Date date = sdf.parse(smsSingleRequest.getTimerTime());
			afterDate.setTime(date);
			afterDate.add(Calendar.MINUTE, 5);
			smsSingleRequest.setTimerTime(sdf.format(afterDate.getTime()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Log.getLogger().info("=============开始单条发送==================");
		ResultModel1 result = request(smsSingleRequest, "http://" + aPPSendMessageVo.getHost() + "/inter/sendSingleSMS",isGzip,aPPSendMessageVo);
		System.out.println("result code :" + result.getCode());
		if(null != result.getCode() && !"".equals(result.getCode())) {
			if("SUCCESS".equals(result.getCode())){
				SmsResponse response = JsonHelperUntil.fromJson(SmsResponse.class, result.getResult());
				if (response != null) {
					try {
						smsSend.setSendDate(sdf.format(smsSend.getTimingSendDate()));
						smsSend.setSmsSendState("已发送");
						state = "发送成功";
					} catch (Exception e) {
						e.printStackTrace();
					}
					Log.getLogger().info("data : " + response.getMobile() + "," + response.getSmsId() + "," + response.getCustomSmsId());
				}else {
					state = "发送失败";
				}
			}else if("ERROR_SMS_TIME".equals(result.getCode())) {
				Log.getLogger().info("定时时间过早");
			}else if("ERROR_REQUEST_NO_VALID".equals(result.getCode())) {
				Log.getLogger().info("请求超时");
			}
		}else {
			state = "发送失败";
		}
		
		Log.getLogger().info("=============结束单条发送==================");
		return state;
	}
	
	/**
	 * 发送批次短信
	 * @param isGzip 是否压缩
	 */
	public  String sendBatchSms(SmsBatchRequest1 smsBatchRequest1,boolean isGzip,APPSendMessageVo aPPSendMessageVo,SmsSend smsSend) {
		String state = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar afterDate = Calendar.getInstance();
		try {
			Date date = sdf.parse(smsBatchRequest1.getTimerTime());
			afterDate.setTime(date);
			afterDate.add(Calendar.MINUTE, 5);
			smsBatchRequest1.setTimerTime(sdf.format(afterDate.getTime()));
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Log.getLogger().info("=============开始批量发送==================");
		ResultModel1 result = request(smsBatchRequest1, "http://" + aPPSendMessageVo.getHost() + "/inter/sendBatchSMS",isGzip,aPPSendMessageVo);
		System.out.println("result code :" + result.getCode());
		if(null != result.getCode() && !"".equals(result.getCode())) {
			if("SUCCESS".equals(result.getCode())){
				SmsResponse[] response = JsonHelperUntil.fromJson(SmsResponse[].class, result.getResult());
				if (response != null) {
					for (SmsResponse d : response) {
						try {
							smsSend.setSendDate(sdf.format(smsSend.getTimingSendDate()));
							smsSend.setSmsSendState("已发送");
							state = "发送成功";
						} catch (Exception e) {
							e.printStackTrace();
						}
						Log.getLogger().info("data:" + d.getMobile() + "," + d.getSmsId() + "," + d.getCustomSmsId());
					}
				}else {
					state = "发送失败";
				}
			}else if("ERROR_SMS_TIME".equals(result.getCode())) {
				Log.getLogger().info("定时时间过早");
			}else if("ERROR_REQUEST_NO_VALID".equals(result.getCode())) {
				Log.getLogger().info("请求超时");
			}
		}else {
			state = "发送失败";
		}
		
		Log.getLogger().info("=============结束批量发送==================");
		return state;
	}
	
	/**
	 * 公共请求方法
	 */
	public  ResultModel1 request(Object content, String url,final boolean isGzip,APPSendMessageVo aPPSendMessageVo) {
		EmayHttpRequestBytes request = null;
		Map<String, String> headers = new HashMap<String, String>();
		try {
			headers.put("appId", aPPSendMessageVo.getAppId());
			String requestJson = JsonHelperUntil.toJsonString(content);
			//System.out.println("result json: " + requestJson);
			byte[] bytes = requestJson.getBytes("UTF-8");
			//System.out.println("request data size : " + bytes.length);
			if (isGzip) {
				headers.put("gzip", "on");
				bytes = GZIPUtils.compress(bytes);
				//System.out.println("request data size [com]: " + bytes.length);
			}
			byte[] parambytes = AES.encrypt(bytes, aPPSendMessageVo.getSecretKey().getBytes("UTF-8"), aPPSendMessageVo.getAlgorithm());
			//System.out.println("request data size [en] : " + parambytes.length);
			request = new EmayHttpRequestBytes(url, "UTF-8", "POST", headers, null, parambytes);
		} catch (Exception e) {
			Log.getLogger().info("加密异常");
			e.printStackTrace();
		}
		EmayHttpClient client = new EmayHttpClient();
		String code = null;
		String result = null;
		try {
			EmayHttpResponseBytes res = client.service(request, new EmayHttpResponseBytesPraser());
			System.out.println(res.getResultCode());
				if (res.getResultCode().equals(EmayHttpResultCode.SUCCESS)) {
					if (res.getHttpCode() == 200) {
						code = res.getHeaders().get("result");
						if (code.equals("SUCCESS")) {
							byte[] data = res.getResultBytes();
							//System.out.println("response data size [en and com] : " + data.length);
							data = AES.decrypt(data, aPPSendMessageVo.getSecretKey().getBytes("UTF-8"), aPPSendMessageVo.getAlgorithm());
							if (isGzip) {
								data = GZIPUtils.decompress(data);
							}
							//System.out.println("response data size : " + data.length);
							result = new String(data, "UTF-8");
							//System.out.println("response json: " + result);
						}
					} else {
						Log.getLogger().info("请求接口异常,请求码:" + res.getHttpCode());
					}
				} else {
					Log.getLogger().info("请求接口网络异常:" + res.getResultCode().getCode());
				}
			
		} catch (Exception e) {
			Log.getLogger().info("解析失败");
			e.printStackTrace();
		}
		ResultModel1 re = new ResultModel1(code, result);
		return re;
	}
	
}

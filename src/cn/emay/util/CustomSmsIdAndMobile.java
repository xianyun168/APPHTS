package cn.emay.util;

import java.io.Serializable;

/**
 * 自定义SMSID 手机号
 * @author Frank
 *
 */
public class CustomSmsIdAndMobile{

	
	private String customSmsId;//自定义ID
	
	private String mobile;//手机号
	
	public CustomSmsIdAndMobile(){
		
	}
	
	public CustomSmsIdAndMobile(String customSmsId,String mobile){
		this.customSmsId = customSmsId;
		this.mobile = mobile;
	}

	public String getCustomSmsId() {
		return customSmsId;
	}

	public void setCustomSmsId(String customSmsId) {
		this.customSmsId = customSmsId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

}

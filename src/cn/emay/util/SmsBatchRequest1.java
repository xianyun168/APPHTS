package cn.emay.util;

/**
 * 批量短信发送参数
 * @author Frank
 *
 */
public class SmsBatchRequest1{

	private CustomSmsIdAndMobile[] smses;//多个手机号和对应的自定义ID
	
	private String content;//消息内容
	
	private String timerTime;//定时发送时间
	
	private Long requestTime;//请求时间
	
	private Integer requestValidPeriod;//请求有效时间
	
	private String extendCode;//扩展字段
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public CustomSmsIdAndMobile[] getSmses() {
		return smses;
	}

	public void setSmses(CustomSmsIdAndMobile[] smses) {
		this.smses = smses;
	}

	public String getTimerTime() {
		return timerTime;
	}

	public void setTimerTime(String timerTime) {
		this.timerTime = timerTime;
	}

	public Long getRequestTime() {
		return requestTime;
	}

	public void setRequestTime(Long requestTime) {
		this.requestTime = requestTime;
	}

	public Integer getRequestValidPeriod() {
		return requestValidPeriod;
	}

	public void setRequestValidPeriod(Integer requestValidPeriod) {
		this.requestValidPeriod = requestValidPeriod;
	}

	public String getExtendCode() {
		return extendCode;
	}

	public void setExtendCode(String extendCode) {
		this.extendCode = extendCode;
	}
	
	
}

 package cn.com.ins.sinosoft.framework.base.web;
 


import cn.com.ins.sinosoft.framework.base.interceptor.DateConvertEditor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
 
 @Controller
 @RequestMapping({"/baseController"})
 public class BaseController 
 {
   @InitBinder
   public void initBinder(ServletRequestDataBinder binder)
   {
     binder.registerCustomEditor(Date.class, new DateConvertEditor());
   }
 
   public Map<String, Object> getRootMap()
   {
     Map rootMap = new HashMap();
     return rootMap;
   }
 }


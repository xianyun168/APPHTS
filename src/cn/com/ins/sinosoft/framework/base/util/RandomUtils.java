 package cn.com.ins.sinosoft.framework.base.util;
 
 import java.util.Random;
 
 public class RandomUtils
 {
   public static int getRandom(int max)
   {
     return new Random().nextInt(max);
   }
 
   public static int getRandom(int min, int max)
   {
     int r = getRandom(max - min);
     return r + min;
   }
 
   public static long getRandomLong(long max)
   {
     long randNum = (long)(Math.random() * max);
     return randNum;
   }
 
   public static long getRandomLong(long min, long max)
   {
     long r = getRandomLong(max - min);
     return r + min;
   }
 
   public static long getSQLRandom(Long num)
   {
     Long newNum = Long.valueOf(getRandomLong(num.longValue()));
     String numStr = String.valueOf(newNum);
     if (numStr.length() < 8) {
       return newNum.longValue();
     }
     int randLen = getRandom(8, numStr.length());
     return Long.valueOf(numStr.substring(0, randLen)).longValue();
   }
 }


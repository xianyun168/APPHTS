package cn.com.ins.sinosoft.framework.base.util;


import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;




import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.UserInfo;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.entity.SessionInfo;


public class ResourceUtil
{
private static final ResourceBundle bundle = ResourceBundle
			.getBundle("sysConfig");


public static final String getSessionattachmenttitle(String sessionName)
{
	return bundle.getString(sessionName);
		}

	
	public static final UserInfo getSessionUserName()
	{
		HttpSession session = SpringContextUtil.getSession();
		session.setMaxInactiveInterval(-1);
		if (session.getAttributeNames().hasMoreElements()) {
			SessionInfo sessionInfo1 = (SessionInfo) session
					.getAttribute(Globals.USER_SESSION);
			if (sessionInfo1 != null) {
				return sessionInfo1.getUser();
				}
			
			return null;
			}
		
		return null;
		}

	
	public static String getRequestPath(HttpServletRequest request)
	{
		String requestPath = request.getRequestURI() + "?"
				+ request.getQueryString();
		if (requestPath.indexOf("&") > -1) {
			requestPath = requestPath.substring(0,
					requestPath.indexOf("&"));
			}
		requestPath = requestPath.substring(request.getContextPath()
				.length() + 1);
		return requestPath;
		}

	
	public static final String getConfigByName(String name)
	{
		return bundle.getString(name);
		}

	
	public static final Map<Object, Object> getConfigMap(String path)
	{
		ResourceBundle bundle = ResourceBundle.getBundle(path);
		Set set = ((Map) bundle).keySet();
		return StringUtil.SetToMap(set);
		}

	
	public static String getSysPath()
	{
		String path = Thread.currentThread().getContextClassLoader()
				.getResource("").toString();
		String temp = path.replaceFirst("file:/", "").replaceFirst(
				"WEB-INF/classes/", "");
		String separator = System.getProperty("file.separator");
		String resultPath = temp
				.replaceAll("/", separator + separator).replaceAll("%20", " ");
		return resultPath;
		}

	
	public static String getPorjectPath()
	{
		String nowpath = System.getProperty("user.dir");
		String tempdir = nowpath.replace("bin", "webapps");
		tempdir = tempdir + "\\";
		return tempdir;
		}

	
	public static String getClassPath() {
		String path = Thread.currentThread().getContextClassLoader()
				.getResource("").toString();
		String temp = path.replaceFirst("file:/", "");
		String separator = System.getProperty("file.separator");
		String resultPath = temp
				.replaceAll("/", separator + separator);
		return resultPath;
		}

	public static String getSystempPath() {
		return System.getProperty("java.io.tmpdir");
		}

	
	public static String getSeparator() {
		return System.getProperty("file.separator");
		}

	
	public static String getParameter(String field) {
		HttpServletRequest request = SpringContextUtil.getRequest();
		return request.getParameter(field);
		}

	
	public static final String getJdbcUrl()
	{
		return bundle.getString("jdbc.url").toLowerCase();
		}

	
	public static void main(String[] args) {
		System.out.println(getPorjectPath());
		System.out.println(getSysPath());
		}

	
	public static final String getUserAuthority()
	{
		HttpSession session = SpringContextUtil.getSession();
		session.setMaxInactiveInterval(-1);
		if (session.getAttributeNames().hasMoreElements()) {
			SessionInfo sessionInfo = (SessionInfo) session
					.getAttribute(Globals.USER_SESSION);
			if (sessionInfo != null) {
				return sessionInfo.getAuthority();
				}
			
			return null;
			}
		
		return null;
		}

	
	public static final List<RoleDetailInfo> getSessionRoleFunction(
			String roleId)
	{
		HttpSession session = SpringContextUtil.getSession();
		session.setMaxInactiveInterval(-1);
		if (session.getAttributeNames().hasMoreElements()) {
			List roleFunctionList = (List) session
					.getAttribute(roleId);
			if ((roleFunctionList != null)
					&& (roleFunctionList.size() > 0)) {
				return roleFunctionList;
				}
			return null;
			}
		
		return null;
		}

	
	public static final List<String> getSessionRoleDeparts()
	{
		HttpSession session = SpringContextUtil.getSession();
		session.setMaxInactiveInterval(-1);
		if (session.getAttributeNames().hasMoreElements()) {
			SessionInfo sessionInfo1 = (SessionInfo) session
					.getAttribute(Globals.USER_SESSION);
			if (sessionInfo1 != null) {
				return sessionInfo1.getDeparts();
				}
			return null;
			}
		
		return null;
		}
	
}


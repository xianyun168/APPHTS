 package cn.com.ins.sinosoft.framework.base.util;
 
 import java.io.BufferedOutputStream;
 import java.io.File;
 import java.io.FileOutputStream;
 import java.io.IOException;
 import java.io.InputStream;
 import java.io.OutputStream;
 import java.util.Iterator;
 import java.util.List;
 import java.util.Set;
 import org.springframework.util.MultiValueMap;
 import org.springframework.web.multipart.MultipartFile;
 import org.springframework.web.multipart.MultipartHttpServletRequest;
 
 public class PluploadUtil
 {
   private static final int BUF_SIZE = 2048;
   public static final String RESP_SUCCESS = "{\"jsonrpc\" : \"2.0\", \"result\" : \"success\", \"id\" : \"id\"}";
   public static final String RESP_ERROR = "{\"jsonrpc\" : \"2.0\", \"error\" : {\"code\": 101, \"message\": \"Failed to open input stream.\"}, \"id\" : \"id\"}";
 
   public static void upload(Plupload plupload, File dir)
     throws IllegalStateException, IOException
   {
     String filename = plupload.getName();
     upload(plupload, dir, filename);
   }
 
   public static void upload(Plupload plupload, File dir, String filename)
     throws IllegalStateException, IOException
   {
     int chunks = plupload.getChunks();
     int chunk = plupload.getChunk();
     MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)plupload.getRequest();
     MultiValueMap map = multipartRequest.getMultiFileMap();
     if (map != null) {
       if (!dir.exists()) dir.mkdirs();
 
       Iterator iter = map.keySet().iterator();
       while (iter.hasNext()) {
         String str = (String)iter.next();
         List<MultipartFile> fileList = (List)map.get(str);
         for (MultipartFile multipartFile : fileList)
         {
           plupload.setMultipartFile(multipartFile);
 
           File targetFile = new File(dir.getPath() + File.separator + filename);
 
           if (chunks > 1)
           {
             File tempFile = new File(dir.getPath() + File.separator + multipartFile.getName());
 
             saveUploadFile(multipartFile.getInputStream(), tempFile, chunk != 0);
 
             if (chunks - chunk == 1) {
               tempFile.renameTo(targetFile);
             }
           }
           else
           {
             multipartFile.transferTo(targetFile);
           }
         }
       }
     }
   }
 
   private static void saveUploadFile(InputStream input, File targetFile, boolean append)
     throws IOException
   {
     OutputStream out = null;
     try {
       if ((targetFile.exists()) && (append))
         out = new BufferedOutputStream(new FileOutputStream(targetFile, true), 2048);
       else {
         out = new BufferedOutputStream(new FileOutputStream(targetFile), 2048);
       }
 
       byte[] buffer = new byte[2048];
       int len = 0;
 
       while ((len = input.read(buffer)) > 0)
         out.write(buffer, 0, len);
     }
     catch (IOException e) {
       throw e;
     }
     finally {
       if (input != null) {
         try {
           input.close();
         } catch (IOException e) {
           e.printStackTrace();
         }
       }
       if (out != null)
         try {
           out.close();
         } catch (IOException e) {
           e.printStackTrace();
         }
     }
   }
 
   public static boolean isUploadFinish(Plupload plupload)
   {
     return plupload.getChunks() - plupload.getChunk() == 1;
   }
 }


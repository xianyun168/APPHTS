 package cn.com.ins.sinosoft.framework.base.util;
 
 import java.beans.PropertyDescriptor;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;

import com.ciitc.webServices.model.UserInfo;

 public class ClassReflectUtil
 {
   public static Object getInstance(String name)
     throws Exception
   {
     Class cls = Class.forName(name);
     return cls.newInstance();
   }
 
   public static void reflect(Object obj)
   {
     Class cls = obj.getClass();
     System.out.println("************构  造  器************");
     Constructor[] constructors = cls.getConstructors();
     for (Constructor constructor : constructors) {
       System.out.println("构造器名称:" + constructor.getName() + "\t" + "    " + 
         "构造器参数类型:" + 
         Arrays.toString(constructor.getParameterTypes()));
     }
     System.out.println("************属     性************");
     Field[] fields = cls.getDeclaredFields();
 
     for (Field field : fields) {
       System.out.println("属性名称:" + field.getName() + "\t" + "属性类型:" + 
         field.getType() + "\t");
     }
     System.out.println("************方   法************");
     Method[] methods = cls.getMethods();
     for (Method method : methods)
       System.out.println("方法名:" + method.getName() + "\t" + "方法返回类型：" + 
         method.getReturnType() + "\t" + "方法参数类型:" + 
         Arrays.toString(method.getParameterTypes()));
   }
 
   public static Object getFieldValue(Object obj, String filedname)
     throws Exception
   {
     Class cls = obj.getClass();
     Field field = null;
     try
     {
       field = cls.getDeclaredField(filedname);
     } catch (Exception e) {
       e.printStackTrace();
       System.out.println("没有这个字段：" + filedname);
     }
 
     field.setAccessible(true);
 
     Object val = field.get(obj);
     return val;
   }
 
   public static Object setIdKeyValue(Object obj, String filedname, String value)
     throws Exception
   {
     Class cls = obj.getClass();
     Field field = null;
     try
     {
       field = cls.getDeclaredField(filedname);
     } catch (Exception e) {
       e.printStackTrace();
       System.out.println("没有这个字段：" + filedname);
     }
     if (field == null) {
       return null;
     }
 
     field.setAccessible(true);
 
     if ((!field.getType().equals(Integer.class)) && 
       (!field.getType().equals(Long.class))) {
       field.set(obj, value);
     }
 
     Object val = field.get(obj);
     field.setAccessible(false);
     return val;
   }
 
   public static void setFieldValue(Object obj, String fieldName, String fieldValue)
   {
     Class cls = obj.getClass();
     Field field = null;
     try
     {
       field = cls.getDeclaredField(fieldName);
     } catch (Exception e) {
       e.printStackTrace();
       System.out.println("没有这个字段：" + fieldName);
     }
     if (field != null)
       try {
         field.setAccessible(true);
         field.set(obj, fieldValue);
       }
       catch (IllegalArgumentException e) {
         e.printStackTrace();
       }
       catch (IllegalAccessException e) {
         e.printStackTrace();
       }
   }
 
   public static Object readObjMethod(Object obj, String methodName, Class<?>[] paramTypes, Object[] params)
     throws Exception
   {
     Class cls = obj.getClass();
 
     Method method = cls.getDeclaredMethod(methodName, paramTypes);
 
     method.setAccessible(true);
     Object val = method.invoke(obj, params);
     return val;
   }
 
   public static void main(String[] args)
   {
   }
 
   public static void setFieldValueNotNull(Object obj, String value)
     throws Exception
   {
     Class clazz = obj.getClass();
     Field[] fields = obj.getClass().getDeclaredFields();
     for (Field field : fields) {
       PropertyDescriptor pd = new PropertyDescriptor(field.getName(), 
         clazz);
       Method getMethod = pd.getReadMethod();
       Object o = getMethod.invoke(obj, new Object[0]);
       field.setAccessible(true);
 
       if (o == null) {
         if (field.getType().equals(Date.class))
         {
           Date date = DateUtil.getDate();
           field.set(obj, date);
         } else if (field.getType().equals(Integer.class)) {
           field.set(obj, Integer.valueOf(0));
         } else if ((field.getName().equals("createdBy")) || (field.getName().equals("updatedBy"))) {
           UserInfo user = ResourceUtil.getSessionUserName();
           String userPkId = "";
           if (user != null) {
             userPkId = user.getPkId();
           }
           field.set(obj, userPkId);
         } else if (field.getType().equals(Timestamp.class)) {
           Timestamp time = Timestamp.valueOf("1111-01-01 00:00:00.000");
           field.set(obj, time);
         } else {
           field.set(obj, "");
         }
 
       }
 
       field.setAccessible(false);
     }
   }
 
   public static void updateFieldValues(Object obj)
     throws Exception
   {
     Class clazz = obj.getClass();
     Field[] fields = obj.getClass().getDeclaredFields();
     for (Field field : fields) {
       PropertyDescriptor pd = new PropertyDescriptor(field.getName(), 
         clazz);
       Method getMethod = pd.getReadMethod();
       Object o = getMethod.invoke(obj, new Object[0]);
       field.setAccessible(true);
 
       if (o == null) {
         if (field.getName().equals("updatedDate"))
         {
           Date date = DateUtil.getDate();
           field.set(obj, date);
         } else if (field.getName().equals("updatedBy")) {
           UserInfo user = ResourceUtil.getSessionUserName();
           String userPkId = "";
           if (user != null) {
             userPkId = user.getPkId();
           }
           field.set(obj, userPkId);
         }
       }
 
       field.setAccessible(false);
     }
   }
 }


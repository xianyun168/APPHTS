 package cn.com.ins.sinosoft.framework.base.util;
 
 public class TimeoutThread extends Thread
 {
   private long timeout;
   private boolean isCanceled = false;
   private TimeoutException timeoutException;
 
   public TimeoutThread(long timeout, TimeoutException timeoutErr)
   {
     this.timeout = timeout;
     this.timeoutException = timeoutErr;
 
     setDaemon(true);
   }
 
   public synchronized void cancel()
   {
     this.isCanceled = true;
   }
 
   public void run()
   {
     try
     {
       Thread.sleep(this.timeout);
       if (!this.isCanceled)
         throw this.timeoutException;
     } catch (InterruptedException ie) {
       ie.printStackTrace();
     } catch (TimeoutException te) {
       te.printStackTrace();
     }
   }
 }


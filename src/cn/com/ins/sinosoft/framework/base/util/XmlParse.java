 package cn.com.ins.sinosoft.framework.base.util;
 
 import java.io.InputStream;
 import java.lang.reflect.Field;
 import java.lang.reflect.ParameterizedType;
 import java.lang.reflect.Type;
 import java.util.ArrayList;
 import java.util.List;
 import org.xmlpull.v1.XmlPullParser;
 import org.xmlpull.v1.XmlPullParserFactory;
 
 public class XmlParse
 {
   public static List getXmlList(InputStream is, Class<?> clazz, String startName)
   {
     List list = null;
     try {
       XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
 
       XmlPullParser parser = pullParserFactory.newPullParser();
       parser.setInput(is, "UTF-8");
       Object object = null;
       int eventType = parser.getEventType();
       try
       {
         while (eventType != 1) {
           switch (eventType) {
           case 0:
             list = new ArrayList();
             break;
           case 2:
             String name = parser.getName();
             if (startName.equals(name)) {
               object = clazz.newInstance();
 
               int count = parser.getAttributeCount();
               for (int i = 0; i < count; i++)
                 setXmlValue(object, parser.getAttributeName(i), parser.getAttributeValue(i)); 
             } else {
               if (object == null) break;
               setXmlValue(object, name, parser.nextText());
             }
             break;
           case 3:
             if (!startName.equals(parser.getName())) break;
             list.add(object);
             object = null;
           case 1:
           }
 
           eventType = parser.next();
         }
       } catch (Exception e) {
         e.printStackTrace();
       }
     }
     catch (Exception localException1)
     {
     }
     return list;
   }
 
/*   public Object getXmlObject(InputStream is, Class<?> clazz)
   {
     Object object = null;
     List list = null;
     Object subObject = null;
     String subName = null;
     try {
       XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
 
       XmlPullParser parser = pullParserFactory.newPullParser();
       try
       {
         parser.setInput(is, "UTF-8");
 
         int eventType = parser.getEventType();
 
         while (eventType != 1)
         {
           String name;
           Field[] f;
           int i;
           switch (eventType) {
           case 0:
             object = clazz.newInstance();
             break;
           case 2:
             name = parser.getName();
 
             f = (Field[])null;
             if (subObject == null) {
               f = object.getClass().getDeclaredFields();
 
               int count = parser.getAttributeCount();
               for (int j = 0; j < count; j++)
                 setXmlValue(object, parser.getAttributeName(j), parser.getAttributeValue(j));
             } else {
               f = subObject.getClass().getDeclaredFields();
             }
 
             i = 0; break;
           case 3:
             while (true) if (f[i].getName().equalsIgnoreCase(name))
               {
                 if (f[i].getType().getName().equals("java.util.List")) {
                   Type type = f[i].getGenericType();
                   if (!(type instanceof ParameterizedType))
                     break;
                   Class subClazz = (Class)((ParameterizedType)type).getActualTypeArguments()[0];
                   subObject = subClazz.newInstance();
                   subName = f[i].getName();
 
                   int count = parser.getAttributeCount();
                   for (int j = 0; j < count; j++) {
                     setXmlValue(subObject, parser.getAttributeName(j), parser.getAttributeValue(j));
                   }
                   if (list != null) break;
                   list = new ArrayList();
                   f[i].setAccessible(true);
                   f[i].set(object, list);
                 }
                 else if (subObject != null) {
                   setXmlValue(subObject, name, parser.nextText());
                 } else {
                   setXmlValue(object, name, parser.nextText());
                 }
               }
               else
               {
                 i++; if (i < f.length)
                 {
                   continue;
                 }
 
                 if ( (subObject == null) || (!subName.equalsIgnoreCase(parser.getName()))) break;
                 list.add(subObject);
                 subObject = null;
                 subName = null;
               }
           case 1:
           }
           eventType = parser.next();
         }
       } catch (Exception e) {
         e.printStackTrace();
       }
     } catch (Exception localException1) {
     }
     return object;
   }*/
 
   private static void setXmlValue(Object t, String name, String value)
   {
     try
     {
       Field[] f = t.getClass().getDeclaredFields();
       for (int i = 0; i < f.length; i++)
         if (f[i].getName().equalsIgnoreCase(name)) {
           f[i].setAccessible(true);
 
           Class fieldType = f[i].getType();
 
           if (fieldType == String.class)
             f[i].set(t, value);
           else if (fieldType == Integer.TYPE)
             f[i].set(t, Integer.valueOf(Integer.parseInt(value)));
           else if (fieldType == Float.TYPE)
             f[i].set(t, Float.valueOf(Float.parseFloat(value)));
           else if (fieldType == Double.TYPE)
             f[i].set(t, Double.valueOf(Double.parseDouble(value)));
           else if (fieldType == Long.TYPE)
             f[i].set(t, Long.valueOf(Long.parseLong(value)));
           else if (fieldType == Short.TYPE)
             f[i].set(t, Short.valueOf(Short.parseShort(value)));
           else if (fieldType == Boolean.TYPE)
             f[i].set(t, Boolean.valueOf(Boolean.parseBoolean(value)));
           else
             f[i].set(t, value);
         }
     }
     catch (Exception e)
     {
       e.printStackTrace();
     }
   }
 }

/* Location:           C:\Users\macaijie\Desktop\sinosoftframework.jar
 * Qualified Name:     cn.com.ins.sinosoft.framework.base.util.XmlParse
 * JD-Core Version:    0.6.0
 */
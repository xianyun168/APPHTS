 package cn.com.ins.sinosoft.framework.base.util.json;
 
 import org.json.JSONString;
 
 public class JSONStringObject
   implements JSONString
 {
   private String jsonString = null;
 
   public JSONStringObject(String jsonString) {
     this.jsonString = jsonString;
   }
 
   public String toString()
   {
     return this.jsonString;
   }
 
   public String toJSONString() {
     return this.jsonString;
   }
 }


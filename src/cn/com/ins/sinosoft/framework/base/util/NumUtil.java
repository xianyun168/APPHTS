 package cn.com.ins.sinosoft.framework.base.util;
 
 import java.io.PrintStream;
 import java.math.BigDecimal;
 import java.text.NumberFormat;
 
 public class NumUtil
 {
   public static BigDecimal toBig(Object o)
   {
     if ((o == null) || (o.toString().equals("")) || (o.toString().equals("NaN"))) {
       return new BigDecimal(0);
     }
     return new BigDecimal(o.toString());
   }
 
   public static String getPercent(Object divisor, Object dividend, boolean flag)
   {
     if ((divisor == null) || (dividend == null)) {
       return "";
     }
     NumberFormat percent = NumberFormat.getPercentInstance();
 
     percent.setMaximumFractionDigits(2);
     BigDecimal a = toBig(divisor);
     BigDecimal b = toBig(dividend);
     double startPercent = 0.5D;
     if ((a.equals(toBig(Integer.valueOf(0)))) || (b.equals(toBig(Integer.valueOf(0)))) || (a.equals(toBig(Double.valueOf(0.0D)))) || 
       (b.equals(toBig(Double.valueOf(0.0D))))) {
       return "0.00%";
     }
     BigDecimal c = a.divide(b, 4, 1);
 
     c = c.divide(toBig(Integer.valueOf(2)));
     if (!flag) {
       c = c.add(new BigDecimal(startPercent));
     }
     return percent.format(c);
   }
 
   public static void main(String[] args) {
     String s = getPercent(Integer.valueOf(1), Integer.valueOf(4), false);
     System.out.print(s);
   }
 
   public static String divideNumber(Object divisor, Object dividend)
   {
     if ((divisor == null) || (dividend == null)) {
       return "";
     }
     BigDecimal a = toBig(divisor);
     BigDecimal b = toBig(dividend);
     if ((a.equals(toBig(Integer.valueOf(0)))) || (b.equals(toBig(Integer.valueOf(0))))) {
       return "0";
     }
     BigDecimal c = a.divide(b, 2, 1);
     return c.toString();
   }
 
   public static String divideNumberHalf(Object divisor, Object dividend, Object totle, boolean flag)
   {
     if ((divisor == null) || (dividend == null)) {
       return "";
     }
     BigDecimal a = toBig(divisor);
     BigDecimal b = toBig(dividend);
     BigDecimal t = toBig(totle);
     if ((a.equals(toBig(Integer.valueOf(0)))) || (b.equals(toBig(Integer.valueOf(0))))) {
       return "0";
     }
     BigDecimal c = a.divide(b, 2, 1);
     t = t.divide(b, 2, 1);
 
     c = c.divide(toBig(Integer.valueOf(2)));
     t = t.divide(toBig(Integer.valueOf(2)));
     if (!flag) {
       c = c.add(t);
     }
     return c.toString();
   }
 
   public static BigDecimal getLongToBig(Object divisor, Object dividend) {
     BigDecimal a = toBig(divisor);
     BigDecimal b = toBig(dividend);
     BigDecimal c = a.divide(b, 2, 1);
     return c;
   }
 
   public static int averageNumber(Object divisor, Object dividend)
   {
     if ((divisor == null) || (dividend == null)) {
       return 0;
     }
     BigDecimal a = toBig(divisor);
     BigDecimal b = toBig(dividend);
     if ((a.equals(toBig(Integer.valueOf(0)))) || (b.equals(toBig(Integer.valueOf(0))))) {
       return 0;
     }
     BigDecimal c = a.divide(b, 0, 4);
     return c.intValue();
   }
 
   public static String getRemain(BigDecimal longToBig, String mbRead) {
     BigDecimal mbReads = toBig(mbRead);
     BigDecimal remain = longToBig.subtract(mbReads);
     remain.divide(toBig(Integer.valueOf(1)), 2, 1);
     return remain.toString();
   }
 }


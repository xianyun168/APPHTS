 package cn.com.ins.sinosoft.framework.base.interceptor;
 
 import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

 import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

 import cn.com.ins.sinosoft.framework.base.util.http.HttpRequestUtil;
 import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.view.RedirectView;

import com.ciitc.webServices.model.MenuInfo;
import com.ciitc.webServices.model.RoleDetailInfo;
import com.ciitc.webServices.model.SysPermissionInfo;
import com.ciitc.webServices.model.UserInfo;
import com.ciitc.webServices.service.SysPermissionInfoService;
import com.ciitc.webServices.service.SystemService;

import cn.com.ins.sinosoft.framework.base.constant.Globals;
import cn.com.ins.sinosoft.framework.base.entity.SessionInfo;
import cn.com.ins.sinosoft.framework.base.util.ResourceUtil;
import cn.com.ins.sinosoft.framework.base.util.SpringContextUtil;
 
 
 
 
 
 
 
 
 public class AuthInterceptor extends HandlerInterceptorAdapter
 {
   private static final Logger logger = Logger.getLogger(AuthInterceptor.class);
 
   @Autowired(required=false)
   private SystemService<Object> systemService;
 
   @Autowired(required=false)
   private SysPermissionInfoService<SysPermissionInfo> sysPermissionInfoService;
   private List<String> excludeUrls;
 
   public List<String> getExcludeUrls() { return this.excludeUrls; }
 
   public void setExcludeUrls(List<String> excludeUrls)
   {
     this.excludeUrls = excludeUrls;
   }
 
   public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception exception)
     throws Exception
   {
   }
 
   public void postHandle(HttpServletRequest request, HttpServletResponse response, Object object, ModelAndView modelAndView)
     throws Exception
   {
   }
 
   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object object)
     throws Exception
   {
     String requestUri = request.getRequestURI();
     String contextPath = request.getContextPath();
     String url = requestUri.substring(contextPath.length());
     String requestPath = ResourceUtil.getRequestPath(request);
     HttpSession session = SpringContextUtil.getSession();
     SessionInfo sessioninfo = (SessionInfo)session
       .getAttribute(Globals.USER_SESSION);
     if (this.excludeUrls.contains(requestPath)) {
       return true;
     }
     if(!HttpRequestUtil.isRole()){
       return false;
     }
     if(url.equals("/orderInforController.do") || url.equals("/goddsAddressController.do") || url.equals("/appShoppingCarInforController.do")){
    	 return true;
     }
     if ((sessioninfo != null) && (sessioninfo.getUser() != null))
     {
       if (!hasMenuAuth(request)) {
         response.sendRedirect("loginController.do?noAuth=key");
         return false;
       }
       String menuId = request.getParameter("clickMenuId");
       List<RoleDetailInfo> roleMenuList;
       if (menuId == null)
       {
         MenuInfo menuinfo = this.systemService.getMenuId(requestPath);
 
         if (menuinfo != null) {
           List<SysPermissionInfo> list = this.sysPermissionInfoService
             .queryByUserPkId(sessioninfo.getUser().getPkId());
           for (SysPermissionInfo sys : list) {
             roleMenuList = ResourceUtil.getSessionRoleFunction(sys.getRolePkId());
 
             if (roleMenuList.size() > 0) {
               for (RoleDetailInfo roleMenu : roleMenuList) {
                 if (menuinfo.getPkId().equals(roleMenu.getMenuPkId())) {
                   menuId = menuinfo.getPkId();
                 }
               }
             }
           }
         }
       }
       if ((menuId != null) && (!"".equals(menuId))) {
         Set operationCodes = this.systemService.getOperationCodesByUserIdAndFunctionId(sessioninfo.getUser().getPkId(), menuId);
         request.setAttribute("operationCodes", operationCodes);
       }
 
       if ((menuId != null) && (!"".equals(menuId))) {
         List<String> allOperation = this.systemService.findOperationCodes(menuId);
 
         List newall = new ArrayList();
         if ((allOperation != null) && (allOperation.size() > 0)) {
           for (String s : allOperation) {
             s = s.replaceAll(" ", "");
             newall.add(s);
           }
           List<String> hasOperList = this.systemService.findOperationList(sessioninfo.getUser().getPkId(), menuId);
           if ((hasOperList != null) && (hasOperList.size() > 0))
           {
             for (String strs : hasOperList) {
               for (String s : strs.split(",")) {
                 s = s.replaceAll(" ", "");
                 newall.remove(s);
               }
             }
           }
         }
         request.setAttribute("noauto_operationCodes", newall);
       }
       return true;
     }
     forward(request, response);
     return false;
   }
 
   @RequestMapping(params={"forword"})
   public ModelAndView forword(HttpServletRequest request)
   {
     return new ModelAndView(new RedirectView("loginController.do?login=key"));
   }
 
   private void forward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
     request.getRequestDispatcher("webpage/pages/login/timeout.jsp").forward(
       request, response);
   }
 
   private boolean hasMenuAuth(HttpServletRequest request) {
     String requestPath = ResourceUtil.getRequestPath(request);
     String menuid = request.getParameter("clickMenuId");
     if (menuid == null) menuid = "";
     if ((requestPath.indexOf("loginController.do") != -1) || (menuid.length() == 0)) {
       return true;
     }
     UserInfo user = ResourceUtil.getSessionUserName();
     String userid = user.getPkId();
 
     return this.systemService.checkAuth(userid, requestPath);
   }
 }


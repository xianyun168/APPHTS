 package cn.com.ins.sinosoft.framework.base.interceptor;
 
 import cn.com.ins.sinosoft.framework.base.page.BasePage;
 import cn.com.ins.sinosoft.framework.base.page.Pager;
 import cn.com.ins.sinosoft.framework.base.util.ClassReflectUtil;
 import java.io.PrintStream;
 import java.util.Map;
 import java.util.Properties;
 import org.apache.ibatis.executor.statement.RoutingStatementHandler;
 import org.apache.ibatis.executor.statement.StatementHandler;
 import org.apache.ibatis.mapping.BoundSql;
 import org.apache.ibatis.plugin.Interceptor;
 import org.apache.ibatis.plugin.Intercepts;
 import org.apache.ibatis.plugin.Invocation;
 import org.apache.ibatis.plugin.Plugin;
 
 @Intercepts({@org.apache.ibatis.plugin.Signature(method="prepare", type=StatementHandler.class, args={java.sql.Connection.class})})
 public class MyibatisInterceptor
   implements Interceptor
 {
   private String databaseType;
   private String ifShowSql = "false";
 
   public Object intercept(Invocation invocation) throws Throwable
   {
     RoutingStatementHandler handler = (RoutingStatementHandler)invocation.getTarget();
 
     StatementHandler delegate = (StatementHandler)ClassReflectUtil.getFieldValue(handler, "delegate");
 
     BoundSql boundSql = delegate.getBoundSql();
 
     Object obj = boundSql.getParameterObject();
 
     String sql = boundSql.getSql();
 
     if ((obj instanceof Map)) {
       Map map = (Map)obj;
       BasePage page = (BasePage)map.get("page");
       if (page != null)
       {
         sql = getAuthorSql(page, sql);
 
         ClassReflectUtil.setFieldValue(boundSql, "sql", sql);
       }
     }
     if ("true".equalsIgnoreCase(this.ifShowSql)) {
       System.out.println(sql);
     }
 
     return invocation.proceed();
   }
 
   public Object plugin(Object target)
   {
     if (("oracle".equalsIgnoreCase(this.databaseType)) || ("mysql".equalsIgnoreCase(this.databaseType)) || ("db2".equalsIgnoreCase(this.databaseType))) {
       return Plugin.wrap(target, this);
     }
     return target;
   }
 
   public void setProperties(Properties properties)
   {
     this.databaseType = properties.getProperty("databaseType");
     this.ifShowSql = properties.getProperty("ifShowSql");
   }
 
   private String getAuthorSql(BasePage page, String sql)
   {
     StringBuffer sqlBuffer = new StringBuffer(sql);
     if ("mysql".equalsIgnoreCase(this.databaseType))
       return getMysqlPageSql(page, sqlBuffer);
     if ("oracle".equalsIgnoreCase(this.databaseType)) {
       return getOraclePageSql(page, sqlBuffer);
     }
     return sqlBuffer.toString();
   }
 
   private String getMysqlPageSql(BasePage page, StringBuffer sqlBuffer)
   {
     sqlBuffer.append(page.getPager().getMysqlQueryCondition());
     return sqlBuffer.toString();
   }
 
   private String getOraclePageSql(BasePage page, StringBuffer sqlBuffer)
   {
     if (page.getPager().getRowCount() > 0)
     {
       sqlBuffer.insert(0, "SELECT TEMP.* FROM (select  ROWNUM NUM,T.*,count(1) over() totalRecordNum from(");
 
       if ((page.getPager().getOrderCondition() != null) && ("".equals(page.getPager().getOrderCondition()))) {
         sqlBuffer.append(page.getPager().getOrderCondition());
       }
       sqlBuffer.append(") T )TEMP\n");
       if ((page.getPager().getOracleQueryCondition() != null) && (!"".equals(page.getPager().getOracleQueryCondition()))) {
         sqlBuffer.append(page.getPager().getOracleQueryCondition());
       }
     }
 
     return sqlBuffer.toString();
   }
 }


package cn.com.ins.sinosoft.framework.base.dao;

import cn.com.ins.sinosoft.framework.base.page.BasePage;

import java.util.List;
import java.util.Map;

public abstract interface BaseDao<T>
{
  public abstract void add(T paramT);

  public abstract void update(T paramT);

  public abstract void updateBySelective(T paramT);

  public abstract void delete(Object paramObject);

  public abstract int queryByCount(BasePage paramBasePage);
 

  public abstract List<T> queryByList(BasePage paramBasePage);

  public abstract int queryByCount(Map<String, Object> paramMap);
  

  public abstract List<T> queryByList(Map<String, Object> paramMap);
 
  
  public abstract T queryById(Object paramObject);

  public abstract List<T> queryByParentId(Map<String, Object> paramMap);

}


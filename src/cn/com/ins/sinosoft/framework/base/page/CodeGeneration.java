 package cn.com.ins.sinosoft.framework.base.page;
 
 import java.util.ArrayList;
 import java.util.HashMap;
 import java.util.List;
 import java.util.Map;
 
 public class CodeGeneration
 {
   public static NewPage findByList(List list, int pageNo, int pageSize)
   {
     NewPage page = null;
     if (pageNo <= 0) {
       pageNo = 1;
     }
     if (pageSize <= 0) {
       pageSize = 10;
     }
     int startIndex = NewPage.getStartOfPage(pageNo, pageSize);
     if (startIndex < 0) {
       return new NewPage();
     }
     int totalCount = list.size();
 
     page = new NewPage(pageNo, totalCount, pageSize, list);
     Long totalPageCount = Long.valueOf(page.getTotalPageCount());
     int realPageSize;
     if (pageNo < totalPageCount.longValue())
       realPageSize = pageSize + startIndex;
     else {
       realPageSize = totalCount;
     }
 
     List realList = new ArrayList();
     for (int i = startIndex; i < realPageSize; i++) {
       realList.add(list.get(i));
     }
     NewPage newpage = new NewPage(startIndex, totalCount, pageSize, realList);
 
     return newpage;
   }
 
   public static Map<String, Object> PageForEasyUi(List list, int pageNo, int pageSize)
   {
     NewPage newpage = findByList(list, pageNo, pageSize);
     Map josnMap = new HashMap();
     josnMap.put("total", Long.valueOf(newpage.getTotalCount()));
     josnMap.put("rows", newpage.getResult());
     return josnMap;
   }
 }


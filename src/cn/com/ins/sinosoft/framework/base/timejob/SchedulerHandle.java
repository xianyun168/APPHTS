 package cn.com.ins.sinosoft.framework.base.timejob;
 
 import cn.com.ins.sinosoft.framework.base.util.StringUtil;
 import java.io.BufferedReader;
 import java.io.InputStreamReader;
 import java.io.PrintStream;
 import java.text.ParseException;
 import java.util.Date;
 import java.util.List;
 import org.apache.log4j.Logger;
 import org.quartz.CronTrigger;
 import org.quartz.JobDetail;
 import org.quartz.Scheduler;
 import org.quartz.SchedulerException;
 import org.quartz.SchedulerFactory;
 import org.quartz.SimpleTrigger;
 import org.quartz.Trigger;
 import org.quartz.impl.StdSchedulerFactory;
 
 public class SchedulerHandle
 {
   private Logger log = Logger.getLogger(getClass());
   SchedulerFactory sf = new StdSchedulerFactory();
   private Scheduler sched = null;
   private static SchedulerHandle schedulerHandle = new SchedulerHandle();
   private final String SIMPLE = "simple";
   private final String CRON = "cron";
   private final String TRIGGER_NAME = "_trigger";
   private List<SysplSchedulerJob> jobList;
 
   public static SchedulerHandle getInstance()
   {
     return schedulerHandle;
   }
 
   public void startScheduler(boolean isInit)
     throws Exception
   {
     try
     {
       if (this.sched == null) {
         this.sched = this.sf.getScheduler();
         this.sched.start();
 
         if (isInit)
           initAllJobs(this.jobList);
       }
     } catch (SchedulerException e) {
       e.printStackTrace();
     }
   }
 
   public void stopScheduler()
   {
     try
     {
       if (this.sched != null) {
         this.sched.shutdown();
         this.sched = null;
       }
     } catch (SchedulerException e) {
       e.printStackTrace();
     }
   }
 
   public void startJob(SysplSchedulerJob sysplSchedulerJob)
     throws Exception
   {
     if (StringUtil.isNull(sysplSchedulerJob))
       throw new Exception("sysplSchedulerJob is null.");
     startScheduler(false);
     addJob(sysplSchedulerJob);
   }
 
   public void stopJob(String jobName)
     throws Exception
   {
     try
     {
       if (this.sched == null) return;
       boolean triggerBol = this.sched.unscheduleJob(jobName + "_trigger", "DEFAULT");
       boolean jobBol = this.sched.deleteJob(jobName, "DEFAULT");
       this.log.info(jobName + "_trigger" + " stop result " + triggerBol);
       this.log.info(jobName + " job stop result " + jobBol);
     } catch (SchedulerException e) {
       e.printStackTrace();
     }
   }
 
   private void initAllJobs(List<SysplSchedulerJob> jobList)
     throws Exception
   {
     if (StringUtil.isNull(jobList))
       throw new Exception("jobList is null.");
     for (SysplSchedulerJob sysplSchedulerJob : jobList) {
       addJob(sysplSchedulerJob);
     }
     this.log.info("scheduler has already inited.");
   }
 
   private void addJob(SysplSchedulerJob sysplSchedulerJob)
   {
     String jobName = sysplSchedulerJob.getJobName();
     boolean bol = jobIsExist(jobName);
     if (bol) {
       return;
     }
     String triggerType = sysplSchedulerJob.getTriggerType();
     String jobClassDescript = sysplSchedulerJob.getJobClassDescript();
     String timeExpress = sysplSchedulerJob.getTimeExpress();
     Date startTime = sysplSchedulerJob.getStartTime() == null ? new Date() : sysplSchedulerJob.getStartTime();
     Date endTime = sysplSchedulerJob.getEndTime();
     int repeatCount = sysplSchedulerJob.getRepeatTime() == null ? 0 : sysplSchedulerJob.getRepeatTime().intValue();
     long repeatInterval = sysplSchedulerJob.getSplitTime() == null ? 0L : sysplSchedulerJob.getSplitTime().longValue();
 
     Class clzz = null;
     try {
       clzz = Class.forName(jobClassDescript);
     } catch (ClassNotFoundException e) {
       e.printStackTrace();
     }
     JobDetail job = null;
     Trigger trigger = null;
     try {
       if ("simple".equalsIgnoreCase(triggerType)) {
         job = new JobDetail(jobName, "DEFAULT", clzz);
         trigger = new SimpleTrigger(jobName + "_trigger", "DEFAULT", 
           startTime, endTime, repeatCount, repeatInterval);
       } else if ("cron".equalsIgnoreCase(triggerType)) {
         job = new JobDetail(jobName, "DEFAULT", clzz);
         trigger = new CronTrigger(jobName + "_trigger", "DEFAULT", timeExpress);
       }
       this.sched.scheduleJob(job, trigger);
     } catch (ParseException e) {
       e.printStackTrace();
     } catch (SchedulerException e) {
       e.printStackTrace();
     }
   }
 
   public boolean jobIsExist(String jobName)
   {
     boolean bol = false;
     if (this.sched == null) return bol; try
     {
       String[] jobNames = this.sched.getJobNames("DEFAULT");
       for (String jobNamee : jobNames)
         if (jobNamee.equals(jobName)) {
           bol = true;
           break;
         }
     }
     catch (SchedulerException e) {
       e.printStackTrace();
     }
     return bol;
   }
 
   public static void main(String[] args)
   {
     BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
     SchedulerHandle handle = getInstance();
     try
     {
       while (true)
       {
         String command = "startJob";
         if ("exit".equals(command)) {
           System.exit(0); continue;
         }if ("startScheduler".equals(command)) {
           handle.startScheduler(true); continue;
         }if ("stopScheduler".equals(command)) {
           handle.stopScheduler(); continue;
         }if ("jobtest".equals(command)) {
           boolean bol = handle.jobIsExist("jobtest");
           System.out.println("jobtest wether exist " + bol); continue;
         }if ("startJob".equals(command)) {
           SysplSchedulerJob job2 = new SysplSchedulerJob();
           job2.setJobName("jobtest3");
           handle.getClass(); job2.setTriggerType("cron");
           job2.setJobClassDescript("com.sinosoft.quotation.simulater.scheduler.JobTest");
 
           job2.setTimeExpress("00 49 15 * * ?");
           handle.startJob(job2); continue;
         }if ("stopJob".equals(command))
           handle.stopJob("jobtest2");
       }
     }
     catch (Exception e) {
       e.printStackTrace();
     }
   }
 
   public List<SysplSchedulerJob> getJobList() {
     return this.jobList;
   }
 
   public void setJobList(List<SysplSchedulerJob> jobList) {
     this.jobList = jobList;
   }
 
   public Scheduler getSched() {
     return this.sched;
   }
 
   public void setSched(Scheduler sched) {
     this.sched = sched;
   }
 }


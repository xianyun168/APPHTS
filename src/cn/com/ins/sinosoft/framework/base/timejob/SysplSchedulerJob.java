 package cn.com.ins.sinosoft.framework.base.timejob;
 
 import java.io.Serializable;
 import java.util.Date;
 
 public class SysplSchedulerJob
   implements Serializable
 {
   private String pkId;
   private String jobName;
   private String jobClassDescript;
   private String triggerType;
   private String timeExpress;
   private Date startTime;
   private Date endTime;
   private Integer repeatTime;
   private Long splitTime;
   private String status;
   private String memo;
   private String creator;
   private Date createDate;
 
   public SysplSchedulerJob()
   {
   }
 
   public SysplSchedulerJob(String pkId, String status)
   {
     this.pkId = pkId;
     this.status = status;
   }
 
   public SysplSchedulerJob(String pkId, String jobName, String jobClassDescript, String triggerType, String timeExpress, Date startTime, Date endTime, Integer repeatTime, Long splitTime, String status, String memo, String creator, Date createDate)
   {
     this.pkId = pkId;
     this.jobName = jobName;
     this.jobClassDescript = jobClassDescript;
     this.triggerType = triggerType;
     this.timeExpress = timeExpress;
     this.startTime = startTime;
     this.endTime = endTime;
     this.repeatTime = repeatTime;
     this.splitTime = splitTime;
     this.status = status;
     this.memo = memo;
     this.creator = creator;
     this.createDate = createDate;
   }
 
   public String getPkId()
   {
     return this.pkId;
   }
 
   public void setPkId(String pkId) {
     this.pkId = pkId;
   }
 
   public String getJobName() {
     return this.jobName;
   }
 
   public void setJobName(String jobName) {
     this.jobName = jobName;
   }
 
   public String getJobClassDescript() {
     return this.jobClassDescript;
   }
 
   public void setJobClassDescript(String jobClassDescript) {
     this.jobClassDescript = jobClassDescript;
   }
 
   public String getTriggerType() {
     return this.triggerType;
   }
 
   public void setTriggerType(String triggerType) {
     this.triggerType = triggerType;
   }
 
   public String getTimeExpress() {
     return this.timeExpress;
   }
 
   public void setTimeExpress(String timeExpress) {
     this.timeExpress = timeExpress;
   }
 
   public Date getStartTime() {
     return this.startTime;
   }
 
   public void setStartTime(Date startTime) {
     this.startTime = startTime;
   }
 
   public Date getEndTime() {
     return this.endTime;
   }
 
   public void setEndTime(Date endTime) {
     this.endTime = endTime;
   }
 
   public Integer getRepeatTime() {
     return this.repeatTime;
   }
 
   public void setRepeatTime(Integer repeatTime) {
     this.repeatTime = repeatTime;
   }
 
   public Long getSplitTime() {
     return this.splitTime;
   }
 
   public void setSplitTime(Long splitTime) {
     this.splitTime = splitTime;
   }
 
   public String getStatus() {
     return this.status;
   }
 
   public void setStatus(String status) {
     this.status = status;
   }
 
   public String getMemo() {
     return this.memo;
   }
 
   public void setMemo(String memo) {
     this.memo = memo;
   }
 
   public String getCreator() {
     return this.creator;
   }
 
   public void setCreator(String creator) {
     this.creator = creator;
   }
 
   public Date getCreateDate() {
     return this.createDate;
   }
 
   public void setCreateDate(Date createDate) {
     this.createDate = createDate;
   }
 }


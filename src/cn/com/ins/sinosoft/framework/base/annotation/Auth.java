package cn.com.ins.sinosoft.framework.base.annotation;

import java.lang.annotation.Annotation;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({java.lang.annotation.ElementType.METHOD})
@Documented
@Inherited
public @interface Auth
{
  public abstract boolean verifyLogin();

  public abstract boolean verifyURL();
}


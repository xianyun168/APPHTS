 package cn.com.ins.sinosoft.framework.base.service;
 
import cn.com.ins.sinosoft.framework.base.dao.BaseDao;
import cn.com.ins.sinosoft.framework.base.page.BasePage;
import cn.com.ins.sinosoft.framework.base.util.ClassReflectUtil;

import java.util.List;
import java.util.Map;
import java.util.UUID;
 
 public abstract class BaseService<T>
 {
   public abstract BaseDao<T> getDao();
 
   public void add(T t)
     throws Exception
   {
     String id = (String)ClassReflectUtil.setIdKeyValue(t, "pkId", UUID.randomUUID().toString());
 
     ClassReflectUtil.setFieldValueNotNull(t, "");
     getDao().add(t);
   }
   
   public void addNoUUID(T t)
		   throws Exception
   {
	   ClassReflectUtil.setFieldValueNotNull(t, "");
	   getDao().add(t);
   }
 
   public void update(T t) throws Exception {
     ClassReflectUtil.updateFieldValues(t);
     getDao().update(t);
   }
 
   public void updateBySelective(T t) {
     try {
       ClassReflectUtil.updateFieldValues(t);
     } catch (Exception e) {
       e.printStackTrace();
     }
     getDao().updateBySelective(t);
   }
 
   public void delete(Object[] ids) throws Exception {
     if ((ids == null) || (ids.length < 1)) {
       return;
     }
     for (Object id : ids)
       getDao().delete(id);
   }
   public void delete(String ids) throws Exception {
/* 51 */       getDao().delete(ids);
   }
 
   public int queryByCount(BasePage page)
     throws Exception
   {
     return getDao().queryByCount(page);
   }
  
   public List<T> queryByList(BasePage page) throws Exception {
     Integer rowCount = Integer.valueOf(queryByCount(page));
     page.getPager().setRowCount(rowCount.intValue());
     return getDao().queryByList(page);
   }
   
   
  
   
   
   public int queryByCount(Map<String, Object> paramMap) throws Exception
   {
     return getDao().queryByCount(paramMap);
   }
 
 
   public List<T> queryByList(Map<String, Object> paramMap)
     throws Exception
   {
     Integer rowCount = Integer.valueOf(queryByCount(paramMap));
     BasePage page = (BasePage)paramMap.get("page");
     page.getPager().setRowCount(rowCount.intValue());
     paramMap.put("page", page);
     return getDao().queryByList(paramMap);
   }
  

 
   public T queryById(Object id) throws Exception {
     return getDao().queryById(id);
   }
 }


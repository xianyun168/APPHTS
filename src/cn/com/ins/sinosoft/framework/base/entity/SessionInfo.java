 package cn.com.ins.sinosoft.framework.base.entity;
 
 import java.io.Serializable;
import java.util.List;

import com.ciitc.webServices.model.UserInfo;
 
 public class SessionInfo
   implements Serializable
 {
   private UserInfo user;
   private String authority;
   private List<String> departs;
 
   public UserInfo getUser()
   {
     return this.user;
   }
 
   public void setUser(UserInfo user) {
     this.user = user;
   }
 
   public String getAuthority() {
     return this.authority;
   }
 
   public void setAuthority(String authority) {
     this.authority = authority;
   }
 
   public List<String> getDeparts() {
     return this.departs;
   }
 
   public void setDeparts(List<String> departs) {
     this.departs = departs;
   }
 }


 package cn.com.ins.sinosoft.framework.base.entity;
 
 import java.io.Serializable;
 import java.util.List;
 import java.util.Map;
 
 public class Tree
   implements Serializable
 {
   private static final long serialVersionUID = 1L;
   private String id = "";
   private String text;
   private String iconCls;
   private Boolean checked = Boolean.valueOf(false);
   private Map<String, Object> attributes;
   private List<Tree> children;
   private String state = "open";
 
   public String getId() {
     return this.id;
   }
 
   public void setId(String id) {
     this.id = id;
   }
 
   public String getText() {
     return this.text;
   }
 
   public void setText(String text) {
     this.text = text;
   }
 
   public Boolean getChecked() {
     return this.checked;
   }
 
   public void setChecked(Boolean checked) {
     this.checked = checked;
   }
 
   public Map<String, Object> getAttributes() {
     return this.attributes;
   }
 
   public void setAttributes(Map<String, Object> attributes) {
     this.attributes = attributes;
   }
 
   public List<Tree> getChildren() {
     return this.children;
   }
 
   public void setChildren(List<Tree> children) {
     this.children = children;
   }
 
   public String getState() {
     return this.state;
   }
 
   public void setState(String state) {
     this.state = state;
   }
 
   public String getIconCls() {
     return this.iconCls;
   }
 
   public void setIconCls(String iconCls) {
     this.iconCls = iconCls;
   }
 }

